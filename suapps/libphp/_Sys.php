<?php
class _Sys{
static $err=false; static $errText='';
static public function _err($errText='',$errNo=3){
	self::$err=true;
	self::$errText=$errText;
}
static public function _exec($cmd='',$P=array()){
	//127-error command simple
	$rt=exec($cmd.' 2>&1',$R,$errNo);
	if($R && preg_match('/sh\: 1\:/',$R[0])){ $errNo=127; }
	if($errNo==2){ self::_err('Error exec #2'); }
	else if($errNo==127){ self::_err('Error exec '.$R[0]); }
	else if($errNo){ self::_err('Error exec '.$errNo); }
	if(self::$err){ $rt= self::$errText.' --> '.$rt; }
	if($P['cmdView']=='Y'){ $rt .= ' query:{{ '.$cmd.' }}'; }
	return $rt;
}
}
?>