<?php
class vAppItm{
 static public function verif($P){
  if(_js::iseErr($P['itemName'],'Debe definir un nombre para el articulo')){}
  else if(_js::textMax($P['itemName'],100,'Nombre')){}
  else if(_js::iseErr($P['sellPrice'],'Debe definir el precio','numeric>0')){}
  else{
   $qD=false;
   if($P['itemId']){
   a_sql::fetch('SELECT itemId FROM sel_oitm WHERE itemId=\''.$P['itemId'].'\' AND ocardId=\''.a_ses::$ocardId.'\' LIMIT 1',array(1=>'Error obteniendo información de articulo: ',2=>'El articulo no fue encontrado para modificar.'));
   }
   if(a_sql::$err){ return _err::err(a_sql::$errText); }
   else if($P['itemCode']!=''){
    $qD=a_sql::fetch('SELECT itemId,itemName FROM sel_oitm WHERE itemCode=\''.$P['itemCode'].'\' AND ocardId=\''.a_ses::$ocardId.'\' LIMIT 1',array(1=>'Error verificando asignación de código'));
    if(a_sql::$err){ return _err::err(a_sql::$errText); }
    else if(a_sql::$errNo==-1 && $qD['itemId']!=$P['itemId']){ _err::err('Ya tienes un artículo con ese código: '.$qD['itemName'],3); }
   }
   return $qD;
  }
 }
 static public function get($D){
 	return a_sql::pagingRows('SELECT * FROM sel_oitm WHERE ocardId=\''.a_ses::$ocardId.'\' ',array(
   1=>'Error obteniendo articulos',2=>'No hay articulos registrados.'
  ));
 }
 static public function getOne($D){
  $a=a_sql::fetch('SELECT * FROM sel_oitm WHERE itemId=\''.$D['itemId'].'\' AND ocardId=\''.a_ses::$ocardId.'\' LIMIT 1',array(1=>'Error obteniendo información de articulo: ',2=>'El articulo no fue encontrado'));
  if(a_sql::$err){ return a_sql::$errNoText; }
  else{
   $a['imgs']=array();
   $qi=a_sql::fetch('SELECT * FROM sel_itm5 WHERE itemId=\''.$D['itemId'].'\' LIMIT 20');
   if(a_sql::$errNo==-1){ unset($qi['itemId']);
    $a['imgs']=$qi;
   }
   return _js::enc2($a);
 }
 }
 static public function post($D){
  self::verif($D);
  if(_err::$err){ return _err::$errText; }
  else{
   $Di=$D['imgs']; unset($D['imgs']);
   $D['ocardId']=a_ses::$ocardId;
   $itemId=a_sql::qInsert($D,array('tbk'=>'sel_oitm'));
   if(a_sql::$err){ return _err::err('Error guardando información: '.a_sql::$errNoText,3); }
   else{
    $D['itemId']=$itemId;
    self::putImgs($Di);
    return _js::r('Articulo modificado correctamente',$D);
   }
  }
  return _err::$errText;
 }
 static public function put($D){
  if(_js::iseErr($D['itemId'],'Se debe definir el Id del articulo')){ return _err::$errText; }
  self::verif($D);
  if(_err::$err){ return _err::$errText; }
  else{
   if(a_sql::$err){ return a_sql::$errNoText; }
   else{
    $Di=$D['imgs']; unset($D['imgs'],$D['ocardId']);
    $Di['itemId']=$D['itemId'];
    a_sql::uniRow($D,array('tbk'=>'sel_oitm','wh_change'=>'itemId=\''.$D['itemId'].'\' LIMIT 1'));
    if(a_sql::$err){ return _err::err('Error actualizando articulo: '.a_sql::$errText,3); }
    else{
     self::putImgs($Di);
     return _js::r('Articulo modificado correctamente',$D);
    }
   }
  }
  return _err::$errText;
 }
 static public function putImgs($D=array()){
  //itemId,src2,src3
  $q=a_sql::uniRow($D,array('tbk'=>'sel_itm5','wh_change'=>'itemId=\''.$D['itemId'].'\' LIMIT 1'));
 }
}
?>
