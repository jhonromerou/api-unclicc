<?php
class crdLgc{
  static public function get($D){
    $D['from']='A.*
    FROM par_olgc A';
    return a_sql::rPaging($D);
  }
  static public function getOne($D){
    if($js=_js::ise($D['uid'],'Se debe definir nombre cobrador.','numeric>0')){ die($js); }
    $M=a_sql::fetch('SELECT A.* 
    FROM par_olgc A 
    WHERE A.uid=\''.$D['uid'].'\' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
    if(a_sql::$err){ die(a_sql::$errNoText); }
    return _js::enc2($M);
  }
  static public function post($D){
    if(_js::iseErr($D['name'],'Se debe definir el nombre del cobrador')){}
    else if(_js::iseErr($D['licTradNum'],'Se debe definir el numero de identificación')){}
    else{
      $uid=$D['uid'];
      $Dx=[];
      $ins=a_sql::uniRow($D,array('tbk'=>'par_olgc','wh_change'=>'uid=\''.$uid.'\' LIMIT 1'));
      if(a_sql::$err){ _err::err('Error guardando información: '.a_sql::$errText,3); }
      else{
        $D['uid']=($ins['insertId'])?$ins['insertId']:$uid;
        $o=array('uid'=>$D['uid'],'v'=>$D['name']);
        $js=_js::r('Información guardada correctamente.',$o);
      }
    }
    _err::errDie();
    return $js;
  }
  static public function put($D=array()){
    if(_js::iseErr($D['uid'],'ID no definido para modificar')){}
    $js=self::post($D);
    _err::errDie();
    return $js;
  }
}
?>