<?php
/*
if(_0s::$router=='GET dt/crdF'){
	echo a_sql::queryJs('SELECT cardCode,'.$_GET['fie'].' FROM par_ocrd WHERE 1',array(1=>'Error obteniendo listado de cliente',2=>'No existe clientes definidos'));
}
else if(_0s::$router=='POST dt/crdF'){
	$js=false;
	$lineEnd=($_GET['lineEnd'] && $_GET['lineEnd']<2000)?$_GET['lineEnd']:500;
	$R = _fread_tab::get($_FILES['file'],array('keyL0'=>'Y','lineIni'=>3,'lineEnd'=>$lineEnd));
	if($R['errNo']){ $js = _js::e($R); }
	else{
		$qI=array(); $ITM=array();
		foreach($R['L'] as $ln => $Da){
			$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
			$lineTotal++;
			if($js=_js::ise($Da['cardCode'],$lnt.'Se debe definir el código del cliente.')){ die($js); }
			else{
				$quk=$Da['cardCode']; unset($Da['cardCode']);
				$QK=_uniK::fromQuery(array('k'=>'cardCode','f'=>'cardId','from'=>'par_ocrd','wh'=>'cardCode=\''.$quk.'\'',
				1=>$lnt.'Error obteniendo Id de socio.',2=>$lnt.'El código del socio ('.$quk.') no existe'));
				if(_err::$err){ die(_err::$errText); }
				$Da[0]='u'; $Da[1]='par_ocrd'; $Da['_wh']='cardId=\''.$QK['cardId'].'\' LIMIT 1';
				$qI[]=$Da;
			}
		}//for
	}
	if($js==false){
		a_sql::multiQuery($qI);
		if(_err::$err){ echo _err::$errText; }
		else{ echo _js::r('Datos actualizados correctamente.'); }
	}
	else{ echo $js; }
}
*/
JRoute::post('crd/dt/cards',function($R){
	$js=false;
	if($R['errNo']){ $js = _js::e($R); }
	else{
		$qI=array(); $ITM=array();
		$nl=1;
		foreach($R['L'] as $ln => $Da){
			$lnt = 'Linea '.$nl.': '; $a = ' Actual: '; $nl++;
			$lineTotal++;
			if($js=_js::ise($Da['cardCode'],$lnt.'Se debe definir el código del cliente.')){ die($js); }
			else{
				if($Da['sucursal']>0){
					$Da['cardCode'] .='-'.$Da['sucursal'];
				}
				unset($Da['sucursal']);
				$quk=$Da['cardCode'];
				$QK=_uniK::fromQuery(array('k'=>'cardCode','f'=>'cardId','from'=>'par_ocrd','wh'=>'cardCode=\''.$quk.'\'', 1=>$lnt.'Error obteniendo Id de tercero.'));
				_err::$err=false;
				if(a_sql::$errNo==1){ die(_err::$errText); }
				else if(a_sql::$errNo==-1){ die(_err::err($lnt.'El código del tercero ('.$quk.') ya existe',3)); }
				$Da[0]='i'; $Da[1]='par_ocrd';
				$R['L'][$ln]=$Da;
			}
		}//for
	}
	if($js==false){
		a_sql::multiQuery($R['L']);
		if(_err::$err){ echo _err::$errText; }
		else{ echo _js::r('Datos actualizados correctamente.'); }
	}
	else{ echo $js; }
});
?>