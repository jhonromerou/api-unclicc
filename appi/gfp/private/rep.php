<?php
JRoute::get('gfp/rep/tcp',function($D){
	$vt=$D['viewType'];
	$year1=$D['docYear'].'-01-01'; $year2=$D['docYear'].'-12-31';
	if($vt!='LP' && (_js::iseErr($D['M_wallId'],'Se debe definir la cuenta','numeric>0') || _js::iseErr($D['C1_pid'],'Corte debe definirse para este reporte','numeric>0'))){}
	else if($vt=='LP' && _js::iseErr($D['docYear'],'Se debe el año para este reporte','numeric>0')){}
	_err::errDie();
	unset($D['viewType'],$D['docYear']);
	_ADMS::lib('sql/filter');
	if($vt=='C'){
		$gb='M.mType,M.credType';
		$query='SELECT '.$gb.',SUM(M.debBal-M.creBal) bal
		FROM gfp_omov M
    JOIN gfp_ctc1 C1 ON (M.docDate>=C1.openDate AND M.docDate<=C1.closeDate)
		WHERE 1 '.a_sql_filtByT($D).' GROUP BY '.$gb;
	}
	else if($vt=='LP'){
		$gb='W.wallId,W.wallName,C1.openDate,C1.closeDate,C1.payDate,C3.pagoMin,C3.puntos';
		$query='SELECT '.$gb.',
		SUM(IF(M.credType=\'compra\',M.creBal,0)) compras,
		SUM(IF(M.credType IN(\'interes\',\'interesM\') AND M.creBal>0,M.creBal,0)) interes,
		SUM(IF(M.credType NOT IN(\'compra\',\'interes\',\'interesM\') AND M.creBal>0,M.creBal,0)) otros,
		SUM(IF(M.debBal>0,M.debBal,0)) pagos
		FROM gfp_owal W
    JOIN gfp_ctc2 C2 ON (C2.wallId=W.wallId)
    JOIN gfp_ctc1 C1 ON (C1.cId=C2.cId)
		JOIN gfp_omov M ON (M.wallId=W.wallId AND M.docDate>=C1.openDate AND M.docDate<=C1.closeDate)
    LEFT JOIN gfp_ctc3 C3 ON (C3.pid=C1.pid AND C3.wallId=W.wallId)
		WHERE W.wallType=\'TC\' AND C1.closeDate BETWEEN \''.$year1.'\' AND \''.$year2.'\' '.a_sql_filtByT($D).' GROUP BY '.$gb.' ORDER BY W.wallCode ASC,C1.openDate ASC';
	}
	else{ $vt='M';
		$gb='M.mid,M.mType,M.credType,M.categId,M.docDate,M.credCuotas,IF(M.debBal>0,M.debBal,-M.creBal) bal,M.lineMemo';
		$query='SELECT '.$gb.'
		FROM gfp_omov M
    JOIN gfp_ctc1 C1 ON (M.docDate>=C1.openDate AND M.docDate<=C1.closeDate)
		WHERE 1 '.a_sql_filtByT($D).' ';
	}
	return a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
},[]);
JRoute::get('gfp/rep/copen',function($D){
	$vt=$D['viewType'];
	//if($vt!='LP' && (_js::iseErr($D['M_wallId'],'Se debe definir la cuenta','numeric>0') || _js::iseErr($D['C1_pid'],'Corte debe definirse para este reporte','numeric>0'))){}
	//else if($vt=='LP' && _js::iseErr($D['docYear'],'Se debe el año para este reporte','numeric>0')){}
	//_err::errDie();
	unset($D['viewType']);
	_ADMS::lib('sql/filter');
	if($vt=='O'){}
	else{ $vt='G';
		$gb='A.mid,W.wallName,W.bankId,A.mType,A.credType,A.categId,A.docDate,A.credCuotas,A.creBal bal,A.credCap,A.credInt,A.credOtros,A.credSal,A.credCuoNum,A.credBalCuota,A.lineMemo';
		$query='SELECT '.$gb.'
		FROM gfp_omov A
		JOIN gfp_owal W ON (W.wallId=A.wallId)
		WHERE A.credCuotas>1 AND W.wallType=\'TC\' AND A.docType=\'CC\' '.a_sql_filtByT($D).' ORDER BY A.docDate DESC';
	}
	return a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
},[]);
JRoute::get('gfp/rep/ctotal',function($D){
	$vt=$D['viewType']; $bankId=$D['bankId'];
	unset($D['viewType'],$D['bankId']);
	_ADMS::lib('sql/filter');
	$wh='A.docType=\'CC\'';
	if($bankId){ $wh .=' AND (W.bankId=\''.$bankId.'\' OR A.bankId=\''.$bankId.'\')'; }
	if($vt=='O'){}
	else{ $vt='G';
		$gb='A.mid,W.wallType,W.wallName,A.mName,W.bankId,A.bankId mbankId,A.mType,A.credType,A.categId,A.docDate,A.credCuotas,A.creBal bal,A.credCap,A.credOtros,A.credBalCuota,A.credInt,A.credSal,A.credCuoNum,A.lineMemo';
		$query='SELECT '.$gb.'
		FROM gfp_omov A
		JOIN gfp_owal W ON (W.wallId=A.wallId)
		WHERE '.$wh.' '.a_sql_filtByT($D).' ORDER BY A.docDate DESC';
	}
	return a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
},[]);
JRoute::get('gfp/rep/crquotes',function($D){
	$vt=$D['viewType']; $bankId=$D['bankId'];
	unset($D['viewType'],$D['bankId']);
	_ADMS::lib('sql/filter');
	$wh='A.docType=\'CC\'';
	if($bankId){ $wh .=' AND (W.bankId=\''.$bankId.'\' OR A.bankId=\''.$bankId.'\')'; }
	if($vt=='O'){}
	else{ $vt='G';
		$gb='A.mid,W.wallType,W.wallName,A.mName,W.bankId,A.bankId mbankId,A.credType,A.categId,A.credCuotas,A.creBal bal,B.capital,B.interes,B.otros,B.saldo,B.lineNum,B.lineDate,A.lineMemo';
		$query='SELECT '.$gb.'
		FROM gfp_mov1 B 
		JOIN gfp_omov A ON (A.mid=B.mid)
		JOIN gfp_owal W ON (W.wallId=A.wallId)
		WHERE '.$wh.' '.a_sql_filtByT($D).' ORDER BY B.lineDate DESC';
	}
	return a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
},[]);
?>
