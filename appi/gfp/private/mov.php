<?php
JRoute::get('gfp/mov','get',['lib'=>'sql/filter']);
JRoute::post('gfp/mov','post');//multi

JRoute::get('gfp/mov/one','oneGet');
JRoute::put('gfp/mov/one','onePut');
JRoute::put('gfp/mov','onePut');
JRoute::put('gfp/mov/oneField','oneFieldPut');
JRoute::delete('gfp/mov','delete');

JRoute::get('gfp/mov/quotes','quotesGet');
JRoute::put('gfp/mov/quotes','quotesPut');
?>
