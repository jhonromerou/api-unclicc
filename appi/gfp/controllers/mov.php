<?php
class gfpMov{
static public function get($P){
 $ordBy=($P['ordBy']=='docDateASC')?'M.docDate ASC,M.mid ASC':'M.docDate DESC,M.mid DESC';
	unset($P['ordBy']);
	$wh='1';
	$jFie=''; $jLeft='';
	if($P['pid']>0){
		$jLeft='JOIN gfp_ctc1 C1 ON (M.docDate>=C1.openDate AND M.docDate<=C1.closeDate)';
		$wh .=' AND C1.pid=\''.$P['pid'].'\'';
	}
	unset($P['pid']);
	return a_sql::queryL('SELECT W.wallType,M.* 
	FROM gfp_omov M 
	JOIN gfp_owal W ON (W.wallId=M.wallId) '.$jLeft.'
	WHERE '.$wh.' '.a_sql_filtByT($P).' ORDER BY '.$ordBy.' '.a_sql::nextLimit(30));
}
static public function post($P=array()){
 	$qI=array(); $sysType=$P['sysType'];
	if($P['sysType']=='CR'){ unset($P['sysType']);
		$P['credSal']=$P['bal'];
		$P['wallId']=1; //1 es credito sistema
		$P=['L'=>[$P]];
	}
 	a_sql::transaction(); $cmt=false;
	if(!is_array($P['L'])){ _err::err('No se envio información',3); }
 	else foreach($P['L'] as $n =>$_J){
 		$isBol=($_J['wbId']>0);
 		$wbId=$_J['wbId']; $bal=$_J['bal'];
 		unset($_J['bal']);
 		if(_js::iseErr($_J['wallId'],'Se debe definir la cuenta.','numeric>0')){ break; }
 		else if(_js::iseErr($_J['mType'],'Se debe el tipo.')){ break; }
 		else if(_js::iseErr($_J['docDate'],'Se debe definir la fecha del movimiento')){ break; }
 		else if(_js::iseErr($bal,'Se debe definir valor de la transacción','numeric>0')){ break; }
 		else if(_js::iseErr($_J['categId'],'Se debe definir la categoria.')){ break; }
 		else{
			_ADMS::libc('gfp','wal');
			if($sysType=='CR' || preg_match('/^(desembolso|compra|avance|rediferido)$/',$_J['credType'])){
				$_J['docType']='CC';
			}
 			$_J['debBal']=$_J['creBal']=0;
 			if($_J['mType']=='I' || $_J['mType']=='IS'){ $_J['debBal']=$bal; }
 			else{ $_J['creBal']=$bal; }
 			$_J['dateC']=date('Y-m-d H:i:s');
 			if($isBol){/* actualizar si es bolsillo */
 				$_J['movInterno']='N';
 				/* solo afectar cuenta si es ingreso o gasto */
 				if($_J['movInterno']=='N'){
 					$qW=a_sql::fetch('SELECT A.wallId
 					FROM gfp_obol B
 					LEFT JOIN gfp_owal A ON (A.wallId=B.wallId)
 					WHERE B.wbId=\''.$_J['wbId'].'\' LIMIT 1 ',array(1=>'Error revisando cuenta del bolsillo.',2=>'El bolsillo no existe.'));
 					if(a_sql::$err){ _err::err(a_sql::$errNoText); break; }
 					$_J['wallId']=$qW['wallId'];
 				}
 				gfpWal::putBal(array('qSet'=>'amount=amount+'.$_J['debBal'].'-'.$_J['creBal']),$_J);
 				if(_err::$err){ break; }
 			}
 			else{ gfpWal::putBal(array('qSet'=>'amount=amount+'.$_J['debBal'].'-'.$_J['creBal']),$_J); }
 			if(_err::$err){ break; }
 			$_J[0]='i'; $_J[1]='gfp_omov';
 			$qI[]=$_J;
 		}
 	}
 	if(!_err::$err){ a_sql::multiQuery($qI); }
  if(_err::$err){ return _err::$errText; }
 	else{ a_sql::transaction(true);
   return _js::r('Movimientos registrados correctamente');
 	}
 }
static public function delete($P){
 if($js=_js::ise($P['mid'],'Id de transación debe estar definido','numeric>0')){ return $js; }
	else{
		_ADMS::libc('gfp','wal');
		a_sql::transaction(); $cmt=false;
		$Da=a_sql::fetch('SELECT wallId,wbId,debBal,creBal FROM gfp_omov WHERE mid=\''.$P['mid'].'\'',array(1=>'Error obteniendo transación a eliminar.',2=>'La transación no existe o ya fue eliminada.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		$De=a_sql::query('DELETE FROM gfp_omov WHERE mid=\''.$P['mid'].'\'',array(1=>'Error eliminando transación'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		gfpWal::putBal(array('qSet'=>'amount=amount-'.$Da['debBal'].'+'.$Da['creBal']),$Da);
		if(_err::$err){ return _err::$errText; }
		else{
			$js=_js::r('Movimiento eliminado correctamente.');
			a_sql::transaction(true);
		}
	}
	return $js;
}

static public function oneGet($P){
	$q=a_sql::fetch('SELECT A.*,IF(A.creBal>0,A.creBal,A.debBal) bal
	FROM gfp_omov A 
	JOIN gfp_owal W ON (W.wallId=A.wallId)
	WHERE A.mid=\''.$P['mid'].'\' LIMIT 1',[1=>'Error obtiendo información del credito',2=>'El credito no existe']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	return _js::enc2($q);
 }
static public function onePut($P){
 	$isBal=array_key_exists('bal',$P);
	$edit=(true); unset($P['edit']);
	if(_js::iseErr($P['mid'],'Id de transación debe estar definido','numeric>0')){}
	else if($isBal && _js::iseErr($P['bal'],'El valor debe ser mayor a 0.','numeric>0')){}
	else if($edit && _js::iseErr($P['docDate'],'Se debe definir la fecha del movimiento')){ }
 	else if($edit && _js::iseErr($P['categId'],'Se debe definir la categoria.')){}
	else{
		_ADMS::libc('gfp','wal');
		a_sql::transaction(); $cmt=false;
		$mid=$P['mid']; unset($P['mid']);
		$Da=a_sql::fetch('SELECT wallId,wbId,debBal,creBal FROM gfp_omov WHERE mid=\''.$mid.'\' LIMIT 1',array(1=>'Error obteniendo transación a modificar.',2=>'La transación no existe.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		$Di=array();
		if($isBal){
			if($Da['creBal']>0){ $bal=$Da['creBal']-$P['bal']; $P['creBal']=$P['bal']; }
			else if($Da['debBal']>0){ $bal=$P['bal']-$Da['debBal']; $P['debBal']=$P['bal'];}
			$Di['qSet']='amount=amount+'.$bal;
		}
		//die(print_r($P));
		unset($P['bal'],$P['wallId'],$P['mType'],$P['sysType'],$P['credCap'],$P['credInt'],$P['credOtros'],$P['credSal']);
		a_sql::oneMulti($P,array('tbk'=>'gfp_omov','qDo'=>'update','wh_change'=>'mid=\''.$mid.'\' LIMIT 1'));
		if(a_sql::$err){ return _js::e(3,a_sql::$errText); }
		gfpWal::putBal($Di,$Da);
		if(_err::$err){ return (_err::$errText); }
		else{
			$js=_js::r('Movimiento modificado correctamente.');
			a_sql::transaction(true);
		}
	}
 if(_err::$errText){ return _err::$errText; }
	return $js;
}
static public function oneFieldPut($P){//acctualizar campo cualquiera
 if(_js::iseErr($P['mid'],'Id de transación debe estar definido','numeric>0')){}
 else{
	 a_sql::transaction(); $cmt=false;
	 $mid=$P['mid']; unset($P['mid']);
	 $Da=a_sql::fetch('SELECT wallId,wbId,debBal,creBal FROM gfp_omov WHERE mid=\''.$mid.'\' LIMIT 1',array(1=>'Error obteniendo transación a modificar.',2=>'La transación no existe.'));
	 if(a_sql::$err){ return a_sql::$errNoText; }
	 //die(print_r($P));
	 unset($P['credBal'],$P['debBal'],$P['bal'],$P['wallId'],$P['mType']);
	 a_sql::oneMulti($P,array('tbk'=>'gfp_omov','qDo'=>'update','wh_change'=>'mid=\''.$mid.'\' LIMIT 1'));
	 if(a_sql::$err){ return _js::e(3,a_sql::$errText); }
	 if(_err::$err){ return (_err::$errText); }
	 else{
		 $js=_js::r('Movimiento modificado correctamente.');
		 a_sql::transaction(true);
	 }
 }
if(_err::$errText){ return _err::$errText; }
 return $js;
}

static public function quotesGet($P){
	 $M=[];
	 $M['L']=a_sql::fetchL('SELECT W.wallId,W.wallType,
	 IF(M.creBal>0,M.creBal,M.debBal) bal,M.docDate,M.credType,M.categId,M.credCuotas,M.lineMemo,M1.*
	 FROM gfp_omov M 
	 JOIN gfp_owal W ON (W.wallId=M.wallId)
	 LEFT JOIN gfp_mov1 M1 ON (M1.mid=M.mid)
	 WHERE M.mid=\''.$P['mid'].'\' ORDER BY M1.lineNum ASC LIMIT 240');
	 $M['Lc']=a_sql::fetchL('SELECT C1.pid k, concat(C1.openDate,\' a \',C1.closeDate) v
	 FROM gfp_ctc2 C2 
	 JOIN gfp_ctc1 C1 ON (C2.cId=C1.cId AND C1.openDate<=\''.date('Y-m-d').'\')
	 WHERE C2.wallId=\''.$M['L'][0]['wallId'].'\' ORDER BY C1.closeDate DESC LIMIT 24');
	 return _js::enc2($M);
 }
static public function quotesPut($P){
 if(_js::iseErr($P['mid'],'Id de transación debe estar definido','numeric>0')){}
 else if(is_array($P['L']) && count($P['L'])>240){ _err::err('No se pueden registrar más de 240 cuotas',3); }
 if(!_err::$err){
	 $qM=a_sql::fetch('SELECT M.creBal,M.docDate FROM gfp_omov M WHERE M.mid=\''.$P['mid'].'\' LIMIT 1',[1=>'Error obteniendo información de la transaccion',2=>'La transacción no existe']);
	 if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	 else{}
 }
 if(!_err::$err && is_array($P['L'])){
	$iD=['u','gfp_omov','credCap'=>0,'credInt'=>0,'credCuoNum'=>0,'credOtros'=>0,'_wh'=>'mid=\''.$P['mid'].'\' LIMIT 1'];
	$Lastt=[]; //ultima
	foreach($P['L'] as $n=>$L){
		if(_js::iseErr($L['lineNum'],'Debe definir el numero de la cuota','numeric>0')){ break; }
		else if(_js::iseErr($L['lineDate'],'Debe definir la fecha del pago')){ break; }
		else if(_js::iseErr($L['capital'],'Debe digitar el capital','numeric')){ break; }
		else if(_js::iseErr($L['interes'],'Debe digitar el interes','numeric')){ break; }
		else{
			$P['L'][$n][0]='i';
			$P['L'][$n]['_unik']='id';
			$P['L'][$n][1]='gfp_mov1';
			$P['L'][$n]['mid']=$P['mid'];
			$iD['credCap']+=$L['capital'];
			$P['L'][$n]['saldo']=$qM['creBal']-$iD['credCap'];
			$iD['credInt']+=$L['interes'];
			$iD['credOtros']+= ($L['otros'])?$L['otros']:0;
			if($iD['credCuoNum']<$L['lineNum']){
				$iD['credCuoNum']=$L['lineNum'];
			}
			$Lastt=$L;
		}
	}
	if(!_err::$err){
		$iD['credSal=']='creBal-'.$iD['credCap'];
		$P['L'][]=$iD;
		 a_sql::transaction(); $c=false;
		 a_sql::multiQuery($P['L']);
		 if(!_err::$err){
			 $js=_js::r('Información guardada correctamente'); $c=true;
		 }
		 a_sql::transaction($c);
	 }
 }
 _err::errdie();
 return $js;
}
}
?>
