<?php
class gfpCre{
  static public function get($P){
    $ordBy=($P['ordBy']=='docDateASC')?'M.docDate ASC,M.mid ASC':'M.docDate DESC,M.mid DESC';
     unset($P['ordBy']);
     $wh='W.wallType=\'CR\'';
     $jFie=''; $jLeft='';
     if($P['pid']>0){
       $jLeft='JOIN gfp_ctc1 C1 ON (M.docDate>=C1.openDate AND M.docDate<=C1.closeDate)';
       $wh .=' AND C1.pid=\''.$P['pid'].'\'';
     }
     unset($P['pid']);
     return a_sql::queryL('SELECT W.wallType,M.* 
     FROM gfp_omov M 
     JOIN gfp_owal W ON (W.wallId=M.wallId) '.$jLeft.'
     WHERE '.$wh.' '.a_sql_filtByT($P).' ORDER BY '.$ordBy.' '.a_sql::nextLimit(30));
   }
}
?>
