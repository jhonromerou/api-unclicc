<?php
class gfpWal{
static public function get($P){
 $today=date('Y-m-d');
 $wh='1';
 if($P['deuda1']>0){ $wh .=' AND A.amount <-'.$P['deuda1']; } // -2000
 if($P['deuda2']>0){ $wh .=' AND A.amount >-'.$P['deuda2']; } // -3000
 unset($P['deuda1'],$P['deuda2']);
 $L=a_sql::queryL('SELECT A.*,
 C1.closeDate,C1.payDate,C12.payDate payDateMin
 FROM gfp_owal A
 LEFT JOIN gfp_ctc2 C2 ON (C2.wallId=A.wallId)
 LEFT JOIN gfp_ctc1 C1 ON (C1.cId=C2.cId AND C1.openDate<=\''.$today.'\' AND C1.closeDate>=\''.$today.'\')
 LEFT JOIN gfp_ctc1 C12 ON (C12.cId=C2.cId AND C12.closeDate=DATE_ADD(C1.openDate,INTERVAL -1 DAY))
 WHERE '.$wh.' '.a_sql_filtByT($P).' ORDER BY A.wallCode ASC ',array('enc'=>'N'));
 if(a_sql::$err){ return a_sql::$errNoText; }
 else{
  $L['wL']=a_sql::queryL('SELECT B.wbId k,B.wbId,B.wallId,B.wbOrder,B.wbName,B.amount, \'\' lineMemo FROM gfp_obol B
 WHERE 1 ',array('enc'=>'N','L'=>'N'));
 }
 return _js::enc2($L);
}
static public function oneGet($P){
 $q=a_sql::fetch('SELECT A.*,C2.cId 
 FROM gfp_owal A LEFT JOIN gfp_ctc2 C2 ON (C2.wallId=A.wallId)
 WHERE A.wallId=\''.$P['wallId'].'\' LIMIT 1',[1=>'Error obtiendo información de la cuenta',2=>'La cuenta no existe']);
 if(a_sql::$err){ return a_sql::$errNoText; }
 return _js::enc2($q);
}
static public function onePut($P){
	$isTC=($P['wallType']=='TC');
	if(_js::iseErr($P['wallType'],'Se debe definir el tipo de cuenta')){}
	else if(_js::iseErr($P['wallName'],'Se debe definir el nombre de cuenta')){}
	else if(_js::iseErr($P['wallCode'],'Se debe definir el codigo de la cuenta')){}
	else if(_js::iseErr($P['dateOpen'],'Se debe definir la fecha de apertura')){}
	else if($isTC && _js::iseErr($P['creditLine'],'Debe definir el cupo.','numeric>0')){}
	else if($isTC && _js::iseErr($P['cId'],'Debe asignar un periodo de cortes.','numeric>0')){}
	else if($isTC && _js::iseErr($P['dueDate'],'Debe definir el vencimiento de la tarjeta.')){}
	if(!_err::$err){
		$qM=[]; $cId=$P['cId']; unset($P['cId']);
		a_sql::transaction(); $c=false;
		$P['prp']=json_encode($P['prp']);
		if($P['wallId']>0){
			$qv=a_sql::fetch('SELECT A.wallType,M.mid
			FROM gfp_owal A
			LEFT JOIN gfp_omov M ON (M.wallId=A.wallId)
			WHERE A.wallId=\''.$P['wallId'].'\' LIMIT 1',[1=>'Error verificando modificación de cuenta',2=>'La cuenta no existe']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if($qv['wallType']=='TC' && $P['wallType']!='TC' && $qv['mid']>0){ _err::err('No puede modificar una Tarjeta de crédito con movimientos registrados'); }
			if(!_err::$err){
				a_sql::qUpdate($P,['tbk'=>'gfp_owal','wh_change'=>'wallId=\''.$P['wallId'].'\' LIMIT 1']);
			}
		}
		else{
			$P['wallId']=a_sql::qInsert($P,['tbk'=>'gfp_owal']);
		}
	}
	if(!_err::$err){
		a_sql::uniRow(['cId'=>$cId,'wallId'=>$P['wallId']],['tbk'=>'gfp_ctc2','wh_change'=>'wallId=\''.$P['wallId'].'\' LIMIT 1']);
		if(!_err::$err){  $c=true;}
	}
	a_sql::transaction($c);
	_err::errDie();
	return _js::r('Información de cuenta guardada correctamente',['wallId'=>$P['wallId']]);
 }

static public function putBal($P=array(),$_J=array()){
	if($_J['wbId']>0){
		a_sql::query('UPDATE gfp_obol SET '.$P['qSet'].' WHERE wbId=\''.$_J['wbId'].'\' LIMIT 1',array(1=>'Error actualizando billetera: '));
		if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	}
	if($_J['wallId']>0){
		a_sql::query('UPDATE gfp_owal SET '.$P['qSet'].' WHERE wallId=\''.$_J['wallId'].'\' LIMIT 1',array(1=>'Error actualizando billetera: '));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	}
}
}
?>
