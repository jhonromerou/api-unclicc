<?php
fOCP::$reqFile='nomBase';
JRoute::get('nom/novTurn','get',[]);
JRoute::get('nom/novTurn/one','getOne',[]);
JRoute::post('nom/novTurn/open','open',[]);
JRoute::post('nom/novTurn','post',[]);
JRoute::put('nom/novTurn','put',[]);
JRoute::post('nom/novTurn/close','close',[]);
JRoute::put('nom/novTurn/cancel','cancel',[]);
JRoute::get('nom/novTurn/prueba',function(){
	_ADMS::lib('_2d');
	$R=[
	['openTurn'=>'2020-01-01 07:00','closeTurn'=>'2020-01-01 15:00',  'timeEnt'=>'2020-01-01 07:00','timeOut'=>'2020-01-01 17:30',  'dominical'=>'N','hextra'=>'N'],
	['openTurn'=>'2020-01-01 00:15','closeTurn'=>'2020-01-01 08:00',  'timeEnt'=>'2020-01-01 00:15','timeOut'=>'2020-01-01 11:45',  'dominical'=>'N','hextra'=>'N'],
	['openTurn'=>'2020-01-01 23:45','closeTurn'=>'2020-01-02 08:00',  'timeEnt'=>'2020-01-01 23:45','timeOut'=>'2020-01-02 11:45',  'dominical'=>'N','hextra'=>'N'],
	['openTurn'=>'2020-01-01 14:30','closeTurn'=>'2020-01-01 21:45',  'timeEnt'=>'2020-01-01 14:30','timeOut'=>'2020-01-01 22:30',  'dominical'=>'N','hextra'=>'N'],

	['openTurn'=>'2020-01-01 14:30','closeTurn'=>'2020-01-01 20:45',  'timeEnt'=>'2020-01-01 14:30','timeOut'=>'2020-01-01 21:30',  'dominical'=>'N','hextra'=>'N'],
	['openTurn'=>'2020-01-01 07:00','closeTurn'=>'2020-01-01 15:00',  'timeEnt'=>'2020-01-01 07:00','timeOut'=>'2020-01-01 17:30',  'dominical'=>'FH','hextra'=>'N'],
	
	['openTurn'=>'2020-01-01 14:00','closeTurn'=>'2020-01-01 23:00',  'timeEnt'=>'2020-01-01 14:00','timeOut'=>'2020-01-02 01:15',  'dominical'=>'N','hextra'=>'N']
	];
	$K=['hextra'=>'Extra?','dominical'=>'F?',
	'timeEnt'=>'Entrada','closeTurn'=>'Fin Turno','timeOut'=>'Salida',
	'hrsNum'=>'Hrs','hrsExtra'=>'Hrs Extra',
	'FL'=>'FL','RN'=>'RN','FLN'=>'FLN','EFN'=>'EFN',
	'EN'=>'EN','EFD'=>'EFD','ED'=>'ED',
	'openTurn'=>'Inicio Turno','hrsTurn'=>'Hrs turno','_blocks'=>'Lineas'
	,'txt'=>'Detalle'
	];
	header('content-type:text/html');
	echo '<style>
	table{ border-collapse:collapse; }
	td{ border:1px solid #000; padding:2px; }
	</style>
	<table>';
	$tr1='<tr>';
	$R1=['N','Y'];
	$R2=['N','HF','FH','FF'];
	$tds=count($R);
	foreach($K as $t){ $tr .='<td style="background-color:#CCC">'.$t.'</td>'; }
	$tr .= '</tr>';
	echo $tr;
	foreach($R as $n =>$L){
		// Replicar N-N,HF,FH,FF  Y-N,HF,FH,FF
		$mismoDay=(substr($L['timeEnt'],0,10)==substr($L['timeOut'],0,10));
		foreach($R1 as $O1){
			foreach($R2 as $O2){
				if($mismoDay && !($O2=='N' || $O2=='FH')){ continue; }
				$L['hextra']=$O1; $L['dominical']=$O2;
		$Di=nomNovTurn::HDCalc($L,['txt'=>$L['txt']]);
		echo '<tr>';
		foreach($K as $k=>$x){
			$txt=$Di[$k];
			if(preg_match('/2020\-/',$txt)){ $txt=substr($txt,-5); }
			echo '<td>'.$txt.'</td>';
		}
		echo '</tr>';
		}}
		echo $tr;
	}
	echo '</table>';
},[]);
JRoute::get('nom/novTurn/recalcular2',function(){
	_ADMS::lib('_2d');
	header('content-type:text/html');
	$tr1='<tr>';
	$R1=['N','Y'];
	$R2=['N','HF','FH','FF'];
	$openDate=$_GET['date'];
	$timeOut=$openDate.' 20:00:00';
	if($_GET['cerrar']){ $txt= '<h5>Cerrando con fecha '.$timeOut.'</h5>'; }
	$q=a_sql::query('SELECT * FROM nom_nov3 WHERE lineStatus=\'C\' AND openDate=\''.$openDate.'\' ');
	$K=['tid'=>'ID','hextra'=>'Extra?','dominical'=>'F?',
	'timeEnt'=>'Entrada','closeTurn'=>'Fin Turno','timeOut'=>'Salida',
	'hrsNum'=>'Hrs','hrsExtra'=>'Hrs Extra',
	'FL'=>'FL','RN'=>'RN','FLN'=>'FLN','EFN'=>'EFN',
	'EN'=>'EN','EFD'=>'EFD','ED'=>'ED',
	'openTurn'=>'Inicio Turno','hrsTurn'=>'Hrs turno','_blocks'=>'Lineas'
	,'txt'=>'Detalle'
	];
	echo '<h4>Cerrado del dia '.$openDate.'</h4>
	'.$txt.'
	<style>
	table{ border-collapse:collapse; }
	td{ border:1px solid #000; padding:2px; }
	</style>
	<table>';
	foreach($K as $t){ $tr .='<td style="background-color:#CCC">'.$t.'</td>'; }
	$tr .= '</tr>';
	echo $tr;
	while($L=$q->fetch_assoc()){
		// Replicar N-N,HF,FH,FF  Y-N,HF,FH,FF
		if($_GET['cerrar']){ $L['timeOut']=$timeOut; }
		$Di=nomNovTurn::HDCalc($L,$L);
		echo '<tr>';
		foreach($K as $k=>$x){
			$txt=$Di[$k];
			if(preg_match('/2020\-/',$txt)){ $txt=substr($txt,-8); }
			echo '<td>'.$txt.'</td>';
		}
		echo '</tr>';
	}
	echo '</table>';
},[]);

?>