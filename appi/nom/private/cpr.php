<?php
JRoute::get('nom/cpr',function($D){
	$hoy=date('Y-m-d');
	$agee='';
	if($D['ageMay']){
		//hoy - 28 años= 1992-09-29 <= 1992-01-01 nacio, 
		$tda=date('Y-m-d',strtotime($hoy.' -'.$D['ageMay'].'years'));
		$agee='P.birthDay<\''.$tda.'\'';
	}
	if($D['ageMen']){
		//hoy - 20 años= 2000-09-29 >= 2000-10-31 nacio, 
		$tda=date('Y-m-d',strtotime($hoy.' -'.$D['ageMen'].'years'));
		if($agee!=''){ $agee .= ' AND '; }
		$agee .='P.birthDay>\''.$tda.'\'';
	}
	unset($D['ageMay'],$D['ageMen']);
	$D['wh']=$agee;
	$D['from']='P.prsId,P.positionId,P.name,P.cellular,P.email,P.birthDay, C.cardName 
	FROM par_ocpr P 
	JOIN par_ocrd C ON (C.cardId=P.cardId AND C.cardType=\'E\')';
	return a_sql::rPaging($D);
},[]);
JRoute::get('nom/cpr/form',function($D){
	$q=a_sql::fetch('SELECT A.*, C.cardName 
	FROM par_ocpr A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId) WHERE A.prsId=\''.$D['prsId'].'\' LIMIT 1',array(1=>'Error obteniendo información de contacto: ',2=>'El contacto no existe'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{ $js=_js::enc2($q); }
	return $js;
},[]);
JRoute::put('nom/cpr',function($D){
	$prsId=$D['prsId']; unset($D['prsId']);
	$D['prsId']=$prsId;
	if($js=a_sql::$err){ $js=a_sql::$errNoText; }
	else if($js=_js::ise($D['name'],'Se debe definir el nombre del contacto.')){}
	else if($js=_js::ise($D['gender'],'Se debe definir el género.')){}
	else if($js=_js::ise($D['cardId'],'Se debe relacionar el empleado','numeric>0')){}
	else if($js=_js::textMax($D['address'],100,'Dirección: ')){}
	else if($js=_js::textMax($D['notes'],200,'Notas: ')){}
	else{
		unset($D['cardName']);
		$ins2=a_sql::uniRow($D,array('tbk'=>'par_ocpr','wh_change'=>'prsId=\''.$prsId.'\' LIMIT 1'));
		$prsId=($ins2['insertId'])?$ins2['insertId']:$prsId;
			if(a_sql::$err){ $js=_js::e(3,$ln.'Error definiendo persona de contacto: '.a_sql::$errText,$jsAdd); $errs++; }
		else{ $js=_js::r('Se guardó la información correctamente.','"prsId":"'.$prsId.'"'); }
	}
	return $js;
},[]);
?>