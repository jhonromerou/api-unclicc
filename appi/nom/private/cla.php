<?php
JRoute::get('nom/cla',function($D){
	$D['from']='A.*, C.cardName FROM nom_cla1 A LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)';
	return a_sql::rPaging($D);
},[]);
JRoute::get('nom/cla/form',function($D){
	$js=false;
	if(_js::iseErr($D['claId'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		return a_sql::rQuery('SELECT A.*, C.cardName FROM nom_cla1 A 
		LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
		WHERE A.claId=\''.$D['claId'].'\' LIMIT 1',['_r'=>1,1=>'Error obteniendo información del contrato',2=>'El contrato no existe']);
	}
	_err::errDie();
},[]);
JRoute::put('nom/cla',function($D){
	$js=false;
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else if(_js::iseErr($D['openDate'],'Se debe la fecha de apertura de proceso de selección.')){}
	//else if(_js::iseErr($D['closeDate'],'Se debe la fecha de ingreso del empleado.')){}
	else if($D['openDate']>$D['closeDate']){ _err::err('La fecha de inicio no puede ser mayor a la de retiro',3); }
	else{
		a_sql::transaction(); $cmt=false;
		$nuevo=(!$D['claId']);
		$crdUpd=''; $updCrd=false;
		{/* Verificar contratos abiertos */
		$qf=a_sql::fetch('SELECT claId,lineStatus,leaveDate FROM nom_cla1 WHERE cardId=\''.$D['cardId'].'\' AND lineStatus=\'O\' LIMIT 1',[1=>'Error validando contratos activos']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		if(!_err::$err && $nuevo){/* Nuevo solo si no hay activo */
			if(a_sql::$errNo==-1){
				_err::err('El empleado ya tiene un contrato vigente, modifiquelo o realice el proceso de retiro.',3);
			}
			else{ $updCrd=true; $crdUpd='active=\'Y\''; }
		}
		}
		/* verificar modifcacion si es el contrato actual, modificar salario y otros */
		if(!_err::$err && $D['claId']>0){
			if($qf['claId']==$D['claId']){ $updCrd=true;
				$crdUpd .=($crdUpd)?',salarioBal=\''.$D['salarioBal'].'\'':'salarioBal=\''.$D['salarioBal'].'\'';
			}
			$qf=a_sql::fetch('SELECT lineStatus,leaveDate FROM nom_cla1 WHERE claId=\''.$D['claId'].'\' LIMIT 1',[1=>'Error revisando contrato a modificar']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if($qf['lineStatus']=='C' && $D['closeDate']>$qf['leaveDate']){ _err::err('La fecha de inicio no puede ser mayor a la fecha del terminación registrada '.$qf['leaveDate'],3); }
		}
		/* verificar que las fechas no den error con otros contractos */
		if(!_err::$err){
			_ADMS::lib('_2d');
			$wf=_2d::rangFields(['closeDate',$D['closeDate'],'leaveDate',$D['closeDate']],2);
			$qf=a_sql::fetch('SELECT claId,closeDate,leaveDate FROM nom_cla1 WHERE cardId=\''.$D['cardId'].'\' AND lineStatus!=\'N\' AND ('.$wf.') LIMIT 1',[1=>'Error verificando fechas para contrato.']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1  && $qf['claId']!=$D['claId']){
				_err::err('No se puede realizar el procesos, las fechas generan inconsistencias con otro contrato con fecha de inicio '.$qf['closeDate'].' y termicación '.$qf['leaveDate'].'.',3);
			}
		}
		/* Definir información en contracto */
		if(!_err::$err){
			$u=a_sql::uniRow($D,['tbk'=>'nom_cla1','wh_change'=>'claId=\''.$D['claId'].'\' LIMIT 1']);
			if(a_sql::$err){  _err::err(a_sql::$errText,3); }
			else{
				$D['claId']=($u['insertId'])?$u['insertId']:$D['claId'];
				if($updCrd){/*Actualizar datos ficha empleado */
					$crdUpd ='claId=\''.$D['claId'].'\','.$crdUpd;
					a_sql::xUpd('nom_ocrd',$crdUpd,'cardId=\''.$D['cardId'].'\' LIMIT 1',[1=>'Error aactualizando ficha de empleado']);
				}
			}
		}
		if(!_err::$err){ $cmt=true;
			$js=_js::r('Información actualizada',$D);
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	else{ return $js; }
},[]);

/* retiro */
JRoute::get('nom/cla/leave/form',function($D){
	$js=false;
	if(_js::iseErr($D['claId'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		return a_sql::rQuery('SELECT A.*, C.cardName FROM nom_cla1 A 
		LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId) 
		WHERE A.claId=\''.$D['claId'].'\' LIMIT 1',['_r'=>1,1=>'Error obteniendo información del reclutamiento',2=>'El reclutamiento no existe']);
	}
	_err::errDie();
},[]);
JRoute::put('nom/cla/leave',function($D){
	$js=false;
	$retirando=($D['leaveDate']);
	if($retirando && _js::iseErr($D['leaveVol'],'Se debe definir si es o no voluntario')){}
	else if($retirando && _js::iseErr($D['leaveReason'],'Se debe definir un motivo de retiro','numeric>0')){}
	else if($retirando && _js::iseErr($D['reinStatus'],'Se debe definir tipo de posible reintegro')){}
	else{
		$D['lineStatus']='C'; $updTb=false;
		a_sql::transaction(); $cmt=false;
		{/* Verificar contrato y fechas validas */
		$qf=a_sql::fetch('SELECT cardId,lineStatus,closeDate FROM nom_cla1 WHERE claId=\''.$D['claId'].'\' LIMIT 1',[1=>'Error obtiendo contrato a modificar',2=>'El contrato no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if($retirando && $D['leaveDate']<$qf['closeDate']){ _err::err('La fecha de terminación no puede ser menor a la fecha del inicio registrada '.$qf['closeDate'],3); }
		}
		$cardId=$qf['cardId'];
		{/* Rango fechas validas */
		if(!_err::$err && $retirando){
			$updTb=($qf['lineStatus']=='O');
			_ADMS::lib('_2d');
			$wf=_2d::rangFields([$D['leaveDate'],'closeDate','leaveDate'],3);
			$qf=a_sql::fetch('SELECT claId,closeDate,leaveDate FROM nom_cla1 WHERE cardId=\''.$cardId.'\' AND lineStatus!=\'N\' AND ('.$wf.') LIMIT 1',[1=>'Error revisando datos de empleado.']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1  && $qf['claId']!=$D['claId']){
				_err::err('La terminación no se puede realizar porque genera inconsistencias con otro contrato que tiene fecha de inicio '.$qf['closeDate'].' y de terminación '.$qf['leaveDate'].'.',3);
			}
		}
		}
		/* cerrar contrato y inactivar empleado */
		if(!_err::$err){
			$u=a_sql::uniRow($D,['tbk'=>'nom_cla1','wh_change'=>'claId=\''.$D['claId'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				if($updTb){
				a_sql::xUpd('nom_ocrd','claId=0,active=\'N\',reinStatus=\''.$D['reinStatus'].'\'','cardId=\''.$cardId.'\' LIMIT 1',[1=>'Error realizando retiro del empleado']);
				}
			}
		}
		if(!_err::$err){ $cmt=true;
			$js=_js::r('Información actualizada',$D);
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	else{ return $js; }
},[]);

/* encueta retiro */
JRoute::get('nom/cla/leaveAns',function($D){
	$M=a_sql::fetch('SELECT F.fId,C.cardId,CL.claId, F.fCode,F.fName,F.version,F.descrip,A.docEntry,A.docDate,C.cardName,A.lineMemo
	FROM nom_cla1 CL
	JOIN nom_ocrd C ON (C.cardId=CL.cardId)
	JOIN nom_ocfr F ON (F.fId=\''.$D['fId'].'\')
	LEFT JOIN nom_ocfrr A ON (A.fId=F.fId AND A.docEntry=CL.leadocEntry)
	WHERE CL.claId=\''.$D['claId'].'\' AND F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT F1.*,B.aText 
	FROM nom_cfr1 F1 
	LEFT JOIN nom_cfr1r B ON (B.aid=F1.aid AND B.docEntry=\''.$M['docEntry'].'\')
	WHERE F1.fId=\''.$M['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	return _js::enc2($M);
},[]);
JRoute::post('nom/cla/leaveAns',function($D){
	$D['docDate']=date('Y-m-d');
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['claId'],'Se debe definir ID contrato','numeric>0')){ $js=_err::$errText; }
	else{
		$claId=$D['claId']; unset($D['claId']);
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_post($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
		if(!_err::$err){
			a_sql::query('UPDATE nom_cla1 SET leaveForm=\'Y\',leadocEntry=\''.$D['docEntry'].'\' WHERE claId=\''.$claId.'\' LIMIT 1',[1=>'Error actualizando retiro del empleado.']);
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Información actualizada correctamente.',$D);
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
},[]);
JRoute::put('nom/cla/leaveAns',function($D){
	$D['docDate']=date('Y-m-d');
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['claId'],'Se debe definir ID contrato','numeric>0')){ $js=_err::$errText; }
	else{
		$claId=$D['claId']; unset($D['claId']);
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_put($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
		if(!_err::$err){
			a_sql::query('UPDATE nom_cla1 SET leaveForm=\'Y\',leadocEntry=\''.$D['docEntry'].'\' WHERE claId=\''.$claId.'\' LIMIT 1',[1=>'Error actualizando retiro del empleado.']);
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Información actualizada correctamente.',$D);
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
},[]);
?>