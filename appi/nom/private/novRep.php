<?php
JRoute::get('nom/novRep/turn',function($D){
	$D['from']='N3.openDate,N3.timeEnt,N3.timeOut,C.workSede,C.workArea,C.workPosi,CC.cardName,CC.licTradNum
	FROM nom_nov3 N3
	LEFT JOIN nom_ocrd C ON (C.cardId=N3.cardId)
	LEFT JOIN par_ocrd CC ON (CC.cardId=C.cardId)
	';
	$D['N3.lineStatus(E_noIgual)']='N';
	return a_sql::Lrows($D);
},[]);
JRoute::get('nom/novRep/novHer',function($D){
	_ADMS::lib('_2d');
	$wh=_2d::rangFields(['N3.openDate',$D['date1'],'N3.openDate',$D['date2']]);
	unset($D['date1'],$D['date2']);
	$D['wh']=' ('.$wh.') ';
	$D['from']='C.cardId,N3.openDate,N2.lineDate,N2.lineDue,N2.quantity,N2.numFactor,N2.salarioBal,N2.lineType,N2.lineType2,C.workSede,C.workArea,C.workPosi,CC.cardName,CC.licTradNum,
	N3.timeEnt,N3.timeOut,N3.hrsTurn,N3.openTurn,N3.closeTurn
	FROM nom_nov3 N3
	LEFT JOIN nom_nov2 N2 ON (N3.openDate=N2.lineDate AND N3.cardId=N2.cardId AND N2.lineType=\'HER\')
	LEFT JOIN nom_ocrd C ON (C.cardId=N3.cardId)
	LEFT JOIN par_ocrd CC ON (CC.cardId=C.cardId)
	';
	$D['N3.timeOut(E_IsNN)']='Y';
	return a_sql::Lrows($D);
},[]);
JRoute::get('nom/novRep/nov',function($D){
	_ADMS::lib('_2d');
	$wh=_2d::rangFields(['N2.lineDate',$D['date1'],'N2.lineDue',$D['date2']]);
	unset($D['date1'],$D['date2']);
	$D['wh']='('.$wh.')';
	$D['from']='C.cardId,N2.lineDate,N2.lineDue,N2.quantity,N2.numFactor,N2.salarioBal,N2.lineType,N2.lineType2,C.workSede,C.workArea,C.workPosi,CC.cardName,CC.licTradNum,
	N3.timeEnt,N3.timeOut,N3.hrsTurn,N3.openTurn,N3.closeTurn
	FROM nom_nov2 N2
	LEFT JOIN nom_nov3 N3 ON (N3.openDate=N2.lineDate AND N3.cardId=N2.cardId)
	LEFT JOIN nom_ocrd C ON (C.cardId=N2.cardId)
	LEFT JOIN par_ocrd CC ON (CC.cardId=C.cardId)
	';
	$D['N2.lineStatus(E_noIgual)']='N';
	return a_sql::Lrows($D);
},[]);
?>