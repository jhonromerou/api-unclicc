<?php
/*
 nom2 usa lineDate
	nom3 usa openDate para los turnos
*/
class nomNovHer{
static public function get($D){
	$D['from']='C.cardId,C.cardName, N.lineDate, N3.timeEnt,N3.timeOut
	FROM nom_nov2 N
	LEFT JOIN nom_nov3 N3 ON (N3.cardId=N.cardId AND N3.openDate=N.lineDate)
	LEFT JOIN nom_ocrd C ON (C.cardId=N.cardId)';
	$D['N.lineType']='HER';
	$D['N.lineNum']=1;
	return a_sql::rPaging($D,false,[1=>'Error obteniendo registros de novedades',2=>'No se encontraron resultados']);
}

static public function getOne($D){
	$whB='N1.cardId=\''.$D['cardId'].'\' AND N1.lineDate=\''.$D['lineDate'].'\' AND N1.lineType=\'HER\'';
	$q=a_sql::query('SELECT C.cardId,C.cardName,N1.periodo, N1.lineDate, N1.lineType2,N1.quantity 
	FROM nom_nov2 N1
	LEFT JOIN nom_ocrd C ON (C.cardId=N1.cardId)
	WHERE '.$whB.' ',[1=>'Error obteniendo registros de horas para este dia',2=>'El empleado no tiene registro de horas para la fecha']);
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		$Mx=array('L'=>[]);
		while($L=$q->fetch_assoc()){
			$Mx['L'][$L['lineType2']]=$L;
		}
		return _js::enc2($Mx);
	}
	if(_err::$err){ return _err::$errText; }
}
static public function post($D){
	return self::put($D,'POST');
}
static public function put($D,$met='PUT'){
	$js=false;
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	//else if(_js::iseErr($D['periodo'],'Se debe definir un periodo','numeric>0')){}
	else if(_js::iseErr($D['lineDate'],'Se debe definir la fecha.')){}
	else{
		_ADMS::lib('_2d');
		_ADMS::libC('nom','nov,crd');
		a_sql::transaction(); $cmt=false;
		$xE=nomCrd::getData($D['cardId'],'C.salarioBal');
		if(!_err::$err){
			$whA='cardId=\''.$D['cardId'].'\' AND lineDate=\''.$D['lineDate'].'\' ';
			$whB=$whA.' AND lineType=\'HER\'';
			nomNov::verificarPer(['cardId'=>$D['cardId'],'lineDate'=>$D['lineDate'],'lineDue'=>$D['lineDate'],'wh'=>'lineType!=\'HER\'','aText'=>'No se puede registrar las horas']);
		}
		if(!_err::$err && $met=='POST'){
			$qE=a_sql::query('SELECT cardId FROM nom_nov2 WHERE '.$whB.' LIMIT 1',[1=>'Error revisando registros de horas']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1){ _err::err('El empleado ya tiene un registro de horas para esta fecha, se debe realizar una modificación',3); }
		}
		if(!_err::$err){
			$errs=0;
			$X_HE=array('RN'=>1.35,'FL'=>1.75,'FLN'=>2.1,
			'ED'=>1.25,'EN'=>1.75,'EFD'=>2,'EFN'=>2.5);
			$HER=0; /* totales */
			$qI=array('cardId'=>$D['cardId'],'lineDate'=>$D['lineDate'],'lineDue'=>$D['lineDate'],'lineType'=>'HER','periodo'=>$D['periodo']);
			$nl=1;
			foreach($X_HE as $k=>$fx){
				$qI['lineType2']=$k;
				$qI['numFactor']=$fx;
				$qI['salarioBal']=$xE['salarioBal'];
				$qI['quantity']=($D[$k])?$D[$k]:0;
				$HER+=$qI['quantity'];
				unset($qI['delete']);
				if($qI['quantity']=='' || $qI['quantity']<=0){
					$qI['delete']='Y';
				}
				else{ $qI['lineNum']=$nl; $nl++; }
				a_sql::uniRow($qI,
				['tbk'=>'nom_nov2','wh_change'=>$whB.' AND lineType2=\''.$k.'\' LIMIT 1']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText,3); $errs++; break; }
			}
			if($errs==0){
				nomNov::setPeriodo($qI,'HER');
				if(_err::$err){ $errs=1; }
			}
			if($errs==0){
				$js=_js::r('Novedades registradas');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}

static public function setPeriodo($P,$type=false){
	//HER,AUS,VAC,LIC,INC
	$D=['cardId'=>$P['cardId'],'periodo'=>$P['periodo']];
	$qD=a_sql::fetch('SELECT SUM(quantity) quantity FROM nom_nov2 WHERE cardId=\''.$D['cardId'].'\' AND periodo=\''.$D['periodo'].'\' AND lineType=\''.$type.'\' ',[1=>'Error calculando HER del periodo.']);
	if(a_sql::$err){ _err::err(a_sql::$errText); }
	else if(a_sql::$errNo==-1){ $D[$type]=$qD['quantity']; }
	if($type && !_err::$err){
		a_sql::uniRow($D,
		['tbk'=>'nom_nov1','wh_change'=>'cardId=\''.$D['cardId'].'\' AND periodo=\''.$D['periodo'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
	}
}
}
?>