<?php
class nomNovTurn{
static public function HDCalc($D,$R=array()){
	$R['_info']='';
	$R['openTurn']=$D['openTurn'];
	$R['closeTurn']=$D['closeTurn'];
	$hEnt=substr($D['timeEnt'],11,2)*1; //2020-01-03 22:30
	$hEntM=substr($D['timeEnt'],14,2)*1; //2020-01-03 22:30
	$hSal=substr($D['timeOut'],11,2)*1;
	$hSalM=substr($D['timeOut'],14,2)*1;
	$minEnt=($hEntM>0)?(60-$hEntM)/60:0; //10:30, dan -0.5 en hora
	$minSal=$hSalM/60;
	$hrMas= $minEnt+$minSal;
	$hNoc=21; $hNoc2=30; //entre 9pm y 6am
	$hSalNum=($hSal<=12)?$hSal+24:$hSal;// 4am es 28
	if($hEnt>$hNoc){ $hAlas12=24-$hEnt; }// 21:30, son 24-21=3 -0.5minEnt, 2.5h
	else{ $hAlas12=24-$hNoc; } // 22-24-(24-21)= -2-3=-7
	if($hAlas12<0){ $hAlas12=0; }
	$hAlTN=$hNoc-$hEnt; //3h de 6pm a 9pm
	if($hAlTN<0){ $hAlTN=0; }
	$diur=0; //8am=32, 2h son diurnas
	if($hSalNum>=$hNoc2){// 7:30, (31.5-30)=1.5h
		$diur=$hSalNum-$hNoc2+$minSal;
	}
	$hFinTC=6;
	//Turno
	$hrsTurn=_2d::getDiff($D['openTurn'],$D['closeTurn'],'h');
	$hFinTurn=$hEnt+$minEnt+$hrsTurn;
	$dx=_2d::getDiff($D['timeEnt'],$D['timeOut'],'h:i');
	$diffTurn=$R['hrsExtra']=$dx[1]-$hrsTurn;
	$R['hrsTurn']=$hrsTurn;
	$R['timeEnt']=$D['timeEnt'];
	$R['timeOut']=$D['timeOut'];
	$R['hextra']=$D['hextra'];
	$R['dominical']=$D['dominical'];
	$R['_info']='turno de '.$hEnt.'.'.$hEntM.' a '.$hFinTurn.'.'.$hEntM.' de '.$hrsTurn.'h, Salida: '.$hSalNum.'.'.$hSalM.', extras: '.$diffTurn.' >6am: '.$diur;
	$R['hrs']=$dx[0];
	$R['hrsNum']=$dx[1];
	if($hrsTurn>$R['hrsNum']){ $hrsTurn=$R['hrsNum']; }
	$ED=$EN=$EFN=$EFD=0;
	$RN=$FL=$FLN=0;
	{/* 2. Horas rango */
	$HO=0;
	$ND1=$ED1=$EN1=$RN1=0;
	$ND2=$ED2=$EN2=$RN2=0;
	$extraD1=($hNoc-$hFinTurn); //21-20, 1h ED1
	if($extraD1>0){//haciendo extras en D1 >9am
		//salida despues de las 6am...6.30, 7.10
		if($hSalNum>=30){ $EN2=$hFinTC; $ED2=$hSal-$hFinTC+$minSal; }
		//salida entre 00 y 5:59... 5.30, 5.45 29-24=5+0.5m
		else if($hSalNum>=24){ $EN2=$hSalNum-24+$minSal; }
		if($R['hrsExtra']>0){
			if($hSalNum>=$hNoc){
				$EN1=($hSalNum>24)?24-$hNoc:$hSalNum-$hNoc+$minSal;
				$ed1=$hNoc-$hFinTurn; //21-20
			}
			else{ $ed1=$R['hrsExtra']; }
			if($ed1>0){ $ED1=$ed1; }
		}
	}
	else if($hSalNum>=$hNoc){ //salgo despues de las 9pm, nada extra
		if($hEnt>=$hNoc) $RN1=24-$hEnt-$minEnt; //24 -22.5 = 1.5
		else $RN1=24-$hNoc; //25>21, 24-21=
		if($hSalNum>=30){//sali despues de las 7
			//echo '>6am ';
			$RN2=$hFinTC; $ED2=$hSal-$hFinTC+$minSal;
			$ex2=30-$hFinTurn;
			if($ex2>0){ $EN2=$ex2; $RN2 -=$EN2; }
		}//00 a 6am
		else if($hSalNum>=24){//entre las 00 y las 6am
			//echo '00 a 05.59 am //';
			//turno 5:30, salida, 5:45, extras=45-30 = 0.25h
			if($hSalNum>=$hFinTurn){
				$RN2=$hFinTurn-24; //29.5-24= 5.5
				if($RN2<0){ $RN2=0; }
				$ex2=$hSalNum+$minSal-24; //5.30-5.15= 0.25h
				if($ex2>0){ $EN2=$ex2-$RN2; }
				else{ $RN2 +=$minSal; }
			}
			else{
				$RN2=$hSalNum-24; //29-24=5,
				$ex2=$hSalNum-$hFinTurn;
				if($ex2>=0){ $EN2=$ex2+$minSal; $RN2 -=$ex2; }
			}
			
		} //00 a 5.30
		//$RN1=24-$hNoc;
		if($hFinTurn>=$hNoc){//calcular si termino luego de las 9
			if($hSalNum<24){
				$EN1=$hSalNum+$minSal-$hFinTurn; //salia a las 11 -9pm = 2
				$RN1=$hSalNum+$minSal-$hFinTurn; //salia a las 11 -9pm = 2
			}
			else if($hFinTurn<24){
				$EN1=24-$hFinTurn; //salia a las 11 -9pm = 2
				$RN1=$hFinTurn-$hNoc;
			}
		}
	}
	if($ED1<0){ $ED1=0; }
	if($EN1<0){ $EN1=0; }
	if($RN1<0){ $RN1=0; }
	if($RN2<0){ $RN2=0; }
	if($EN1<0){ $EN1=0; }
	if($EN2<0){ $EN2=0; }
	if($ED2<0){ $ED2=0; }
	$HO=$R['hrsNum']-$ED1-$EN1-$RN1-$RN2-$EN2-$ED2;
	//$R['txt'] .= '--> '." HO($HO), ED1($ED1), EN1($EN1), RN1($RN1), RN2($RN2), EN2($EN2), ED2($ED2)";
	}/* end 2.rango */
	{// calculo
	$extra=($D['hextra']=='Y');
	if($D['dominical']=='FH'){/* domingo, lunes normal */
		if($extra){
			if($HO>0 || $ED1>0){ $R['EFD']=$HO+$ED1; }
			if($ED2>0){ $R['ED']=$ED2; }
			if($RN1>0 || $EN1>0){ $R['EFN']=$RN1+$EN1; }
			if($EN2>0 || $RN2>0){ $R['EN']=$RN2+$EN2; }
		}else{
			if($HO>0){ $R['FL']=$HO; }
			if($ED1>0){ $R['EFD']=$ED1; }
			if($EN1>0){ $R['EFN']=$EN1; }
			if($RN1>0){ $R['FLN']=$RN1; }
			if($EN2>0){ $R['EN']=$EN2; }
			if($RN2>0){ $R['RN']=$RN2; }
			if($ED2>0){ $R['ED']=$ED2; }
		}
	}
	else if($D['dominical']=='HF'){/* sabado a domingo */
		if($extra){
			if($HO>0 || $ED1>0){ $R['ED']=$HO+$ED1; }
			if($ED2>0){ $R['EFD']=$ED2; }
			if($RN1>0 || $EN1>0){ $R['EN']=$RN1+$EN1; }
			if($EN2>0 || $RN2>0){ $R['EFN']=$RN2+$EN2; }
		}else{
			if($ED1>0){ $R['ED']=$ED1; }
			if($EN1>0){ $R['EN']=$EN1; }
			if($RN1>0){ $R['RN']=$RN1; }
			if($EN2>0){ $R['EFN']=$EN2; }
			if($RN2>0){ $R['FLN']=$RN2; }
			if($ED2>0){ $R['EFD']=$ED2; }
		}
	}
	else if($D['dominical']=='FF'){/* domingo y lunes festivo */
		if($extra){
			if($HO>0 || $ED1>0 || $ED2>0){ $R['EFD']=$HO+$ED1+$ED2; }
			if($RN1>0 || $RN2>0 || $EN2>0){ $R['EFN']=$RN1+$RN2+$EN2; }
		}else{
			if($HO>0){ $R['FL']=$HO; }
			if($ED1>0 || $ED2>0){ $R['EFD']=$ED1+$ED2; }
			if($EN1>0 || $EN2){ $R['EFN']=$EN1+$EN2; }
			if($RN1>0 || $RN2>0){ $R['FLN']=$RN1+$RN2; }
		}
	}
	else if($extra){
		if($HO>0 || $ED1>0 || $ED2>0){ $R['ED']=$HO+$ED1+$ED2; }
		if($EN1>0 || $EN2){ $R['EN']=$EN1+$EN2+$RN1+$RN2; }
	}
	else{
		if($ED1>0 || $ED2>0){ $R['ED']=$ED1+$ED2; }
		if($EN1>0 || $EN2){ $R['EN']=$EN1+$EN2; }
		if($RN1>0 || $RN2>0){ $R['RN']=$RN1+$RN2; }
	}
	}//end calculo
	//echo substr($R['timeEnt'],-5).' Fin '.substr($R['closeTurn'],-5).' a '.substr($R['timeOut'],-5)." HO($HO), ED1($ED1), EN1($EN1), RN1($RN1), RN2($RN2), EN2($EN2), ED2($ED2)<hr/>";
	unset($R['_info']);
	return $R;
}
static public function getBC($D,$P=array()){ //revisar, lo pase a nomBC.php
	if(_js::iseErr($D['lineCode'],'Se debe definir el código para buscar')){}
	else{
		$fie=($P['fie'])?','.$P['fie']:'';
		$wh=($P['wh'])?'AND '.$P['wh']:'';
		$whL=($P['whL'])?'AND '.$P['whL']:'';
		a_sql::transaction(); $cmt=false;
		$q=a_sql::fetch('SELECT C.cardId,C.cardName,N2.lineStatus,N2.openDate,N2.timeEnt'.$fie.'
		FROM nom_ocrd C
		LEFT JOIN nom_nov3 N2 ON (N2.cardId=C.cardId AND N2.lineStatus!=\'N\' '.$whL.')
		WHERE C.bCode=\''.$D['lineCode'].'\' '.$wh.' LIMIT 1',[1=>'Error consultando información de empleado',2=>'El código {'.$D['lineCode'].'} no fue encontrado']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		return $q;
	}
}
static public function getOne($D){
	$qa=a_sql::fetch('SELECT C.cardId,C.cardName,N3.openDate,N3.timeEnt,N3.timeOut,N3.openTurn,N3.closeTurn,N3.breakTime
	FROM nom_nov3 N3
	JOIN nom_ocrd C ON (C.cardId=N3.cardId)
	WHERE N3.tid=\''.$D['tid'].'\' LIMIT 1',array(1=>'Error obteniendo registro de turno.',2=>'El empleado no tiene registro de entrada.'));
	if(_err::$err){ return _err::$errNoText; }
	else{ return _js::enc2($qa); }
}
static public function get($D){
	$D['from']='N2.tid,N2.lineStatus,N2.tt,C.cardId,C.cardName,N2.timeEnt,N2.timeOut
	FROM nom_ocrd C
	JOIN nom_nov3 N2 ON (N2.cardId=C.cardId) ';
	return a_sql::rPaging($D);
}
static public function open($D){
	$js=false; $openDate=date('Y-m-d');
	if(_js::iseErr($D['openTurn'],'Se debe definir la hora inicial del turno. Formato hh:mm (24 horas)')){}
	else if(_js::iseErr($D['closeTurn'],'Se debe definir la hora final del turno. Formato hh:mm (24 horas)')){}
	else if(_js::iseErr($D['lineCode'],'Se debe definir el código para buscar')){}
	else{
		a_sql::transaction(); $cmt=false;
		$q=self::getBC($D,['whL'=>'N2.openDate=\''.$openDate.'\'']);
		if(_err::$err){ return _err::$errText; }
		else if($q['openDate']){
			_err::err(''.$q['cardName'].' ya tiene un registro de entrada para hoy: '.$q['timeEnt'],3);
		}
		else{
			$timeEnt=$openDate.' '.date('H:i');
			$Di=['cardId'=>$q['cardId'],'openDate'=>$openDate,'timeEnt'=>$timeEnt,'tt'=>'B','dominical'=>$D['dominical'],'hextra'=>$D['hextra']];
			$Di['openTurn']=$openDate.' '.$D['openTurn'];
			if($D['openTurn']>$D['closeTurn']){//sale al otro dia
				$Di['closeTurn']=date('Y-m-d',strtotime($openDate)+' 1days').' '.$D['closeTurn'];
			}else{ $Di['closeTurn']=$openDate.' '.$D['closeTurn']; }
			$tid=a_sql::qInsert($Di,
			['tbk'=>'nom_nov3']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{
				$q['tid']=$tid;
				$q['timeEnt']=$timeEnt;
				$js=_js::r('Turno abierto correctamente',$q);
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
static public function close($D){
	$js=false;
	a_sql::transaction(); $cmt=false;
	$q=self::getBC($D,['fie'=>'N2.cardId,N2.tid,N2.timeOut,N2.dominical,N2.hextra,N2.openDate,N2.openTurn,N2.closeTurn,N2.dominical,N2.hextra','whL'=>'N2.lineStatus=\'O\' ']);
	if(_err::$err){ return _err::$errText; }
	else if($q['lineStatus']==''){
		_err::err(''.$q['cardName'].' no tiene turno abierto.',3);
	}
	else{
		_ADMS::lib('_2d');
		$q['timeOut']=date('Y-m-d H:i');
		$Di=['lineStatus'=>'C','cardId'=>$q['cardId'],'openDate'=>$q['openDate']];
		$Di=self::HDCalc($q,$Di);
		//echo 'ALERTJSON'; print_r($Di); die();
		$x=a_sql::qUpdate($Di,
			['tbk'=>'nom_nov3','wh_change'=>'tid=\''.$q['tid'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		if(!_err::$err){
			self::setHRE($Di);
		}
		if(!_err::$err){
			$js=_js::r('Turno cerrado correctamente');
			$cmt=true;
		}
	}
	a_sql::transaction($cmt);
	if(_err::$err){ return _err::$errText; }
	return $js;
}

static public function put($D){
	if(_js::iseErr($D['cardId'],'Se debe definir ID del empleado','numeric>0')){}
	else if(_js::iseErr($D['openDate'],'Se debe definir fecha del turno')){}
	else if(_js::iseErr($D['timeEnt'],'Se debe definir fecha de entrada.')){}
	else if(_js::iseErr($D['timeOut'],'Se debe definir fecha de salida.')){}
	else if($D['timeEnt']>$D['timeOut']){ _err::err('La salida no puede ser menor a la entrada.',3); }
	else if($D['openDate']!=substr($D['timeEnt'],0,10)){ _err::err('No puede registrar una fecha de entrada diferente a la fecha de la novedad.',3); }
	else{
		a_sql::transaction(); $cmt=false;
		$qa=a_sql::fetch('SELECT tid FROM nom_nov3 WHERE cardId=\''.$D['cardId'].'\' AND openDate=\''.$D['openDate'].'\' LIMIT 1',array(1=>'Error obteniendo registro de turno.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			_ADMS::lib('_2d');
			$Di=array('cardId'=>$D['cardId'],'openDate'=>$D['openDate'],
			'openTurn'=>$D['openTurn'],'closeTurn'=>$D['closeTurn'],'breakTime'=>$D['breakTime'],
			'timeEnt'=>$D['timeEnt'],'timeOut'=>$D['timeOut']
			);
			$Di=self::HDCalc($D,$Di);
			if($Di['hrsNum']>20){ $errs=1;
				return _err::err('La jornada excede las 20 horas. Revise los datos.',3);
			}
			if($D['preview']=='Y'){
				$Di['preview']='Y';
				a_sql::transaction(true); return _js::enc2($Di);
			}
			//echo 'ALERTJSON'; print_r($Di); die();
			a_sql::uniRow($Di,
				['tbk'=>'nom_nov3','wh_change'=>'tid=\''.$qa['tid'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				$js=_js::r('Turno cerrado correctamente');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}

static public function cancel($D){
	$js=false;
	if(_js::iseErr($D['tid'],'Se debe definir ID del turno a anular')){}
	else{
		$Di=array('lineStatus'=>'N');
		$x=a_sql::qUpdate($Di,
			['tbk'=>'nom_nov3','wh_change'=>'tid=\''.$D['tid'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			$js=_js::r('Turno anulado correctamente');
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
	static public function setHRE($D=array()){
		if(!_err::$err){
			$errs=0;
			$X_HE=array('HEO'=>1.25,'HEN'=>1.75,'HED'=>2,'HEX'=>2.5,
			'REN'=>1.35,'RED'=>1.75,'RND'=>2.1
			);
			$HER=0; /* totales */
			$qI=array('cardId'=>$D['cardId'],'lineDate'=>$D['openDate'],'lineDue'=>$D['openDate'],'lineType'=>'HER','periodo'=>$D['periodo']);
			$nl=1;
			$whB='cardId=\''.$D['cardId'].'\' AND lineDate=\''.$D['openDate'].'\' AND lineType=\'HER\'';
			foreach($X_HE as $k=>$fx){
				$qI['lineType2']=$k;
				$qI['numFactor']=$fx;
				$qI['quantity']=($D[$k]);
				unset($qI['delete']);
				if(!$D[$k] || $qI['quantity']=='' || $qI['quantity']<=0){
					$qI['delete']='Y';
				}
				else{ $qI['lineNum']=$nl; $nl++; }
				a_sql::uniRow($qI,
				['tbk'=>'nom_nov2','wh_change'=>$whB.' AND lineType2=\''.$k.'\' LIMIT 1']);
				if(a_sql::$err){ _err::err(a_sql::$errText,3); $errs++; break; }
			}
		}
	}
}
?>