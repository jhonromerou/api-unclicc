<?php
JRoute::get('xGe/wpt',function($D){
	$D['from']='A.* FROM xge_owpt A ';
	return a_sql::rPaging($D);
},[]);
JRoute::get('xGe/wpt/one','getOne');
JRoute::delete('xGe/wpt/one','delOne');
JRoute::post('xGe/wpt','post');
JRoute::put('xGe/wpt','put');

JRoute::get('xGe/wpt/rep',function($D){
  _ADMS::lib('sql/filter');
  $vt=$D['viewType']; unset($D['viewType']);
  if($vt=='D'){
    $wh=a_sql_filter($D);
    $fie='A.docEntry,A.docDate,I.itemId,I.itemType,I.itemCode,I.itemName,A.itemSzId,I.udm,A.whsId,A.quantity,A.docTotal,A.docTotal2';
    $gb=$fie;
    $qury='SELECT '.$fie.'
    FROM wma_oddp A
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    WHERE A.canceled=\'N\' '.$wh.' ';
  }
  else{ $vt='G';
    $wh=a_sql_filter($D);
    $fie='A.*,C.cardName';
    $gb=$fie;
    $qury='SELECT '.$fie.'
    FROM xge_owpt A
    LEFT JOIN xge_wpt1 B ON (B.docEntry=A.docEntry)
    LEFT JOIN nom_ocrd C ON (C.cardId=B.cardId)
    WHERE 1 '.$wh.' ';
  }
  $wh=a_sql_filter($D);
  return a_sql::fetchL($qury,
  ['k'=>'L','D'=>['_view'=>$vt]],true);
});
?>