<?php
JRoute::put('xGe/b1Cae',function($D){
	/* Si xGePdr=N esta retirado */
	$js=false;
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del entrenamiento')){}
	else{ $n=1;
		$mQ=array();
		foreach($D['L'] as $k=>$L){
			$ln='Linea '.$n.': '; $n++;
			if(_js::iseErr($L['lineType'],$ln.'Se debe definir el tipo de entrenamiento')){ break; }
			else if(_js::iseErr($L['lineHrs'],$ln.'Se debe definir la cantidad de horas')){ break; }
			else{
				$L[0]='i'; $L[1]='xge_cae1';
				$L['_unik']='id';
				$L['docEntry']='x';
				$mQ[]=$L;
				$D['hrsTotal']+=$L['lineHrs'];
			}
		}
		if(!_err::$err){
			a_sql::transaction(); $cmt=false;
			unset($D['L']);
			$u=a_sql::uniRow($D,['tbk'=>'xge_ocae','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
			if(a_sql::$err){  _err::err(a_sql::$errText,3); }
			else{
				$D['docEntry']=($u['insertId'])?$u['insertId']:$D['docEntry'];
				a_sql::multiQuery($mQ,null,['docEntry'=>$D['docEntry']]);
			}
			if(!_err::$err){ $cmt=true;
				$js=_js::r('Información actualizada',$D);
			}
			a_sql::transaction($cmt);
		}
	}
	if(_err::$err){ return _err::$errText; }
	else{ return $js; }
},[]);
JRoute::get('xGe/b1Cae',function($D){
	return a_sql::toL(a_sql::qPaging(['from'=>'A.*, C.cardName FROM xge_ocae A LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)']));
},[]);
JRoute::get('xGe/b1Cae/form',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.*, C.cardName FROM xge_ocae A LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId) WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información del entrenamiento',2=>'El entrenamiento no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{
			$M['L']=array();
			$q=a_sql::query('SELECT * FROM xge_cae1 WHERE docEntry=\''.$D['docEntry'].'\' ORDER BY lineNum ASC',[1=>'Error obteniendo lineas del documento']);
			if(a_sql::$err){ return a_sql::$errNoText; }
			if(a_sql::$errNo==-1) while($L=$q->fetch_assoc()){
				$M['L'][]=$L;
			}
			return _js::enc2($M);
		}
	}
	if(_err::$err){ return _err::$errText; }
},[]);

?>