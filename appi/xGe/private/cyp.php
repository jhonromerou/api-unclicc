<?php
JRoute::get('xGe/cyp',function($D){
	$Px=fOCP::fg('owsu',['in'=>'C.workSede'],'nomBase');
	_ADMS::lib('iDoc');
	$D['fromA']='A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.userUpd,A.dateUpd,A.docDate,A.docStatus,A.canceled,A.docClass,A.lineMemo,C.cardName,C.licTradType,C.licTradNum 
	FROM xge_ocyp A
	JOIN nom_ocrd C ON (C.cardId=A.cardId)
	'.$Px['l'];
	return iDoc::get($D);
},[]);
JRoute::get('xGe/cyp/view',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.*,C.cardId,C.cardName,C.licTradType,C.licTradNum
		FROM xge_ocyp A 
		LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
		WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{ return _js::enc2($M); }
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::post('xGe/cyp',function($D){
	_ADMS::lib('docSeries');
	$js=false;
	if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else{
		a_sql::transaction(); $c=false;
		$D=docSeries::nextNum($D,$D);
		if(!_err::$err){
			$D['docEntry']=a_sql::qInsert($D,['tbk'=>'xge_ocyp','qk'=>'ud','qku'=>'ud']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{
				$c=true;
				$js=_js::r('Información guardada correctamente','"docEntry":"'.$D['docEntry'].'"');
			}
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
});
JRoute::put('xGe/cyp',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir Id para modificar','numeric>0')){}
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado')){}
	else{
		a_sql::qUpdate($D,['tbk'=>'xge_ocyp','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1','qku'=>'ud']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$js=_js::r('Información guardada correctamente','"docEntry":"'.$D['docEntry'].'"');
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});
JRoute::put('xGe/cyp/statusCancel',function($D){
	a_sql::transaction(); $cmt=false;
	_ADMS::lib('iDoc');
	iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>'xge_ocyp','docEntry'=>$D['docEntry'],'qku'=>'ud'));
	if(_err::$err){ return _err::$errText; }
	else{ $cmt=true; $js=_js::r('Documento anulado correctamente.'); }
		a_sql::transaction($cmt);
	return $js;
});
?>