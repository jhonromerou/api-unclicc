<?php
class xGeWpt extends JxDoc{
  static $serieSin='wmaWpt';
  static $AI='docEntry';
  static $serie='xwmaWpt';
  static $tbk='xge_owpt';
  static $tbk1='xge_wpt1';
  static public function getOne($D=array()){
    if(_js::iseErr($D['docEntry'],'Se debe definir ID para modificar','numeric>0')){}
    else{
      $M=a_sql::fetch('SELECT A.* FROM '.self::$tbk.' A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
      if(a_sql::$err){ return a_sql::$errNoText; }
      else{
        $M['L']=a_sql::fetchL('SELECT B.*,C.cardName
        FROM '.self::$tbk1.' B 
        JOIN nom_ocrd C ON (C.cardId=B.cardId)
        WHERE B.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'error obteniendo lineas documento',2=>'El documento no tiene lineas registradas']);
        return _js::enc2($M);
      }
    }
    if(_err::$err){ return _err::$errText; }
  }
  static public function delOne($D=array()){
    $js=false;
    if(_js::iseErr($D['docEntry'],'Se debe definir ID a eliminar','numeric>0')){}
    else{
      a_sql::transaction(); $c=false;
      $M=a_sql::fetch('SELECT docEntry FROM '.self::$tbk.' A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento #'.$D['docEntry'].'',2=>'El documento #'.$D['docEntry'].' no existe']);
      if(a_sql::$err){ _err::err(a_sql::$errNoText); }
      else{
        a_sql::query('DELETE FROM '.self::$tbk.' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error eliminando documento. ']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else{
          a_sql::query('DELETE FROM '.self::$tbk1.' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 500',[1=>'Error eliminando personal relacionado. ']);
          if(a_sql::$err){ _err::err(a_sql::$errNoText); }
          else{ $c=true; $js=_js::r('Documento eliminado correctamente'); }
        }
      }
      a_sql::transaction($c);
    }
    _err::errDie();
    return $js;
  }
  static public function fieldsRequire($D=array()){
		return [
			['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
			['k'=>'workSede','ty'=>'id','iMsg'=>'Sede'],
			['k'=>'area','ty'=>'id','iMsg'=>'Area'],
			['k'=>'labor','ty'=>'id','iMsg'=>'Labor'],
			['k'=>'encargado','ty'=>'id','iMsg'=>'Encargado'],
			['k'=>'qty','ty'=>'>0','iMsg'=>'Total'],
			['k'=>'marca','ty'=>'id','iMsg'=>'Marca'],
			['k'=>'presen','ty'=>'id','iMsg'=>'Presentacion'],
			['k'=>'lineMemo','maxLen'=>200],
			['ty'=>'L','k'=>'L','req'=>'N','iMsg'=>'Auxiliares','L'=>[
				['k'=>'cardId','ty'=>'id','iMsg'=>'ID Empleado']
			]
			]
		];
	}
	static public function post($D=array()){
    return self::save($D);
  }
	static public function put($D=array()){
    if(_js::iseErr($D['docEntry'],'Se debe definir ID del documento.','numeric>0')){}
    else{ return self::save($D); }
    _err::errDie();
  }
	static public function save($D=array()){
		self::formRequire($D);
		if(!_err::$err){
			a_sql::transaction(); $c=false;
			$docEntryN=self::nextID();
      if(!_err::$err){
        $L=$D['L']; unset($D['L']);
        if($D['docEntry']){
          a_sql::qUpdate($D,['tbk'=>self::$tbk,'qku'=>'ud','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
        }
        else{ $D['docEntry']=$docEntryN;
          a_sql::qInsert($D,['tbk'=>self::$tbk,'qk'=>'ud','qku'=>'ud']);
        }
        if(a_sql::$err){ _err::err(a_sql::$errText,3); }
        else{
          self::putAux($L,$D);
          if(!_err::$err){ $c=true;
            $js=_js::r('Información guardada correctamente','"docEntry":"'.$D['docEntry'].'"');
          }
        }
      }
      a_sql::transaction($c);
    }
    _err::errDie();
    return $js;
  }
  static public function putAux($L=[],$D=[]){
    if(is_array($L)) foreach($L as $n=>$L2){
      $L2['docEntry']=$D['docEntry'];
      a_sql::uniRow($L2,['tbk'=>self::$tbk1,'wh_change'=>'docEntry=\''.$L2['docEntry'].'\' AND cardId=\''.$L2['cardId'].'\' LIMIT 1']);
    }
  }

}
?>