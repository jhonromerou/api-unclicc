/*
lValidate:{
	minLen:, maxLen, 
}
*/
Api.JForm={pr:'/appi/private/jform/'};
$V.JFormLType=[{k:1,v:'Respuesta Corta'},{k:2,v:'Número'},{k:3,v:'Lista'},{k:4,v:'Fecha'},{k:5,v:'Parrafo'},
{k:6,v:'Rango'},//1-10, 10-1, 0-10-2 (cada 2)
{k:51,v:'Check'},
{k:50,v:'Si/No'},
{k:20,v:'Archivo',_active:'N'},
{k:7,v:'Variables Sistema'},//$Vs{k:'YN',v:'Si/No',o:'$V.YN'}
{k:91,v:'Tercero',_active:'N'}
];

$M.kauAssg('JForm',[
	{k:'JForm.forms',t:'Definir Plantillas Forms'},
	{k:'JForm.ans',t:'Registrar Respuestas Forms'},
]);

var JForm={ /* Forms Querys, omitir de batso*/
	a:function(g,k,P){
		return '#'+JForm.h(g,k,P,'a');
	},
	h:function(g,k,P,r){
		x='JForm'; var a=''; P=(P)?P:{};
		if(g=='forms'){//Listado form-template
			x +='.forms';
		}
		else if(g=='form'){// Edición de form-template
			x +='.form'; a=(P.fId)?'fId:'+P.fId:'';
		}
		else if(g=='docs'){ //Listado de Respuestas por docEntry
			x +='.docs'; a=(P.fId)?'fId:'+P.fId:'';
		}
		else if(g=='doc'){ //ver docEntry
			x +='.doc'; a=(P.docEntry)?'docEntry:'+P.docEntry:'';
		}
		else if(g=='ans'){ //Form nuevo para respuestas
			x +='.ans'; a=(P.fId)?'fId:'+P.fId:'';
			if(P.docEntry){ a +=(P.docEntry)?',docEntry:'+P.docEntry:''; }
		}
		else if(g=='qua'){ //Calificar Respuestas Manual
			x +='.qua'; a='fId:'+P.fId+',docEntry:'+P.docEntry;
		}
		else if(g=='rep'){// Edición de form-template
			a=(P.fId)?'fId:'+P.fId:'';
			x +='.rep';
		}
		else{// .get Listado de Documentos para responder
			
		}
		x=(k)?x+'.'+k:x; //JForm.form.xGen33rindu
		if(r=='a'){ return x+'!!{'+a+'}'; }
		if(r){ return x; }
		$M.to(x,a)
	},
	namer:function(k,ln){
		var name='';
		var ln=(ln)?ln:'L';
		if(ln=='key'){ return k; }
		else{ return ln+'['+k+']'; } 
	},
	lineTypeD:function(L){
		/*interpretar LineType*/
		var R={};
		if(L.lineType==3 && (typeof(L.lOpts)) == 'string'){
			R.opts=L.lOpts.split(',');
		}
		else if(L.lineType==50 || L.lineType==51){ R.opts=$V.YN; }
		else if(L.lineType==6){//rango de numeros
			var sep=L.lOpts.split('-');
			var n1=sep[0]*1;
			var n2=sep[1]*1;
			var n3=sep[2]*1;//sumar de a 2 o 3
			var nSum=1;
			if(n3>1){ nSum=n3; }
			R.opts=[];
			if(n1>n2){//10a1
				while(n1>=n2){ R.opts.push({k:n1,v:n1}); n1=n1-nSum; }
			}
			else{//1a10
				while(n1<=n2){ R.opts.push({k:n1,v:n1}); n1=n1+nSum; }
			}
		}
		return R;
	},
	repData:function(Jr,P){
		//Devuelve Tbf para TH,  L, y aL
		var R={Tbf:P.Tbf,aL:[],L:[]}
		var xA={};
		for(var i in Jr.aL){
			if(P.Tbf){ R.Tbf.push(Jr.aL[i].lineText); }
			xA[Jr.aL[i].aid]=Jr.aL[i];
			R.aL.push(Jr.aL[i]);
		}
		Rs={}; n=0;
		for(var i in Jr.L){ var L=Jr.L[i];
			ik=L.docEntry+'_'+L.fId;
			if(typeof(Rs[ik])=='undefined'){
				Rs[ik]=n;
				ikn=Rs[ik]; 
				R.L[ikn]=L; R.L[ikn].a={};
			}
			R.L[ikn].a[L.aid]={aText:gVal(L,xA)};
		}
		function gVal(L,xA){
			var tv=xA[L.aid];
			if(tv && tv.lineType==3){
				if(typeof(tv.lOpts) == 'string'){
					opts=tv.lOpts.split(',');
					return opts[L.aText];
				}
			}
			else{ return L.aText; }
		}
		return R;
	},
	
	/* obsoleto */
	reply_put:function(){ 
		var Pa=$M.read(); var cont=$M.Ht.cont;
		$ps_DB.get({f:'GET '+Api.Fqu.a+'batso.fre', inputs:'freId='+Pa.freId, loade:cont, func:function(Jr){
				if(Jr.errNo){ $Api.resp(cont,Jr); }
				else{
					JForm.Tpt.base(Jr,cont);
				}
			}
		});
	},
}

JForm.Tpt=function(P,cont,P2){
	var cont=(cont)?cont:$M.Ht.cont;
	var P2=(P2)?P2:{};
	var JrX=(P.Jr)?P.Jr:{};
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.fId){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//create form
		var jsF=$Api.JS.cls;
		cont=$1.t('div',{'class':'JFormTptForm'},cont);
		if(!JrX.L){ JrX.L=[]; }
		if(!JrX.version){ JrX.version=1; }
		if(!JrX.docType){ JrX.docType='F'; }
		if(P.docType){ JrX.docType=P.docType; }
		JrX.L=$js.sortBy('lineNum',JrX.L);
		{
			if(JrX.fId){ $Api.JS.addF({name:'fId',value:JrX.fId},cont); }
			$Api.JS.addF({name:'docType',value:JrX.docType},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Código',req:'Y',I:{lTag:'input',name:'fCode',value:JrX.fCode,'class':jsF}},cont);
			$1.T.divL({wxn:'wrapx4',L:'Nombre',req:'Y',I:{lTag:'input',name:'fName',value:JrX.fName,'class':jsF}},divL);
			$1.T.divL({wxn:'wrapx10',L:'Versión',req:'Y',I:{lTag:'number',inputmode:'numeric',min:1,name:'version',value:JrX.version,'class':jsF}},divL);
			if(P.grIds){
				$1.T.divL({wxn:'wrapx8',L:'Grupo',req:'Y',I:{lTag:'select',name:'grId',value:JrX.grId,'class':jsF,opts:P.grIds}},divL);
			}
			if(P.cardType){//=Y para mostrar
				opts=[{k:'N',v:'No'},{k:'C',v:'Cliente'},{k:'E',v:'Empleado'},{k:'S',v:'Proveedor'}];
				$1.T.divL({wxn:'wrapx8',L:'Tercero',req:'Y',I:{lTag:'select',name:'cardType',value:JrX.cardType,'class':jsF,opts:opts,noBlank:'Y'}},divL);
			}
			if(P.docClass){
				if(!P.docClass.I){ P.docClass.I={lTag:'select'}; }
				if(!P.docClass.I['class']){ P.docClass.I['class']=jsF; }
				else{ P.docClass.I['class'] +=' '+jsF; }
				P.docClass.I.name='docClass'; P.docClass.I.value=JrX.docClass;
				$1.T.divL(P.docClass,divL);
			}
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',req:'Y',I:{lTag:'textarea',name:'descrip',value:JrX.descrip,'class':jsF}},cont);
			/* Propiedades */
			if(P2.Prp){
				var n=1;
				for(var i in P2.Prp){ var Lp=P2.Prp[i];
					if(n==1){
						var prpL=$1.T.divL({divLine:1,wxn:'wrapx5',L:Lp.t,I:{lTag:'select',name:'prp'+n,value:JrX['prp'+n],'class':jsF,opts:Lp.opts}},cont);
					}
					else{
						$1.T.divL({wxn:'wrapx5',L:Lp.t,I:{lTag:'select',name:'prp'+n,value:JrX['prp'+n],'class':jsF,opts:Lp.opts}},prpL);
					}
					n++;
				}
			}
			if(JrX.docType=='CL'){
				var Tbs=['','Punto a Revisar','Tipo','','Detalles de revisión','Eliminar']; 
			}
			else{ var Tbs=['','Pregunta','Tipo','','Descripción','Eliminar']; }
			var tb=$1.T.table(Tbs,0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in JrX.L){ trA(JrX.L[i],tBody); }
			if(JrX.L.length<=0){ trA(L,tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',func:function(){ trA({fid:1},tBody); }},cont);
			$1.t('span',{textNode:' | '},cont);
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Sección',func:function(){ trASep({fid:1},tBody); }},cont);
		}
	}
	function trASep(L,tBody){
		var jsFL=$Api.JS.clsL;
		var jsFLN=$Api.JS.clsLN;
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('tr',{'class':jsFL,style:css1},tBody);
		var td=$1.t('td',{style:css1},tr);/* orden */
		$1.Move.btns({},td);
		var td=$1.t('td',{colspan:4,style:css1},tr);
		$1.t('input',{type:'text',name:'lineText',value:L.lineText,'class':jsFLN,AJs:{lineTop:'Y'},style:'width:100%; fontSize:105%; border:none; borderBottom:1px solid #000; font-weigth:bold; background:transparent;'},td);
		var td=$1.t('td',0,tr);
		$1.lineDel(L,{id:'aid'},td);
	}
	
	function trA(L,tBody){
		if(L.lineTop=='Y'){ return trASep(L,tBody); }
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		var jsFL=$Api.JS.clsL;
		var jsFLN=$Api.JS.clsLN;
		lTag.value=L.lineText;
		lTag.name='lineText'; lTag['class']=jsFLN;
		var AJs={};
		var tr=$1.t('tr',{'class':jsFL},tBody);
		var td=$1.t('td',0,tr);/* orden */
		$1.Move.btns({},td);
		var td=$1.t('td',0,tr);
		$1.lTag(lTag,td);
		var td=$1.t('td',0,tr);
		$1.T.sel({name:'lineType',selected:L.lineType,opts:$V.JFormLType,noBlank:'Y','class':'JFormOptsOpts '+jsFLN},td);
		var tsel=$1.q('.JFormOptsOpts',td);
		tsel.P=L;
		tsel.onchange=function(){ trAType(this); }
		var td=$1.t('td',0,tr);
		var span=$1.t('div',{style:'fontSize:13px;','class':'JFormOptsTxt'},td);
		$1.t('div',0,span);
		$1.t('input',{type:'text',name:'lOpts',value:L.lOpts,'class':'_opt1'},span);
		$1.T.sel({name:'lOpts',opts:$VsU,selected:L.lOpts,'class':'_opt2'},span);
		var td=$1.t('td',0,tr);
		$1.t('textarea',{name:'descrip',value:L.descrip,'class':jsFLN},td);
		var td=$1.t('td',0,tr);
		$1.lineDel(L,{id:'aid'},td);
		trAType(tsel);
	}
	function trAType(T){
		var jsFLN=$Api.JS.clsLN;
		var span=$1.q('.JFormOptsTxt',T.parentNode.parentNode);
		var sText=span.firstChild;
		var opt1=$1.q('._opt1',span);
		var opt2=$1.q('._opt2',span);
		opt1.classList.remove(jsFLN); opt1.style.display='none';
		opt2.classList.remove(jsFLN); opt2.style.display='none';
		var val=$1.qOpt(T);
		sText.innerHTML='';
		if(val==3){
			span.style.display='block'; sText.innerHTML='Separar por comas (,)';
			opt1.classList.add(jsFLN);
			opt1.style.display='block';
		}
		else if(val==6){
			span.style.display='block';
			$1.t('span',{_iHelp:'<b>1-10</b>: Entre 1 y 10\n<b>10-1</b>: Entre 10 y 1\n<b>0-12-3</b>:Entre 0 y 12 (0,3,6,9,12)\n<b>8-0-2</b>:Entre 8 y 0 (8,6,4,2,0)'},sText);
			$1.t('span',{textNode:'1-10 (Bajo-Alto)'},sText);
			opt1.classList.add(jsFLN);
			opt1.style.display='block';
		}
		else if(val==7){
			span.style.display='block';
			opt2.classList.add(jsFLN);
			opt2.style.display='block';
		}
		else{ span.style.display='none'; }
	}
}
JForm.Res=function(P,cont){
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.docEntry){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var fpath=(P.relpath)?P.relpath:'f'; // /dyd/f/archivo.pdf
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		$Api.JS.addF({name:'fId',value:JrX.fId},cont); 
		if(JrX.docEntry){ $Api.JS.addF({name:'docEntry',value:JrX.docEntry},cont); }
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){
					pare=$1.T.divL(F,cont);
				}
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		lineNum=1;
		for(var i in JrX.L){ L=JrX.L[i];
			if(L.lineTop=='Y'){ trASep(L,tBody); }
			else{ trA(L,tBody,lineNum); lineNum++; }
		}
	}
	function trASep(L,tBody){
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('div',{'class':'JFormRes-row',style:css1},tBody);
		$1.t('h5',{textNode:L.lineText},tr);
	}
	function trA(L,tBody,lineNum){
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		L.lineType *=1;
		switch(L.lineType){
			case 2: lTag.type='number'; lTag.inputmode='numeric'; break;
			case 3:{ lTag.tag='select'; }break;
			case 4: lTag.type='date'; break;
			case 5: lTag.tag='textarea'; break;
			case 6: lTag.tag='select'; break;
			case 7: lTag.tag='select'; //VsU
				lTag.opts=eval(L.lOpts);
			break;
			case 20: lTag.tag='attach'; 
				lTag.tt='fId'; lTag.tr=1; //solo necesito obtener fileId
				lTag.path=fpath; break;
			case 50: lTag.tag='select'; lTag.opts=$V.YN; break;
			case 51: lTag.tag='check';
				lTag.checked=(L.aText=='Y');
				lTag.t='Seleccione para SI';
			break;
			case 91: lTag.tag='apiBox';  break;
			default: lTag.type='text'; break;
		}
		if(L.lineType==3 && (typeof(L.lOpts)) == 'string'){
			lTag.opts=L.lOpts.split(',');
		}
		else if(L.lineType==6){//rango de numeros
			var sep=L.lOpts.split('-');
			var n1=sep[0]*1;
			var n2=sep[1]*1;
			var n3=sep[2]*1;//sumar de a 2 o 3
			var nSum=1;
			if(n3>1){ nSum=n3; }
			lTag.opts=[];
			if(n1>n2){//10a1
				while(n1>=n2){ lTag.opts.push({k:n1,v:n1}); n1=n1-nSum; }
			}
			else{//1a10
				while(n1<=n2){ lTag.opts.push({k:n1,v:n1}); n1=n1+nSum; }
			}
		}
		var jsFL=$Api.JS.clsL; var jsFLN=$Api.JS.clsLN;
		lTag.value=(L.aText)?L.aText:'';
		lTag.name='aText'; lTag['class']=jsFLN;
		lTag.AJs={aid:L.aid};
		if(L.id){ lTag.AJs['id']=L.id; }
		var tr=$1.t('div',{'class':jsFL+' JFormRes-row'},tBody);
		var td=$1.t('div',{'class':'JFormRes-rowText'},tr);/* orden */
		$1.t('b',{textNode:lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},tr); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},tr);
		var etiq=$1.lTag(lTag,td);
	}
}
JForm.Doc=function(P,cont){
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){ pare=$1.T.divL(F,cont); }
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		lineNum=1;
		for(var i in JrX.L){ L=JrX.L[i];
			if(L.lineTop=='Y'){ trASep(L,tBody); }
			else{ trA(L,tBody,lineNum); lineNum++; }
		}
	}
	function trASep(L,tBody){
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('div',{'class':'JFormRes-row',style:css1},tBody);
		$1.t('h5',{textNode:L.lineText},tr);
	}
	function trA(L,tBody,lineNum){
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		L.lineType *=1;
		var R=JForm.lineTypeD(L);
		var tr=$1.t('div',{'class':' JFormRes-row'},tBody);
		var td=$1.t('div',{'class':'JFormRes-rowText'},tr);/* orden */
		$1.t('b',{textNode:lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},tr); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},tr);
		if(R.opts){
			$1.t('div',{textNode:_g(L.aText,R.opts)},td);
		}
		else if(L.lineType==20){
			if(L.aText){
				$1.t('a',{href:L.aText,textNode:'Ver Archivo','class':'fa fa-paperclip',target:'_BLANK'},td);
			}
			else{
				$1.t('a',{textNode:'No Definido','class':'fa fa-paperclip'},td);
			}
		}
		else{ $1.t('div',{textNode:L.aText},td); }
	}
}

JForm.Qua=function(P,cont){
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.docEntry){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		$Api.JS.addF({name:'fId',value:JrX.fId},cont); 
		$Api.JS.addF({name:'docEntry',value:JrX.docEntry},cont);
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){
					pare=$1.T.divL(F,cont);
				}
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		for(var i in JrX.L){ trA(JrX.L[i],tBody); }
	}
	function trA(L,tBody){
		L=(L)?L:{};
		var val=L.aText;
		var R=JForm.lineTypeD(L);
		if(R.opts){ val=_g(L.aText,R.opts); }
		var jsFL=$Api.JS.clsL; var jsFLN=$Api.JS.clsLN;
		var tr=$1.t('div',{'class':jsFL+' JFormRes-row'},tBody);
		rQua=$1.t('div',{'class':'JFormRes-rowQua'},tr);
		var punt=$1.lTag({tag:'number','class':jsFLN,name:'aPoints',value:L.aPoints,AJs:{aid:L.aid}},rQua);
		rQua2=$1.t('div',{'class':'JFormRes-rowQua2'},tr);
		var td=$1.t('div',{'class':'JFormRes-rowText'},rQua2);/* orden */
		$1.t('b',{textNode:L.lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},rQua2); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},rQua2);
		$1.t('div',{textNode:val},td);
	}
}
JForm.Sign={
	win:function(P){
		var cont=$1.t('div');
		var vPost='pdocEntry='+P.pdocEntry;
		$Api.get({f:P.api,inputs:vPost,loade:cont,func:function(Jr){
			$1.T.barCode({jsF:$Api.JS.cls,func:function(val){
				$Api.put({f:P.api,inputs:'bCode='+val+'&'+vPost,func:P.func});
			}},cont);
			if(!P.func){
				P.func=function(Jr2){ trAdds(Jr2.L); }
			}
			var tb=$1.T.table(['','Participante','Fecha'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			trAdds(Jr.L);
			function trAdds(Lx){
				if(Lx){ for(var i in Lx){ L=Lx[i];
					var tr=$1.t('tr',0,tBody);
					$1.t('td',0,tr);
					$1.t('td',{textNode:L.lineText},tr);
					$1.t('td',{textNode:L.signDate},tr);
				}}
			}
		}});
		$1.Win.open(cont,{winTitle:'Asistencia',winSize:'medium'});
	}
}

JForm.Sea={
	one:function(D1,wrap,P2){
		var jsF=$Api.JS.cls;
		var P2=(P2)?P2:{};
		return $1.lTag({tag:'apiSeaBox',value:D1.fName,api:P2.api,fieDefAt:wrap,'class':jsF,D:D1,AJsPut:['fId'],func:P2.func});
	}
}

_Fi['JForm.forms']=function(wrap,x){
	$Doc.filter({func:function(){ JForm.i.forms(); } },
	[{wxn:'wrapx8',L:'Grupo',I:{tag:'select',name:'grId',opts:$JsV.JFormsGrs}},
	{wxn:'wrapx8', L:'Código Formato',I:{tag:'input',type:'text',name:'A.fCode(E_like3)'}},
	{wxn:'wrapx4',L:'Nombre Formato',I:{tag:'input',type:'text',name:'A.fName(E_like3)'}},
	],wrap);
};
_Fi['JForm.docs']=function(wrap,x){
	$Doc.filter({func:function(){ JForm.i.rDocs(); } },
	[{k:'d1'},{k:'d2'},	{wxn:'wrapx4',L:'Tercero',I:{tag:'input',type:'text',name:'C.cardName(E_like3)'}},
	{divLine:1,wxn:'wrapx8',L:'Grupo',I:{tag:'select',name:'grId',opts:$JsV.JFormsGrs}},
	{wxn:'wrapx8', L:'Código Formato',I:{tag:'input',type:'text',name:'A.fCode(E_like3)'}},
	{wxn:'wrapx4',L:'Nombre Formato',I:{tag:'input',type:'text',name:'A.fName(E_like3)'}},
	],wrap);
};

JForm.i={ //forms diversos (desempeño,etc)
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('form',null,T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	//plantillas
	forms:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.JForm.pr+'forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Clase','Versión'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				JForm.i.Lg(L,td);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:_g(L.grId,$JsV.JFormsGrs)},tr);
				$1.t('td',{textNode:L.version},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.JForm.pr+'forms/form',loadVerif:!Pa.fId,inputs:'fId='+Pa.fId,loade:cont,errWrap:cont, func:function(Jr){
			var xF=new JForm.Tpt({Jr:Jr,api:Api.JForm.pr+'forms/form',
			grIds:$JsV.JFormsGrs,cardType:'Y',
			func:function(Jr2){
			}},
			cont,{
				Prp__:[{t:'Publico Objetivo',opts:$V.xGeB1CdpPo}]
			});
			xF.send();
		}});
	},

	LgDocs:function(L,men){
		var Li=[];
		$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('ans',null,T.P) }},men);
		
		Li.push({k:'attach',ico:'fa fa-paperclip',textNode:' Archivos', P:L, func:function(T){ Attach.openWin({tt:'JForm',tr:T.P.docEntry, title:'Formulario # '+T.P.docEntry}) } });
		Li={Li:Li,PB:L};
		return $1.Menu.winLiRel(Li,men);
	},
	rNew:function(){
		cont=$1.t('div');
		var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Formulario',Inode:JForm.Sea.one({},cont,{api:Api.JForm.pr+'forms/sea',func:(L)=>{
			if(L.fId){ $M.to('JForm.ans','fId:'+L.fId); $1.delet(wP); }
		}})},cont);
		wP=$1.Win.open(cont,{winSize:'medium',winTitle:'Nuevo Registro'});
	},
	rDocs:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.JForm.pr+'forms/rdocs',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Código','Formato','Tercero','Realizado'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				cardName=(L.cardName)?L.cardName:'N/A';
				JForm.i.LgDocs(L,td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:cardName},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
		}});
	},
	//Responder Formulario
	pForm:function(Pa){
		var Pa=(Pa)?Pa:{};
		var cont=$1.t('div'); var jsF=$Api.JS.cls;
		$Api.get({f:Api.xGe.b+'b1Cdp/p/form',loadVerif:!Pa.pdocEntry,inputs:'pdocEntry='+Pa.pdocEntry,loade:cont,errWrap:cont, func:function(Jr){
			Jr.lineText=Jr.fName;
			if(Pa.pdocEntry){ $Api.JS.addF({name:'pdocEntry',value:Pa.pdocEntry},cont); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Formulario',Inode:JForm.Sea.one(Jr,cont,{api:Api.xGe.b+'b1Cdp/forms/sea'})},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha',I:{tag:'input',type:'date','class':jsF,name:'docDate',value:Jr.docDate}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsF,name:'prp1',opts:$Tb.owsu,selected:Jr.prp1}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Público',I:{tag:'select','class':jsF,name:'prp2',opts:$V.xGeB1CdpPo,selected:Jr.prp2}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Añadir Participante',Inode:Nom.Sea.crd({jsF:'N',clearInp:'Y',func:function(R){
				trA(R,rEmp);
			}})},cont);
			var rEmp=$1.t('div',0,$1.T.fieset({L:{textNode:'Participantes'}},cont));
			function trA(L,tBody){
				if(L.tr){ L.cardId=L.tr; }
				tag=$1.t('div',{textNode:L.cardName});
				var tp=$Api.JS.uniqAJs(tag,{tt:'card',tr:L.cardId},{L:'Y',uniq:L.cardId},tBody);
				$1.lineDel(L,null,tag);
			}
			if(Jr.L && !Jr.L.errNo){ for(var i in Jr.L){ trA(Jr.L[i],rEmp); } }
			var resp=$1.t('div',0,cont);
			Ps={POST:Api.xGe.b+'b1Cdp/p',jsBody:cont,loade:resp,func:function(Jr2){
				if(!Jr2.errNo){ $Api.resp(resp,Jr2); }
				else{ $Api.resp(cont,Jr2); }
			}};
			if(Pa.pdocEntry){ Ps.PUT=Ps.POST; delete(Ps.POST); }
			$Api.send(Ps,cont);
		}});
		$1.Win.open(cont,{winSize:'medium',winTitle:'Planificar Capacitación'});
	},
	ans:function(){
		$y.css([{h:'sapi',src:'jform/jform.css'}],()=>{
			var Pa=$M.read();
			var cont=$M.Ht.cont;
			var vPost='fId='+Pa.fId;
			if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
			$Api.get({f:Api.JForm.pr+'forms/ans',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
				$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
				tX=[
					{divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date',name:'docDate',value:Jr.docDate}}
				];
				if(Jr.cardType=='E'){
					tX.push({wxn:'wrapx4',L:'Empleado',Inode:Nom.Sea.crd(Jr)});
				}
				else if(Jr.cardType=='C'){
					tX.push({wxn:'wrapx4',L:'Tercero',Inode:$1.lTag({tag:'card',cardType:'C'},null,Jr)});
				}
				else if(Jr.cardType=='S'){
					tX.push({wxn:'wrapx4',L:'Tercero',Inode:$1.lTag({tag:'card',cardType:'S'},null,Jr)});
				}
				tX.push(
				{wxn:'wrapx8',L:'Tiempo (min)',I:{tag:'input',type:'number',name:'timeTotal',value:Jr.timeTotal}},
				{wxn:'wrapx8',L:'Calificación',I:{tag:'input',type:'number',name:'qualNum',value:Jr.qualNum}},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},);
				var xF= new JForm.Res({Jr:Jr,api:Api.JForm.pr+'forms/ans',func:function(Jr2){
				},
				F:tX},cont);
				xF.send();
			}});
		});
	},
	qua:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId;
		if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
		$Api.get({f:Api.xGe.b+'b1Cdp/qua',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var nd=Nom.Sea.crd(Jr,cont);
			var xF= new JForm.Qua({Jr:Jr,api:Api.xGe.b+'b1Cdp/qua',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'qualMemo',value:Jr.qualMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	rep:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Cdp/rep',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,func:function(Jr){
			R=JForm.repData(Jr,{Tbf:['Documento','Empleado','Fecha']});
			var tb=$1.T.table(R.Tbf);
			var tBody=$1.t('tbody',0,tb);
			for(var i in R.L){ L=R.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docEntry},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				for(var i in R.aL){
					val=(L.a[R.aL[i].aid])?L.a[R.aL[i].aid].aText:'';
					$1.t('td',{textNode:val},tr);
				}
				$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
			}
		}});
	},
}

$M.liAdd('JForm',[
{lineText:'JForm'},
{k:'JForm.forms',t:'Formularios - Plantillas', kau:'JForm.forms', ini:{f:'JForm.forms',btnGo:'JForm.form',gyp:JForm.i.forms} },
{k:'JForm.form',t:'Formulario - Plantilla', kau:'JForm.forms', ini:{g:JForm.i.form} },

{k:'JForm.docs',t:'Registros', kau:'JForm.forms',ini:{f:'JForms.docs',fNew:JForm.i.rNew,gyp:JForm.i.rDocs}},
{k:'JForm.ans',t:'Registro (form)', kau:'JForm.forms',ini:{g:JForm.i.ans}},

]);

$JsV._i({kObj:'JFormsGrs',mdl:'a1',liK:'JFormsGrs',liTxtG:'Grupo Formularios',liTxtF:'Grupo Formulario (Form)'});