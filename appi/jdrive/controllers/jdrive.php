<?php
class JDrive {
	static $limitFiles=30;
	static $maxDepth=4;
	static $qus1=0; //no volver a consultar
	static $qus2=0; //no volver a consultar
	static $FOLs=array(); //no volver a consultar
	static $FOL=array(); //obtener todas las carpetas

	static public function searchFiles($txt=''){
		/* 1. obtener 10 resultados
		2. con las carpetas obtenidas, obtener carpetas que el usuario pueda ver
		3. quita resultados que no cumplan con las carpetas
		*/
		$q=a_sql::query('SELECT B.folId,B.fileId,B.fileName,B.fileType,B.fileSizeText FROM
			gtd_ffc1 B
			WHERE B.fileName LIKE \'%'.$txt.'%\' LIMIT 10',
			[1=>'Error buscando archivos',2=>'No se encontraron resultados']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$R=[]; $fols=[];
			while($L=$q->fetch_assoc()){
				$R[]=$L;
				$fols[$L['folId']]=$L['folId'];
			}
			$x=self::userFolders(['wh'=>'AND A.folId IN ('.implode(',',$fols).') ']);
			foreach($R as $n=>$L){
				if($L['folId']==0){ continue; }
				else if(preg_match('/\''.$L['folId'].'\'/',$x['R'])){}
				else{ unset($R[$n]); }
			}
			$js=_js::enc2($R);
		}
		return $js;
	}
	//obtiene las carpetas que puede manipular el usuario
	static $uFolds=[];
	static public function userFolders($P=[]){
		$userId=($P['userId'])?$P['userId']:a_ses::$userId;
		$uFolders=[];
		self::$uFolds=['R'=>'','W'=>'','P'=>''];
		$q=a_sql::query('SELECT A.userId,A.privacity,A.folId,B.userId userM,B.perms FROM gtd_offc A
			LEFT JOIN gtd_ffc2 B ON (B.folId=A.folId AND B.userId=\''.$userId.'\')
		WHERE 1 '.$P['wh'].'
		AND (A.privacity=\'PU\'
		OR (
			(A.userId=\''.$userId.'\')
			OR (B.userId=\''.$userId.'\')
		))
		ORDER BY A.lv ASC',[1=>'Error obteniendo carpetas del usuario']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){
				if(self::$FOLs[$L['folId']]){ continue; }
				$perms='R';
				if($L['userId']==$userId || ($L['userM']==$userId AND $L['perms']=='P')){
					$perms='P';
				}
				else if($L['userM']==$userId){ $perms=$L['perms'];}
				else if($L['privacity']=='PU'){ $perms='R';}
				$txt='\''.$L['folId'].'\',';
				if($perms=='P'){
					self::$uFolds['R'] .=$txt;
					self::$uFolds['W'].=$txt;
					self::$uFolds['P'].=$txt;
				}
				else if($perms=='W'){
					self::$uFolds['R'].=$txt;
					self::$uFolds['W'].=$txt;
				}
				else if($perms=='R'){
					self::$uFolds['R'].=$txt;
				}
				self::getChild($L['folId'],['uFolds'=>$perms,'fie'=>'folId']);
			}
		}
		_err::errDie();
		$M=self::$uFolds; self::$uFolds=[];
		return $M;
	}
	static $permsLeft=['f'=>'','l'=>'','wh'=>''];
	static public function permsLeft($P=[]){
		self::$permsLeft['f']='A.permsDefined,A.privacity,B2.perms,A.lv,A.folId,A.fatherId,A.userId,A.dateC,A.sysCode,A.folName,A.folIco,A.folColor,A.folGrCode';
		self::$permsLeft['l']='LEFT JOIN gtd_ffc2 B2 ON (B2.folId=A.folId AND B2.userId=\''.a_ses::$userId.'\') ';
		self::$permsLeft['wh']='(
			A.privacity=\'PU\' 
			OR (A.privacity=\'PR\' AND B2.folId=A.folId AND B2.userId=\''.a_ses::$userId.'\')
		)';
	}
	static $num=-1;
	static public function folderList($P=array()){
		self::permsLeft();
		$err1=($P['err1'])?$P['err1']:'Error obteniendo listado de carpetas: ';
		$err2=($P['err2'])?$P['err2']:'No se encontraron carpetas.';
		$wh=($P['wh'])?'AND '.$P['wh']:'';
		$q=a_sql::query('SELECT '.self::$permsLeft['f'].'
			FROM gtd_offc A 
			'.self::$permsLeft['l'].'
		WHERE '.self::$permsLeft['wh'].'
		'.$wh.' ORDER BY A.lv ASC',array(1=>$err1,2=>$err2));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			while($L=$q->fetch_assoc()){
				//evitar leer las que tengo permisos y son hijas ya obtenidas
				if(self::$FOLs[$L['folId']]){ continue; }
				if($L['lv']==0){ $L['permsDefined']='Y'; }
				self::$FOL[]=$L;
				self::getChild($L['folId'],$P);
				if(_err::$err){ break; }
			}
			if(!_err::$err){
				$js=_js::enc2(['L'=>self::$FOL]);
			}
		}
		self::$FOL=array();
		_err::errDie();
		return $js;
	}
	
	static public function getChild($fatherId=-1,$P=array(),$conteo=false){
		if($fatherId<=0){ self::$num=-1; return true; }
		if($conteo){ self::$num++; }
		if(self::$num>self::$maxDepth){ _err::err('Depth de carpetas excedido (to:'.$fatherId.'): '.self::$maxDepth.'.',3); }
		else if(self::$FOLs[$fatherId]){ return true;}
		else{
			self::$FOLs[$fatherId]=1;
			$fie=($P['fie'])?','.$P['fie']:'';
			$wh=($P['wh'])?'AND '.$P['wh']:'';
			$q=a_sql::query('SELECT '.self::$permsLeft['f'] .$fie.'
				FROM gtd_offc A '.self::$permsLeft['l'].'
			WHERE A.fatherId=\''.$fatherId.'\''.$wh,array(1=>'Error obteniendo listado de carpetas hijas: '));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			else if(a_sql::$errNo==2){ self::$num=-1; }
			else if(a_sql::$errNo==-1){
				while($L=$q->fetch_assoc()){
					//si permisos definido y usuario no asignado, no continuo
					if($L['permsDefined']=='Y' && $P['perms']==null){
						continue;
					}
					self::$FOL[]=$L;
					self::getChild($L['folId'],$P,true);
				}
			}
		}
	}
	/* folder */
	static $fatherDepth=0;
	static public function isPerm($pReq='P',$D=[]){
		if($pReq=='R' && ($D['privacity']=='PU' || $D['perms']=='P' || $D['perms']=='W' || $D['perms']=='R')){ return true; }
		if($pReq=='W' && ($D['perms']=='W' || $D['perms']=='P')){ return true; }
		if($pReq=='P' && ($D['perms']=='P')){ return true; }
		return false;
	}
	static public function getFolder($P=array()){
		self::permsLeft();
		self::$fatherDepth=0;
		if($P['getFolder']>0){
			$P['folId']=$P['getFolder']; //carpeta a consultar, es misma, padre, usada en creacion
		}
		$wh =($P['wh'])?' AND '.$P['wh']:'';
		if(!$P['WH'] && $js=_js::ise($P['folId'],'Se debe definir el ID de la carpeta')){ _err::err($js); return false; }
		$fie=($P['fie'])?',A.folDesc,'.$P['fie']:',A.folDesc';
		$M=a_sql::fetch('SELECT '.self::$permsLeft['f'] .$fie.'
		FROM gtd_offc A '.self::$permsLeft['l'].'
		WHERE A.folId=\''.$P['folId'].'\' '.$wh.' LIMIT 1',array(1=>'Error obteniendo información de carpeta',2=>'La carpeta no existe o no tiene permisos suficientes.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
		$P['fatherId']=$M['fatherId'];
		if($P['getFolder']>0){
			$P['fatherId']=$P['getFolder']; 
		}
		$P['_permsDefined']=$M['permsDefined'];
		$P['folName']='<'.$M['folName'].'>';
		if($P['permsReq']!='N' && !$M=self::folderPerms($M,$P)){
			return false;
		}
		if($P['_users']){
			$q=a_sql::query('SELECT id,folId,userId,perms FROM gtd_ffc2 WHERE folId=\''.$P['folId'].'\' LIMIT 50',[1=>'Error obteniendo usuarios']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==2){ $M['U']=[]; }
			else{
				while($L=$q->fetch_assoc()){ $M['U'][]=$L; }
			}
		}
		return $M;
	}

	static public function folderPerms($M=array(),$P=array()){
		/* al crear carpeta, y sin fatherId, no debe revisar */
		if($P['creating']=='Y' && $P['fatherId']==0){ return $M; }
		$_permsVerify=($P['_permsVerify']=='N');
		if(self::$fatherDepth>6){ _err::err(_js::e(3,'fatherDepth maximum exec.')); return false; }
		self::$fatherDepth++;
		$M['permsTop']=$M['perms'];
		$M['parents'] .= $P['fatherId'].' -> ';
		if(!$M['fTree']){ $M['fTree']=[]; }
		$Mpa=$M;
		/* Revisar permisos padre, si carpeta no tiene definidos explictamente */
		if($P['fatherId']>0 && $P['_permsDefined']=='N'){
			$M['_topParentId']=$P['fatherId'];
			$fie=($P['fie'])
			?$P['fie'].',A.fatherId,A.lv,A.folId,A.folName,A.userId,A.privacity,B2.perms'
			:'A.*,B2.perms';
			$Mpa=a_sql::fetch('SELECT A.permsDefined,A.privacity,B2.perms,A.lv,A.fatherId,A.folId,A.folName
			FROM gtd_offc A 
			LEFT JOIN gtd_ffc2 B2 ON (B2.folId=A.folId AND B2.userId=\''.a_ses::$userId.'\')
			WHERE A.folId=\''.$P['fatherId'].'\' LIMIT 1
			',array(1=>'Error obteniendo información de permisos de carpeta :',2=>'La carpeta padre no existe para revisar los permisos.'));
			if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
			else{
				/* se debe definir para poder revisar permisos de superarioes */
				$P['fatherId']=$Mpa['fatherId'];
				$P['lastFolId']=$Mpa['folId'];
				$P['_permsDefined']=$Mpa['permsDefined'];
				$M['privacity']=$Mpa['privacity'];
				$M['perms']=$Mpa['perms']; //permisos igual a padre
				$M['permsTop']=$Mpa['perms'];
				$P['folName'] = 'padre <'.$Mpa['folName'].'>';
				if(!$_permsVerify){
					$M['fTree'][]=['permsDefined'=>$Mpa['permsDefined'],'perms'=>$Mpa['perms'],'lv'=>$Mpa['lv'],'folId'=>$Mpa['folId'],'folName'=>$Mpa['folName']];
				}
				//si el padre no tiene permisos especificados individualmente, llamo al padre
				if($Mpa['fatherId']>0 && $Mpa['permsDefined']=='N'){
					return self::folderPerms($M,$P);
				}
			}
		}
		//verificar los permisos de carpeta o padre actual
		{
			$folId=($P['lastFolId'])?$P['lastFolId']:$M['folId'];
			$M['folderPerms']='Y';
			/* no verificar permisos, solo obtenerlos */
			if($_permsVerify){ return $M; }
			$permsReq=$P['permsReq'];
			$R=array('R'=>'Lectura','W'=>'Modificacion','P'=>'Administracion');
			$permsAct=$R[$M['permsTop']]; $permsAct=($permsAct)?$permsAct:'Ninguno ('.$M['permsTop'].')';
			$txt=($P['permsText'])?$P['permsText']:'La carpeta '.$P['folName'].' requiere permiso de '.$R[$permsReq].' para realizar la acción. Permisos actuales: '.$permsAct.'. folId: '.$folId.'\n'; $errs=0;
			if(!self::isPerm($permsReq,['privacity'=>$M['privacity'],'perms'=>$M['permsTop']])){
				$errs++;
			}
			if(!array_key_exists($permsReq,$R)){ $errs++; }
			if($errs>0){ _err::err(_js::e(3,$txt)); return false; }
		}
		return $M;
	}
	static public function getFiles($P=[]){
		_ADMS::lib('sql/filter');
		$folId=$P['folId']; unset($P['folId']);
		$wh='A.folId=\''.$folId.'\' '; 
		//$wh .= a_sql_filter($P);
		$P['wh']=$wh; $P['__limit']=self::$limitFiles;
		$P['from']='B1.*
			FROM gtd_ffc1 B1
			JOIN gtd_offc A ON (A.folId=B1.folId)';
		$M=a_sql::rPaging($P, true,array(1=>'Error obteniendo archivos de carpeta: ',2=>'N'));
		$err2=false;
		a_sql::$AordBy='A'; a_sql::$setRows='N';
		if(a_sql::$errNo==2){ $err2=true; }
		if(!_err::$err){
			$q=a_sql::query('SELECT A.folId,A.folName,A.folColor,A.folIco,A.dateC,A.userId FROM gtd_offc A WHERE A.fatherId=\''.$folId.'\' LIMIT 100',[1=>'Error obteniendo carpetas internas']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==2){ if($err2==false){ a_sql::$errNo=-1; }   }
			else if(a_sql::$errNo==-1){
				if($err2==true){ a_sql::$errNo=2; }
				while($L=$q->fetch_assoc()){
					$L['isFile']='N';
					array_unshift($M,$L);
				}
			}
		}
		return $M;
	}
}
?>