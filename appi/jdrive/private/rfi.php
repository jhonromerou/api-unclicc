<?php
class gtdRfi{
static public function get($P=array()){
	$tt=$P['tt']; $tr=$P['tr']; unset($P['tt'],$P['tr']);
	if($js=_js::ise($tt,'Se debe definir tt para obtener archivos requeridos.')){ _err::err($js); }
	else if($js=_js::ise($tr,'Se debe definir tt para obtener archivos requeridos.','numeric>0')){ _err::err($js); }
	else{
		$js=a_sql::queryL('SELECT R1.id,F1.*,R.rfiId,R.rfiName,R.isOpt
		FROM gtd_orfi R
		LEFT JOIN gtd_rfi1 R1 ON (R1.rfiId=R.rfiId AND R1.tr=\''.$tr.'\')
		LEFT JOIN gtd_ffc1 F1 ON (F1.fileId=R1.fileId)
		WHERE R.tt=\''.$tt.'\' LIMIT 500',array(1=>'Error obteniendo archivos requeridos: ',2=>'No hay archivos requeridos relacionados.'));
	}
	return $js;
}
static public function post($P=array()){
	$ori=' on[gtdRfi::post]';
	$tt=$P['tt']; $tr=$P['tr'];
	if($js=_js::ise($P['rfiId'],'Se debe definir rfiId para asignar.','numeric>0')){ _err::err($js); }
	else if($js=_js::ise($P['tt'],'Se debe definir tt para obtener archivos requeridos.')){ _err::err($js); }
	else if($js=_js::ise($P['tr'],'Se debe definir tt para obtener archivos requeridos.','numeric>0')){ _err::err($js); }
	else{
		a_sql::transaction(); $cmt=false;
		$Re=a_sql::fetch('SELECT R.rfiName,IF(R1.id,R1.id,0) id
		FROM gtd_orfi R
		LEFT JOIN gtd_rfi1 R1 ON (R1.rfiId=R.rfiId)
		WHERE R1.rfiId=\''.$P['rfiId'].'\' AND R1.tt=\''.$P['tt'].'\' AND R1.tr=\''.$P['tr'].'\' LIMIT 1',array(1=>'Error revisando definicion de archivo.'.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$qDo='insert';
			if(a_sql::$errNo==-1){ $qDo='update'; }
			$iF=Attach::post($_FILES['file'],array('Ln'=>0,'revStor'=>'Y'));
			if(_err::$err){ die(_err::$errText); }
			$iF['tt']=$P['tt']; $iF['tr']=$P['tr']; unset($iF['fileUri']);
			$P['fileId']=a_sql::qInsert($iF,array('tbk'=>'gtd_ffc1','qk'=>'ud'));
			if(a_sql::$err){
				Attach::delLast();
				die(_js::e(3,'Error guardando archivo: '.a_sql::$errText));
			}
			foreach($P as $k=>$v){ $iF[$k]=$v; }
			$ins=a_sql::insert($P,array('table'=>'gtd_rfi1','qDo'=>$qDo,'wh_change'=>'WHERE id=\''.$Re['id'].'\' LIMIT 1'));
			$iF['id']=($ins['insertId'])?$ins['insertId']:$Re['id'];
			$iF['rfiName']=$Re['rfiName'];
			if($ins['err']){ die(_js::e(3,'Error relacionando archivo requerido: '.$ins['text'])); }
			a_sql::transaction(true);
			$js=_js::r('Se relaciona correctamente el archivo.',$iF);
		}
	}
	return $js;
}
static public function del($P=array()){
	$ori=' on[gtdRfi::del]';
	if($js=_js::ise($P['id'],'Se debe definir Id de relación.','numeric>0')){ _err::err($js); }
	else{
		$qA=a_sql::fetch('SELECT F.fileId,F.file,fileSize FROM gtd_ffc1 F
		JOIN gtd_rfi1 B ON (B.fileId=F.fileId)
		WHERE B.id=\''.$P['id'].'\' LIMIT 1',array(1=>'Error obteniendo información del archivo a eliminar.'.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==2){ $js=_js::r('Proceso realizado anteriormente.'); }
		else{
			Attach::del(array($qA));
			if(Attach::$err){ die(Attach::$errText); }
			a_sql::transaction(); $cmt=false;
			a_sql::query('DELETE FROM gtd_ffc1 WHERE fileId=\''.$qA['fileId'].'\' LIMIT 1',array(1=>'Error eliminando relación de archivo.'.$ori));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			a_sql::query('DELETE FROM gtd_rfi1 WHERE id=\''.$P['id'].'\' LIMIT 1',array(1=>'Error eliminando relación de archivo.'.$ori));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			else{ a_sql::transaction(true); }
			$js=_js::r('Archivo eliminado correctamente.');
		}
	}
	return $js;
}

}

if(_0s::$router =='GET rfi'){
	echo gtdRfi::get($_GET);
}
else if(_0s::$router =='POST rfi'){
	_ADMS::lib('xCurl,Attach');
	$js=gtdRfi::post($___D);
	if(_err::$err){ $js=_err::$errText; }
	echo $js;
}
else if(_0s::$router =='DELETE rfi'){
	_ADMS::lib('xCurl,Attach');
	$js=gtdRfi::del($___D);
	if(_err::$err){ $js=_err::$errText; }
	echo $js;
}

//
else if(_0s::$router=='GET rfi/tt'){ a_ses::hashKey('sysd.su');
	_ADMS::_lb('sql/filter');
	echo a_sql::queryL('SELECT A.* FROM gtd_orfi A WHERE 1 '.a_sql_filtByT($_GET['wh']).' LIMIT 200');
}
else if(_0s::$router =='PUT rfi/tt'){ a_ses::hashKey('sysd.su');
	if(!isset($_J['L']) || count($_J['L'])==0){ die(_js::e(3,'No hay lineas a modificar.')); }
	else{
		$nl=1;
		a_sql::transaction(); $cmt=false;
		foreach($_J['L'] as $n=>$X){
			$ln='Linea '.$nl.': '; $nl++;
			if($js=_js::ise($X['tt'],$ln.'Se debe definir tt')){ break; }
			else if(_js::iseErr($X['isOpt'],$ln.'Se debe definir si es opcional')){ break; }
			else if(_js::iseErr($X['rfiName'],$ln.'Se debe definir el nombre del requerido.')){ break; }
			else if($js=_js::textMax($X['rfiName'],50,$ln.'Nombre del requerido:')){ _err::err($js); break; }
			else{
				$_J['L'][$n][0]='i';
				$_J['L'][$n][1]='gtd_orfi';
				$_J['L'][$n]['_unik']='rfiId';
			}
		}
		if(_err::$err){ $js=_err::$errText; }
		else{
			a_sql::multiQuery($_J['L']);
			if(_err::$err){ $js=_err::$errText; }
			else{ $cmt=true; $js=_js::r('Datos actualizados correctamente.'); }
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}

?>
