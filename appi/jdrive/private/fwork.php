<?php
JRoute::get('jdrive/fwork/files',function($D){
	_ADMS::libC('jdrive','jdrive'); 
	$M= JDrive::getFolder(['getFolder'=>$D['folId'],'perms'=>'R']);
	if(!_err::$err){
		$M['L']=JDrive::getFiles(['folId'=>$D['folId'],'wh'=>'tt=\'fWork\' AND tr=1']);
		$js=_js::enc($M);
	}
	_err::errDie();
	return $js;
});

JRoute::post('jdrive/fwork/files',function($D){
	$Lx=$D['L']; unset($D['L']);
	if(_js::iseErr($D['folId'],'Se debe definir el ID de la carpeta','numeric>0')){}
	else if(_js::isArray($Lx)){_err::err('No se enviaron archivos a guardar en la carpeta.',3); }
	else{
		//revisar permisos solo si no estoy definiendo un pwork
		{
			_ADMS::libC('jdrive','jdrive');
			$M= JDrive::getFolder(['folId'=>$D['folId'],'perms'=>'P']);
		}
		if(!_err::$err){
			$n=0; $errs=0; $files=0;
			$Ls=array(); $dateC=date('Y-m-d H:i:s');
			foreach($Lx as $nx => $L){ $n++;
				$ln='Linea '.$n.': ';
				$L['folId']=$D['folId']; $L['tt']='fWork'; $L['tr']=1;
				$L['versNum']=1; $L['userId']=a_ses::$userId; $L['dateC']=$dateC;
				$L['fileId']=a_sql::qInsert($L,['tbk'=>'gtd_ffc1']);
				if(a_sql::$err){ _err::err($ln.'Error guardando archivo . '.a_sql::$errText,3); $errs++; break; }
				else{ $files++;
					$Ls[]=$L;
				}
			}
			if(!_err::$err){ $js=_js::r('Se guardaron '.$files.' archivos.',$Ls); }
		}
	}
	_err::errDie();
	return $js;
});
?>