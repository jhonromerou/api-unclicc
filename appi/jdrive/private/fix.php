<?php
class gtdFix{
static public function get($P=array()){
	$tt=$P['tt']; $tr=$P['tr']; unset($P['tt'],$P['tr']);
	if($js=_js::ise($tt,'Se debe definir tt para obtener archivos fijos.')){ _err::err($js); }
	else if($js=_js::ise($tr,'Se debe definir tt para obtener archivos fijos.','numeric>0')){ _err::err($js); }
	else{
		$js=a_sql::queryL('SELECT F1.*,F.folName
		FROM gtd_ofix FX
		JOIN gtd_ffc1 F1 ON (F1.fileId=FX.fileId) 
		LEFT JOIN gtd_offc F ON (F.folId=F1.folId) 
		WHERE FX.tt=\''.$tt.'\' AND FX.tr=\''.$tr.'\' LIMIT 500',array(1=>'Error obteniendo archivos de fijos: ',2=>'No han archivos fijos relacionados.'));
	}
	return $js;
}
static public function put($P=array()){
	$ori=' on[gtdFix::put()]';
	if($js=_js::ise($P['tt'],'Se debe definir tt.'.$ori)){ _err::err($js); }
	else if($js=_js::ise($P['tr'],'Se debe definir tr.'.$ori)){ _err::err($js); }
	else if($js=_js::ise($P['fileId'],'Se debe definir Id de archivo.'.$ori,'numeric>0')){ _err::err($js); }
	else{
		a_sql::transaction(); $cmt=false;
		$q=a_sql::insert($P,array('table'=>'gtd_ofix','qDo'=>'insertOrDelete','wh_change'=>'WHERE tt=\''.$P['tt'].'\' AND tr=\''.$P['tr'].'\' AND fileId=\''.$P['fileId'].'\' LIMIT 1'));
		if($q['err']){ _err::err('Error fijando archivo. '.$q['text'].$ori,3); }
		else{ $cmt=true;
			if($q['qact']=='deleteInsert'){ $js=_js::r('Archivo desmarcado.');  }
			else{ $js=_js::r('Archivo fijado en perfil.');  }
		}
		a_sql::transaction($cmt);
		return $js;
	}
}
}
if(_0s::$router =='GET fix'){
	echo gtdFix::get($_GET);
}
else if(_0s::$router =='PUT fix'){
	$js=gtdFix::put($_J);
	if(_err::$err){ $js=_err::$errText; }
	echo $js;
}
?>