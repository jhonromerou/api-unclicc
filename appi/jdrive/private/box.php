<?php
JRoute::get('jdrive/box',function($D){
	_ADMS::libC('jdrive','jdrive');
	if($D['sysCode']){
		$D['wh']= 'sysCode=\''.$D['sysCode'].'\'';
	}
	$js= JDrive::folderList($D);
	return $js;
});

/* edicion carpetas */
JRoute::get('jdrive/box/files',function($D){
	_ADMS::libC('jdrive','jdrive'); 
	$M= JDrive::getFolder(['folId'=>$D['folId'],'permsReq'=>'R']);
	if(!_err::$err){
		$M['L']=JDrive::getFiles($D);
		if(a_sql::$errNo==2){ $M['Lerr']=json_decode(_js::e(3,'No se encontraron archivos')); }
		$js=_js::enc($M);
	}
	_err::errDie();
	return $js;
});

JRoute::post('jdrive/box/files',function($D){
	$Lx=$D['L']; unset($D['L']);
	if(_js::iseErr($D['folId'],'Se debe definir el ID de la carpeta','numeric>0')){}
	else if(_js::isArray($Lx)){_err::err('No se enviaron archivos a guardar en la carpeta.',3); }
	else{
		//revisar permisos solo si no estoy definiendo un pwork
		{
			_ADMS::libC('jdrive','jdrive');
			$M= JDrive::getFolder(['folId'=>$D['folId'],'permsReq'=>'W']);
		}
		if(!_err::$err){
			$n=0; $errs=0; $files=0;
			$Ls=array(); $dateC=date('Y-m-d H:i:s');
			foreach($Lx as $nx => $L){ $n++;
				$ln='Linea '.$n.': ';
				$L['folId']=$D['folId'];
				$L['versNum']=1; $L['userId']=a_ses::$userId; $L['dateC']=$dateC;
				$L['tt']=$D['tt']; $L['tr']=$D['tr'];
				$L['fileId']=a_sql::qInsert($L,['tbk'=>'gtd_ffc1']);
				if(a_sql::$err){ _err::err($ln.'Error guardando archivo . '.a_sql::$errText,3); $errs++; break; }
				else{ $files++;
					$Ls[]=$L;
				}
			}
			if(!_err::$err){ $js=_js::r('Se guardaron '.$files.' archivos.',$Ls); }
		}
	}
	_err::errDie();
	return $js;
});
JRoute::get('jdrive/box/folders',function($D){
	_ADMS::libC('jdrive','jdrive');
	$js=_js::enc2(JDrive::getFolder(['folId'=>$D['folId'],'permsReq'=>'N','_users'=>'Y','creating'=>'Y']));
	_err::errDie();
	return $js;
});
JRoute::put('jdrive/box/folders',function($D){
	_ADMS::libC('jdrive','jdrive');
	if(_js::iseErr($D['folName'],'Se debe definir un nombre para la carpeta')){}
	else if(_js::iseErr($D['privacity'],'Se debe definir la privacidad para la carpeta')){}
	else{
		a_sql::transaction(); $c=false;
		$Lx=($D['L'])?$D['L']:[]; unset($D['L']);
		$M=JDrive::getFolder(['getFolder'=>$D['folId'],'permsReq'=>'P','creating'=>'Y']);
		if(!_err::$err && $D['fatherId']>0){
			$M=JDrive::getFolder(['getFolder'=>$D['fatherId'],'permsReq'=>'P','creating'=>'Y']);
		}
		//miembros -permisos
		if(!_err::$err && !_js::isArray($Lx)){
			foreach($Lx as $n=>$L){
				$L[0]='i'; $L[1]='gtd_ffc2'; $L['folId']=$D['folId'];
				if($L['id']>0){ $L[0]='u'; $L['_wh']='folId=\''.$L['folId'].'\' AND userId=\''.$L['userId'].'\' '; }
				$Lx[$n]=$L;
			}
		}
		if(!_err::$err){
			$D[0]='u'; $D[1]='gtd_offc'; $D['_wh']='folId=\''.$D['folId'].'\' LIMIT 1';
			$D['_err1']='Error actualizando carpeta';
			array_unshift($Lx,$D);
			a_sql::multiQuery($Lx);
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Carpeta actualizada correctamente',$D);
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
});
JRoute::post('jdrive/box/folders',function($D){
	_ADMS::libC('jdrive','jdrive');
	if(_js::iseErr($D['folName'],'Se debe definir un nombre para la carpeta')){}
	else if(_js::iseErr($D['privacity'],'Se debe definir la privacidad para la carpeta')){}
	a_sql::transaction(); $c=false;
	$Lx=($D['L'])?$D['L']:[]; unset($D['L']);
	if(!_err::$err){ $D['folId']=a_sql::nextID('gtd_offc'); }
	if(!_err::$err){
		if($D['fatherId']>0){
			$M=JDrive::getFolder(['getFolder'=>$D['fatherId'],'permsReq'=>'P','creating'=>'Y']);
			if(!_err::$err){
				$D['lv']=$M['lv']+1;
			}
			if($D['lv']>JDrive::$maxDepth){ _err::err('No se puede añadir más niveles a las carpetas. Máximo '.JDrive::$maxDepth,3); }
		}
		//miembros -permisos
		if(!_err::$err && !_js::isArray($Lx)){
			foreach($Lx as $n=>$L){
				$L[0]='i'; $L[1]='gtd_ffc2'; $L['folId']=$D['folId'];
				$Lx[$n]=$L;
			}
		}
		if(!_err::$err){
			$D[0]='i'; $D[1]='gtd_offc'; $D[2]='ud';
			$D['_err1']='Error creando carpeta';
			array_unshift($Lx,$D);
			a_sql::multiQuery($Lx);
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Carpeta creada correctamente',$D);
		}
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
});

?>