<?php
class Gda{
static $TBK=array('gda_oaud','gda_aud1','gda_aud2','k'=>'docEntry');
static $TBKT=array('gda_odba','gda_dba1','gda_dba2','k'=>'docEntry','template'=>true);

static public function dba_verif($_J=array()){
	$js=false;
	if($js=_js::ise($_J['version'],'Se debe definir la versión.')){}
	else if($js=_js::ise($_J['docCode'],'Se debe definir el código.')){}
	else if($js=_js::ise($_J['dbaName'],'Se debe definir el nombre.')){}
	else if($js=_js::textMax($_J['objetivo'],1000,'Objetivo: ')){}
	else if($js=_js::textMax($_J['alcance'],1000,'Alcance: ')){}
	else if($js=_js::textMax($_J['descrip'],2000,'Descripción: ')){}
	if($_J['stages'] && $_J['stages']!=''){ $_J['doStages']='Y';
		if($js=_js::textMax($_J['stages'],100,'Etapas: ')){}
	}
	else{ $_J['doStages']='N'; }
	if($js){ _err::err($js); }
	return $_J;
}

static public function get($D=array()){
	_ADMS::_lb('sql/filter');
	return a_sql::queryJs('SELECT A.*
	FROM '.self::$TBK[0].' A 
	WHERE 1 '.a_sql_filtByT($D).' ');
}
static public function post($_J=array()){
	$L=$_J['L']; unset($_J['L']);
	$_J=self::dba_verif($_J);
	if(_err::$err){}
	else{
		a_sql::transaction(); $cmt=false;
		$docEntry=a_sql::qInsert($_J,array('tbk'=>self::$TBK[0]));
		if(a_sql::$err){ _err::err('Error generando documento de auditoria: '.a_sql::$errText,3); }
		else{
			self::putTask($docEntry,$L);
			if(!_err::$err){
				$js=_js::r('Documento #'.$docEntry.' generado correctamente.','"docEntry":"'.$docEntry.'"');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	return $js;
}
static public function put($_J=array()){
	$docEntry=$_J['docEntry'];
	$L=$_J['L']; unset($_J['L']);
	if($js=_js::ise($docEntry,'Se debe definir Id de documento para actualizar.','numeric>0')){ die($js); }
	$_J=self::dba_verif($_J);
	if(_err::$err){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $cmt=false;
		$upds=a_sql::qUpdate($_J,array('tbk'=>self::$TBK[0],'qku'=>'ud','wh_change'=>'docEntry=\''.$docEntry.'\' LIMIT 1'));
		if(a_sql::$err){ _err::err('Error actualizando documento auditoria: '.a_sql::$errText,3); }
		else{
			self::putTask($docEntry,$L);
			if(!_err::$err){
				$js=_js::r('Documento #'.$docEntry.' actualizado correctamente.','"docEntry":"'.$docEntry.'"');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	return $js;
}
static public function getForm($docEntry=''){
	$M=a_sql::fetch('SELECT A.*
	FROM '.self::$TBK[0].' A 
	WHERE A.docEntry=\''.$docEntry.'\' LIMIT 1');
	$M['L']=a_sql::queryL('SELECT B.*
	FROM '.self::$TBK[1].' B 
	WHERE B.docEntry=\''.$docEntry.'\' ',array('L'=>'N','enc'=>'N'));
	return _js::enc2($M);
}
static public function view($docEntry=''){
	$M=a_sql::fetch('SELECT A.* FROM '.self::$TBK[0].' A 
	WHERE A.docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo información del documento.',2=>'El documeto no existe.'));
	$M['L']=a_sql::queryL('SELECT B.* FROM '.self::$TBK[1].' B WHERE B.docEntry=\''.$docEntry.'\' ',array('enc'=>'N','L'=>'N'));
	$M['Lf']=a_sql::queryL('SELECT B2.* FROM '.self::$TBK[2].' B2 WHERE B2.docEntry=\''.$docEntry.'\' ',array('enc'=>'N','L'=>'N'));
	return _js::enc2($M);
}
static public function getTask($P=array()){
	$js=false;
	$tbkId=self::$TBK['k'];
	_ADMS::_lb('sql/filter');
	$M=a_sql::fetch('SELECT A.version,A.docCode,A.dbaName,A.doStages,A.stages FROM '.self::$TBK[0].' A WHERE A.docEntry=\''.$P['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de formulario.',2=>'El documento no existe.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$M['L']=a_sql::queryL('SELECT B1.*
		FROM '.self::$TBK[1].' B1 
		WHERE B1.docEntry=\''.$P['docEntry'].'\' ',array('enc'=>'N','L'=>'N'));
		$js=_js::enc2($M);
	}
	return $js;
}
static public function putTask($docEntry=0,$_JL=array()){
	$tbk=self::$TBK[1]; $tdkId=self::$TBK['k'];
	if($js=_js::ise($docEntry,'Se debe definir Id de auditoria','numeric>0')){ _err::err($js); }
	else if(is_array($_JL) && count($_JL)>0){ 
		$qA=array(); $errs=0; $ln=1;
		foreach($_JL as $x=>$L){
			$L=self::taskVerif($L,array('ln'=>'Linea '.$ln.': ')); 
			if(_err::$err){ $errs++; break; }
			$L[0]='i';
			$L[1]=$tbk; $L['_wh']='id=\''.$L['id'].'\' LIMIT 1';
			$L['docEntry']=$docEntry;
			$L['lineNum']=$ln;
			if($L['delete']=='Y'){ $L[0]='d'; }
			else if($L['id']){ $L[0]='u'; }
			$qA[]=$L; $ln++;
		}
		if($errs==0){
			a_sql::multiQuery($qA);
			if(_err::$err){ $js=_err::$errText; }
			else{ $js=_js::r('Se registraron los tareas correctamente.'); }
		}
	}
	return $js;
}
static public function taskVerif($_J=array(),$P=array()){
	$js=false;
	$ln=$P['ln'];
	$isT=(self::$TBK['template']);
	if($js=_js::ise($_J['proceso'],$ln.'Se debe definir el proceso.')){}
	else if($js=_js::textMax($_J['proceso'],100,$ln.'Tarea:')){}
	else if($js=_js::textMax($_J['assg'],50,$ln.'Responsable:')){}
	else if($js=_js::textMax($_J['hourTxt'],50,$ln.'Horario: ')){}
	else if($js=_js::textMax($_J['audit'],50,$ln.'Auditor:')){}
	else if($js=_js::textMax($_J['teamAudit'],50,$ln.'Equipo Auditor:')){}
	else if($js=_js::textLen($_J['lineMemo'],400,$ln.'Detalles: ')){}
	if($js){ _err::err($js); }
	return $_J;
}

static public function getDocs($P=array()){
	$js=false;
	$tbkId=self::$TBK['k'];
	_ADMS::_lb('sql/filter');
	$today=date('Y-m-d');
	$M=a_sql::fetch('SELECT A.version,A.docCode,A.dbaName FROM '.self::$TBK[0].' A WHERE A.docEntry=\''.$P['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de formulario.',2=>'El documento no existe.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$M['L']=a_sql::queryL('SELECT B2.*,F.fileType,F.fileSize, F.fileName 
		FROM '.self::$TBK[2].' B2 
		LEFT JOIN gtd_ffc1 F ON (F.fileId=B2.fileId)
		WHERE B2.docEntry=\''.$P['docEntry'].'\' ',array('enc'=>'N','L'=>'N'));
		$js=_js::enc2($M);
	}
	return $js;
}
static public function postDocs($D=array()){
	$D=self::docsVerif($D);
	if(_err::$err){ $js=_err::$errText; }
	else{
		_ADMS::lib('Attach');
		$Fi=Attach::postAndTb1($_FILES['file'],$D);
		if(_err::$err){ $js=_err::$errText; }
		else{
			$D['fileId']=$Fi['fileId'];
			$D['dauId']=a_sql::qInsert($D,array('tbk'=>self::$TBK[2]));
			if(a_sql::$err){ die(_js::e(3,'Error registrando documento para auditoria: '.a_sql::$errText)); }
			else{
				$D=a_sql::fetch('SELECT B2.*,F.fileType,F.fileSize, F.fileName 
				FROM '.self::$TBK[2].' B2 
				LEFT JOIN gtd_ffc1 F ON (F.fileId=B2.fileId)
				WHERE 
				B2.fileId=\''.$Fi['fileId'].'\' LIMIT 1');
				$js=_js::r('Documento relacionado correctamente a auditoria.',$D);
			}
		}
	}
	return $js;
}
static public function docsVerif($_J=array()){
	$js=false;
	if($js=_js::ise($_J['docType'],'Se debe definir el tipo.')){}
	else if($js=_js::ise($_J['docName'],'Se debe definir el nombre.')){}
	else if($js=_js::textLen($_J['docName'],50,'Nombre: No puede exceder los 50 caracteres.')){}
	if($js){ _err::err($js); }
	return $_J;
}

/* Aud */
static public function audFromDba($_J=array()){
	$ori=' on[Gda::copyFromDba()]';
	$docEntry=$_J['docEntry']; $_J['docEntry'];
	if($js=_js::ise($docEntry,'El número de la plantilla debe estar definido.','numeric>0')){ _err::err($js); }
	else if($js=_js::ise($_J['cardId'],'El nombre del cliente debe estar definido','numeric>0')){ _err::err($js); }
	else{
		$qA=array();
		$q=a_sql::fetch('SELECT * FROM gda_odba WHERE docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo plantilla de auditoria.'.$ori,2=>'El documento no puede ser copiado,  no existe.'.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			unset($q['docEntry'],$q['userId'],$q['dateC'],$q['userUpd'],$q['dateUpd']);
			$q['cardId']=$_J['cardId'];
			$q1=a_sql::query('SELECT * FROM gda_dba1 WHERE docEntry=\''.$docEntry.'\' ',array(1=>'Error obteniendo tareas del documento.'.$ori,2=>'El documento no puede ser copiado,  no tiene tareas registradas.'.$ori));
			if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			$q2=a_sql::query('SELECT * FROM gda_dba2 WHERE docEntry=\''.$docEntry.'\' ',array(1=>'Error obteniendo documentación relacionada.'.$ori));
			if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			$readDocs=(a_sql::$errNo==-1);
			while($L=$q1->fetch_assoc()){
				unset($L['id']);
				$L[0]='i'; $L[1]='gda_aud1';
				$qA[]=$L;
			}
			if($readDocs) while($L=$q2->fetch_assoc()){
				unset($L['id']);
				$L[0]='i'; $L[1]='gda_aud2';
				$qA[]=$L;
			}
			a_sql::transaction(); $cmt=false;
			$docEntryN=a_sql::qInsert($q,array('tbk'=>'gda_oaud','qk'=>'ud','qku'=>'ud'));
			if(a_sql::$err){ $js=_js::e(3,'Error duplicando documento.'.$ori.' '.a_sql::$errText); }
			else{
				a_sql::multiQuery($qA,array('i'=>array('docEntry'=>$docEntryN)));
				if(_err::$err){ $js=_err::$errText; }
				else{ $cmt=true; $js=_js::r('Documento generado correctamente.','"docEntry":"'.$docEntryN.'"'); }
			}
			a_sql::transaction($cmt);
		}
	}
	return $js;
}

/* Nc */

static public function nc_verif($_J=array()){
	$js=false;
	if($js=_js::ise($_J['docType'],'Se debe definir el tipo de no conformidad.')){}
	else if($js=_js::ise($_J['docDate'],'Se debe definir la fecha de no conformidad.')){}
	else if($js=_js::ise($_J['docTitle'],'Se debe definir un titulo para la no conformidad.')){}
	else if($js=_js::textMax($_J['docTitle'],50,'Titulo: ')){}
	else if($js=_js::textMax($_J['descrip'],500,'Descripción: ')){}
	else if($js=_js::textMax($_J['accMemo'],500,'Nota de acciones: ')){}
	return $js;
}
static public function nc_get($D=array()){
	_ADMS::_lb('sql/filter');
	return a_sql::queryJs('SELECT A.*
	FROM gda_ocnc A 
	WHERE 1 '.a_sql_filtByT($D).' ');
}
static public function nc_post($_J=array()){
	if($js=self::nc_verif($_J)){}
	else{
		$docEntry=a_sql::qInsert($_J,array('tbk'=>'gda_ocnc'));
		if(a_sql::$err){ _err::err('Error registrando no conformidad: '.a_sql::$errText,3); }
		else{
			$js=_js::r('No Conformidad #'.$docEntry.' generada correctamente.','"docEntry":"'.$docEntry.'"');
		}
	}
	return $js;
}
static public function nc_put($_J=array()){
	$docEntry=$_J['docEntry'];
	$L=$_J['L']; unset($_J['L']);
	if($js=self::nc_verif($_J)){}
	else{
		a_sql::transaction(); $cmt=false;
		$upds=a_sql::qUpdate($_J,array('tbk'=>'gda_ocnc','qku'=>'ud','wh_change'=>'docEntry=\''.$docEntry.'\' LIMIT 1'));
		if(a_sql::$err){ _err::err('Error registrando no conformidad: '.a_sql::$errText,3); }
		else{
			self::nc_putTb1($docEntry,$L);
			if(!_err::$err){
				$js=_js::r('No Conformidad #'.$docEntry.' actualizada correctamente.','"docEntry":"'.$docEntry.'"');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
	}
	return $js;
}
static public function nc_getForm($docEntry=''){
	$M=a_sql::fetch('SELECT A.* FROM gda_ocnc A 
	WHERE A.docEntry=\''.$docEntry.'\' LIMIT 1');
	$M['L']=a_sql::queryL('SELECT B.*
	FROM gda_cnc1 B 
	WHERE B.docEntry=\''.$docEntry.'\' ',array('L'=>'N','enc'=>'N'));
	return _js::enc2($M);
}
static public function nc_view($docEntry=''){
	$M=a_sql::fetch('SELECT A.* FROM gda_ocnc A 
	WHERE A.docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo información del documento.',2=>'El documeto no existe.'));
	$M['L']=a_sql::queryL('SELECT B.* FROM gda_cnc1 B WHERE B.docEntry=\''.$docEntry.'\' ',array('enc'=>'N','L'=>'N'));
	return _js::enc2($M);
}
static public function nc_putTb1($docEntry=0,$_JL=array()){
	if($js=_js::ise($docEntry,'Se debe definir Id de no conformidad','numeric>0')){ _err::err($js); }
	else if(is_array($_JL) && count($_JL)>0){ 
		$qA=array(); $errs=0; $ln=1;
		foreach($_JL as $x=>$L){
			$lnt='Linea '.$ln.': ';
			if($js=_js::ise($L['accion'],$lnt.'Se debe definir la acción')){ _err::err($js); $errs++; break; }
			else if($js=_js::textMax($L['accion'],50,$lnt.'Acción')){ _err::err($js); $errs++; break; }
			$L['_unik']='id';
			$L[1]='gda_cnc1';
			$L['docEntry']=$docEntry;
			$L['lineNum']=$ln;
			$qA[]=$L; $ln++;
		}
		if($errs==0){
			a_sql::multiQuery($qA);
			if(_err::$err){ $js=_err::$errText; }
			else{ $js=_js::r('Se registraron los tareas correctamente.'); }
		}
	}
	return $js;
}
}
?>