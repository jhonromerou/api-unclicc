
Api.Sgc={pr:'/appi/private/sgc/'};
$oB.push($V.docTT,[
  {k:'sgcAcm',v:'Acciones C//M/R'}
]);
$V.sgcAcmType=[{k:'C',v:'Correctiva'},{k:'P',v:'Preventiva'},{k:'M',v:'Mejora'}];
$V.sgcAcmStatus2=[{k:'A',v:'Efectiva'},{k:'B',v:'No Efectiva'},{k:'C',v:'No Implementada'},{k:'P',v:'Pendiente',_inactive:'Y'}];
$V.Mdls['sgc']={t:'S.G.C',ico:'fa fa-finger-up'};

_Fi['sgcAcm']=function(wrap){
	$Doc.filter({func:Sgc.Acm.get,docNum:'Y',docUpd:'Y'},[
  {k:'d1'},{k:'d2'},{k:'docStatus'},
  {L:'Fecha Cierre',wxn:'wrapx8',I:{lTag:'date',name:'A.closeDate(E_mayIgual)'}},
  {L:'Fecha Fin',wxn:'wrapx8',I:{lTag:'date',name:'A.closeDate(E_menIgual)'}},
  ,{k:'ordBy'},
  {divLine:1,L:'Tipo',wxn:'wrapx8',I:{lTag:'select',name:'A.docType',opts:$V.sgcAcmType}},
  {L:'Origen',wxn:'wrapx8',I:{lTag:'select',name:'A.docClass',opts:$JsV.sgcAcmClass}},
  {L:'Proceso',wxn:'wrapx8',I:{lTag:'select',name:'A.dpto',opts:$JsV.sgcWp}},
  {L:'Titulo',wxn:'wrapx4',I:{lTag:'input',name:'A.docTitle(E_like3)'}},
  {L:'Descripción',wxn:'wrapx4',I:{lTag:'input',name:'A.lineMemo(E_like3)'}},
	],wrap);
};
_Fi['sgcRep.acm']=function(wrap,x){
	var jsV = 'jsFiltVars';
  opt1=[{k:'C',v:'General'}];
  $Doc.filter({func:Sgc.Rep.acm,docNum:'Y'},[
    {k:'rep',opts:opt1},
    {k:'d1',value:$2d.last7},{k:'d2',value:$2d.today},{k:'docStatus',opts:$V.docStatusOC},{tbSerie:'sgcAcm'},{k:'docNum'},{k:'card'},
    {divLine:1,L:'Proceso',wxn:'wrapx8',I:{lTag:'select',name:'A.dpto',opts:$JsV.sgcWp}},
    {L:'Tipo',wxn:'wrapx8',I:{lTag:'select',name:'A.docType',opts:$V.sgcAcmType}},
    {L:'Origen',wxn:'wrapx8',I:{lTag:'select',name:'A.docClass',opts:$JsV.sgcAcmClass}},
  ],wrap);
};

Sgc={};
Sgc.Acm={
	OLg:function(L){
    var Li=[];
    var ab=new $Doc.liBtn(Li,L,{api:Api.Sgc.pr+'acm',tbSerie:'sgcAcm'});
    ab.add('v');
    ab.add('E',{canEdit:(L.docStatus=='O')});
    if(L.docStatus=='O'){
      ab.add('win',{perms:'sgcAcm.sup',jsF:$Api.JS.cls,ico:'fa fa-lock',textNode:'Cerrar',
      W:{api:'statusC',fTag:Sgc.Acm.close}
      });
    }
    ab.add('N',{perms:'sgcAcm.sup',addText:''});
    ab.add('L');
    return $Opts.add('sgcAcm',Li,L);;
  },
  opts:function(P,pare){
    Li={Li:Sgc.Acm.OLg($js.clone(P.L)),PB:P.L,textNode:P.textNode};
    var mnu=$1.Menu.winLiRel(Li);
    if(pare){ pare.appendChild(mnu); }
    return mnu;
  },
  get:function(){
    var cont=$M.Ht.cont;
    var Cols=[{H:'Estado',k:'docStatus',_g:$V.docStatusAll},
    {H:'Fecha',k:'docDate',dateText:'mmm d'},
    {H:'Tipo',k:'docType',_g:$V.sgcAcmType},
    {H:'Origen',k:'docClass',_g:$JsV.sgcAcmClass},
    {H:'Plazo',k:'dueDate',dateText:'mmm d'},
		{H:'Titulo',k:'docTitle'},
		{H:'Cant.',k:'lineNums'},
		{H:'Proceso',k:'dpto',_g:$JsV.sgcWp}
    ];
    $Doc.tbList({api:Api.Sgc.pr+'acm',inputs:$1.G.filter(),
    fOpts:Sgc.Acm.opts,view:'Y',docBy:'userDate',docUpd:'userDate',
    tbSerie:'sgcAcm',
    TD:Cols,
    tbExport:{ext:'xlsx',fileName:'Listado ACM'}
    },cont);
  },
  form:function(P){
		var D=$Cche.d(0,{});
    D.docDate=$2d.today;
    var Pa=$M.read();
		var wrap=$M.Ht.cont; var jsF=$Api.JS.cls;
		$Api.get({f:Api.Sgc.pr+'acm/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:wrap,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
			$Api.form2({api:Api.Sgc.pr+'acm',AJs:D.AJs,PUTid:Pa.docEntry,JrD:D,vidn:'docEntry',midTag:'Y',to:'sgcAcm.view',
      tbH:[
        {divLine:1,req:'Y',wxn:'wrapx8',L:'Serie',I:{xtag:'docSeries',tbSerie:'sgcAcm',jsF:jsF}},	
				{wxn:'wrapx4',L:'Asunto',I:{lTag:'input','class':jsF,name:'docTitle',value:Jr.lineTitle}},
				{L:'Tipo',req:'Y',wxn:'wrapx8',I:{lTag:'select',name:'docType','class':jsF,opts:$V.sgcAcmType}},
				{L:'Origen',req:'Y',wxn:'wrapx8',I:{lTag:'select',name:'docClass','class':jsF,opts:$JsV.sgcAcmClass}},
				{L:'Proceso',req:'Y',wxn:'wrapx8',I:{lTag:'select',name:'dpto','class':jsF,opts:$JsV.sgcWp}},
				{wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'date','class':jsF,name:'docDate',value:Jr.docDate}},
				{wxn:'wrapx8',L:'Plazo',req:'Y',I:{lTag:'date','class':jsF,name:'dueDate',value:Jr.dueDate}},
				{divLine:1,wxn:'wrapx1',L:'Descripcion de la acción',I:{lTag:'textarea','class':jsF+' tareamulti',name:'lineMemo',value:Jr.lineMemo}},
      ],
      reqFields:{
        D:[{k:'serieId',iMsg:'Serie'},{k:'docTitle',iMsg:'Titulo'},{k:'docType',iMsg:'Tipo'},{k:'docClass',iMsg:'Origen'},{k:'dpto',iMsg:'Proceso'},{k:'docDate',iMsg:'Fecha'},{k:'dueDate',iMsg:'Plazo'}],
        L:[{k:'lineStatus',iMsg:'Estado'},{k:'lineText',iMsg:'Actividad'},{k:'lineAssg',iMsg:'Responsables'},{k:'lineDate',iMsg:'Fecha'}],
      },
      },wrap);
			midCont=$1.q('.midCont',wrap);
			fie=$1.T.fieset({L:{textNode:'Plan de Acción'}},midCont);
			var tb=$1.T.table(['','Estado','Actividad','Responsables','Fecha',''],0,fie);
			var tBody=$1.t('tbody',0,tb);
			$1.T.btnFa({faBtn:'fa-plus-circle',textNode:'Añadir',func:function(){ trA({},tBody); }},midCont);
			function trA(L,tBody){
        var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
        jsFLN=$Api.JS.clsLN;
        var td=$1.t('td',0,tr);
				$1.Move.btns({},td);
        var td=$1.t('td',0,tr);
				$1.lTag({tag:'select',name:'lineStatus','class':jsFLN,opts:$V.docStatusOC,selected:L.lineStatus,noBlank:'Y'},td);
				var td=$1.t('td',0,tr);
				$1.lTag({tag:'textarea',name:'lineText','class':jsFLN,value:L.lineText,style:'width:420px'},td);
				var td=$1.t('td',0,tr);
				$1.lTag({tag:'input',name:'lineAssg','class':jsFLN,value:L.lineAssg},td);
				var td=$1.t('td',0,tr);
				$1.lTag({tag:'date',name:'lineDate','class':jsFLN,value:L.lineDate,style:'width:140px'},td);
        $1.lineDel(L,{},$1.t('td',0,tr));
			}
			if(Jr.L && !Jr.L.errNo){ for(var i2 in Jr.L){
				trA(Jr.L[i2],tBody);
			} }
		}});
  },
  view:function(){
    var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
    $Api.get({f:Api.Sgc.pr+'acm/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
      midCont=$1.t('div');
      var tP={tbSerie:'sgcAcm',D:Jr,midCont:midCont,main:Sgc.Acm.OLg,
        THs:[
          {sdocNum:'Y'},{sdocTitle:'Y',cs:5,ln:1},{k:'dpto',_g:$JsV.sgcWp,ln:1,cs:2},
          {t:'Tipo',k:'docType',_g:$V.sgcAcmType},{middleInfo:'Y'},{logo:'Y'},
          {k:'docDate',t:'Fecha'},{k:'dueDate',t:'Plazo'},
          {t:'Origen',k:'docClass',_g:$JsV.sgcAcmClass,cs:2},
          {k:'docTitle',ln:1,cs:5},
          {k:'lineMemo',cs:8,tlabel:'Descripción de la acción:'},
          {t:'Estado',k:'docStatus',_g:$V.docStatusAll},
          {t:'Implementación',k:'docStatus2',_g:$V.sgcAcmStatus2,_def:'P',ln:1},{k:'closeDate',t:'Cerrada',ln:1,cs:3},
          {k:'lineMemoClose',cs:8,tlabel:'Detalles del Cierre:'},
        ],
      };
      tP.Tabs=[
        {k:'plugsCmts',P:{tt:'sgcAcm',tr:Jr.docEntry,getList:'Y'}},
        {k:'plugsFiles',P:{tt:'sgcAcm',tr:Jr.docEntry,getList:'Y'}},
      ];
      $Doc.view(cont,tP);
      if(1){
        var tb=$1.T.table(['Estado','Actividad','Responsables','Fecha']);
        tb.classList.add('table_x100');
        $1.T.fieset({L:{textNode:'Plan de Acción'}},midCont,tb)
        var tBody=$1.t('tbody',0,tb);
        if(Jr.L && !Jr.L.errNo){ for(var i2 in Jr.L){ L=Jr.L[i2];
          var tr=$1.t('tr',0,tBody);
          $1.t('td',{textNode:_g(L.lineStatus,$V.docStatus)},tr);
          $1.t('td',{textNode:L.lineText,style:'width:420px'},tr);
          var td=$1.t('td',{textNode:L.lineAssg},tr);
          td=$1.t('td',{textNode:L.lineDate,style:'width:100px'},tr);
        } }
      }
    }});
  },
  close:function(cont,P2,nD){
    $Api.JS.addF([{name:'docEntry',value:nD.docEntry,jsF:P2.jsF}],cont);
    $1.multiDivL([
      {divLine:1,L:'Fecha',wxn:'wrapx2',req:'Y',I:{lTag:'date',name:'closeDate','class':P2.jsF}},
      {L:'Implementación',wxn:'wrapx2',I:{lTag:'select',name:'docStatus2',opts:$V.sgcAcmStatus2,'class':P2.jsF}},
      {divLine:1,L:'Detalles de Cierre',wxn:'wrapx1',I:{lTag:'textarea',name:'lineMemoClose','class':P2.jsF}}
    ],cont);
  }
}

Sgc.Rep={
  acm:function(){
		$Api.Rep.base({f:Api.Sgc.pr+'rep/acm',inputs:$1.G.filter(),
		V_C:[
      {t:'No.',k:'docNum'},{t:'Estado',k:'docStatus',_g:$V.docStatusAll},
      {t:'Tipo',k:'docType',_g:$V.sgcAcmType},
      {t:'Origen',k:'docClass',_g:$JsV.sgcAcmClass},{t:'Dpto',k:'dpto',_g:$JsV.sgcWp},{t:'Fecha',k:'docDate',fType:'date'},{t:'Plazo',k:'duedate',fType:'date'},
      {t:'Asunto',k:'docTitle'},{t:'Descripcion',k:'lineMemo'},
      {t:'Fecha Cierre',k:'closeDate'},{t:'Detalle Cierre',k:'lineMemoClose'},
		],
	},$M.Ht.cont);
	}
}

$M.kauAssg('sgc',[
	{k:'sgcAcm',t:'Acciones C/M/R'},
	{k:'sgcAcm.sup',t:'Acciones C/M/R - Supervisor'}
]);

$M.liAdd('sgc',[
	{_lineText:'Acciones C/M/R'},
	{k:'sgcAcm',t:'Acciones',kau:'sgcAcm',ini:{f:'sgcAcm', btnGo:'sgcAcm.form',gyp:Sgc.Acm.get }},
	{k:'sgcAcm.form',t:'Acción', kau:'sgcAcm',ini:{g:Sgc.Acm.form }},
	{k:'sgcAcm.view',noTitle:'Y',t:'Accion (Doc)', kau:'sgcAcm',ini:{g:Sgc.Acm.view }},
],{prp:{mdlActive:'sgc'}});

$M.liRep('sgc',[
	{_lineText:'_REP'},
	{k:'sgcRep.acm',t:'Acciones C/P/M', kauAssg:'sgcAcm',ini:{f:'sgcRep.acm'}},
],{repM:['sgc'],prp:{mdlActive:'sgc'}});

$JsV.liAdd([
  {kMdl:'sgc',kObj:'sgcAcmClass',mdl:'a1',liTxtG:'Origen de Acciones',liTxtF:'Origen de Acción'},
  {kMdl:'sgc',kObj:'sgcDpto',mdl:'a1',liTxtG:'Origen de Acciones',liTxtF:'Origen de Acción'},
  {kMdl:'gvt',kObj:'sgcWp',mdl:'a1',mdlActive:'sgc',liTxtG:'Procesos SGC',liTxtF:'Proceso SGC'},
  ],{mdlActive:'sgc'});