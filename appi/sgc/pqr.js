
$oB.push($V.docTT,[{k:'sgcPqr',v:'PQR'}]);

$V.sgcPqrType=[{k:'P',v:'Petición'},{k:'Q',v:'Queja'},{k:'R',v:'Reclamo'},{k:'O',v:'Otros'}];
$V.sgcPqrType2=[{k:'G',v:'Ninguno'},{k:'I',v:'Articulos'}];
$V.sgcPqrAccep=[{k:'Y',v:'Si'},{k:'N',v:'No'},{k:'P',v:'Parcial'},{k:'o',v:'Abierta',_inactive:'Y'}];

_Fi['sgcPqr']=function(wrap){
	$Doc.filter({func:Sgc.Pqr.get,docNum:'Y'},[
  {k:'d1'},{k:'d2'},{k:'docStatus'},{tbSerie:'sgcPqr'},{k:'docNum'},{k:'card',f:'C.cardName'},{k:'ordBy'},
  {divLine:1,L:'Departamento',wxn:'wrapx8',I:{lTag:'select',name:'A.dpto',opts:$JsV.parDpto}},
  {L:'Tipo',wxn:'wrapx8',I:{lTag:'select',name:'A.docType',opts:$V.sgcPqrType}},
  {L:'Causa',wxn:'wrapx8',I:{lTag:'select',name:'A.docClass',opts:$JsV.sgcPqrClass}},
  {L:'Responsable',wxn:'wrapx8',I:{lTag:'select',name:'A.slpId',opts:$Tb.oslp}},
	],wrap);
};
_Fi['sgcRep.pqr']=function(wrap,x){
	opt1=[{k:'C',v:'General'}];
  $Doc.filter({func:Sgc.Rep.pqr,docNum:'Y'},[
    {k:'rep',opts:opt1},
    {k:'d1',value:$2d.last7},{k:'d2',value:$2d.today},{k:'docStatus'},{tbSerie:'sgcPqr'},{k:'docNum'},{k:'card'},
    {divLine:1,L:'Departamento',wxn:'wrapx8',I:{lTag:'select',name:'A.dpto',opts:$JsV.parDpto}},
    {L:'Tipo',wxn:'wrapx8',I:{lTag:'select',name:'A.docType',opts:$V.sgcPqrType}},
    {L:'Causa',wxn:'wrapx8',I:{lTag:'select',name:'A.docClass',opts:$JsV.sgcPqrClass}},
    {L:'Responsable',wxn:'wrapx8',I:{lTag:'select',name:'A.slpId',opts:$Tb.oslp}},
    ],wrap);
};

Sgc.Pqr={
  OLg:function(L){
    var Li=[];
    var ab=new $Doc.liBtn(Li,L,{api:Api.Sgc.pr+'pqr',tbSerie:'sgcPqr'});
    ab.add('v');
    ab.add('E',{canEdit:(L.docStatus=='S')});
    if(L.docStatus=='S'){
      ab.add('win',{perms:'sgcPqr.sup',jsF:$Api.JS.cls,ico:'fa fa-rocket',textNode:'Abrir',
      W:{api:'statusO',fTag:(ww,P2,nD)=>{
        $Api.JS.addF([{name:'docEntry',value:nD.docEntry,jsF:P2.jsF}],ww);
        $1.multiDivL([{divLine:1,L:'Departamento',wxn:'wrapx2',req:'Y',I:{lTag:'select',name:'dpto','class':P2.jsF,opts:$JsV.parDpto}},
        {divLine:1,L:'Detalles Apertura',wxn:'wrapx1',I:{lTag:'textarea',name:'lineMemoOpen','class':P2.jsF+' tareamulti'}}
      ],ww);
      }}
      });
    }
    if(L.docStatus=='O'){
      ab.add('win',{perms:'sgcPqr.sup',jsF:$Api.JS.cls,ico:'fa fa-lock',textNode:'Cerrar',
      W:{api:'statusC',fTag:(ww,P2,nD)=>{
        $Api.JS.addF([{name:'docEntry',value:nD.docEntry,jsF:P2.jsF}],ww);
        $1.multiDivL([{divLine:1,L:'Aceptada',wxn:'wrapx2',req:'Y',I:{lTag:'select',name:'docAccep','class':P2.jsF,opts:$V.sgcPqrAccep}},
        {L:'Costo',wxn:'wrapx2',I:{lTag:'$',name:'cost','class':P2.jsF}},
        {divLine:1,L:'Detalles de Cierre',wxn:'wrapx1',I:{lTag:'textarea',name:'lineMemoClose','class':P2.jsF}}
      ],ww);
      }}
      });
    }
    ab.add('N',{perms:'sgcPqr.sup',addText:''});
    ab.add('L');
    return $Opts.add('sgcPqr',Li,L);;
  },
  opts:function(P,pare){
    Li={Li:Sgc.Pqr.OLg($js.clone(P.L)),PB:P.L,textNode:P.textNode};
    var mnu=$1.Menu.winLiRel(Li);
    if(pare){ pare.appendChild(mnu); }
    return mnu;
  },
  get:function(){
    var cont=$M.Ht.cont;
    var Cols=[{H:'Estado',k:'docStatus',_g:$V.docStatusAll},
    {H:'Fecha',k:'docDate',dateText:'mmm d'},
    {H:'Tipo',k:'docType',_g:$V.sgcPqrType},
    {H:'Causa',k:'docClass',_g:$JsV.sgcPqrClass},
    {H:'Dpto.',k:'dpto',_g:$JsV.parDpto},
    {H:'Cliente',k:'cardName'},
    {H:'Responsable',k:'slpId',_g:$Tb.oslp},
    {H:'Asunto',k:'docTitle'},
    ];
    $Doc.tbList({api:Api.Sgc.pr+'pqr',inputs:$1.G.filter(),
    fOpts:Sgc.Pqr.opts,view:'Y',docBy:'userDate',docUpd:'userDate',
    tbSerie:'sgcPqr',
    TD:Cols,
    tbExport:{ext:'xlsx',fileName:'Listado PQR'}
    },cont);
  },
  form:function(){
    var D=$Cche.d(0,{});
    var cont=$M.Ht.cont;
    D.docDate=$2d.today;
    var cont =$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.Sgc.pr+'pqr/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
      var jsF=$Api.JS.cls;
      var card=$1.lTag({tag:'card','class':'_crd',cardType:'C',topPare:cont,D:D,fie:'slpId'});
      $Api.form2({api:Api.Sgc.pr+'pqr',AJs:D.AJs,PUTid:Pa.docEntry,JrD:D,vidn:'docEntry',to:'sgcPqr.view',midTag:'Y',
      tbH:[
        {divLine:1,req:'Y',wxn:'wrapx8',L:'Serie',I:{xtag:'docSeries',tbSerie:'sgcPqr',jsF:jsF}},	
        {L:'Fecha',wxn:'wrapx8',req:'Y',I:{lTag:'date',name:'docDate',value:D.docDate,'class':$Doc.Fx.clsdocDate,'class':jsF}},
        {L:'Tercero',req:'Y',wxn:'wrapx4',Inode:card},
        {L:'Resp. Venta',wxn:'wrapx8',I:{lTag:'slp',name:'slpId',selected:D.slpId,'class':$Api.Sea.clsBox,k:'slpId','class':jsF}},
        {L:'Tipo',wxn:'wrapx8',I:{lTag:'select',name:'docType',selected:D.docType,opts:$V.sgcPqrType,'class':jsF}},
        {L:'Causa',wxn:'wrapx8',I:{lTag:'select',name:'docClass',selected:D.docClass,opts:$JsV.sgcPqrClass,'class':jsF}},
        {divLine:1,L:'Asunto',wxn:'wrapx8_1',I:{lTag:'input','class':jsF,name:'docTitle',value:D.docTitle}},
        {L:'Relación',wxn:'wrapx8',I:{lTag:'select',name:'docType2',selected:D.docType2,opts:$V.sgcPqrType2,'class':'_docType2 '+jsF}},
        {divLine:1,L:'Descripción',wxn:'wrapx1',I:{tag:'textarea',name:'lineMemo',textNode:D.lineMemo,'class':'tareamulti '+jsF}}
      ]},cont);
      midCont=$1.q('.midCont',cont);
      $1.onchange('._docType2',(val)=>{ ini(val); },cont);
      function ini(val){
        midCont.innerHTML='';
        if(val=='I'){
          var tb=$1.T.table(['Código','Articulo','Cant.','Motivo',''],0,midCont);
          var tBody=$1.t('tbody',0,tb);
          Itm.sea2Add({type:'ivt',itemType:'',vPost:'',bCode:'Y',func:function(Ds){
            for(var i in Ds){ trAItems(Ds[i],tBody); }
          }},midCont);
          if(Jr.L && !Jr.L.errNo){ for(var i2 in Jr.L){
            trAItems(Jr.L[i2],tBody);
          } }
        }
      }
      function trAItems(L,tBody){
        var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
        jsFLN=$Api.JS.clsLN;
        $1.t('td',{textNode:Itm.Txt.code(L)},tr);
        $1.t('td',{textNode:Itm.Txt.name(L)},tr);
        var td=$1.t('td',0,tr);
        qty=$1.lTag({tag:'number',name:'quantity','class':jsFLN,min:0,value:L.quantity*1,style:'width:6rem'},td);
        qty.AJs={itemId:L.itemId,itemSzId:L.itemSzId};
        td=$1.t('td',0,tr);
        $1.lTag({tag:'select',name:'lineClass','class':jsFLN,opts:$JsV.sgcPqrClassL,selected:L.lineClass},td);
        $1.lineDel(L,{},$1.t('td',0,tr));
      }
      ini(Jr.docType2);
    }});
  },
  view:function(){
    var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
    $Api.get({f:Api.Sgc.pr+'pqr/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
      midCont=$1.t('div');
      var tP={tbSerie:'sgcPqr',D:Jr,midCont:midCont,
        main:Sgc.Pqr.OLg,
        THs:[
          {sdocNum:'Y'},{sdocTitle:'Y',cs:5,ln:1},{k:'dpto',_g:$JsV.parDpto,ln:1,cs:2},
          {k:'docDate',t:'Fecha'},{middleInfo:'Y'},{logo:'Y'},
          {k:'slpId',cs:2,_g:$Tb.oslp},
          {t:'Tipo',k:'docType',_g:$V.sgcPqrType},
          {k:'licTradType',_V:'licTradType'},{k:'licTradNum',ln:1},
          {k:'cardName',cs:5,ln:1},
          {t:'Causa',k:'docClass',_g:$JsV.sgcPqrClass,cs:2},{k:'docTitle',ln:1,cs:5},
          {k:'lineMemo',cs:8,tlabel:'Detalles:'},
          {k:'lineMemoOpen',cs:4,tlabel:'Detalles de Apertura:'},
          {k:'lineMemoClose',ln:1,cs:4,tlabel:'Detalles de Cierre:'},
        ],
      };
      tP.Tabs=[
        {k:'plugsCmts',P:{tt:'sgcPqr',tr:Jr.docEntry,getList:'Y'}},
        {k:'plugsFiles',P:{tt:'sgcPqr',tr:Jr.docEntry,getList:'Y'}},
      ];
      $Doc.view(cont,tP);
      if(Jr.docType2=='I'){
        var tb=$1.T.table(['Código','Articulo','Cant.','Motivo']);
        $1.T.fieset({L:{textNode:'Articulos'}},midCont,tb)
        var tBody=$1.t('tbody',0,tb);
        if(Jr.L && !Jr.L.errNo){ for(var i2 in Jr.L){ L=Jr.L[i2];
          var tr=$1.t('tr',0,tBody);
          $1.t('td',{textNode:Itm.Txt.code(L)},tr);
          $1.t('td',{textNode:Itm.Txt.name(L)},tr);
          var td=$1.t('td',{textNode:L.quantity*1},tr);
          td=$1.t('td',{textNode:_g(L.lineClass,$JsV.sgcPqrClassL)},tr);
        } }
      }
    }});
  }
}

Sgc.Rep.pqr=function(){
  $Api.Rep.base({f:Api.Sgc.pr+'rep/pqr',inputs:$1.G.filter(),
  V_C:[
    {t:'No.',k:'docNum'},{t:'Estado',k:'docStatus',_g:$V.docStatusAll},
    {t:'Tipo',k:'docType',_g:$V.sgcPqrType},
    {t:'Causa',k:'docClass',_g:$JsV.sgcPqrClass},
    {t:'Dpto',k:'dpto',_g:$JsV.parDpto},
    {t:'Fecha',k:'docDate',fType:'date'},
    {t:'Asunto',k:'docTitle'},{t:'Descripcion',k:'lineMemo'},
    {t:'Resp. Venta',k:'slpId',_g:$Tb.oslp},
    {t:'Cliente',k:'cardName'},
    {t:'Fecha Cierre',k:'closeDate'},{t:'Detalle Cierre',k:'lineMemoClose'},
  ],
},$M.Ht.cont);
}
$JsV._i({kMdl:'sgc',kObj:'sgcPqrClass',mdl:'a1',liTxtG:'Causas PQR',liTxtF:'Causa PQR'});
$JsV._i({kMdl:'sgc',kObj:'sgcPqrClassL',mdl:'a1',liTxtG:'Causas Articulos PQR',liTxtF:'Causa Articulo PQR'});


$M.kauAssg('sgc',[
	{k:'sgcPqr',t:'PQRs Clientes'},
	{k:'sgcPqr.sup',t:'PQRs Clientes - Supervisor'}
]);

$M.liAdd('sgc',[
	{_lineText:'PQR'},
	{k:'sgcPqr',t:'PQRs',kau:'sgcPqr',ini:{f:'sgcPqr', btnGo:'sgcPqr.form',gyp:Sgc.Pqr.get }},
	{k:'sgcPqr.form',t:'PQR', kau:'sgcPqr',ini:{g:Sgc.Pqr.form }},
	{k:'sgcPqr.view',noTitle:'Y',t:'PQR (Doc)', kau:'sgcPqr',ini:{g:Sgc.Pqr.view }},
],{prp:{mdlActive:'sgc'}});
	
$M.liRep('sgc',[
	{_lineText:'_REP'},
	{k:'sgcRep.pqr',t:'Reporte de PQR', kauAssg:'sgcPqr.sup',ini:{f:'sgcRep.pqr'}}
],{repM:['sgc']});
