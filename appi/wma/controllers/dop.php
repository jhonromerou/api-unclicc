<?php
class wmaDop extends JxDoc{
  static $serie='wmaDop';
  static $tbk99='wma3_doc99'; //componentes
  static $tbk='wma_odop';
  static $tbk1='wma_dop1'; //fases a produccir
  static $tbk2='wma_dop2'; //componentes
  static public function get($D=[]){
    $D['from']='A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docStatus,A.docType,A.dueDate,A.userUpd,A.dateUpd,A.lineMemo FROM '.self::$tbk.' A';
    return a_sql::rPaging($D,false,[1=>'Error obteniendo listado',2=>'No se encontraron resultados.']);
  }
  static public function rev($D=[]){
    if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
    else if(_js::iseErr($D['dueDate'],'Se debe definir la fecha de entrega')){}
    else if(_js::iseErr($D['itemId'],'Se debe definir el articulo','numeric>0')){}
    else if(_js::iseErr($D['itemSzId'],'Se debe definir el articulo (2)','numeric>0')){}
    else if(_js::iseErr($D['quantity'],'Se debe definir la cantidad a produccion','numeric>0')){}
    else if(_js::iseErr($D['docClass'],'Se debe definir la clase de la orden')){}
    else if(_js::isArray($D['L'])){ _err::err('No se enviaron fases para la orden de producción',3); }
  }
  static public function revL($D=[]){
    if(_js::iseErr($D['wfaId'],'Se debe definir la fase','numeric>0')){}
    else if(_js::iseErr($D['wfaIdNext'],'Se debe definir la fase siguiente','numeric>0')){}
    else if(_js::iseErr($D['quantity'],'Se debe definir la cantidad en la fase','numeric>0')){}
  }
  static public function revLF($D=[]){
    if(_js::iseErr($D['wfaId'],'Se debe definir la fase para asignar el material','numeric>0')){}
    else if(_js::iseErr($D['itemId'],'Se debe definir ID del material','numeric>0')){}
  }
  static public function post($D){
    _ADMS::lib('JLog');
    self::rev($D);
    if(!_err::$err){
      a_sql::transaction(); $c=false;
      $D['docEntry']=a_sql::nextID(self::$tbk);
    }
    if(!_err::$err){//registrar fases
      $mI=$D['L'];unset($D['L']);
      foreach($mI as $n=>$L){
        $L[0]='i'; $L[1]=self::$tbk1;
        $L['quantity']=$L['openQty']=$D['quantity'];
        $L['itemId']=$D['itemId'];
        $L['itemSzId']=$D['itemSzId'];
        $L['docEntry']=$D['docEntry'];
        $L['lineNum']=$n+1;
        $Lx=$mI[$n-1];
        if($n>0){ $L['wfaIdBef']=$Lx['wfaId']; }
        else{ $L['wfaIdBef']=0; }
        $Lx=$mI[$n+1];
        if($Lx){ $L['wfaIdNext']=$Lx['wfaId']; }
        else{ $L['wfaIdNext']=9999; }
        self::revL($L);
        if(_err::$err){ break; }
        $mI[$n]=$L;
      }
    }
    if(!_err::$err && is_array($D['LF'])){// registear materiales
      foreach($D['LF'] as $n=>$L){
        self::revLF($L);
        if(_err::$err){ break; }
        $L[0]='i'; $L[1]=self::$tbk2; $L['docEntry']=$D['docEntry'];
        $L['priceLine']=$L['price']*$L['quantity'];
        $mI[]=$L;
      }
      unset($D['LF']);
    }
    if(!_err::$err){//put query
      if($D['ott']=='wmaPdp' && $D['otr']>0){
        $mI[]=['p','UPDATE wma3_pdp1 SET openQty=openQty-'.$D['quantity'].' WHERE docEntry=\''.$D['otr'].'\' AND itemId=\''.$D['itemId'].'\' AND itemSzId=\''.$D['itemSzId'].'\' LIMIT 1'];
        $mI[]=['p','UPDATE wma3_opdp SET docStatus=\'C\' WHERE docEntry=\''.$D['otr'].'\' AND (SELECT SUM(openQty) FROM wma3_pdp1 WHERE docEntry=\''.$D['otr'].'\') <=0  LIMIT 1'];
      }
      $D[0]='i'; $D[1]=self::$tbk; $D[2]='ud';
      $mI[]=$D;
      a_sql::multiQuery($mI);
    }
    if(!_err::$err){ $c=true;
      $js=_js::r('Documento generado correctamente','"docEntry":"'.$D['docEntry'].'"');
      self::tb99P(['docEntry'=>$D['docEntry'],'dateC'=>1]);
      self::tbRel1P(['ott'=>$D['ott'],'otr'=>$D['otr'],'tr'=>$D['docEntry'],'serieId'=>$_J['serieId'],'docNum'=>$_J['docNum']]);
    }
    a_sql::transaction($c);
    _err::errDie();
    return $js;
  }
  static public function put($D){
    if(_js::iseErr($D['docEntry'],'Se debe definir ID del documento','numeric>0')){}
    if(!_err::$err){ self::rev($D); }
    a_sql::transaction(); $c=false;
   if(!_err::$err){
     $qa=a_sql::fetch('SELECT A.docStatus,A.canceled FROM '.self::$tbk.' A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
     if(a_sql::$err){ _err::err(a_sql::$errNoText);  }
     else if($qa['canceled']=='Y'){ _err::err('La orden de producción está anulada.',3); }
     else if($qa['docStatus']!='P'){ _err::err('La orden de producción debe estar planificada para poder modificar',3); }
   }
   if(!_err::$err){
     $mI=$D['L'];unset($D['L']);
     foreach($mI as $n=>$L){
       $L[0]='i'; $L[1]=self::$tbk1;
       $L['_unik']='id';
       $L['quantity']=$L['openQty']=$D['quantity'];
       $L['itemId']=$D['itemId'];
       $L['itemSzId']=$D['itemSzId'];
       $L['docEntry']=$D['docEntry'];
       $L['lineNum']=$n+1;
       $Lx=$mI[$n-1];
       if($n>0){ $L['wfaIdBef']=$Lx['wfaId']; }
       else{ $L['wfaIdBef']=0; }
       $Lx=$mI[$n+1];
       if($Lx){ $L['wfaIdNext']=$Lx['wfaId']; }
       else{ $L['wfaIdNext']=9999; }
       self::revL($L);
       if(_err::$err){ break; }
       $mI[$n]=$L;
     }
   }
   if(!_err::$err && is_array($D['LF'])){ // registrar materiales
     foreach($D['LF'] as $n=>$L){
       self::revLF($L);
       if(_err::$err){ break; }
       $L[0]='i'; $L[1]=self::$tbk2; $L['docEntry']=$D['docEntry'];
       $L['_unik']='id';
       $L['priceLine']=$L['price']*$L['quantity'];
       $mI[]=$L;
     }
     unset($D['LF']);
   }
   if(!_err::$err){
     $D[0]='u'; $D[1]=self::$tbk; $D['_unik']='docEntry';
     $mI[]=$D;
     a_sql::multiQuery($mI);
   }
   if(!_err::$err){ $c=true;
     $js=_js::r('Documento actualizado correctamente','"docEntry":"'.$D['docEntry'].'"');
   }
   a_sql::transaction($c);
   _err::errDie();
   return $js;
  }
  static public function getOne($D){
    $M=a_sql::fetch('SELECT A.docEntry,A.docStatus,A.docType,A.docClass,A.docDate,A.dueDate,A.ref1,A.cardId, I.itemCode,I.itemName,A.itemSzId,A.quantity,C.cardName,A.lineMemo
    FROM '.self::$tbk.' A 
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
    WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
    if(a_sql::$err){ return a_sql::$errNoText; }
    else{
      $M['L']=[];
      $M['LF']=[];
      $q=a_sql::query('SELECT B.wfaId,B.quantity,B.openQty,B.priceLine,B.okQty,B.rejQty
      FROM '.self::$tbk1.' B
      WHERE B.docEntry=\''.$D['docEntry'].'\' ORDER BY B.lineNum ASC',[1=>'Error obteniendo fases de la orden',2=>'La orden no tiene fases registradas']);
      if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
          $M['L'][]=$L;
        }
      }
      /* Materiales */
      $q=a_sql::query('SELECT B.wfaId,I.itemCode,B.itemSzId itemSzId,I.itemName,B.price,B.quantity,I.udm,B.priceLine,B.lineType,B.lineMemo
      FROM '.self::$tbk2.' B
      LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
      WHERE B.docEntry=\''.$D['docEntry'].'\' ',[1=>'Error obteniendo materiales de las fases',2=>'Las fases no tiene materiales asignados']);
      if(a_sql::$err){ $M['LF']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
          $M['LF'][]=$L;
        }
      }
      $js=_js::enc2($M);
    }
    return $js;
  
  }
  static public function formData($D){
    $M=a_sql::fetch('SELECT A.*,I.itemCode,I.itemName,C.cardName
    FROM '.self::$tbk.' A 
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
    WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
    if(a_sql::$err){ return a_sql::$errNoText; }
    else{
      $M['L']=[];
      $M['LF']=[];
      $q=a_sql::query('SELECT B.*
      FROM '.self::$tbk1.' B
      WHERE B.docEntry=\''.$D['docEntry'].'\' ORDER BY B.lineNum ASC',[1=>'Error obteniendo fases de la orden',2=>'La orden no tiene fases registradas']);
      if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
          $M['L'][]=$L;
        }
      }
      /* Materiales */
      $q=a_sql::query('SELECT B.*,I.itemCode,I.itemName,B.price,I.udm
      FROM '.self::$tbk2.' B
      LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
      WHERE B.docEntry=\''.$D['docEntry'].'\' ',[1=>'Error obteniendo materiales de las fases',2=>'Las fases no tiene materiales asignados']);
      if(a_sql::$err){ $M['LF']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
          $M['LF'][]=$L;
        }
      }
      $js=_js::enc2($M);
    }
    return $js;
  }

  static public function liberate($D){
    if(_js::iseErr($D['docEntry'],'Se debe definir ID del documento','numeric>0')){}
    a_sql::transaction(); $c=false;
    if(!_err::$err){
      $qa=a_sql::fetch('SELECT A.docStatus,A.canceled FROM '.self::$tbk.' A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
      if(a_sql::$err){ _err::err(a_sql::$errNoText);  }
      else if($qa['canceled']=='Y'){ _err::err('La orden de producción está anulada.',3); }
      else if($qa['docStatus']!='P'){ _err::err('La orden ya fue liberada',3); }
      else{
        a_sql::query('UPDATE '.self::$tbk.' SET docStatus=\'O\' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error liberando orden de producción']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
      }
    }
    if(!_err::$err){ $c=true;
      $js=_js::r('Orden liberada correctamente');
    }
    a_sql::transaction($c);
    _err::errDie();
    return $js;
  }
}
?>