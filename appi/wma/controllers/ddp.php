<?php
class wmaDdp extends JxDoc{
	static $serie='wmaDpf';
	static $AI='docEntry';
	static $tbk99='wma3_doc99';
	static $tbk='wma_oddp';
	static $tbk1='wma_ddp1'; //materiales-costos
	static $tbk2='wma_ddp2'; // productos obtenidos en el proceso --> acerrin
	static public function get($D=[]){
		$D['from']='A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docStatus,A.itemId,A.itemSzId,A.quantity,A.docTotal,I.itemCode,I.itemName,A.userUpd,A.dateUpd,A.lineMemo FROM '.self::$tbk.' A
		JOIN itm_oitm I ON (I.itemId=A.itemId)
		';
    return a_sql::rPaging($D,false,[1=>'Error obteniendo listado',2=>'No se encontraron resultados.']);
	}
	static public function fieldsRequire($D=array()){
		return [
			['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
			['k'=>'itemId','ty'=>'id','iMsg'=>'Articulo'],
			['k'=>'itemSzId','ty'=>'id','iMsg'=>'subproducto'],
			['k'=>'whsId','ty'=>'id','iMsg'=>'Almacen ingreso'],
			['k'=>'quantity','ty'=>'>0','iMsg'=>'Cantidad'],
			['k'=>'lineMemo','maxLen'=>200],
			['ty'=>'L','k'=>'LF','iMsg'=>'Componentes','L'=>[
				['k'=>'lineType','ty'=>'B','iMsg'=>'Tipo de Componente'],
				['k'=>'itemId','ty'=>'id','iMsg'=>'Articulo'],
				['k'=>'itemSzId','ty'=>'id','iMsg'=>'Articulo (2)'],
				//['k'=>'price','iMsg'=>'Costo'],
				['k'=>'quantity','ty'=>'>0','iMsg'=>'Cantidad'],
			]
			]
		];
	}
	static public function post($D=array()){
		self::formRequire($D);
		if(!_err::$err){
			a_sql::transaction(); $c=false;
			$docEntryN=self::nextID();
		}
		if(!_err::$err){ //registrar componentes y secundarios
			_ADMS::lib('JLog');
			_ADMS::mApps('ivt/Ivt');
			$ivt=new Ivt(['tt'=>self::$serie,'tr'=>$docEntryN,'docDate'=>$D['docDate']]);
			$qI=[0=>'Doc'];
			$D['docTotal']=$D['docTotal2']=$D['docTotal3']=0;
			/* PeP y SE ya tiene componentes consumidos en otros docs */
			$D['PePtotal']=$D['SEtotal']=$D['MPtotal']=$D['MOtotal']=$D['MAtotal']=$D['SVtotal']=$D['CIFtotal']=0;
			$Lx=$D['LF']; $ILx=$D['LS']; unset($D['LF'],$D['LS']);
			if(!_js::isArray($ILx)){ // registrar articulos secundarios
				foreach($ILx as $n=>$L){
					$ivt->getInfo($L,['ln'=>'Linea '.($n+1).': ']); if(_err::$err){ break; }
					$ivt->handSett(['inQty'=>$L['quantity']]);
				}
			}
			Ivt::$canNegDef='Y';
			if(!_err::$err) foreach($Lx as $n=>$L){ //registrar componentes
				if($L['lineType']=='MP' || $L['lineType']=='SE'){
					$ivt->getInfo($L);if(_err::$err){ break; }
					$ivt->handRevi(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					$ivt->handSett(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					$L['price3']=$ivt->La['cost'];
				}
				else{ $L['price3']=$L['price2']; }
				$L['priceLine']=$L['price']*$L['quantity']; //digitado
				$L['priceLine2']=$L['price2']*$L['quantity'];//ficha
				$L['priceLine3']=$L['price3']*$L['quantity']; //promedio
				$L['reqQty'] = $L['baseQty']*$D['quantity'];
				$L['diffQty'] = $L['quantity']-$L['reqQty'];
				$L['diffPrice'] = $L['priceLine']-($L['price2']*$L['reqQty']);
				$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
				$L['_err1']='Error registrando linea de documento';
				$qI[]=$L;
				$k=$L['lineType'].'total';
				$D[$k] +=$L['priceLine'];
				$D['docTotal'] += $L['priceLine'];
				$D['docTotal2'] += $L['priceLine2']; 
				$D['docTotal3'] += $L['priceLine3'];
			}
		}
		if(!_err::$err){//registrar ingresos PeP costMet
			$costInPep=$D['docTotal'];
			$costInPep=$costInPep/$D['quantity']; //pasar el unitario
			if(!_err::$err){
				$ivt->getInfo(['itemId'=>$D['itemId'],'itemSzId'=>$D['itemSzId'],'whsId'=>$D['whsId']],[]);
				if(!_err::$err){ $ivt->handSett(['inQty'=>$D['quantity'],'price'=>$costInPep]); }
			}
		}
		//print_r(($ivt->Livt)); die('----');
		if(!_err::$err){
			$D[0]='i'; $D[1]=self::$tbk; $D[2]='udUpd';
			$D['docEntry']=$docEntryN; $D['_err1']='Error generando documento';
			$qI[0]=$D;
			if($D['pdocEntry']){
				$qI[]=['p','UPDATE wma_dop1 SET openQty=openQty-'.$D['quantity'].', okQty=okQty+'.$D['quantity'].' WHERE docEntry=\''.$D['pdocEntry'].'\' AND itemId=\''.$D['itemId'].'\' AND itemSzId=\''.$D['itemSzId'].'\' AND wfaId=\''.$D['wfaId'].'\' '];
			}
			a_sql::multiQuery($qI);
		}
		if(!_err::$err && !_js::isArray($ivt->Livt)){ a_sql::multiQuery($ivt->Livt); }
		if(!_err::$err){ $c=true;
			$js=_js::r('Documento generado correctamente','"docEntry":"'.$docEntryN.'"');
			self::tb99P(['docEntry'=>$docEntryN,'dateC'=>1]);
      self::tbRel1P(['ott'=>$D['ott'],'otr'=>$D['otr'],'tr'=>$docEntryN,'serieId'=>$D['serieId'],'docNum'=>$D['docNum']]);
		}
		a_sql::transaction($c);
		_err::errDie();
		return $js;
	}

	static public function getOne($D){
    $M=a_sql::fetch('SELECT A.*, I.itemCode,I.itemName
    FROM '.self::$tbk.' A 
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
    if(a_sql::$err){ return a_sql::$errNoText; }
    else{
      $M['MPtotal2']=$M['MOtotal2']=$M['MAtotal2']=$M['MPtotal2']=$M['SVtotal2']=$M['CIFtotal2']=$M['SEtotal2']=$M['PePtotal2']=0;
      $M['MPtotal3']=$M['MOtotal3']=$M['MAtotal3']=$M['MPtotal3']=$M['SVtotal3']=$M['CIFtotal3']=$M['SEtotal3']=$M['PePtotal3']=0;
      $M['LF']=[];
      /* Materiales */
      $q=a_sql::query('SELECT B.wfaId,I.itemCode,B.itemSzId itemSzId,I.itemName,B.price,B.quantity,I.udm,B.priceLine,B.lineType,B.priceLine2,B.reqQty,B.diffPrice,B.diffQty, B.priceLine3
      FROM '.self::$tbk1.' B
      LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
      WHERE B.docEntry=\''.$D['docEntry'].'\' ',[1=>'Error obteniendo materiales de las fases',2=>'Las fases no tiene materiales asignados']);
      if(a_sql::$err){ $M['LF']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
					$k=$L['lineType'];
					$M[$k.'total2']=$L['priceLine2'];
					$M[$k.'total3']=$L['priceLine3'];
          $M['LF'][]=$L;
        }
      }
      $js=_js::enc2($M);
    }
    return $js;
  
  }
}
?>