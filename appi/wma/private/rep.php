<?php
JRoute::get('wma/rep/ipc',function($D){
  _ADMS::lib('sql/filter');
  $vt=$D['viewType']; unset($D['viewType']);
  if($vt=='G'){
    $wh=a_sql_filter($D);
    $qury='SELECT
    I.itemId,I.itemType,I.itemCode,I.itemName,I.grsId,grs2.itemSzId,I.udm,I.sellPrice,I.buyPrice,I.invPrice,I.standPrice,PC.cost,PC.costMP,PC.costMO,PC.costSV,PC.costMA,PC.cif,PC.dateUpd
    FROM itm_oitm I
    JOIN itm_grs2 grs2 ON (grs2.grsId=I.grsId)
    LEFT JOIN itm_oipc PC ON (PC.isModel=\'Y\' AND PC.itemId=I.itemId AND PC.itemSzId=grs2.itemSzId)
    WHERE I.prdItem=\'Y\' '.$wh.' ORDER BY I.itemCode ASC ';
  }
  return a_sql::fetchL($qury,
  ['k'=>'L','D'=>['_view'=>$vt]],true);
});
JRoute::get('wma/rep/ddp',function($D){
  _ADMS::lib('sql/filter');
  $vt=$D['viewType']; unset($D['viewType']);
  if($vt=='G'){
    $wh=a_sql_filter($D);
    $fie='I.itemId,I.itemType,I.itemCode,I.itemName,A.itemSzId,I.udm,A.whsId';
    $gb=$fie;
    $qury='SELECT '.$fie.',SUM(A.quantity) quantity,SUM(A.docTotal) docTotal,SUM(A.docTotal2) docTotal2
    FROM wma_oddp A
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    WHERE A.canceled=\'N\' '.$wh.' GROUP BY '.$gb;
  }
  else if($vt=='D'){
    $wh=a_sql_filter($D);
    $fie='A.docEntry,A.docDate,I.itemId,I.itemType,I.itemCode,I.itemName,A.itemSzId,I.udm,A.whsId,A.quantity,A.docTotal,A.docTotal2';
    $gb=$fie;
    $qury='SELECT '.$fie.'
    FROM wma_oddp A
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    WHERE A.canceled=\'N\' '.$wh.' ';
  }
  $wh=a_sql_filter($D);
  return a_sql::fetchL($qury,
  ['k'=>'L','D'=>['_view'=>$vt]],true);
});
?>
