<?php
_ADMS::lib('JFRead');
JRoute::post('wma/dt/pepAwh',function($D){
		if(_js::isArray($D['L'])){ die(_err::err('No se enviaron lineas en el documento',3)); }
	a_sql::transaction(); $c=false;
	$docEntry=a_sql::nextAI(['tb'=>'pep_oawh']);
	_err::errDie();
	$js=false;
	//$R = JFRead::tsv($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,'K'=>array('itemCode','itemSzId','quantity')));
	$D['docEntry']=$docEntry;
	//if($R['errNo']){ $js = _js::e($R); }
	if(_js::iseErr($D['docEntry'],'Se debe definir el número de documento','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['serieId'],'Se debe definir el Id de la serie','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docType'],'Se debe definir el tipo de documento')){ $js=_err::$errText; }
	else if(_js::iseErr($D['whsId'],'Se debe definir la bodega','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['wfaId'],'Se debe definir la fase','numeric>0')){ $js=_err::$errText; }
	else{
		$Bod=array();
		$ITM=array();
		//Obtener bodegas
		$w=a_sql::fetch('SELECT whsCode,whsId FROM ivt_owhs WHERE whsId=\''.$D['whsId'].'\' LIMIT 1',array(1=>'Error obteniendo bodega.',2=>'No existe la bodega definida.'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		$wf=a_sql::fetch('SELECT wfaId FROM wma_owfa WHERE wfaId=\''.$D['wfaId'].'\' LIMIT 1',array(1=>'Error obteniendo fase.',2=>'No existe la fase definida.'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		$DC=array('docEntry'=>$docEntry,'docDate'=>date('Y-m-d'),
		'docType'=>$D['docType'],
		'serieId'=>$D['serieId'],'whsId'=>$w['whsId'],'wfaId'=>$wf['wfaId'],
		'lineMemo'=>'Generado desde Data Transfer.');
		$DC['L']=array();
		/* generar */
		$Di=array(); $qI=array();
		foreach($D['L'] as $ln => $Da){
			$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
			$lineTotal++;
			$Da['docEntry']=$docEntry;
			$Da['quantity']=preg_replace('/(\s|\t\n)+/','',$Da['quantity']);
			if($js=_js::ise($Da['docEntry'],$lnt.'Se debe definir el número de documento','numeric>0')){ die($js); }
			else if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
			else if($js=_js::ise($Da['itemSzId'],$lnt.'Se debe definir el ID del Subproducto.')){ die($js); }
			else if($js=_js::ise($Da['quantity'],$lnt.'La cantidad a definir no puede ser negativa. valor:('.$Da['quantity'].')')){ die($js); }
			else{
				//verificar articulo
				$quk=$Da['itemCode'];
				$QK1=_uniK::fromQuery(['k'=>$Da['itemCode'],'f'=>'itemId','from'=>'itm_oitm','wh'=>'itemCode=\''.$Da['itemCode'].'\'',
				1=>$lnt.'Error obteniendo Id de articulo.',2=>$lnt.'El código del articulo ('.$Da['itemCode'].') no existe']);
				if(_err::$err){ die(_err::$errText); }
				//verificar s/p
				$QK2=_uniK::fromQuery(['k'=>'sp_'.$Da['itemSzId'],'f'=>'itemSzId','from'=>'itm_grs1','wh'=>'itemSzId=\''.$Da['itemSzId'].'\'',
				1=>$lnt.'Error obteniendo Id de articulo.',2=>$lnt.'El subproducto ('.$Da['itemSzId'].') no existe']);
				if(_err::$err){ die(_err::$errText); }
				$DC['L'][]=array(
				'handInv'=>'Y','itemId'=>$QK1['itemId'],'itemSzId'=>$QK2['itemSzId'],'quantity'=>$Da['quantity']);
			}
		}//for
	}
	if($js==false){
		_ADMS::lib('iDoc,docSeries');
		_ADMS::mApps('wma/pepAwh');
		$js=wmaPepAwh::post($DC);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Proceso realizado correctamente'); }
	}
	a_sql::transaction($c);
	echo $js;
});
?>