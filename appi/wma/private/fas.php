<?php
JRoute::get('wma/fas',function($D){
	$D['from']='wfaId,wfaCode,wfaName,descrip FROM wma_owfa';
	return a_sql::rPaging($D,false,[1=>'Error obteniendo resultados.',2=>'No se encontraron resultados.']);
});
JRoute::get('wma/fas/form',function($D){
	$q=a_sql::fetch('SELECT * FROM wma_owfa WHERE wfaId=\''.$_GET['wfaId'].'\' LIMIT 1',array(1=>'Error obteniendo información de la fase.',2=>'No se encontró la fase Id '.$D['wfaId'].'.'));
	if(a_sql::$errNoText!=''){ $js=a_sql::$errNoText; }
	else{ $js =_js::enc($q); }
	return $js;
});
JRoute::put('wma/fas',function($D){
	if($js=_js::ise($D['wfaCode'],'Se debe definir el código.')){}
	else if(!preg_match('/^([a-z0-9\-]{1,10})$/i',$D['wfaCode'])){$js=_js::e(3,'El código no puede exceder los 10 caracteres y solo pueden ser letras,números o -.'); }
	else if($js=_js::ise($D['wfaName'],'Se debe definir el nombre.')){}
	else if(!_js::textLimit($D['wfaName'],50)){ $js=_js::e(3,'El nombre no puede exceder 50 caracteres.'); }
	else if(!_js::textLimit($D['descrip'],100)){ $js=_js::e(3,'La descripción no puede exceder los 100 caracteres.'); }
	else{
		$ins=a_sql::uniRow($D,array('tbk'=>'wma_owfa','wh_change'=>'wfaId=\''.$D['wfaId'].'\' LIMIT 1'));
		$wfaId=($ins['insertId'])?$ins['insertId']:$D['wfaId'];
		$adJs='"wfaId":"'.$wfaId.'"';
		if(a_sql::$err){ $js=_js::e(1,'Error definiendo fase: '.a_sql::$errText,$adJs); }
		else{ $js=_js::r('Guardado correctamente.',$adJs); }
	}
	return $js;
});
?>