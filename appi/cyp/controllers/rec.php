<?php
class cypRec extends JxDoc{
static $serie='cypRec';
static $AI='docEntry';
static $tbk='cyp_orec';
static $tbk1='cyp_rec1';
static $tbk99='cyp_doc99';
static public function get($D){
	$D['from']='A.*,C.cardName FROM '.self::$tbk.' A
  JOIN cyp_ocon D ON (D.docEntry=A.otr)
  JOIN par_ocrd C ON (C.cardId=D.cardId)';
	return a_sql::rPaging($D);
}
static public function fieldsRequire(){
  return [
    ['k'=>'otr','ty'=>'numeric>0','iMsg'=>'No Prestamo'],
    ['k'=>'lineNum','ty'=>'numeric>0','iMsg'=>'No Cuota'],
		['k'=>'docClass','ty'=>'id','iMsg'=>'Concepto'],
    ['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
    ['k'=>'docTotal','ty'=>'numeric>0','iMsg'=>'Valor Recargo'],
    ['k'=>'lineMemo','maxLen'=>200,'iMsg'=>'Descripcion'],
  ];
}
static public function post($_J=array()){
  //$_J['otr'] es numero contrato
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
		$_J['docEntry']=$docEntryN=self::nextID();
		//if(!_err::$err){ $_J=self::nextNum($_J); }
  }
  $_J[0]='i'; $_J[1]=self::$tbk; $_J[2]='ud';
  $qI=[$_J];
  if(!_err::$err){
    $qF=a_sql::fetch('SELECT id FROM cyp_con1 WHERE docEntry=\''.$_J['otr'].'\' AND lineNum=\''.$_J['lineNum'].'\' LIMIT 1',[1=>'Error verificando cuota a aplicar mora',2=>'La cuota '.$_J['lineNum'].' no existe en el contrato']);
    if(a_sql::$err){ _err::err(a_sql::$errNoText); }
    else{
      $balTotal =$_J['docTotal']*1;
      $qI[]=['p','UPDATE cyp_con1 SET lineTotal=lineTotal+'.$balTotal.', balDue=balDue+'.$balTotal.', balRec=balRec+'.$balTotal.' WHERE id=\''.$qF['id'].'\' LIMIT 1'];
      $qI[]=['p','UPDATE cyp_ocon SET docTotal=docTotal+'.$balTotal.', balDue=balDue+'.$balTotal.', recTotal=recTotal+'.$balTotal.' WHERE docEntry=\''.$_J['otr'].'\' LIMIT 1'];
    }
  }
	if(!_err::$err){ a_sql::multiQuery($qI); }
	if(!_err::$err){ $c=true; $js=_js::r('Recargo registrado correctamente','"docEntry":"'.$docEntryN.'"');
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function statusN($_J=array()){
	_ADMS::lib('iDoc');
	 a_sql::transaction(); $c=false;
   iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'serieType'=>self::$serie, 'log'=>self::$tbk99,'sreqMemo'=>'Y','lineMemo'=>$_J['lineMemo'],'fie'=>'otr,lineNum,docTotal','D'=>'Y'));
	if(!_err::$err){
    $qD=a_sql::fetch('SELECT A.docTotal bal,D1.balPay,D1.cuoV,D1.balRec
		FROM cyp_orec A
		JOIN cyp_con1 D1 ON (D1.docEntry=A.otr AND D1.lineNum=A.lineNum)
		JOIN cyp_ocon D ON (D.docEntry=D1.docEntry)
    WHERE A.docEntry=\''.$_J['docEntry'].'\' ',[1=>'Error revisando consistencia de la anulación']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
    if($qD['balPay'] > ($qD['cuoV']+$qD['balRec']-$qD['bal'])){ _err::err('No se puede anular el recargo, ya fue pagado total o parcialmente.',3); }
    if(!_err::$err){
      $qI=[];
      $balTotal =iDoc::$D['docTotal']*1;
      $qI[]=['p','UPDATE cyp_con1 SET lineTotal=lineTotal-'.$balTotal.', balDue=balDue-'.$balTotal.', balRec=balRec-'.$balTotal.' WHERE docEntry=\''.iDoc::$D['otr'].'\' AND lineNum=\''.iDoc::$D['lineNum'].'\' LIMIT 1'];
      $qI[]=['p','UPDATE cyp_ocon SET docTotal=docTotal-'.$balTotal.', balDue=balDue-'.$balTotal.', recTotal=recTotal-'.$balTotal.' WHERE docEntry=\''.iDoc::$D['otr'].'\' LIMIT 1'];
      a_sql::multiQuery($qI);
    }
	}
	if(!_err::$err){ $c=true; $js=_js::r('Documento anulado correctamente.'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
}
?>