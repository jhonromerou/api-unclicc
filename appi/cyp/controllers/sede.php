<?php
class cypSede{
	static public function get($D=[]){
    $gb='A.vid,A.docOpen,A.name,A.prsAssg,A.balIni,A.phone1,A.address,A.userUpd,A.dateUpd';
    $D['from']=$gb.',SUM(B.debBal-B.creBal) balTotal
    FROM cyp_osed A
    LEFT JOIN cyp_sed1 B ON (B.sucSede=A.vid AND B.canceled=\'N\')
    ';
    $D['gBy']=$gb;
    return a_sql::rPaging($D);
  }
  static public function getForm($D){
    if($js=_js::ise($D['vid'],'No se ha definido Id de la sede.','numeric>0')){ die($js); }
    $M=a_sql::fetch('SELECT A.*
    FROM cyp_osed  A 
    WHERE A.vid=\''.$D['vid'].'\' '.$whSlp.' LIMIT 1',[1=>'error obteniendo sede',2=>'La sede no existe']);
    if(a_sql::$err){ die(a_sql::$errNoText); }
    return _js::enc2($M);
  }

  static public function post($D){
    if(_js::iseErr($D['name'],'Nombre debe estar definido')){}
    else if(_js::iseErr($D['docOpen'],'Fecha de apertura debe estar definida')){}
    else if(_js::iseErr($D['balIni'],'El Saldo inicial debe definirse','numeric>')){}
    else{
      $D['vid']=a_sql::qInsert($D,array('tbk'=>'cyp_osed','qku'=>'ud'));
      if(a_sql::$err){ _err::err('Error guardando información: '.a_sql::$errText,3); }
      else{
        self::saldoIni(['sucSede'=>$D['vid'],'tt'=>'cypSed','tr'=>$vid,'debBal'=>$D['balIni'],'docDate'=>$D['docOpen']]);
        if(!_err::$err){
          $o=array('vid'=>$D['vid'],'v'=>$D['value']);
          $js=_js::r('Información guardada correctamente.',$o);
        }
      }
    }
    _err::errDie();
    echo $js;
  }
  static public function put($D){
    if(_js::iseErr($D['vid'],'Se debe definir ID de sede')){}
    else if(_js::iseErr($D['name'],'Nombre debe estar definido')){}
    else if(_js::iseErr($D['docOpen'],'Fecha de apertura debe estar definida')){}
    else if(_js::iseErr($D['balIni'],'El Saldo inicial debe definirse','numeric>')){}
    else{
      $vid=$D['vid'];
      $ins=a_sql::uniRow($D,array('tbk'=>'cyp_osed','qku'=>'ud','wh_change'=>'vid=\''.$vid.'\' LIMIT 1'));
      if(a_sql::$err){ _err::err('Error guardando información: '.a_sql::$errText,3); }
      else{
        self::saldoIni(['sucSede'=>$vid,'tt'=>'cypSed','tr'=>$vid,'debBal'=>$D['balIni'],'docDate'=>$D['docOpen']]);
        if(!_err::$err){
          $D['vid']=($ins['insertId'])?$ins['insertId']:$vid;
          $o=array('vid'=>$D['vid'],'v'=>$D['name']);
          $js=_js::r('Información guardada correctamente.',$o);
        }
      }
    }
    _err::errDie();
    return $js;
  }
  static public function statusN($D){
    a_sql::query('UPDATE cyp_sed1 SET canceled=\'Y\' WHERE tt=\''.$D['tt'].'\' AND tr=\''.$D['tr'].'\'',[1=>'Error anulando movimiento de cuenta']);
    if(a_sql::$err){ _err::err(a_sql::$errNoText); }
  }

  static public function history($D){
    $D['from']='A.tt,A.tr,A.sucSede,A.debBal,A.creBal,A.docDate,A.cardId,A.slpId,A.logRoute,C.cardName
    FROM cyp_sed1 A
    LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
    ';
    $D['wh']='A.canceled=\'N\'';
    return a_sql::rPaging($D);
  }
  static public function historyFieldsAllow($D){
    $X=[
			'xaccId'=>1,
			'tt'=>1,'tr'=>1,'serieId'=>1,'docNum'=>1,
			'docDate'=>1,
			'sucSede'=>1,'cardId'=>1,'slpId'=>1,'logRoute'=>1,'categId'=>1,
      'debBal'=>1,'creBal'=>1,'credBal'=>1,
      'lineMemo'=>1
		];
    foreach($D as $k=>$v){
      if(!$X[$k]){ unset($D[$k]); }
    }
    return $D;
  }
	static public function historyPost($D=[],$P=[]){
		a_sql::qInsert([
			'xaccId'=>$D['xaccId'],
			'tt'=>$P['tt'],'tr'=>$P['tr'],'serieId'=>$D['serieId'],'docNum'=>$D['docNum'],
			'docDate'=>$D['docDate'],
			'sucSede'=>$D['sucSede'],'cardId'=>$D['cardId'],'slpId'=>$D['slpId'],'logRoute'=>$D['logRoute'],
      'categId'=>$D['categId'],
      'debBal'=>$P['debBal'],'creBal'=>$P['creBal'],
      'lineMemo'=>$D['lineMemo']
		],['tbk'=>'cyp_sed1','qk'=>'ud']);
		if(a_sql::$err){ _err::err('Error registrando movimiendo de dinero: '.a_sql::$errText,3); }
  }
  static public function historyPut($D=[],$P=[]){
    $Dx=self::historyFieldsAllow($D);
		a_sql::qUpdate($Dx,['tbk'=>'cyp_sed1','qk'=>'ud','wh_change'=>'tt=\''.$P['tt'].'\' AND tr=\''.$P['tr'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err('Error actualizando movimiendo de dinero: '.a_sql::$errText,3); }
  }
  static public function historyRow($D=[]){
		a_sql::uniRow([
			'xaccId'=>$D['xaccId'],
			'tt'=>$D['tt'],'tr'=>$D['tr'],'serieId'=>$D['serieId'],'docNum'=>$D['docNum'],
			'docDate'=>$D['docDate'],
			'sucSede'=>$D['sucSede'],'cardId'=>$D['cardId'],'slpId'=>$D['slpId'],
      'categId'=>$D['categId'],
      'debBal'=>$D['debBal'],'creBal'=>$D['creBal'],
      'lineMemo'=>$D['lineMemo']
		],['tbk'=>'cyp_sed1','qk'=>'ud','wh_change'=>'tt=\''.$D['tt'].'\' AND tr=\''.$D['tr'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err('Error registrando movimiendo de dinero: '.a_sql::$errText,3); }
  }
  static public function saldoIni($D=[],$P=[]){
    unset($D['serieId'],$D['docNum']);
		a_sql::uniRow($D,
    ['tbk'=>'cyp_sed1','qk'=>'ud','wh_change'=>'sucSede=\''.$D['sucSede'].'\' AND tt=\''.$D['tt'].'\' AND tr=\''.$D['tr'].'\' LIMIT 1']);
    if(a_sql::$err){ _err::err('Error actualizando saldos iniciales: '.a_sql::$errText,3); }
	}
}
?>