<?php
class cypEgr extends JxDoc{
static $serie='cypEgr';
static $AI='id';
static $tbk='cyp_sed1';
static $tbk99='cyp_doc99';
static public function get($D){
	_ADMS::lib('iDoc,sql/filter');
	$D['from']='A.id,A.docStatus,A.tt,A.tr,A.tr,A.serieId,A.docNum,A.docDate,A.canceled,A.creBal,A.sucSede,A.slpId,A.logRoute,A.categId,C.licTradNum,C.cardName,A.userId,A.lineMemo
	FROM '.self::$tbk.' A
  LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
  $D['wh']='A.tt=\''.self::$serie.'\'';
	return a_sql::rPaging($D);
}
static public function getOne($D){
	if($js=_js::ise($D['id'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT A.*,C.licTradType,C.licTradNum,C.cardName
	FROM '.self::$tbk.' A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE A.id=\''.$D['id'].'\' '.$whSlp.' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	return _js::enc2($M);
}
static public function fieldsRequire(){
  return [
		['k'=>'sucSede','ty'=>'id','iMsg'=>'Sucursal'],
    ['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
    ['k'=>'creBal','ty'=>'numeric>0','iMsg'=>'Valor retiro'],
    ['k'=>'lineMemo','maxLen'=>500,'iMsg'=>'Descripcion'],
  ];
}
static public function post($_J=array()){
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
		$_J['id']=self::nextID();
		$js=self::save($_J);
  }
	if(!_err::$err){ $c=true;  a_sql::transaction($c);
		self::tb99P(['docEntry'=>$_J['id'],'dateC'=>1]);
	}
	_err::errDie();
	return $js;
}
static public function put($_J=array()){
	a_sql::transaction(); $c=false;
	if(_js::iseErr($_J['id'],'Id de transaccion debe definirse','numeric>0')){}
	else{
		_ADMS::lib('JLog,iDoc');
		iDoc::vStatus(['tbk'=>self::$tbk,'docEntryAlias'=>'id','docEntry'=>$_J['id'],'closeOmit'=>'Y','reqMemo'=>'Y','lineMemo'=>$_J['lineMemoUpd'],
		'setLog'=>['tbk'=>self::$tbk99,'serieType'=>self::$serie,'dateUpd'=>1,'lineMemo'=>$_J['lineMemoUpd']]]);
	}
	unset($_J['lineMemoUpd']);
	if(!_err::$err){
		self::formRequire($_J);
		$js=self::save($_J); if(!_err::$err){ $c=true; }
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function save($_J=array()){
  if(!_err::$err){
    require('sede.php');
    $_J['tt']=self::$serie; $_J['tr']=$_J['id'];
    //$_J['debBal']=$_J['docTotal'];
		cypSede::historyRow($_J);
	}
	if(!_err::$err){ $js=_js::r('Retiro registrado correctamente','"docEntry":"'.$_J['id'].'"');
	}
	return $js;
}
static public function statusN($_J=array()){
	_ADMS::lib('JLog,iDoc');
	a_sql::transaction(); $c=false;
	iDoc::putStatus(['t'=>'N','tbk'=>self::$tbk,'docEntryAlias'=>'id','docEntry'=>$_J['docEntry'],'closeOmit'=>'Y','serieType'=>self::$serie,'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$_J['lineMemo']]);
	if(!_err::$err){
		require('sede.php');
		cypSede::statusN(['tt'=>self::$serie,'tr'=>$_J['docEntry']]);
		if(!_err::$err){ $c=true; $js=_js::r('Documento anulado correctamente.'); }
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
}
?>