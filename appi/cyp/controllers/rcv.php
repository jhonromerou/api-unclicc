<?php
class cypRcv extends JxDoc{
static $serie='cypRcv';
static $AI='docEntry';
static $tbk='cyp_orcv';
static $tbk1='cyp_rcv1';
static $tbk99='cyp_doc99';
static public function get($D){
	_ADMS::lib('iDoc,sql/filter');
	$D['from']='A.docEntry,A.serieId,A.docNum,A.docDate,A.docStatus,A.canceled,A.dateC,A.userId,A.docTotal,A.sucSede,A.slpId,A.logRoute,A.lineMemo,C.licTradNum,C.cardName
	FROM '.self::$tbk.' A
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	$D['permsBy']='lgcs'; 
	return a_sql::rPaging($D);
}
static public function getOne($D){
	if($js=_js::ise($D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT A.*,C.licTradType,C.licTradNum,C.cardName
	FROM '.self::$tbk.' A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE A.docEntry=\''.$D['docEntry'].'\' '.$whSlp.' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	//else if(a_ses::uspLgc($M['slpId'])){ die(_err::$errText); }
	else{
		$M['L']=a_sql::fetchL('SELECT D.docEntry,D1.lineNum,B.bal,D1.lineDue,D1.balDue,D1.cuoV,D1.balRec
		FROM cyp_rcv1 B 
		LEFT JOIN cyp_con1 D1 ON (D1.id=B.cuoid)
		LEFT JOIN cyp_ocon D ON (D.docEntry=D1.docEntry)
		WHERE B.docEntry=\''.$D['docEntry'].'\' ORDER BY B.lineNum ASC',[1=>'error obteniendo lineas documento',2=>'El documento no tiene lineas registradas']);
	}
	return _js::enc2($M);
}
static public function fieldsRequire(){
  return [
		['k'=>'cardId','ty'=>'id','iMsg'=>'Cliente'],
		['k'=>'slpId','ty'=>'id','iMsg'=>'Cobrador'],
		['k'=>'logRoute','ty'=>'id','iMsg'=>'Ruta'],
		['k'=>'sucSede','ty'=>'id','iMsg'=>'Sede'],
    ['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
    ['k'=>'docTotal','ty'=>'numeric>0','iMsg'=>'Valor Recibido'],
    ['k'=>'lineMemo','maxLen'=>500,'iMsg'=>'Descripcion'],
    ['ty'=>'L','k'=>'L','req'=>'Y','iMsg'=>'Pago','L'=>[
      ['k'=>'cuoid','ty'=>'id','iMsg'=>'ID de cuota'],
      //['k'=>'bal','ty'=>'numeric>0','iMsg'=>'Valor a pagar']
    ]
    ]
  ];
}
static public function post($_J=array()){
	_ADMS::libC('cyp','sede,route');
	$Dx=cypRoute::getInf($_J); _err::errDie();
	$_J['sucSede']=$Dx['sucSede'];
	self::formRequire($_J);
  unset($_J['docEntry']);
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
		$_J['docEntry']=$docEntryN=self::nextID();
  }
  $qI=[];
  if(!_err::$err){
		cypSede::historyPost($_J,['tt'=>self::$serie,'tr'=>$docEntryN,'debBal'=>$_J['docTotal'],'creBal'=>0,'slpId'=>$_J['slpId']]);
	}
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
    $_J[0]='i'; $_J[1]=self::$tbk; $_J[2]='udUpd';
    $ln=0;
		$qI[]=$_J; $docTotal=0;
    foreach($Lx as $n=>$L){
			if($L['payTo']=='N'){ continue; }
			$lnt='Contrato #'.$L['docEntry'].' cuota '.$L['lineNum'].': ';
			if(_js::iseErr($L['bal'],$lnt.'El valor debe estar definido y ser mayor de 0','numeric>0')){ break; }
			$qBal=a_sql::fetch('SELECT A.sucSede,B.balDue 
			FROM cyp_con1 B 
			JOIN cyp_ocon A ON (A.docEntry=B.docEntry)
			WHERE B.id=\''.$L['cuoid'].'\' LIMIT 1',[1=>$lnt.'Error verificando cuota',2=>$lnt.'La cuota a aplicar el pago no existe']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); break; }
			else if($qBal['balDue']<$L['bal']){ _err::err($lnt.'El valor a pagar ('.$L['bal'].') es mayor al pendiente ('.($qBal['balDue']*1).')',3); break; }
			//else if(_js::iseErr($qBal['sucSede'],$lnt.'Sucursal del contrato no definida')){ break; }
			$ln++; unset($L['payTo']);
			$odocEntry=$L['docEntry'];//documento
			unset($L['docEntry']);//id cuota
			$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
      $L['lineNum']=$ln;
      $docTotal +=$L['bal'];
			$qI[]=$L;
			$qI[]=['u','cyp_ocon','_unik'=>'docEntry','docEntry'=>$odocEntry,'balDue='=>'balDue-'.$L['bal'],'balPay='=>'balPay+'.$L['bal']];
			$qI[]=['u','cyp_con1','_unik'=>'id','id'=>$L['cuoid'],'balDue='=>'balDue-'.$L['bal'],'balPay='=>'balPay+'.$L['bal']];
		}
		if(!_err::$err && $docTotal!=$_J['docTotal']){ _err::err('El valor de los pagos no puede ser diferente al valor recibido',3); }
	}
	//_err::errDie(); die(print_r(a_sql::toQuerys($qI)));
	if(!_err::$err){ a_sql::multiQuery($qI); }
	if(!_err::$err){ $c=true; $js=_js::r('Pago registrado correctamente','"docEntry":"'.$docEntryN.'"');
		self::tb99P(['docEntry'=>$docEntryN,'dateC'=>1]);
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function statusN($_J=array()){
	_ADMS::lib('iDoc');
	 a_sql::transaction(); $c=false;
	 iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'serieType'=>self::$serie, 'log'=>self::$tbk99,'sreqMemo'=>'Y','lineMemo'=>$_J['lineMemo']));
	if(!_err::$err){
		$qD=a_sql::query('SELECT B.cuoid,D.docEntry,B.bal
		FROM '.self::$tbk1.' B
		JOIN cyp_con1 D1 ON (D1.id=B.cuoId)
		JOIN cyp_ocon D ON (D.docEntry=D1.docEntry)
		WHERE B.docEntry=\''.$_J['docEntry'].'\' ',[1=>'Error revertiendo los pagos aplicados']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			require('sede.php');
			cypSede::statusN(['tt'=>self::$serie,'tr'=>$_J['docEntry']]);
			if(!_err::$err){
			$qI=[];
			while($L=$qD->fetch_assoc()){
				$L['bal'] *=1;
				$balDue='balDue+'.$L['bal'];
				$qI[]=['u','cyp_ocon','_unik'=>'docEntry','docEntry'=>$L['docEntry'],'balDue='=>'IF('.$balDue.'<=docTotal,'.$balDue.',balDue)','balPay='=>'balPay-'.$L['bal']];
				$qI[]=['u','cyp_con1','_unik'=>'id','id'=>$L['cuoid'],'balDue='=>'IF('.$balDue.'<=lineTotal,'.$balDue.',balDue)','balPay='=>'balPay-'.$L['bal']];
			}
			a_sql::multiQuery($qI);
			}
		}
	}
	if(!_err::$err){ $c=true; $js=_js::r('Documento anulado correctamente.'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
}
?>