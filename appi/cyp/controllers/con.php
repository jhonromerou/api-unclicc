<?php
class cypCon extends JxDoc{
static $serie='cypCon';
static $AI='docEntry';
static $tbk='cyp_ocon';
static $tbk1='cyp_con1';
static $tbk99='cyp_doc99';
static public function get($D){
	_ADMS::lib('iDoc,sql/filter');
	$D['from']='A.docEntry,A.serieId,A.docNum,A.docDate,A.dueDate,A.docStatus,A.canceled,A.docType,A.docClass,A.dateC,A.userId,A.dateUpd,A.userUpd,A.cuoV,A.iTasa,A.vPre,A.vFut,A.balDue,A.cuoNum,A.cuoUdm,A.logRoute,A.slpId,C.licTradNum,C.cardName
	FROM '.self::$tbk.' A
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	$D['permsBy']='lgcs';
	return a_sql::rPaging($D);
}
static public function getOne($D){
	if($js=_js::ise($D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	if($D['_mod']=='Y'){ cypCon::rev2Null($D,'No se puede modificar el contrato.'); }
	_err::errDie();
	$M=a_sql::fetch('SELECT A.*,C.licTradType,C.licTradNum,C.cardName
	FROM '.self::$tbk.' A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE A.docEntry=\''.$D['docEntry'].'\' '.$whSlp.' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	//else if(!a_ses::uspLgc($M['lgcId'])){ die(_err::$err); }
	else{
		$M['L']=a_sql::fetchL('SELECT B.*
		FROM '.self::$tbk1.' B 
		WHERE B.docEntry=\''.$D['docEntry'].'\' ORDER BY B.lineNum ASC',[1=>'error obteniendo lineas documento',2=>'El documento no tiene lineas registradas']);
	}
	return _js::enc2($M);
}
static public function fieldsRequire(){
  return [
		['k'=>'cardId','ty'=>'id','iMsg'=>'Tercero'],
		['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
		['k'=>'slpId','ty'=>'id','iMsg'=>'Cobrador'],
		['k'=>'logRoute','ty'=>'id','iMsg'=>'Ruta'],
		['k'=>'sucSede','ty'=>'id','iMsg'=>'Sede'],
		['k'=>'vPre','ty'=>'id','iMsg'=>'Monto solicitado'],
		['k'=>'docType','ty'=>'R','iMsg'=>'Tipo'],
		['k'=>'iTasa','ty'=>'id','iMsg'=>'Interes'],
		['k'=>'cuoNum','ty'=>'numeric>0','iMsg'=>'No. Cuotas'],
    ['k'=>'cuoUdm','ty'=>'R','iMsg'=>'Frecuencia'],
    ['k'=>'cuoV','ty'=>'id','iMsg'=>'Valor Cuota'],
    ['k'=>'vFut','ty'=>'id','iMsg'=>'Total de prestamo'],
    ['k'=>'lineMemo','maxLen'=>1000,'iMsg'=>'Descripcion'],
    ['ty'=>'L','k'=>'L','req'=>'Y','iMsg'=>'Cuotas','L'=>[
      ['k'=>'lineDue','ty'=>'R','iMsg'=>'Vencimiento'],
      ['k'=>'cuoV','ty'=>'id','iMsg'=>'Valor Cuota'],
      ['k'=>'cuoInt','ty'=>'num','iMsg'=>'Valor interes de cuota'],
      ['k'=>'cuoCap','ty'=>'num','iMsg'=>'Valor capital de cuota'],
      ['k'=>'capAt','ty'=>'num','iMsg'=>'Saldo capital de cuota'],
      ['k'=>'cuoSaldo','ty'=>'num','iMsg'=>'Pendiente de cuota']
    ]
    ]
  ];
}
static public function post($_J=array()){
	_ADMS::libC('cyp','sede,route');
	$Dx=cypRoute::getInf($_J); _err::errDie();
	$_J['sucSede']=$Dx['sucSede'];
  unset($_J['docEntry']);
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
		$_J['docEntry']=$docEntryN=self::nextID();
		//if(!_err::$err){ $_J=self::nextNum($_J); }
  }
  $qI=[];
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
    $_J[0]='i'; $_J[1]=self::$tbk; $_J[2]='udUpd';
    $ln=0;
    $vFut=0;
    foreach($Lx as $n=>$L){
			$ln++;
      $L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
			$L['lineNum']=$ln;
			$L['lineTotal']=$L['balDue']=$L['cuoV'];
      $vFut +=$L['cuoV'];
      $qI[]=$L;
		}
		$_J['docTotal']=$_J['balDue']=$_J['vFut'];
		$_J['intTotal']=$_J['vFut']-$_J['vPre'];
		if(!_err::$err && $vFut!=$_J['vFut']){ _err::err('El total de las cuotas ('.$vFut.') es diferente al valor futuro ('.$_J['vFut'].')',3); }
		if(!_err::$err){
			cypSede::historyPost($_J,['tt'=>self::$serie,'tr'=>$docEntryN,'creBal'=>$_J['vPre'],'debBal'=>0]);
		}
		if(!_err::$err){ $qI[]=$_J; a_sql::multiQuery($qI); }
  }
	if(!_err::$err){ $c=true; $js=_js::r('Contrato registrado correctamente','"docEntry":"'.$docEntryN.'"');
		self::tb99P(['docEntry'=>$docEntryN,'dateC'=>1]);
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function put($_J=array()){
	_ADMS::libC('cyp','sede,route');
	$Dx=cypRoute::getInf($_J); _err::errDie();
	$_J['sucSede']=$Dx['sucSede'];
  $docEntryN=$_J['docEntry'];
	self::formRequire($_J);
	$qI=[];
	_ADMS::lib('iDoc,JLog');
	a_sql::transaction(); $c=false;
	if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$_J['docEntry']))){}
  if(!_err::$err){ self::rev2Null($_J,'No se puede modificar el contrato.'); }
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L'],$_J['docEntry']);
		$ln=0;
		//eliminar antes de agregar
		$qI[]=['p','DELETE FROM cyp_con1 WHERE docEntry=\''.$docEntryN.'\' '];
    foreach($Lx as $n=>$L){
			$ln++;
			$L['lineNum']=$ln;
			$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
			$L['lineTotal']=$L['balDue']=$L['cuoV'];
			$vFut +=$L['cuoV'];
      $qI[]=$L;
		}
		$_J['docTotal']=$_J['balDue']=$_J['vFut'];
		$_J['intTotal']=$_J['vFut']-$_J['vPre'];
		if(!_err::$err && $vFut!=$_J['vFut']){ _err::err('El total de las cuotas ('.$vFut.') es diferente al valor futuro ('.$_J['vFut'].')',3); }
		if(!_err::$err){
			$_J['creBal']=$_J['vPre']; $_J['debBal']=0;
			cypSede::historyPut($_J,['tt'=>self::$serie,'tr'=>$docEntryN]);
			unset($_J['creBal'],$_J['debBal']);
		}
		$_J[0]='u'; $_J[1]=self::$tbk; $_J[2]='udU';
		$_J['_wh']='docEntry=\''.$docEntryN.'\' LIMIT 1';
		if(!_err::$err){ $qI[]=$_J;  a_sql::multiQuery($qI); }
  }
	if(!_err::$err){ $c=true; $js=_js::r('Documento actualizado correctamente','"docEntry":"'.$docEntryN.'"'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function statusN($_J=array()){
	_ADMS::lib('iDoc');
	$js=''; a_sql::transaction(); $cmt=false;
	iDoc::putStatus(array('log'=>self::$tbk99,'closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'serieType'=>self::$serie,'lineMemo'=>$_J['lineMemo']));
	if(!_err::$err){ self::rev2Null($_J,'No se puede anular el contrato.'); }
	if(!_err::$err){ $cmt=true; $js=_js::e(3,'Documento anulado correctamente.'); }
	a_sql::transaction($cmt);
	_err::errDie();
	return $js;
}
static public function rev2Null($_J=[],$errText=''){
	$qD=a_sql::fetch('SELECT D.docEntry
	FROM cyp_ocon A
	JOIN cyp_con1 B ON (B.docEntry=A.docEntry)
	JOIN cyp_rcv1 D1 ON (D1.cuoid=B.id)
	JOIN cyp_orcv D ON (D.docEntry=D1.docEntry)
	WHERE A.docEntry=\''.$_J['docEntry'].'\' AND D.canceled=\'N\' LIMIT 1',[1=>'Error revisando si contrato tiene pagos activos.']);
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1){ _err::err($errText.' Tiene pagos activos relacionados (#'.$qD['docEntry'].').',3); }
	if(!_err::$err){
		$qD=a_sql::fetch('SELECT D.docEntry
		FROM cyp_ocon A
		JOIN cyp_con1 B ON (B.docEntry=A.docEntry)
		JOIN cyp_orec D ON (D.otr=A.docEntry AND D.lineNum=B.lineNum)
		WHERE A.docEntry=\''.$_J['docEntry'].'\' AND D.canceled=\'N\' LIMIT 1',[1=>'Error revisando si contrato recargos activos.']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){ _err::err($errText.' Tiene recargos activos relacionados (#'.$qD['docEntry'].').',3); }
	}
}

static public function topay($D){
	if($js=_js::ise($D['cardId'],'Se debe definir el cliente.','numeric>0')){ die($js); }
	return a_sql::fetchL('SELECT A.docEntry,A.vPre,A.cuoNum,A.cuoUdm,
	B.id,B.lineNum,B.lineDue,B.balDue,B.balRec,B.cuoV
	FROM '.self::$tbk.' A 
	JOIN '.self::$tbk1.' B ON (B.docEntry=A.docEntry)
	WHERE A.cardId=\''.$D['cardId'].'\' AND A.canceled=\'N\' AND B.balDue>0 ORDER BY A.docEntry ASC,B.lineNum ASC',['k'=>'L',1=>'Error obteniendo contratos abiertos',2=>'El cliente no tiene contratos abiertos'],true);
}

}
?>