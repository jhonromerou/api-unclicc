<?php
class cypRoute{
  static public function get($D=[]){
    $gb='A.*';
    $D['from']=$gb.'
    FROM cyp_orou A';
    return a_sql::rPaging($D);
  }
  static public function getInf($D=[]){
    $ori=' on cypRoute::getInf();';
    if(_js::iseErr($D['logRoute'],'Se debe definir ID de ruta.'.$ori,'numeric>0')){}
    else{
      $qR=a_sql::fetch('SELECT vid,sucSede FROM cyp_orou WHERE vid=\''.$D['logRoute'].'\' LIMIT 1',[1=>'Error obteniendo datos de ruta.'.$ori,2=>'La ruta no existe.'.$ori]);
      if(a_sql::$err){ _err::err(a_sql::$errNoText); }
      else{ return $qR; }
    }
  }
  static public function oneGet($D){
    if($js=_js::ise($D['vid'],'No se ha definido Id de la ruta.','numeric>0')){ die($js); }
    $M=a_sql::fetch('SELECT A.*
    FROM cyp_orou  A 
    WHERE A.vid=\''.$D['vid'].'\' '.$whSlp.' LIMIT 1',[1=>'error obteniendo ruta',2=>'La ruta no existe']);
    if(a_sql::$err){ die(a_sql::$errNoText); }
    return _js::enc2($M);
  }
  static public function post($D){
    if(_js::iseErr($D['routeName'],'Nombre debe estar definido')){}
    else if(_js::iseErr($D['sucSede'],'Debe asignar la ruta a una sede','numeric>0')){}
    else{
      $D['vid']=a_sql::qInsert($D,array('tbk'=>'cyp_orou'));
      if(a_sql::$err){ _err::err('Error guardando información: '.a_sql::$errText,3); }
      else{
        if(!_err::$err){
          $o=array('vid'=>$D['vid'],'v'=>$D['routeName'],'sucSede'=>$D['sucSede']);
          $js=_js::r('Información guardada correctamente.',$o);
        }
      }
    }
    _err::errDie();
    echo $js;
  }
  static public function put($D){
    if(_js::iseErr($D['vid'],'Se debe definir ID de sede')){}
    else if(_js::iseErr($D['routeName'],'Nombre debe estar definido')){}
    else if(_js::iseErr($D['sucSede'],'Debe asignar la ruta a una sede','numeric>0')){}
    else{
      $qa=a_sql::query('SELECT id FROM cyp_sed1 WHERE logRoute=\''.$D['vid'].'\' AND sucSede!=\''.$D['sucSede'].'\' AND canceled=\'N\' LIMIT 1',[1=>'Error verificando modificación de ruta']);
      if(a_sql::$err){ _err::err(a_sql::$errNoText); }
      else if(a_sql::$errNo==-1){ _err::err('La ruta tiene registrados movimientos en una sede diferente a la que está definiendo. Debe crear una ruta nueva.',3); }
    }
    if(!_err::$err){
      $ins=a_sql::qUpdate($D,array('tbk'=>'cyp_orou','wh_change'=>'vid=\''.$D['vid'].'\' LIMIT 1'));
      if(a_sql::$err){ _err::err('Error guardando información: '.a_sql::$errText,3); }
      else{
        if(!_err::$err){
          $o=array('vid'=>$D['vid'],'v'=>$D['routeName'],'sucSede'=>$D['sucSede']);
          $js=_js::r('Información guardada correctamente.',$o);
        }
      }
    }
    _err::errDie();
    return $js;
  }
}
?>