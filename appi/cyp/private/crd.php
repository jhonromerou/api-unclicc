<?php
JRoute::get('cyp/crd',function(){
	$wh=''; _ADMS::_lb('sql/filter');
	if($_GET['textSearch']!=''){
		$se=a_sql::toSe($_GET['textSearch']); $wh='AND (A.cardName '.$se.' OR A.cardCode '.$se.')';
	} unset($_GET['textSearch']);
	$wh .= a_ses::ousp('slps',array('tbA'=>'A'));
	$wh .= a_sql_filtByT($_GET);
	$q=a_sql::query('SELECT A.cardId,A.licTradType,A.grId,A.slpId,A.licTradNum,A.cardName,phone1,phone2,cellular,email,A.logRoute,A.sucSede FROM par_ocrd A WHERE 1 '.$wh.' '.a_sql::nextLimit(),array(1=>'Error obteniendo listado de contactos: ',2=>'No se encontraron contactos.'));
	if(a_sql::$errNoText!=''){ $js=a_sql::$errNoText; }
	else{$M=array('L'=>array());
		while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
		$js=_js::enc($M);
	}
	echo $js;
});
JRoute::get('cyp/crd/form',function(){
	$q=a_sql::fetch('SELECT * FROM par_ocrd WHERE cardId=\''.$_GET['cardId'].'\' LIMIT 1',array(1=>'Error obteniendo información del contacto: ',2=>'El contacto no existe ID:'.$_GET['cardId']));
	if(($js=a_sql::$errNoText)!=''){}
	else{
		$js=_js::enc2($q);
	}
	echo $js;
});
JRoute::post('cyp/crd',function($J){
	if($js=_js::ise($J['cardType'],'No se ha definido el tipo.')){}
	else if($js=_js::ise($J['cardName'],'El nombre debe estar definido.')){}
	else if(!_js::textLimit($J['cardName'],100)){ $js=_js::e(3,'El nombre no puede exceder 100 caracteres.'); }
	else{
		$errs==0;
		if($errs==0){
			a_sql::transaction(); $cmt=false;
			$cardId=$J['cardId']; unset($J['cardId']);
			$J['RF_mmag']=$J['cityCode'];
			$ins=a_sql::uniRow($J,array('tbk'=>'par_ocrd','qk'=>'ud','wh_change'=>'cardId=\''.$cardId.'\' LIMIT 1'));
		if(a_sql::$err){ $js=_err::err(a_sql::$errText,3); $errs++; }
			else{
				$cardId=($ins['insertId'])?$ins['insertId']:$cardId;
				$jsAdd='"cardId":"'.$cardId.'"';
			}
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Información guardada correctamente.',$jsAdd);
		}
		a_sql::transaction($cmt);
	}
	echo $js;
});
JRoute::put('cyp/crd',function($J){
	if($js=_js::ise($J['cardType'],'No se ha definido el tipo.')){}
	else if($js=_js::ise($J['cardName'],'El nombre debe estar definido.')){}
	else if(!_js::textLimit($J['cardName'],100)){ $js=_js::e(3,'El nombre no puede exceder 100 caracteres.'); }
	else{
		$errs==0;
		if($errs==0){
			a_sql::transaction(); $cmt=false;
			$cardId=$J['cardId']; unset($J['cardId']);
			$J['RF_mmag']=$J['cityCode'];
			$ins=a_sql::uniRow($J,array('tbk'=>'par_ocrd','qk'=>'ud','wh_change'=>'cardId=\''.$cardId.'\' LIMIT 1'));
			if(a_sql::$err){ $js=_err::err(a_sql::$errText,3); $errs++; }
			else{
				$cardId=($ins['insertId'])?$ins['insertId']:$cardId;
				$jsAdd='"cardId":"'.$cardId.'"';
			}
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Información guardada correctamente.',$jsAdd);
		}
		a_sql::transaction($cmt);
	}
	echo $js;
});
?>