<?php
JRoute::get('cyp/rep/quotes',function($D){
  $M=['_view'=>$D['_view']]; unset($D['_view']);
  if($M['_view']=='D'){
    $D['B.lineDue(E_menIgual)']=date('Y-m-d');
  }
  $M['L']=a_sql::fetchL('SELECT B.*,A.slpId,A.logRoute,A.sucSede,C.licTradNum,C.cardName
  FROM cyp_ocon A
  JOIN cyp_con1 B ON (B.docEntry=A.docEntry)
  LEFT JOIN par_ocrd C ON (C.cardId=A.cardId) 
  WHERE A.canceled=\'N\' '.a_sql_filter($D).'
  ORDER BY B.lineDue ASC,B.lineNum ASC');
  return _js::enc2($M);
},['lib'=>'sql/filter']);
JRoute::get('cyp/rep/contratos',function($D){
  $M=['_view'=>$D['_view']]; unset($D['_view']);
  if($M['_view']=='O'){ $D['A.balDue(E_may)']=0;  $M['_view']='G'; }
  $M['L']=a_sql::fetchL('SELECT A.docEntry,A.docDate,A.vPre,A.vFut,A.intTotal,A.recTotal,A.docTotal,A.balDue,A.slpId,A.cuoNum,A.cuoV,A.cuoUdm,A.logRoute,A.sucSede,C.licTradNum,C.cardName
  FROM cyp_ocon A
  LEFT JOIN par_ocrd C ON (C.cardId=A.cardId) 
  WHERE A.canceled=\'N\' '.a_sql_filter($D).'
  ORDER BY C.cardName ASC,A.docDate ASC');
  return _js::enc2($M);
},['lib'=>'sql/filter']);
JRoute::get('cyp/rep/rcv',function($D){
  $M=['_view'=>$D['_view']]; unset($D['_view']);
  if($M['_view']=='C'){
    $gb='A.slpId,A.logRoute,A.sucSede';
    $M['L']=a_sql::fetchL('SELECT '.$gb.',SUM(A.docTotal) docTotal
		FROM cyp_orcv A
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.canceled=\'N\' '.a_sql_filter($D).' GROUP BY '.$gb.'',[1=>'error obteniendo cobros',2=>'No se encontraron cobros']);
  }
  else if($M['_view']=='S'){
    $gb='A.slpId';
    $M['L']=a_sql::fetchL('SELECT '.$gb.',SUM(A.docTotal) docTotal
		FROM cyp_orcv A
		WHERE A.canceled=\'N\' '.a_sql_filter($D).' GROUP BY '.$gb.'',[1=>'error obteniendo cobros',2=>'No se encontraron cobros']);
  }
  else if($M['_view']=='CC'){
    $gb='A.cardId,C.cardName';
    $M['L']=a_sql::fetchL('SELECT '.$gb.',SUM(A.docTotal) docTotal
		FROM cyp_orcv A
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.canceled=\'N\' '.a_sql_filter($D).' GROUP BY '.$gb.'',[1=>'error obteniendo cobros',2=>'No se encontraron cobros']);
  }
  else{
    $gb='D.docEntry otr,A.slpId,A.logRoute,A.sucSede,D1.lineNum,D1.lineDue,A.docDate,B.bal,C.cardName';
    $M['L']=a_sql::fetchL('SELECT '.$gb.'
		FROM cyp_orcv A
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		JOIN cyp_rcv1 B ON (B.docEntry=A.docEntry)
		LEFT JOIN cyp_con1 D1 ON (D1.id=B.cuoid)
		LEFT JOIN cyp_ocon D ON (D.docEntry=D1.docEntry)
		WHERE A.canceled=\'N\' '.a_sql_filter($D).' ORDER BY A.docDate ASC,D.docEntry ASC,D1.lineNum ASC',[1=>'error obteniendo cobros',2=>'No se encontraron cobros']);
  }
  return _js::enc2($M);
},['lib'=>'sql/filter']);
JRoute::get('cyp/rep/crd',function($D){
  $M=['_view'=>$D['_view']]; unset($D['_view']);
  if($M['_view']=='D'){
    $gb='A.cardId,C.cardName,A.slpId,A.logRoute,A.sucSede,C.phone1,C.cellular,C.email';
    $M['L']=a_sql::fetchL('SELECT '.$gb.',COUNT(A.docEntry) docs,SUM(A.vPre) vPre,SUM(A.balDue) balDue
		FROM cyp_ocon A
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.canceled=\'N\' '.a_sql_filter($D).' GROUP BY '.$gb.'',[1=>'error obteniendo documentos',2=>'No se encontraron prestamos']);
  }
  else{
    $gb='C.cardId,C.cardName,C.slpId,C.logRoute,C.sucSede,C.phone1,C.cellular,C.email';
    $M['L']=a_sql::fetchL('SELECT '.$gb.'
		FROM par_ocrd C
		WHERE C.cardType=\'C\' '.a_sql_filter($D),[1=>'error obteniendo documentos',2=>'No se encontraron prestamos']);
  }
  return _js::enc2($M);
},['lib'=>'sql/filter']);
JRoute::get('cyp/rep/rec',function($D){
  $M=['_view'=>$D['_view']]; unset($D['_view']);
  if($M['_view']=='P'){ $M['_view']='G'; //misma plantilla
    $gb='A.cardId,C.cardName,A.slpId,A.logRoute,A.sucSede,D.otr,D.lineNum,B.cuoV,B.lineDue,D.docTotal,D.docDate,D.docClass,B.balDue,B.lineTotal,B.balPay,D.lineMemo';
    $M['L']=a_sql::fetchL('SELECT '.$gb.'
		FROM cyp_orec D
    JOIN cyp_con1 B ON (B.docEntry=D.otr AND B.lineNum=D.lineNum)
    JOIN cyp_ocon A ON (A.docEntry=B.docEntry)
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.canceled=\'N\' AND B.balDue>0 '.a_sql_filter($D).'',[1=>'error obteniendo documentos',2=>'No se encontraron recargos']);
  }
  else{
    $gb='A.cardId,C.cardName,A.slpId,A.logRoute,A.sucSede,D.otr,D.lineNum,B.cuoV,B.lineDue,D.docTotal,D.docDate,D.docClass,B.balDue,B.lineTotal,B.balPay,D.lineMemo';
    $M['L']=a_sql::fetchL('SELECT '.$gb.'
		FROM cyp_orec D
    JOIN cyp_con1 B ON (B.docEntry=D.otr AND B.lineNum=D.lineNum)
    JOIN cyp_ocon A ON (A.docEntry=B.docEntry)
    JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.canceled=\'N\' '.a_sql_filter($D).'',[1=>'error obteniendo documentos',2=>'No se encontraron recargos']);
  }
  return _js::enc2($M);
},['lib'=>'sql/filter']);
?>