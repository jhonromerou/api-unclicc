<?php
include_once c::$V['PATH_libexterna'].'composer/vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;
$payUrl=c::$V['URI_TPD'].'gvt/pyms?docEntry='.$_GET['docEntry'];
if(JRoute::$view=='gvt/sin'){//no cambiar por POST interno
  $sendEmail=(JRoute::$Met=='POST');
  if($sendEmail){ _ADMS::lib('JMail,xCurl');
    $_POST=JRoute::getData();
    if(_js::iseErr($_POST['to'],'El destinatario debe estar definido')){}
    else if(_js::iseErr($_POST['subject'],'El asunto debe estar definido')){}
    _err::errDie();
  }
  $_tempDoc=['basic'=>__DIR__.'/sin.tp1.php'];
  $_tempMail=['basic'=>__DIR__.'/sin.mail1.php'];
  _ADMS::lib('_File,_2d');
  $_HTML=a_sql::fetch('SELECT A.docEntry,A.docNum,A.docDate,A.dueDate,A.cardName,C.licTradNum,C.licTradType,A.address,A.phone1,A.email,A.baseAmnt,A.vatSum,A.rteSum,A.docTotal,A.lineMemo,S.slpName,P.pymName,DO.srCode,DO.noteFix
  FROM gvt_oinv A
  JOIN par_ocrd C ON (C.cardId=A.cardId)
  LEFT JOIN par_oslp S ON (S.slpId=A.slpId)
  LEFT JOIN gfi_opym P ON (P.pymId=A.pymId)
  LEFT JOIN doc_oser DO ON (DO.serieId=A.serieId)
  WHERE A.docEntry=\''.$_GET['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información del documento',2=>'Documento no encontrado']);
  if(a_sql::$err){ _err::err(a_sql::$errNoText); }
  if(!_err::$err){
    $_HTML['docNumText']=$_HTML['srCode'].'-'.$_HTML['docNum'];
    $_HTML['urlPay']=$payUrl;
    $_HTML['o']=a_sql::fetch('SELECT licTradType,licTradNum,ocardName,pbx,address,pbx,mail,logo from a0_mecrd ',[1=>'Error obteniendo información de la empresa',2=>'No se encontró información del negocio.']);
  }
  if(!_err::$err){
    $_HTML['L']=a_sql::fetchL('select I.itemName,B.price,B.quantity,B.priceLine,B.lineText FROM gvt_inv1 B 
    JOIN itm_oitm I ON (I.itemId=B.itemId)
    WHERE B.docEntry=\''.$_GET['docEntry'].'\'',[1=>'Error obteniendo lineas del documento',2=>'Documento no tiene lineas registradas']);
    if(a_sql::$err){ _err::err(a_sql::$errNoText); }
  }
  if(!_err::$err){ $html=JFile::replaceOn($_tempDoc['basic'],$_HTML); }
  if(!_err::$err){
    if($_GET['viewHtmlMail']=='Y'){
      header("Content-type: text/html"); echo JFile::replaceOn($_tempMail['basic'],$_HTML);
    }
    else if($_GET['viewHtml']=='Y'){ header("Content-type: text/html"); echo $html; }
    else{
      error_reporting(E_ERROR|E_PARSE);
      $dompdf = new Dompdf();
      $options=$dompdf->getOptions();
      $options->set(['isRemoteEnabled'=>true,'isPhpEnabled'=>true,'dpi'=>100]);
      $dompdf->setOptions($options);
      $dompdf->loadHtml($html,'letter'); $html='';
      $dompdf->setPaper('letter','portrait');
      $dompdf->render();
      if($sendEmail){
        $Ds=['subject'=>$_POST['subject'],'to'=>$_POST['to'],
        'from'=>$_HTML['o']['mail'],'fromName'=>$_HTML['o']['ocardName'],
        '_Fi'=>[]];
        $_HTML['msgBody']=$_POST['msgBody'];
        $Ds['html'] =JFile::replaceOn($_tempMail['basic'],$_HTML);
        $Ds['_Fi'][] = JFile::tempFile($dompdf->output(),'factura '.$_HTML['docNum'].'.pdf',['_xfname'=>'attachment']);
        $s = JMail::send($Ds);
        JFile::dels($Ds['_Fi']);
        if(!_err::$err){ echo _js::r('Correo enviado correctamente'); }
      }
      else{
        header("Content-type: application/pdf");
        if($_GET['view']){ header("Content-Disposition: inline; filename=factura.pdf"); }
        else{ header("Content-Disposition: attachment; filename=factura.pdf"); }
        echo $dompdf->output();
      }
    }
  }
  _err::errDie();
}
?>