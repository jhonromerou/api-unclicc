<?php
if(!$_HTML){
$_HTML=[
'docEntry'=>$_GET['docEntry'],'docNum'=>'CC-111','docDate'=>'2021-01-01','dueDate'=>'2021-01-16',
'cardName'=>'Geotecnia Ingenieria SAS','licTradType'=>'NIT','licTradNum'=>'1125082294',
'address'=>'Calle 43 #8-10, Los Reyes. Dosquebradas, Risaralda','phone1'=>'(576) 342 0830','email'=>'info@geotecniaingenieria.co','slpName'=>'Jhon Romero','pymGr'=>'Contado',
'lineNmo'=>'Para efectos de la aplicación de la tabla de retenc',
'L'=>[
 ['itemName'=>'Bota Seguridad Negra','price'=>10000,'quantity'=>10,'priceLine'=>100000,'lineText'=>'Disponible en talla 38 a 45'],
 ['itemName'=>'Bota Seguridad Negra','price'=>10000,'quantity'=>10,'priceLine'=>100000,'lineText'=>'Disponible en talla 38 a 45'],
],
'o'=>[
    'ocardName'=>'ADMS Sistems','address'=>'Carrera 8 #43 - 01, Los Reyes','pbx'=>'+57 323 338 9324','mail'=>'info@admsistems.com',
]
];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo c::$V['tdocsTemp']; ?>/gvt.sin1.css" />
    <title>Factura</title>  
    <?php include('comun.css'); ?>
</head>
<body>              
<div class="page">
<table class="headinfo">
    <tr>
    <td class="logo">
    <img src="<?php echo $_HTML['o']['logo']; ?>" style="max-height:80px; max-width:300px;">
    </td>
    <td class="topinfo">
    <div><?php echo $_HTML['o']['ocardName']; ?></div>
    <div><?php echo $_HTML['o']['address']; ?></div>
    <div><?php echo $_HTML['o']['pbx']; ?></div>
    <div><?php echo $_HTML['o']['mail']; ?></div>
    </td>
    <td class="topRight">
    <div class="docNum"><?php echo $_HTML['docNumText']; ?></div>
    <div><b>Fecha</b>: <?php echo $_HTML['docDate']; ?></div>
    <div><b>Vence</b>: <?php echo $_HTML['dueDate']; ?></div>
    </td>
    </tr>
</table>
<div class="cardInfo">
<table>
<tr>
    <th>Cliente</th> <td colspan=3><?php echo $_HTML['cardName']; ?></td>
    <th><?php echo $_HTML['licTradType']; ?></th> <td><?php echo $_HTML['licTradNum']; ?></td>
</tr>
<tr>
    <th>Dirección</th> <td colspan=3><?php echo $_HTML['address']; ?></td>
    <th>Resp.</th><td><?php echo $_HTML['slpName']; ?></td>
</tr>
<tr>
    <th>Telefono</th> <td><?php echo $_HTML['phone1']; ?></td>
    <th>Email</th> <td><?php echo $_HTML['email']; ?></td>
    <th>Condic.</th><td><?php echo $_HTML['pymName']; ?></td>
</tr>
<tr>
    <td colspan=6 class="lineMemo"><?php echo $_HTML['lineMemo']; ?></td>
</tr>
</table>
</div>
<div class="items">
<table>
<tr>
 <th>Articulo</th>
 <th class="price">Precio</th>
 <th class="qty">Cant</th>
 <th class="lineTotal">Total</th>
 <th class="lineMemo">Detalles</th>
</tr>
<?php
foreach($_HTML['L'] as $n=>$L){
 echo '<tr>
 <td>'.$L['itemName'].'</td>
 <td class="price">'._js::money($L['price']).'</td>
 <td class="qty">'.($L['quantity']*1).'</td>
 <td class="lineTotal">'._js::money($L['priceLine']).'</td>
 <td class="lineMemo">'.$L['lineText'].'</td>
</tr>';
}
?>
<tfoot>
<tr>
 <td></td>
 <td colspan="2">SubTotal</td>
 <td><?php echo _js::money($_HTML['baseAmnt']); ?></td>
 <td></td>
</tr>
<tr>
 <td></td>
 <td colspan="2">+ Impuestos</td>
 <td><?php echo _js::money($_HTML['vatSum']); ?></td>
 <td></td>
</tr>
<tr>
 <td></td>
 <td colspan="2">- Retención</td>
 <td><?php echo _js::money($_HTML['rteSum']); ?></td>
 <td></td>
</tr>
<tr>
 <td></td>
 <th colspan="2">Total</th>
 <td><?php echo _js::money($_HTML['docTotal']); ?></td>
 <td></td>
</tr>
</tfoot>
</table>
</div>
<?php include('pyms.btn.php'); ?>
<div class="bottomInfo"><?php echo $_HTML['noteFix']; ?></div>

<div class="bottomAdms"><?php echo c::$V['softWaterBottom']; ?></div>
</body>
</html>