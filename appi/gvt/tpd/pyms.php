<?php
c::$V['tdocsTemp']=c::$V['URI_API'].'/static/pyms';
JRoute::get('gvt/pyms',function($D){
  $urlreturn=c::$V['URI_TPD'].'gvt/pyms/success/';
  $pyms=a_mdl::cnf2V($q,['pymCnf'=>'Y']);
  $pyms['wompi']['pubkey']='pub_test_sv3EcJGhntO8E0RrHbJg99YhNCJKGqvT';
  $pyms['epayco']['pubkey']='707dc1e2e859970123c5666244eab7b1';
  header('Content-Type: text/html');
  $_HTML=a_sql::fetch('SELECT A.docEntry,A.docNum,A.docDate,A.dueDate,A.cardName,C.licTradNum,C.licTradType,A.address,A.phone1,A.email,A.baseAmnt,A.vatSum,A.rteSum,A.docTotal,A.lineMemo,S.slpName,P.pymName,DO.srCode,DO.noteFix
  FROM gvt_oinv A
  JOIN par_ocrd C ON (C.cardId=A.cardId)
  LEFT JOIN par_oslp S ON (S.slpId=A.slpId)
  LEFT JOIN gfi_opym P ON (P.pymId=A.pymId)
  LEFT JOIN doc_oser DO ON (DO.serieId=A.serieId)
  WHERE A.docEntry=\''.$_GET['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información del documento',2=>'Documento no encontrado']);
  if(a_sql::$err){ echo _err::err(a_sql::$errNoText); }
  else{
    $_HTML['docTotal']=($_HTML['docTotal']*1);
    $uniref=$_GET['docEntry'].'-'.substr(time(),5);
    $frmWompi='
    <body>
    <script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>
    <script type="text/javascript">
    var checkout = new WidgetCheckout({
      currency: "COP",
      amountInCents: '.($_HTML['docTotal']*100).',
      reference: "'.$uniref.'",
      publicKey: "'.$pyms['wompi']['pubkey'].'",
      redirectUrl: "'.$urlreturn.'wompi/'.$_GET['docEntry'].'" // Opcional
    });
    function xsend(){
      checkout.open(function ( result ) {
        var transaction = result.transaction
        console.log("Transaction ID: ", transaction.id)
        console.log("Transaction object: ", transaction);
        if(transaction.status=="APPROVED"){ location.href=transaction.redirectUrl; }
      })
    }
    </script>
      <img src="'.c::$V['URI_STATIC'].'/_img/wompipay.png" /><br/>
      <button class="waybox-button" type="submit" onclick="xsend();">Paga con <strong>Wompi</strong></button>
    </body>';

    $frmEpayco='<script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
<script type="text/javascript">
var handler = ePayco.checkout.configure({
    key: "'.$pyms['epayco']['pubkey'].'",
    test: true
});
var data={
  //Parametros compra (obligatorio)
  name: "Pago a Factura",
  description: "Pago a Factura",
  invoice: "'.$_GET['docEntry'].'",
  currency: "cop",
  amount: "'.$_HTML['docTotal'].'",
  tax_base: "0",
  tax: "0",
  country: "co",
  lang: "es",
  test:"true",
  //Onpage="false" - Standard="true"
  external: "false",
  //atributo deshabilitación metodo de pago "TDC", "PSE","SP","CASH","DP"
  methodsDisable: ["SP"],

  //Atributos opcionales, extra1,extra2,extra3,
  confirmation: "'.$urlreturn.'epayco/'.$_GET['docEntry'].'",
  response: "'.$urlreturn.'epayco/'.$_GET['docEntry'].'",

  //Atributos cliente
  //name_billing: "Andres Perez",
  //address_billing: "Carrera 19 numero 14 91",
  //type_doc_billing: "cc",
  //mobilephone_billing: "3050000000",
  //number_doc_billing: "100000000"
  }
</script>
<button style="padding: 0; background: none; border: none; cursor: pointer;" class="epayco-button-render" onclick="handler.open(data)"><img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/btn3.png"></button>';
    echo $frmWompi;
    //echo $frmEpayco;
  }
});

JRoute::get('gvt/pyms/success/{provider}/{docEntry}',function($D){
  if($D['ref_payco']){ $D['docEntry']=$D['ref_payco']; }
  print_r($D);
  $qi=a_sql::qInsert(['uid'=>$D['provider'].'-'.$D['docEntry'],
  'docEntry'=>$D['docEntry'],'dateC'=>date('Y-m-d H:i:s'),'provider'=>$D['provider'],'cid'=>$D['id']],['tbk'=>'pse_opym']);
  print_r($qi);
  echo _err::errDie();
},['_wh'=>['pv'=>'\w+','docEntry'=>'\d+']]);
?>