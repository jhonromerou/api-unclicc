/* GeSN!
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
$MdlStatus.put('mpaOpo','Y');

$M.liAdd('mpa',[
{k:'mpaOpo',t:'Oportunidades', kau:'mpaOpo', func:function(){
	$M.Ht.ini({f:'mpaOpo',g:function(){ Mpa.Opo.xHtml[1]=$M.Ht.cont; Mpa.Opo.get(); }
	});
}}
]);

_Fi['mpaOpo']=function(w){
	Mpa.Opo.Fi.F.push(['Contacto',{tag:'input',name:'P.name(E_like3)'}]);
	Mpa.Opo.Fi.F.push(['Socio',{tag:'input',name:'C.cardName(E_like3)'}]);
	Mpa.Opo.profile='A';
	Mpa.Opo.xHtml=[];
	Mpa.Opo.xHtml[0]=w;
	Mpa.Opo.ini({openAlw:'Y'});
}
$V.mpaOpoStatus=[{k:'O',v:'Abierto'},{k:'W',v:'Ganada'},{k:'L',v:'Perdida'}];
Mpa.Opo={
profile:false,//A,crd,cpr
gData:['',{}],//&cardId=1
openAlw:'Y',//Abrir siempre
xHtml:[],//0-filt,1-list
ini:function(P){
	var P=(P)?P:{};
	if(Mpa.Opo.openAlw=='Y' || P.openAlw=='Y'){
		Mpa.Opo.Fi.form({cont:Mpa.Opo.xHtml[0]});
		$1.addBtnFas([
		{fa:'fa_doc',textNode:'Nueva',func:function(){ Mpa.Opo.form(); }}
		],Mpa.Opo.xHtml[0]);
		Mpa.Opo.get();
	}
	Mpa.Opo.openAlw=P.openAlw;
},
Fi:new $Api.Filt({active:'Y',cols:2,
	func:function(T){ Mpa.Opo.get(T); },
	F:[
		['Estado',{tag:'select',name:'A.docStatus',opts:$V.mpaOpoStatus,noBlank:'Y'}],
		['Valor >',{tag:'number',name:'A.bal(E_mayIgual)'}],
		['% Probabilidad >',{tag:'number',name:'A.percWin(E_mayIgual)'}],
		['Nombre',{tag:'input',name:'A.title(E_like3)'}],
		['Origen',{tag:'select',name:'A.gOri',opts:$JsV.mpaOpoOri}],
		['Tipo',{tag:'select',name:'A.gType',opts:$JsV.mpaOpoType}],
		['Prioridad',{tag:'select',name:'A.gPrio',opts:$JsV.mpaOpoPrio}],
		['Responsable',{tag:'userAssg',name:'A.userAssg'}],
		['Fecha Estimada',{TA:[{tag:'date',name:'A.dueDate(E_mayIgual)'},{tag:'date',name:'A.dueDate(E_menIgual)'}]}],
		['Fecha Cierre',{TA:[{tag:'date',name:'A.closeDate(E_mayIgual)'},{tag:'date',name:'A.closeDate(E_menIgual)'}]}],
		['Creada',{TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]}]
	]
}),
get:function(T){
	vPost =Mpa.Opo.Fi.get()+Mpa.Opo.gData[0];
	var wList=Mpa.Opo.xHtml[1];
	$Api.get({f:Api.Mpa.b+'opo',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ leer(Jr); }
	},moreWrap:wList,moreFunc:leer
	});
	function leer(Jr){
		if(!Jr.errNo){
			for(var i in Jr.L){
				Mpa.Opo.drawRow(Jr.L[i]);
			}
		}
	}
},
form:function(P){ var P=(P)?P:{};
	var fP={Win:{winTitle:'Oportunidad de Venta'},
	vidn:'gid',gid:P.gid,jsAdd:Mpa.Opo.gData[1],api:Api.Mpa.b+'opo',
	Cols:[
	['Nombre Oportunidad',{k:'title',T:{divLine:1,req:'Y',wxn:'wrapx2',tag:'input'}}],
	['Valor',{k:'bal',T:{wxn:'wrapx4',tag:'$',min:0}}],
	['% Probabilidad',{k:'percWin',T:{req:'Y',wxn:'wrapx4',tag:'number',min:0,max:100}}],
	['Estado',{k:'docStatus',T:{divLine:1,req:'Y',wxn:'wrapx3',tag:'select',opts:$V.mpaOpoStatus,noBlank:'Y'}}],
	['Embudo',{k:'funId',T:{req:'Y',wxn:'wrapx3','class':'_mpaOpoFunSel',tag:'select',opts:$JsV.mpaOpoFunnel}}],
	['Etapa',{k:'stageId',T:{req:'Y',wxn:'wrapx3','class':'_mpaOpoFunStage',tag:'select'}}],
	['Responsable',{k:'userAssg',T:{divLine:1,wxn:'wrapx4',req:'Y',tag:'select',opts:$Tb.ousr}}],
	['Origen',{k:'gOri',T:{req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaOpoOri}}],
	['Tipo',{k:'gType',T:{req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaOpoType}}],
	['Prioridad',{k:'gPrio',T:{wxn:'wrapx4',tag:'select',opts:$JsV.mpaOpoPrio}}],
	['Fecha Estimada',{divLine:1,k:'dueDate',T:{wxn:'wrapx4',tag:'date'}}],
	['Fecha Cierre',{k:'closeDate',T:{wxn:'wrapx4',tag:'date'}}]
	],
	delCont:'Y',
	oFunc:function(o){
		Mpa.Opo.drawRow(o);
	}
	}
	if(Mpa.Opo.profile=='A' || Mpa.Opo.profile=='crd'){
		fP.Cols.push(['Contacto',{k:'name',T:{divLine:1,wxn:'wrapx2',tag:'cpr'}}]);
	}
	if(Mpa.Opo.profile=='A' || Mpa.Opo.profile=='cpr'){
		fP.Cols.push(['Socio Neg.',{k:'cardName',T:{wxn:'wrapx2',tag:'crd'}}]);
	}
	fP.Cols.push(['Descripción',{k:'shortDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'textarea'}}]);
	$Api.form(fP,function(wCont,Jr){
		var fsel=$1.q('._mpaOpoFunSel',wCont);
		var stage=$1.q('._mpaOpoFunStage',wCont);
		fsel.onchange=function(){
			$1.T.sel({reLoad:stage,opts:$JsV.mpaOpoStage,selected:Jr.stageId,kIf:{prp1:this.value}});
		}
		$1.T.sel({reLoad:stage,opts:$JsV.mpaOpoStage,selected:Jr.stageId,kIf:{prp1:Jr.funId}});
	});
},
drawRow:function(L){
	L=Mpa.Opo.Ht.vD(L);
	var wList=Mpa.Opo.xHtml[1];
	var ide='_mpaOpo_'+L.gid;
	var divW=$1.uniNode($1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'}),wList);
	var div=$1.t('div',0,divW);
	$1.t('div',{textNode:L.title,'class':'mPointer _oOpen',style:'padding:6px 0 0',P:L},divW).onclick=function(){ Mpa.Opo.view(this.P); }
	var div2=$1.t('div',0,divW);
	Mpa.Opo.Ht.edit(L,div);
	Mpa.Opo.Ht.percWin(L,div);
	Mpa.Opo.Ht.bal(L,div);
	Mpa.Opo.Ht.type(L,div);
	Mpa.Opo.Ht.prio(L,div);
	Mpa.Opo.Ht.ori(L,div);
	Mpa.Opo.Ht.dueDate(L,div);
	Mpa.Opo.Ht.userAssg(L,div2);
	Mpa.Opo.Ht.profileBot(L,div2);
	Mpa.Opo.Ht.nums(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=$1.t('div',0); var jsF='jsFields';
	$Api.get({f:Api.Mpa.b+'opo/'+P.gid,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x8',t:'Probabilidad',iT:{k:'percWin'}},
		{wxn:'tbf_x4',t:'Valor',iT:{k:'bal'}},
		{line:1,wxn:'tbf_x1',t:'Nombre',iT:{k:'title'}},
		{line:1,wxn:'tbf_x4',t:'Estado',iT:{k:'docStatus',_V:'mpaOpoStatus'}},
		{wxn:'tbf_x4',t:'Embudo',iT:{k:'funId',_JsV:'mpaOpoFunnel'}},
		{wxn:'tbf_x4',t:'Etapa',iT:{k:'stageId',_JsV:'mpaOpoStage'}},
		{line:1,wxn:'tbf_x3',t:'Fecha Estimada',iT:{k:'dueDate'}},
		{wxn:'tbf_x3',t:'Cierre',iT:{k:'closeDate'}},
		{line:1,wxn:'tbf_x4',t:'Responsable',iT:{k:'userAssg',_gTb:'ousr'}},
		{wxn:'tbf_x4',t:'Origen',iT:{k:'gOri',_JsV:'mpaOpoOri'}},
		{wxn:'tbf_x4',t:'Tipo',iT:{k:'gType',_JsV:'mpaOpoType'}},
		{wxn:'tbf_x4',t:'Prioridad',iT:{k:'gPrio',_JsV:'mpaOpoPrio'}},
		{line:1,wxn:'tbf_x2',t:'Contacto',iT:{k:'name'}},
		{wxn:'tbf_x2',t:'Socio Negocios',iT:{k:'cardName'}},
		{line:1,wxn:'tbf_x1',t:'Descripción',iT:{k:'shortDesc','class':'pre'}},
		],cont,Jr);
		Mpa.Opo.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Oportunidad'});
},
}

Mpa.Opo.Ht={
vD:function(L){
	if(L.gPrio){ L.prioText=_g(L.gPrio,$JsV.mpaOpoPrio); }
	var oT=_gO(L.gType,$JsV.mpaOpoType);;
	L.typeText=oT.v;
	L.typeCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	var oT=_gO(L.gOri,$JsV.mpaOpoOri);;
	L.oriText=oT.v;
	L.oriCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.dueDate)){ L.dueDateDefine=true;
		L.dueDateText=$2d.f(L.dueDate,'mmm d');
	}
	if(L.prsId>0){ L.prsDefine=true; }
	if(L.cardId>0){ L.cardDefine=true; }
	return L;
},
edit:function(L,div){
	var divR=$1.t('div',{style:'float:right'},div);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		Mpa.Opo.form(T.P);
	}},divR);
},
percWin:function(L,divT){
	return $1.t('div',{title:'% Ganarla','class':'crm_prpLine fa fa-percent',textNode:L.percWin*1},divT);
},
bal:function(L,divT){
	return $1.t('div',{title:'% Ganarla','class':'crm_prpLine fa fa-money',textNode:$Str.money(L.bal)},divT);
},
prio:function(L,divT){
	if(L.gPrio>0){
		return $1.t('div',{title:'Prioridad','class':'crm_prpLine fa fa_prio',textNode:L.prioText},divT);
	}
},
ori:function(L,divT){
	if(L.gOri>0){
		return $1.t('div',{title:'Origen del Oportunidad','class':'crm_prpLine fa fa-dot-circle-o',textNode:' '+L.oriText},divT);
	}
},
type:function(L,divT){
	var div=$1.t('div',{textNode:L.typeText,title:'Tipo','class':'crm_prpLine',style:L.typeCss},divT);
	return div;
},
dueDate:function(L,divT){
	if(L.dueDateDefine){
		$1.t('div',{title:'Plazo','class':'fa fa-calendar crm_prpLine',textNode:L.dueDateText},divT);
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'fa fa-user crm_prpLine',textNode:L.userAssgText},divT);
	}
},
profileBot:function(L,divP){
	if(Mpa.Opo.profile=='A'){
		Mpa.Opo.Ht.prs(L,divP);
		Mpa.Opo.Ht.crd(L,divP);
	}
	else if(Mpa.Opo.profile=='crd'){
		Mpa.Opo.Ht.prs(L,divP);
	}
	else if(Mpa.Opo.profile=='cpr'){
		Mpa.Opo.Ht.crd(L,divP);
	}
},
longDesc:function(L,divT){
	if(L.longDesc && L.longDesc!=''){
		div= $1.t('div',{textNode:L.longDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
nums:function(L,divT){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments crm_prpLine',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		lineText=(L.prsName)?L.prsName:lineText;
		var div=$1.t('div',{title:'Contacto','class':'mPointer fa fa-handshake-o crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='prsId:'+L.prsId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCpr.view',pars);
		}
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		lineText=(L.cardName)?L.cardName:lineText;
		var div= $1.t('div',{title:'Socio de Negocios','class':'mPointer fa fa-institution crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='cardId:'+L.cardId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCrd.view',pars);
		}
		return div;
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(cFol,_5fw); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	var _5c=Wins['win5c'];
	var _5f=Wins['win5f']
	var inf=Wins['winInfo'];
	//Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},_5c);
	//Attach.Tt.form({tt:'crmNov',tr:L.gid, addLine:'Y',func:function(Jrr,o){}},_5f);
	var _5fw=$1.t('div',0,_5f);
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creado',v:L.dateC},
		{wxn:'tbf_x4',t:'Creado por',v:_g(L.userId,$Tb.ousr)},
		],inf);
}

};

$JsV._i({kObj:'mpaOpoOri',mdl:'mpa',liK:'mpaJsv',liTxtG:'Origen de Oportunidades',liTxtF:'Origen Oportunidad(Form)'});
$JsV._i({kObj:'mpaOpoType',mdl:'mpa',liK:'mpaJsv',liTxtG:'Tipo de Oportunidades',liTxtF:'Tipo Oportunidad (Form)'});
$JsV._i({kObj:'mpaOpoPrio',mdl:'mpa',liK:'mpaJsv',liTxtG:'Prioridad de Oportunidades',liTxtF:'Prioridad Oportunidad(Form)'});
$JsV._i({kObj:'mpaOpoFunnel',mdl:'mpa',liK:'mpaJsv',liTxtG:'Embudo de Oportunidades',liTxtF:'Embudo Oportunidad (Form)'});
$JsV._i({kObj:'mpaOpoStage',mdl:'mpa',liK:'mpaJsv',liTxtG:'Etapas de Embudo',liTxtF:'Etapa Embudo (Form)',
Cols:[
{t:'Nombre Etapa',k:'value',T:{tag:'input'}},
{t:'Embudo',k:'prp1',_JsV:'mpaOpoFunnel',T:{tag:'select',opts:$JsV.mpaOpoFunnel}}
]
});
