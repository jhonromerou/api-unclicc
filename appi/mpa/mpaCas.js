/* GeSN!
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
$MdlStatus.put('mpaCas','Y');

$M.liAdd('mpa',[
{k:'mpaCas',t:'Casos', kau:'mpaCas', func:function(){
	$M.Ht.ini({f:'mpaCas',gyp:function(){ Mpa.Cas.xHtml[1]=$M.Ht.cont; Mpa.Cas.get(); }
	});
}}
]);

_Fi['mpaCas']=function(w){
	Mpa.Cas.Fi.F.push(['Contacto',{tag:'input',name:'P.name(E_like3)'}]);
	Mpa.Cas.Fi.F.push(['Socio',{tag:'input',name:'C.cardName(E_like3)'}]);
	Mpa.Cas.profile='A';
	Mpa.Cas.xHtml=[];
	Mpa.Cas.xHtml[0]=w;
	Mpa.Cas.ini({openAlw:'Y'});
}
$V.mpaCasStatus=[{k:'O',v:'Abierto'},{k:'C',v:'Cerrado'}];
Mpa.Cas={
profile:false,//A,crd,cpr
gData:['',{}],//&cardId=1
openAlw:'Y',//Abrir siempre
xHtml:[],//0-filt,1-list
ini:function(P){
	var P=(P)?P:{};
	if(Mpa.Cas.openAlw=='Y' || P.openAlw=='Y'){
		Mpa.Cas.Fi.form({cont:Mpa.Cas.xHtml[0]});
		$1.addBtnFas([
		{fa:'fa_doc',textNode:'Nueva',func:function(){ Mpa.Cas.form(); }}
		],Mpa.Cas.xHtml[0]);
		Mpa.Cas.get();
	}
	Mpa.Cas.openAlw=P.openAlw;
},
Fi:new $Api.Filt({active:'Y',cols:2,
	func:function(T){ Mpa.Cas.get(T); },
	F:[
		['Estado',{tag:'select',name:'A.docStatus',opts:$V.mpaCasStatus,noBlank:'Y'}],
		['Asunto',{tag:'input',name:'A.title(E_like3)'}],
		['Origen',{tag:'select',name:'A.gOri',opts:$JsV.mpaCasOri}],
		['Tipo',{tag:'select',name:'A.gType',opts:$JsV.mpaCasType}],
		['Gestión',{tag:'select',name:'A.gtatus',opts:$JsV.mpaCasStatus}],
		['Prioridad',{tag:'select',name:'A.gPrio',opts:$JsV.mpaCasPrio}],
		['Responsable',{tag:'userAssg',name:'A.userAssg'}],
		['Fecha Registro',{TA:[{tag:'date',name:'A.docDate(E_mayIgual)'},{tag:'date',name:'A.docDate(E_menIgual)'}]}],
		['Fecha Cierre',{TA:[{tag:'date',name:'A.closeDate(E_mayIgual)'},{tag:'date',name:'A.closeDate(E_menIgual)'}]}],
		['Fecha Plazo',{TA:[{tag:'date',name:'A.dueDate(E_mayIgual)'},{tag:'date',name:'A.dueDate(E_menIgual)'}]}],
		['Creada',{TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]}]
	]
}),
get:function(T){
	vPost =Mpa.Cas.Fi.get()+Mpa.Cas.gData[0];
	var wList=Mpa.Cas.xHtml[1];
	$Api.get({f:Api.Mpa.b+'cas',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ leer(Jr); }
	},moreWrap:wList,moreFunc:leer
	});
	function leer(Jr){
		if(!Jr.errNo){
			for(var i in Jr.L){
				Mpa.Cas.drawRow(Jr.L[i]);
			}
		}
	}
},
form:function(P){ var P=(P)?P:{};
	var fP={Win:{winTitle:'Tarea'},
	vidn:'gid',gid:P.gid,jsAdd:Mpa.Cas.gData[1],api:Api.Mpa.b+'cas',
	Cols:[
	['Estado',{k:'docStatus',T:{divLine:1,req:'Y',wxn:'wrapx3',tag:'select',opts:$V.mpaCasStatus,noBlank:'Y'}}],
	['Origen',{k:'gOri',T:{req:'Y',wxn:'wrapx3',tag:'select',opts:$JsV.mpaCasOri}}],
	['Tipo',{k:'gType',T:{req:'Y',wxn:'wrapx3',tag:'select',opts:$JsV.mpaCasType}}],
	['Prioridad',{k:'gPrio',T:{divLine:1,wxn:'wrapx3',tag:'select',opts:$JsV.mpaCasPrio}}],
	['Responsable',{k:'userAssg',T:{wxn:'wrapx3',tag:'select',opts:$Tb.ousr}}],
	['Gestión',{k:'gStatus',T:{wxn:'wrapx3',tag:'select',opts:$JsV.mpaCasStatus}}],
	['Titulo',{k:'title',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'input'}}],
	['Fecha',{k:'docDate',T:{divLine:1,wxn:'wrapx3',tag:'date'}}],
	['Plazo',{k:'dueDate',T:{wxn:'wrapx3',tag:'date'}}],
	['Fecha Cierre',{k:'closeDate',T:{wxn:'wrapx3',tag:'date'}}]
	],
	delCont:'Y',
	oFunc:function(o){
		Mpa.Cas.drawRow(o);
	}
	}
	if(Mpa.Cas.profile=='A' || Mpa.Cas.profile=='crd'){
		fP.Cols.push(['Contacto',{k:'name',T:{divLine:1,wxn:'wrapx2',tag:'cpr'}}]);
	}
	if(Mpa.Cas.profile=='A' || Mpa.Cas.profile=='cpr'){
		fP.Cols.push(['Socio Neg.',{k:'cardName',T:{wxn:'wrapx2',tag:'crd'}}]);
	}
	fP.Cols.push(['Descripción',{k:'shortDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'textarea'}}]);
	var x=$Api.form(fP);
},
drawRow:function(L){
	L=Mpa.Cas.Ht.vD(L);
	var wList=Mpa.Cas.xHtml[1];
	var ide='_mpaCas_'+L.gid;
	var divW=$1.uniNode($1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'}),wList);
	var div=$1.t('div',0,divW);
	$1.t('div',{textNode:L.title,'class':'mPointer _oOpen',style:'padding:6px 0 0',P:L},divW).onclick=function(){ Mpa.Cas.view(this.P); }
	Mpa.Cas.Ht.edit(L,div);
	Mpa.Cas.Ht.type(L,div);
	Mpa.Cas.Ht.prio(L,div);
	Mpa.Cas.Ht.ori(L,div);
	Mpa.Cas.Ht.dueDate(L,div);
	Mpa.Cas.Ht.userAssg(L,div);
	Mpa.Cas.Ht.profileBot(L,div);
	Mpa.Cas.Ht.nums(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=$1.t('div',0); var jsF='jsFields';
	$Api.get({f:Api.Mpa.b+'cas/'+P.gid,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'No. Caso',iT:{k:'gid'}},
		{wxn:'tbf_x4',t:'Estado',iT:{k:'docStatus',_V:'mpaCasStatus'}},
		{wxn:'tbf_x4',t:'Origen',iT:{k:'gOri',_JsV:'mpaCasOri'}},
		{wxn:'tbf_x4',t:'Tipo',iT:{k:'gType',_JsV:'mpaCasType'}},
		{line:1,wxn:'tbf_x1',t:'Asunto',iT:{k:'title'}},
		{line:1,wxn:'tbf_x4',t:'Prioridad',iT:{k:'gPrio',_JsV:'mpaCasPrio'}},
		{wxn:'tbf_x2',t:'Responsable',iT:{k:'userAssg',_gTb:'ousr'}},
		{wxn:'tbf_x4',t:'Gestión',iT:{k:'gStatus',_JsV:'mpaCasStatus'}},
		{line:1,wxn:'tbf_x3',t:'Fecha',iT:{k:'docDate'}},
		{wxn:'tbf_x3',t:'Plazo',iT:{k:'dueDate'}},
		{wxn:'tbf_x3',t:'Cierre',iT:{k:'closeDate'}},
		{line:1,wxn:'tbf_x2',t:'Contacto',iT:{k:'name'}},
		{wxn:'tbf_x2',t:'Socio Negocios',iT:{k:'cardName'}},
		{line:1,wxn:'tbf_x1',t:'Descripción',iT:{k:'shortDesc','class':'pre'}},
		],cont,Jr);
		Mpa.Cas.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Caso'});
},
}

Mpa.Cas.Ht={
vD:function(L){
	if(L.gPrio){ L.prioText=_g(L.gPrio,$JsV.mpaCasPrio); }
	var oT=_gO(L.gType,$JsV.mpaCasType);;
	L.typeText=oT.v;
	L.typeCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	var oT=_gO(L.gOri,$JsV.mpaCasOri);;
	L.oriText=oT.v;
	L.oriCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.dueDate)){ L.dueDateDefine=true;
		L.dueDateText=$2d.f(L.dueDate,'mmm d');
	}
	if(L.prsId>0){ L.prsDefine=true; }
	if(L.cardId>0){ L.cardDefine=true; }
	return L;
},
edit:function(L,div){
	var divR=$1.t('div',{style:'float:right'},div);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		Mpa.Cas.form(T.P);
	}},divR);
},
prio:function(L,divT){
	if(L.gPrio>0){
		return $1.t('div',{title:'Prioridad','class':'crm_prpLine fa fa_prio',textNode:L.prioText},divT);
	}
},
ori:function(L,divT){
	if(L.gOri>0){
		return $1.t('div',{title:'Origen del Caso','class':'crm_prpLine fa fa-dot-circle-o',textNode:' '+L.oriText},divT);
	}
},
type:function(L,divT){
	var div=$1.t('div',{textNode:L.typeText,title:'Tipo','class':'crm_prpLine',style:L.typeCss},divT);
	return div;
},
dueDate:function(L,divT){
	if(L.dueDateDefine){
		$1.t('div',{title:'Plazo','class':'fa fa-calendar crm_prpLine',textNode:L.dueDateText},divT);
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'fa fa-user crm_prpLine',textNode:L.userAssgText},divT);
	}
},
profileBot:function(L,divP){
	if(Mpa.Cas.profile=='A'){
		Mpa.Cas.Ht.prs(L,divP);
		Mpa.Cas.Ht.crd(L,divP);
	}
	else if(Mpa.Cas.profile=='crd'){
		Mpa.Cas.Ht.prs(L,divP);
	}
	else if(Mpa.Cas.profile=='cpr'){
		Mpa.Cas.Ht.crd(L,divP);
	}
},
longDesc:function(L,divT){
	if(L.longDesc && L.longDesc!=''){
		div= $1.t('div',{textNode:L.longDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
nums:function(L,divT){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments crm_prpLine',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		lineText=(L.prsName)?L.prsName:lineText;
		var div=$1.t('div',{title:'Contacto','class':'mPointer fa fa-handshake-o crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='prsId:'+L.prsId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCpr.view',pars);
		}
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		lineText=(L.cardName)?L.cardName:lineText;
		var div= $1.t('div',{title:'Socio de Negocios','class':'mPointer fa fa-institution crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='cardId:'+L.cardId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCrd.view',pars);
		}
		return div;
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(cFol,_5fw); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	var _5c=Wins['win5c'];
	var _5f=Wins['win5f']
	var inf=Wins['winInfo'];
	//Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},_5c);
	//Attach.Tt.form({tt:'crmNov',tr:L.gid, addLine:'Y',func:function(Jrr,o){}},_5f);
	var _5fw=$1.t('div',0,_5f);
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creado',v:L.dateC},
		{wxn:'tbf_x4',t:'Creado por',v:_g(L.userId,$Tb.ousr)},
		],inf);
}

};

$JsV._i({kObj:'mpaCasOri',mdl:'mpa',liK:'mpaJsv',liTxtG:'Origen de Casos',liTxtF:'Origen Caso(Form)'});
$JsV._i({kObj:'mpaCasType',mdl:'mpa',liK:'mpaJsv',liTxtG:'Tipo de Casos',liTxtF:'Tipo Caso (Form)'});
$JsV._i({kObj:'mpaCasPrio',mdl:'mpa',liK:'mpaJsv',liTxtG:'Prioridad de Casos',liTxtF:'Prioridad Caso(Form)'});
$JsV._i({kObj:'mpaCasStatus',mdl:'mpa',liK:'mpaJsv',liTxtG:'Estado de Casos',liTxtF:'Estado Caso(Form)'});
