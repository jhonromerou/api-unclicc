<?php
JRoute::get('gvp/cr',function($D){
	$D['from']='A.* FROM gvp_ocre A';
	return a_sql::rPaging($D);
});
JRoute::get('gvp/cr/form',function($D){
	if(_js::iseErr($D['crId'],'Se debe definir ID de la caja a modificar','numeric>0')){}
	else{
		$r=a_sql::fetch('SELECT * FROM gvp_ocre WHERE crId=\''.$D['crId'].'\' LIMIT 1',[1=>'Error obteniendo información de la caja',2=>'La caja no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{ return _js::enc2($r); }
	}
	_err::errDie();
	
},[]);
JRoute::put('gvp/cr',function($D){
	if(_js::iseErr($D['crCode'],'Se debe definir un código para la caja')){}
	else if($js=_js::textMax($D['crCode'],4,'Código ')){ _err::err($js); }
	else if(_js::iseErr($D['crName'],'Se debe definir un nombre')){}
	else if($js=_js::textMax($D['crName'],12,'Nombre ')){ _err::err($js); }
	else if(_js::iseErr($D['docSin'],'Se debe definir una númeración para la factura','numeric>0')){}
	else if(_js::iseErr($D['docRcv'],'Se debe definir una númeración para los pagos','numeric>0')){}
	else if(_js::iseErr($D['fdpIdEFE'],'Se debe forma de pago del efectivo','numeric>0')){}
	else if(_js::iseErr($D['fdpIdTIB'],'Se debe forma de pago de las transferencia','numeric>0')){}
	else{
		$R=a_sql::uniRow($D,['tbk'=>'gvp_ocre','wh_change'=>'crId=\''.$D['crId'].'\' LIMIT 1']);
		if(!_err::$err){
			if($R['insertId']){ $D['crId']=$R['insertId']; }
			return _js::r('Información guardada correctamente',$D);
		}
	}
	_err::errDie();
},[]);

JRoute::get('gvp/cr/model___',function($D){
	echo JRModel::sql($D);
},['libM'=>['gvp','gvp']]);
?>