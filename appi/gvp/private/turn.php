<?php
JRoute::get('gvp/turn','get');
JRoute::get('gvp/turn/audit','audit');
JRoute::get('gvp/turn/one','getOne');
JRoute::put('gvp/turn/one','putOne');
JRoute::get('gvp/turn/oneClose',function($D){
  $qf=a_sql::fetch('SELECT pcId FROM gvp_otur WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información del turno',2=>'Turno no existe']);
  if(a_sql::$err){ die(a_sql::$errNoText); }
  echo gvpTurn::status(['pcId'=>$qf['pcId']],'noPass');
});

JRoute::post('gvp/turn/status','status');
JRoute::put('gvp/turn/open','open');
JRoute::put('gvp/turn/close','close');
JRoute::put('gvp/turn/statusCancel','statusCancel');
?>