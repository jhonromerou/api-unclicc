<?php
//Caja REgistradora
JRModel::sett('gvp_ocre',[
'crId'=>'smallint(4) unsigned auto_increment,',
'active'=>'enum(\'Y\',\'N\') not null default \'Y\',',
'crCode'=>'varchar(4),',
'crName'=>'varchar(10) not null default \'\',',
'docMemo'=>'varchar(50) not null default \'\'',
'_index'=>['primary key(`crId`)','unique index `crCode_u` (`crCode`)']
]);
//Personal CAja
JRModel::sett('gvp_opca',[
'pcId'=>'smallint(4) unsigned auto_increment,',
'active'=>'enum(\'Y\',\'N\') not null default \'Y\',',
'pcCode'=>'varchar(4),',
'pcName'=>'varchar(20) not null default \'\',',
'userId'=>'smallint(5) unsigned',
'_index'=>['primary key(`pcId`)','unique index `pcCode_u` (`pcCode`)']
]);
?>