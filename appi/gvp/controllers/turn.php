<?php
class gvpTurn{
  static public function get($D=[]){
    $D['from']='A.docEntry,A.docStatus,A.canceled,A.crId,A.pcId,A.openDate,A.closeDate,A.balOpen,A.balRcv,A.balRce,A.balAjus,A.balClose,AC.accName,A.dateC,A.userId,A.dateUpd,A.userUpd
    FROM gvp_otur A
    LEFT JOIN gfi_opdc AC ON (AC.accId=A.accId)
    ';
    return a_sql::rPaging($D);
  }
  static public function getOne($D=[]){
   $q=a_sql::fetch('SELECT * FROM gvp_otur WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información del turno',2=>'El turno no existe']);
   if(a_sql::$err){ return _err::err(a_sql::$err); }
   else{ return _js::enc2($q); }
  }
  static public function verifyPc($D=[]){
    if(_js::iseErr($D['pcId'],'Debe definir el ID del cajero del turno','numeric>0')){}
    else if(_js::iseErr($D['pcPass'],'Debe digitar la clave del cajero')){}
    else{
      $qf=a_sql::fetch('SELECT pcId,pcName FROM gvp_opca WHERE pcId=\''.$D['pcId'].'\' AND pcPass = BINARY \''.$D['pcPass'].'\' LIMIT 1',[1=>'Error realizando login de cajero',2=>'Los datos de acceso del cajero no coinciden']);
      if(a_sql::$err){ _err::err(a_sql::$errNoText); }
    }
  }
  static public function status($D=[],$salto=false){
    if($salto!='noPass'){ self::verifyPc($D); }
    if(!_err::$err){
      $qf=a_sql::fetch('SELECT accId,docEntry,docStatus,balOpen,crId,pcId,openDate,closeDate
      FROM gvp_otur 
      WHERE pcId=\''.$D['pcId'].'\' AND docStatus=\'O\' LIMIT 1',[1=>'Error revisando turnos abiertos']);
      //print_r($qf);
      if(a_sql::$err){ _err::err(a_sql::$errNoText); }
      else if(a_sql::$errNo==2){ $js=_js::enc2(['open'=>'N']); }
      else if(a_sql::$errNo==-1){
        $D['closeDate']=($qf['closeDate']) ?$qf['closeDate'] : date('Y-m-d H:i:59');
        $qb=a_sql::fetch('SELECT 
        SUM(IF(AC.lineType=\'RC\',AC.debBal,0)) balRcv,
        SUM(IF(AC.lineType=\'RP\',AC.creBal,0)) balRce
        FROM gfi_dac1 AC
        WHERE AC.canceled=\'N\' AND AC.accId=\''.$qf['accId'].'\' AND AC.lineType IN(\'RC\',\'RP\') AND AC.dateC>=\''.$qf['openDate'].'\' AND AC.dateC<=\''.$D['closeDate'].'\' ',[1=>'Error obteniendo movimientos de la cuenta']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else{
          if(_js::ise($qb['balRcv'],'','numeric')){ $qb['balRcv']=0; }
          if(_js::ise($qb['balRce'],'','numeric')){ $qb['balRce']=0; }
          $js=_js::enc2(['open'=>'Y','docEntry'=>$qf['docEntry'],'crId'=>$qf['crId'],'pcId'=>$qf['pcId'],
          'balOpen'=>$qf['balOpen'],
          'balRcv'=>$qb['balRcv'],'balRce'=>$qb['balRce'],
          'balCloseCalc'=>$qf['balOpen']+$qb['balRcv']*1-$qb['balRce']*1]);
        }
      }
    }
    _err::errDie();
    return $js;
  }
  static public function open($D=[]){
    if(_js::iseErr($D['crId'],'Debe seleccionar la terminal','numeric>0')){}
    else if(_js::iseErr($D['balOpen'],'El saldo inicial debe estar definido','numeric')){}
    else if(_js::iseErr($D['openDate'],'Se debe definir el inicio del turno')){}
    else if(_js::iseErr($D['closeDate'],'Se debe definir el final del turno')){}
    else if($D['openDate']>$D['closeDate']){ _err::err('El inicio del turno no puede ser mayor que el final del turno',3); }
    else{
      a_sql::transaction(); $c=false; //login
      self::verifyPc($D);
      if(!_err::$err){//turno abierto para cajero
        a_sql::fetch('SELECT openDate FROM gvp_otur WHERE pcId=\''.$D['pcId'].'\' AND docStatus=\'O\' LIMIT 1',[1=>'Error revisando turnos abiertos para el cajero']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else if(a_sql::$errNo==-1){ _err::err('El cajero tiene un turno abierto actualmente, debe cerrarlo para continuar.',3); }
      }
      if(!_err::$err){//turno abierto para terminal
        a_sql::fetch('SELECT openDate FROM gvp_otur WHERE crId=\''.$D['crId'].'\' AND docStatus=\'O\' LIMIT 1',[1=>'Error revisando turnos abiertos para el terminal']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else if(a_sql::$errNo==-1){ _err::err('Otro cajero tiene abierto un turno para este terminal, no puede abrirse el mismo terminal.',3); }
      }
      $DI=['pcId'=>$D['pcId'],'crId'=>$D['crId'],'balOpen'=>$D['balOpen'],'openDate'=>$D['openDate'],'closeDate'=>$D['closeDate']];
      if(!_err::$err){//obtener cuenta
        $qac=a_sql::fetch('SELECT A.fdpIdEFE,FP.accId 
        FROM gvp_ocre A LEFT JOIN gfi_ofdp FP ON (FP.fpId=A.fdpIdEFE)
        WHERE crId=\''.$D['crId'].'\' LIMIT 1',[1=>'Error obtiendo información del terminal',2=>'El terminal no existe']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else if(_js::iseErr($qac['fdpIdEFE'],'El terminal no tiene definida el medio de pago en efectivo','numeric>0')){}
        else if(_js::iseErr($qac['accId'],'El medio de pago en efectivo no está asociado a ninguna cuenta contable','numeric>0')){}
        $DI['accId']=$qac['accId'];
      }
      if(!_err::$err){//abrir
        $docEntry=a_sql::qInsert($DI,['tbk'=>'gvp_otur','qk'=>'ud','qku'=>'ud']);
      }
      if(!_err::$err){ $c=true;
        $js= _js::r('Iniciado',['docEntry'=>$docEntry,'crId'=>$D['crId'],'pcId'=>$D['pcId']]);
      }
      a_sql::transaction($c);
    }
    _err::errDie(); return $js;
  }
  static public function close($D=[]){
    if(_js::iseErr($D['docEntry'],'ID del turno debe estar definido','numeric>0')){}
    else if(_js::iseErr($D['pcPass'],'Debe digitar la clave del cajero')){}
    else if(_js::iseErr($D['balOpen'],'El saldo inicial debe estar definido','numeric')){}
    else if(_js::iseErr($D['balClose'],'El saldo final debe estar definido','numeric')){}
    else if($D['balRcv']!=0 && _js::iseErr($D['balRcv'],'Se deben definir los ingresos','numeric')){}
    else if($D['balRce']!=0 && _js::iseErr($D['balRce'],'Se deben definir los egresos','numeric')){}
    else if(_js::iseErr($D['balAjus'],'Se deben definir el valor del ajuste','numeric')){}
    if(!_err::$err){
      $D['balDiff']=$D['balClose']-($D['balOpen']+$D['balRcv']-$D['balRce']);
      if($D['balDiff']!=$D['balAjus']){ _err::err('La diferencia ('.$D['balDiff'].') debe ser estar definidad en el ajuste ('.$D['balAjus'].') y ser iguales.',3); }
      else if($D['balDiff']!=0 && _js::iseErr($D['lineMemoClose'],'Debe especificar el motivo de la diferencia del cierre, la diferencia es de '.$D['balDiff'].'')){}
    }
    if(!_err::$err){
      /*cierre actual o con fecha del turno modificado*/
      $DI=['docStatus'=>'C','closeDate'=>date('Y-m-d H:i:s'),
      'balSin'=>$D['balSin'],'balRcv'=>$D['balRcv'],'balRce'=>$D['balRce'],'balAjus'=>$D['balAjus'],'balClose'=>$D['balClose'],'lineMemoClose'=>$D['lineMemoClose']];
      a_sql::transaction(); $c=false;
      self::verifyPc($D);
      if(!_err::$err){//verificar turno
        $qf=a_sql::fetch('SELECT closeDate,docStatus FROM gvp_otur WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error revisando turnos abiertos','El turno a cerrar no existe']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else if($qf['docStatus']=='N'){ _err::err('El turno está anulado',3); }
        else if($qf['docStatus']=='C'){ _err::err('El turno ya se encuentra cerrado',3); }
        //usar cierre solo asi
        if($qf['closeDate']!=null && $qf['closeDate']!='0000-00-00 00:00:00'){ $DI['closeDate']=$qf['closeDate']; }
      }
      if(!_err::$err){//cerrarlo
        a_sql::qUpdate($DI,['tbk'=>'gvp_otur','qku'=>'ud','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
      }
      if(!_err::$err){ $c=true; $js= _js::r('Turno cerrado correctamente'); }
      a_sql::transaction($c);
    }
    _err::errDie(); return $js;
  }
  static public function putOne($D=[]){
    if(_js::iseErr($D['docEntry'],'ID del turno debe estar definido','numeric>0')){}
    else if(_js::iseErr($D['pcPass'],'Debe digitar la clave del cajero')){}
    else if(_js::iseErr($D['openDate'],'Apertura debe estar definida')){}
    //else if(_js::iseErr($D['closeDate'],'Cierre debe estar definida')){}
    else if(_js::iseErr($D['balOpen'],'El saldo inicial debe estar definido','numeric')){}
    if(!_err::$err){
      /*cierre actual o con fecha del turno modificado*/
      $DI=['openDate'=>$D['openDate'],'closeDate'=>$D['closeDate'],'balOpen'=>$D['balOpen']];
      a_sql::transaction(); $c=false;
      {//verificar turno
        $qf=a_sql::fetch('SELECT pcId,closeDate,docStatus FROM gvp_otur WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error revisando turnos abiertos','El turno a cerrar no existe']);
        if(a_sql::$err){ _err::err(a_sql::$errNoText); }
        else if($qf['docStatus']=='N'){ _err::err('El turno está anulado',3); }
        else if($qf['docStatus']=='C'){ _err::err('No puede modificar un turno cerrado',3); }
        //if($qf['closeDate']){ $DI['closeDate']=$qf['closeDate']; }
        $D['pcId']=$qf['pcId'];
      }
      if(!_err::$err){ /*contraseña cajero*/
        self::verifyPc($D);
      }
      if(!_err::$err){//cerrarlo
        a_sql::qUpdate($DI,['tbk'=>'gvp_otur','qku'=>'ud','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
      }
      if(!_err::$err){ $c=true; $js= _js::r('Turno actualizado correctamente'); }
      a_sql::transaction($c);
    }
    _err::errDie(); return $js;
  }
  static public function statusCancel($D=array()){
    a_sql::transaction(); $cmt=false;
    _ADMS::lib('iDoc');
    iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>'gvp_otur','docEntry'=>$D['docEntry']));
    if(_err::$err){ return _err::$errText; }
    else{ $cmt=true; $js=_js::r('Turno anulado correctamente.'); }
      a_sql::transaction($cmt);
    return $js;
  }
  static public function audit($D){
    $R=a_sql::fetchL('SELECT 
    T.pcId,T.crId,
    T.docStatus,T.balOpen,T.balClose,T.balAjus,T.openDate,T.closeDate,A.lineType,A.canceled,A.serieId,A.docNum,A.tt,A.tr,A.debBal,A.creBal,A.debBalDue,A.creBalDue,A.dateC,C.cardName,PC.accName,T.lineMemoClose
    FROM gvp_otur T 
    JOIN gfi_opdc PC ON (PC.accId=T.accId)
    JOIN gfi_dac1 A ON (
      A.accId=T.accId AND A.lineType IN (\'RC\',\'RP\') AND A.dateC>=T.openDate
      AND ( T.closeDate IN(null,\'0000-00-00 00:00:00\') 
        OR (A.dateC<=T.closeDate)
      )
    )
    LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
    WHERE T.docEntry=\''.$D['docEntry'].'\' ORDER BY A.acId ASC',['k'=>'L']);
    $R['I']=a_sql::fetchL('SELECT A.lineType,A.canceled,A.serieId,A.docNum,A.tt,A.tr,A.debBal,A.creBal,A.debBalDue,A.creBalDue,A.dateC,C.cardName
    FROM gvp_otur T 
    JOIN gvp_ocre CR ON (CR.crId=T.crId)
    JOIN gfi_dac1 A ON (A.lineType =\'FV\' AND A.serieId=CR.docSin AND A.dateC>=T.openDate
    AND ( T.closeDate IN(null,\'0000-00-00 00:00:00\') 
      OR (A.dateC<=T.closeDate)
    ))
    LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
    WHERE T.docEntry=\''.$D['docEntry'].'\' ORDER BY A.acId ASC');
    return _js::enc2($R);
  }
}
?>