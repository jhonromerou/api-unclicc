<?php
if(!$_HTML){
$_HTML=[
'docEntry'=>$_GET['docEntry'],'docNum'=>'CC-111','docDate'=>'2021-01-01','dueDate'=>'2021-01-16',
'cardName'=>'Geotecnia Ingenieria SAS','licTradType'=>'NIT','licTradNum'=>'1125082294',
'address'=>'Calle 43 #8-10, Los Reyes. Dosquebradas, Risaralda','phone1'=>'(576) 342 0830','email'=>'info@geotecniaingenieria.co','slpName'=>'Jhon Romero','pymGr'=>'Contado',
'lineNmo'=>'Para efectos de la aplicación de la tabla de retenc',
'L'=>[
 ['itemName'=>'Bota Seguridad Negra','price'=>10000,'quantity'=>10,'priceLine'=>100000,'lineText'=>'Disponible en talla 38 a 45'],
 ['itemName'=>'Bota Seguridad Negra','price'=>10000,'quantity'=>10,'priceLine'=>100000,'lineText'=>'Disponible en talla 38 a 45'],
],
'o'=>[
    'ocardName'=>'ADMS Sistems','address'=>'Carrera 8 #43 - 01, Los Reyes','pbx'=>'+57 323 338 9324','mail'=>'info@admsistems.com',
]
];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php
		echo '<!-- .GVP_ticketSize='.c::$V['.GVP_ticketSize'].' -->'."\n";
		if(c::$V['.GVP_ticketSize']=='58mm'){
			echo '<link rel="stylesheet" href="'.c::$V['tdocsTemp'].'/gvp.ticket.58.css?temp=113" />';
		}
		else if(c::$V['.GVP_ticketSize']=='80mm'){
			echo '<link rel="stylesheet" href="'.c::$V['tdocsTemp'].'/gvp.ticket.80.css?temp=117" />';
		}
		else{ echo '<link rel="stylesheet" href="'.c::$V['tdocsTemp'].'/gvp.ticket1.css?temp=113" />'; }
		?>
    <title>Tirilla POS</title>  
    <?php //include('comun.css'); ?>
</head>
<body onload="window.print(); ">
<div class="posTicket1">
<div class="centrado">
<div><?php echo $_HTML['o']['ocardName']; ?></div>
<div><?php echo $_HTML['o']['licTradNum']; ?></div>
<div><?php echo $_HTML['o']['address']; ?></div>
<div><?php echo $_HTML['o']['cityCode']; ?></div>
<div><?php echo $_HTML['o']['pbx']; ?></div>
<hr/>
Doc. Equiv.: <?php echo $_HTML['docNumText']; ?></span>
<br/>Cliente: <span><?php echo $_HTML['cardName']; ?></span>
<br/>Doc: <span><?php echo $_HTML['licTradNum']; ?></span>
<br/>Atendido por:<span id="pcName"><?php echo $_HTML['pcName']; ?></span>
<br/><span id="dateC"><?php echo $_HTML['dateC']; ?>1</span>
</div>
<br/>
<table id="docTable">
<thead>
<tr>
	<th class="quantity">Cant.</th>
	<th class="item">Detalle</th>
	<th class="price">Precio</th>
</tr>
</thead>
<tbody>
<?php
foreach($_HTML['L'] as $n=>$L){
	echo '<tr>
	<td class="quantity">'.($L['quantity']*1).'</td>
	<td class="item">'.$L['itemName'].'</td>
	<td class="price">'._js::money($L['priceLine']).'</td>
</tr>';
}
?>
</tbody>
</table>
<div id="docTotals" style="text-align:right;">
	<div><b>Subtotal</b>&nbsp;&nbsp;&nbsp;<span id="baseAmnt"><?php echo _js::money($_HTML['baseAmnt']); ?></span></div>
	<div><b>Impuestos</b>&nbsp;&nbsp;&nbsp;<span id="vatTotal"><?php echo _js::money($_HTML['vatSum']-$_HTML['rteSum']); ?></span></div>
	<div><b>Total</b>&nbsp;&nbsp;&nbsp;<span id="docTotal"><?php echo _js::money($_HTML['docTotal']); ?></span></div>
</div>
<div id="balGetWrap" style="text-align:left;">
		<hr/>
		<div><b>Valor pagado</b>&nbsp;&nbsp;&nbsp;<span id="balGet"><?php echo _js::money($_HTML['balGet']); ?></span></div>
		<div><b>Cambio</b>&nbsp;&nbsp;&nbsp;<span id="balRet"><?php echo _js::money($_HTML['balRet']); ?></span></div>
	</div>
<p>&nbsp;</p>
<p class="softFrom">Elaborado con unClicc!
<br>www.admsistems.com/unclicc</p>
<p class="softFrom">Gracias por su compra</p>
<p>&nbsp;</p>
</div>
</body>
</html>