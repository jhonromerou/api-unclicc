Api.Gda={b:'/a/gda/'};

$js.push($V.docSerieType,{gdaDba:'Plantilla de Auditoria'});
$Doc.ttAdd({
	gdaDba:{b:'gda.dba',t:'Plantilla de Auditoria'},
	gdaAud:{b:'gda.aud',t:'Doc. de Auditoria'},
	dgaNc:{b:'gda.Nc',t:'No Conformidad'}
});

$V.gdaDocType=[{k:'Doc',v:'Documentación'},{k:'PT',v:'Papel de Trabajo'},{k:'R',v:'Registro'},{k:'MC',v:'Manual Calidad'}];
$V.gdaStageDoc=[]; //Se define en cada Doc

$M.sAdd([
	{L:[{folId:'gda',folName:'Auditoria'}]},
	{fatherId:'gda',MLis:['gda.dba','gda.aud','gda.nc']}
]);

$DocTb.kTb['gdaDbaL']={
proceso:['Proceso',{tag:'input',kf:'proceso',k:'proceso'}],
stage:['Etapa',{tag:'select',kf:'stage',k:'stage',_Vopts:'gdaStageDoc'}],
assg:['Responsable',{tag:'input',kf:'assg',k:'assg'}],
docDate:['Fecha',{tag:'date',kf:'docDate',k:'docDate',style:'width:9rem'}],
hourTxt:['Horario',{tag:'input',kf:'hourTxt',k:'hourTxt',style:'width:8rem'}],
audit:['Auditor Lider',{tag:'input',kf:'audit',k:'audit'}],
teamAudit:['Equipo Auditor',{tag:'input',kf:'teamAudit',k:'teamAudit'}],
lineMemo:['Detalles',{tag:'textarea',kf:'lineMemo',k:'lineMemo',style:'width:400px; resize:auto'}],
};
$DocTb.kTb['gdaNcL']={
lineStatus:['Estado',{tag:'select',kf:'lineStatus',k:'lineStatus',_Vopts:'docStatus1',noBlank:'Y'}],
accion:['Acción',{tag:'input',kf:'accion',k:'accion'}],
assg:['Responsable',{tag:'input',kf:'assg',k:'assg'}],
lineDue:['Plazo Hasta',{tag:'date',kf:'lineDue',k:'lineDue',style:'width:9rem'}]
};

_Fi['gfp.wal']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'M.docDate(E_menIgual)'}},wrap);
		$1.T.divL({wxn:'wrapx8', L:'Tipo',I:{tag:'select','class':jsV,name:'A.wallType',opts:$V.gfpWallType}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.wallName(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Gfp.Wal.get},wrap);
};

$M.li['gda.dba']={t:'Plantilas de Auditoria', kau:'gda',func:function(){
	var wrap=$1.t('div');
	$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo',func:function(){ $M.to('gda.dba.form'); }},wrap)
	$M.Ht.ini({btnNew:wrap, func_filt:'gda.dba', func_pageAndCont:Gda.Dba.get});
}};
$M.li['gda.dba.form']={t:'Plantilla de Auditoria', kau:'gda.write',func:function(){ $M.Ht.ini({func_cont:Gda.Dba.form}); }};
$M.li['gda.dba.view']={noTitle:'Y', kau:'gda',func:function(){ $M.Ht.ini({func_cont:Gda.Dba.view}); }};
$M.li['gda.dba.docs']={t:'Documentación para Auditoria',kau:'gda.write',func:function(){ $M.Ht.ini({func_cont:Gda.Dba.docs}); }};

$M.li['gda.aud']={t:'Doc. de Auditoria', kau:'gda',func:function(){
	var wrap=$1.t('div');
	$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo',func:function(){ $M.to('gda.aud.form'); }},wrap)
	$M.Ht.ini({btnNew:wrap, func_filt:'gda.aud', func_pageAndCont:Gda.Aud.get});
}};
$M.li['gda.aud.form']={t:'Doc. Base de Auditoria', kau:'gda.write',func:function(){ $M.Ht.ini({func_cont:Gda.Aud.form}); }};
$M.li['gda.aud.view']={noTitle:'Y', kau:'gda',func:function(){ $M.Ht.ini({func_cont:Gda.Aud.view}); }};
$M.li['gda.aud.docs']={t:'Documentación para Auditoria',kau:'gda.write',func:function(){ $M.Ht.ini({func_cont:Gda.Aud.docs}); }};

$M.li['gda.nc']={t:'No Conformidades', kau:'gdaNc',func:function(){
	var wrap=$1.t('div');
	$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo',func:function(){ $M.to('gda.aud.form'); }},wrap)
	$M.Ht.ini({btnNews:wrap, func_filt:'gda.nc', func_pageAndCont:Gda.Nc.get});
}};
$M.li['gda.nc.form']={t:'No Conformidad - Registro', kau:'gdaNc.write',func:function(){ $M.Ht.ini({func_cont:Gda.Nc.form}); }};
$M.li['gda.nc.view']={noTitle:'Y', kau:'gdaNc',func:function(){ $M.Ht.ini({func_cont:Gda.Nc.view}); }};

var Gda={};
Gda.Dba={
OLg:function(L){
	var Li=[]; var n=0;
	Li.push({ico:'fa fa-eye',textNode:' Visualizar Doc.', P:L, func:function(T){ $Doc.go('gda.dba','v',T.P,1); } });
	Li.push({k:'edit',ico:'fa fa-edit',textNode:' Modificar', P:L, func:function(T){ $Doc.go('gda.dba','f',T.P,1); } });
	Li.push({k:'docs',ico:'fa fa-files-o',textNode:' Documentación', P:L, func:function(T){ $Doc.go('gda.dba','docs',T.P,1); } });
	Li.push({k:'createAud',ico:'fa fa-clone',textNode:' Generar Nueva Auditoria', P:L, func:function(T){ Gda.Aud.fromDba(T.P); } });
	return $Opts.add('gda.dba',Li,L);;
},
opts:function(P,pare){
	Li={Li:Gda.Dba.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.Gda.b+'dba',inputs:$1.G.filter(),
	fOpts:Gda.Dba.opts,btns:'edit',view:'Y',docBy:'userDate',docUpd:'userDateUpd',
	tt:'gda.dba',
	TD:[
		{H:'Código',k:'docCode'},
		{H:'Versión',k:'version'},
		{H:'Area',k:'area',_V:'gdaAreas'},
		{H:'Clase',k:'docClass',_V:'gdaClass'},
		{H:'Nombre',k:'dbaName'},
		{H:'Tiene Etapas',k:'doStages',_V:'NY'}
	],
	tbExport:{ext:'xlsx',fileName:'Listado Doc. Base Auditoria'}
	},cont);
},
form:function(){
	Gda.Def.form({api:Api.Gda.b+'dba',go:'gda.dba',isTemplate:'Y'});
},
docs:function(){
	Gda.Def.docs({api:Api.Gda.b+'dba/docs',isTemplate:'Y'});
},
view:function(){
	Gda.Def.view({api:Api.Gda.b+'dba',isTemplate:'Y',
	btnsTop:{ks:'print,edit,task,docs,createAud,',icons:'Y',Li:Gda.Dba.OLg}
	});
},
}

Gda.Aud={
OLg:function(L){
	var Li=[]; var n=0;
	Li.push({ico:'fa fa-eye',textNode:' Visualizar Doc.', P:L, func:function(T){ $Doc.go('gda.aud','v',T.P,1); } });
	Li.push({k:'edit',ico:'fa fa-edit',textNode:' Modificar', P:L, func:function(T){ $Doc.go('gda.aud','f',T.P,1); } });
	Li.push({k:'formNc',ico:'fa fa-file-o',textNode:' Registrar No Conformidad', P:L, func:function(T){ var wrap=$1.t('div');
		$1.Win.open(wrap,{winTitle:'Nueva No Conformidad en Auditoria'});
	} });
	Li.push({k:'formDocs',ico:'fa fa-attach',textNode:' Documentación', P:L, func:function(T){ $Doc.go('gda.aud','docs',T.P,1); } });
	return $Opts.add('gda.aud',Li,L);
},
opts:function(P,pare){
	Li={Li:Gda.Aud.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.Gda.b+'aud',inputs:$1.G.filter(),
	fOpts:Gda.Aud.opts,btns:'edit',view:'Y',docBy:'userDate',docUpd:'userDateUpd',
	tt:'gda.aud',
	TD:[
		{H:'Código',k:'docCode'},
		{H:'Versión',k:'version'},
		{H:'Area',k:'area',_V:'gdaAreas'},
		{H:'Clase',k:'docClass',_V:'gdaClass'},
		{H:'Nombre',k:'dbaName'},
		{H:'Tiene Etapas',k:'doStages',_V:'NY'}
	],
	tbExport:{ext:'xlsx',fileName:'Listado Doc. Auditoria'}
	},cont);
},
form:function(){
	Gda.Def.form({api:Api.Gda.b+'aud',go:'gda.aud'});
},
view:function(){
	Gda.Def.view({api:Api.Gda.b+'aud',
	btnsTop:{ks:'print,edit,formNc,',icons:'Y',Li:Gda.Aud.OLg}
	});
},
fromDba:function(P){
	var cont=$1.t('div');
	var jsF=$Api.JS.cls;
	var tP={go:'gdaAud',cont:cont, Series:'N', docEntry:'N', jsF:jsF,POST:Api.Gda.b+'aud/fromDba',
		func:null,
		tbHead:[
		{lTag:'date',wxn:'wrapx8',L:'Fecha',I:{'class':jsF,name:'docDate',value:$2d.today,
		AJs:{docEntry:P.docEntry} }},
		{lTag:'crd',wxn:'wrapx2',L:'Cliente',req:'Y',I:{'class':jsF,fie:'slpId,fdpId,pymId,countyCode,cityCode,address,phone1,rteIva,rteIca',topPare:cont}}
		],
		};
	$Doc.form(tP);
	$1.Win.open(cont,{winTitle:'Nueva Auditoria desde Plantilla',winSize:'medium'});
}
}

Gda.Def={
topInfo:function(Jr,pare){
	$1.t('span',{textNode:'Versión: '+Jr.version},pare);
	$1.t('span',{textNode:'\u0020\u0020Código: '+Jr.docCode},pare);
	$1.t('h4',{textNode:Jr.dbaName},pare);
},
form:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:P.api+'/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={go:P.go,docEntry:Pa.docEntry, cont:cont, Series:'N', jsF:jsF,POST:P.api,
		func:null,
		tbHead:[
		{lTag:'input',wxn:'wrapx8',L:'Versión',req:'Y',I:{'class':jsF,value:Jr.version,name:'version'}},
		{lTag:'input',wxn:'wrapx8',L:'Código',req:'Y',I:{'class':jsF,value:Jr.docCode,name:'docCode'}},
		{lTag:'input',wxn:'wrapx2',L:'Nombre Auditoria',req:'Y',I:{'class':jsF,value:Jr.dbaName,name:'dbaName'}},
		{divLine:1,lTag:'select',wxn:'wrapx8',L:'Area',req:'Y',I:{'class':jsF,name:'area',value:Jr.area,opts:$V.gdaAreas}},
		{lTag:'select',wxn:'wrapx8',L:'Clase',I:{'class':jsF,name:'docClass',value:Jr.docClass,opts:$V.gdaClass}},
		{lTag:'select',wxn:'wrapx8',L:'Grupo',I:{'class':jsF,name:'docGr',value:Jr.docGr,opts:$V.gdaGr}},
		{lTag:'input',wxn:'wrapx2',L:'Etapas',subText:'Planeacion,Revision,Reunion',I:{'class':jsF,name:'stages',value:Jr.stages}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Objetivo',I:{'class':jsF,name:'objetivo',textNode:Jr.objetivo,style:'resize:vertical'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Alcance',I:{'class':jsF,name:'alcance',textNode:Jr.alcance,style:'resize:vertical'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Descripción',I:{'class':jsF,name:'descrip',textNode:Jr.descrip,style:'resize:vertical'}}
		],
		};
		if(Pa.docEntry){ tP.PUT=tP.POST; delete(tP.POST); }
		tP.midCont=$1.t('div');
		$Doc.form(tP);
		Gda.Def.task({isTemplate:P.isTemplate,Jr:Jr},tP.midCont);
	}});
},
task:function(P,pare){
	var cont=pare;
	var Jr=P.Jr;
	if(Jr.errNo){ return $Api.resp(cont,Jr); }
	if(Jr.L && Jr.L.errNo==1){ $Api.resp(wAdd,Jr.L); }
	var wAdd=$1.t('div',0,cont);
	if(Jr.L && Jr.L.errNo){ Jr.L=[]; }
	Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
	kFie='proceso,assg,docDate,hourTxt,audit,teamAudit,lineMemo';
	if(P.isTemplate){ kFie='proceso,assg,lineMemo'; }
	if(Jr.doStages=='Y'){
		$V.gdaStageDoc=$1.optsFromTxt(Jr.stages);
		kFie='proceso,stage,assg,docDate,hourTxt,audit,teamAudit,lineMemo';
		if(P.isTemplate){ kFie='proceso,stage,assg,lineMemo'; }
	}
	var Rd=$DocTb.ini({xMov:'Y',xNum:'N',xDel:'Y',L:Jr.L,kTb:'gdaDbaL',
	btnAddL:'Y',fieldset:'Tareas de Auditoria',
	kFie:kFie
	});
	wAdd.appendChild(Rd.fieldset);
},
docs:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read();
	$Api.get({f:P.api,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		if(Jr.errNo){ return $Api.resp(cont,Jr); }
		Gda.Def.topInfo(Jr,cont);
		var jsF=$Api.xFields;
		var wAdd=$1.t('div',0,cont);
		var wList=$1.t('div',0,cont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tipo',I:{tag:'select','class':jsF,name:'docType',opts:$V.gdaDocType,noBlank:'Y'}},wAdd);
		$1.T.divL({wxn:'wrapx4',L:'Nombre Documento',req:'Y',I:{tag:'input',type:'text','class':jsF,name:'docName',AJs:{docEntry:Pa.docEntry}}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Archivo',req:'Y',I:{tag:'input',type:'file','class':jsF,name:'file'}},divL);
		var resp=$1.t('div',0,wAdd);
		$Api.send({textNode:'Añadir Archivo',POST:P.api,formData:wAdd,loade:resp,func:function(Jr2,o){
			$Api.resp(resp,Jr2);
			if(o && o.fileId){
				Gda.Dba.trADocs(o,tBody);
			}
		}},wAdd);
		//lista
		if(Jr.L && !Jr.L.errNo){
			var tb=$1.T.table(['Tipo','Nombre','Archivo'],0,wList);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ Gda.Def.trADocs(Jr.L[i],tBody); }
		}
	}});
},
trADocs:function(L,tBody){
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:_g(L.docType,$V.gdaDocType)},tr);
	$1.t('td',{textNode:L.docName},tr);
	var td=$1.t('td',0,tr);
	Attach.btnView(L,td,{btnDelete:'N'});
},
view:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:P.api+'/view',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		Jr.L=$js.sortBy('lineNum',Jr.L);
		var tP={D:Jr,
			btnsTop:P.btnsTop,
			THs:[
				{t:'Proceso',k:'dbaName',cs:5},{k:'version',t:'Versión',ln:1},
				{logo:'Y',ln:'N'},{middleInfo:'Y'},{k:'docCode',t:'Código',ln:1},
				{t:'Area',k:'area',_V:'gdaAreas'},
				{t:'Clase',k:'docClass',_V:'gdaClass'},
				{k:'objetivo',cs:8,addB:$1.t('b',{textNode:'Objetivo:\u0020'}),Tag:{'class':'pre'}},
				{k:'alcance',cs:8,addB:$1.t('b',{textNode:'Alcance:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',fieldset:'Programacion',tb:{style:'fontSize:14px'},TLs:[
				{t:'No.',k:'lineNum'},
				{t:'Etapa',k:'stage',_V:'gdaStageDoc'},
				{k:'proceso',t:'Actividad'},
				{k:'assg',t:'Responsable'},
				{k:'lineMemo',t:'Detalles',Tag:{'class':'pre'}}
			]}
			],
			TFs:null
		};
		tP.mTL=[
		{L:'L',fieldset:'Programacion',tb:{style:'fontSize:14px'},TLs:[
			{t:'No.',k:'lineNum'},
			{t:'Etapa',k:'stage',_V:'gdaStageDoc'},
			{k:'proceso',t:'Actividad'},
			{k:'assg',t:'Responsable'}
		]}
		];
		if(P.isTemplate!='Y'){ 
			tP.mTL[0].TLs.push({k:'docDate',t:'Fecha'});
			tP.mTL[0].TLs.push({k:'hourTxt',t:'Horario'});
			tP.mTL[0].TLs.push({k:'audit',t:'Auditor'});
			tP.mTL[0].TLs.push({k:'teamAudit',t:'Equipo Auditor'});
		}
		tP.mTL[0].TLs.push({k:'lineMemo',t:'Detalles',Tag:{'class':'pre'}});
		if(Jr.doStages=='N'){ delete(tP.mTL[0].TLs[1]); }
		$V.gdaStageDoc=$1.optsFromTxt(Jr.stages);
		$Doc.view(cont,tP);
	}});
}
}

Gda.Nc={
OLg:function(L){
	var Li=[]; var n=0;
	Li.push({ico:'fa fa-eye',textNode:' Visualizar Doc.', P:L, func:function(T){ $Doc.go('gda.nc','v',T.P,1); } });
	Li.push({k:'edit',ico:'fa fa-edit',textNode:' Modificar', P:L, func:function(T){ $Doc.go('gda.nc','f',T.P,1); } });
	if(L.docStatus!='N'){
		Li.push({k:'statusN',ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.statusDefine({reqMemo:'N',docEntry:T.P.docEntry,api:Api.Gda.b+'nc/statusCancel',text:'Se va anular el documento.'}); } });
	}
	return $Opts.add('gda.ncc',Li,L);;
},
opts:function(P,pare){
	Li={Li:Gda.Nc.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.Gda.b+'nc',inputs:$1.G.filter(),
	fOpts:Gda.Nc.opts,btns:'edit',view:'Y',docBy:'userDate',docUpd:'userDateUpd',
	tt:'gda.nc',
	TD:[
		{H:'Fecha',k:'docDate'},
		{H:'Estado',k:'docStatus',_V:'dStatus'},
		{H:'Origen',k:'origen',_V:'gdaNcOrigen',tt:'tt',tr:'tr'},
		{H:'Tipo',k:'docType',_V:'gdaNcType'},
		{H:'Titulo',k:'docTitle'}
	],
	tbExport:{ext:'xlsx',fileName:'Listado No Conformidades'}
	},cont);
},
form:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	var api=Api.Gda.b+'nc';
	$Api.get({f:api+'/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={go:P.go,docEntry:Pa.docEntry, cont:cont, Series:'N', jsF:jsF,POST:api,
		func:null,
		tbHead:[
		{lTag:'select',wxn:'wrapx8',L:'Origen',req:'Y',I:{'class':jsF,name:'origen',value:Jr.origen,opts:$V.gdaNcOrigen}},
		{lTag:'select',wxn:'wrapx8',L:'Tipo',req:'Y',I:{'class':jsF,name:'docType',value:Jr.docType,opts:$V.gdaNcType}},
		{lTag:'input',wxn:'wrapx2',L:'No Conformidad',req:'Y',I:{'class':jsF,value:Jr.docTitle,name:'docTitle'}},
		{divLine:1,lTag:'select',wxn:'wrapx8',L:'Estado',req:'Y',I:{'class':jsF,name:'docStatus',value:Jr.docStatus,opts:$V.docStatus1,noBlank:'Y'}},
		{lTag:'date',wxn:'wrapx8',L:'Fecha',req:'Y',I:{'class':jsF,value:Jr.docDate,name:'docDate'}},
		{lTag:'date',wxn:'wrapx8',L:'Fecha Limite',req:'Y',I:{'class':jsF,value:Jr.dueDate,name:'dueDate'}},
		{lTag:'select',wxn:'wrapx4',L:'Requiere Acción Correctiva',req:'Y',I:{'class':jsF,name:'reqAc',value:Jr.reqAc,opts:$V.NY,noBlank:'Y'}},
		{lTag:'select',wxn:'wrapx4',L:'Requiere Acción Preventiva',req:'Y',I:{'class':jsF,name:'reqAp',value:Jr.reqAp,opts:$V.NY,noBlank:'Y'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Acción a tomar',I:{'class':jsF,name:'descrip',textNode:Jr.descrip,style:'resize:vertical'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Causas',I:{'class':jsF,name:'descrip',textNode:Jr.descrip,style:'resize:vertical'}}
		],
		};
		if(Pa.docEntry){ tP.PUT=tP.POST; delete(tP.POST); }
		tP.Forms=[
		{xMov:'Y',xNum:'N',xDel:'Y',L:Jr.L,sortBy:'lineNum',kTb:'gdaNcL',btnAddL:'Y',kFie:'lineStatus,accion,assg,lineDue',fieldset:'Plan de Acción'},
		{divL:[{divLine:1,wxn:'wrapx1',L:'Notas de las acciones realizadas',I:{tag:'textarea','class':jsF,name:'accionMemo'}}]}
		];
		$Doc.form(tP);
		//Gda.Nc.task({Jr:Jr},tP.midCont);
	}});
},
task:function(P,pare){
	var cont=pare;
	var Jr=P.Jr;
	if(Jr.errNo){ return $Api.resp(cont,Jr); }
	if(Jr.L && Jr.L.errNo==1){ $Api.resp(wAdd,Jr.L); }
	var wAdd=$1.t('div',0,cont);
	if(Jr.L && Jr.L.errNo){ Jr.L=[]; }
	Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
	kFie='accion,assg,dueDate';
	var Rd=$DocTb.ini({xMov:'Y',xNum:'N',xDel:'Y',L:Jr.L,kTb:'gdaNcL',
	btnAddL:'Y',fieldset:'Plan de Acción',
	kFie:kFie
	});
	wAdd.appendChild(Rd.fieldset);
},
view:function(){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Gda.b+'nc/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		Jr.L=$js.sortBy('lineNum',Jr.L);
		var tP={D:Jr,
			btnsTop:{ks:'print,edit,statusN,',icons:'Y',Li:Gda.Nc.OLg},
			THs:[
				{docTitle:'Documento de No Conformidad',cs:4},{t:'Origen',k:'origen',_V:'gdaNcOrigen',ln:1},{t:'Tipo',k:'docType',_V:'gdaNcType',ln:1},
				{logo:'Y',ln:'N'},{middleInfo:'Y'},{t:'Estado',k:'docStatus',_V:'dStatus',ln:1},
				{t:'Fecha',k:'docDate'},
				{t:'Fecha Limite',k:'dueDate'},
				{k:'docTitle',cs:8,addB:$1.t('b',{textNode:'No Conformidad:\u0020'})},
				{k:'descrip',cs:8,addB:$1.t('b',{textNode:'Causas de No Conformidad:\u0020'}),HTML:1,Tag:{'class':'pre'}},
				{k:'accionMemo',cs:8,addB:$1.t('b',{textNode:'Notas de las acciones realizadas:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',fieldset:'Plan de Acción',tb:{style:'fontSize:14px'},TLs:[
				{t:'No.',k:'lineNum'},
				{t:'Estado',k:'lineStatus',_V:'docStatus1'},
				{k:'accion',t:'Acción'},
				{k:'assg',t:'Responsable'},
				{k:'lineDue',t:'Plazo'}
			]}
			],
			TFs:null
		};
		$Doc.view(cont,tP);
	}});
}
}
