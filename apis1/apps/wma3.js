$M.s['prod'].L.push({t:'Planificación',L:['wma3.opdp']});


$M.li['wma3.opdp'] ={t:'Programación Producción', kau:'wma3.opdp', func:function(){
	var btnA=$1.T.btnFa({fa:'fa fa_doc',textNode:'Nuevo Documento',func:function(){ $M.go('wma3.opdp.form'); }});
	$M.Ht.ini({btnNew:btnA,func_cont:null});
}};

$M.li['wma3.opdp.form']={t:'Documento Programación Producción', kau:'wma3.opdp', func:function(){
	$M.Ht.ini({func_cont:Wma3.Pdp.form});
}};

var Wma3={};
Wma3.Pdp={
form:function(){
	var cont =$M.Ht.cont; var Pa=$M.read(); jsF='jsFields';
	$ps_DB.get({file:'GET '+Api.Ord.a+'form', loadVerif:!Pa.docEntry, loade:cont, inputs:'docEntry='+Pa.docEntry, func:function(Jr){
	_Doc.formSerie({cont:cont,serieType:'opvt', docEntry:Pa.docEntry, jsF:jsF,
		api:'PUT '+Api.Ord.a+'form', func:function(Jr2){
			$M.to('sellOrd.view','docEntry:'+Jr2.docEntry);
		},
		Li:[
		{fType:'user'},
		{wxn:'wrapx8',L:'Estado',I:{tag:'select',sel:{disabled:'disabled',title:Jr.docStatus},opts:$V.ordStatus,selected:Jr.docStatus,noBlank:1}},
		{fType:'date',name:'docDate',req:'Y',value:Jr.docDate},
		{wxn:'wrapx1',divLine:1,L:'Detalles',I:{tag:'textarea',name:'lineMemo',textNode:Jr.lineMemo}}
		]
	});
	var tb=$1.T.table(['#','Descripción']);
		tb.classList.add('table_x100');
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
		cont.appendChild(fie); var n=1;
		var tBody= $1.t('tbody',0,tb);
	Drw.itemReader({cont:fie,tBody:tBody,fields:'I.itemType&wh[I.prdItem]=Y'},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'table',jsF:jsF,JrS:P.JrS, n:n,noFields:{price:'N',priceList:'N'}}); n++;
	}});
	}});
}
}