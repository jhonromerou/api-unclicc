
$ps_DB.get = function(PARS){
	PARS.headers={'ocardtooken':$0s.stor('ocardtooken')};
	$ps_DB.get1(PARS);
}
var jsFi={'extends':'/com1/js/extends.php'};
$V.dbReportLen={parcial:'Parcial','full':'Completo'};
$V.wopType={I:'Interna',E:'Externa'};
$V.docSerieType={
opvt:'Ped. Venta',ordn:'Dev. Venta',ocvt:'Cot. Venta',odlv:'Desp. Venta',
owht:'Trans.-Stock',oing:'Ent-Inv',oegr:'Sal-Inv',ocat:'Tom-Inv',ocph:'Cambio Homol.',oinc:'Rec. Inv.',
opor:'O. Compra'
};
$Doc.a['opvt']={a:'sellOrd.view'};
$Doc.a['ordn']={a:'sellRdn.view'};
$Doc.a['octv']={a:'sellCtv.view'};
$Doc.a['odlv']={a:'sellDlv.view'};
$Doc.a['owht']={a:'ivt.wht.view'};
$Doc.a['oing']={a:'ivt.ing.view'};
$Doc.a['oegr']={a:'ivt.egr.view'};
$Doc.a['ocat']={a:'ivt.cat.view'};
$Doc.a['ocph']={a:'ivt.cph.view'};
$Doc.a['oinc']={a:'ivt.inc.view'};

$oTy.odlv='odlv';$oTy.opvt='opvt';

$V.itmLog2Type={mpUpdatePrice:'Costo de Materia Prima',model:'Materiales',modelVari:'Materiales (Variantes)',defineCost:'Definición Manual',defineMO:'Mano de Obra',defineCIF:'CIF'};
Api.Ord = {a:'/sa/c70/sellOrd/',s:'/sa/c70/sellOrd/s',dlv:'/sa/c70/sellDlv/'};
Api.Sell = {rep:'/sa/c70/sellRep/',rdn:'/sa/c70/sellRdn/'};
Api.buyOrd = {a:'/sa/c70/buyOrd/'};
Api.Exp = {a:'/sa/c70/exp/'};
Api.Itm = {a:'/sa/c70/itm/',bc:'/sa/c70/itm/bc',chr:'/sa/c70/itm/chr', rc:'/sa/c70/itm/rc'};
Api.Ivt = {a:'/sa/c70/ivt/',b:'/sa/c70/ivt/barcode',whs:'/sa/c70/ivt/whs'};
Api.Barc={a:'/sa/c70/barcode/',s:'/sa/c70/barcode/stickers',};
Api.Wma={a:'/sa/c70/wma/'};

$V.bar2 = {'1':'Contables'};
$V.mathOper={'=':'= Igual','x':'x Mult.', '/':'/ Dividir','%':'% Resto'};
$V.active={'active':'Activo',inactive:'Inactivo'};
$V.faseUdm={hours:'Horas',minutes:'Minutos',seconds:'Segundos',days:'Días'};
$V.ingDocClass={produccion:'Producción',devolucion:'Devolución',cambio:'Cambio',muestra:'Muestra',otros:'Otros'};
$V.workOperat={'1':'Troquelado','2':'Guarnecida'};
$V.egrDocClass={produccion:'Producción',cambio:'Cambio',muestra:'Muestra',otros:'Otros'};
$V.whtDocClass={produccion:'Producción',cambio:'Cambio',muestra:'Muestra',otros:'Otros'};
$V.itemType = {'P':'Modelo','MP':'Materia Prima','SE':'Semi Elaborado'};
$V.itemUdm ={'und':'Und','par':'Par',kgs:'Kgs'};
$V.tipoPuntera={sp:'Sin Puntera',pa:'Puntera Acero',pc:'Puntera Composite'};
$V.tipoLinea={iny:'Inyección',vulc:'Vulcanizado',otro:'Otros'};
$V.variType={N:'Ninguna',tcons:'Consumo por Talla',tcode:'Código por Talla'};
$V.ordStatus ={D:'Borrador',O:'Abierto',C:'Cerrado',N:'Anulado',S:'Enviado'};
$V.rdnStatus ={D:'Borrador',O:'Abierto',C:'Cerrado',N:'Anulado'};
$V.rdnClasif={primera:'Primera',segunda:'Segunda',tercera:'Tercera'};
$V.rdnWarranty={cambiotalla:'Cambio de Talla',cambiocalidad:'Cambio por Calidad',cambioref:'Cambio por Referencia',notacred:'Nota Crédito'};
$V.opvtCartStatus={P:'No revisado',L:'Bloqueado',O:'Autorizado',R:'Revisión',V:'Solicitar Detalles'};

ColMt['opvt']={O:'#0F0',D:'orange',S:'blue',N:'#B1B1B1'};


$M.ttK['FV']={a:'sell.envoiceView',p:{tr:'handNum'}};
$M.ttK['FC']={a:'buy.envoiceView',p:{tr:'handNum'}};
$M.ttK['WTD']={a:'items.WTD.view',p:{tr:'handNum'}};
/* _Fi k:func */
var _Fi= {
'ocrd':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'A.cardCode(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'A.cardName(E_like3)'}},divL);
	if(Pa=='crd.c'){
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Responsable Venta'},I:{tag:'select',sel:{'class':jsV,name:'A.slpId(E_igual)'},opts:$Tb.oslp}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsV,name:'A.grId(E_igual)'},opts:$V.crdGroup}},divL);
	}
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:$crd.get});
	wrap.appendChild(btnSend);
},
'sellDlv.get':function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx10',L:{textNode:'Número'},I:{tag:'input',type:'number',inputmode:'numeric',min:1,'class':jsV,name:'A.docEntry'}},wrap);
	$1.T.divL({wxn:'wrapx10',L:{textNode:'Ref. Base'},I:{tag:'input',type:'text','class':jsV,name:'A.tr',placeholder:'No Pedido'}},divL);
	$1.T.divL({wxn:'wrapx8',subText:'Fecha Despacho',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.dStatus}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dateC(E_mayIgual)(T_time)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dateC(E_menIgual)(T_time)'}},divL);
		$1.T.divL({wxn:'wrapx4', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellDlv.get});
	wrap.appendChild(btnSend);
},
'sellOrd.get':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'No Documento'},I:{tag:'input',type:'text','class':jsV,name:'A.docEntry(E_igual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Orden de Compra'},I:{tag:'input',type:'text','class':jsV,name:'ref1'}},divL);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.ordStatus}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado Despacho'},I:{tag:'select',sel:{'class':jsV,name:'A.dlvStatus(E_igual)'},opts:{pendiente:'Pendiente','despachado':'Despachado','despacho parcial':'Despacho Parcial'}}},divL);
	if(Pa=='sellOrd.p'){
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	}
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Reporte',I:{tag:'select',sel:{'class':jsV,name:'__dbReportLen'},opts:$V.dbReportLen,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Orden Listado'},I:{tag:'select',sel:{'class':jsV,name:'orderBy'},opts:{dateCDesc:'Fecha Creado DESC',dateCAsc:'Fecha Creado ASC',docDateDesc:'Fecha Pedido DESC',docDateAsc:'Fecha Pedido ASC',docTotalDesc:'Valor Total DESC',docTotalAsc:'Valor Total ASC'},noBlank:1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellOrd.get});
	wrap.appendChild(btnSend);
},
'sellOrd.p':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:{textNode:'Número'},I:{tag:'input',type:'text','class':jsV,name:'A.docEntry(E_igual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.ordStatus}},divL);
	if(Pa=='sellOrd.p'){
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	}
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellOrd.P.get});
	wrap.appendChild(btnSend);
},
'sellRep.ordOpenQty':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Agrupado Por'},I:{tag:'select',sel:{'class':jsV+' __viewType',name:'viewType'},opts:{doc:'Pedido',card:'Cliente'}}},wrap);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Talla'},I:{tag:'select',sel:{'class':jsV,name:'B.itemSzId'},opts:$V.grs1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellRep.ordOpenQty});
	wrap.appendChild(btnSend);
},
'sellRep.rdn':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var last7 = $2d.add($2d.today,'-7days','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:last7}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Despacho',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.delivDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.delivate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Talla'},I:{tag:'select',sel:{'class':jsV,name:'B.itemSzId'},opts:$V.grs1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellRep.rdn});
	wrap.appendChild(btnSend);
},
'sellRep.itemCanceled':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var last7 = $2d.add($2d.today,'-7days','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Anulación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'D99.dateC(E_mayIgual)(T_time)',value:last7}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'D99.dateC(E_menIgual)(T_time)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Creación Doc.',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Talla'},I:{tag:'select',sel:{'class':jsV,name:'B.itemSzId'},opts:$V.grs1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:sellRep.itemCanceled});
	wrap.appendChild(btnSend);
},
'buyOrd.get':function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Creación',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.ordStatus}},divL);
	if(Pa=='sellOrd.p'){
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode}},divL);
	}
	$1.T.divL({wxn:'wrapx2', L:{textNode:'Contacto'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del contacto...',name:'A.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:buyOrd.get});
	wrap.appendChild(btnSend);
},
'Ivt.whs.history':function(wrap,itemType){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Código /s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'401,501',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Talla /s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemSize(E_in)',placeholder:'37,41',value:Pa.itemSize}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Descripción'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',O:{vPost___:'I.itemType(E_igual)='+itemType}}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx6',L:'Bodega',I:{tag:'select',sel:{'class':jsV,name:'W1.whsId(E_igual)'},opts:$V.whsCode,selected:Pa.whsId}},wrap);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Creado',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'W1.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'W1.docDate(E_menIgual)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Ivt.Whs.history});
	wrap.appendChild(btnSend);
},
'Ivt.whs':function(wrap,itemType){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Agrupado Por'},I:{tag:'select',sel:{'class':jsV,name:'viewType'},opts:{general:'Artículo-Talla',itemCode:'Solo Artículo',whs:'Bodega'},noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV+' __whsId',name:'whs1.whsId(E_in)'},opts:$V.whsCode}},divL);
	var vt2={'all':'Todos',negative:'Solo Negativos',positive:'Solo Positivos'};
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Visualizar'},I:{tag:'select',sel:{'class':jsV,name:'viewType2'},opts:vt2,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Reporte'},I:{tag:'select',sel:{'class':jsV,name:'reportLen'},opts:$V.dbReportLen,noBlank:1}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Código /s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'401,501'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Talla /s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemSize(E_in)',placeholder:'37,41'}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Descripción'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',O:{vPost:'I.itemType(E_igual)='+itemType}}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Ivt.Whs.get});
	wrap.appendChild(btnSend);
},
'itmPget':function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read('!');
	var grType=$V.itemGrP; func=Itm.p.get;
	if(Pa=='itm.mp'){ grType=$V.itemGrMP; func=Itm.mp.get; }
	if(Pa=='itm.se'){ grType=$V.itemGrSE; func=Itm.se.get; }
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsV,name:'I.itemGr(E_igual)'},opts:grType}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:func});
	wrap.appendChild(btnSend);
},
'itmAll':function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read('!');
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'I.itemType(E_igual)'},opts:$V.itemType}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Itm.RC.list});
	wrap.appendChild(btnSend);
},
'itm.rc':function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read('!');
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Itm.RC.list});
	wrap.appendChild(btnSend);
},
'wma.cost':function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Talla'},I:{tag:'select',sel:{'class':jsV,name:'grs2.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;'},opts:$V.grs1}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'MP Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.mpManual(E_igual)'},opts:$V.YN}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'MO Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.moManual(E_igual)'},opts:$V.YN}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'CIF Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.cifManual(E_igual)'},opts:$V.YN}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Cost.get});
	wrap.appendChild(btnSend);
},

}

$M.s={};
$M.s['sell']={t:'Ventas',ico:'fa_tags',sty:'color:#00E;',
L:['sellOrd.get','sellOrd.p','sellDlv.get',
{t:'Devoluciones', L:['sellRdn.get']},
{t:'Reportes',ico:'fa_bolt',L:['sellRep.ordOpenQty','sellRep.rdn','sellRep.itemCanceled']}
]};

$M.s['buy']={t:'Compras', ico:'fa_shopcart',
L:['buyOrd.get']};

$M.s['crd']={t:'Socios de Negocios',ico:'fa_handshake',L:['crd.c','crd.s']};

$M.s['ivt'] = {t:'Inventario', ico:'fa_hands',
L:['ivt.stock.p','ivt.stock.mp',
{t:'Operaciones de Stock',L:['ivt.wht','ivt.ing','ivt.egr','ivt.cat','ivt.cph','ivt.inc']
}
]};
$M.s['catalog'] = {t:'Catálogo', ico:'fa_key',
	L:[
	{t:'Artículos',L:['itm.p','itm.mp']},
	{t:'Producción',L:['wma.wfa','wma.wop']}
	]
};
$M.s['wma']={t:'Producción',ico:'iBg iBg_produccion',
L:['itm.rc.list',
	{t:'Escandallos',L:['wma.mpg','wma.cif']},
	{t:'Costos',ico:'fa_dollar', L:['wma.cost','wma.cost.log2']},
]
};

$M.s['tools'] = {t:'Herramientas',ico:'fa_magic',
L:['barcode.stickers',
	{t:'Exportaciones', L:['exp.invsiigo']}
],
}


if($0s.user!='supersu'){ delete($M.s['masters']); }

$M.li['api']={t:'Api Pruebas', func:function(){
	$M.Ht.ini({func_filt:function(w){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'URL',I:{tag:'input',type:'text','class':'__url'}},w);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Header 1',I:{tag:'input',type:'text','class':'__header'}},w);
		$1.T.divL({wxn:'wrapx4',L:'Header 1 Val',I:{tag:'input',type:'text','class':'__headerVal'}},divL);
		var btn=$1.T.btnFa({textNode:'Consultar', func:function(){
			$ps_DB.get({f:$1.q('.__url').value, inputs:$1.q('.__header').value+'='+$1.q('.__headerVal').value, func:function(re){  alert(re); }});
		}});w.appendChild(btn);
	}});
}};

$M.li['ivt.cat'] ={t:'Toma de Inventario', kau:'ivt.cat.basic', func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Documento de Inventario', func:function(){  $M.to('ivt.cat.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'none', func_filt:null, btnNew:btn,
	func_pageAndCont:Ivt.Cat.get });
}};
$M.li['ivt.cat.view'] ={t:'-', kau:'ivt.cat.basic',func:function(){
	$M.Ht.ini({func_cont:Ivt.Cat.view});
}};
$M.li['ivt.cat.form'] = {t:'Documento de Toma de Inventario', kau:'ivt.cat.basic',func:function(){ $M.Ht.ini({func_cont:Ivt.Cat.form});
}};
$M.li['ivt.cat.digit'] = {t:'Captura por Código de Barras', kau:'ivt.cat.basic', func:function(){ $M.Ht.ini({ func_cont:Ivt.Cat.digit });
}};

$M.li['ivt.ing'] ={t:'Ingreso de Producto', kau:'ivt.ing.basic',func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Documento de Ingreso', func:function(){  $M.to('ivt.ing.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'none', func_filt:null, btnNew:btn,
	func_pageAndCont:Ivt.Ing.get });
}};
$M.li['ivt.ing.form'] = {t:'Documento de Ingreso',kau:'ivt.ing.basic', func:function(){ $M.Ht.ini({func_cont:Ivt.Ing.form});
}};
$M.li['ivt.ing.digit'] = {t:'Captura por Código de Barras', kau:'ivt.ing.basic', func:function(){ $M.Ht.ini({ func_cont:Ivt.Ing.digit });
}};
$M.li['ivt.ing.view'] ={t:'-', kau:'ivt.ing.basic',func:function(){
	$M.Ht.ini({func_cont:Ivt.Ing.view});
}};

$M.li['ivt.egr'] ={t:'Salida de Producto', kau:'ivt.egr.basic', func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Documento de Salida', func:function(){  $M.to('ivt.egr.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'none', func_filt:null, btnNew:btn,
	func_pageAndCont:Ivt.Egr.get });
}};
$M.li['ivt.egr.form'] = {t:'Documento de Salida', kau:'ivt.egr.basic',func:function(){ $M.Ht.ini({func_cont:Ivt.Egr.form});
}};
$M.li['ivt.egr.digit'] = {t:'Captura por Código de Barras', kau:'ivt.egr.basic', func:function(){ $M.Ht.ini({ func_cont:Ivt.Egr.digit });
}};
$M.li['ivt.egr.view'] ={t:'-', kau:'ivt.egr.basic', func:function(){
	$M.Ht.ini({func_cont:Ivt.Egr.view});
}};

$M.li['ivt.wht'] = {t:'Transferencia de Stock', kau:'ivt.wht.basic', func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Documento', func:function(){  $M.to('ivt.wht.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'none', func_filt:null, btnNew:btn,
	func_pageAndCont:Ivt.Wht.get });
}};
$M.li['ivt.wht.view'] ={t:'-', kau:'ivt.wht.basic',func:function(){
	$M.Ht.ini({func_cont:Ivt.Wht.view});
}};
$M.li['ivt.wht.form'] = {t:'Transferencia de Stock', kau:'ivt.wht.basic',func:function(){ $M.Ht.ini({func_cont:Ivt.Wht.form});
}};

$M.li['ivt.inc'] = {t:'Recuento de Inventario',kau:'ivt.inc.basic', func:function(){ $M.Ht.ini({func_cont:Ivt.Inc.get}); }};
$M.li['ivt.inc.form'] = {t:'Recuento de Inventario',kau:'ivt.ing.basic', func:function(){ $M.Ht.ini({func_cont:Ivt.Inc.form});
	}};

$M.li['ivt.stock.p'] ={t:'Estado de Stock (Modelos)', func:function(){
	$M.Ht.ini({func_filt:function(wf){ _Fi['Ivt.whs'](wf,'P'); }, func_pageAndCont:Ivt.Whs.get});
}};
$M.li['ivt.stock.mp'] ={t:'Estado de Stock (Materia Prima)', func:function(){
	$M.Ht.ini({func_filt:function(wf){ _Fi['Ivt.whs'](wf,'MP'); }, func_pageAndCont:Ivt.Whs.get});
}};

$M.li['ivt.stock.pHistory'] ={t:'Histórico de Movimientos (Modelos)', kau:'ivt.stock.history', func:function(){
	$M.Ht.ini({func_filt:function(wf){ _Fi['Ivt.whs.history'](wf,'P'); }, func_pageAndCont:Ivt.Whs.history});
}};
$M.li['ivt.stock.mpHistory'] ={t:'Histórico de Movimientos (Materia Prima)', kau:'ivt.stock.history', func:function(){
	$M.Ht.ini({func_filt:function(wf){ _Fi['Ivt.whs.history'](wf,'MP'); }, func_pageAndCont:Ivt.Whs.history});
}};

$M.li['ivt.cph'] = {t:'Cambios entre productos similares', kau:'ivt.cph.basic', func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Documento', func:function(){  $M.to('ivt.cph.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'none', func_filt:null, btnNew:btn,
	func_pageAndCont:Ivt.Cph.get });
}};
$M.li['ivt.cph.form']={t:'Cambio por Producto Similar', kau:'ivt.cph.basic', func:function(){
	$M.Ht.ini({func_filt:null, func_cont:Ivt.Cph.form});
}, descrip:'El artículo original aumentará su cantidad en el inventario, el de cambio disminuirá.'};
$M.li['ivt.cph.view'] ={t:'-', kau:'ivt.cph.basic', func:function(){
	$M.Ht.ini({func_cont:Ivt.Cph.view});
}};


$M.li['barcode.stickers'] = {t:'Generar Etiquetas de Producto', func:function(){
	Che.L=[]; Che.T=[];
	var tb=$1.T.table(['Código','Grupo Código']); 
	var tr0=$1.q('thead tr',tb);
	var tBody=$1.t('tbody',0,tb);
	var jsF='jsFields'; var n=0;
	$M.Ht.ini({
	func_filt:function(wrap){
		var inpSea = $Sea.input(null,{api:'itemData',inputs:'fields=I.grsId',placeholder:'Nombre o código de artículo...',func:function(Jq){
			var id1 ='__trTaStickers';
			var grs2=$V.grs2[Jq.grsId];//tallas
			for(var i in grs2){ Che.T[i] = grs2[i]; }
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:Jq.itemCode},tr);
			var td=$1.t('td',0,tr); var ln='LN['+n+']'; n++;
			var sel=$1.T.sel({opts:$V.bar2,noBlank:1,sel:{'class':jsF,name:ln+'[grTypeId]',O:{vPost:ln+'[itemId]='+Jq.itemId}}});
			td.appendChild(sel);
			for(var i in Che.T){
				var td=$1.t('td',0,tr);
				if(grs2[i]){
				$1.t('div',{textNode:Che.T[i]},td);
				$1.t('input',{type:'number',min:0,inputmode:'numeric','class':jsF,name:ln+'['+i+']',style:'width:3rem;'},td);
				}
			}
	}});
	var divL=$1.T.divL({divLine:1, wxn:'wrapx1',L:{textNode:'Buscar...'},Inode:inpSea},wrap);
	},
	func_cont:function(cont){
		cont.appendChild(tb);
		var wList=$1.t('div',{id:'__wList'},cont);
		var btnSend= $1.T.btnSend({textNode:'Generar Etiquetas'},{f:'GET '+Api.Barc.s, getInputs:function(){ return $1.G.inputs(tb,jsF); },loade:wList, errWrap:wList, func:function(Jr2){
			var tb=$1.T.table(['barcode','itemCode','itemName','itemSize','quantity']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr2){ var L=Jr2[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.barCode},tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.itemSize},tr);
				$1.t('td',{textNode:L.quantity},tr);
			}
			tb=$1.T.tbExport(tb,{print:false,fileName:'barcodes',ext:'txt'});
			wList.appendChild(tb);
		}
		});
		cont.appendChild(btnSend);
	}});
}};

$M.li['itm.p'] ={t:'Maestro de Modelos', kau:'itm.p.basic',func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Artículo', func:function(){  $M.to('itm.p.form'); }});
	$M.Ht.ini({fieldset:true,func_filt:'itmPget', btnNew:btn,
	func_pageAndCont:Itm.p.get });
}};
$M.li['itm.p.form'] ={t:'Formulario de Modelo', kau:'itm.p.basic',func:function(){
	$M.Ht.ini({jsLib:{tbk:'itm_oitp,$Tb.otaxI,$Tb.otaxR,$Tb.oiac',noReload:'Y'},func_cont:Itm.p.form});
}};

$M.li['itm.mp'] ={t:'Maestro de Materia Prima', kau:'itm.mp.basic',func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Artículo', func:function(){  $M.to('itm.mp.form'); }});
	$M.Ht.ini({jsLib:{tbk:'itm_oitp,$Tb.otaxI,$Tb.otaxR,$Tb.oiac',noReload:'Y'},fieldset:true,func_filt:'itmPget', btnNew:btn,
	func_pageAndCont:Itm.mp.get });
}};
$M.li['itm.mp.form'] ={t:'Formulario de Materia Prima', kau:'itm.mp.basic',func:function(){
	$M.Ht.ini({func_cont:Itm.mp.form});
}};

$M.li['itm.se'] ={t:'Maestro de Semielaborados', func:function(){
	var btn = $1.T.btnFa({fa:'fa_doc',textNode:' Nuevo Artículo', func:function(){  $M.to('itm.se.form'); }});
	$M.Ht.ini({fieldset:true,func_filt:'itmPget', btnNew:btn,
	func_pageAndCont:Itm.se.get });
}};
$M.li['itm.se.form'] ={t:'Formulario de Semielaborado', func:function(){
	$M.Ht.ini({func_cont:Itm.se.form});
}};

$M.li['itm.chr.form'] ={t:'Características de Producto',kau:'itm.chr.basic',  func:function(){
	$M.Ht.ini({func_cont:Itm.Chr.form});
}};

$M.li['itm.BC.form'] ={t:'Códigos de Barras del Producto', kau:'itm.bc.basic', func:function(){
	$M.Ht.ini({func_cont:Itm.BC.form});
}};

$M.li['sellOrd.get']={t:'Pedidos de Venta',kau:'sellOrd.basic', func:function(){ $M.Ht.ini({fieldset:'Y-none', func_filt:'sellOrd.get', btnNew:$1.T.btnFa({fa:'faBtnCt fa_doc',textNode:'Nuevo Pedido de Venta',func:function(){ $M.to('sellOrd.form'); }}), func_pageAndCont:sellOrd.get}); }};
$M.li['sellOrd.view']={noTitle:true,kau:'sellOrd.basic', func:function(){ $M.Ht.ini({func_cont:sellOrd.view}); }};
$M.li['sellOrd.openQty']={t:'Pendientes de Pedido', kau:'sellOrd.basic', func:function(){ $M.Ht.ini({func_cont:sellOrd.openQty}); }};
$M.li['sellOrd.form']={t:'Pedido de Venta',kau:'sellOrd.basic', func:function(){ $M.Ht.ini({func_cont:sellOrd.form}); }, tau:'Generar Pedido de Venta'};

$M.li['sellOrd.p']={t:'Pedidos Pendientes (Para Despacho)',kau:'sellDlv.basic', func:function(){ $M.Ht.ini({fieldset:'Y', func_filt:'sellOrd.p',func_pageAndCont:sellOrd.P.get}); }};

$M.li['sellRep.ordOpenQty']={t:'Pedidos Pendientes', func:function(){ $M.Ht.ini({fieldset:'Y', func_filt:'sellRep.ordOpenQty',func_cont:sellRep.ordOpenQty}); }};
$M.li['sellRep.rdn']={t:'Estado de Devoluciones', func:function(){ $M.Ht.ini({fieldset:'Y', func_filt:'sellRep.rdn', func_cont:sellRep.rdn}); }};
$M.li['sellRep.itemCanceled']={t:'Revisión de Pedidos Anulados Artículo', kau:'sellRep.itemCanceled', func:function(){ $M.Ht.ini({fieldset:'Y', func_filt:'sellRep.itemCanceled', func_pageAndCont:sellRep.itemCanceled}); }};

$M.li['sellDlv.get']={t:'Despachos (Entrega)', kau:'sellDlv.basic', func:function(){ $M.Ht.ini({fieldset:'Y-none', func_filt:'sellDlv.get', func_pageAndCont:sellDlv.get}); }};
$M.li['sellDlv.view']={noTitle:true, kau:'sellDlv.basic',func:function(){ $M.Ht.ini({func_cont:sellDlv.view}); }};
$M.li['sellDlv.packing']={noTitle:true, kau:'sellDlv.basic', func:function(){ $M.Ht.ini({func_cont:sellDlv.packing}); }};
$M.li['sellDlv.packingRol']={noTitle:true, kau:'sellDlv.basic', func:function(){ $M.Ht.ini({func_cont:sellDlv.packingRol}); }};

$M.li['sellDlv.form']={t:'Despacho de Pedido', kau:'sellDlv.basic', func:function(){ $M.Ht.ini({func_cont:sellDlv.form}); }}
$M.li['sellDlv.digit']={t:'Despacho de Pedido', kau:'sellDlv.digit', func:function(){ $M.Ht.ini({func_cont:sellDlv.digit}); }}

$M.li['sellRdn.get']={t:'Devoluciones de Venta', kau:'sellRdn.basic', func:function(){ $M.Ht.ini({fieldset:'Y-none', func_filt:'sellRdn.get', btnNew:$1.T.btnFa({fa:'faBtnCt fa_doc',textNode:'Nueva Devolución',func:function(){ $M.to('sellRdn.form'); }}), func_pageAndCont:sellRdn.get}); }};
$M.li['sellRdn.form']={t:'Devoluciones', kau:'sellRdn.basic', func:function(){ $M.Ht.ini({func_cont:sellRdn.form}); }}
$M.li['sellRdn.form2']={t:'Clasificar Devolución', func:function(){ $M.Ht.ini({func_cont:sellRdn.form2}); }}
$M.li['sellRdn.view']={noTitle:true, kau:'sellRdn.basic', func:function(){ $M.Ht.ini({func_cont:sellRdn.view}); }}

$M.li['buyOrd.get']={t:'Ordenes de Compra', kau:'buyOrd.basic', func:function(){ $M.Ht.ini({fieldset:'Y-none', func_filt:'buyOrd.get', btnNew:$1.T.btnFa({fa:'faBtnCt fa_doc',textNode:'Nueva Orden de Compra',func:function(){ $M.to('buyOrd.form'); }}), func_pageAndCont:buyOrd.get}); }};
$M.li['buyOrd.view']={noTitle:true, kau:'buyOrd.basic',func:function(){ $M.Ht.ini({func_cont:buyOrd.view}); }};
$M.li['buyOrd.form']={t:'Orden de Compra', kau:'buyOrd.basic',func:function(){ $M.Ht.ini({func_cont:buyOrd.form}); }};

$M.li['crd.c']={t:'Clientes', kau:'crd.c.basic', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nuevo Cliente',func:function(){ $M.to('crd.c.form'); }});
	$M.Ht.ini({func_filt:'ocrd',btnNew:btn, func_pageAndCont:$crd.get}); }
}
$M.li['crd.c.form']={t:'Formulario de Socio', kau:'crd.c.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.form({cardType:'C'}); }}); }}

$M.li['crd.s']={t:'Proveedores', kau:'crd.s.basic', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nuevo Proveedor',func:function(){ $M.to('crd.s.form'); }});
	$M.Ht.ini({btnNew:btn, func_pageAndCont:$crd.get}); }
}
$M.li['crd.s.form']={t:'Formulario de Proveeedor', kau:'crd.s.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.form({cardType:'S'}); }}); }}

$M.li['exp.invsiigo']={t:'Inventario a Siigo', func:function(){ 
	$M.Ht.ini({topCont:$1.t('p',{textNode:'Genera la plantilla para cargar cantidades a SIIGO.'}), fieldset:true, func_filt:function(filt){
		var jsV='jsFiltVars';
		var divL=$1.T.divL({divLine:1, wxn:'wrapx8',req:'Y',L:{textNode:'Tipo Documento'},I:{tag:'select',sel:{'class':jsV,name:'docType'},opts:{oing:'Ingresos',oegr:'Salidas','ocat':'Inventario'}}},filt);
		$1.T.divL({wxn:'wrapx8',req:'Y',L:{textNode:'No. Documento'},I:{tag:'input',type:'text',inputmode:'numeric','class':jsV,name:'docEntry'}},divL)
		var divL=$1.T.divL({divLine:1, wxn:'wrapx8',req:'Y',supText:'Próximo Documento en Siigo',L:{textNode:'No. Documento'},I:{tag:'input',type:'text',inputmode:'numeric','class':jsV,name:'docSiigo'}},filt);
		var btn=$1.T.btnSend({textNode:'Obtener Plantilla', func:getPlantilla});
		filt.appendChild(btn);
	}, func_cont:null }); function getPlantilla(){
		var cont=$M.Ht.cont;
		$ps_DB.get({f:'GET '+Api.Exp.a+'invsiigo', loade:cont, errWrap:cont, inputs:$1.G.filter(), func:function(Jr){
			var tb=$1.T.table(Jr.TH); 
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i]; 
				var tr=$1.t('tr',0,tBody);
				for(var f in Jr.TH){
					var css=(L[f]==='' || L[f]==null)?'backgroundColor:#FF0;':'';
					$1.t('td',{textNode:L[f],style:css},tr);
				}
			}
			tb=$1.T.tbExport(tb,{fileName:Jr.fileName,ext:'xlsx'});
			cont.appendChild(tb);
		}});
	}
}};

$M.li['wma.wop']={t:'Maestro de Operaciones', kau:'wma.wop.basic', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Operación',func:function(){ $M.to('wma.wop.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Wop.get(); }});
}}
$M.li['wma.wop.form']={t:'Formulario de Operación',kau:'wma.wop.basic', func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wop.form(); }}); }}
$M.li['wma.wfa']={t:'Maestro de Fases', kau:'wma.wfa.basic',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Fase',func:function(){ $M.to('wma.wfa.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Wfa.get(); }});
}}
$M.li['wma.wfa.form']={t:'Formulario de Fase', kau:'wma.wfa.basic',func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wfa.form(); }}); }}

$M.li['itm.rc.list'] ={t:'Lista de Materiales',kau:'wma.rc.basic',ico:'fa_cubes', func:function(){
	$M.Ht.ini({func_filt:'itm.rc', func_pageAndCont:Itm.RC.list});
}};
$M.li['itm.rc.form'] ={t:'Lista Materiales (Base)',kau:'wma.rc.basic',d:'Lista de Materiales según modelo.', func:function(){
	$M.Ht.ini({jsLib:{tbk:'wma_owfa',noReload:'Y'}, func_cont:Itm.RC.form});
}};
$M.li['itm.rc.form2'] ={t:'Lista de Materiales (Variable)',kau:'wma.rc.form2', d:'Lista de materiales según talla variable.', func:function(){
	$M.Ht.ini({func_cont:Itm.RC.form2});
}};


$M.li['wma.mpg']={t:'Mano de Obra (Escandallos)', kau:'wma.mpg.basic', ico:'fa_handRock', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Mano de Obra',func:function(){ $M.to('wma.mpg.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Mpg.get(); }});
}}
$M.li['wma.mpg.form']={t:'Mano de Obra - Formulario',kau:'wma.mpg.basic', func:function(){ 
	$M.Ht.ini({jsLib:{tbk:'wma_owfa,wma_owop',noReload:1}, func_cont:function(){ Wma.Mpg.form(); }});
}}

$M.li['wma.cif']={t:'CIF (Escandallos)',kau:'wma.cif.basic',ico:'fa_handRock', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nuevo CIF',func:function(){ $M.to('wma.cif.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.CIF.get(); }});
}}
$M.li['wma.cif.form']={t:'Definición de CIF - Formulario',kau:'wma.cif.basic', func:function(){ 
	$M.Ht.ini({jsLib:{tbk:'wma_owfa,wma_owop',noReload:1}, func_cont:function(){ Wma.CIF.form(); }});
}}

$M.li['wma.cost']={t:'Costo de Producción de Artículos',kau:'wma.cost.view',func:function(){
	$M.Ht.ini({fieldset:1,func_filt:'wma.cost', func_pageAndCont:function(){ Wma.Cost.get(); }});
}};
$M.li['wma.cost.defineForm']={t:'Costo de Producción de Artículo',kau:'wma.cost.basic',func:function(){
	$M.Ht.ini({func_cont:function(){ Wma.Cost.defineForm(); }});
}}
$M.li['wma.cost.log2']={t:'Log de Modificacines',kau:'wma.cost.log2.view',func:function(){
	$M.Ht.ini({func_pageAndCont:function(){ Wma.Cost.Log2.get(); }});
}}
if(!$M.liA){ $M.liA={}; }

$M.liA['crd']={
t:'Socios de Negocios', ico:'fa_handshake',
L:[
	{t:'Clientes',L:[{k:'crd.c.basic',t:'Ver / Modificar Clientes'}]},
	{t:'Proveedores',L:[{k:'crd.s.basic',t:'Ver / Modificar Proveedores'}]}
]
};
$M.liA['sell']={
	t:'Ventas',kdata:'gvt_folder',
	L:[
		{t:'Pedido de Venta',
		L:[{k:'sellOrd.basic',t:'Ver / Modificar Pedidos'},{k:'sellOrd.openQty',t:'Ver Pedidos Pendientes'},{k:'sellOrd.status.receive',t:'Recibir Pedidos'},{k:'sellOrd.status.handClose',t:'Cerrar Pedidos'},{k:'sellOrd.status.cancel',t:'Anular Pedidos'},
		{k:'sellOrd.status.openToDraw',t:'Volver a Borrador un Pedido Recibido'},
		{k:'sellOrd.status.cartStatus',t:'Definir estado en Cartera'}
		]
		},
		{t:'Devoluciones',
		L:[{k:'sellRdn.basic', t:'Ver / Modificar Devoluciones'},{k:'sellRdn.status.receive',t:'Recibir Devoluciones'},{k:'sellRdn.form2',t:'Clasificación de Devoluciones'}
		]},
		{t:'Entregas / Despachos',L:[{k:'sellDlv.basic',t:'Ver y Crear Despachos'},{k:'sellDlv.digit',t:'Añadir Cantidades a Documento (Digitar)'},{k:'ivt.whsTransfer.odlv',t:'Generar Movimientos en Bodega'}
		]
		},
		{t:'Reportes',L:[{k:'sellRep.ordOpenQty',t:'Reporte de Pendientes'},{k:'sellRep.scheduleWeek',t:'Programación Entregas'},{k:'sellRep.rdn',t:'Reporte de Devoluciones'},{t:'Revisión de Anulación de Pedido por Artículo',k:'sellRep.itemCanceled'}]}
	]
};
$M.liA['buy']={
t:'Compras',
L:[{t:'Orden de Compra', 
	L:[{k:'buyOrd.basic',t:'Ver / Modificar ordenes de compra'},{k:'buyOrd.status.receive',t:'Recibir ordenes de Compra'}]}
],
}
$M.liA['ivt']={ t:'Inventario',
L:[{k:'ivt.stock.p',t:'Estado de Stock (Modelos)'}, {k:'ivt.stock.mp',t:'Estado de Stock (Materia Prima)'},{k:'ivt.stock.history',t:'Histórico Movimientos (Modelos)'},
	{t:'Operaciones de Stock', L:[{k:'ivt.wht.basic',t:'Transferencia de Stock'},{k:'ivt.ing.basic',t:'Ingreso de Artículos'},,{k:'ivt.egr.basic',t:'Salida de Artículos'},{k:'ivt.cat.basic',t:'Toma de Inventario'},{k:'ivt.whsTransfer.ocat',t:'Confirmar Ingreso desde documento de Toma de Inventario'},{k:'ivt.cph.basic',t:'Cambio Entre Productos'},{t:'Recuento de Inventario',k:'ivt.inc.basic'}]}
]
};
$M.liA['master']={t:'Maestro',
L:[{t:'Artículos', L:[{k:'itm.p.basic',t:'Ver / Modificar Modelos'},{k:'itm.mp.basic',t:'Ver / Modificar Materia Prima'}, {k:'itm.bc.basic',t:'Código de Barras de Artículo'},{k:'itm.chr.basic',t:'Características de Artículo'}]}
]
};
$M.liA['wma']={t:'Producción',
L:[{t:'Maestro', L:[{k:'wma.wfa.basic',t:'Ver / Modificar Fases de Producción'},{k:'wma.wop.basic',t:'Ver / Modificar Operaciones de Producción'}]},
	{t:'Escandallos',L:[
	{k:'wma.mpg.basic',t:'Mano de Obra (Ver / Modificar)'},{k:'wma.mpg.assg',t:'Mano de Obra (Asignar a Artículos)'},
	{k:'wma.cif.basic',t:'CIF (Ver / Modificar)'},{k:'wma.cif.assg',t:'CIF (Asignar a Artículos)'},
	{k:'wma.rc.basic',t:'Materia Prima - Composición de Materiales (Ver / Modificar)'},
	{k:'wma.rc.form2',t:'Materia Prima - Composición por Variantes (Ver/Modificar)'}]},
	{t:'Costos',L:[{k:'wma.cost.view',t:'Visualizar Costos de Producción'},{k:'wma.cost.basic',t:'Definir Costos de Producción (Ver/Modificar)'},{k:'wma.cost.log2.view',t:'Visualizar Log de Modificaciones'},{k:'wma.cost.log2.basic',t:'Ejecutar Log de Modificaciones'}]}
]
};
$M.liA['tools']={
t:'Herramientas',ico:'fa_magic',L:[{k:'barcode.stickers',t:'Generar Etiquetas de Productos'},
	{t:'Exportar',ico:'iBg iBg_icoxls',L:[{k:'exp.invsiigo',t:'Inventario a Siigo'}]}
]
};

var Che={};

var Ivt= {
whsPut:function(P){//cargar a bodega
	var nod=$1.t('div',0); var divLine=true; var cont=nod;
	if(P.fie_docDate){
		var divL=$1.T.divL({divLine:divLine,wxn:'wrapx2',L:{textNode:'Fecha Recibido'},I:{tag:'input',type:'date','class':'jsFields',name:'docDate',value:P.fie_docDate}},cont);
		divLine=false; cont=divL;
	}
	if(P.fie_whsId){
		$1.T.divL({divLine:divLine,wxn:'wrapx2',subText:'Defina a que Bodega ingresa la devolución.',L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':'jsFields',name:'whsId'},opts:$V.whsCode}},cont);
	}
	$1.Win.confirm({text:'El documento será cerrado, se cargarán las cantidades en la Bodega '+$V.whsCode[P.whsId]+', y no se podrá modificar la información.',Inode:nod, noClose:1, func:function(resp,wrapC,btn){
		$ps_DB.get({f:'PUT '+Api.Ivt.a+'whsTransfer.'+P.serieType,btnDisabled:btn,inputs:'docEntry='+P.docEntry+'&'+$1.G.inputs(nod), loade:resp, func:function(Jr){
			$ps_DB.response(resp,Jr);
		}});
	}});
}
}
Ivt.btnTransfer = function(P,btn){
	P.serieType=(P.docType)?P.docType:P.serieType;
	function send(){
		$1.Win.confirm({text:'Se realizán los movimientos en la bodega '+$V.whsCode[P.whsId]+'. No se podrá reversar la acción', noClose:true,func:function(resp){
			$ps_DB.get({f:'POST '+Api.Ivt.a+'whsTransfer.'+P.serieType, inputs:'docEntry='+P.docEntry,func:function(Jr2){
				$ps_DB.response(resp,Jr2);
			}});
		}});
	}
	if(btn=='func'){ return send; }
	if(!btn){ var btn=$1.T.btnNew({fa:'fa_move',textNode:'Generar movimiento en Bodega'}); }
	btn.onclick= send;
	return btn;
}

Ivt.Cat = {
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont;
	var jsF='jsFields';
	_Doc.formSerie({cont:cont, serieType:'ocat',jsF:jsF,
	api:'PUT '+Api.Ivt.a+'ocat', func:function(Jr2){
		$M.to('ivt.cat.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',name:'docDate'},
	{fType:'crd',wxn:'wrapx3',L:'Socio de Negocio'},
	{fType:'user'},
	{divLine:1,wxn:'wrapx8',L:'Clasificación',I:{tag:'select',sel:{'class':jsF,name:'docClass'},opts:$V.egrDocClass}},
	{wxn:'wrapx8',L:'Bodega',I:{tag:'select',sel:{'class':jsF,name:'whsId'},opts:$V.whsCode}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo'}}
	]
	});
},
get:function(cont){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'ocat', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','No.','Estado',{textNode:'Ing.',title:'¿Generó Ingreso a la Bodega?'},'Fecha','Bodega',,'Detalles','Creado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var tdB = $1.t('td',0,tr);
				var td = $1.t('td',0,tr);
				$1.t('a',{textNode:L.docEntry,'class':'fa fa_eye',href:$M.to('ivt.cat.view','docEntry:'+L.docEntry,'r')},td);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				var td=$1.t('td',0,tr);
				var tdwhs=$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				if(L.invMov=='Y'){ $1.t('span',{textNode:'Si','class':'fa fa_history'},td);
					tdwhs.style.color='blue'; tdwhs.style.fontWeight='bold';
				}
				else{ $1.t('span',{textNode:'No'},td); }
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var menu=$1.Menu.winLiRel({Li:[ 
					{ico:'iBg iBg_barcode',textNode:' Capturar', P:L, func:function(T){ $M.to('ivt.cat.digit','docEntry:'+T.P.docEntry); } },
					{ico:'fa fa_eye',textNode:' Ver Documento (Packing)', P:L, func:function(T){ $M.to('ivt.cat.view','viewType:packing,docEntry:'+T.P.docEntry); } },
					{ico:'iBg ico_xls',textNode:' Generar Etiquetas', P:L, func:function(T){ Barc.stickFromDoc({docType:'ocat',docEntry:T.P.docEntry}); } },
					{ico:'fa fa_history',textNode:' Ingresar a Bodega', P:L, func:function(T){ Ivt.whsPut({serieType:'ocat',docEntry:T.P.docEntry,whsId:T.P.whsId,fie_docDate:T.P.docDate}); } },
				]});
				tdB.appendChild(menu);
			};
		}
	}});
},
digit:function(){
	var contTop = $M.Ht.cont;
	var cont = $1.t('div');
	Che.L=[]; Che.T={}; Che.t=[];//temporal
	Che.n = -1; $M.U.i();
	var Pa=$M.read();
	var fie=$1.T.fieldset(cont,{L:{textNode:'Documento No. '+Pa.docEntry}});
	contTop.appendChild(fie);
	cont.appendChild(Barc.input({func:function(Jr,boxNum){
		Barc.Draw.tbDetail(Jr,boxNum);
	}}));
	$1.t('div',{id:'_tableWrap'},cont);
	var resp = $1.t('div',0,cont);
	var btn=$1.T.btnSend({textNode:'Guardar'},{f:'POST '+Api.Ivt.b+'.ocat', getInputs:function(){
		var d = Barc.getData(cont);
		return 'docEntry='+Pa.docEntry+'&D='+JSON.stringify(d);
	}, func:function(Jr2){
		if(!Jr2.errNo){ $M.U.e(); $M.to('ivt.cat.view','docEntry:'+Pa.docEntry); }
		$ps_DB.response(resp,Jr2);
	}}); cont.appendChild(btn);
},
view:function(){
	var Pa=$M.read(); cont=$M.Ht.cont;
	if(Pa.viewType=='packing'){ return Ivt.Cat.viewPacking(); }
	$ps_DB.get({f:'GET '+Api.Ivt.a+'ocat.view', errWrap:cont, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		var printt= $1.T.btnFa({fa:'fa_print',textNode:' Imprimir',func:function(){ $1.Win.print(cont); }}); cont.parentNode.insertBefore(printt,cont);
		var top=$1.t('div',{'class':'ffLineNoBd'});
		var fie=$1.T.fieldset(top,{L:{textNode:'Documento de Inventario'}});
		cont.appendChild(fie);
		var ffL=$1.T.ffLine({ffLine:1, w:'ffx4', t:'No.', v:Pa.docEntry},top);
		$1.T.ffLine({w:'ffx4', t:'Fecha', v:Jr.docDate},ffL);
		$1.T.ffLine({w:'ffx4', t:'Bodega', v:$V.whsCode[Jr.whsId]},ffL);
		$1.t('div',{'class':'textarea',textNode:Jr.lineMemo},top);
		var tb= $1.T.table(['#','Código','Descripción']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb);
		var ToT={total:0};
		for(var ta in Jr.T){ $1.t('td',{textNode:ta},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var total=0;
			for(var ta in Jr.T){
				var Lta=L.T[ta];
				if(!Lta){ td=$1.t('td',0,tr); }
				else{
					tta=Lta.quantity*1; total +=tta;
					if(!ToT[ta]){ ToT[ta] = 0; }
					ToT[ta] +=tta; ToT.total += tta;
					sty=titl='';
					if(tta!=Lta.breads){
						sty='backgroundColor:#FF0; cursor:pointer;'; titl='La cantidad ('+tta+') no coincide con las lecturas ('+Lta.breads+')'; }
					var td=$1.t('td',{textNode:tta,style:sty,title:titl,},tr);
					if(sty!=''){ td.onclick=function(){ $1.Win.message({text:this.title}); } }
				}
			}
			$1.t('td',{textNode:total},tr);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',colspan:3},tr);
		for(var ta in Jr.T){
			var tta=(ToT[ta])?ToT[ta]:'';
			$1.t('td',{textNode:tta},tr);
		}
		$1.t('td',{textNode:ToT.total},tr);
	}});
},
viewPacking:function(){
	var Pa=$M.read(); cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'ocat.view.packing', errWrap:cont, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		var printt= $1.T.btnFa({fa:'fa_print',textNode:' Imprimir',func:function(){ $1.Win.print(cont); }}); cont.parentNode.insertBefore(printt,cont);
		var top=$1.t('div',{'class':'ffLineNoBd'});
		var fie=$1.T.fieldset(top,{L:{textNode:'Documento de Inventario / Lista Empaque'}});
		cont.appendChild(fie);
		var ffL=$1.T.ffLine({ffLine:1, w:'ffx4', t:'No.', v:Pa.docEntry},top);
		$1.T.ffLine({w:'ffx4', t:'Fecha', v:Jr.docDate},ffL);
		$1.T.ffLine({w:'ffx4', t:'Bodega', v:$V.whsCode[Jr.whsId]},ffL);
		$1.t('div',{'class':'textarea',textNode:Jr.lineMemo},top);
		var tb= $1.T.table(['#','Código','Descripción']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb);
		for(var ta in Jr.T){ $1.t('td',{textNode:ta},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		var n=1;
		var ToT={total:0};
		Jr.L=$js.sortNum(Jr.L,{k:'detail'});
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.detail},tr); n++;
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var total=0;
			for(var ta in Jr.T){
				var Lta=L.T[ta];
				if(!Lta){ td=$1.t('td',0,tr); }
				else{
					tta=Lta.quantity*1; total +=tta;
					if(!ToT[ta]){ ToT[ta] = 0; }
					ToT[ta] +=tta; ToT.total += tta;
					sty=titl='';
					if(tta!=Lta.breads){
						sty='backgroundColor:#FF0; cursor:pointer;'; titl='La cantidad ('+tta+') no coincide con las lecturas ('+Lta.breads+')'; }
					var td=$1.t('td',{textNode:tta,style:sty,title:titl,},tr);
					if(sty!=''){ td.onclick=function(){ $1.Win.message({text:this.title}); } }
				}
			}
			$1.t('td',{textNode:total},tr);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',colspan:3},tr);
		for(var ta in Jr.T){
			var tta=(ToT[ta])?ToT[ta]:'';
			$1.t('td',{textNode:tta},tr);
		}
		$1.t('td',{textNode:ToT.total},tr);
	}});
},
}

Ivt.opts=function(P,e){ e=(e)?e:'';
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	var basic=true;
	var winTi=(P.serieType=='oing')?'Documento de Ingreso':'Documento Inventario';
	winTi=(P.serieType=='oegr')?'Documento de Salida':winTi;
	if(basic){
		Li[n]={ico:'fa fa_comment',textNode:'Comentarios',L:L,func:function(T){ $5c.form({tt:P.serieType,tr:L.docEntry, getList:'Y',winTitle:winTi+': '+L.docEntry}); } }; n++;
		Li[n]={ico:'fa fa_attach',textNode:'Archivos',L:L,func:function(T){ $5fi.btnOnTb({tt:P.serieType,tr:L.docEntry, getList:'Y',winTitle:winTi+': '+L.docEntry}); } }; n++;
	}
	return Li={Li:Li,textNode:P.textNode};
},

Ivt.fromWhs=function(D,P){
	var vPost='itemId='+D.itemId+'&itemSzId='+D.itemSzId+'&whsId='+D.whsId;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'fromWhs',inputs:vPost, loaderFull:1, func:function(Jr){
		if(Jr.errNo){ $1.Win.message(Jr); }
		else{ P.func(Jr); }
	}
	});
}

Ivt.Inc={
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; var jsF='jsFields'; var n=1;
	var Pa=$M.read();
	$ps_DB.get({f:'GET '+Api.Gvt.cvt+'.form', loadVerif:!Pa.docEntry, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
	var tb=$1.T.table([{textNode:'Código'},{textNode:'Descripción'},'Almacen',{textNode:'Sistema',style:'width:6rem;'},{textNode:'Físico'},'Desviación','']);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody= $1.t('tbody',0,tb);
	$Doc.L.itmWinSz(tBody,{priceDefiner:'N',cont:fie,fields:'I.grsId&wh[I.handInv]=Y',func:function(T,L){
		trA(tBody,T,L);
	}}); 
	$Doc.formSerie({cont:cont, middleCont:fie, serieType:'oinc',docEntry:Pa.docEntry,jsF:jsF,Jr:{}, api:'PUT '+Api.Ivt.a+'oinc.form', func:function(Jr2){
			$M.to('inv.inc.view','docEntry:'+Jr2.docEntry);
		},
	Li:[
	{wxn:'wrapx8',fType:'date',name:'docDate',value:Jr.docDate,req:'Y'},
	{divLine:1,wxn:'wrapx2',L:'Detalles',I:{tag:'textarea',name:'lineMemo',textNode:Jr.lineMemo,'class':jsF}}
	]});
	var n=1;
	function trA(tBody,T,D){
		for(var i in T){
		var ln='L['+n+']'; n++;
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:D.itemCode},tr);
		$1.t('td',{textNode:D.itemName},tr);
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$V.whsCode});
		sel.D={itemId:D.itemId,itemSzId:i};
		sel.onchange=function(){
			var pare=this.parentNode.parentNode;
			var Tw={itemId:this.D.itemId,itemSzId:this.D.itemSzId,whsId:this.value};
			Ivt.fromWhs(Tw,{func:function(D2){
				var osys=$1.q('.__inWhsQty',pare);
				osys.value=D2.onHand*1;
				updDiff(pare);
			}})
		}
		td.appendChild(sel);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __inWhsQty',name:ln+'[inWhsQty]',disabled:'disabled',readonly:'readonly',style:'width:7rem;'},td);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __qtyCount',name:ln+'[qtyCount]',value:T[i],O:{vPost:ln+'[itemId]='+D.itemId+'&'+ln+'[itemSzId]='+i},style:'width:6rem;',D:D,onkeychange:function(T){
			updDiff(T.parentNode.parentNode);
		}},td);
		var td=$1.t('td',{'class':'__diff'},tr);
		var td=$1.t('td',0,tr);
		var btnFa=$1.T.btnFa({fa:'fa fa_close',textNode:' Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
		}
	}
	function updDiff(pare){
		var osys=$1.q('.__inWhsQty',pare).value*1;
		var qty=$1.q('.__qtyCount',pare).value*1;
		var diff=$1.q('.__diff',pare);
		diff.innerText='';
		if(osys!=qty){ diff.innerText= qty-osys; }
		
	}
	if(Jr.L && !Jr.L.errNo){ for(var i in Jr.L){ trA(tBody,Jr.L[i]); } }
	}});
},

}

Ivt.Ing = {
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont;
	var jsF='jsFields';
	var n=1;
	var tb=$1.T.table([{textNode:'#',style:'width:2.5rem;'},'Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Total',style:'width:6rem;'}]);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	Drw.itemReader({cont:fie,tBody:tBody},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'table',jsF:jsF,JrS:P.JrS, n:n,noFields:{price:'N',priceList:'N'}}); n++;
	}});
	_Doc.formSerie({cont:cont, serieType:'oing',jsF:jsF,
	middleCont:fie,api:'PUT '+Api.Ivt.a+'oing', func:function(Jr2){
		$M.to('ivt.ing.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',name:'docDate'},
	{fType:'crd',wxn:'wrapx3',L:'Socio de Negocio'},
	{fType:'user'},
	{divLine:1,wxn:'wrapx8',L:'Clasificación',I:{tag:'select',sel:{'class':jsF,name:'docClass'},opts:$V.whtDocClass}},
	{wxn:'wrapx8',L:'Bodega',I:{tag:'select',sel:{'class':jsF,name:'whsId'},opts:$V.whsCode}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo'}}
	]
	});
},
get:function(cont){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'oing', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','No.','Clasificación','Estado',{textNode:'Inv.',title:'¿Movimientos de Inventario Realizados?'},'Fecha','Bodega',,'Detalles','Creado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var tdB = $1.t('td',0,tr);
				tdB.appendChild($1.Menu.winLiRel(Ivt.opts({L:L,serieType:'oing'})));
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.to('ivt.ing.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:L.docEntry},td);
				$1.t('td',{textNode:$V.ingDocClass[L.docClass]},tr);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$V.YN[L.invMov]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				
			};
		}
	}});
},
view:function(){
	var Pa=$M.read(); cont=$M.Ht.cont;
	if(Pa.viewType=='packing'){ return Ivt.Ing.viewPacking(); }
	$ps_DB.get({f:'GET '+Api.Ivt.a+'oing.view', errWrap:cont, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		var printt= $1.T.btnFa({fa:'fa_print',textNode:' Imprimir',func:function(){ $1.Win.print(cont); }}); cont.parentNode.insertBefore(printt,cont);
		var top=$1.t('div',{'class':'ffLineNoBd'});
		var fie=$1.T.fieldset(top,{L:{textNode:'Documento de Ingreso'}});
		cont.appendChild(fie);
		var ffL=$1.T.ffLine({ffLine:1, w:'ffx4', t:'No.', v:Pa.docEntry},top);
		$1.T.ffLine({w:'ffx4', t:'Fecha', v:Jr.docDate},ffL);
		$1.T.ffLine({w:'ffx4', t:'Bodega', v:$V.whsCode[Jr.whsId]},ffL);
		$1.t('div',{'class':'textarea',textNode:Jr.lineMemo},top);
		var tb= $1.T.table(['#','Código','Descripción']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb);
		var ToT={total:0};
		for(var ta in Jr.T){ $1.t('td',{textNode:ta},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var total=0;
			for(var ta in Jr.T){
				var Lta=L.T[ta];
				if(!Lta){ td=$1.t('td',0,tr); }
				else{
					tta=Lta.quantity*1; total +=tta;
					if(!ToT[ta]){ ToT[ta] = 0; }
					ToT[ta] +=tta; ToT.total += tta;
					sty=titl='';
					if(tta!=Lta.breads){
						sty='backgroundColor:#FF0; cursor:pointer;'; titl='La cantidad ('+tta+') no coincide con las lecturas ('+Lta.breads+')'; }
					var td=$1.t('td',{textNode:tta,style:sty,title:titl,},tr);
					if(sty!=''){ td.onclick=function(){ $1.Win.message({text:this.title}); } }
				}
			}
			$1.t('td',{textNode:total},tr);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',colspan:3},tr);
		for(var ta in Jr.T){
			var tta=(ToT[ta])?ToT[ta]:'';
			$1.t('td',{textNode:tta},tr);
		}
		$1.t('td',{textNode:ToT.total},tr);
		
	}});
},
}

Ivt.Egr = {
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont;
	var jsF='jsFields';
	var n=1;
	var tb=$1.T.table([{textNode:'#',style:'width:2.5rem;'},'Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Total',style:'width:6rem;'}]);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	Drw.itemReader({cont:fie,tBody:tBody},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'table',jsF:jsF,JrS:P.JrS, n:n,noFields:{price:'N',priceList:'N'}}); n++;
	}}); 
	_Doc.formSerie({cont:cont, serieType:'oegr',jsF:jsF,
	middleCont:fie,api:'PUT '+Api.Ivt.a+'oegr', func:function(Jr2){
		$M.to('ivt.egr.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',name:'docDate'},
	{fType:'crd',wxn:'wrapx3',L:'Socio de Negocio'},
	{fType:'user'},
	{divLine:1,wxn:'wrapx8',L:'Clasificación',I:{tag:'select',sel:{'class':jsF,name:'docClass'},opts:$V.egrDocClass}},
	{wxn:'wrapx8',L:'Bodega',I:{tag:'select',sel:{'class':jsF,name:'whsId'},opts:$V.whsCode}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo'}}
	]
	});
},
get:function(cont){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'oegr', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','No.','Clasificación','Estado',{textNode:'Inv.',title:'¿Movimientos de Inventario Realizados?'},'Fecha','Bodega',,'Detalles','Creado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var tdB = $1.t('td',0,tr);
				tdB.appendChild($1.Menu.winLiRel(Ivt.opts({L:L,serieType:'oegr'})));
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('ivt.egr.view','docEntry:'+L.docEntry,'r'),textNode:L.docEntry,'class':'fa fa_eye'},td);
				$1.t('td',{textNode:$V.egrDocClass[L.docClass]},tr);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$V.YN[L.invMov]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);

			};
		}
	}});
},
view:function(){
	var Pa=$M.read(); cont=$M.Ht.cont;
	if(Pa.viewType=='packing'){ return Ivt.Egr.viewPacking(); }
	$ps_DB.get({f:'GET '+Api.Ivt.a+'oegr.view', errWrap:cont, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		var printt= $1.T.btnFa({fa:'fa_print',textNode:' Imprimir',func:function(){ $1.Win.print(cont); }}); cont.parentNode.insertBefore(printt,cont);
		var top=$1.t('div',{'class':'ffLineNoBd'});
		var fie=$1.T.fieldset(top,{L:{textNode:'Documento de Ingreso'}});
		cont.appendChild(fie);
		var ffL=$1.T.ffLine({ffLine:1, w:'ffx4', t:'No.', v:Pa.docEntry},top);
		$1.T.ffLine({w:'ffx4', t:'Fecha', v:Jr.docDate},ffL);
		$1.T.ffLine({w:'ffx4', t:'Bodega', v:$V.whsCode[Jr.whsId]},ffL);
		$1.t('div',{'class':'textarea',textNode:Jr.lineMemo},top);
		var tb= $1.T.table(['#','Código','Descripción']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb);
		var ToT={total:0};
		for(var ta in Jr.T){ $1.t('td',{textNode:ta},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var total=0;
			for(var ta in Jr.T){
				var Lta=L.T[ta];
				if(!Lta){ td=$1.t('td',0,tr); }
				else{
					tta=Lta.quantity*1; total +=tta;
					if(!ToT[ta]){ ToT[ta] = 0; }
					ToT[ta] +=tta; ToT.total += tta;
					sty=titl='';
					if(tta!=Lta.breads){
						sty='backgroundColor:#FF0; cursor:pointer;'; titl='La cantidad ('+tta+') no coincide con las lecturas ('+Lta.breads+')'; }
					var td=$1.t('td',{textNode:tta,style:sty,title:titl,},tr);
					if(sty!=''){ td.onclick=function(){ $1.Win.message({text:this.title}); } }
				}
			}
			$1.t('td',{textNode:total},tr);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',colspan:3},tr);
		for(var ta in Jr.T){
			var tta=(ToT[ta])?ToT[ta]:'';
			$1.t('td',{textNode:tta},tr);
		}
		$1.t('td',{textNode:ToT.total},tr);
	}});
},
}

Ivt.Wht= {
get:function(cont){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'owht', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','No.','Estado','Fecha','De Bodega','Bodega Dest.','Detalles','Creado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.to('ivt.wht.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:L.docEntry},td);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsFrom]},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsTo]},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_eye',textNode:' Ver Documento', P:L, func:function(T){ $M.to('ivt.cat.view','docEntry:'+T.P.docEntry); } }, 
					{ico:'iBg iBg_barcode',textNode:' Capturar', P:L, func:function(T){ $M.to('ivt.cat.digit','docEntry:'+T.P.docEntry); } },
					{ico:'fa fa_eye',textNode:' Ver Documento (Packing)', P:L, func:function(T){ $M.to('ivt.cat.view','viewType:packing,docEntry:'+T.P.docEntry); } },
					{ico:'iBg ico_xls',textNode:' Generar Etiquetas', P:L, func:function(T){ Barc.stickFromDoc({docType:'ocat',docEntry:T.P.docEntry}); } }
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont;
	var jsF='jsFields';
	var n=1;
	var tb=$1.T.table([{textNode:'#',style:'width:2.5rem;'},'Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Total',style:'width:6rem;'}]);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	Drw.itemReader({cont:fie,tBody:tBody},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'table',jsF:jsF,JrS:P.JrS, n:n,noFields:{price:'N',priceList:'N'}}); n++;
	}});
	_Doc.formSerie({cont:cont, serieType:'owht',jsF:jsF, middleCont:fie,
	api:'PUT '+Api.Ivt.a+'owht', func:function(Jr2){
		$M.to('ivt.wht.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',name:'docDate'},
	{fType:'crd',wxn:'wrapx3',L:'Socio de Negocios'},
	{fType:'user'},
	{divLine:1,wxn:'wrapx8',L:'Clasificación',I:{tag:'select',sel:{'class':jsF,name:'docClass'},opts:$V.whtDocClass}},
	{wxn:'wrapx8',L:'De Bodega',I:{tag:'select',sel:{'class':jsF,name:'whsFrom'},opts:$V.whsCode}},
	{wxn:'wrapx8',L:'Bodega Destino',I:{tag:'select',sel:{'class':jsF,name:'whsTo'},opts:$V.whsCode}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo'}}
	]
	});
},
view:function(){
	var Pa=$M.read(); cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'owht.view', errWrap:cont, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		var printt= $1.T.btnFa({fa:'fa_print',textNode:' Imprimir',func:function(){ $1.Win.print(cont); }}); cont.parentNode.insertBefore(printt,cont);
		var top=$1.t('div',{'class':'ffLineNoBd'});
		var fie=$1.T.fieldset(top,{L:{textNode:'Documento de Ingreso'}});
		cont.appendChild(fie);
		var ffL=$1.T.ffLine({ffLine:1, w:'ffx4', t:'No.', v:Pa.docEntry},top);
		$1.T.ffLine({w:'ffx4', t:'Fecha', v:Jr.docDate},ffL);
		$1.T.ffLine({w:'ffx4', t:'De Bodega', v:$V.whsCode[Jr.whsFrom]},ffL);
		$1.T.ffLine({w:'ffx4', t:'Bodega Ingreso', v:$V.whsCode[Jr.whsTo]},ffL);
		$1.t('div',{'class':'textarea',textNode:Jr.lineMemo},top);
		var tb= $1.T.table(['#','Código','Descripción']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb);
		var ToT={total:0};
		for(var ta in Jr.T){ $1.t('td',{textNode:ta},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var total=0;
			for(var ta in Jr.T){
				var Lta=L.T[ta];
				if(!Lta){ td=$1.t('td',0,tr); }
				else{
					tta=Lta.quantity*1; total +=tta;
					if(!ToT[ta]){ ToT[ta] = 0; }
					ToT[ta] +=tta; ToT.total += tta;
					sty=titl='';
					if(tta!=Lta.breads){
						sty='backgroundColor:#FF0; cursor:pointer;'; titl='La cantidad ('+tta+') no coincide con las lecturas ('+Lta.breads+')'; }
					var td=$1.t('td',{textNode:tta,style:sty,title:titl,},tr);
					if(sty!=''){ td.onclick=function(){ $1.Win.message({text:this.title}); } }
				}
			}
			$1.t('td',{textNode:total},tr);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',colspan:3},tr);
		for(var ta in Jr.T){
			var tta=(ToT[ta])?ToT[ta]:'';
			$1.t('td',{textNode:tta},tr);
		}
		$1.t('td',{textNode:ToT.total},tr);
	}});
},
}

Ivt.Whs= {
get:function(cont){
	var whsId=$1.q('.__whsId',$M.Ht.filt).value;
	cont =$M.Ht.cont; var Pa=$M.read('!');
	$ps_DB.get({f:'GET '+Api.Ivt.whs, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = Drw.whs_itemSize(Jr,{whsId:whsId});
			tb=$1.T.tbExport(tb,{fileName:'Reporte de Inventario'});
			cont.appendChild(tb);
		};
		}
	});
},
history:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.whs+'.history', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['Tipo','Número','Artículo','Descripción','Talla','Cantidad','Saldo','Fecha']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var css1=(L.onHandAt<0)?'color:#E00; font-weight:bold;':'';
				var td=$1.t('td',0,tr);
				$Doc.href(L.tt,{docEntry:L.tr},td);
				$1.t('td',{textNode:L.tr},tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$V.grs1[L.itemSzId]},tr);
				$1.t('td',{textNode:L.quantity,style:css1},tr);
				$1.t('td',{textNode:L.onHandAt,style:css1},tr);
				$1.t('td',{textNode:L.docDate},tr);
			}
			cont.appendChild(tb);
		}
	}});
}
}

Ivt.Cph={
transf:function(P){
	$1.Win.confirm({text:'Se realizarán entradas para los artículos de origen y salidas para los artículos de cambio. Esta acción no se podrá reversar.',noClose:'Y', func:function(resp2){
		$ps_DB.get({f:'PUT '+Api.Ivt.a+'ocph.transfer', inputs:'docEntry='+P.docEntry, loade:resp2, func:function(Jr3){ $ps_DB.response(resp2,Jr3); }});
	}});
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont;
	var jsF='jsFields';
	var n=1;
	var tb=$1.T.table([{textNode:'#',style:'width:4rem;'},{textNode:'Tipo Doc.',style:'width:6rem;'},{textNode:'N°. Doc.',style:'width:6rem;'},'Artículo Original','Artículo para Cambio',{textNode:'Cantidad',style:'width:6rem;'}],{tbData:{'class':'table_zh table_x100'}});
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	_Doc.formSerie({cont:cont, serieType:'ocph',jsF:jsF, middleCont:fie,
	api:'PUT '+Api.Ivt.a+'cph', func:function(Jr2){
		$M.to('ivt.cph.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',name:'docDate',req:'Y'},
	{fType:'user'},
	{wxn:'wrapx8',req:'Y',L:'Bodega',I:{tag:'select',sel:{'class':jsF,name:'whsId'},opts:$V.whsCode}},
	{divLine:1,wxn:'wrapx1',L:'Observación',I:{tag:'textarea','class':jsF,name:'lineMemo',placeholder:'Detalles para documento.'}},
	]});
	var btn=$1.T.btnFa({fa:'fa fa_plusCircle', textNode:' Añadir Lineas',func:function(){
		trA({},n); n++;
	}},fie);
	function trA(L,n){
		var ln='L['+n+']';
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:n},tr);
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[tt]',style:'width:6rem;'},opts:{opvt:'Pedido Venta'}});
		sel.O={vPost:''};
		td.appendChild(sel);
		var td=$1.t('td',0,tr);
		var trInp=$1.t('input',{type:'number','class':jsF,name:ln+'[tr]',style:'width:6rem;',O:{vPost:''}},td);
		var td=$1.t('td',0,tr);
		var sea=$Sea.input(td,{api:'itemData',inputs:'viewType=itemSize', fPars:{ln:ln,inpD:trInp}, func:function(Js,inp,fP){
		fP.inpD.O.vPost=fP.ln+'[itemIdFrom]='+Js.itemId+'&'+fP.ln+'[itemSzIdFrom]='+Js.itemSzId;
		}});
		var td=$1.t('td',0,tr);
		var sea=$Sea.input(td,{api:'itemData',inputs:'viewType=itemSize', fPars:{ln:ln,sel:sel}, func:function(Js,inp,fP){
		fP.sel.O.vPost=fP.ln+'[itemIdTo]='+Js.itemId+'&'+fP.ln+'[itemSzIdTo]='+Js.itemSzId;
		}});
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'number','class':jsF,name:ln+'[quantity]',style:'width:6rem;'},td);
	}
	var n=1;
	trA({},n); n++;
},
get:function(cont){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ivt.a+'ocph', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','No','Estado',{textNode:'Inv.',title:'¿Inventario Modificado?'},'Fecha','Bodega','Detalles','Realizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var tdB = $1.t('td',0,tr);
				tdB.appendChild($1.Menu.winLiRel({Li:[
					{ico:'fa fa_history',textNode:' Realizar Movimientos en Inventario', P:L, func:function(T){ 
						Ivt.Cph.transf(T.P);
					}}
				]}));
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.to('ivt.cph.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:L.docEntry},td);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$V.YN[L.invMov]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				
			};
		}
	}});
},
view:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont; $1.clear(contPa);
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	$1.t('span',{textNode:' | '},divTop);
	var btnT=$1.T.btnFa({fa:'fa_history',textNode:' Generar Movimientos', P:{docEntry:Pa.docEntry}, func:function(T){ Ivt.Cph.transf(T.P); }});
	divTop.appendChild(btnT);
	$ps_DB.get({f:'GET '+Api.Ivt.a+'ocph.view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		sHt.ivtCphHead(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Tipo Doc.',style:'width:4rem;'},{textNode:'N°. Doc.',style:'width:4rem;'},{textNode:'Artículo Original',colspan:2},{textNode:'Artículo de Cambio',colspan:2},{textNode:'Cant.',style:'width:6rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Pedido'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:L.tt},tr);
			$1.t('td',{textNode:L.tr},tr);
			$1.t('td',{textNode:Itm.Txt.code({itemCode:L.itemCodeFrom,itemSzId:L.itemSzIdFrom})},tr);
			$1.t('td',{textNode:Itm.Txt.name({itemName:L.itemNameFrom,itemSzId:L.itemSzIdFrom})},tr);
			$1.t('td',{textNode:Itm.Txt.code({itemCode:L.itemCodeTo,itemSzId:L.itemSzIdTo})},tr);
			$1.t('td',{textNode:Itm.Txt.name({itemName:L.itemNameTo,itemSzId:L.itemSzIdTo})},tr);
			$1.t('td',{textNode:L.quantity*1,'class':'__tbColNums','tbColNum':4},tr);
		}
		var tr=$1.t('tr',0,tF);
		$1.t('td',{colspan:7,textNode:'Cantidad Total',style:'text-align:right;'},tr);
		$1.t('td',{'class':'__tbColNumTotal4'},tr);
		$Tol.tbSum(tb);
	}});
}
}

var Drw ={
whs_itemSize:function(Jr,P){
	var cs=3; var P=(P)?P:{};
	var tr1=['Código','Descripción','Talla','Bodega'];
	if(Jr.viewType=='itemCode'){ cs=2; delete(tr1[2]); delete(tr1[3]); }
	else if(Jr.viewType!='whs'){ delete(tr1[3]); }
	if(Jr.viewType=='whs'){ cs=4; }
	var tb = $1.T.table(tr1);
	var tBody = $1.t('tbody',0,tb);
	var tr0=$1.q('thead tr',tb);
	$js.sortNum(Jr.T);
	$js.sortNum(Jr.L,{k:'itemCode'});
	$1.t('td',{textNode:'En Stock'},tr0);
	$1.t('td',{textNode:'Solicitado'},tr0);
	$1.t('td',{textNode:'Disponible'},tr0);
	for(var i in Jr.L){ L=Jr.L[i];
		for(var ta in L.T){ LT=L.T[ta];
			var tr = $1.t('tr',0,tBody);
			var disp=(LT.onHand-LT.isCommited-LT.onOrder);
			var disp=(LT.onHand-LT.onOrder);
			var css1=(LT.onHand<0)?'color:#E00; font-weight:bold;':'';
			var css2=(disp<0)?'color:#E00; font-weight:bold;':'';
			var tdi=$1.t('td',0,tr);
			ks='itemCode:'+L.itemCode;
			var itemSize=$V.grs1[ta];
			ks+=(Jr.viewType!='itemCode')?',itemSize:'+itemSize:'';
			ks+=(P.whsId!='')?',whsId:'+P.whsId:'';
			$1.t('a',{'class':'fa faBtn fa_eye',href:$M.to('ivt.stock.pHistory',ks,'r'),title:'Visualizar Movimientos de Artículo'},tdi);
			$1.t('span',{textNode:' '+L.itemCode},tdi);
			$1.t('td',{textNode:L.itemName},tr);
			if(Jr.viewType!='itemCode'){ $1.t('td',{textNode:itemSize},tr); }
			if(Jr.viewType=='whs'){ $1.t('td',{textNode:$V.whsCode[L.whsId]},tr);}
			$1.t('td',{textNode:LT.onHand,style:css1,'class':tbSum.tbColNums,tbColNum:1},tr);
			$1.t('td',{textNode:LT.onOrder,'class':tbSum.tbColNums,tbColNum:2},tr);
			$1.t('td',{textNode:disp,style:css2,'class':tbSum.tbColNums,tbColNum:3},tr);
		}
	}
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{colspan:cs,textNode:'Total'},tr);
	$1.t('td',{'class':tbSum.tbColNumTotal+'1'},tr);
	$1.t('td',{'class':tbSum.tbColNumTotal+'2'},tr);
	$1.t('td',{'class':tbSum.tbColNumTotal+'3'},tr);
	tbSum.get(tb);
	return tb;
},
docLineItemSz:function(tBody,P){//addLine on Order
	/*
	handSize=Y, debe venir con grsId para obtener tallas
	itemSz=Y, se obtiene cuando se lee por tallas
	*/
	var JrS=P.JrS;
	var itemSz=(P.JrS.itemSz=='Y');//leyendo desde getSizes->
	var NohandSize=(P.JrS.handSize=='N');
	if(NohandSize){ P.Ds={T:{}}; P.Ds.T[$V.uniqSize]=1; }//sin talla, cargar unique
	else if(itemSz){ P.Ds={T:{}}; P.Ds.T[P.JrS.itemSzId]=1; } //talla simple
	if(P.winOpen!='N'){
		if(NohandSize || itemSz){
			P=mkL(P);
			if(P.func){ P.func(P,tBody); }
			else if(P.trLine=='table'){ Drw.itemSzTb(tBody,P); }
			else{ Drw.itemSzTr(tBody,P); }
		}
		else{ Itm.winAdd({func:function(Ds){
			P.Ds=Ds; P=mkL(P);
			if(P.func){ P.func(P,tBody); }
			else if(P.trLine=='table'){ Drw.itemSzTb(tBody,P) }
			else{ Drw.itemSzTr(tBody,P) }
		}},P.JrS);
		}
	}
	else{
		if(P.func){ P.func(P,tBody); }
		else if(P.trLine=='table'){ Drw.itemSzTb(tBody,P) }
		else{ Drw.itemSzTr(tBody,P); }
	}
	var n=0; P.L=[];
	function mkL(P){
		var Ds=P.Ds;
		if(!P.L){ P.L=[]; } if(!n){ n=0; }
		for(var ta in Ds.T){ P.L[n]={};
			for(var i in JrS){ P.L[n][i]= JrS[i]; }
			if(Ds.T[ta].quantity){ P.L[n].quantity = Ds.T[ta].quantity; }
			else if(Ds.T[ta]){ P.L[n].quantity = Ds.T[ta]; }
			else{ P.L[n].quantity=1; }
			P.L[n].itemSzId=ta;
			P.L[n].itemSize=$V.grs1[ta];
			n++;
		}
		P.Ds=Ds;
		return P;
	}
},
itemReader:function(P1,P){
	var tBody=P1.tBody; var cont=P1.cont;
	if(P.func=='default'){
		P.func=Drw.docLineItemSz;
		for(var i in P.funcPars){ P[i]=P.funcPars[i]; }
	}
	var oB={item:'Producto',itemSize:'Talla',barCode:'Código de Barras'};
	if(P1.noSelBy){
		if(P1.noSelBy.match(/itemCode/)){ delete(oB.item); }
		if(P1.noSelBy.match(/itemSize/)){ delete(oB.itemSize); }
		if(P1.noSelBy.match(/barCode/)){ delete(oB.itemSize); }
	}
	var divL =$1.T.divL({divLine:1, wxn:'wrapx8',supText:'Por',I:{tag:'select',sel:{'class':'__sel'},opts:oB,noBlank:1}},cont);
	var sel=$1.q('.__sel',cont);
	sel.onchange = function(){$Sea.addInputs='';
		if(this.value=='barCode'){ sea.style.display='none'; bc.style.display='block'; }
		else{ sea.style.display='block'; bc.style.display='none';
			if(this.value=='itemSize'){ $Sea.addInputs='viewType=itemSize'; }
		}
	}
	if(sel.value=='itemSize'){ $Sea.addInputs='viewType=itemSize'; }
	var inputs='fields=I.sellPrice,I.grsId,I.sellUdm';
	inputs +=(P1.fields)?','+P1.fields:'';
	inputs +=(P1.wh)?'&'+P1.wh:'';
	var sea=$Sea.input(cont,{api:'itemData',subText:'Busque para agregar un producto...',inputs:inputs,inpBlank:true, funcAddInputs:P.funcAddInputs, func:function(Jr,inp){
			P.JrS=Jr;//trSize,jsF,noFields{}
			P.func(tBody,P);
			//Drw.docLineItemSz(tBody,{trSize:'Y',jsF:jsF,n:P.n,JrS:Jr, noFields:{price:'N',priceList:'N'}});
	}});
	var bc=Barc.input({box:'N', func:function(Jr,boxNum){
		P.JrS=Jr;
		P.func(tBody,P);
	}});
	bc.style.display='none';
	$1.T.divL({wxn:'wrapx8_1',Inode:sea},divL);
	$1.T.divL({wxn:'wrapx8_1',Inode:bc},divL);
},
itemSzTr:function(tBody,P){//tr para cada itemSize
		//#,itemCode, itemName, itemSz q, udm, btns
	L=P.JrS; Ds=P.Ds;
	var noFields=(P.noFields)?P.noFields:{};
	var Fields=(P.Fields)?P.Fields:{};
	var Dsb=(P.Disabled)?P.Disabled:{};
	var tbPare=tBody.parentNode;
	var jsF= (P.jsF)?P.jsF:'jsFields';
	if(L.T && !Ds){ Ds= {T:L.T}; }
	else if(L && !Ds){ Ds={T:{}};
		Ds.T[L.itemSzId]=L;
	}
	if(!Ds){ Ds={T:{}}; }
	var n=1;
	for(var ta in Ds.T){
		var ln= 'L['+P.n+'_'+n+']'; n++;
		var vPost ='&'+ln+'[itemSzId]='+ta;
		var quantity=(Ds.T[ta])?Ds.T[ta]:L.quantity;
		quantity=(Ds.T[ta] && Ds.T[ta].quantity)?Ds.T[ta].quantity:quantity;
		var tr=$1.t('tr',{'class':'__tdNumGroup'},tBody);
		$1.t('td',{textNode:P.n,'class':'_noHidden'},tr);
		var td=$1.t('td',{'class':'_noHidden',textNode:L.itemCode},tr);
		var td=$1.t('td',{textNode:L.itemName},tr);
		var td=$1.t('td',{textNode:$V.grs1[ta]},tr);
		var tdt=$1.t('td',0,tr);
		var inp=$1.t('input',{type:'number',inputmode:'numeric',min:0,value:quantity,style:'width:3.25rem; font-size:0.8rem;','class':jsF+' __tdNum',name:ln+'[quantity]'},tdt);
		inp.onkeyup=function(){ $Tol.tbSum(tbPare); }
		inp.onchange=function(){ $Tol.tbSum(tbPare); }
		var val=(L.sellPrice)?L.sellPrice:L.price;
		var priceList=(L.sellPrice)?L.sellPrice:L.priceList;
		//cols
		if(!noFields.udm){
			var udm=(L.sellUdm)?L.sellUdm:((L.udm)?L.udm:'?');
			$1.t('td',{textNode:udm},tr);
		}
		if(!noFields.price){
			var kv='price';
			if(L.curr && L.curr!=$0s.currDefault){ val=L.priceME; kv='priceME'; }
			var td=$1.t('td',{style:'width:5rem;'},tr);
			var inp=$1.t('input',{type:'text','class':jsF+' __tdNum2',numberformat:'mil',style:'width:90%;',name:ln+'[price]',value:val, onkeychange:function(){ $Tol.tbSum(tbPare); }},td);
			if(Dsb.price=='Y'){ inp.setAttribute('disabled','disabled'); }
		}
		if(Fields.disc){
			var td=$1.t('td',{style:'width:5rem;'},tr);
			var inp=$1.t('input',{type:'number',inputmode:'numeric',min:0,max:100,'class':jsF+' __tdNumDiscVar',name:ln+'[disc]',value:L.disc, onkeychange:function(){ $Tol.tbSum(tbPare); }},td);
		}
		
		if(!noFields.price){
			var tdTotal=$1.t('td',{'class':'__tdTotal',vformat:'money'},tr);
		}
		var td=$1.t('td',{style:'width:3rem;','class':'_noHidden'},tr);
		td.visible='Y';
		var btn=$1.T.btnFa({fa:'fa_close',textNode:' Quitar', name:ln, func:function(T){
			var tr=T.parentNode.parentNode;
			var tds=$1.q('td',tr,'all');
			var ttd=T.parentNode;//td
			for(var i3=0; i3<tds.length;i3++){
				if(ttd.visible=='Y' && !(tds[i3].classList.contains('_noHidden'))){ tds[i3].style.visibility='hidden'; }
				else{ tds[i3].style.visibility=''; }
			}
			if(ttd.visible=='Y'){
				if(tdTotal){ tdTotal.classList.add('__tdTotalNoCount'); }
				T.innerText= ' Restablecer';
				T.classList.replace('fa_close','fa_arrowBack');
				ttd.visible='N';
				$1.t('input',{type:'hidden',value:'Y',name:T.name+'[delete]','class':jsF+' _inputDelete'},T.parentNode);
			}
			else{
				ttd.visible='Y';
				T.innerText=' Quitar';
				T.classList.replace('fa_arrowBack','fa_close');
				if(tdTotal){ tdTotal.classList.remove('__tdTotalNoCount'); }
				$1.delet($1.q('._inputDelete',ttd));
			}
				$Tol.tbSum(tbPare);
			}
		},td);
		if(!noFields.priceList){ vPost += '&'+ln+'[priceList]='+priceList; }
		var inp=$1.t('input',{type:'hidden','class':jsF,name:ln+'[itemId]',value:L.itemId,O:{vPost:vPost}},td);
	}
	$Tol.tbSum(tbPare);
},
itemSzTb(tBody,P){
	L=P.JrS; Ds=P.Ds;
	var noFields=(P.noFields)?P.noFields:{};
	var Fields=(P.Fields)?P.Fields:{};
	var Dsb=(P.Disabled)?P.Disabled:{};
	var tbPare=tBody.parentNode;
	var tr=$1.t('tr',{'class':'__tdNumGroup'},tBody);
	var jsF= (P.jsF)?P.jsF:'jsFields';
	$1.t('td',{textNode:P.n,'class':'_noHidden'},tr);
	var td=$1.t('td',{'class':'_noHidden'},tr);
	$1.t('b',{textNode:L.itemCode+') '},td);
	$1.t('span',{textNode:L.itemName},td);
	var tb=$1.t('table',{'class':'tableSizes'},td);
	var tH=$1.t('thead',0,tb); var tr0=$1.t('tr',0,tH);
	var tB=$1.t('tbody',0,tb); var tr1=$1.t('tr',0,tB);
	if(L.T && !Ds){ Ds= {T:L.T}; }
	if(!Ds){ Ds={T:{}}; }
	var total=0;
	var val=(L.sellPrice)?L.sellPrice:L.price;
	var priceList=(L.sellPrice)?L.sellPrice:L.priceList;
	var ln='L['+P.n+']';//L{price, T:{itemId,priceList,itemSzId,qua }
	for(var ta in Ds.T){
		var lnt= ln+'[T]['+ta+']';
		$1.t('td',{textNode:$V.grs1[ta]},tr0);
		var tdt=$1.t('td',0,tr1);
		var vPost=lnt+'[itemId]='+L.itemId+'&'+lnt+'[itemSzId]='+ta;
		if(!noFields.priceList){ vPost += '&'+lnt+'[priceList]='+priceList; }
		var inp=$1.t('input',{type:'number',inputmode:'numeric',min:0,value:Ds.T[ta],style:'width:3.25rem; font-size:0.8rem;','class':jsF+' __tdNum',name:lnt+'[quantity]',O:{vPost:vPost}},tdt);
		total+=Ds.T[ta]*1;
		inp.onkeyup=function(){ $Tol.tbSum(tbPare); }
		inp.onchange=function(){ $Tol.tbSum(tbPare); }
	}
	//cols
	if(!noFields.udm){
		var udm=(L.sellUdm)?L.sellUdm:((L.udm)?L.udm:'?');
		$1.t('td',{textNode:udm},tr);
	}
	if(!noFields.price){
		var kv='price';
		if(L.curr && L.curr!=$0s.currDefault){ val=L.priceME; kv='priceME'; }
		var valto=val*total;
		var td=$1.t('td',{style:'width:5rem;'},tr);
		var inp=$1.t('input',{type:'text','class':jsF+' __tdNum2',numberformat:'mil',style:'width:90%;',name:ln+'[price]',value:val, onkeychange:function(){ $Tol.tbSum(tbPare); }},td);
		if(Dsb.price=='Y'){ inp.setAttribute('disabled','disabled'); }
		inp.onchange=function(){ $Tol.tbSum(tbPare); }
	}
	if(Fields.disc){
		var valto=val*total;
		var td=$1.t('td',{style:'width:5rem;'},tr);
		var inp=$1.t('input',{type:'number',inputmode:'numeric',min:0,max:100,'class':jsF+' __tdNumDiscVar',name:ln+'[disc]',value:L.disc, onkeychange:function(){ $Tol.tbSum(tbPare); }},td);
	}
	$1.t('td',{textNode:total,'class':'__tdTotalNum'},tr);
	if(!noFields.price){
		var tdTotal=$1.t('td',{textNode:$Str.money({value:valto,curr:L.curr}),'class':'__tdTotal',vformat:'money'},tr);
	}
	var td=$1.t('td',{style:'width:3rem;','class':'_noHidden'},tr);
	td.visible='Y';
	var btn=$1.T.btnFa({fa:'fa_close',textNode:' Quitar', func:function(T){
		var tds=$1.q('td',tr,'all');
		var ttd=T.parentNode;//td
		for(var i3=0; i3<tds.length;i3++){
			if(ttd.visible=='Y' && !(tds[i3].classList.contains('_noHidden'))){ tds[i3].style.visibility='hidden'; }
			else{ tds[i3].style.visibility=''; }
		}
		if(ttd.visible=='Y'){
			if(tdTotal){ tdTotal.classList.add('__tdTotalNoCount'); }
			T.innerText= ' Restablecer';
			T.classList.replace('fa_close','fa_arrowBack');
			ttd.visible='N';
			$1.t('input',{type:'hidden',value:'Y',name:ln+'[delete]','class':jsF+' _inputDelete'},T.parentNode);
		}
		else{
			ttd.visible='Y';
			T.innerText=' Quitar';
			T.classList.replace('fa_arrowBack','fa_close');
			if(tdTotal){ tdTotal.classList.remove('__tdTotalNoCount'); }
			$1.delet($1.q('._inputDelete',ttd));
		}
			$Tol.tbSum(tbPare);
		}
	},td);
		$Tol.tbSum(tbPare);
}

}

var Barc={
input:function(P){
	var box=$1.t('input',{type:'text',id:'__invInpBoxData','class':'iBg iBg_recibir',placeholder:'1,A4'});
var liner=$1.t('input',{type:'text','class':'iBg iBg_barcode',id:'__invInpBarCode',placeholder:'Lea o digité el código'});
	if(P.box!='N'){
		var divL= $1.T.divL({divLine:1,wxn:'wrapxauto',wxT:{style:'width:5rem;'},Inode:box});
		$1.T.divL({wxn:'wrapxauto',wxT:{style:'min-width:70%;'},Inode:liner},divL);
	}
	else{ 
		var divL= $1.T.divL({divLine:1, supText:'Código de Barras...',wxn:'wrapx1',Inode:liner});
	}
	inp= $1.q('#__invInpBarCode',divL);
	inp.onkeyup = function(ev){ This=this;
		$js.isKey(ev,'enter',{func:function(){//onRead
			Barc.get(This.value,P); This.value='';
		}});
	}
	return divL;
},
get:function(barCode,P){ P=(P)?P:{};
	boxNum='';
	var boxN=$1.q('#__invInpBoxData'); if(boxN){ boxNum=boxN.value; }
	if(!$Tb.itm_bar1){ $Tb.itm_bar1 ={}; }
	if($Tb.itm_bar1[barCode]){
		if(P.func){ P.func($Tb.itm_bar1[barCode],boxNum); }
	}
	else{
	$ps_DB.get({f:'GET '+Api.Ivt.b, inputs:'barCode='+barCode, func:function(Jr){
		if(Jr.errNo){ $1.Win.message(Jr); }
		else{
			$Tb.itm_bar1[barCode] = Jr;
			if(P.func){ P.func($Tb.itm_bar1[barCode],boxNum); }
		}
	}});
	}
},
getData:function(cont){
	var l=$1.q('._bcLine',cont,'all'); var d=[];
	for(var i=0; i<l.length; i++){
		var L2=l[i].js;
		L2.quantity=l[i].value;
		d[i]=L2;
	}
	return d;
}
}

Barc.stickFromDoc = function(P){
	$ps_DB.get({f:'GET '+Api.Barc.a+'stickFromDoc', inputs:'docType='+P.docType+'&docEntry='+P.docEntry, func:function(Jr){
		var wrap = $1.t('div');
		var tb=$1.T.table(['barcode','itemCode','itemName','itemSize','quantity']);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.barCode},tr);
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			$1.t('td',{textNode:L.itemSize},tr);
			$1.t('td',{textNode:L.quantity},tr);
		}
		tb=$1.T.tbExport(tb,{print:1,fileName:'barcodes.txt'});
		wrap.appendChild(tb);
		var win=$1.Win.open(wrap,{winTitle:'Matriz de códigos del documento',onBody:1});
	}});
}
Barc.Draw = {
tbDetail:function(O,boxNum,P){//genera tabla item,itemSize... 
	var k ='_'+O.itemCode;
	P=(P)?P:{};
	if(boxNum){
		if(!isNaN(boxNum)){ boxNum = boxNum*1; }
		var k =boxNum+'_'+O.itemCode;
	}
	var ta = O.itemSzId;
	Che.T[ta] = O.itemSize;
	if(!Che.t[k]){ Che.t[k] = k; Che.n++; n = Che.n;
		n=k;
		Che.L[n] = {}; for(var ik in O){ Che.L[n][ik] = O[ik]; }
		Che.L[n].detail = boxNum;
		Che.L[n]['T'] = {};
	}
	n = Che.n; n=k;
	if(!Che.L[n]['T'][ta]){Che.L[n]['T'][ta] = {quantity:0, reads:0 }; }
	Che.L[n]['T'][ta].quantity = Che.L[n]['T'][ta].quantity*1+1;
	Che.L[n]['T'][ta].reads += 1;
	(Che.L).sort(function(a,b){
		if(!isNaN(a.detail) && !isNaN(b.detail)){
			if(a.detail*1<b.detail*1){ return -1; }
			if(a.detail*1>b.detail*1){ return 1; }
		}
	});
	var cont = $1.q('#_tableWrap',$M.Ht.cont); $1.clear(cont);
	var tb=$1.t('table',{'class':'table_zh'},cont);
	var tHead =$1.t('thead',0,tb);
	var tr=$1.t('tr',0,tHead);
	$1.t('td',{textNode:'Det.'},tr);
	$1.t('td',{textNode:'Código'},tr);
	$1.t('td',{textNode:'Nombre'},tr);
	for(var t in Che.T){ $1.t('td',{textNode:Che.T[t]},tr); }
	$1.t('td',{textNode:'Total'},tr);
	var tBody =$1.t('tbody',0,tb);
	for(var i in Che.L){ var L=Che.L[i];
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:L.detail},tr);
		$1.t('td',{textNode:L.itemCode},tr);
		$1.t('td',{textNode:L.itemName},tr);
		var total=0;
		for(var t in Che.T){
			var ttext=(L.T[t])?L.T[t].quantity*1:'';
			var reads=(L.T[t])?L.T[t].reads:'';
			var tsi=t;
			var _i=L.itemId; var _i2=t;
			total += ttext*1;
			var td= $1.t('td',0,tr);
			if(ttext!=''){
			var inp=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':'_bcLine __trLine __itemTd',style:'width:3rem;',value:ttext, L:{i:i,ta:tsi,t:t} });
			inp.js={detail:L.detail,itemId:L.itemId,itemSzId:t,reads:reads};
			inp.D={itemCode:L.itemCode,itemName:L.itemName};
			inp.onkeyup = function(){ trUpd(this); if(P.inpChange){ P.inpChange(this); } }
			inp.onchange = function(){ trUpd(this); if(P.inpChange){ P.inpChange(this); } }
			td.appendChild(inp);
			var del=$1.t('button',{'class':'fa faBtn fa_close',title:'Eliminar cantidades',tabindex:'-1'},td);
			del.onclick=function(){ tdPare=this.parentNode; inpT=$1.q('input',tdPare);
					$1.Win.confirm({text:'Se eliminar las cantidades, no se puede recuperar esta información', func:function(){
					var i3=inpT.L.i; var i4=inpT.L.t; inpT.value=0;
					inpT.noReadAndDelete='Y';//usar para no sumar y luego eliminar
					Che.L[i3].T[i4].quantity=0; 
					if(P.inpChange){ P.inpChange(this); }
			}}); }
			}
		}
		$1.t('td',{textNode:total,'class':'__trTotal'},tr);
	}
	function trUpd(T){
		var i =T.L.i; var ta=T.L.ta;
		Che.L[i]['T'][ta].quantity = T.value*1;
		$Tol.sumTotal('.__trLine','.__trTotal',T.parentNode.parentNode);
	}
}
}

var _Doc={
cancel:function(P){ return $Doc.cancel(P); },
close:function(P){ return $Doc.close(P); },
formSerie:function(P){ return $Doc.formSerie(P); }
}

var _Frm={
n:{i:1},
itm:function(P,tBody){
	var difCurr=(P.curr!=$0s.currDefault);
	var P2=(P.JrS)?P.JrS:P;
	var Ta=(P.Ds)?P.Ds.T:{}; var jsF=P.jsF;
	var Fie=(P.Fie)?P.Fie:{};
	var basic=(Fie && Fie.formFields=='basic')?true:false;//itemCode,itemName,q,udm
	for(var i in P.L){ P2=P.L[i];
		var ln='L['+_Frm.n.i+']';
		var itnc=P2.itemCode;
		var itn=P2.itemName;
		P2.itemSize=$V.grs1[P2.itemSzId];
		if(P2.itemSzId!=$V.uniqSize){
			var itnc=P2.itemCode+'-'+P2.itemSize;
			var itn=P2.itemName+'  T: '+P2.itemSize
		}
		var vPost= ln+'[itemId]='+P2.itemId+'&'+ln+'[itemSzId]='+P2.itemSzId+'&';
		var pricDisab=(Fie.price && Fie.price.disabled=='Y');
		var discDisab=(Fie.disc && Fie.disc.disabled=='Y');
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:_Frm.n.i},tr); _Frm.n.i++;
		var priceBase=0;
		var td=$1.t('td',{textNode:itnc,'class':'_noHidden'},tr);
		if(!P2.itemSize){ P2.itemSize=$V.grs1[P2.itemSzId]; }
		$1.t('td',{textNode:itn},tr);
		var td=$1.t('td',{style:'width:4.25rem;'},tr);
		var qua=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __tdNum2 __tbColNums', tbColNum:2, value:P2.quantity,name:ln+'[quantity]',onkeychange:function(){ $Tol.tbSum(tBody.parentNode); },style:'width:4rem;'},td);
		udm=(P.formType=='compra')?P2.buyUdm:((P2.sellUdm)?P2.sellUdm:'?');
		$1.t('td',{textNode:udm},tr);
		if(!basic){
		if(P.formType=='compra'){
			var price=(P2.buyPrice)?P2.buyPrice:P2.price;
		price=(P2.priceDefine)?P2.priceDefine:price;//definir antes
			var tdPrice=$1.t('td',{style:'width:7rem;'},tr);
			var cost=(P2.buyPrice)?P2.buyPrice:((P2.price)?P2.price:P2.cost);
			priceBase=(P2.priceList)?P2.priceList:cost;
			vPost+= (P2.priceList)?ln+'[priceList]='+P2.priceList+'&':ln+'[priceList]='+cost+'&';
			var inpPrice=$1.t('input',{type:'text',numberformat:'mil', value:cost,'class':jsF+' __tdNum',onkeychange:function(){ $Tol.tbSum(tBody.parentNode); },name:ln+'[price]',kn:'buyPrice',style:'width:7rem;'},tdPrice);
			inpPrice.numBase = priceBase;
		}
		else{//venta
			udm=P2.sellUdm; $1.t('td',{textNode:udm},tr);
			var price=(P2.sellPrice)?P2.sellPrice:P2.price;
			price=(P2.priceDefine)?P2.priceDefine:price;//definir antes
			priceBase=(P2.priceList)?P2.priceList:P2.sellPrice;
			vPost+=(P2.priceList)?ln+'[priceList]='+P2.priceList+'&':ln+'[priceList]='+P2.sellPrice+'&';
			var tdPrice=$1.t('td',{style:'width:7rem;'},tr);
			var inpPrice=$1.t('input',{type:'text',numberformat:'mil', value:price,'class':jsF+' __tdNum',onkeychange:function(){ $Tol.tbSum(tBody.parentNode); },name:ln+'[price]',kn:'sellPrice',title:'sellPrice',style:'width:7rem;'},tdPrice);
			inpPrice.numBase = priceBase;
		}
		if(pricDisab){ inpPrice.setAttribute('disabled','disabled'); }
		if(difCurr){ pric=P2.priceME; }
		if(Fie.disc && Fie.disc!='N'){
			var td=$1.t('td',{style:'width:4.5rem;'},tr);
			var discI=$1.t('input',{type:'number',inputmode:'numeric',min:0,max:100,'class':jsF+' __tdNumDiscVar', value:P2.disc,name:ln+'[disc]',onkeychange:function(){ $Tol.tbSum(tBody.parentNode); },style:'width:3rem;'},td);
			td.appendChild($1.t('textNode','%'));
			if(discDisab){ discI.setAttribute('disabled','disabled'); }
		}
		td=$1.t('td',{'class':'__tdTotal',vformat:'money'},tr);
		}
		qua.O={vPost:vPost};
		td=$1.t('td',0,tr);
		_Frm.tdBtnDel({td:td,ln:ln});
	}
	$Tol.tbSum(tBody.parentNode);
},
tdBtnDel:function(P){
	var td=P.td; var tr=P.tr; var ln=P.ln;
	if(!td.classList.contains('_noHidden')){ td.classList.add('_noHidden'); }
	var tr=(P.tr)?P.tr:td.parentNode;
	var tbPare=(P.tbPare)?P.tbPare:tr.parentNode.parentNode;//tbody->table
	var btn=$1.T.btnFa({fa:'fa_close',textNode:' Quitar', func:function(T){
		var tds=$1.q('td',tr,'all');//tr
		var ttd=T.parentNode;//td
		var tdTotal=$1.q('.__tdTotal',tr);
		for(var i3=0; i3<tds.length;i3++){
			if(ttd.visible=='Y' && !(tds[i3].classList.contains('_noHidden'))){ tds[i3].style.visibility='hidden'; }
			else{ tds[i3].style.visibility=''; }
		}
		if(ttd.visible=='Y'){
			if(tdTotal){ tdTotal.classList.add('__tdTotalNoCount'); }
			T.innerText= ' Restablecer';
			T.classList.replace('fa_close','fa_arrowBack');
			ttd.visible='N';
			$1.t('input',{type:'hidden',value:'Y',name:ln+'[delete]','class':jsF+' _inputDelete'},T.parentNode);
		}
		else{
			ttd.visible='Y';
			T.innerText=' Quitar';
			T.classList.replace('fa_arrowBack','fa_close');
			if(tdTotal){ tdTotal.classList.remove('__tdTotalNoCount'); }
			$1.delet($1.q('._inputDelete',ttd));
		}
			$Tol.tbSum(tbPare);
		}
	},td);
	return btn;
}
}

/* ITM */
var Itm={};

Itm.Ht={
formComm:function(cont,Jr,P){
	jsF=(P.jsF)?P.jsF:'jsFields';
	var itemGr=$V.itemGrP; 
	if(P.itemType=='MP'){ itemGr=$V.itemGrMP; }
	else if(P.itemType=='SE'){ itemGr=$V.itemGrSE; }
	var divL=$1.T.divL({divLine:1, wxn:'wrapx10',req:'Y',L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsF+' _itemCode',name:'itemCode',value:Jr.itemCode,O:{vPost:'itemId='+Pa.itemId+'&old[grsId]='+Jr.grsId}}},cont);
	$1.T.divL({wxn:'wrapx10',req:'Y',L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsF,name:'status'},opts:{active:'Activo',inactive:'Inactivo'},selected:Jr.status,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx10',req:'Y',L:{textNode:'Estado Web',title:'Permitir Generar Pedido'},I:{tag:'select',sel:{'class':jsF,name:'webStatus'},opts:{active:'Activo',inactive:'Inactivo'},selected:Jr.webStatus,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx3',req:'Y',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
	$1.T.divL({wxn:'wrapx8',req:'Y',L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsF,name:'itemGr'},opts:itemGr,selected:Jr.itemGr}},divL);
	$1.T.divL({wxn:'wrapx10',req:'Y',L:{textNode:'UdM'},I:{tag:'select',sel:{'class':jsF,name:'udm'},opts:Udm.O,selected:Jr.udm}},divL);
	var dv=$1.T.divL({wxn:'wrapx10',req:'Y',L:{textNode:'Tallaje'},I:{tag:'select',sel:{'class':jsF+' __grsId',name:'grsId'},opt1:{k:$V.uniqSize,t:'Sin Talla',vPost:'handSize=Y'} ,opts:$V.ogrs,selected:Jr.grsId,noBlank:1}},divL);
	var grs=$1.q('.__grsId',dv); 
	grs.O={vPost:((Jr.grsId==$V.uniqSize)?'handSize=N':'handSize=Y')};
	grs.onchange =function(){
		this.O.vPost =(this.value==$V.uniqSize)?'handSize=N':'handSize=Y';
	}
	if(P.itemType=='MP' || P.grSize=='N'){ dv.style.display='none'; }
	var div=$1.t('div',{style:'padding:0.5rem 0;',textNode:'Artículo de: '},cont);
		$1.T.ckLabel({t:'Compra',I:{'class':jsF,name:'buyItem',checked:(Jr.buyItem=='Y')}, func:function(cke){
			var dbuy=$1.q('.__wrapBuyItem',cont);
			dbuy.style.display=(cke=='Y')?'block':'none';
		}},div);
		$1.T.ckLabel({t:'Venta',I:{'class':jsF,name:'sellItem',checked:(Jr.sellItem=='Y')}, func:function(cke){
			var dSell=$1.q('.__wrapSellItem',cont);
			dSell.style.display=(cke=='Y')?'block':'none';
		}},div);
		$1.T.ckLabel({t:'Inventario',I:{'class':jsF,name:'handInv',checked:(Jr.handInv=='Y')}},div);
		if(P.itemType=='P'){
			$1.T.ckLabel({t:'Producción',I:{'class':jsF,name:'prdItem',checked:(Jr.prdItem=='Y')}},div);
		}
	var winM=$1.Menu.inLine([
		{textNode:'General','class':'active',winClass:'_general'},
		{textNode:'Compras',winClass:'_compras'},
		{textNode:'Ventas','class':'',winClass:'_ventas'},
		{textNode:'Producción','class':'',winClass:'_produccion'},
		{textNode:'Propiedades','class':'',winClass:'_prp'}
		],{winCont:1}); cont.appendChild(winM);
	var gen=$1.t('div',{'class':'winMenuInLine _general'},winM);
	$1.Tb.trsI([
		{t:'ID Adicional',node:$1.t('input',{type:'text','class':jsF,name:'idAdd',value:Jr.idAdd})},
		{t:'Sujeto a Impuesto',node:$1.T.sel({sel:{'class':jsF,name:'taxAval'},opts:$V.NY,selected:Jr.taxAval})},
		{t:'Sujeto a Rte. Impuesto',node:$1.T.sel({sel:{'class':jsF,name:'taxRte'},opts:$V.NY,selected:Jr.taxRte})},
		{t:'% Impuesto (IVA)',node:$1.T.sel({sel:{'class':jsF,name:'taxCode'},opts:$Tb.otaxI,selected:Jr.taxCode})},
		{t:'% Retención',node:$1.T.sel({sel:{'class':jsF,name:'rteCode'},opts:$Tb.otaxR,selected:Jr.rteCode})},
		{t:'Grupo Contable',node:$1.T.sel({sel:{'class':jsF,name:'accgrId'},opts:$Tb.oiac,selected:Jr.accgrId})}
		],gen);
	var com=$1.t('div',{'class':'winMenuInLine _compras',style:'display:none'},winM);
	var sup=$Sea.input(null,{api:'crd.s','class':jsF, vPost:'cardId='+Jr.cardId+'&cardName='+Jr.cardName, defLineText:Jr.cardName, func:function(L,inp){
			inp.O={vPost:'cardId='+L.cardId+'&cardName='+L.cardName};
		}});
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Precio',title:'Precio de Compra'},I:{tag:'input',type:'text',min:0,inputmode:'numeric',numberformat:'mil','class':jsF,name:'buyPrice',value:Jr.buyPrice}},com);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Und de Compra'},I:{tag:'select',sel:{'class':jsF,name:'buyUdm'},opts:Udm.O,selected:Jr.buyUdm}},divL);
		$1.T.divL({wxn:'wrapx10',L:{textNode:'Operación',title:'Operación para convertir en unidad de medida'},I:{tag:'select',sel:{'class':jsF,name:'buyOper'},opts:$V.mathOper,selected:Jr.buyOper,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx10',L:{textNode:'Cant. x Und.',title:'Cantidad por und de compra.'},I:{tag:'input',type:'number',min:1,inputmode:'numeric','class':jsF,name:'buyFactor',value:Jr.buyFactor}},divL);
		$1.Tb.trsI([
		{t:'Proveedor Predeterminado',node:sup},
		{t:'Método Planificación',node:$1.T.sel({sel:{'class':jsF,name:'mrpPlan'},opts:$V.mrpPlan,selected:Jr.mrpPlan,noBlank:1})},
		{t:'Cant. Pedido Mínima',node:$1.t('input',{type:'number',inputmode:'numeric','class':jsF,name:'minBuyOrd',min:0,value:Jr.minBuyOrd})},
		{t:'Tiempo de entrega',node:$1.t('input',{type:'number',inputmode:'numeric','class':jsF,name:'leadTime',min:0,value:Jr.leadTime})}
		],com);
		
		var vt=$1.t('div',{'class':'winMenuInLine _ventas',style:'display:none'},winM);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Precio',title:'Precio de Venta'},I:{tag:'input',type:'text',min:0,inputmode:'numeric',numberformat:'mil','class':jsF,name:'sellPrice',value:Jr.sellPrice}},vt);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Unidad de Venta'},I:{tag:'select',sel:{'class':jsF,name:'sellUdm'},opts:Udm.O,selected:Jr.sellUdm}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Operación',title:'Operación a Udm al Vender'},I:{tag:'select',sel:{'class':jsF,name:'sellOper'},opts:$V.mathOper,selected:Jr.sellOper,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Factor',title:'Unidad por Cantidad de Venta'},I:{tag:'input',type:'number',min:1,inputmode:'numeric','class':jsF,name:'sellFactor',value:Jr.sellFactor}},divL);
		var prd=$1.t('div',{'class':'winMenuInLine _produccion',style:'display:none'},winM);
		$1.Tb.trsI([
		{t:'Costo Estandar de Producción',node:$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'prdStdPrice',value:Jr.prdStdPrice,min:0})}
		],prd);
		var Lpr=[]; var jrPrp=(Jr.Prp && !Jr.errNo)?Jr.Prp:{};
		var prp=$1.t('div',{'class':'winMenuInLine _prp',style:'display:none'},winM);
		for(var i in $Tb.oitp){
			var tag=$1.t('input',{type:'text','class':jsF,name:'PRP['+i+'][prValue]',value:jrPrp[i]});
			if($Tb.oitp_opts && $Tb.oitp_opts[i]){
				var tp=$Tb.oitp_opts[i];
				if(tp.tag=='select'){ tag=$1.T.sel({sel:{'class':jsF,name:'PRP['+i+'][prValue]'},opts:tp.opts,selected:jrPrp[i]}); }
			}
			ck=$1.t('input',{type:'checkbox','class':jsF,name:'PRP['+i+'][active]',checked:(!!(jrPrp[i]))});
			Lpr.push({tds:'Y',td1:{textNode:$Tb.oitp[i]},td2:ck,td3:tag});
		}
		$1.Tb.trsI(Lpr,prp);
}
}

Itm.p = {
get:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Itm.a+'p', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Grupo','Estado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemGrP[L.itemGr]},tr);
				$1.t('td',{textNode:$V.active[L.status]},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('itm.p.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Características', P:L, func:function(T){ $M.to('itm.chr.form','itemId:'+T.P.itemId); } },
					{ico:'iBg iBg_barcode',textNode:' Códigos de Barras',href:$M.to('itm.BC.form','itemId:'+L.itemId,'r')}
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(){
	cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Itm.a+'p.formData',loade:cont, loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, func:function(Jr){
		Jr=(Jr)?Jr:{};
		$1.t('input',{type:'hidden','class':jsF,name:'itemType',value:'P'},cont);//1=modelo
		var TB1=(Jr.TB1)?Jr.TB1:{};
		var TBC=(Jr.TBC)?Jr.TBC:{};
		Jr.buyFactor=(Jr.buyFactor)?Jr.buyFactor:1;
		Jr.sellFactor=(Jr.sellFactor)?Jr.sellFactor:1;
		Itm.Ht.formComm(cont,Jr,{jsF:jsF,itemType:'P'});
		var resp=$1.t('div',0,cont);
		var btnSend=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.a+'p.formData', getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
			if(!Jr2.errNo){
				$1.q('._itemCode').O.vPost = 'itemId='+Jr2.itemId;
			}
			$ps_DB.response(resp,Jr2);
		}});
		cont.appendChild(btnSend);
	}});
}
}

Itm.mp = {
get:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Itm.a+'mp', inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Grupo','Estado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemGrMP[L.itemGr]},tr);
				$1.t('td',{textNode:$V.active[L.status]},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('itm.mp.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Características', P:L, func:function(T){ $M.to('itm.chr.form','itemId:'+T.P.itemId); } },
					{ico:'iBg iBg_barcode',textNode:' Códigos de Barras',href:$M.to('itm.BC.form','itemId:'+L.itemId,'r')}
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(){
	cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Itm.a+'mp.formData',loade:cont, loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, func:function(Jr){
		Jr=(Jr)?Jr:{};
		var TB1=(Jr.TB1)?Jr.TB1:{};
		var TBC=(Jr.TBC)?Jr.TBC:{};
		Jr.buyFactor=(Jr.buyFactor)?Jr.buyFactor:1;
		Jr.sellFactor=(Jr.sellFactor)?Jr.sellFactor:1;
		$1.t('input',{type:'hidden','class':jsF,name:'itemType',value:'MP'},cont);
		Itm.Ht.formComm(cont,Jr,{jsF:jsF,itemType:'MP'});
		var resp=$1.t('div',0,cont);
		var btnSend=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.a+'mp.formData', getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
			if(!Jr2.errNo){
				$1.q('._itemCode').O.vPost = 'itemId='+Jr2.itemId;
			}
			$ps_DB.response(resp,Jr2);
		}});
		cont.appendChild(btnSend);
	}});
}
}

Itm.se={
get:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Itm.a, inputs:'itemType=SE&'+$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Grupo','Estado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemGrSE[L.itemGr]},tr);
				$1.t('td',{textNode:$V.active[L.status]},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('itm.se.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Características', P:L, func:function(T){ $M.to('itm.chr.form','itemId:'+T.P.itemId); } }
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(){
	cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Itm.a+'formData',loade:cont,  loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, func:function(Jr){
		Jr=(Jr)?Jr:{};
		var TB1=(Jr.TB1)?Jr.TB1:{};
		var TBC=(Jr.TBC)?Jr.TBC:{};
		Jr.buyFactor=(Jr.buyFactor)?Jr.buyFactor:1;
		Jr.sellFactor=(Jr.sellFactor)?Jr.sellFactor:1;
		$1.t('input',{type:'hidden','class':jsF,name:'itemType',value:'SE'},cont);
		Itm.Ht.formComm(cont,Jr,{jsF:jsF,itemType:'SE',grSize:'N'});
		$1.t('div',{textNode:'Datos de Compra (Costo Materiales)','class':'divLineTitleSection'},cont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Unidad'},I:{tag:'select',sel:{'class':jsF,name:'buyUdm'},opts:Udm.O,selected:Jr.buyUdm}},cont);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Precio',title:'Precio de Compra'},I:{tag:'input',type:'text',min:0,inputmode:'numeric',numberformat:'mil','class':jsF,name:'buyPrice',value:Jr.buyPrice}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Operación',title:'Operación a Udm al Comprar'},I:{tag:'select',sel:{'class':jsF,name:'buyOper'},opts:$V.mathOper,selected:Jr.buyOper,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Factor',title:'Número para operación'},I:{tag:'input',type:'number',min:1,inputmode:'numeric','class':jsF,name:'buyFactor',value:Jr.buyFactor}},divL);
		$1.t('div',{textNode:'Datos de Venta','class':'divLineTitleSection'},cont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Unidad'},I:{tag:'select',sel:{'class':jsF,name:'sellUdm'},opts:Udm.O,selected:Jr.sellUdm}},cont);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Precio',title:'Precio de Venta'},I:{tag:'input',type:'text',min:0,inputmode:'numeric',numberformat:'mil','class':jsF,name:'sellPrice',value:Jr.sellPrice}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Operación',title:'Operación a Udm al Vender'},I:{tag:'select',sel:{'class':jsF,name:'sellOper'},opts:$V.mathOper,selected:Jr.sellOper,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Factor',title:'Unidad por Cantidad de Venta'},I:{tag:'input',type:'number',min:1,inputmode:'numeric','class':jsF,name:'sellFactor',value:Jr.sellFactor}},divL);
		var resp=$1.t('div',0,cont);
		var btnSend=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.a+'formData', getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
			if(!Jr2.errNo){
				$1.q('._itemCode').O.vPost = 'itemId='+Jr2.itemId;
			}
			$ps_DB.response(resp,Jr2);
		}});
		cont.appendChild(btnSend);
	}});
}
}

Itm.Chr={
form:function(){
	cont = $M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Itm.chr+'.formData', inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
	if(Jr.errNo){ $ps_DB.response(cont,Jr); }
	else{ $1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont); }
	var tb=$1.T.table(['Valor','Caracteristica']); cont.appendChild(tb);
	var tBody=$1.t('tbody',0,tb);
	for(var c in Jr.L){ L=Jr.L[c];
		var tr=$1.t('tr',0,tBody);
		var td=$1.t('td',0,tr);
		var co=L.config;
		if(L.chrType=='select'){
			var sel=$1.T.sel({sel:{'class':jsF,name:'CHR['+L.chrId+']'},opts:eval(co.opts),selected:L.value});
			td.appendChild(sel);
		}
		else{
			$1.t('input',{type:L.chrType,'class':jsF,name:'CHR['+L.chrId+']',value:L.value},td);
		}
		$1.t('td',{textNode:L.chrName},tr);
	}
	resp=$1.t('div',0,cont);
	var btnSend=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.chr+'.formData', getInputs:function(){ return 'itemId='+Pa.itemId+'&'+
	$1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
		$ps_DB.response(resp,Jr2);
	}});
	cont.appendChild(btnSend);
	}});
},

}

Itm.BC={
form:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	var vPost='itemId='+Pa.itemId+'&';
	var h5=$1.t('h4',0,cont); var jsF='jsFields';
	var grC=$1.T.sel({opts:$V.bar2,noBlank:1,sel:{'class':jsF,name:'grTypeId'}}); cont.appendChild(grC);
	grC.onchange=function(){ w(); }
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Grupo Código'},Inode:grC},cont);
	var wTb=$1.t('div',0,cont);
	w();
	function w(){
		$ps_DB.get({f:'GET '+Api.Itm.bc, loadVerif:!Pa.itemId, loade:wTb, inputs:vPost+$1.G.inputs(divL,jsF), 
		func:function(Jr){
			h5.innerText=Jr.itemCode+') '+Jr.itemName;
			Jr.L=$js.sortNum(Jr.L,{k:'itemSize'});
			var tb=$1.T.table(['Talla','Código']); wTb.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i]; var k=L.itemSzId;
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.itemSize},tr);
				var td=$1.t('td',0,tr);
				$1.t('input',{type:'text','class':jsF,name:'TA['+k+']',value:L.barCode},td);
			}
		}});
	}
	var resp=$1.t('div',0,cont);
	var btnSend=$1.T.btnSend({textNode:'Definir y Guardar Códigos'},{f:'PUT '+Api.Itm.bc, getInputs:function(){ return vPost+$1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
		$ps_DB.response(resp,Jr2);
	}});
	cont.appendChild(btnSend);
}
}

Itm.RC={
list:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Itm.rc, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Tipo','Grupo','Actualizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemType[L.itemType]},tr);
				var ty=(L.itemType=='P')?$V.itemGrP:$V.itemGrMP;
				$1.t('td',{textNode:ty[L.itemGr]},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Composición Base', P:L, func:function(T){ $M.to('itm.rc.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Composición de Variantes', P:L, func:function(T){ $M.to('itm.rc.form2','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Mano de Obra', P:L, func:function(T){ Wma.Mpg.view({itemId:T.P.itemId,h5Text:T.P.itemCode+') '+T.P.itemName}) } }
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Itm.rc+'.formData', errWrap:cont, loade:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Variante',{textNode:'Fase Cons.',style:'width:4rem;'},'Total','']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $ps_DB.response(tb,Jr.L); }
		else if(!Jr.L.errNo) for(var i in Jr.L){ trA(Jr.L[i],tBody); }
		var btnA=$1.T.btnFa({fa:'fa_cells', textNode:'Añadir 2 Lineas',func:function(){ trA({},tBody); trA({},tBody); } }); cont.appendChild(btnA);
		var tfoot = $1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tfoot);
		$1.t('td',{colspan:6,textNode:'Total'},tr);
		$1.t('td',{},tr);
		$1.t('td',{'class':'__tbTotal',vformat:'money'},tr);
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.rc+'.formData', loade:resp, getInputs:function(){ return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$ps_DB.response(resp,Jr2);
		} }); cont.appendChild(btnSend);
		$Tol.tbSum(tBody.parentNode);
	}});
	n=1;
	function trA(L,tBody){
		$1.nullBlank='';
		var tr=$1.t('tr',0,tBody);
		var ln='L['+n+']';
		$1.t('td',{textNode:n},tr);
		var td=$1.t('td',0,tr);
		var inpPie= $1.t('input',{type:'text','class':jsF,name:ln+'[pieceName]',value:L.pieceName},td);
		var td=$1.t('td',{'class':$Sea.clsName,k:'itemCode',style:'width:6rem;',textNode:L.itemCode},tr);//itemCode
		var td=$1.t('td',0,tr);
		var vPost=(L.udm)?ln+'[id]='+L.id+'&'+ln+'[citemId]='+L.citemId+'&'+ln+'[buyPrice]='+L.buyPrice:'';
		inpPie.O ={vPost:vPost};
		$Sea.input(td,{api:'itemData','class':$Sea.clsName,k:'itemName',inputs:'wh[I.itemType(E_noIgual)]=P&fields=I.udm,I.buyPrice',defLineText:L.itemName, vPost:vPost, func:function(Jr2,inp){
			inpPie.O.vPost = ln+'[id]='+L.id+'&'+ln+'[citemId]='+Jr2.itemId+'&'+ln+'[buyPrice]='+Jr2.buyPrice;
			$Sea.replaceData(Jr2,inp.pare.parentNode);
		}});
		var td=$1.t('td',{'class':$Sea.clsNameInp+' __tdNum',k:'buyPrice',kformat:'money',style:'width:6rem;',textNode:$Str.money(L.buyPrice)},tr);
		var td=$1.t('td',{'class':$Sea.clsNameInp,k:'udm',style:'width:4rem',textNode:L.udm},tr);
		var td=$1.t('td',0,tr);
		var inp= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __tdNum2',name:ln+'[quantity]',style:'width:6rem;',value:L.quantity*1},td);
		inp.onkeyup = inp.onchange = function(){
			$Tol.tbSum(tBody.parentNode);
		}
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[variType]'},opts:$V.variType,selected:L.variType,noBlank:1}); td.appendChild(sel);
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId}); td.appendChild(sel);
		var td=$1.t('td',{'class':'__tdTotal',style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
		var td=$1.t('td',0,tr);
		if(L.id){
			$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
		}
		else{ var btn =$1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(tr); }}); td.appendChild(btn); }
		n++; $1.nullBlank=false;
	}
},
form2:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	n=1;
	var jsF='jsFields'; var ni = 1;
	$ps_DB.get({f:'GET '+Api.Itm.rc+'.formData2', errWrap:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb); trH.classList.add('trHead');
		var Ta=$V.grs2[Jr.grsId];
		$1.t('td',{textNode:'Variante'},trH);
		var trs=1;
		for(var t in Ta){ $1.t('td',{textNode:t},trH); }
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $ps_DB.response(tb,Jr.L); }
		else if(!Jr.L.errNo){
			for(var i in Jr.L){ L=Jr.L[i];
				if(trs==5){ tBody.appendChild(trH.cloneNode(1)); trs=1; } trs++;
				var tr=$1.t('tr',0,tBody);
				var csp=(L.variType=='tcode')?2:1;
				$1.t('td',{textNode:n,rowspan:csp},tr); n++;
				$1.t('td',{textNode:L.pieceName,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemCode,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemName,rowspan:csp},tr);
				$1.t('td',{textNode:L.buyPrice,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:L.udm,rowspan:csp},tr);
				$1.t('td',{textNode:L.quantity,pformat:'float_2',rowspan:csp},tr);
				$1.t('td',{textNode:L.lineTotal,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:$V.variType[L.variType],rowspan:csp},tr);
				if(L.variType=='tcons'){ trTabl(L,tr,ni); ni++; }
				else if(L.variType=='tcode'){
					var tr2=$1.t('tr',0,tBody);
					trTabl(L,tr2,ni);
					trCode(L,tr,ni); ni++;
				}
			}
		}
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Itm.rc+'.formData2', loade:resp, getInputs:function(){ $1.G.noGet.push('0'); return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$ps_DB.response(resp,Jr2);
		} }); cont.appendChild(btnSend);
		function trTabl(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPost=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[buyPrice]='+Ld.buyPrice+'&'+ln+'[citemId]='+Ld.citemId+'&'+ln+'[itemSzId]='+ta+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[id]='+L.id;
				$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem;','class':jsF+' _ntak_'+ni+'_'+ta,name:ln+'[quantity]',O:{vPost:vPost},value:L.quantity},td);
				if(L.id){
					$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
				}
			}
		}
		function trCode(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPost=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[id]='+L.id+'&'+ln+'[itemSzId]='+ta+'&';
				var vPost2=(L.id)?vPost+ln+'[citemId]='+L.citemId+'&'+ln+'[buyPrice]='+L.buyPrice:'';
				var ntak='_ntak_'+ni+'_'+ta;
				var inpta=$1.q('.'+ntak,tr.parentNode.parentNode); inpta.O={vPost:vPost2}
				$Sea.input(td,{api:'itemData','class':$Sea.clsName+' '+jsF,k:'itemName',inputs:'FIE[][I.itemType]=MP&fields=I.udm,I.buyPrice',defLineText:L.itemName, inpVPost:inpta ,fPars:{vPost:vPost,ln:ln,ntak:ntak}, func:function(Jr2,inp,fPars){
					var inpta=$1.q('.'+fPars.ntak,tb); 
					inpta.O = {vPost:fPars.vPost+fPars.ln+'[citemId]='+Jr2.itemId+'&'+fPars.ln+'[buyPrice]='+Jr2.buyPrice};
					$Sea.replaceData(Jr2,inp.pare);
				}});
			}
		}
		
	}});
},
}

//Abrir ventana para añadir tallas
Itm.winAdd=function(P,L){ L=(L)?L:{};
	var wrap=$1.t('div'); var jsF='jsFields';
	var Ta=(L.grsId)?$V.grs2[L.grsId]:{};
	var tb=$1.T.table(['Talla','Cant.','Talla','Cant.','Talla','Cant.']); wrap.appendChild(tb);
	var tBody=$1.t('tbody',0,tb);
	var tds=0;
	for(var ta in Ta){
		if(tds==0 || tds%3==0){ var tr=$1.t('tr',0,tBody); } tds++;
		$1.t('td',{textNode:Ta[ta],style:'backgroundColor:#EEE;'},tr);
		var td=$1.t('td',0,tr);
		var inp=$1.t('input',{type:'number',inputmode:'numeric','class':jsF+' _tdTa __tbColNums','tbColNum':1,name:'',style:'width:4rem;',onkeychange:function(){ $Tol.tbSum(tb); }},td);
		inp.ta=ta;
	}
	var reqPrice=(P.priceDefiner!='N');
	if(reqPrice){
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{colspan:2,textNode:'Cant. total'},tr);
	$1.t('td',{colspan:4,'class':'__tbColNumTotal1'},tr);
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{colspan:2,textNode:'Precio'},tr);
	var td=$1.t('td',{colspan:4},tr);
	var price=(L.sellPrice)?L.sellPrice:L.buyPrice;
	$1.t('input',{type:'text','class':'inputPrice divLine',value:price,numberformat:'mil'},td);
		var tr=$1.t('tr',0,tBody);
	$1.t('td',{colspan:2,textNode:'Histórico',expTitle:'Último precio de venta relacionado al cliente.'},tr);
	var val=(L.lastSellPriceCard==-1)?'Error':$Str.money(L.lastSellPriceCard);
	val=(L.lastSellPriceCard==0)?'Primera Vez':val;
	var td=$1.t('td',{colspan:4,textNode:val},tr);
	}
	var resp=$1.t('p',0,wrap);
	var btn=$1.T.btnSend({textNode:'Añadir Lineas',func:function(){
		$1.clear(resp);
		var err=false; var errText='';
		var Ds={total:0,T:{}};
		var tas=$1.q('._tdTa',tb,'all'); 
		for(var i=0; i<tas.length;i++){
			if(tas[i].value!=''){ var nv=tas[i].value*1;
				Ds.T[tas[i].ta] = nv; Ds.total +=nv;
			}
		}
		var price=$1.q('.inputPrice',wrap);
		price=(price && price.value)?$Str.toNumber(price.value):0;
		if(Ds.total==0){ $ps_DB.response(resp,{errNo:3,text:'No se han definido cantidades por Talla'}); }
		else if(reqPrice && price<=0){ $ps_DB.response(resp,{errNo:3,text:'Defina un precio para continuar'}); }
		else{
			L.priceDefine=price;
			if(P.func){ P.func(Ds,L); }
			$1.delet(wrapBk);
		}
	}}); wrap.appendChild(btn);
	var wrapBk=$1.Win.open(wrap,{winSize:'full',onBody:1,winTitle:'Definir Cantidad por Tallas'});
}

Itm.Txt={
code:function(P){
	var t=(P.itemCode)?P.itemCode: ' Code N/D';
	t +=(P.itemSzId)?'-'+$V.grs1[P.itemSzId]:'';
	return t;
},
name:function(P){
	var t=(P.itemName)?P.itemName: ' Nombre N/D';
	t +=(P.itemSzId)?'\u0020\u0020T: '+$V.grs1[P.itemSzId]:'';
	return t;
},
size:function(P){
	var k=(typeof(P)=='object')?P.itemSzId:P;
	var t =(k)?$V.grs1[k]:'N/A';
	return t;
},
imgSrc:function(P){
	var img=(P.srcA)?P.srcA:$V.itmImageUndefined;
	if(img==null || img==undefined){ img ='http://st2.admsistems.com/_img/itmImgNot.jpg'; }
	return img;
}
}

$V.ordTypePE={A:'Primeras',B:'Segundas'};
var sellOrd={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	var basic=e.match(/basic,?/);
	if(basic || e.match(/modify,?/)){
		Li[n]={ico:'fa fa_pencil',textNode:' Modificar Orden', P:L, func:function(T){ $M.to('sellOrd.form','docEntry:'+T.P.docEntry); } }; n++;
	}
	if(e.match(/receive,?/)){
		Li[n]={ico:'fa fa_listWin',textNode:' Recibir Mercancía', func:function(){ buyOrd.markReceive({docEntry:L.docEntry,odocType:'opor'}); }}; n++;
	}
	if(basic){
		Li[n]={ico:'fa fa_info',textNode:' Información de Documento', P:L, func:function(T){ sellOrd.info(T.P); } }; n++;
	}
	if(L.docStatus=='D'){
		Li[n]={ico:'fa fa_listWin',textNode:' Enviar Pedido', func:function(){ Ord.mark2Send({docEntry:L.docEntry,odocType:'opvt'}); }}; n++;;
	}
	if(L.docStatus=='S'){
		Li[n]={ico:'fa fa_listWin',textNode:' Recibir Pedido', func:function(){ Ord.mark2Receive({docEntry:L.docEntry,odocType:'opvt'}); }}; n++;;
	}
	Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ _Doc.cancel({serieType:'opvt',docEntry:T.P.docEntry,api:Api.Ord.a+'status.cancel',text:'Se va anular el documento, no se puede reversar está acción.'}); } }; n++;
	Li[n]={ico:'iBg iBg_candado',textNode:'Cerrar Documento', P:L, func:function(T){ _Doc.cancel({docEntry:T.P.docEntry,api:Api.Ord.a+'status.handClose',text:'Se va a cerrar el Documento, no se puede reversar está acción.'}); } }; n++; 
	Li[n]={ico:'fa fa_pencil',textNode:' Volver a Borrador', P:L, func:function(T){ $Doc.statusDefine({docEntry:T.P.docEntry,api:Api.Ord.a+'status.openToDraw',text:'El documento será marcado como borrador para modificarlo. Recuerde que debe ser enviado y recibido nuevamente.'}); } }; n++;
	Li[n]={ico:'fa fa_eye',textNode:'Documento Pendientes', P:L, func:function(T){ $M.to('sellOrd.openQty','docEntry:'+T.P.docEntry); } }; n++;
		Li[n]={ico:'fa fa_',textNode:'Estado en Cartera',P:L,func:function(T){ $Doc.statusDefine({docEntry:T.P.docEntry,api:Api.Ord.a+'status.cartStatus',text:'Se va a definir el siguiente estado en cartera.',Opts:$V.opvtCartStatus,selected:T.P.cartStatus}); } }; n++;
		Li[n]={ico:'fa fa_attach',textNode:'Archivos',L:L,func:function(T){ $5fi.btnOnTb({tt:'opvt',tr:L.docEntry, getList:'Y',winTitle:'Pedido de Venta: '+L.docEntry}); } }; n++;
	if(basic){
		Li[n]={ico:'iBg iBg_money',textNode:'Recalcular Lineas',P:L,func:function(T){ $ps_DB.get({f:'PUT '+Api.Ord.a+'recalLines',inputs:'docEntry='+T.P.docEntry, func:function(Jr){ $1.Win.message(Jr); }}); } };
	}
	return Li={Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ord.a, inputs:$1.G.filter(), loade:cont, func:function(Jr){ 
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table([{'class':$Xls.tdNo},'N°-','Estado','Fecha Doc.','Cliente','Tipo','Fecha Entrega','Vendedor',{textNode:'M',title:'Moneda'},'Valor','Estado Despacho','Estado Cartera','Realizado',{'class':$Xls.tdNo}]);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				if(i==2){ tr.classList.add($Xls.trNo); }
				var td=$1.t('td',0,tr);
				var menu=$1.Menu.winLiRel(sellOrd.opts({L:L},'basic')); td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('sellOrd.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$V.ordStatus[L.docStatus],style:ColMt.get('opvt',L.docStatus)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d'),xls:{t:L.docDate,style:{format:'dd-mm-yyyy'}}},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$V.ordTypePE[L.docType]},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d'),xls:{t:L.dueDate}},tr);
				$1.t('td',{textNode:L.slpName},tr);
				$1.t('td',{textNode:L.curr},tr);
				$1.t('td',{textNode:$Str.money({curr:L.curr,value:L.priceVal}),xls:{t:L.priceVal}},tr);
				$1.t('td',{textNode:L.dlvStatus},tr);
				$1.t('td',{textNode:$V.opvtCartStatus[L.cartStatus]},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var td=$1.t('td',0,tr); 
				$1.T.btnFa({fa:'fa_comment',L:L,func:function(T){ $5c.form({tt:'opvt',tr:T.L.docEntry, getList:'Y',winTitle:'Pedido de Venta: '+T.L.docEntry}); } },td); 
				$1.T.btnFa({fa:'fa_attach',L:L,func:function(T){ $5fi.btnOnTb({tt:'opvt',tr:T.L.docEntry, getList:'Y',winTitle:'Pedido de Venta: '+T.L.docEntry}); } },td); 
			}
			tb=$1.T.tbExport(tb,{fileName:'Listado de Pedidos'});
			cont.appendChild(tb);
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; var jsF='jsFields'; var Pa=$M.read();
	$ps_DB.get({file:'GET '+Api.Ord.a+'form', loadVerif:!Pa.docEntry, loade:cont, inputs:'docEntry='+Pa.docEntry, func:function(Jr){
		var vS=(Jr.cardId)?{cardId:Jr.cardId}:null;
		var inputs='fields=A.slpId,A.discPf&_otD[addr][addrType(E_in)]=merch,inv';
		var sea=$Sea.input(null,{api:'crd.c',classKey:'___cardId', 'class':jsF,req:'Y', inputs:inputs, vS:vS, vPost:'cardId='+Jr.cardId+'&cardName='+Jr.cardName+'&discDef='+Jr.discDef, defLineText:Jr.cardName, func:function(L,inp){
			inp.O={vPost:'cardId='+L.cardId+'&cardName='+L.cardName+'&discDef='+L.discPf};
			$Sea.replaceData(L,cont);
			$Tol.tbSum(cont);
		}});
		var fFie={price:{disabled:'N'},disc:{disabled:'Y'}};
		{//fie table
			var tb=$1.T.table([{textNode:'#',style:'width:2rem;'},{textNode:'Código',style:'width:5rem;'},{textNode:'Descripción',style:'width:22rem;'},'Cant.','UdM','Precio','Desc.',{textNode:'Total',style:'width:8rem;'},{textNode:''}]);
		tb.classList.add('table_x100');
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
		cont.appendChild(fie);
		var tBody= $1.t('tbody',0,tb);
		var tFo= $1.t('tfoot',0,tb);
		var n=1;
		var trF=$1.t('tr',0,tFo); var cs=8;
		var td=$1.t('td',{colspan:3,textNode:'Total Pares: ',style:'text-align:right;'},trF);
		var td=$1.t('td',{'class':'__tbColNumTotal2'},trF);
		var td=$1.t('td',{colspan:cs-4,textNode:'Moneda: ',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2},trF);
		var sel=$1.T.sel({sel:{'class':jsF+' __tbCurr',name:'curr'},opts:$0s.Curr,noBlank:1,selected:Jr.curr}); td.appendChild(sel);
		sel.onchange=function(){ $Tol.tbSum(cont); }
		var trF=$1.t('tr',0,tFo);
		$1.t('td',{colspan:cs,textNode:'Total Lineas',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2,'class':'__tbTotal',vformat:'money',style:'width:4rem;'},trF);
		var trF=$1.t('tr',0,tFo);
		/*ojo: quito descuento  k:discPf*/
		if($jSoc.discPfActive=='Y'){
				var disc=$1.t('td',{colspan:cs,textNode:'Descuento: ',style:'text-align:right;'},trF);
			var inpd=$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem;','class':jsF+' __tbDisc '+$Sea.clsName,k:'discPf',name:'discPf',value:(Jr.discPf*1)},disc);
		}
		else{
				var disc=$1.t('td',{colspan:cs,textNode:'Descuento: ',style:'text-align:right; visibility:hidden;'},trF);
			var inpd=$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem; visibility:hidden;','class':jsF+' __tbDisc '+$Sea.clsName,k:'discPf__',name:'discPf',value:(Jr.discPf*1)},disc);
		}
		inpd.onkeyup=function(){ $Tol.tbSum(tb); }
		inpd.onchange=function(){ $Tol.tbSum(tb); }
		var td=$1.t('td',{colspan:2,'class':'__tbDiscText',vformat:'money',style:'width:4rem;'},trF);
		var trF=$1.t('tr',0,tFo);
		var disc=$1.t('td',{colspan:cs,textNode:'Total',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2,'class':'__tbTotal2',vformat:'money',style:'width:4rem;'},trF);
		Drw.itemReader({cont:fie,tBody:tBody,fields:'I.sellPrice,I.grsId,I.sellUdm&wh[I.sellItem]=Y&wh[I.webStatus]=active'},{funcAddInputs:function(){
			var gc=$1.q('.___cardId'); var v='';
			if(gc && gc.vS){ v=gc.vS.cardId; }
			return 'wh[lastSellPriceCardId]='+v;
		}, func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'tr',jsF:jsF,JrS:P.JrS, n:n,Fie:fFie, func:function(P,tBody){
				P.formType='venta';
				_Frm.itm(P,tBody);
			}}); n++;
	}});
		}
		dateDisab=($jSoc.opvt_docDateDisabled=='Y')?'disabled':null;
		Jr.docDate=(Jr.docDate)?Jr.docDate:$2d.today;
		if($jSoc.opvt_minDaysToDeliv && !Jr.dueDate){ Jr.dueDate=$2d.add($2d.today,'+'+$jSoc.opvt_minDaysToDeliv+'days','Y-m-d'); }
		Jr.dueDate=(Jr.dueDate)?Jr.dueDate:$2d.today;
		
		_Doc.formSerie({cont:cont,serieType:'opvt', docEntry:Pa.docEntry, jsF:jsF,
		api:'PUT '+Api.Ord.a+'form', func:function(Jr2){
			$M.to('sellOrd.view','docEntry:'+Jr2.docEntry);
		},
		middleCont:fie,
		Li:[
		{wxn:'wrapx8',L:'Tipo Documento',req:'Y',I:{tag:'select',sel:{'class':jsF,name:'docType'},opts:$V.ordTypePE,selected:Jr.docType}},
		{fType:'user'},
		{wxn:'wrapx8',L:'Estado',I:{tag:'select',sel:{disabled:'disabled',title:Jr.docStatus},opts:$V.ordStatus,selected:Jr.docStatus,noBlank:1}},
		{fType:'date',name:'docDate',req:'Y',value:Jr.docDate,I:{'disabled':dateDisab}},
		{fType:'date',name:'dueDate',req:'Y',value:Jr.dueDate,textNode:'Fecha Entrega'},
		{divLine:1,wxn:'wrapx8',L:{textNode:'Orden de Compra',style:'color:#F00;'},req:'Y',I:{tag:'input',type:'text','class':jsF,name:'ref1',value:((Jr.ref1)?Jr.ref1:'')}},
		{wxn:'wrapx3',req:'Y',L:'Cliente',Inode:sea},
		{wxn:'wrapx4',req:'Y',L:{textNode:'Empleado Ventas'},I:{tag:'select',sel:{'class':jsF+' '+$Sea.clsName,name:'slpId',k:'slpId',disabled:'disabled'},opts:$Tb.oslp,selected:Jr.slpId}},
		{divLine:1,wxn:'wrapx2',req:'Y',L:'Dirección para mercancías',I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'addrMerch',k:'_addr_merch',value:Jr.addrMerch}},
		{wxn:'wrapx2',req:'Y',L:'Dirección para facturación',I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'addrInv',k:'_addr_inv',value:Jr.addrInv}},
		{divLine:1,wxn:'wrapx1',L:{textNode:'Detalles/Observaciones'},I:{tag:'textarea','class':jsF,name:'lineMemo',textNode:Jr.lineMemo}}
		]
		});
		if(Jr.L && Jr.L.errNo){ $ps_DB.response(tBody,Jr.L); }
		else{
			Jr.jsF=jsF; Jr.formType='venta'; Jr.Fie=fFie;
			_Frm.itm(Jr,tBody);
		}
	}
	});
},
view:function(){ 
	var Pa=$M.read(); var contPa=$M.Ht.cont; $1.clear(contPa);
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;','class':'no-print'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	$ps_DB.get({f:'GET '+Api.Ord.a+'view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		divTop.appendChild($1.Menu.winLiRel(sellOrd.opts({L:Jr,textNode:'Opciones'},'basic')));
		if(Jr.docStatus=='D'){
			var btnSend=$1.T.btnFa({fa:'fa_listWin __btnSellOrdSend',textNode:' Enviar Pedido', func:function(){ Ord.mark2Send({docEntry:Jr.docEntry,odocType:'opvt'}); }});
			divTop.appendChild(btnSend);
		}
		if(Jr.docStatus=='S'){
			var btnSend=$1.T.btnFa({fa:'fa_listWin __btnSellOrdReceive',textNode:' Recibir Pedido', func:function(){ Ord.mark2Receive({docEntry:Jr.docEntry,odocType:'opvt'}); }});
			divTop.appendChild(btnSend);
		}
		sHt.sellOrdHead(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Código',style:'width:3rem;'},{textNode:'Descripcón'},{textNode:'Cant.',style:'width:5rem;'},{textNode:'UdM',style:'width:3rem;'},{textNode:'Precio',style:'width:6rem;'},{textNode:'Desc.',style:'width:3rem;'},{textNode:'Total',style:'width:8rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Pedido'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		var tFo=$1.t('tfoot',0,tb); var cs=7;
		var tr=$1.t('tr',0,tFo);
		$Str.useCurr=Jr.curr;
		$1.t('td',{textNode:'Total Pares',colspan:3,style:'text-align:right;'},tr);
		$1.t('td',{'class':'__tbColNumTotal1'},tr);
		$1.t('td',{textNode:'Total Lineas',colspan:cs-4,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Descuento ('+(Jr.discPf*1)+'%)',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal*Jr.discPf/100)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Total',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalVal)},tr);
		var currD=(Jr.curr!=$0s.currDefault);
		if(Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			var val=(currD)?L.priceME:L.price;
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.quantity*1,'class':tbSum.tdNum+' '+tbSum.tbColNums,tbColNum:1},tr);
			$1.t('td',{textNode:L.sellUdm},tr);
			$1.t('td',{textNode:$Str.money(val),'class':tbSum.tdNum2,vformat:'money'},tr);
			$1.t('td',{textNode:(L.disc*1)+'%'},tr);
			$1.t('td',{'class':tbSum.tdTotal,vformat:'money'},tr);
		}
		}
		$Str.useCurr=false;
		tbSum.get(tb);
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
info:function(P){
	var wrap=$1.t('div');
	$ps_DB.get({f:'GET '+Api.Ord.a+'info', inputs:'docEntry='+P.docEntry,loaderFull:true, func:function(Jr){
		$1.Tb.trFieVal({L:$TbExp.rep(Jr,'gvt_opvt')});
	}});
},
openQty:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont; $1.clear(contPa);
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;','class':'no-print'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	$ps_DB.get({f:'GET '+Api.Ord.a+'openQty', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		sHt.sellOrdHead(Jr,cont);
		$1.t('div',{textNode:'Solo se visualizan las lineas que tengan cantidades pendientes.',style:'font-weight:bold; padding:0.25rem;'},cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Código',style:'width:3rem;'},{textNode:'Descripcón'},{textNode:'UdM',style:'width:3rem;'},{textNode:'Pedido',style:'width:5rem;'},{textNode:'Pendiente',style:'width:5rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Pedido'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		var tFo=$1.t('tfoot',0,tb); var cs=7;
		var tr=$1.t('tr',0,tFo);
		$Str.useCurr=Jr.curr;
		$1.t('td',{textNode:'Total Pares',colspan:4,style:'text-align:right;'},tr);
		$1.t('td',{'class':'__tbColNumTotal1'},tr);
		$1.t('td',{'class':'__tbColNumTotal2'},tr);
		$1.t('td',{colspan:4,style:'text-align:right;'},tr);
		var currD=(Jr.curr!=$0s.currDefault);
		if(Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			if(L.openQty<=0){ continue; }
			$1.t('td',{textNode:L.lineNum},tr);
			var val=(currD)?L.priceME:L.price;
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.sellUdm},tr);
			$1.t('td',{textNode:L.quantity*1,'class':'__tdNum __tbColNums',tbColNum:1},tr);
			$1.t('td',{textNode:L.openQty*1,'class':'__tdNum __tbColNums',tbColNum:2},tr);
			
		}
		}
		$Str.useCurr=false;
		tbSum.get(tb);
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
}
sellOrdReview={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	Li[n]={ico:'fa fa_listWin',textNode:' Definir estado en Cartera', func:function(){ buyOrd.markReceive({docEntry:L.docEntry,odocType:'opor'}); }}; n++;
	return Li={Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ord.a, inputs:$1.G.filter(), loade:cont, func:function(Jr){ 
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table([{textNode:'','class':$Xls.tdNo},'N°','Estado','Fecha Doc.','Cliente','Tipo','Fecha Entrega','Vendedor',{textNode:'M',title:'Moneda'},'Valor','Estado Despacho','Realizado',{'class':$Xls.tdNo}]);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				if(i==2){ tr.classList.add($Xls.trNo); }
				var td=$1.t('td',0,tr);
				var menu=$1.Menu.winLiRel(sellOrd.opts({L:L},'basic')); td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('sellOrd.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$V.ordStatus[L.docStatus],style:ColMt.get('opvt',L.docStatus)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d'),xls:{t:L.docDate,style:{format:'dd-mm-yyyy'}}},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$V.ordTypePE[L.docType]},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d'),xls:{t:L.dueDate}},tr);
				$1.t('td',{textNode:L.slpName},tr);
				$1.t('td',{textNode:L.curr},tr);
				$1.t('td',{textNode:$Str.money({curr:L.curr,value:L.priceVal}),xls:{t:L.priceVal}},tr);
				$1.t('td',{textNode:L.dlvStatus},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var td=$1.t('td',0,tr); 
				$1.T.btnFa({fa:'fa_comment',L:L,func:function(T){ $5c.form({tt:'opvt',tr:T.L.docEntry, getList:'Y',winTitle:'Pedido de Venta: '+T.L.docEntry}); } },td); 
				$1.T.btnFa({fa:'fa_attach',L:L,func:function(T){ $5fi.btnOnTb({tt:'opvt',tr:T.L.docEntry, getList:'Y',winTitle:'Pedido de Venta: '+T.L.docEntry}); } },td); 
			}
			tb=$1.T.tbExport(tb,{fileName:'Listado de Pedidos'});
			cont.appendChild(tb);
		}
	}});
},
}

sellOrd.P={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ord.a+'pending', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var div=$1.t('div',{style:'text-align:right;'},cont);
			var resp=$1.t('div');
			var btnUpd=$1.T.btnFa({textNode:'Actualizar a cerrado pedidos ya completos',fa:'fa fa_history',func:function(){
				$ps_DB.get({f:'PUT '+Api.Ord.a+'status.closeIfopenQty', loade:resp, func:function(Jr0){
					$ps_DB.response(resp,Jr0);
					if(Jr0.update=='Y'){ sellOrd.P.get(); }
				}});
			}});
			div.appendChild(btnUpd);
			div.appendChild(resp);
			var tb=$1.T.table(['','N°','Fecha Doc.','Bodega','Cliente','Tipo','Fecha Entrega','Vendedor','Valor','Realizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var dlvBtn=$1.T.btnFa({fa:'iBg iBg_wareDisponible',textNode:' Despacho', L:L, func:function(T){ $M.Ht.ini({func_cont:function(){ sellDlv.form(T.L); } });; }});
				td.appendChild(dlvBtn);
				var menu=$1.Menu.winLiRel({Li:[
					{}
				]}); //td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('sellOrd.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$V.ordTypePE[L.docType]},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
				$1.t('td',{textNode:L.slpName},tr);
				$1.t('td',{textNode:$Str.money({curr:L.curr,value:L.priceVal})},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
			}
		}
	}});
},
}

var sellRep={
ordOpenQty:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Sell.rep+'ordOpenQty', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var viewType=$1.q('.__viewType',cont.parentNode).value;
			var Tds=['Código','Descripción',{textNode:'Talla'},'UdM','Pedido',{style:'background-color:#EEE;',textNode:'Pendiente'},'Bodega','En Bodega',{textNode:'Pendiente Total',title:'Total Pendientes con Otros Pedidos'},{textNode:'Ok',title:'¿Las cantidades disponibles cubren los pendientes Totales'},/* 10...*/
			'No. Pedido','Fecha Entrega','Cliente','Resp. Ventas'];
			if(viewType=='card'){ delete(Tds[10]); delete(Tds[11]); delete(Tds[12]) }
			else if(viewType!='doc'){ delete(Tds[10]); delete(Tds[11]); delete(Tds[12]); delete(Tds[13]) }
			var tb=$1.T.table(Tds);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr1=$1.t('tr',0,tBody);
				var v1=(L.openQty)?L.openQty*1:'';
				var v2=(L.onHand)?L.onHand*1:'';
				$1.t('td',{textNode:Itm.Txt.code(L)},tr1);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr1);
				$1.t('td',{textNode:$V.grs1[L.itemSzId]},tr1);
				$1.t('td',{textNode:L.udm},tr1);
				$1.t('td',{textNode:(L.quantity*1),'class':'__tbColNums',tbColNum:1},tr1);
				$1.t('td',{textNode:v1,style:'background-color:#EEE;','class':'__tbColNums',tbColNum:2},tr1);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr1);
				$1.t('td',{textNode:v2,'class':'__tbColNums',tbColNum:3},tr1);
				$1.t('td',{textNode:(L.onOrder*1),'class':'__tbColNums',tbColNum:4},tr1);
				var ok='No'; var okcss='backgroundColor:#F00;';
				if(L.onOrder*1<=L.onHand*1){ ok='Si'; okcss='backgroundColor:#0F0;'; }
				$1.t('td',{textNode:ok,style:okcss},tr1);
				if(viewType=='doc'){
					var td=$1.t('td',0,tr1);
					$1.t('a',{href:$M.to('sellOrd.openQty','docEntry:'+L.docEntry,'r'),textNode:L.docEntry},td);
					$1.t('td',{textNode:L.dueDate},tr1);
					$1.t('td',{textNode:L.cardName},tr1);
					$1.t('td',{textNode:$Tb.oslp[L.slpId]},tr1);
				}
				if(viewType=='card'){
					$1.t('td',{textNode:L.cardName},tr1);
					$1.t('td',{textNode:$Tb.oslp[L.slpId]},tr1);
				}
			}
			var tr=$1.t('tr',0,tBody);
			$1.t('td',0,tr);
			$1.t('td',{textNode:'Total'},tr);
			$1.t('td',0,tr); $1.t('td',0,tr);
			$1.t('td',{'class':'__tbColNumTotal1'},tr);
			$1.t('td',{'class':'__tbColNumTotal2'},tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':'__tbColNumTotal3'},tr);
			$1.t('td',{'class':'__tbColNumTotal4'},tr);
			$1.t('td',0,tr); $Tol.tbSum(tb);
			var gb=(viewType=='doc')?' agrupado por Pedido':'';
			gb=(viewType=='card')?' agrupado por Cliente':gb;
			tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Pendientes Totales'+gb});  cont.appendChild(tb);
		}
	}});
},
rdn:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Sell.rep+'rdn', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var Tds=['#','Estado','Fecha','Socio de Negocios','Fecha Recibido','Bodega','Código','Descripción','Talla','Cant','Motivo','Ref. 1','Clasificación','Garantía','N° Guia','Despachado',{textNode:'Días',title:'Dias transcurridos entre la fecha del documento y la fecha de despacho'}];
			var tb=$1.T.table(Tds);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr1=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docEntry},tr1);
				$1.t('td',{textNode:$V.rdnStatus[L.docStatus]},tr1);
				$1.t('td',{textNode:L.docDate},tr1);
				$1.t('td',{textNode:L.cardName},tr1);
				$1.t('td',{textNode:L.dateGet},tr1);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr1);
				$1.t('td',{textNode:Itm.Txt.code(L)},tr1);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr1);
				$1.t('td',{textNode:$V.grs1[L.itemSzId]},tr1);
				$1.t('td',{textNode:(L.quantity*1)},tr1);
				$1.t('td',{textNode:L.reason},tr1);
				$1.t('td',{textNode:L.ref1},tr1);
				$1.t('td',{textNode:$V.rdnClasif[L.ref2]},tr1);
				$1.t('td',{textNode:$V.rdnWarranty[L.ref3]},tr1);
				$1.t('td',{textNode:L.delivRef},tr1);
				$1.t('td',{textNode:L.delivDate},tr1);
				var daysToDeliv=($2d.is0(L.delivDate))?'Pendiente':L.daysToDeliv;
				$1.t('td',{textNode:daysToDeliv},tr1);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Reporte Devoluciones',print:1});
			cont.appendChild(tb);
		}
	}});
},
itemCanceled:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Sell.rep+'itemCanceled', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		var tb=$1.T.table(['Número','Creado','Fecha','Fecha Entrega','Socio de Negocios','Fecha de Anulación',{textNode:'Días',title:'Días entre creación y anulación'},'Usuario','Detalle Anulación']);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',0,tr).appendChild($Doc.href('opvt',L,{format:'id'}));
			$1.t('td',{textNode:L.dateC},tr);
			$1.t('td',{textNode:L.docDate},tr);
			$1.t('td',{textNode:L.dueDate},tr);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:L.dateCNull},tr);
			$1.t('td',{textNode:$2d.diff({date1:L.dateC,date2:L.dateCNull,round:'Y'})},tr);
			$1.t('td',{textNode:L.userId},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		cont.appendChild(tb);
	}});
}
}

var Ord={
mark2Send:function(P){
	var wrap=$1.t('div',0);
	$1.t('p',{textNode:'El documento será marcado como ENVIADO, ya no podrás modificarlo.'},wrap);
	var resp=$1.t('div',0,wrap);
	var c=$1.T.btnSend({textNode:'Marcar como Enviado'},{f:'PUT '+Api.Ord.a+'status.send',getInputs:function(){ return 'docEntry='+P.docEntry+'&odocType='+P.odocType; }, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){ $1.delet('.__btnSellOrdSend'); }
	}});
	wrap.appendChild(c);
	$1.Win.open(wrap,{winTitle:'Marcar como Enviado',onBody:1,winSize:'medium'});
},
mark2Receive:function(P){
	var divL=$1.T.divL({divLine:1,wxn:'wrapx1',supText:'Defina desde que Bodega Saldrá la Mercancía.',L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':'jsFields',name:'whsId',O:{vPost:'docEntry='+P.docEntry+'odocType='+P.odocType}},opts:$V.whsCode}});
	$1.Win.confirm({text:'El documento será marcado como recibido, entrará a formar parte de los pendientes y no podrá modificarse.', noClose:1, Inode:divL, func:function(resp,wrapC){
		$ps_DB.get({f:'PUT '+Api.Ord.a+'status.receive',inputs:$1.G.inputs(wrapC), loade:resp, func:function(Jr){
			$ps_DB.response(resp,Jr);
			if(!Jr.errNo){ $1.delet('.__btnSellOrdReceive'); }
		}});
	}});
}
}

var sellDlv={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	var basic=e.match(/basic,?/);
	if(basic){
	Li[n]={ico:'fa fa_pencil',textNode:' Modificar Documento', P:L, func:function(T){ $M.to('sellDlv.form','docEntry:'+T.P.docEntry); } }; n++;
	Li[n]={ico:'iBg iBg_barcode',textNode:' Añadir Cantidades', P:L, func:function(T){ $M.to('sellDlv.digit','docEntry:'+T.P.docEntry); } }; n++;
	Li[n]={ico:'fa fa_cells',textNode:' Lista de Empaque', P:L, func:function(T){ $M.to('sellDlv.packing','docEntry:'+T.P.docEntry); } },
	Li[n]={ico:'fa fa_cells',textNode:' Rótulo Empaque', P:L, func:function(T){ $M.to('sellDlv.packingRol','docEntry:'+T.P.docEntry); } },
	{ico:'fa fa_history',textNode:' Generar Movimiento en Bodega', P:L, func:function(T){ Ivt.btnTransfer({docType:'odlv',docEntry:T.P.docEntry,whsId:T.P.whsId},'func')(); } }; n++;
	}
	if(basic){
	Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ _Doc.cancel({docEntry:T.P.docEntry,api:Api.Ord.dlv+'status.cancel',text:'Se va anular el documento. Las cantidades pendientes del pedido de origen volverán a definirse a los valores que tenian antes de la creación de este documento.'}); } }; n++;
	}
	return Li={Li:Li,textNode:P.textNode};
},
form:function(P){
	cont=$M.Ht.cont; var jsF='jsFields'; Pa=$M.read();
	$ps_DB.get({f:'GET '+Api.Ord.dlv+'form',loadVerif:!Pa.docEntry, inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}},cont);
	var vTT='cardId='+Jr.cardId+'&tt='+Jr.tt+'&tr='+Jr.tr;
	Jr=(Jr.docEntry)?Jr:P;
	if(!Pa.docEntry){ Jr.tr=Jr.docEntry;
		Jr.lineMemoOrd=Jr.lineMemo; Jr.lineMemo='';
		var vTT='cardId='+Jr.cardId+'&tt='+$oTy.opvt+'&tr='+Jr.tr;
	}
	$1.T.divL({wxn:'wrapx4',subText:'De salida de mercancía',req:'Y',L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsF,name:'whsId'},opts:$V.whsCode,selected:Jr.whsId}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Usuario'},I:{tag:'input',type:'text',disabled:'disabled',value:$0s.userName}},divL);
	$1.T.divL({wxn:'wrapx8',subText:'logística...',L:{textNode:'Guia Despacho'},I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'delivRef1',value:Jr.delivRef1}},divL);
		$1.T.divL({wxn:'wrapx8',subText:'factura...',L:{textNode:'No. Referencia'},I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'delivRef2',value:Jr.delivRef2}},divL);
	if(1 || P.docEntry){
		var sea=$1.t('input',{type:'text','class':jsF,name:'cardName',value:Jr.cardName,O:{vPost:vTT},disabled:'disabled'});
	}
	else{
	var inputs='&_otD[addr][addrType(E_in)]=merch,inv';
	var sea=$Sea.input(cont,{api:'crd.c','class':jsF, inputs:inputs, vPost:'cardId='+Jr.cardId+'&cardName='+Jr.cardName, defLineText:Jr.cardName, func:function(L,inp){
			inp.O={vPost:'cardId='+L.cardId+'&cardName='+L.cardName};
			$Sea.replaceData(L,cont);
		}});
	}
	$1.T.divL({divLine:1,wxn:'wrapx1',req:'Y',L:{textNode:'Cliente'},Inode:sea},cont);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:{textNode:'Dirección Envio'},I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,k:'_addr_merch',name:'addrMerch',value:Jr.addrMerch}},cont); 
	$1.T.divL({wxn:'wrapx2',req:'Y',L:{textNode:'Dirección Facturación'},I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,k:'_addr_inv',name:'addrInv',value:Jr.addrInv}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:{textNode:'Detalles de Pedido'},I:{tag:'textarea','class':jsF,name:'lineMemoOrd',textNode:Jr.lineMemoOrd}},cont);
	$1.T.divL({wxn:'wrapx2',L:{textNode:'Detalles'},I:{tag:'textarea','class':jsF,name:'lineMemo',textNode:Jr.lineMemo}},divL);
	var resp=$1.t('div',0,cont);
	var btn=$1.T.btnSend({textNode:'Guardar'},{f:'POST '+Api.Ord.dlv+'form', getInputs:function(){ return 'docEntry='+Pa.docEntry+'&'+$1.G.inputs(cont,jsF); }, loade:resp, respDiv:resp, func:function(Jr2){
		$ps_DB.response(resp,Jr2);
		if(!Pa.docEntry && Jr2.docEntry){ $M.to('sellDlv.digit','docEntry:'+Jr2.docEntry); }
	}});
	cont.appendChild(btn);
	}});
},
digit:function(){
	cont=$M.Ht.cont; Pa=$M.read();
	$ps_DB.get({f:'GET '+Api.Ord.dlv+'digit',inputs:'docEntry='+Pa.docEntry, loade:cont, func:function(Jr){
		sHt.sellDlvFormHead(Jr,cont);
		var wList=$1.t('div',0,cont);
		if(Jr.errNo){ $ps_DB.response(wList,Jr); return false;}
		if(Jr.O && Jr.O.errNo){
			$M.U.e();
			$ps_DB.response(wList,Jr.O); return false;
		}
		//barcode
		Che.tt=Jr.tt;
		Che.Ai={};
		Che.L=[]; Che.T={}; Che.t=[];//temporal
		var Op=(Jr.O && Jr.O.L)?Jr.O.L:{};
		Che.AiT=(Jr.O && Jr.O.T)?Jr.O.T:{};
		for(var i in Op){ L=Op[i];
			var ik=L.itemId;
			if(!Che.Ai[ik]){ Che.Ai[ik]={itemId:L.itemId,itemCode:L.itemCode,itemName:L.itemName,T:{}}; }
			for(var t in L.T){
				if(!Che.Ai[ik].T[t]){ Che.Ai[ik].T[t]={o:L.T[t].o,c:0}; }
			}
		}
		Che.n = -1; $M.U.i();
		cont.appendChild(Barc.input({func:function(Jr,boxNum){
			Barc.Draw.tbDetail(Jr,boxNum,{inpChange:function(){ updPend(wrapCons); }}); updPend(wrapCons);
		}}));
		var wrapCons=$1.t('div',{id:'_tableConsol'},cont);
		$1.t('div',{id:'_tableWrap'},cont);
		updPend(wrapCons);
		var resp = $1.t('div',0,cont);
		var btn=$1.T.btnSend({textNode:'Guardar'},{loadVerif:false, f:'POST '+Api.Ord.dlv+'digit', getInputs:function(){
			var d = JSON.stringify(Barc.getData(cont));
			return 'docEntry='+Pa.docEntry+'&tt='+Jr.tt+'&tr='+Jr.tr+'&discPf='+Jr.discPf+'&D='+d;
		}, func:function(Jr2){
			if(!Jr2.errNo){ $M.U.e(); $1.delet(btn); $M.to('sellDlv.view','docEntry:'+Pa.docEntry); }
			$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btn);
	}
	});
	function updPend(cont){
		//read inputs form barcodes
		$1.clear(cont);
		var tds=$1.q('.__itemTd',cont.parentNode,'all');
		for(var i=0; i<tds.length; i++){//reset to 2
			var js=tds[i].js;
			var Djs=tds[i].D;
			var _i=js.itemId;
			var _i2=js.itemSzId;
			if(Che.Ai){
				if(!Che.Ai[_i]){ Che.Ai[_i] ={itemCode:Djs.itemCode,itemName:Djs.itemName,T:{}}; }
				if(!Che.Ai[_i].T[_i2]){ Che.Ai[_i].T[_i2] ={o:0,c:0}; }
				Che.Ai[_i].T[_i2].c = 0;
			}
			if(tds[i].noReadAndDelete=='Y'){
				if(Che.Ai[_i] && Che.Ai[_i].T[_i2]){
					if(!Che.Ai[_i].T[_i2].o){ delete(Che.AiT[_i2]); }
				}
				continue;
			}
			if(Che.AiT){
				if(!Che.AiT[_i2]){ Che.AiT[_i2]=$V.grs1[_i2]; }
			}
		}
		for(var i=0; i<tds.length; i++){//put quantity
			var js=tds[i].js;
			var _i=js.itemId;
			var _i2=js.itemSzId;
			if(tds[i].noReadAndDelete=='Y'){ $1.clear(tds[i].parentNode); continue;}
			if(!Che.AiT[_i2]){ Che.AiT[_i2]=$V.grs1[_i2]; }
			Che.Ai[_i].T[_i2].c += tds[i].value*1;
		}
		var isOrd=(Che.tt!='');
		var tb=$1.T.table(['Código','Descripción','']);
		$1.delet($1.q('#_tbPend')); tb.setAttribute('id','_tbPend');
		var fie=$1.T.fieldset(tb,{L:{textNode:'Consolidado Pendientes / Despacho'}});cont.appendChild(fie);
		var trH=$1.q('thead tr',tb);
		for(var t in Che.AiT){ $1.t('td',{textNode:Che.AiT[t]},trH); }
		$1.t('td',{textNode:'Total'},trH);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Che.Ai){ L=Che.Ai[i]; var total=total2=0;
			var itemId=L.itemId;
			var trO=$1.t('tr',0,tBody);
			var rs=(isOrd)?2:1;
			if(isOrd){ var trC=$1.t('tr',0,tBody); }
			else{ var trC=trO; }
			$1.t('td',{textNode:L.itemCode,rowspan:rs},trO);
			$1.t('td',{textNode:L.itemName,rowspan:rs},trO);
			if(isOrd){ $1.t('td',{textNode:'Pendiente'},trO); }
			$1.t('td',{textNode:'Documento'},trC);
			for(var t in Che.AiT){
				var val=val2=''; css='';
				if(L.T && L.T[t] && L.T[t].o){val=L.T[t].o*1; total+=val; }
				if(L.T && L.T[t] && L.T[t].c){val2=L.T[t].c*1; total2+=val2; }
				if(isOrd && L.T[t]){
				if(L.T[t].c && !L.T[t].o){ var css='backgroundColor:#E00;'; }
				else if(L.T[t].o<L.T[t].c){ var css='backgroundColor:#41c6f9;'; }
				else if(L.T[t].o!=L.T[t].c){ var css='backgroundColor:#EE0;'; }
				else if(L.T[t].o ==L.T[t].c){ var css='backgroundColor:#0E0;'; }
				}
				if(isOrd){ $1.t('td',{textNode:val,style:css},trO); }
				$1.t('td',{textNode:val2,style:css,kdate:itemId+'_'+t},trC);
			}
			if(isOrd){ $1.t('td',{textNode:total},trO); }
			$1.t('td',{textNode:total2},trC);
		}
		
	}
},
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Ord.dlv, inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','N°.','Estado','Fecha Doc.','Bodega','Cliente','Resp. Ventas','Realizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var menu=$1.Menu.winLiRel(sellDlv.opts({L:L},'basic')); td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('sellDlv.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$Tb.oslp[L.slpId]},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
			}
		}
	}});
},
view:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont;
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;','class':'no-print'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	divTop.appendChild($1.T.btnFa({fa:'fa_cells',textNode:' Lista de Empaque', func:function(){ $M.to('sellDlv.packing','docEntry:'+Pa.docEntry); }}));
	$ps_DB.get({f:'GET '+Api.Ord.dlv+'view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		var btn=Ivt.btnTransfer({docType:'odlv',docEntry:Pa.docEntry,whsId:Jr.whsId});
			divTop.appendChild(btn);
		sHt.sellDlvHead(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Código',style:'width:4rem;'},{textNode:'Descripción'},{textNode:'Cant.',style:'width:4rem;'},{textNode:'UdM',style:'width:3rem;'},{textNode:'Precio',style:'width:6rem;'},{textNode:'Desc.',style:'width:3rem;'},{textNode:'Total',style:'width:9rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas de Documento'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		var tFo=$1.t('tfoot',0,tb); var cs=7;
		var tr=$1.t('tr',0,tFo);
		$Str.useCurr=Jr.curr;
		$1.t('td',{textNode:'Cantidad Total',colspan:3,style:'text-align:right;'},tr);
		$1.t('td',{'class':'__tbColNumTotal1'},tr);
		$1.t('td',{textNode:'Total Lineas',colspan:cs-4,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Descuento ('+(Jr.discPf*1)+'%)',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal*Jr.discPf/100)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Total',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalVal)},tr);
		var currD=(Jr.curr!=$0s.currDefault);
		if(Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{ var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			var val=(currD)?L.priceME:L.price;
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.quantity*1,'class':'__tbColNums',tbColNum:1},tr);
			$1.t('td',{textNode:L.sellUdm},tr);
			$1.t('td',{textNode:$Str.money(val)},tr);
			$1.t('td',{textNode:(L.disc*1)+'%'},tr);
			$1.t('td',{textNode:$Str.money(val*L.quantity)},tr);
		}
		}
		$Tol.tbSum(tb);
		$Str.useCurr=false;
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
packing:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont;
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	var TA={};
	$ps_DB.get({f:'GET '+Api.Ord.dlv+'packing', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		sHt.sellDlvHead(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:2rem;'},{textNode:'Código',style:'width:4.5rem;'},'Descripción']);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lista de Empaque'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		if(Jr.errNo){ $ps_DB.response($1.t('div',0,fie),Jr.L0);  }
		else{
		var tBody=$1.t('tbody',0,tb);
		var total=0;
		var trH=$1.q('thead tr',tb);
		$1.t('td',{textNode:'Total'},trH);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.detail,style:'width:2rem;'},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.quantity*1},tr);
			total+=L.quantity*1;
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total',colspan:3},tr);
		$1.t('td',{textNode:total},tr);
		}
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
		}
	});
},
packingRol:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont;
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	var TA={}; var n=1;
	$ps_DB.get({f:'GET '+Api.Ord.dlv+'packingRol', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		for(var i in Jr.L){
			var L=Jr.L[i];
			var div=$1.t('div',{'class':'rotulWrap',style:'width:45%; margin-bottom:3rem; border:0.0625rem solid #000; padding:0.5rem; margin-left:0.5rem; float:left;'},cont);
			var tb=$1.t('table',{'class':'table_zh',style:'width:100%;'},div);
			var tBody=$1.t('tbody',0,tb);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'No. Documento'},tr);
			$1.t('td',{textNode:Jr.licTradNum},tr);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Cliente'},tr); $1.t('td',{textNode:Jr.cardName},tr);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Dirección'},tr); $1.t('td',{textNode:Jr.addrMerch},tr);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Caja: '+L.detail},tr); $1.t('td',{textNode:'Caja '+n+' de '+Jr.numBox},tr);
			//items
			n++;
			$1.t('div',{textNode:'DM: '+Jr.docEntry,style:'padding:0.25rem; font-size:0.9rem;'},div);
			var tb=$1.T.table(['Código','Descripción']); var trh=$1.q('tr',tb);
			tb.style.width='100%';
			div.appendChild(tb);
			for(var ta in L.T){ $1.t('td',{textNode:L.T[ta]},trh); }
			$1.t('td',{textNode:'Total'},trh);
			var tBody=$1.t('tbody',0,tb);
			for(var ite in L.i){ var L2=L.i[ite];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L2.itemCode},tr);
				$1.t('td',{textNode:L2.itemName},tr); var total=0;
				for(var ta in L.T){
					var val=(L2.T[ta])?L2.T[ta]*1:'';
					total+=val*1;
					$1.t('td',{textNode:val},tr);
				}
				$1.t('td',{textNode:total},tr);
			}
			//addr
				var divb=$1.t('div',{style:'margin-top:0.75rem; line-height:1.2;'},div);
				var dle=$1.t('div',{style:'float:left; width:48%;'},divb);
				$1.t('div',{textNode:$Soc.address},dle);
				$1.t('div',{textNode:$Soc.pbx},dle);
				$1.t('div',{textNode:$Soc.web+'\u00A0\u00A0\u00A0 Email: '+$Soc.mail},dle);
				var drig=$1.t('div',{style:'float:left; width:48%;'},divb);
				$1.t('img',{src:$Soc.logo,style:'float:right; width:18.75rem;'},drig);
				$1.t('div',{'class':'clear'},divb);
		}
	}
	});
}
}

var sellRdn={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	var basic=e.match(/basic,?/);
	Li[n]={ico:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('sellRdn.form','docEntry:'+T.P.docEntry); } }; n++;
	Li[n]={ico:'fa fa_history',textNode:' Recibir Devolución', P:L, func:function(T){ sellRdn.markReceive({docEntry:T.P.docEntry,odocType:'ordn'}); }
	}; n++;
	Li[n]={ico:'fa fa_history',textNode:' Clasificar Devolución', P:L, func:function(T){ $M.to('sellRdn.form2','docEntry:'+T.P.docEntry); } }; n++;
	if(basic){
	Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ _Doc.cancel({docEntry:T.P.docEntry,api:Api.Sell.rdn+'status.cancel',text:'Se va anular el documento.'}); } }; n++;
	}
	if(basic){
	Li[n]={ico:'fa fa_history',textNode:' Cerrar Documento', P:L, func:function(T){ _Doc.close({docEntry:T.P.docEntry,api:Api.Sell.rdn+'status.close',text:'Se va cerrar el documento.'}); } }; n++;
	}
	return Li={Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Sell.rdn, inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','N°.','Estado','Fecha Doc.','Bodega','Cliente','Resp. Ventas','Realizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var Li=[];
				var menu=$1.Menu.winLiRel(sellRdn.opts({L:L},'basic')); td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('sellRdn.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$V.rdnStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$V.whsCode[L.whsId]},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$Tb.oslp[L.slpId]},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; Pa=$M.read(); var n=1;
	var jsF='jsFields'; var n=1;
	$ps_DB.get({f:'GET '+Api.Sell.rdn+'form', loadVerif:!Pa.docEntry, inputs:'docEntry='+Pa.docEntry, func:function(Jr){
	if(Jr.errNo){ $ps_DB.response(cont,Jr); }
	var tb=$1.T.table([{textNode:'#',style:'width:2.5rem;'},'Código','Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Motivo',style:'width:8rem;'}]);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	Drw.itemReader({cont:fie,tBody:tBody, noSelBy:'iss',fields:'I.itemId&wh[I.sellItem]=Y'},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{jsF:jsF,JrS:P.JrS, n:n,noFields:{price:'N',priceList:'N'}, func:function(P,tBody){ trA(P); } });
	}});
	_Doc.formSerie({cont:cont, serieType:'ordn',jsF:jsF, middleCont:fie, docEntry:Pa.docEntry,
	api:'PUT '+Api.Sell.rdn+'form', func:function(Jr2){
		$M.to('sellRdn.view','docEntry:'+Jr2.docEntry);
	},
	Li:[
	{fType:'date',req:'Y',name:'docDate',value:Jr.docDate},
	{fType:'crd',wxn:'wrapx3',L:'Socio de Negocios',req:'Y',cardId:Jr.cardId,cardName:Jr.cardName, replaceData:'Y',inputs:'fields=A.slpId'},
	{wxn:'wrapx8',L:'Resp. de Ventas',req:'Y',I:{tag:'select',sel:{'class':jsF+' '+$Sea.clsName,name:'slpId',k:'slpId'},opts:$Tb.oslp,selected:Jr.slpId}},
	{fType:'user'},
	{divLine:1,wxn:'wrapx2',req:'Y',L:'Acción Propuesta',I:{tag:'textarea','class':jsF,name:'doAction',placeholder:'Dar garantia por mal despacho...',textNode:Jr.doAction}},
	{wxn:'wrapx2',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo',textNode:Jr.lineMemo,disabled:'disabled'}}
	]
	});
	
	if(Jr.L){
		$js.sortNum(Jr.L,{k:'lineNum'}); 
		for(var i in Jr.L){ n=Jr.L[i].lineNum; trA(Jr.L[i],n); }
	}
	function trA(Pr){
		var P2=(Pr.JrS)?Pr.JrS:Pr;
		var Ta=(Pr.Ds)?Pr.Ds.T:{};
		if(P2.itemSzId){ Ta[P2.itemSzId]=P2.quantity; }
		for(var ta in Ta){
			var ln='L['+n+']';
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:P2.itemCode},tr);
			$1.t('td',{textNode:P2.itemName+'  T: '+ta},tr);
			$1.t('td',{textNode:P2.sellUdm},tr);
			var qua=(Ta[ta])?Ta[ta]:1;
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',value:qua,'class':jsF,name:ln+'[quantity]',style:'width:5rem;',O:{vPost:ln+'[itemId]='+P2.itemId+'&'+ln+'[itemSzId]='+ta}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text',value:((P2.reason)?P2.reason:''),'class':jsF,name:ln+'[reason]',style:'width:10rem;'},td);
		}
	}
	}});
},
view:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont; $1.clear(contPa);
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	$ps_DB.get({f:'GET '+Api.Sell.rdn+'view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.docStatus=='D'){
			var btnSend=$1.T.btnFa({fa:'fa_listWin',textNode:' Recibir Devolución', func:function(){ sellRdn.markReceive({docEntry:Jr.docEntry,odocType:'ordn'}); }});
			divTop.appendChild(btnSend);
		}
		sHt.sellRdnHead(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Código',style:'width:6rem;'},'Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Motivo',style:'width:8rem;'},{textNode:'Ref. 1'},{textNode:'Clasificación',style:'width:6rem;'},{textNode:'Garantía'},{textNode:'N°. Guia',style:'width:4rem;'},{textNode:'Despachado',style:'width:4rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Pedido'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.udm},tr);
			$1.t('td',{textNode:L.quantity},tr);
			$1.t('td',{textNode:L.reason},tr);
			$1.t('td',{textNode:L.ref1},tr);
			$1.t('td',{textNode:$V.rdnClasif[L.ref2]},tr);
			$1.t('td',{textNode:$V.rdnWarranty[L.ref3]},tr);
			$1.t('td',{textNode:L.delivRef},tr);
			$1.t('td',{textNode:L.delivDate},tr);
		}
		}
		$Str.useCurr=false;
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
form2:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; Pa=$M.read();
	var jsF='jsFields'; var n=1;
	$ps_DB.get({f:'GET '+Api.Sell.rdn+'form2', loadVerif:!Pa.docEntry, inputs:'docEntry='+Pa.docEntry, func:function(Jr){
	if(Jr.errNo){ $ps_DB.response(cont,Jr); }
	var tb=$1.T.table([{textNode:'#',style:'width:2.5rem;'},'Código','Descripción',{textNode:'UdM',style:'width:3rem;'},{textNode:'Cant.',style:'width:6rem;'},{textNode:'Motivo',style:'width:8rem;'},{textNode:'Clasificación',style:'width:4rem;'},{textNode:'Garantia',style:'width:4rem;'},{textNode:'Ref. 1',style:'width:4rem;'},{textNode:'Ref. 2 (Desp.)',style:'width:4rem;'},{textNode:'Fecha. Desp.',style:'width:4rem;'}]);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
	cont.appendChild(fie);
	var tBody=$1.t('tbody',0,tb);
	
	_Doc.formSerie({cont:cont, serieType:'ordn',jsF:jsF, middleCont:fie, docEntry:Pa.docEntry,
	api:'PUT '+Api.Sell.rdn+'form2', func:function(Jr2){
	},
	Li:[
	{wxn:'wrapx10',L:'Fecha',I:{tag:'input',type:'text',value:Jr.docDate,disabled:'disabled'}},
	{wxn:'wrapx4',L:'Socio Negocios',I:{tag:'input',type:'text',value:Jr.cardName,disabled:'disabled'}},
	{wxn:'wrapx6',L:{textNode:'Empleado Ventas'},I:{tag:'input',type:'text',value:$Tb.oslp[Jr.slpId],disabled:'disabled'}},
	{divLine:1,wxn:'wrapx2',L:'Acción Propuesta',I:{tag:'textarea',disabled:'disabled',placeholder:'Dar garantia por mal despacho...',textNode:Jr.doAction}},
	{wxn:'wrapx2',L:'Detalles',I:{tag:'textarea',textNode:Jr.lineMemo,name:'lineMemo','class':jsF}}
	]
	});
	var n=1;
	if(Jr.L){
		$js.sortNum(Jr.L,{k:'lineNum'}); 
		for(var i in Jr.L){ n=Jr.L[i].lineNum; trA(Jr.L[i]); }
	}
	function trA(Pr){
		var P2=(Pr.JrS)?Pr.JrS:Pr;
		var ln='L['+n+']';
		var Ta=(Pr.Ds)?Pr.Ds.T:{};
		if(P2.itemSzId){ Ta[P2.itemSzId]=P2.quantity; }
		for(var ta in Ta){
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:Itm.Txt.code(P2)},tr);
			$1.t('td',{textNode:Itm.Txt.name(P2)},tr);
			$1.t('td',{textNode:P2.udm},tr);
			var td=$1.t('td',{textNode:P2.quantity},tr);
			var vPost =ln+'[itemId]='+P2.itemId+'&'+ln+'[itemSzId]='+ta;
			var td=$1.t('td',{textNode:P2.reason},tr);
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.sel({sel:{'class':jsF,name:ln+'[ref2]'},opts:$V.rdnClasif,selected:P2.ref2}));
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.sel({sel:{'class':jsF,name:ln+'[ref3]'},opts:$V.rdnWarranty,selected:P2.ref3}));
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[ref1]',value:P2.ref1,style:'width:8rem;'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[delivRef]',value:P2.delivRef,style:'width:8rem;'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'date','class':jsF,name:ln+'[delivDate]',value:P2.delivDate,style:'width:8rem;'},td);
		}
	}
	}});
},
}

sellRdn.markReceive=function(P){
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:{textNode:'Fecha Recibido'},I:{tag:'input',type:'date','class':'jsFields',name:'dateGet'}});
	$1.T.divL({wxn:'wrapx2',subText:'Defina a que Bodega ingresa la devolución.',L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':'jsFields',name:'whsId',O:{vPost:'docEntry='+P.docEntry}},opts:$V.whsCode}},divL);
	$1.Win.confirm({text:'El documento será marcado como recibido, se cargarán las cantidades en la Bodega y no se podrá modificar la información.', noClose:1, Inode:divL, func:function(resp,wrapC){
		$ps_DB.get({f:'PUT '+Api.Sell.rdn+'status.receive',inputs:$1.G.inputs(wrapC), loade:resp, func:function(Jr){
			$ps_DB.response(resp,Jr);
		}});
	}});
}

var buyOrd={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	var basic=e.match(/basic,?/);
	if(basic || e.match(/modify,?/)){
		Li[n]={ico:'fa fa_pencil',textNode:' Modificar Orden', P:L, func:function(T){ $M.to('buyOrd.form','docEntry:'+T.P.docEntry); } }; n++;
	}
	if(e.match(/receive,?/)){
		Li[n]={ico:'fa fa_listWin',textNode:' Recibir Mercancía', func:function(){ buyOrd.markReceive({docEntry:L.docEntry,odocType:'opor'}); }}; n++;
	}
	if(basic){
		Li[n]={ico:'fa fa_info',textNode:' Información de Documento', P:L, func:function(T){ buyOrd.info(T.P); } }; n++;
	}
	if(basic){
		Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ _Doc.cancel({docEntry:T.P.docEntry,api:Api.buyOrd.a+'status.cancel',text:'Se va anular el documento, no se puede reversar está acción.'}); } }; n++;
	}
	if(basic){
		Li[n]={ico:'fa fa_comment',textNode:'Comentarios',L:L,func:function(T){ $5c.form({tt:'opor',tr:L.docEntry, getList:'Y',winTitle:'Orden de Compra: '+L.docEntry}); } }; n++;
		Li[n]={ico:'fa fa_attach',textNode:'Archivos',L:L,func:function(T){ $5fi.btnOnTb({tt:'opor',tr:L.docEntry, getList:'Y',winTitle:'Orden de Compra: '+L.docEntry}); } }; n++;
	}
	return Li={Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.buyOrd.a, inputs:$1.G.filter(), loade:cont, func:function(Jr){ 
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','N°','Estado','Fecha Doc.','Proveedor','Fecha Entrega','Valor','Realizado','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var menu=$1.Menu.winLiRel(buyOrd.opts({L:L},'basic,receive'));
				td.appendChild(menu);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.to('buyOrd.view','docEntry:'+L.docEntry,'r'),'class':'fa fa_eye',textNode:' '+L.docEntry},td);
				$1.t('td',{textNode:$V.ordStatus[L.docStatus]},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
				$1.t('td',{textNode:$Str.money({curr:L.curr,value:L.priceVal})},tr);
				$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var td=$1.t('td',0,tr); 
				$1.T.btnFa({fa:'fa_comment',L:L,func:function(T){ $5c.form({tt:'opor',tr:T.L.docEntry, getList:'Y',winTitle:'Orden de Compra: '+T.L.docEntry}); } },td); 
				$1.T.btnFa({fa:'fa_attach',L:L,func:function(T){ $5fi.btnOnTb({tt:'opor',tr:T.L.docEntry, getList:'Y',winTitle:'Orden de Compra: '+T.L.docEntry}); } },td); 
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; var jsF='jsFields'; var Pa=$M.read();
	$ps_DB.get({file:'GET '+Api.buyOrd.a+'form', loadVerif:!Pa.docEntry, loade:cont, inputs:'docEntry='+Pa.docEntry, func:function(Jr){
		_Frm.n.i=1;//reset
		{//fie table
			var tb=$1.T.table([{textNode:'#',style:'width:2rem;'},'Código',{textNode:'Descripción',style:'width:22rem;'},'Cant.','UdM','Precio',{textNode:'Total',style:'width:8rem;'},{textNode:''}]);
		tb.classList.add('table_x100');
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
		cont.appendChild(fie);
		var tBody= $1.t('tbody',0,tb);
		var tFo= $1.t('tfoot',0,tb);
		var n=1;
		var trF=$1.t('tr',0,tFo);
		var td=$1.t('td',{colspan:5,textNode:'Moneda: ',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2},trF);
		var sel=$1.T.sel({sel:{'class':jsF+' __tbCurr',name:'curr'},opts:$0s.Curr,noBlank:1,selected:Jr.curr}); td.appendChild(sel);
		sel.onchange=function(){ $Tol.tbSum(cont); }
		var trF=$1.t('tr',0,tFo);
		$1.t('td',{colspan:5,textNode:'Total Lineas',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2,'class':'__tbTotal',vformat:'money',style:'width:4rem;'},trF);
		var trF=$1.t('tr',0,tFo);
		var disc=$1.t('td',{colspan:5,textNode:'Descuento: ',style:'text-align:right;'},trF);
		var inpd=$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem;','class':jsF+' __tbDisc '+$Sea.clsName,k:'discPf',name:'discPf',value:(Jr.discPf*1)},disc);
		inpd.onkeyup=function(){ $Tol.tbSum(tb); }
		inpd.onchange=function(){ $Tol.tbSum(tb); }
		var td=$1.t('td',{colspan:2,'class':'__tbDiscText',vformat:'money',style:'width:4rem;'},trF);
		var trF=$1.t('tr',0,tFo);
		var disc=$1.t('td',{colspan:5,textNode:'Total',style:'text-align:right;'},trF);
		var td=$1.t('td',{colspan:2,'class':'__tbTotal2',vformat:'money',style:'width:4rem;'},trF);
		Drw.itemReader({cont:fie,tBody:tBody,fields:'I.buyPrice,I.buyUdm&wh[I.buyItem]=Y'},{func:function(tBody,P){
		Drw.docLineItemSz(tBody,{trLine:'tr',jsF:jsF,JrS:P.JrS, n:n,formType:'buy', func:function(P,tBody){
				P.formType='compra';
				_Frm.itm(P,tBody);
			}}); n++;
	}});
		}
		_Doc.formSerie({cont:cont,serieType:'opor', docEntry:Pa.docEntry, jsF:jsF,
		api:'PUT '+Api.buyOrd.a+'form', func:function(Jr2){
			$M.to('buyOrd.view','docEntry:'+Jr2.docEntry);
		},
		middleCont:fie,
		Li:[
		{fType:'crd',wxn:'wrapx4',L:'Proveedor',req:'Y',api:'crd.s', replaceData:'Y', inputs:'fields=A.discPf', cardId:Jr.cardId, cardName:Jr.cardName},
		{fType:'user'},
		{wxn:'wrapx8',L:'Estado',req:'Y',I:{tag:'select',sel:{disabled:'disabled',title:Jr.docStatus},opts:$V.ordStatus,selected:Jr.docStatus,noBlank:1}},
		{fType:'date',name:'docDate',req:'Y',value:Jr.docDate},
		{fType:'date',name:'dueDate',req:'Y',value:Jr.dueDate,textNode:'Fecha Entrega'},
		{divLine:1,wxn:'wrapx4',req:'Y',L:'Dirección para mercancías',I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'addrMerch',k:'_addr_merch',value:Jr.addrMerch}},
		{wxn:'wrapx4',req:'Y',L:'Dirección para facturación',I:{tag:'input',type:'text','class':jsF+' '+$Sea.clsName,name:'addrInv',k:'_addr_inv',value:Jr.addrInv}},
		{divLine:1,wxn:'wrapx1',L:{textNode:'Detalles/Observaciones'},I:{tag:'textarea','class':jsF,name:'lineMemo',textNode:Jr.lineMemo}}
		]
		});
		if(Jr.L && !Jr.L.errNo){
			Jr.jsF=jsF;  Jr.formType='compra';
			_Frm.itm(Jr,tBody);
		}
	}
	});
},
view:function(){
	var Pa=$M.read(); var contPa=$M.Ht.cont; $1.clear(contPa);
	var divTop=$1.t('div',{style:'marginBottom:0.5rem;'},contPa);
	var cont=$1.t('div',0,contPa);
	var btnPrint=$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont); }});
	divTop.appendChild(btnPrint);
	$ps_DB.get({f:'GET '+Api.buyOrd.a+'view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); return true;}
		divTop.appendChild($1.Menu.winLiRel(buyOrd.opts({L:Jr,textNode:'Opciones'},'basic')));
		if(Jr.docStatus=='D'){
			var btnSend=$1.T.btnFa({fa:'fa_listWin',textNode:' Recibir Mercancía', func:function(){ buyOrd.markReceive({docEntry:Jr.docEntry,odocType:'opor'}); }});
			divTop.appendChild(btnSend);
		}
		sHt.BuyOrd(Jr,cont);
		var tb=$1.T.table([{textNode:'#',style:'width:3rem;'},{textNode:'Código',style:'width:6rem;'},{textNode:'Descripción'},{textNode:'UdM',style:'width:3rem;'},{textNode:'Precio',style:'width:6rem;'},{textNode:'Cant.',style:'width:5rem;'},{textNode:'Total',style:'width:8rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Pedido'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		var tFo=$1.t('tfoot',0,tb); var cs=5;
		var tr=$1.t('tr',0,tFo);
		$Str.useCurr=Jr.curr;
		$1.t('td',{textNode:'Total Lineas',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Descuento ('+(Jr.discPf*1)+'%)',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalLineVal*Jr.discPf/100)},tr);
		var tr=$1.t('tr',0,tFo);
		$1.t('td',{textNode:'Total',colspan:cs,style:'text-align:right;'},tr);
		$1.t('td',{textNode:$Str.money(Jr.totalVal)},tr);
		var currD=(Jr.curr!=$0s.currDefault);
		if(Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody); L.quantity *=1;
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			var val=(currD)?L.priceME:L.price;
			$1.t('td',{textNode:L.buyUdm},tr);
			$1.t('td',{textNode:$Str.money(val)},tr);
			$1.t('td',{textNode:L.quantity},tr);
			$1.t('td',{textNode:$Str.money(val*L.quantity)},tr);
		}
		}
		$Str.useCurr=false;
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
info:function(P){
	var wrap=$1.t('div');
	$ps_DB.get({f:'GET '+Api.buyOrd.a+'info', inputs:'docEntry='+P.docEntry,loaderFull:1, loade:wrap, func:function(Jr){
		$1.Tb.trFieVal({L:$TbExp.rep(Jr,'gvt_opvt')});
	}});
}
}
buyOrd.markReceive=function(P){
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:{textNode:'Fecha Recibido'},I:{tag:'input',type:'date','class':'jsFields',name:'dateGet'}});
	$1.T.divL({wxn:'wrapx2',subText:'Defina a que Bodega ingresa la devolución.',L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':'jsFields',name:'whsId',O:{vPost:'docEntry='+P.docEntry}},opts:$V.whsCode,selected:P.whsId}},divL);
	$1.Win.confirm({text:'El documento será marcado como recibido, se cargarán las cantidades en la Bodega y no se podrá modificar la información.', noClose:1, Inode:divL, func:function(resp,wrapC){
		$ps_DB.get({f:'PUT '+Api.buyOrd.a+'status.receive',inputs:$1.G.inputs(wrapC), loade:resp, func:function(Jr){
			$ps_DB.response(resp,Jr);
		}});
	}});
}

var Wma={};

Wma.Wop={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'wop', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Tipo','Nombre','Descripción']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wop.form','wopId:'+T.P.wopId); }},td);
				$1.t('td',{textNode:L.wopCode},tr);
				$1.t('td',{textNode:$V.wopType[L.wopType]},tr);
				$1.t('td',{textNode:L.wopName},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'wop.formData', loadVerif:!Pa.wopId, inputs:'wopId='+Pa.wopId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wopId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wopCode',value:Jr.wopCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wopName',value:Jr.wopName}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'wopType'},opts:$V.wopType,noBlank:1,selected:Jr.wopType}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wopId',cont);
		wId.O={vPost:'wopId='+Jr.wopId};
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'wop.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.wopId){ wId.O={vPost:'wopId='+Jr2.wopId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
}
}
Wma.Wfa={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'wfa', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Descripción']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wfa.form','wfaId:'+T.P.wfaId); }},td);
				$1.t('td',{textNode:L.wfaCode},tr);
				$1.t('td',{textNode:L.wfaName},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'wfa.formData', loadVerif:!Pa.wfaId, inputs:'wfaId='+Pa.wfaId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wfaId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wfaCode',value:Jr.wfaCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wfaName',value:Jr.wfaName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wfaId',cont);
		wId.O={vPost:'wfaId='+Jr.wfaId};
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'wfa.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.wfaId){ wId.O={vPost:'wfaId='+Jr2.wfaId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
}
}

Wma.Mpg={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Tiempo','Costo','Costo Operaciones','Descripción','','Actualizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.mpg.form','mpgId:'+T.P.mpgId); }},td);
				$1.t('td',{textNode:L.mpgCode},tr);
				$1.t('td',{textNode:L.mpgName},tr);
				$1.t('td',{textNode:L.faseTime},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costFaseF)},tr);
				$1.t('td',{textNode:L.descrip},tr);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_arrowBack',textNode:' Asignar A', P:L, func:function(T){ Wma.Mpg.formItm(T.P); }},td);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.formData', loadVerif:!Pa.mpgId, inputs:'mpgId='+Pa.mpgId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'mpgCode',value:Jr.mpgCode}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF+' __mpgId',name:'mpgName',value:Jr.mpgName}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF,name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Udm Tiempo',I:{tag:'select',sel:{'class':jsF,name:'faseUdm'},opts:$V.faseUdm,noBlank:1,selected:Jr.faseUdm}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__mpgId',cont);
		wId.O={vPost:'mpgId='+Jr.mpgId};
		var winM=$1.Menu.inLine([
		{textNode:'Costos','class':'active fa fa_dollar',winClass:'wfaCost'},
		{textNode:'Operaciones','class':'fa fa_priv_members',winClass:'wfaOper'}
		],{winCont:1}); cont.appendChild(winM);
		var wfaOper=$1.t('div',{'class':'winMenuInLine wfaOper',style:'display:none'},winM);
		var wfaCost=$1.t('div',{'class':'winMenuInLine wfaCost'},winM);
		$1.t('p',{textNode:'Utilice un costo manual para la mano de Obra sino desea definir las operaciones y otros costos.'},wfaCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'¿Costo Manual?',I:{tag:'select',sel:{'class':jsF,name:'isManual'},selected:Jr.isManual,noBlank:1,opts:$V.NY}},wfaCost);
		$1.T.divL({wxn:'wrapx8',L:'Costo Manual',I:{tag:'input',type:'text','class':jsF,name:'costManual',value:Jr.costManual,numberformat:'mil'}},divL);
		$1.t('p',{textNode:'Defina el Costo Fijo. Este se sumará a los otros costos, para obtener el costo total.'},wfaCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Costo Fijo',I:{tag:'input',type:'text','class':jsF,name:'costF',value:Jr.costF,numberformat:'mil'}},wfaCost);
		var tb=$1.T.table(['#','Operación','Tiempo UdM','Coste','Comentarios','Tercero','']);
		if(Jr.WFA1 && Jr.WFA1.errNo){ $ps_DB.response(wfaOper,Jr.WFA1); }
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		btnAdd=$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }}); td.appendChild(btnAdd);
		var trF=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:2},trF);
		$1.t('td',{'class':'__tbTotal_col1'},trF);
		$1.t('td',{'class':'__tbTotal_col2',vformat:'money'},trF);
		$1.t('td',{colspan:3},trF);
		var ni=1;
		function trOpet(tBody,L){
			var ln='_WFOP['+ni+']';
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',{textNode:ni},tr);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wopId]'},opts:$Tb.owop,selected:L.wopId});
			td.appendChild(sel);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum',name:ln+'[teoricTime]',min:0,value:L.teoricTime,style:'width:4rem;', onkeychange:function(){ $Tol.tbSum(tb);} },td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum2',name:ln+'[cost]',min:0,value:L.cost,style:'width:4rem;',numberformat:'mil',onkeychange:function(){ $Tol.tbSum(tb);}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			$Sea.input(td,{api:'crd.all','class':jsF, vPost:ln+'[cardId]='+L.cardId, defLineText:L.cardName, fPars:{ln:ln}, func:function(L,inp,fPars){ inp.O={vPost:fPars.ln+'[cardId]='+L.cardId};
			}});
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}}));
		}
		for(var i in Jr.WFA1){ ni=Jr.WFA1[i].lineNum;
			trOpet(tBody,Jr.WFA1[i]); ni++; }
		for(var i=1; i<=5; i++){ trOpet(tBody,{}); ni++; }
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'mpg.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.mpgId){ wId.O={vPost:'mpgId='+Jr2.mpgId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
},
formItm:function(P){
	var cont=$1.t('div'); var ide='WmaMpgFormItm_wrap'; $1.delet(ide);
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.formItm', loadVerif:!P.mpgId, inputs:'mpgId='+P.mpgId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		$Sea.input(cont,{'class':'divLine',api:'itemData',inputs:'wh[I.itemType]=P',placeholder:'Nombre o código de artículo...',func:function(Jq){
				trA(tBody,Jq); n++;
			}});
			var tb=$1.T.table(['Código','Descripción','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Relacionar a Artículos'},{f:'PUT '+Api.Wma.a+'mpg.formItm',loade:resp, getInputs:function(){ return 'mpgId='+P.mpgId+'&'+$1.G.inputs(cont); }, func:function(Jr2){
					$ps_DB.response(resp,Jr2);
				}}); cont.appendChild(btn);
			var n=1;
			for(var i in Jr.L){ trA(tBody,Jr.L[i]); n++; }
		function trA(tBody,L){
			var ln='L['+n+']'; 
			var tr=$1.t('tr',0,tBody);
			var css=(L.dateUpd!=L.moDateUpd)?'color:#F00; font-weight:bold;':''
			$1.t('td',{textNode:L.itemCode,style:css},tr);
			$1.t('td',{textNode:L.itemName,style:css},tr);
			$1.t('td',{textNode:$2d.f(L.moDateUpd,'mmm d H:iam'),style:css},tr);
			var td=$1.t('td',0,tr);
			var ckL=$1.T.ckLabel({t:'Quitar',I:{'class':'jsFields',name:ln+'[delete]',O:{vPost:ln+'[mpgId]='+P.mpgId+'&'+ln+'[itemId]='+L.itemId}}},td);
		}
	}});
	$1.Win.open(cont,{winTitle:'Asignar Mano de Obra A',winSize:'medium',onBody:1,winId:ide});
},
view:function(P){
	var wrapTop=$1.t('div'); var wrap=$1.t('div');
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.view',loade:wrapTop, inputs:'itemId='+P.itemId, func:function(Jr){
		wrapTop.appendChild($1.Win.print(wrap,{btn:{textNode:'Imprimir'}}));
		wrapTop.appendChild(wrap);
		if(Jr.errNo){ return $ps_DB.response(wrap,Jr); }
		var h5Text=(P.h5Text)?P.h5Text:Jr.mpgName;
		$1.t('h5',{textNode:h5Text},wrap);
		var tb=$1.T.table(['#','Operación','Tiempo','Costo','Tercero','Detalles']); wrap.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:L.wopName},tr);
			$1.t('td',{textNode:L.teoricTime*1},tr);
			$1.t('td',{textNode:$Str.money(L.cost)},tr);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		var tF=$1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total Operaciones',colspan:2},tr);
		$1.t('td',{textNode:Jr.faseTime*1},tr);
		$1.t('td',{textNode:$Str.money(Jr.costFaseF)},tr); $1.t('td',{colspan:2,rowspan:3},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Costo Fijo',colspan:3},tr);
		$1.t('td',{textNode:$Str.money(Jr.costF)},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:3},tr);
		var td=$1.t('td',{textNode:$Str.money(Jr.cost)},tr);
		if(Jr.isManual=='Y'){ td.appendChild($1.t('textNode',' (Manual)')); }
		$1.t('p',{textNode:'Para modificar está información, debe ir al maestro de mano de Obra.'},wrapTop);
	}})
	$1.Win.open(wrapTop,{winTitle:'Mano de Obra',onBody:1,winSize:'medium'});
}
}

Wma.CIF={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Costo','Costo Fases','Descripción','','Actualizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.cif.form','cifId:'+T.P.cifId); }},td);
				$1.t('td',{textNode:L.cifCode},tr);
				$1.t('td',{textNode:L.cifName},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costFaseF)},tr);
				$1.t('td',{textNode:L.descrip},tr);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_arrowBack',textNode:' Asignar A', P:L, func:function(T){ Wma.CIF.formItm(T.P); }},td);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif.formData', loadVerif:!Pa.cifId, inputs:'cifId='+Pa.cifId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'cifCode',value:Jr.cifCode}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF+' __cifId',name:'cifName',value:Jr.cifName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__cifId',cont);
		wId.O={vPost:'cifId='+Jr.cifId};
		var winM=$1.Menu.inLine([
		{textNode:'Costos','class':'active fa fa_dollar',winClass:'cifCost'},
		{textNode:'Fases','class':'fa fa_priv_members',winClass:'cifFase'}
		],{winCont:1}); cont.appendChild(winM);
		var wfaOper=$1.t('div',{'class':'winMenuInLine cifFase',style:'display:none'},winM);
		var cifCost=$1.t('div',{'class':'winMenuInLine cifCost'},winM);
		$1.t('p',{textNode:'Defina el CIF manualmente, sino desea determinarlo por fases.'},cifCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'¿CIF Manual?',I:{tag:'select',sel:{'class':jsF,name:'isManual'},selected:Jr.isManual,noBlank:1,opts:$V.NY}},cifCost);
		$1.T.divL({wxn:'wrapx8',L:'Valor Manual',I:{tag:'input',type:'text','class':jsF,name:'costManual',value:Jr.costManual,numberformat:'mil'}},divL);
		$1.t('p',{textNode:'Defina el Costo Fijo. Este se sumará a los otros costos, para obtener el costo total.'},cifCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Costo Fijo',I:{tag:'input',type:'text','class':jsF,name:'costF',value:Jr.costF,numberformat:'mil'}},cifCost);
		var tb=$1.T.table(['#','Fase','Costo','Comentarios','Tercero','']);
		if(Jr.LN && Jr.LN.errNo){ $ps_DB.response(wfaOper,Jr.LN); }
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		btnAdd=$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }}); td.appendChild(btnAdd);
		var trF=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:2},trF);
		$1.t('td',{'class':'__tbTotal_col1'},trF);
		$1.t('td',{'class':'__tbTotal_col2',vformat:'money'},trF);
		$1.t('td',{colspan:3},trF);
		var ni=1;
		function trOpet(tBody,L){
			var ln='_WFA['+ni+']';
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',{textNode:ni},tr);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId});
			td.appendChild(sel);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum2',name:ln+'[cost]',min:0,value:L.cost,style:'width:4rem;',numberformat:'mil',onkeychange:function(){ $Tol.tbSum(tb);}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			$Sea.input(td,{api:'crd.all','class':jsF, vPost:ln+'[cardId]='+L.cardId, defLineText:L.cardName, fPars:{ln:ln}, func:function(L,inp,fPars){ inp.O={vPost:fPars.ln+'[cardId]='+L.cardId};
			}});
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}}));
		}
		for(var i in Jr.LN){ ni=Jr.LN[i].lineNum;
			trOpet(tBody,Jr.LN[i]); ni++; }
		for(var i=1; i<=5; i++){ trOpet(tBody,{}); ni++; }
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'cif.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.cifId){ wId.O={vPost:'cifId='+Jr2.cifId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
},
formItm:function(P){
	var cont=$1.t('div'); var ide='WmaMpgFormItm_wrap'; $1.delet(ide);
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif.formItm', loadVerif:!P.cifId, inputs:'cifId='+P.cifId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		$Sea.input(cont,{'class':'divLine',api:'itemData',inputs:'wh[I.itemType]=P',placeholder:'Nombre o código de artículo...',func:function(Jq){
				trA(tBody,Jq); n++;
			}});
			var tb=$1.T.table(['Código','Descripción','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Relacionar a Artículos'},{f:'PUT '+Api.Wma.a+'cif.formItm',loade:resp, getInputs:function(){ return 'cifId='+P.cifId+'&'+$1.G.inputs(cont); }, func:function(Jr2){
					$ps_DB.response(resp,Jr2);
				}}); cont.appendChild(btn);
			var n=1;
			for(var i in Jr.L){ trA(tBody,Jr.L[i]); n++; }
		function trA(tBody,L){
			var ln='L['+n+']'; 
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var td=$1.t('td',0,tr);
			var ckL=$1.T.ckLabel({t:'Quitar',I:{'class':'jsFields',name:ln+'[delete]',O:{vPost:ln+'[cifId]='+P.cifId+'&'+ln+'[itemId]='+L.itemId}}},td);
		}
	}});
	$1.Win.open(cont,{winTitle:'Asignar CIF A',winSize:'medium',onBody:1,winId:ide});
},
}

Wma.fichaProd=function(){
	var wrap=$1.t('div');
	{
	var tb=$1.t('table',{'class':'table_zh'});
	}
}

Wma.Cost={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Descripción','Precio Venta','Costo','MP','MO','CIF','Tiempo','Actualizado']);
			var tBody=$1.t('tbody',0,tb);
			Jr.L=$js.sortNum(Jr.L,{k:'itemCode'});
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.cost.defineForm','itemId:'+T.P.itemId+',itemSzId:'+T.P.itemSzId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName+'  T: '+$V.grs1[L.itemSzId]},tr);
				$1.t('td',{textNode:$Str.money(L.sellPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				var mpCost=((L.mpManual=='Y')?L.mpCostManual:L.mpCost);
				var moCost=((L.moManual=='Y')?L.moCostManual:L.moCost);
				var cif=((L.cifManual=='Y')?L.cifCostManual:L.cif);
				var other=((L.otherManual=='Y')?L.otherCostManual:L.other);
				$1.t('td',{textNode:$Str.money(mpCost),xls:{t:mpCost}},tr);
				$1.t('td',{textNode:$Str.money(moCost),xls:{t:moCost}},tr);
				$1.t('td',{textNode:$Str.money(cif),xls:{t:cif}},tr);
				//$1.t('td',{textNode:$Str.money(other),xls:{t:other}},tr);
				$1.t('td',{textNode:(L.faseTime*1)+' '+$V.faseUdm[L.faseUdm]},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
			tb=$1.T.tbExport(tb,{print:1,fileName:'Resultado Costo de Produccion de Articulos'});
			cont.appendChild(tb);
		}
	}});
},
defineForm:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	var vPost='itemId='+Pa.itemId+'&itemSzId='+Pa.itemSzId;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost.define', loade:cont, inputs:vPost, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			$M.Ht.title.innerHTML= Jr.itemCode+') '+Jr.itemName+'  T: '+$V.grs1[Jr.itemSzId];
			var tb=$1.T.table(['','Manual','Valor Manual','Matriz']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Materia Prima'},tr);
			var td=$1.t('td',0,tr);
			var ma0=$1.T.sel({sel:{'class':jsF,name:'mpManual'},opts:$V.NY,noBlank:1,selected:Jr.mpManual});
			td.appendChild(ma0);
			var td=$1.t('td',0,tr);
			var ma1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'mpCostManual',value:Jr.mpCostManual},td);
			var td=$1.t('td',0,tr);
			var ma2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'mpCost',value:Jr.mpCost,disabled:'disabled'},td);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Mano de Obra'},tr);
			var td=$1.t('td',0,tr);
			var mo0=$1.T.sel({sel:{'class':jsF,name:'moManual'},opts:$V.NY,noBlank:1,selected:Jr.moManual});
			td.appendChild(mo0);
			var td=$1.t('td',0,tr);
			var mo1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'moCostManual',value:Jr.moCostManual},td);
			var td=$1.t('td',0,tr);
			var mo2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'moCost',value:Jr.moCost,disabled:'disabled'},td);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'CIF'},tr);
			var td=$1.t('td',0,tr);
			var cif0=$1.T.sel({sel:{'class':jsF,name:'cifManual'},opts:$V.NY,noBlank:1,selected:Jr.cifManual});
			td.appendChild(cif0);
			var td=$1.t('td',0,tr);
			var mo1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'cifCostManual',value:Jr.cifCostManual},td);
			var td=$1.t('td',0,tr);
			var mo2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'cif',value:Jr.cif,disabled:'disabled'},td);
			var tr=$1.t('tr');
			$1.t('td',{textNode:'Otros Costos'},tr);
			var td=$1.t('td',0,tr);
			var oth0=$1.T.sel({sel:{'class':jsF,name:'otherManual'},opts:$V.NY,noBlank:1,selected:Jr.otherManual});
			td.appendChild(oth0);
			var td=$1.t('td',0,tr);
			var oth1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'otherCostManual',value:Jr.otherCostManual},td);
			var td=$1.t('td',0,tr);
			var oth2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'other',value:Jr.other,disabled:'disabled'},td);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Definir Costos'},{f:'PUT '+Api.Wma.a+'cost.define', getInputs:function(){ return vPost+'&'+$1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
				$ps_DB.response(resp,Jr2);
			}}); cont.appendChild(btn);
			$1.t('p',{'class':'input_ok fa fa_info',textNode:'Si realiza actualizaciones desde alguna de las matrices de datos, pero el artículo tiene definido el costo manualmente (por ejemplo para la Mano de Obra) se calculará con base al costo manual y no con el de la matriz al ejecutar la actualización desde el Log de Modificaciones.'},cont);
		}
	}});
}
}
Wma.Cost.Log2={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost.log2', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['Ejecución','Tipo','Código','Descripción','Solicitado','Ejecutado Por']);
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var tExec=(L.isExec=='Y')?'Por '+$Tb.ousr[L.userExec]+', el '+$2d.f(L.dateExec,'mmm d H:iam'):'Pendiente';
				if(L.isExec=='Y'){
					$1.t('span',{'class':'fa fa_history',style:'color:#0F0;',textNode:' Realizada'},td);
				}
				else{
				$1.T.btnFa({fa:'fa fa_history',textNode:' Pendiente', P:L, func:function(T){ $ps_DB.get({f:'PUT '+Api.Wma.a+'cost.log2',inputs:'id='+T.P.id, func:function(Jr2){ $1.Win.message(Jr2); }}); }},td);
				}
				$1.t('td',{textNode:$V.itmLog2Type[L.updateType]},tr);
				var text=(L.itemSzId && L.itemSzId!=0)?' (T:  '+$V.grs1[L.itemSzId]+')':' (Todas)';
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName+text},tr);
				$1.t('td',{textNode:'Por '+$Tb.ousr[L.userId]+', el '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				$1.t('td',{textNode:tExec},tr);
			}
		}
	}});
},

}


sHt.sellOrdHead=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo});
	var Ls=[
	{v:'Estado: '+$V.ordStatus[Jr.docStatus]},{v:'Pedido de Venta',vSty:'text-align:center; font-weight:bold;',ln:1,cs:7},
	{t:'Número',v:Jr.docEntry},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:5,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,rs:3},
	{t:'Fecha Pedido',v:Jr.docDate,vSty:'width:7rem;'},
	{t:'Fecha Entrega',v:Jr.dueDate},
	{t:Jr.licTradType, v:Jr.licTradNum},{t:'Cliente',v:Jr.cardName,ln:1,cs:6},
	{t:'Ref.',v:Jr.ref1},{t:'Vendedor',v:$Tb.oslp[Jr.slpId],ln:1,cs:4},{v:'Tipo: '+$V.ordTypePE[Jr.docType],ln:1},
	{t:'Dirección Envio',v:Jr.addrMerch,cs:3},{t:'Dirección Facturación',v:Jr.addrInv,ln:1,cs:3},
	{t:'Notas',v:Jr.lineMemo,cs:7}
	];
	var tb=$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}
sHt.BuyOrd=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo});
	var Ls=[
	{v:'Estado: '+$V.ordStatus[Jr.docStatus]},{v:'Orden de Compra',vSty:'text-align:center; font-weight:bold;',ln:1,cs:7},
	{t:'Número',v:Jr.docEntry},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:5,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,rs:3},
	{t:'Fecha Pedido',v:Jr.docDate,vSty:'width:7rem;'},
	{t:'Fecha Entrega',v:Jr.dueDate},
	{t:Jr.licTradType, v:Jr.licTradNum},{t:'Cliente',v:Jr.cardName,ln:1,cs:6},
	{t:'Dirección Envio',v:Jr.addrMerch,cs:3},{t:'Dirección Facturación',v:Jr.addrInv,ln:1,cs:3},
	{t:'Notas',v:Jr.lineMemo,cs:7}
	];
	var tb=$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}
sHt.sellDlvHead=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo},td);
	var Ls=[
	{v:'Estado: '+$V.ordStatus[Jr.docStatus]},{v:'Despacho de Mercancía',vSty:'text-align:center; font-weight:bold;',ln:1,cs:7},
	{t:'Número',v:Jr.docEntry,vSty:'width:7rem;'},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:5,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,rs:3},
	{t:'Fecha Documento',v:Jr.docDate},
	{t:'Ref.',v:((Jr.tr)?Jr.tr:'N/A')},
	{t:Jr.licTradType, v:Jr.licTradNum},{t:'Cliente',v:Jr.cardName,ln:1,cs:6},
	{t:'Bodega',v:$V.whsCode[Jr.whsId],cs:2},{t:'Vendedor',v:$Tb.oslp[Jr.slpId],ln:1,cs:4},
	{t:'Dirección Envio',v:Jr.addrMerch,cs:3},{t:'Dirección Facturación',v:Jr.addrInv,ln:1,cs:3},
	{t:'Observación de Pedido',v:Jr.lineMemoOrd,cs:7},
	{t:'Observación',v:Jr.lineMemo,cs:7}
	];
	if($jSoc.odlv_showRefs=='Y'){
		Ls.push({t:'No. Guia',v:Jr.delivRef1,cs:2});
		Ls.push({t:'No. Referencia',v:Jr.delivRef2,cs:2,ln:1});
		Ls.push({v:'',cs:2,ln:1});
	}
	$1.Tb.trCols(Ls,{cols:9,styT:'width:7rem; font-weight:bold;'},cont);
}
sHt.sellDlvFormHead=function(Jr,cont){
	var Ls=[
	{v:'Estado: '+$V.ordStatus[Jr.docStatus]},{v:'Despacho de Mercancía',ln:1,cs:7,vSty:'font-weight:bold; text-align:center;'},
	{t:'Número',v:Jr.docEntry},{t:'Fecha Documento',v:Jr.docDate,ln:1},{t:'Bodega',v:$V.whsCode[Jr.whsId],ln:1},{t:'Cliente',v:Jr.cardName,ln:1},
	{t:'Ref.',v:((Jr.tr)?Jr.tr:'N/A')},{t:'Desc.',v:Jr.discPf+'%',ln:1},{t:Jr.licTradType, v:Jr.licTradNum,ln:1},{t:'Vendedor',v:$Tb.oslp[Jr.slpId],ln:1,cs:1}
	];
	$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}
sHt.sellRdnHead=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo});
	var Ls=[
	{v:'Estado: '+$V.rdnStatus[Jr.docStatus]},{v:'Devolución de Venta',vSty:'text-align:center; font-weight:bold;',ln:1,cs:7},
	{t:'Número',v:Jr.docEntry},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:5,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,rs:3},
	{t:'Fecha Documento',v:Jr.docDate,vSty:'width:7rem;'},
	{t:'Fecha Recibido',v:Jr.dateGet},
	{t:'Bodega:',v:$V.whsCode[Jr.whsId]},{t:'Cliente',v:Jr.cardName,cs:5,ln:1},
	{t:'Acción Propuesta',v:Jr.doAction,cs:3},
	{t:'Detalles',v:Jr.lineMemo,cs:3,ln:1},
	];
	var tb=$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}
sHt.ivtCphHead=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo});
	var Ls=[
	{v:'Estado: '+$V.ordStatus[Jr.docStatus]},{v:'Cambio Producto Similares',vSty:'text-align:center; font-weight:bold;',ln:1,cs:7},
	{t:'Número',v:Jr.docEntry},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:5,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,rs:3},
	{t:'Fecha Documento',v:Jr.docDate,vSty:'width:7rem;'},
	{t:'Bodega',v:$V.whsCode[Jr.whsId]},
	{t:'Notas',v:Jr.lineMemo,cs:7}
	];
	var tb=$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}


$M.go({ini:'no.index'});
/* f(x)*/

$TbExp={
rep:function(K,tb){
	var R={};
	var tb=$TbExp[tb];
	for(var i in tb){ var L=tb[i];
		var tk=L.k; var k=L.k;
		if(K[k]){ tk=L.t;
			var text=K[k];
			if(L.type=='object'){
				var O=eval(L.O);
				text= (O && O[text])?O[text]:text;
			}
			switch(tb[i].format){
				case 'money': text=$Str.money(text); break;
				case 'mil': text=$Str.toMil(text); break;
			}
			R[tk]=text;
		}
	}
	return R;
},
'gvt_opvt':[
{k:'userName',t:'Creado Por'}, 
{k:'dateC',t:'Fecha Creación'}, 
{k:'slpId',t:'Responsable de Ventas', type:'object', O:'$Tb.oslp'}, 
{k:'docStatus',t:'Estado de Documento',type:'object',O:'$V.ordStatus'}, 
{k:'docDate',t:'Fecha Documento'}, 
{k:'cardName',t:'Nombre del Cliente'}, 
{k:'dueDate',t:'Fecha de Entrega o Vencimiento'}, 
{k:'addrMerch',t:'Dirección entrega Mercancía'}, 
{k:'addrInv',t:'Dirección Facturación'}, 
{k:'discDef',t:'Descuento Autorizado'}, 
{k:'discPf',t:'Descuento Pie Factura'}, 
{k:'discTotal',t:'Descuento Total'}, 
{k:'docTotalLine',t:'Total Lineas',format:'money'}, 
{k:'docTotal',t:'Total con descuento',format:'money'}, 
{k:'rate',t:'Tasa a la Fecha',format:'money'},
{k:'curr',t:'Moneda Documento'},
{k:'docTotalME',t:'Total ME',format:'mil'}
]
}
