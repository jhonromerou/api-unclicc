$M.sAdd([
{topFolder:{folId:'ivt',folName:'Inventario',fa:'fa fa_hands'}},
{fatherId:'ivt',MLis:['barcode.stickers']},
{fatherId:'ivt',L:[{folId:'itm',folName:'Artículos'}]},
{fatherId:'mast',L:[{folId:'ivtMast',folName:'Inventario'}]},

{fatherId:'itm',MLis:['itm.p','itm.mp','itm.se']},
{fatherId:'itm',L:[{folId:'itmCost',folName:'Costes',ico:'fa fa-money'}]},
{fatherId:'itmCost',MLis:['itm.cost','itm.cost.log2']},
{fatherId:'ivtMast',MLis:['jsv.itmGr','tb.itmOwhs','tb.itmOitp']},
]);