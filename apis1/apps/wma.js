
Api.Wma={a:'/sa/c70/wma/',bom:'/sa/c70/itm/rc'};

$V.wmaVariType={N:'Ninguna',tcons:'Consumo por Talla',tcode:'Código por Talla'};
$V.faseUdm={minutes:'Minutos',hours:'Horas',seconds:'Segundos',days:'Días'};
$V.itmLog2Type={mpUpdatePrice:'Costo de Materia Prima',model:'Materiales',modelVari:'Materiales (Variantes)',defineCost:'Definición Manual',defineMO:'Mano de Obra',defineCIF:'CIF'};

$M.sAdd([
{fatherId:'masters',L:[{folId:'mastWma3',folName:'Producción'}]},
{fatherId:'mastWma3',MLis:['wma.wfa','wma.wop','wma.ma','wma.sysd.wopGr','sysd.massData.wmaWfa']},
{L:[{folId:'wma3',folName:'Producción',ico:'iBg iBg_produccion'}]},
]);
$M.liA['wma']={t:'Producción',L:[
{t:'Maestro', L:[
{k:'wma.wfa.basic',t:'Maestro de Fases (Producción)'},
{k:'wma.ma',t:'Maestro de Máquinaria'},
{k:'wma.wop',t:'Maestro Mano de Obra'}
]},
{t:'Escandallos',L:[
	{k:'wma.mpg.basic',t:'Mano de Obra (Ver / Modificar)'},
	{k:'wma.cif.basic',t:'CIF (Ver / Modificar)'},
]},
{t:'Lista de Materiales',L:[
	{k:'wma.rc.basic',t:'Lista de Materiales - Composicion Base'},
	{k:'wma.rc.form2',t:'Lista de Materiales - Composicion por Variantes'}]
},
{t:'Costos',L:[
{k:'wma.cost.view',t:'Visualizar Costos de Producción'},
{k:'wma.cost.basic',t:'Definir Costos de Producción (Ver/Modificar)'},
{k:'wma.cost.log2.view',t:'Visualizar Log de Modificaciones'},
{k:'wma.cost.log2.basic',t:'Ejecutar Log de Modificaciones'}
]}
]};

_Fi['wma3.mpg']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mpg.get});
	wrap.appendChild(btnSend);
};
_Fi['wma.bom']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Bom.get});
	wrap.appendChild(btnSend);
};


$M.li['wma.wop']={t:'Maestro de Mano de Obra',kau:'wma.wop',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Mano de Obra',func:function(){ $M.to('wma.wop.form'); }});
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Wop.get(); }});
}}
$M.li['wma.wop.form']={t:'Formulario de Mano de Obra',kau:'wma.wop', func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wop.form(); }}); }}
$M.li['wma.ma']={t:'Maestro de Maquinaría',kau:'wma.ma',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Máquina',func:function(){ $M.to('wma.ma.form'); }});
	$M.Ht.ini({btnNew:btn, func_pageAndCont:function(){ Wma.MA.get(); }});
}}
$M.li['wma.ma.form']={t:'Formulario de Máquina',kau:'wma.ma', func:function(){ $M.Ht.ini({func_cont:function(){ Wma.MA.form(); }}); }};

$M.li['wma.sysd.wopGr']={t:'Grupos de Operaciones',kau:'sysd.suadmin',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nuevo Grupo',func:function(){ $M.to('wma.sysd.wopGr.form'); }});
	$M.Ht.ini({btnNew:btn, func_pageAndCont:Wma.Sysd.WopGr.get});
}}
$M.li['wma.sysd.wopGr.form']={t:'Grupo de Operación',kau:'sysd.suadmin', func:function(){ $M.Ht.ini({func_cont:Wma.Sysd.WopGr.form}); }}

$M.li['wma.wfa']={t:'Maestro de Fases', kau:'wma.wfa.basic',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Fase',func:function(){ $M.to('wma.wfa.form'); }});
	$M.Ht.ini({btnNew:btn, func_pageAndCont:Wma.Wfa.get});
}}
$M.li['wma.wfa.form']={t:'Formulario de Fase', kau:'wma.wfa.basic',func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wfa.form(); }}); }}
$M.li['wma.mpg']={t:'Definición Fases Articulo', kau:'wma.mpg.basic', ico:'fa_handRock', func:function(){
	$M.Ht.ini({func_filt:'wma3.mpg',func_pageAndCont:function(){ Wma.Mpg.get(); }});
}}
$M.li['wma.mpg.form']={t:'Asignación de Fases a Artículo',kau:'wma.mpg.basic', func:function(){
	$M.Ht.ini({jsLib:{tbk:'wma_owfa,wma_owop',noReload:1}, func_cont:function(){ Wma.Mpg.form(); }});
}}


$M.li['wma.bom'] ={t:'Estructura de Artículo',kau:'wma.rc.basic',ico:'fa_cubes', func:function(){
	$M.Ht.ini({func_filt:'wma.bom', func_pageAndCont:Wma.Bom.get});
}};
$M.li['wma.bom.form'] ={t:'Estructura Artículo (Base)',kau:'wma.rc.basic',d:'Lista de Materiales según modelo.', func:function(){
	$M.Ht.ini({jsLib:{tbk:'wma_owfa',noReload:'Y'}, func_cont:Wma.Bom.form});
}};
$M.li['wma.bom.form2'] ={t:'Estructura Artículo (Variantes)',kau:'wma.rc.form2', d:'Lista de materiales según talla variable.', func:function(){ $M.Ht.ini({func_cont:Wma.Bom.form2}); }};


var Wma={};
Wma.Wop={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma3.b+'wop', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Grupo','Nombre','Coste x Und','Coste Prep.','Tiempo x Und.','Tipo']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wop.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:_g(L.itemGr,$V.wmaWoGr)},tr);
				$1.t('td',{textNode:_g(L.prdType,$V.wopType)},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1+' Min'},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Wma3.b+'wop/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wopId',name:'status'},opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'prdType'},opts:$V.wopType,noBlank:1,selected:Jr.prdType}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Grupo Operación',aGo:'wma.sysd.wopGr',I:{tag:'select',sel:{'class':jsF,name:'itemGr'},opts:$V.wmaWoGr,selected:Jr.itemGr}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Coste x Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Tiempo x Und (min)',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.O={vPost:'itemId='+Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma3.b+'wop/form', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.itemId){ wId.O={vPost:'itemId='+Jr2.itemId}; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}
Wma.MA={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma3.b+'ma', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Coste x Und','Coste Prep.','Tiempo x Und.']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.ma.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1+' Min'},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Wma3.b+'ma/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wopId',name:'status'},opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Coste x Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Tiempo x Und (min)',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.O={vPost:'itemId='+Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma3.b+'ma/form', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.itemId){ wId.O={vPost:'itemId='+Jr2.itemId}; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}

Wma.Wfa={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma3.b+'wfa', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','ID','Código','Nombre','Descripción']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wfa.form','wfaId:'+T.P.wfaId); }},td);
				$1.t('td',{textNode:L.wfaId,style:'backgroundColor:#CCC'},tr);
				$1.t('td',{textNode:L.wfaCode},tr);
				$1.t('td',{textNode:L.wfaName},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Wma3.b+'wfa/form', loadVerif:!Pa.wfaId, inputs:'wfaId='+Pa.wfaId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wfaId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wfaCode',value:Jr.wfaCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wfaName',value:Jr.wfaName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wfaId',cont);
		wId.O={vPost:'wfaId='+Jr.wfaId};
		var resp=$1.t('div',0,cont);
	$Api.send({textNode:'Guardar Información',PUT:Api.Wma3.b+'wfa/form', loade:resp, getInputs:function(){ return $1.G.inputs(cont); },func:function(Jr2){
		if(Jr2.wfaId){ wId.O={vPost:'wfaId='+Jr2.wfaId}; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}

Wma.Mpg={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma3.b+'mpg', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Coste Total',{textNode:'$ M.P',_iHelp:'Coste Materiales y Semielaborados'},{textNode:'$ M.O',_iHelp:'Coste de Operaciones'},{textNode:'$ M.A',_iHelp:'Coste Máquinaria'},{textNode:'$ CIF',_iHelp:'Otros Costes'},'Tiempo','Udm','Descripción','Actualizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.mpg.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costMP)},tr);
				$1.t('td',{textNode:$Str.money(L.costMO)},tr);
				$1.t('td',{textNode:$Str.money(L.costMA)},tr);
				$1.t('td',{textNode:$Str.money(L.cif)},tr);
				$1.t('td',{textNode:L.faseTime*1},tr);
				$1.t('td',{textNode:_g(L.faseUdm,$V.faseUdm)},tr);
				$1.t('td',{textNode:L.descrip},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Wma3.b+'mpg/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:'Artículo',I:{tag:'input',type:'text',value:Jr.itemName,disabled:'disabled'}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF,name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Udm Tiempo',I:{tag:'select',sel:{'class':jsF,name:'faseUdm'},opts:$V.faseUdm,noBlank:1,selected:Jr.faseUdm}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var winM=$1.Menu.inLine([
		{textNode:'Fases','class':'fa fa_priv_members active',winClass:'wfaOper'},
		{textNode:'Costos','class':'fa fa_dollar',winClass:'wfaCost'}
		],{winCont:1}); cont.appendChild(winM);
		var wfaOper=$1.t('div',{'class':'winMenuInLine wfaOper'},winM);
		var wfaCost=$1.t('div',{'class':'winMenuInLine wfaCost',style:'display:none'},winM);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Materiales',I:{tag:'input',type:'text',disabled:'disabled',value:Jr.costMP,numberformat:'mil'}},wfaCost);
		$1.T.divL({wxn:'wrapx8',L:'Fases',I:{tag:'input',type:'text','class':'__costMo',disabled:'disabled',numberformat:'mil',value:Jr.costMo*1}},divL);
		$1.T.divL({wxn:'wrapx8',L:'CIF',I:{tag:'input',type:'text',value:Jr.cif,'class':jsF+' __cifTotal',name:'cif',numberformat:'mil',disabled:'disabled'}},divL);
		/*{textNode:'Fase Ant.',title:'Fase Anterior'},{textNode:'Fase Sig.',title:'Fase Siguiente'}*/
		if(Jr.L && Jr.L.errNo){ $Api.resp(wfaOper,JrL); }
		var inf=$1.t('div',{'class':'input_ok',style:'font-weight:normal; font-size:inherit'},wfaOper);
		$1.t('span',{'class':'fa fa-info'},inf);
		$1.t('span',{},inf).innerHTML = '<ul><li><u>bodega def.</u> es donde ingresará por defecto el componente nuevo.</li><li>La <u>Bod. Componentes</u> es desde donde se obtienen los componentes en la <u>fase anterior</u> a la que se desea realizar.</li></ul>';
		var tb=$1.T.table(['','Fase','Tiempo UdM',{textNode:'Coste M.P',_iHelp:'Costo de materiales'},{textNode:'Coste M.O',_iHelp:'Costo de Operaciones de la Fase'},{textNode:'Coste M.A',_iHelp:'Costo de Máquinaría'},'CIF',{textNode:'Exceder',_iHelp:'Permite que la fase permita más cantidades de la planificada'},'Bodega Def.','Bod. Componentes','Comentarios']);
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		btnAdd=$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }}); td.appendChild(btnAdd);
		var trF=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:2},trF);
		$1.t('td',{'class':tbSum.tbColNumTotal+'1'},trF);
		$1.t('td',{'class':tbSum.tbColNumTotal+'4',vformat:'money'},trF);
		$1.t('td',{'class':tbSum.tbColNumTotal+'5',vformat:'money'},trF);
		$1.t('td',{'class':tbSum.tbColNumTotal+'6',vformat:'money'},trF);
		$1.t('td',{'class':tbSum.tbColNumTotal+'7',vformat:'money'},trF);
		$1.t('td',{colspan:5},trF);
		var ni=1;
		function trOpet(tBody,L){
			var ln='L['+ni+']';
			var tr=$1.t('tr',{'data-vPost':'Y'},tBody);
			if(L.id){ tr.vPost=ln+'[id]='+L.id; }
			var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa-caret-up',title:'Poner Arriba',func:function(T){
			$1.Move.to('before',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){}});
			}},td);
			$1.T.btnFa({fa:'fa-caret-down',title:'Poner Abajo',func:function(T){
			$1.Move.to('next',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){}});
			}},td);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' '+tbSum.tbColNums,tbColNum:1,name:ln+'[teoricTime]',min:0,value:L.teoricTime,style:'width:4rem;', onkeychange:function(){ $Tol.tbSum(tb);} },td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' '+tbSum.tbColNums,tbColNum:4,name:ln+'[costMP]',min:0,value:L.costMP,style:'width:4rem;',numberformat:'mil',onkeychange:function(T){
					$Tol.tbSum(tb);
				}},td);
				var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' '+tbSum.tbColNums,tbColNum:5,name:ln+'[costMO]',min:0,value:L.costMO,style:'width:4rem;',numberformat:'mil',onkeychange:function(T){
					$Tol.tbSum(tb);
				}},td);
				var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' '+tbSum.tbColNums,tbColNum:6,name:ln+'[costMA]',min:0,value:L.costMA,style:'width:4rem;',numberformat:'mil',onkeychange:function(T){
					$Tol.tbSum(tb);
				}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __cifLine '+tbSum.tbColNums,tbColNum:7,name:ln+'[cif]',min:0,value:L.cif,style:'width:4rem;',numberformat:'mil',onkeychange:function(T){
					$Tol.tbSum(tb);
				}},td);
				var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[canExcess]'},opts:$V.NY,selected:L.canExcess,noBlank:1},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$Tb.whsPeP,selected:L.whsId},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[whsIdBef]'},opts:$Tb.whsPeP,selected:L.whsIdBef},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}}));
		}
		for(var i in Jr.L){ ni=Jr.L[i].lineNum;
			trOpet(tBody,Jr.L[i]); ni++;
		}
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		$Api.send({PUT:Api.Wma3.b+'mpg/form', loade:resp, getInputs:function(){ return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); },func:function(Jr2){
		$Api.resp(resp,Jr2);
		if(!Jr2.errNo){ Wma.Mpg.form(); }
		}},cont);
	}});
},
view:function(P){
	var wrapTop=$1.t('div'); var wrap=$1.t('div');
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.view',loade:wrapTop, inputs:'itemId='+P.itemId, func:function(Jr){
		wrapTop.appendChild($1.Win.print(wrap,{btn:{textNode:'Imprimir'}}));
		wrapTop.appendChild(wrap);
		if(Jr.errNo){ return $ps_DB.response(wrap,Jr); }
		var h5Text=(P.h5Text)?P.h5Text:Jr.mpgName;
		$1.t('h5',{textNode:h5Text},wrap);
		var tb=$1.T.table(['#','Operación','Tiempo','Costo','Tercero','Detalles']); wrap.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:L.wopName},tr);
			$1.t('td',{textNode:L.teoricTime*1},tr);
			$1.t('td',{textNode:$Str.money(L.cost)},tr);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		var tF=$1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total Operaciones',colspan:2},tr);
		$1.t('td',{textNode:Jr.faseTime*1},tr);
		$1.t('td',{textNode:$Str.money(Jr.costFaseF)},tr); $1.t('td',{colspan:2,rowspan:3},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Costo Fijo',colspan:3},tr);
		$1.t('td',{textNode:$Str.money(Jr.costF)},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:3},tr);
		var td=$1.t('td',{textNode:$Str.money(Jr.cost)},tr);
		if(Jr.isManual=='Y'){ td.appendChild($1.t('textNode',' (Manual)')); }
		$1.t('p',{textNode:'Para modificar está información, debe ir al maestro de mano de Obra.'},wrapTop);
	}})
	$1.Win.open(wrapTop,{winTitle:'Mano de Obra',onBody:1,winSize:'medium'});
}
}

Wma.Bom={
get:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.bom, inputs:$1.G.filter(), loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Tipo','Grupo','Actualizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemType[L.itemType]},tr);
				var ty=(L.itemType=='P')?$V.itemGrP:$V.itemGrMP;
				$1.t('td',{textNode:_g(L.itemGr,ty)},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
				$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Composición Base', P:L, func:function(T){ $M.to('wma.bom.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Composición de Variantes', P:L, func:function(T){ $M.to('wma.bom.form2','itemId:'+T.P.itemId); } }
				]},td);
			};
		}
	}});
},
form:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	var jsF='jsFields'; n=1;
	$ps_DB.get({f:'GET '+Api.Wma.bom+'.formData', errWrap:cont, loade:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['','Tipo',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total','Pieza','Variante','']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var vPost='';
			var i_WFA=(Jr._WFA)?Jr._WFA:{}; var wfaAnt=0;
		if(Jr.L.errNo==1){ $Api.resp(tb,Jr.L); }
		var nx=0;
		/* trFase tiene en cuenta trFaseSiguiente para añadir antes de */
		for(var i in i_WFA){ var L=i_WFA[i];
			vPost += 'WFA['+L.k+']=1&';
			if(nx==0){ nx=1;
				var tr=$1.t('tr',{'wfaId':L.k,'_1moveLimits':'top'},tBody);
			}
			else{
				var tr=$1.t('tr',{'wfaId':L.k,'wfaIdBef':wfaAnt,'class':'__wmaBomFase_'+wfaAnt},tBody);
			}
			var wfaAnt=L.k;
			var td=$1.t('td',{textNode:L.v,colspan:10,style:'backgroundColor:#CCC;'},tr);
			$1.T.btnFa({fa:'fa_cells', textNode:'Añadir Material',P:{k:L.k},func:function(T){
				Wma.Bom._trMP({wfaId:T.P.k},{n:n,tBody:tBody,jsF:jsF,tr:$1.q('.__wmaBomFase_'+T.P.k)}); n++;
			} },td);
		}
		vPost=(''+vPost).substr(0,vPost.length-1);
		if(!Jr.L.errNo){ for(var i in Jr.L){ var L=Jr.L[i];
				Wma.Bom._trMP(L,{n:n,tBody:tBody,jsF:jsF,tr:$1.q('.__wmaBomFase_'+L.wfaId)}); n++;
			}
		}
		var tfoot = $1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tfoot);
		$1.t('td',{colspan:6,textNode:'Total'},tr);
		$1.t('td',{},tr);
		$1.t('td',{'class':'__tbTotal',vformat:'money'},tr);
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.bom+'.formData', loade:resp, getInputs:function(){ return 'itemId='+Pa.itemId+'&'+vPost+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Wma.Bom.form(); }
		} }); cont.appendChild(btnSend);
		$Tol.tbSum(tBody.parentNode);

	}});
},
form2:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	n=1;
	var jsF='jsFields'; var ni = 1;
	$ps_DB.get({f:'GET '+Api.Wma.bom+'.formData2', errWrap:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb); trH.classList.add('trHead');
		var Ta=$V.grs2[Jr.grsId];
		$1.t('td',{textNode:'Variante'},trH);
		var trs=1;
		for(var t in Ta){ $1.t('td',{textNode:_g(t,$V.grs1)},trH); }
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $ps_DB.response(tb,Jr.L); }
		else if(!Jr.L.errNo){
			for(var i in Jr.L){ L=Jr.L[i];
				if(trs==5){ tBody.appendChild(trH.cloneNode(1)); trs=1; } trs++;
				var tr=$1.t('tr',0,tBody);
				var csp=(L.variType=='tcode')?2:1;
				$1.t('td',{textNode:n,rowspan:csp},tr); n++;
				$1.t('td',{textNode:L.pieceName,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemCode,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemName,rowspan:csp},tr);
				$1.t('td',{textNode:L.buyPrice,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:L.udm,rowspan:csp},tr);
				$1.t('td',{textNode:L.quantity,pformat:'float_2',rowspan:csp},tr);
				$1.t('td',{textNode:L.lineTotal,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:_g(L.variType,$V.wmaVariType),rowspan:csp},tr);
				if(L.variType=='tcons'){ trTabl(L,tr,ni); ni++; }
				else if(L.variType=='tcode'){
					var tr2=$1.t('tr',0,tBody);
					trTabl(L,tr2,ni);
					trCode(L,tr,ni); ni++;
				}
			}
		}
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.bom+'.formData2', loade:resp, getInputs:function(){ $1.G.noGet.push('0'); return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Wma.Bom.form2(); }
		} }); cont.appendChild(btnSend);
		function trTabl(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPost=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[buyPrice]='+Ld.buyPrice+'&'+ln+'[citemId]='+Ld.citemId+'&'+ln+'[itemSzId]='+ta+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[id]='+L.id;
				var inpc=$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem;','class':jsF+' _ntak_'+ni+'_'+ta,name:ln+'[quantity]',O:{vPost:vPost},value:L.quantity},td);
				if(L.id){
					$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
				}
			}
		}
		function trCode(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPostB=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[itemSzId]='+ta+'&';
				if(L.id){ vPostB +=ln+'[id]='+L.id+'&'; }
				var vPost2=(L.id)?vPostB+ln+'[citemId]='+L.citemId+'&'+ln+'[citemSzId]='+L.citemSzId+'&'+ln+'[buyPrice]='+L.buyPrice:'';
				var ntak='_ntak_'+ni+'_'+ta;
				var inpta=$1.q('.'+ntak,tr.parentNode.parentNode);
				inpta.O={vPost:vPost2}
				inpta.title=vPost2;
				var inpText=(L.udm)?Itm.Txt.name(L):'';
				$Api.Sea.input({api:Api.Itm.b+'sea/itemSz',
				P:{ln:ln,inpta:inpta,vPostB:vPostB},
				vPost:'I.itemType(E_in)=MP,SE&fie=I.buyPrice',lineTfunc:Itm.Txt.name,value:inpText,func:function(R,inp){
					var ln=inp.P.ln;
					inp.value =Itm.Txt.name(R);
					inp.P.inpta.O.vPost = inp.P.vPostB+ln+'[citemId]='+R.itemId+'&'+ln+'[citemSzId]='+R.itemSzId+'&'+ln+'[buyPrice]='+R.buyPrice;
					$Sea.replaceData(R,inp.pare.parentNode);
				}},td);
				/*
				inpta.O={vPost:vPost2}
				$Sea.input(td,{api:'itemData','class':$Sea.clsName+' '+jsF,k:'itemName',inputs:'FIE[][I.itemType]=MP&fields=I.udm,I.buyPrice',defLineText:L.itemName, inpVPost:inpta ,fPars:{vPost:vPost,ln:ln,ntak:ntak}, func:function(Jr2,inp,fPars){
					var inpta=$1.q('.'+fPars.ntak,tb);
					inpta.O = {vPost:fPars.vPost+fPars.ln+'[citemId]='+Jr2.itemId+'&'+fPars.ln+'[buyPrice]='+Jr2.buyPrice};
					$Sea.replaceData(Jr2,inp.pare);
				}}); */
			}
		}

	}});
},
_trMP:function(L,P){
		$1.nullBlank=''; var jsF=P.jsF;
		var ln='L['+P.n+']';
		var tr=$1.t('tr',{'data-vPost':'Y'});
		tr.setAttribute('wfaDef',L.wfaId);
		tr.vPost= ln+'[wfaId]='+L.wfaId;
		P.tBody.insertBefore(tr,P.tr);
		var td=$1.t('td',0,tr);
		$1.T.btnFa({fa:'fa-caret-up',title:'Poner Arriba',func:function(T){
			$1.Move.to('before',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){
				var wfa=trAnt.getAttribute('wfaIdBef');
				tr.setAttribute('wfaDef',wfa);
				if(wfa){ tr.vPost=ln+'[wfaId]='+wfa; }
			}});
		}},td);
		$1.T.btnFa({fa:'fa-caret-down',title:'Poner Abajo',func:function(T){
			$1.Move.to('next',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){
				var wfa=trAnt.getAttribute('wfaId');
				tr.setAttribute('wfaDef',wfa);
				if(wfa){ tr.vPost=ln+'[wfaId]='+wfa; }
			}});
		}},td);
		$1.t('td',{textNode:L.lineType,'class':$Sea.clsNameInp,k:'itemType'},tr);
		var vPost=(L.udm)?ln+'[id]='+L.id+'&'+ln+'[lineType]='+L.lineType+'&'+ln+'[citemId]='+L.citemId+'&'+ln+'[citemSzId]='+L.citemSzId+'&'+ln+'[buyPrice]='+L.buyPrice
		:'';
		var td=$1.t('td',{'class':$Sea.clsName,k:'itemCode',style:'width:6rem;',textNode:L.itemCode},tr);//itemCode
		var td=$1.t('td',0,tr);
		var inpText=(L.udm)?Itm.Txt.name(L):'';
		/* ojo estoy usand invPrice */
		$Api.Sea.input({api:Api.Itm.b+'sea/itemSz',vPost:'I.itemType(E_in)=MP,SE,MA,MO&fie=I.udm,I.invPrice,I.itemType',lineTfunc:Itm.Txt.name,value:inpText,func:function(R,inp){
			inp.value =Itm.Txt.name(R);
			R.buyPrice=R.invPrice; L.lineType=R.itemType;
			inpQty.vPost = ln+'[id]='+L.id+'&'+ln+'[citemId]='+R.itemId+'&'+ln+'[citemsZId]='+R.itemSzId+'&'+ln+'[buyPrice]='+R.invPrice+'&'+ln+'[lineType]='+R.itemType;
			R.udm=_g(R.udm,Udm.O);
			$Sea.replaceData(R,inp.pare.parentNode);
			if(!inpQty.value){ inpQty.value=1; $Tol.tbSum(P.tBody.parentNode); }
		}},td);
		var td=$1.t('td',{'class':$Sea.clsNameInp+' __tdNum',k:'buyPrice',kformat:'money',style:'width:6rem;',textNode:$Str.money(L.buyPrice)},tr);
		var td=$1.t('td',{'class':$Sea.clsNameInp,k:'udm',style:'width:4rem',textNode:_g(L.udm,Udm.O)},tr);
		var td=$1.t('td',0,tr);
		var inpQty= $1.t('input',{'data-vPost':'Y',type:'number',inputmode:'numeric',min:0,'class':jsF+' __tdNum2',name:ln+'[quantity]',style:'width:6rem;',value:L.quantity*1},td);
		inpQty.vPost=vPost;
		inpQty.onkeyup = inpQty.onchange = function(){ $Tol.tbSum(P.tBody.parentNode); }
		var td=$1.t('td',{'class':'__tdTotal',style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
		if(L.lineType=='MP' || L.lineType=='SE'){
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[pieceName]',value:L.pieceName},td);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[variType]'},opts:$V.wmaVariType,selected:L.variType,noBlank:1}); td.appendChild(sel);
		}
		else{ td=$1.t('td',{colspan:2},tr); }
		var td=$1.t('td',0,tr);
		if(L.id){
			$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
		}
		else{ var btn =$1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(tr); }}); td.appendChild(btn); }
		n++; $1.nullBlank=false;
	}
}

Wma.fichaProd=function(){
	var wrap=$1.t('div');
	{
	var tb=$1.t('table',{'class':'table_zh'});
	}
}

Wma.Sysd={};
Wma.Sysd.WopGr={
get:function(){ $Sysd.Jsv.get({get:Api.Wma3.b+'sysd/wopGr',toEdit:'wma.sysd.wopGr.form'}); },
form:function(){ $Sysd.Jsv.form({form:Api.Wma3.b+'sysd/wopGr/form',put:Api.Wma3.b+'sysd/wopGr',V:$V.wmaWoGr}); }
}
