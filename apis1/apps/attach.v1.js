Api.Attach.a='/a/attach/';
AttachV1={
formLine:function(P,pare){ //ir quitando por btnUp
	delete(P.getList);
	return Attach.btnUp(P,pare);
	//return Attach.btn(P,pare);
},

/* end integrado */
svr:'http://api0.admsistems.com/a/attach/a/upload',
svrLocal:Attach_svrLocal,
lPost:'/_files/uploadFile.php',
lDel:'/_files/delete.php',
//svr:'http://192.168.0.104:8051/1/attach/upload',
uriPost:'',
btn:function(P,pare){P=(P)?P:{};
	//if($s.storage=='L'){ Attach.svr='/_files/uploadFile.php'; }
	if($s.storage=='L'){ Attach.svr=Attach.svrLocal+'/storage'; }
	if(xsvr=$MCnf.get('Attach.uriSvr')){ Attach.svr=xsvr+'/storage'; }
	P.wT=(P.wT)?P.wT:{};
	if(P.fDraw=='Y'){//dibujar al subir
		P.func=function(o,P1){
			if(o){ for(var c in o){ Attach.drawL(o[c],P1.wList); } }
		}
	}
	var wid=Attach.wid+P.tt+'_'+P.tr;
	P.wT.id='__btnUpload';
	var wrap = $1.t('div',P.wT,pare);
	var vPostBase=gPost='tt='+P.tt+'&tr='+P.tr+'&';
	var vPost=vPostBase;
	if(!P.vP){ P.vP={}; }
	P.vP['tt']=P.tt; P.vP['tr']=P.tr;
	for(var i in P.gP){ gPost +=i+'='+P.vP[i]+'&'; }
	delete(P.noLoad);
	var lid='fileBtn_'+Attach.ni; Attach.ni++;
	var formu = $1.t('form',{name:'psUploadFileForm','style':'padding:5px 0;','server':$s.storage},wrap);
	var dFile = $1.t('div');
	var iFile = $1.t('input',{type:'file', id:lid, 'class':'psUFile psUFile_x32 _fileInput', name:'file'},formu);
	var iLabel = $1.t('label',{'for':lid,textNode:'Selección de Archivo'},formu);
	$1.t('span',{'class':'fa fa-info',title:'Storage: '+$s.storage+', svr: '+Attach.svr},formu);
	iFile.setAttribute('multiple',true);
	var wResp = $1.t('div',{'class':'__fileUpdwrapResp'},wrap); 
	var imgUp=new JStor.iFile(iFile,{func:function(Jq,JrL){
		if(P.ttPost=='Y'){ Attach.post(Jq,P,{cont:wResp}); }
		if(P.func){ P.func(Jq,JrL); }
	}});
	/*
	iFile.onchange = function(){
			if(this.files.length>3){ $1.Win.message({text:'No puede seleccionar más de 3 archivos.'}); }
			else{ Attach.revBef(this,P,wrap); $1.clearInps(this.parentNode); }
	}
	*/
	var wList = $1.t('div',{id:wid,'class':'_5f_wrapList',style:'margin-top:15px;'},wrap);
	P.wList=wList;
	if(pare){ pare.P=P; } //usar para obtener
	var inpS=$1.t('input',{type:'hidden','class':'jsFiltVars',name:'searchFie',value:'',O:{vPost:vPostBase}},wrap);
	if(P.open=='Y'){ iFile.click(); }
	if(P.getList=='tt'){ Attach.get(P); }
	if(P.winTitle){
		$1.delet(wid+'_');
		$1.Win.open(wrap,{winTitle:P.winTitle,onBody:1,winSize:'medium',winId:wid+'_'});
	}
	return wrap;
},
revBef:function(Tfile,P,wrapPar){ /* 1. revisión antes de */
	var r = Attach.r_size(Tfile.files);
	P.wList = $1.q('._5f_wrapList',wrapPar);
	if(r){ $1.Win.message(r); }
	else{ Attach.uplSvr(Tfile,P,wrapPar); }
},
storGet:false,/* obtener espacio */
revStor:function(Tfile,P,wrapPar){
	var total=0; var r=false;
	var tFile=Tfile.files; var maxFi=0;
	for(var i=0; i<tFile.length; i++){
		var fiSize=tFile[i].size;
		if(maxFi<fiSize){ maxFi=fiSize; }
		total += fiSize;
	}
	var vPost='storGet=Y&ocardCode='+$0s.ocardCode+'&maxFile='+maxFi+'&upTotal='+total
	$Api.get({url:Attach.svr,f:'',inputs:vPost,func:function(Jr){
		if(Jr.errNo){ $1.Win.message(Jr); return false; }
		P.wList = $1.q('._5f_wrapList',wrapPar);
		Attach.uplSvr(Tfile,P,wrapPar)
	}});
},
revSize:function(tFile){
	var total=0
	for(var i=0; i<tFile.length; i++){
		var fileSize= Math.round(tFile[i].size/1024/1024).toFixed(2);
		if($0s.cFile.maxSize<fileSize){ return {errNo:3,text:'El archivo supera el tamaño máximo por archivo a subir de '+$0s.cFile.maxSize+'Mb. ('+fileSize+')'}; }
		total+= fileSize;
	}
	return false;
},
uplSvr:function(Tfile,Po,wrapPar){
	var Po = (Po) ? Po : {};
	var Func = (Po.func)?Po.func: null;
	var formData = new FormData();
	var wResp = $1.q('.__fileUpdwrapResp',wrapPar);
	if(!wResp){  wResp = $1.t('div',{'class':'__fileUpdwrapResp'},wrapPar); }
	for(var f=0; f<Tfile.files.length; f++){
		var file = Tfile.files[f];
		var fName=(Po.fileNum)?f:'file['+f+']';
		formData.append(fName, file, file.name);
	}
	var MR = Po.MR;
	for(var k in Po.vP){ formData.append(k,Po.vP[k]);
	}
	if(window.XMLHttpRequest){ var xhr = new XMLHttpRequest();}
	else if(window.ActiveXObject){
		var xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xhr.open('POST', Attach.svr, true);
	if($0s.ocardCode){ xhr.setRequestHeader('ocardcode',$0s.ocardCode); }
	if($0s.stor('ocardtooken')){ xhr.setRequestHeader('ocardtooken',$0s.stor('ocardtooken')); }
	if($V.AJAXHeaders){
		for(var h in $V.AJAXHeaders){ xhr.setRequestHeader(h,$V.AJAXHeaders[h]); }
	}
	var abt = $1.t('button',{'class':'iBg iBg_closeSmall',title:'Cancelar Operación'},wResp);
	abt.onclick = function(){
		xhr.abort(); $1.clear(wResp);
	}
	var progress = $1.t('progress',{value:0,max:100,style:'width:90%; background-color:#0F0;'},wResp);
	xhr.upload.addEventListener("progress", function(e){
		progress.value = ((e.loaded/e.total)*100);
	}, false);
	xhr.onload = function(Event){
		if(xhr.status == 200){
			$1.clear(wResp);
			var Jq = JSON.parse(xhr.responseText);
			if(Jq.errNo || Jq.errs){ $1.Win.message(Jq); }
			else if(Po.ttPost=='Y'){ Attach.post(Jq,Po,{cont:wResp}) }
			else{ if(Func){ Func(Jq,Po); } }
		}
		else{
			$Api.resp(wResp,{errNo:5,text:'Err Status: '+xhr.status});
		}
	};
	xhr.send(formData);
	if($0s.consLogApi){ console.log('POST-FILE '+url); }
	Tfile.value = '';
},
ttgetL:function(P){
	var wList=P.wList;
	var vPost=(P.gP)?P.gP:'tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:Api.Stor, inputs:vPost, loade:wList,
	func:function(Jq){
		wList.classList.add(Attach.wid+'opened');
		if(Jq.errNo){ $Api.resp(wList,Jq); }
		for(var c in Jq.L){ Attach.Ht.drawL(Jq.L[c],wList); }
	}
	});
},
deleteBySrc:function(src){
	$Api.post({f:Api.Stor+'/deleteBySrc', inputs:'src='+src});
}
};


Attach.Tt={
win:function(P,pare){
	return Attach.openWin(P,pare);
},

delete:function(P){
	var btnDel = $1.T.btnFa({faBtn:'fa-trash',title:'Eliminar Archivo (3)',func:function(T){
		pare=T.parentNode;
		Attach.delete(P,T.parentNode);
	}});
	return btnDel;
},


form:function(D,pare){
	Attach.formLine({func:function(Jr3){
		if(!Jr3.errNo){ D.L=Jr3.L;
			Attach.Tt.up(D, function(Jrr,o){
				if(D.addLine && o){
					var wList=$1.q('._5f_wrapList',pare); /* pare=_5f */
					for(var i in o){
						Attach.Dw.line(o[i],wList);
					}
				}
				if(D.func){ D.func(Jrr,o); }
			});
		}
	}},pare);
},
up:function(D,func){
	var vPost ='&tt='+D.tt+'&tr='+D.tr;
	for(var i in D.L){
		for(var i2 in D.L[i]){ vPost += '&L['+i+']['+i2+']='+D.L[i][i2]; }
	}
	$Api.post({f:Api.Attach.a+'tt/up', inputs:vPost, winErr3:'Y', func:func});
},
get00:function(P,wList){
	if(wList && wList.classList.contains(Attach.wid+'opened')){
		return false;
	}
	var vPost='tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:Api.Attach.a+'tt/up', inputs:vPost, loade:wList,
	func:function(Jq){
		wList.classList.add(Attach.wid+'opened');
		if(Jq.errNo){ $Api.resp(wList,Jq); }
		for(var c in Jq.L){ Attach.Dw.line(Jq.L[c],wList); }
	}
	});
},

}

Attach.Ht={
drawL:function(J,wList){
	var wrap = $1.t('div',{'class':'_oFile_'+J.fileId+' psUpdList_Item',style:'position:relative;'},wList);
	var fileName = $1.t('a',{'href':J.purl,'target':'_BLANK','class':'iName'},wrap);
	$1.t('span',{'class':'iBg iBg_ico'+J.fileType},fileName);
	$1.t('span',{textNode:J.fileName},fileName);
	if(J.canDelete == 'Y'){ btnDel = Attach.Ht.delById(J); wrap.appendChild(btnDel) }
	var docInf= $1.t('div',{'class':'docInfo'},wrap);
	$1.t('span',{'class':'userName','textNode':J.userName},docInf);
	$1.t('span',{'class':'dateC','textNode':J.dateC},docInf);
	$1.t('span',{'class':'dateC','textNode':J.fileSizeText},docInf);
},
delById:function(P){
	var btnDel = $1.t('input',{type:'button','class':'btn iBg_trash btn2Right',title:'Eliminar este archivo'});
	btnDel.fileName = P.fileName; btnDel.fileId = P.fileId;
	btnDel.onclick = function(){
		var objDel = this.parentNode;
		$1.Win.confirm({text:'Se va eliminar el archivo, no se podrá recuperar esta información', func:function(){
			if($0s.fireb && $0s.fireb.apps>0){
				var refDel = firebase.storage().refFromURL(P.file);
				if(refDel){ refDel.delete(); }
			}
			$Api.delete({f:Api.Stor,inputs:'fileId='+P.fileId,
			func:function(J2){ if(!J2.errNo){ $1.delet(objDel);
			} }
			});
		}});
	}
	return btnDel;
}
}

Attach.r_size=function(tFile){
	var total=0
	var maxSize=$MCnf.get('Attach','maxSize');
	for(var i=0; i<tFile.length; i++){
		var fileSize= Math.round(tFile[i].size/1024/1024).toFixed(2);
		if(maxSize<fileSize){ return {errNo:3,text:'El archivo supera el tamaño máximo por archivo a subir de '+maxSize+'Mb. ('+fileSize+')'}; }
		total+= fileSize;
	}
	return false;
}

Attach.btnView=function(L,pare,P2){
	return $1.t('span',{'class':'fa fa-eye',title:'Ver Archivo',L:L,textNode:L.fileName},pare).onclick=function(){ Attach.view(this.L,null,P2); }
}


Attach.Dw={
line:function(J,wList){
	var wrap = $1.t('div',{'class':'gid_file_'+$o.T.fileUpd+'_'+J.fileId+' psUpdList_Item',style:'position:relative;'},wList);
	var fileName = $1.t('a',{'target':'_BLANK','class':'iName'},wrap);
	$1.t('span',{'class':'iBg iBg_ico'+J.fileType},fileName);
	$1.t('span',{textNode:J.fileName},fileName);
	$1.T.btnFa({fa:'fa-eye',func:function(){
		var w3=$1.t('div',{'class':''});
		$1.Win.open(w3,{winSize:'medium',onBody:1});
		GeDoc.Fi.view({pare:w3,fileId:J.fileId});
	}},fileName);
	var docInf= $1.t('div',{'class':'docInfo'},wrap);
	docInf.appendChild($1.t('span',{'class':'userName','textNode':$Tb._g('ousr',J.userId)}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.dateC}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.fileSizeText}));
	wrap.appendChild(docInf);
},
}
