
Api.Gtd = {a:'/v/gtd/',list:'/1/gtd/list/',task:'/1/gtd/task/'};

var $Hry={
/*Li[
{userId:1, v:markCompleted, from:completed, vto:uncompleted, dateC}
] */
_g:function(Li,pare,PD){
	var PD=(PD)?PD:{}; //obtener optionds etc
	var div1=$1.t('div',{'class':'Hry_wrapper'},pare);
	for(var i in Li){ var L=Li[i]; var vb=''+L.vb;
		var div=$1.t('div',{style:'font-size:0.8rem;'},div1);
		if(L.userId){ $1.t('span',{textNode:$Tb._g('ousr',L.userId)+' '},div); }
		if($Hry.v[vb]){ $1.t('span',{textNode:$Hry.v[vb]+' '},div); }
		else{ $1.t('span',{textNode:vb+' '},div); }
		if(vb=='assg'){ $1.t('span',{textNode:$Tb._g('ousr',L.vto)+' '},div); }
		else if(vb=='cardRel'){ div.appendChild($Doc.href('crd',{cardId:L.vto},{hrefText:'Socio'})); }
		else if(vb=='notes'){ }
		else{
			if(L.from){ $1.t('span',{textNode:'de '+$Hry._t(L.from,PD[vb])+' '},div); }
			if(L.vto){ $1.t('span',{textNode:'a '+$Hry._t(L.vto,PD[vb])+' '},div); }
		}
		$1.t('span',{textNode:' * '+L.dateC},div);
	}
	return div1;
},
_t:function(k,PD){
	return $js.k(PD,k,k);
	if(PD && PD[k]){ return PD[k]; } //correo
	else{ return k; } //email
}
};
$Hry.v={
created:'creó',
dueDate:'cambio la fecha de vencimiento',
markCompleted:'Marcó como completa',
markUncompleted:'Marcó como no completa',
type:'Cambio el tipo', priority:'Cambio la prioridad',
assg:'Asignó tarea a',
comment:'Realizo un comentario',
fileUpd:'Subio un archivo',
notes:'Actualizó la nota',
cardRel:'Relacionó',
}

var $Nty={
/*Li[
{userId:1 to,romUserId:1, vb:assg, oTy, gid}
] */
get:function(P,pare){ P=(P)?P:{};
	var winId='__NtyWrapPreview';
	var old=$1.q('#'+winId);
	if(old){ $1.delet(old); return false; }
	var wrap=$1.t('div',{id:winId+'_int'});
	$Api.get({f:'/1/nty/',loade:wrap, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wrap,Jr); }
		else{ $Nty._g(Jr.L,wrap);}
	}});
	if(P.win!='N'){
		$1.Win.minRel(wrap,{winId:winId,winTitle:'Última 20 sin leer...',onBody:1,winSize:'medium',posi:'r',width:'20rem;'},pare);
	}
},
_g:function(Li,pare,PD){
	var winId='__NtyWrapPreview';
	var PD=(PD)?PD:{}; //obtener optionds etc
	var div1=$1.t('div',{'class':'Nty_wrapper'},pare);
	for(var i in Li){ var L=Li[i]; var vb=L.vb;
		var div=$1.t('div',{'class':'Nty_lineWrap'},div1);
		if(L.fromUserId){
			$1.t('div',{textNode:$Tb._g('ousr',L.fromUserId)+' ','class':'_user'},div);
		}
		if(L.dateC){
			$1.t('div',{textNode:L.dateC,'class':'_dateC'},div);
		}
		var subDiv=$1.t('div',{'class':'_textarea'},div);
		if($Nty.vb[vb]){ $1.t('span',{textNode:$Nty.vb[vb]+' '},subDiv); }
		else{ $1.t('span',{textNode:vb+' '},subDiv); }
		if(L.oTy){ $Nty.oTy._g(L,subDiv,{func:function(){ $1.delet(winId); }}); }
	}
	var p=$1.t('p',0,div1);
	$1.T.btnFa({fa:'fa_eye', textNode:'Ver Todas',func:function(){
		$M.to('Nty'); $1.delet(winId);
	}},p);
	return div1;
},
_t:function(v,PD){
	if(PD && PD[v]){ return PD[v]; } //correo
	else{ return v; } //email
},
};

$Nty.vb={
assg:'Te asignó',
}
$Nty.oTy={
_g:function(L,pare,P){
	var P=(P)?P:{};
	var k=L.oTy;
	var o=$Nty.oTy[k];
	if(o && o.t){
		a=$1.t('a',{href:$Doc.href(k,L,{kl:'gid',r:1}),textNode:o.t},pare);
	}
	else{ a=$1.t('a',{href:'ls',textNode:k},pare); }
	if(P.func){ a.onclick=function(){ P.func(); } }
	return a;
},
gtdTask:{t:'Una actividad'}
}

var $Rel={
widCard:'__relCardWrap_'
};
$Rel.Card={
formLine:function(cont,P){
	var cont=(cont)?cont:$M.Ht.cont;
	var wrap=$1.t('div',0,cont);
	var sea=$crd.sea({func:P.func},cont);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:'Buscar Socio de Negocios...',Inode:sea},wrap);
	$1.t('div',{id:$Rel.widCard},wrap);
	return wrap;
},
add:function(L,tBody){
	if(!tBody){ tBody=$1.q('#'+$Rel.widCard+' tbody'); }
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:L.trMemo},tr);
	$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
	var td=$1.t('td',0,tr);
	btn=$1.T.btnFa({fa:'fa_close',title:'Eliminar Relación',func:function(){
		$Api.delete({f:Api.Task.rel,inputs:'taskId='+L.taskId+'&tt='+L.tt+'&tr='+L.tr, winErr1:'Y', func:function(Jr2){
			if(!Jr2.errNo){ $1.delet(tr); }
		}});
	}},td);
	return tr;
},
get:function(P,wList){
	wList=(wList)?wList:$1.q('#'+$Rel.widCard);
	$Api.get({f:Api.Task.rel,inputs:'taskId='+P.taskId,loade:wList, func:function(Jr){
		wList.classList.add($Rel.widCard+'opened');
		if(Jr.errNo){ $Api.resp(wList,Jr); }
		else{
			var tb=$1.T.table(['','','']); wList.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				$Rel.Card.add(L,tBody);
			}
		}
	}});
},
openOne:function(P){
	var wList=$1.q('#'+$Rel.widCard);
	if(wList && wList.classList && wList.classList.contains($Rel.widCard+'opened')){}
	else{ $Rel.Card.get(P,wList); }
}
}
