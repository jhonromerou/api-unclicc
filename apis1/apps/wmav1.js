
Api.Wma={a:'/sa/c70/wma/',bom:'/sa/c70/itm/rc'};

$V.variType={N:'Ninguna',tcons:'Consumo por Talla',tcode:'Código por Talla'};
$V.faseUdm={hours:'Horas',minutes:'Minutos',seconds:'Segundos',days:'Días'};
$V.itmLog2Type={mpUpdatePrice:'Costo de Materia Prima',model:'Materiales',modelVari:'Materiales (Variantes)',defineCost:'Definición Manual',defineMO:'Mano de Obra',defineCIF:'CIF'};

_Fi['wma.cost']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Talla'},I:{tag:'select',sel:{'class':jsV,name:'grs2.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;'},opts:$V.grs1}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'MP Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.mpManual(E_igual)'},opts:$V.YN}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'MO Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.moManual(E_igual)'},opts:$V.YN}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'CIF Manual'},I:{tag:'select',sel:{'class':jsV,name:'PC.cifManual(E_igual)'},opts:$V.YN}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Cost.get});
	wrap.appendChild(btnSend);
};
_Fi['wma.bom']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read('!');
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Bom.get});
	wrap.appendChild(btnSend);
};


$M.li['wma.wop']={t:'Maestro de Operaciones',kau:'wma.wop.basic',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Operación',func:function(){ $M.to('wma.wop.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Wop.get(); }});
}}
$M.li['wma.wop.form']={t:'Formulario de Operación',kau:'wma.wop.basic', func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wop.form(); }}); }}
$M.li['wma.wfa']={t:'Maestro de Fases', kau:'wma.wfa.basic',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Fase',func:function(){ $M.to('wma.wfa.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Wfa.get(); }});
}}
$M.li['wma.wfa.form']={t:'Formulario de Fase', kau:'wma.wfa.basic',func:function(){ $M.Ht.ini({func_cont:function(){ Wma.Wfa.form(); }}); }}
$M.li['wma.mpg']={t:'Mano de Obra (Escandallos)', kau:'wma.mpg.basic', ico:'fa_handRock', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Mano de Obra',func:function(){ $M.to('wma.mpg.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.Mpg.get(); }});
}}
$M.li['wma.mpg.form']={t:'Mano de Obra - Formulario',kau:'wma.mpg.basic', func:function(){ 
	$M.Ht.ini({jsLib:{tbk:'wma_owfa,wma_owop',noReload:1}, func_cont:function(){ Wma.Mpg.form(); }});
}}

$M.li['wma.cif']={t:'CIF (Escandallos)',kau:'wma.cif.basic',ico:'fa_handRock', func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nuevo CIF',func:function(){ $M.to('wma.cif.form'); }}); 
	$M.Ht.ini({btnNew:btn, func_cont:function(){ Wma.CIF.get(); }});
}}
$M.li['wma.cif.form']={t:'Definición de CIF - Formulario',kau:'wma.cif.basic', func:function(){ 
	$M.Ht.ini({jsLib:{tbk:'wma_owfa,wma_owop',noReload:1}, func_cont:function(){ Wma.CIF.form(); }});
}}

$M.li['wma.cost']={t:'Costo de Producción de Artículos',kau:'wma.cost.view',func:function(){
	$M.Ht.ini({fieldset:1,func_filt:'wma.cost', func_pageAndCont:function(){ Wma.Cost.get(); }});
}};
$M.li['wma.cost.defineForm']={t:'Costo de Producción de Artículo',kau:'wma.cost.basic',func:function(){
	$M.Ht.ini({func_cont:function(){ Wma.Cost.defineForm(); }});
}}
$M.li['wma.cost.log2']={t:'Log de Modificacines',kau:'wma.cost.log2.view',func:function(){
	$M.Ht.ini({func_pageAndCont:function(){ Wma.Cost.Log2.get(); }});
}}

$M.li['wma.bom'] ={t:'Lista de Materiales',kau:'wma.rc.basic',ico:'fa_cubes', func:function(){
	$M.Ht.ini({func_filt:'wma.bom', func_pageAndCont:Wma.Bom.get});
}};
$M.li['wma.bom.form'] ={t:'Lista Materiales (Base)',kau:'wma.rc.basic',d:'Lista de Materiales según modelo.', func:function(){
	$M.Ht.ini({jsLib:{tbk:'wma_owfa',noReload:'Y'}, func_cont:Wma.Bom.form});
}};
$M.li['wma.bom.form2'] ={t:'Lista de Materiales (Variable)',kau:'wma.rc.form2', d:'Lista de materiales según talla variable.', func:function(){ $M.Ht.ini({func_cont:Wma.Bom.form2}); }};


var Wma={};
Wma.Wop={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'wop', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Tipo','Nombre','Descripción']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wop.form','wopId:'+T.P.wopId); }},td);
				$1.t('td',{textNode:L.wopCode},tr);
				$1.t('td',{textNode:$V.wopType[L.wopType]},tr);
				$1.t('td',{textNode:L.wopName},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'wop.formData', loadVerif:!Pa.wopId, inputs:'wopId='+Pa.wopId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wopId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wopCode',value:Jr.wopCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wopName',value:Jr.wopName}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'wopType'},opts:$V.wopType,noBlank:1,selected:Jr.wopType}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wopId',cont);
		wId.O={vPost:'wopId='+Jr.wopId};
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'wop.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.wopId){ wId.O={vPost:'wopId='+Jr2.wopId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
}
}
Wma.Wfa={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'wfa', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Descripción']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.wfa.form','wfaId:'+T.P.wfaId); }},td);
				$1.t('td',{textNode:L.wfaCode},tr);
				$1.t('td',{textNode:L.wfaName},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'wfa.formData', loadVerif:!Pa.wfaId, inputs:'wfaId='+Pa.wfaId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wfaId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wfaCode',value:Jr.wfaCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wfaName',value:Jr.wfaName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wfaId',cont);
		wId.O={vPost:'wfaId='+Jr.wfaId};
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'wfa.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.wfaId){ wId.O={vPost:'wfaId='+Jr2.wfaId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
}
}

Wma.Mpg={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Tiempo','Costo','Costo Operaciones','Descripción','','Actualizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.mpg.form','mpgId:'+T.P.mpgId); }},td);
				$1.t('td',{textNode:L.mpgCode},tr);
				$1.t('td',{textNode:L.mpgName},tr);
				$1.t('td',{textNode:L.faseTime},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costFaseF)},tr);
				$1.t('td',{textNode:L.descrip},tr);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_arrowBack',textNode:' Asignar A', P:L, func:function(T){ Wma.Mpg.formItm(T.P); }},td);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.formData', loadVerif:!Pa.mpgId, inputs:'mpgId='+Pa.mpgId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'mpgCode',value:Jr.mpgCode}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF+' __mpgId',name:'mpgName',value:Jr.mpgName}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF,name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Udm Tiempo',I:{tag:'select',sel:{'class':jsF,name:'faseUdm'},opts:$V.faseUdm,noBlank:1,selected:Jr.faseUdm}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__mpgId',cont);
		wId.O={vPost:'mpgId='+Jr.mpgId};
		var winM=$1.Menu.inLine([
		{textNode:'Costos','class':'active fa fa_dollar',winClass:'wfaCost'},
		{textNode:'Operaciones','class':'fa fa_priv_members',winClass:'wfaOper'}
		],{winCont:1}); cont.appendChild(winM);
		var wfaOper=$1.t('div',{'class':'winMenuInLine wfaOper',style:'display:none'},winM);
		var wfaCost=$1.t('div',{'class':'winMenuInLine wfaCost'},winM);
		$1.t('p',{textNode:'Utilice un costo manual para la mano de Obra sino desea definir las operaciones y otros costos.'},wfaCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'¿Costo Manual?',I:{tag:'select',sel:{'class':jsF,name:'isManual'},selected:Jr.isManual,noBlank:1,opts:$V.NY}},wfaCost);
		$1.T.divL({wxn:'wrapx8',L:'Costo Manual',I:{tag:'input',type:'text','class':jsF,name:'costManual',value:Jr.costManual,numberformat:'mil'}},divL);
		$1.t('p',{textNode:'Defina el Costo Fijo. Este se sumará a los otros costos, para obtener el costo total.'},wfaCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Costo Fijo',I:{tag:'input',type:'text','class':jsF,name:'costF',value:Jr.costF,numberformat:'mil'}},wfaCost);
		var tb=$1.T.table(['#','Operación','Tiempo UdM','Coste','Comentarios','Fase','Tercero','']);
		if(Jr.WFA1 && Jr.WFA1.errNo){ $ps_DB.response(wfaOper,Jr.WFA1); }
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		btnAdd=$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }}); td.appendChild(btnAdd);
		var trF=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:2},trF);
		$1.t('td',{'class':'__tbTotal_col1'},trF);
		$1.t('td',{'class':'__tbTotal_col2',vformat:'money'},trF);
		$1.t('td',{colspan:3},trF);
		var ni=1;
		function trOpet(tBody,L){
			var ln='_WFOP['+ni+']';
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',{textNode:ni},tr);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wopId]'},opts:$Tb.owop,selected:L.wopId});
			td.appendChild(sel);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum',name:ln+'[teoricTime]',min:0,value:L.teoricTime,style:'width:4rem;', onkeychange:function(){ $Tol.tbSum(tb);} },td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum2',name:ln+'[cost]',min:0,value:L.cost,style:'width:4rem;',numberformat:'mil',onkeychange:function(){ $Tol.tbSum(tb);}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId});
			td.appendChild(sel);
			var td=$1.t('td',0,tr);
			$Sea.input(td,{api:'crd.all','class':jsF, vPost:ln+'[cardId]='+L.cardId, defLineText:L.cardName, fPars:{ln:ln}, func:function(L,inp,fPars){ inp.O={vPost:fPars.ln+'[cardId]='+L.cardId};
			}});
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}}));
		}
		for(var i in Jr.WFA1){ ni=Jr.WFA1[i].lineNum;
			trOpet(tBody,Jr.WFA1[i]); ni++; }
		for(var i=1; i<=5; i++){ trOpet(tBody,{}); ni++; }
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'mpg.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.mpgId){ wId.O={vPost:'mpgId='+Jr2.mpgId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
},
formItm:function(P){
	var cont=$1.t('div'); var ide='WmaMpgFormItm_wrap'; $1.delet(ide);
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.formItm', loadVerif:!P.mpgId, inputs:'mpgId='+P.mpgId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		$Sea.input(cont,{'class':'divLine',api:'itemData',inputs:'wh[I.itemType]=P',placeholder:'Nombre o código de artículo...',func:function(Jq){
				trA(tBody,Jq); n++;
			}});
			var tb=$1.T.table(['Código','Descripción','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Relacionar a Artículos'},{f:'PUT '+Api.Wma.a+'mpg.formItm',loade:resp, getInputs:function(){ return 'mpgId='+P.mpgId+'&'+$1.G.inputs(cont); }, func:function(Jr2){
					$ps_DB.response(resp,Jr2);
				}}); cont.appendChild(btn);
			var n=1;
			for(var i in Jr.L){ trA(tBody,Jr.L[i]); n++; }
		function trA(tBody,L){
			var ln='L['+n+']'; 
			var tr=$1.t('tr',0,tBody);
			var css=(L.dateUpd!=L.moDateUpd)?'color:#F00; font-weight:bold;':''
			$1.t('td',{textNode:L.itemCode,style:css},tr);
			$1.t('td',{textNode:L.itemName,style:css},tr);
			$1.t('td',{textNode:$2d.f(L.moDateUpd,'mmm d H:iam'),style:css},tr);
			var td=$1.t('td',0,tr);
			var ckL=$1.T.ckLabel({t:'Quitar',I:{'class':'jsFields',name:ln+'[delete]',O:{vPost:ln+'[mpgId]='+P.mpgId+'&'+ln+'[itemId]='+L.itemId}}},td);
		}
	}});
	$1.Win.open(cont,{winTitle:'Asignar Mano de Obra A',winSize:'medium',onBody:1,winId:ide});
},
view:function(P){
	var wrapTop=$1.t('div'); var wrap=$1.t('div');
	$ps_DB.get({f:'GET '+Api.Wma.a+'mpg.view',loade:wrapTop, inputs:'itemId='+P.itemId, func:function(Jr){
		wrapTop.appendChild($1.Win.print(wrap,{btn:{textNode:'Imprimir'}}));
		wrapTop.appendChild(wrap);
		if(Jr.errNo){ return $ps_DB.response(wrap,Jr); }
		var h5Text=(P.h5Text)?P.h5Text:Jr.mpgName;
		$1.t('h5',{textNode:h5Text},wrap);
		var tb=$1.T.table(['#','Operación','Tiempo','Costo','Tercero','Detalles']); wrap.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.lineNum},tr);
			$1.t('td',{textNode:L.wopName},tr);
			$1.t('td',{textNode:L.teoricTime*1},tr);
			$1.t('td',{textNode:$Str.money(L.cost)},tr);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		var tF=$1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total Operaciones',colspan:2},tr);
		$1.t('td',{textNode:Jr.faseTime*1},tr);
		$1.t('td',{textNode:$Str.money(Jr.costFaseF)},tr); $1.t('td',{colspan:2,rowspan:3},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Costo Fijo',colspan:3},tr);
		$1.t('td',{textNode:$Str.money(Jr.costF)},tr);
		var tr=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:3},tr);
		var td=$1.t('td',{textNode:$Str.money(Jr.cost)},tr);
		if(Jr.isManual=='Y'){ td.appendChild($1.t('textNode',' (Manual)')); }
		$1.t('p',{textNode:'Para modificar está información, debe ir al maestro de mano de Obra.'},wrapTop);
	}})
	$1.Win.open(wrapTop,{winTitle:'Mano de Obra',onBody:1,winSize:'medium'});
}
}

Wma.CIF={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Costo','Costo Fases','Descripción','','Actualizado']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.cif.form','cifId:'+T.P.cifId); }},td);
				$1.t('td',{textNode:L.cifCode},tr);
				$1.t('td',{textNode:L.cifName},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costFaseF)},tr);
				$1.t('td',{textNode:L.descrip},tr);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_arrowBack',textNode:' Asignar A', P:L, func:function(T){ Wma.CIF.formItm(T.P); }},td);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif.formData', loadVerif:!Pa.cifId, inputs:'cifId='+Pa.cifId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'cifCode',value:Jr.cifCode}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF+' __cifId',name:'cifName',value:Jr.cifName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__cifId',cont);
		wId.O={vPost:'cifId='+Jr.cifId};
		var winM=$1.Menu.inLine([
		{textNode:'Costos','class':'active fa fa_dollar',winClass:'cifCost'},
		{textNode:'Fases','class':'fa fa_priv_members',winClass:'cifFase'}
		],{winCont:1}); cont.appendChild(winM);
		var wfaOper=$1.t('div',{'class':'winMenuInLine cifFase',style:'display:none'},winM);
		var cifCost=$1.t('div',{'class':'winMenuInLine cifCost'},winM);
		$1.t('p',{textNode:'Defina el CIF manualmente, sino desea determinarlo por fases.'},cifCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'¿CIF Manual?',I:{tag:'select',sel:{'class':jsF,name:'isManual'},selected:Jr.isManual,noBlank:1,opts:$V.NY}},cifCost);
		$1.T.divL({wxn:'wrapx8',L:'Valor Manual',I:{tag:'input',type:'text','class':jsF,name:'costManual',value:Jr.costManual,numberformat:'mil'}},divL);
		$1.t('p',{textNode:'Defina el Costo Fijo. Este se sumará a los otros costos, para obtener el costo total.'},cifCost);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Costo Fijo',I:{tag:'input',type:'text','class':jsF,name:'costF',value:Jr.costF,numberformat:'mil'}},cifCost);
		var tb=$1.T.table(['#','Fase','Costo','Comentarios','Tercero','']);
		if(Jr.LN && Jr.LN.errNo){ $ps_DB.response(wfaOper,Jr.LN); }
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		btnAdd=$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }}); td.appendChild(btnAdd);
		var trF=$1.t('tr',0,tF);
		$1.t('td',{textNode:'Total',colspan:2},trF);
		$1.t('td',{'class':'__tbTotal_col1'},trF);
		$1.t('td',{'class':'__tbTotal_col2',vformat:'money'},trF);
		$1.t('td',{colspan:3},trF);
		var ni=1;
		function trOpet(tBody,L){
			var ln='_WFA['+ni+']';
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',{textNode:ni},tr);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId});
			td.appendChild(sel);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF+' __tdNum2',name:ln+'[cost]',min:0,value:L.cost,style:'width:4rem;',numberformat:'mil',onkeychange:function(){ $Tol.tbSum(tb);}},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			$Sea.input(td,{api:'crd.all','class':jsF, vPost:ln+'[cardId]='+L.cardId, defLineText:L.cardName, fPars:{ln:ln}, func:function(L,inp,fPars){ inp.O={vPost:fPars.ln+'[cardId]='+L.cardId};
			}});
			var td=$1.t('td',0,tr);
			td.appendChild($1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}}));
		}
		for(var i in Jr.LN){ ni=Jr.LN[i].lineNum;
			trOpet(tBody,Jr.LN[i]); ni++; }
		for(var i=1; i<=5; i++){ trOpet(tBody,{}); ni++; }
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		btnS=$1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.a+'cif.formData', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); },func:function(Jr2){
		if(Jr2.cifId){ wId.O={vPost:'cifId='+Jr2.cifId}; }
		$ps_DB.response(resp,Jr2);
		}}); cont.appendChild(btnS);
	}});
},
formItm:function(P){
	var cont=$1.t('div'); var ide='WmaMpgFormItm_wrap'; $1.delet(ide);
	$ps_DB.get({f:'GET '+Api.Wma.a+'cif.formItm', loadVerif:!P.cifId, inputs:'cifId='+P.cifId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		$Sea.input(cont,{'class':'divLine',api:'itemData',inputs:'wh[I.itemType]=P',placeholder:'Nombre o código de artículo...',func:function(Jq){
				trA(tBody,Jq); n++;
			}});
			var tb=$1.T.table(['Código','Descripción','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Relacionar a Artículos'},{f:'PUT '+Api.Wma.a+'cif.formItm',loade:resp, getInputs:function(){ return 'cifId='+P.cifId+'&'+$1.G.inputs(cont); }, func:function(Jr2){
					$ps_DB.response(resp,Jr2);
				}}); cont.appendChild(btn);
			var n=1;
			for(var i in Jr.L){ trA(tBody,Jr.L[i]); n++; }
		function trA(tBody,L){
			var ln='L['+n+']'; 
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var td=$1.t('td',0,tr);
			var ckL=$1.T.ckLabel({t:'Quitar',I:{'class':'jsFields',name:ln+'[delete]',O:{vPost:ln+'[cifId]='+P.cifId+'&'+ln+'[itemId]='+L.itemId}}},td);
		}
	}});
	$1.Win.open(cont,{winTitle:'Asignar CIF A',winSize:'medium',onBody:1,winId:ide});
},
}

Wma.Bom={
get:function(){
	cont =$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.bom, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Tipo','Grupo','Actualizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.udm},tr);
				$1.t('td',{textNode:$V.itemType[L.itemType]},tr);
				var ty=(L.itemType=='P')?$V.itemGrP:$V.itemGrMP;
				$1.t('td',{textNode:ty[L.itemGr]},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
				var menu=$1.Menu.winLiRel({Li:[
					{ico:'fa fa_pencil',textNode:' Composición Base', P:L, func:function(T){ $M.to('wma.bom.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Composición de Variantes', P:L, func:function(T){ $M.to('wma.bom.form2','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Mano de Obra', P:L, func:function(T){ Wma.Mpg.view({itemId:T.P.itemId,h5Text:T.P.itemCode+') '+T.P.itemName}) } }
				]});
				td.appendChild(menu);
			};
		}
	}});
},
form:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	var jsF='jsFields';
	$ps_DB.get({f:'GET '+Api.Wma.bom+'.formData', errWrap:cont, loade:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Variante',{textNode:'Fase Cons.',style:'width:4rem;'},'Total','']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $ps_DB.response(tb,Jr.L); }
		else if(!Jr.L.errNo) for(var i in Jr.L){ trA(Jr.L[i],tBody); }
		var btnA=$1.T.btnFa({fa:'fa_cells', textNode:'Añadir 2 Lineas',func:function(){ trA({},tBody); trA({},tBody); } }); cont.appendChild(btnA);
		var tfoot = $1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tfoot);
		$1.t('td',{colspan:6,textNode:'Total'},tr);
		$1.t('td',{},tr);
		$1.t('td',{'class':'__tbTotal',vformat:'money'},tr);
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.bom+'.formData', loade:resp, getInputs:function(){ return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$ps_DB.response(resp,Jr2);
		} }); cont.appendChild(btnSend);
		$Tol.tbSum(tBody.parentNode);
	}});
	n=1;
	function trA(L,tBody){
		$1.nullBlank='';
		var tr=$1.t('tr',0,tBody);
		var ln='L['+n+']';
		$1.t('td',{textNode:n},tr);
		var td=$1.t('td',0,tr);
		var inpPie= $1.t('input',{type:'text','class':jsF,name:ln+'[pieceName]',value:L.pieceName},td);
		var td=$1.t('td',{'class':$Sea.clsName,k:'itemCode',style:'width:6rem;',textNode:L.itemCode},tr);//itemCode
		var td=$1.t('td',0,tr);
		var vPost=(L.udm)?ln+'[id]='+L.id+'&'+ln+'[citemId]='+L.citemId+'&'+ln+'[buyPrice]='+L.buyPrice:'';
		inpPie.O ={vPost:vPost};
		$Sea.input(td,{api:'itemData','class':$Sea.clsName,k:'itemName',inputs:'wh[I.itemType(E_noIgual)]=P&fields=I.udm,I.buyPrice',defLineText:L.itemName, vPost:vPost, func:function(Jr2,inp){
			inpPie.O.vPost = ln+'[id]='+L.id+'&'+ln+'[citemId]='+Jr2.itemId+'&'+ln+'[buyPrice]='+Jr2.buyPrice;
			$Sea.replaceData(Jr2,inp.pare.parentNode);
		}});
		var td=$1.t('td',{'class':$Sea.clsNameInp+' __tdNum',k:'buyPrice',kformat:'money',style:'width:6rem;',textNode:$Str.money(L.buyPrice)},tr);
		var td=$1.t('td',{'class':$Sea.clsNameInp,k:'udm',style:'width:4rem',textNode:L.udm},tr);
		var td=$1.t('td',0,tr);
		var inp= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __tdNum2',name:ln+'[quantity]',style:'width:6rem;',value:L.quantity*1},td);
		inp.onkeyup = inp.onchange = function(){
			$Tol.tbSum(tBody.parentNode);
		}
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[variType]'},opts:$V.variType,selected:L.variType,noBlank:1}); td.appendChild(sel);
		var td=$1.t('td',0,tr);
		var sel=$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId}); td.appendChild(sel);
		var td=$1.t('td',{'class':'__tdTotal',style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
		var td=$1.t('td',0,tr);
		if(L.id){
			$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
		}
		else{ var btn =$1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(tr); }}); td.appendChild(btn); }
		n++; $1.nullBlank=false;
	}
},
form2:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	n=1;
	var jsF='jsFields'; var ni = 1;
	$ps_DB.get({f:'GET '+Api.Wma.bom+'.formData2', errWrap:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total']); cont.appendChild(tb);
		var trH=$1.q('thead tr',tb); trH.classList.add('trHead');
		var Ta=$V.grs2[Jr.grsId];
		$1.t('td',{textNode:'Variante'},trH);
		var trs=1;
		for(var t in Ta){ $1.t('td',{textNode:t},trH); }
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $ps_DB.response(tb,Jr.L); }
		else if(!Jr.L.errNo){
			for(var i in Jr.L){ L=Jr.L[i];
				if(trs==5){ tBody.appendChild(trH.cloneNode(1)); trs=1; } trs++;
				var tr=$1.t('tr',0,tBody);
				var csp=(L.variType=='tcode')?2:1;
				$1.t('td',{textNode:n,rowspan:csp},tr); n++;
				$1.t('td',{textNode:L.pieceName,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemCode,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemName,rowspan:csp},tr);
				$1.t('td',{textNode:L.buyPrice,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:L.udm,rowspan:csp},tr);
				$1.t('td',{textNode:L.quantity,pformat:'float_2',rowspan:csp},tr);
				$1.t('td',{textNode:L.lineTotal,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:$V.variType[L.variType],rowspan:csp},tr);
				if(L.variType=='tcons'){ trTabl(L,tr,ni); ni++; }
				else if(L.variType=='tcode'){
					var tr2=$1.t('tr',0,tBody);
					trTabl(L,tr2,ni);
					trCode(L,tr,ni); ni++;
				}
			}
		}
		var resp=$1.t('div',0,cont);
		var btnSend = $1.T.btnSend({textNode:'Guardar Información'},{f:'PUT '+Api.Wma.bom+'.formData2', loade:resp, getInputs:function(){ $1.G.noGet.push('0'); return 'itemId='+Pa.itemId+'&'+$1.G.inputs(cont,jsF); }, func:function(Jr2){
			$ps_DB.response(resp,Jr2);
		} }); cont.appendChild(btnSend);
		function trTabl(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPost=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[buyPrice]='+Ld.buyPrice+'&'+ln+'[citemId]='+Ld.citemId+'&'+ln+'[itemSzId]='+ta+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[id]='+L.id;
				$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem;','class':jsF+' _ntak_'+ni+'_'+ta,name:ln+'[quantity]',O:{vPost:vPost},value:L.quantity},td);
				if(L.id){
					$1.T.ckLabel({t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:ln+'[delete]'}},td);
				}
			}
		}
		function trCode(Ld,tr,ni){
			for(var ta in Ta){
				var td=$1.t('td',0,tr);
				ln='L['+ni+'-'+ta+']';
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var vPost=ln+'[itemId]='+Ld.itemId+'&'+ln+'[fatherId]='+Ld.id+'&'+ln+'[pieceName]='+Ld.pieceName+'&'+ln+'[id]='+L.id+'&'+ln+'[itemSzId]='+ta+'&';
				var vPost2=(L.id)?vPost+ln+'[citemId]='+L.citemId+'&'+ln+'[buyPrice]='+L.buyPrice:'';
				var ntak='_ntak_'+ni+'_'+ta;
				var inpta=$1.q('.'+ntak,tr.parentNode.parentNode); inpta.O={vPost:vPost2}
				$Sea.input(td,{api:'itemData','class':$Sea.clsName+' '+jsF,k:'itemName',inputs:'FIE[][I.itemType]=MP&fields=I.udm,I.buyPrice',defLineText:L.itemName, inpVPost:inpta ,fPars:{vPost:vPost,ln:ln,ntak:ntak}, func:function(Jr2,inp,fPars){
					var inpta=$1.q('.'+fPars.ntak,tb); 
					inpta.O = {vPost:fPars.vPost+fPars.ln+'[citemId]='+Jr2.itemId+'&'+fPars.ln+'[buyPrice]='+Jr2.buyPrice};
					$Sea.replaceData(Jr2,inp.pare);
				}});
			}
		}
		
	}});
},
}

Wma.fichaProd=function(){
	var wrap=$1.t('div');
	{
	var tb=$1.t('table',{'class':'table_zh'});
	}
}

Wma.Cost={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Descripción','Precio Venta','Costo','MP','MO','CIF','Tiempo','Actualizado']);
			var tBody=$1.t('tbody',0,tb);
			Jr.L=$js.sortNum(Jr.L,{k:'itemCode'});
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wma.cost.defineForm','itemId:'+T.P.itemId+',itemSzId:'+T.P.itemSzId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName+'  T: '+$V.grs1[L.itemSzId]},tr);
				$1.t('td',{textNode:$Str.money(L.sellPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				var mpCost=((L.mpManual=='Y')?L.mpCostManual:L.mpCost);
				var moCost=((L.moManual=='Y')?L.moCostManual:L.moCost);
				var cif=((L.cifManual=='Y')?L.cifCostManual:L.cif);
				var other=((L.otherManual=='Y')?L.otherCostManual:L.other);
				$1.t('td',{textNode:$Str.money(mpCost),xls:{t:mpCost}},tr);
				$1.t('td',{textNode:$Str.money(moCost),xls:{t:moCost}},tr);
				$1.t('td',{textNode:$Str.money(cif),xls:{t:cif}},tr);
				//$1.t('td',{textNode:$Str.money(other),xls:{t:other}},tr);
				$1.t('td',{textNode:(L.faseTime*1)+' '+$V.faseUdm[L.faseUdm]},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
			tb=$1.T.tbExport(tb,{print:1,fileName:'Resultado Costo de Produccion de Articulos'});
			cont.appendChild(tb);
		}
	}});
},
defineForm:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	var vPost='itemId='+Pa.itemId+'&itemSzId='+Pa.itemSzId;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost.define', loade:cont, inputs:vPost, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			$M.Ht.title.innerHTML= Jr.itemCode+') '+Jr.itemName+'  T: '+$V.grs1[Jr.itemSzId];
			var tb=$1.T.table(['','Manual','Valor Manual','Matriz']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Materia Prima'},tr);
			var td=$1.t('td',0,tr);
			var ma0=$1.T.sel({sel:{'class':jsF,name:'mpManual'},opts:$V.NY,noBlank:1,selected:Jr.mpManual});
			td.appendChild(ma0);
			var td=$1.t('td',0,tr);
			var ma1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'mpCostManual',value:Jr.mpCostManual},td);
			var td=$1.t('td',0,tr);
			var ma2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'mpCost',value:Jr.mpCost,disabled:'disabled'},td);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Mano de Obra'},tr);
			var td=$1.t('td',0,tr);
			var mo0=$1.T.sel({sel:{'class':jsF,name:'moManual'},opts:$V.NY,noBlank:1,selected:Jr.moManual});
			td.appendChild(mo0);
			var td=$1.t('td',0,tr);
			var mo1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'moCostManual',value:Jr.moCostManual},td);
			var td=$1.t('td',0,tr);
			var mo2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'moCost',value:Jr.moCost,disabled:'disabled'},td);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'CIF'},tr);
			var td=$1.t('td',0,tr);
			var cif0=$1.T.sel({sel:{'class':jsF,name:'cifManual'},opts:$V.NY,noBlank:1,selected:Jr.cifManual});
			td.appendChild(cif0);
			var td=$1.t('td',0,tr);
			var mo1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'cifCostManual',value:Jr.cifCostManual},td);
			var td=$1.t('td',0,tr);
			var mo2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'cif',value:Jr.cif,disabled:'disabled'},td);
			var tr=$1.t('tr');
			$1.t('td',{textNode:'Otros Costos'},tr);
			var td=$1.t('td',0,tr);
			var oth0=$1.T.sel({sel:{'class':jsF,name:'otherManual'},opts:$V.NY,noBlank:1,selected:Jr.otherManual});
			td.appendChild(oth0);
			var td=$1.t('td',0,tr);
			var oth1=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'otherCostManual',value:Jr.otherCostManual},td);
			var td=$1.t('td',0,tr);
			var oth2=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'other',value:Jr.other,disabled:'disabled'},td);
			var resp=$1.t('div',0,cont);
			var btn=$1.T.btnSend({textNode:'Definir Costos'},{f:'PUT '+Api.Wma.a+'cost.define', getInputs:function(){ return vPost+'&'+$1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
				$ps_DB.response(resp,Jr2);
			}}); cont.appendChild(btn);
			$1.t('p',{'class':'input_ok fa fa_info',textNode:'Si realiza actualizaciones desde alguna de las matrices de datos, pero el artículo tiene definido el costo manualmente (por ejemplo para la Mano de Obra) se calculará con base al costo manual y no con el de la matriz al ejecutar la actualización desde el Log de Modificaciones.'},cont);
		}
	}});
}
}
Wma.Cost.Log2={
get:function(){
	var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Wma.a+'cost.log2', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['Ejecución','Tipo','Código','Descripción','Solicitado','Ejecutado Por']);
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var tExec=(L.isExec=='Y')?'Por '+$Tb.ousr[L.userExec]+', el '+$2d.f(L.dateExec,'mmm d H:iam'):'Pendiente';
				if(L.isExec=='Y'){
					$1.t('span',{'class':'fa fa_history',style:'color:#0F0;',textNode:' Realizada'},td);
				}
				else{
				$1.T.btnFa({fa:'fa fa_history',textNode:' Pendiente', P:L, func:function(T){ $ps_DB.get({f:'PUT '+Api.Wma.a+'cost.log2',inputs:'id='+T.P.id, func:function(Jr2){ $1.Win.message(Jr2); }}); }},td);
				}
				$1.t('td',{textNode:$V.itmLog2Type[L.updateType]},tr);
				var text=(L.itemSzId && L.itemSzId!=0)?' (T:  '+$V.grs1[L.itemSzId]+')':' (Todas)';
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName+text},tr);
				$1.t('td',{textNode:'Por '+$Tb.ousr[L.userId]+', el '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				$1.t('td',{textNode:tExec},tr);
			}
		}
	}});
},

}
