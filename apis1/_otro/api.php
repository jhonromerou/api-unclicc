<?php
require('tbTop.php');
/*
$q=a_sql::query('SELECT A.tbk, A.tbId,B.id,B.tbId FROM otb A JOIN tb1 B ON (B.tbId=A.tbId) ',array(1=>'Error obteniendo estructura api1',2=>'No hay parametros definidos para api1'));
	if(a_sql::$err){ echo a_sql::$errNoText; }
	else{
	while($L=$q->fetch_assoc()){
		a_Sql::query('UPDATE tb1 SET tbk=\''.$L['tbk'].'\' WHERE id=\''.$L['id'].'\' LIMIT 1');
	}
	}
#*/
/*
$q=a_sql::query('SELECT A.tbk, A.tbId,B.id,B.tbRelId FROM otb A JOIN tb1 B ON (B.tbRelId=A.tbId) ',array(1=>'Error obteniendo estructura api1',2=>'No hay parametros definidos para api1'));
	if(a_sql::$err){ echo a_sql::$errNoText; }
	else{
	while($L=$q->fetch_assoc()){
		a_Sql::query('UPDATE tb1 SET tbRelId=\''.$L['tbk'].'\' WHERE id=\''.$L['id'].'\' LIMIT 1');
	}
	}
#*/ 

class _otb{
static public function getfType($L=array()){
	$k=($L['fieType'])?$L['fieType']:'??';
	$kr=$k; $tit='Máximo';
	$k=strtolower($k);
	if(preg_match('/(int|bigint|mediumint)/',$k)){ $kr='Numero'; }
	else if(preg_match('/(varchar|text)/',$k)){  $kr='Texto'; $tit='Largo Texto'; }
	else if($k=='date'){ $kr='Fecha (AAAA-mm-dd)'; }
	if($L['len']){ $kr .= ' <span title="'.$tit.'"> ('.$L['len'].')</span>'; }
	return $kr;
}
}

if($_GET['uriId']){
	$qA=a_sql::fetch('SELECT * FROM oapi WHERE uriId=\''.$_GET['uriId'].'\' LIMIT 1');
	$q=a_sql::query('SELECT B.*, tb1.fiek fiekTb,tb1.type fieType,tb1.len,tb1.description FROM api1 B 
	LEFT JOIN tb1 ON (tb1.tbk=IF(B.tbkRel!=\'\',B.tbkRel,B.tbk) AND tb1.fiek=B.fiek AND tb1.tbkRel=\'\')  
	WHERE B.uriId=\''.$_GET['uriId'].'\' ORDER BY B.orden ASC, tb1.orden ASC',array(1=>'Error obteniendo estructura api1',2=>'No hay parametros definidos para api1'));
	if(a_sql::$err){ $html= '<div class="input_error">'.a_sql::$errNoText.'</div>'; }
	else{
		$lastTbk='';
		while($L=$q->fetch_assoc()){
			$trTbk=($lastTbk!=$L['tbk']);
			$html .= trFie($L,$trTbk);
			$lastTbk=$L['tbk'];
		}
	}
	echo '
	<h3>'.$qA['method'].' '.$qA['uri'].'</h3>
	<p style="font-size:0.75rem;">'.$qA['descrip'].'</p>
	<fieldset class="_1dispWrap">
	<legend "class"="_1dispBtn">Parametros</legend>
	<table class="table_zh _1dispCont">
	<thead><tr>
	<td>Campo</td> <td>Opc.</td> <td>Tipo</td> <td>Descripción</>
	</tr></thead>
	<tbody>'.$html.'</tbody>
	</table>
	</fieldset>
	<h5>Explicación</h5>
	<div class="textarea pre">'.$qA['expl'].'</div><br/>
	
	<h5>Detalles de Código</h5>
	<div class="textarea"><b>PHP: </b><br/>'.$qA['php'].'</div><br/>
	<div class="textarea"><b>JS: </b><br/>'.$qA['js'].'</div>
	';
}
else{
	
	$q=a_sql::query('SELECT uriId,method,uri,reqTooken,descrip FROM oapi ORDER BY module, method ');
	while($L=$q->fetch_assoc()){
		$html .= trUri($L);
	}
	echo '<table class="table_zh">
	<thead><tr>
	<td>Metodo URI</td> <td>Tooken</td> <td>Descrip.</td>
	</tr></thead>
	<tbody>'.$html.'</tbody>
	</table>';
}

function trUri($L){
	$html='<tr>
	<td><pre><a href="?uriId='.$L['uriId'].'">'.$L['method'].'</a>		'.$L['uri'].'</pre></td>
	<td>'.$L['reqTooken'].'</td>
	<td>'.$L['descrip'].'</td>
</tr>';
return $html;
}
function trFie($L,$trTbk){
	$fiek=($L['fiekTb']!='')?$L['fiekTb']:$L['fiek'];
	$fiek=($L['fiekApi']!='')?$L['fiekApi']:$fiek;
	$desc=($L['description']!='')?$L['description']:$L['apiDesc'];
	$html='';
	if($trTbk){
		$html .='<tr>
		<td colspan="4" style="background-color:#DDD;">'.hrefTbk($L).'</td>
		</tr>';
	}
	$html .='<tr>
	<td title="Campo de Tabla: '.$L['tbk'].'">'.$fiek.'</td>
	<td>'.$L['optional'].'</td>
	<td>'._otb::getfType($L).'</td>
	<td>'.$desc.'</td>
</tr>';
return $html;
}
function hrefTbk($L=array(),$fiek=''){
	$tex=$L['tbk'];
	if($fiek!=''){
		$tex=$fiek;
		$fiek='#'.$fiek;
	}
	return '<a href="./tb.php?tbk='.$L['tbk'].$fiek.'" class="fa fa_arrowCircleR">'.$tex.'</a>';
}
?>
</body>