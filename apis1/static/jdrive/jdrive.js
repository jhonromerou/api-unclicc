
Api.JDrive={pr:'/appi/private/jdrive/'};

$V.JDrivePrivacity=[{k:'PR',v:'Privada'},{k:'PU',v:'Pública'}];
$V.JDrivePermsDefined=[{k:'N',v:'Herencia'},{k:'Y',v:'Especificos'}];
$V.JDrivePerms=[{k:'W',v:'Modificar'},{k:'R',v:'Ver'},{k:'P',v:'Administrador'}];
$V.JDriveSysCode=[{k:'cardE',v:'Empleados'},{k:'sgc',v:'Calidad'}];
$V.JDriveTtypes=[];
$V.JDriveFileTypes=[{k:'img',v:'Imagenes'},{k:'pdf',v:'PDF'},{k:'xls',v:'Hoja Calculo'},{k:'doc',v:'Documento'}];

$M.kauAssg('JDrive',[
	{k:'JDrive.sumaster',t:'Administrador Nube'},
	{k:'JDrive.box',t:'Nube'},
	{k:'JDrive.i.cardE',t:'Archivos Empleado'},
]);

$M.liAdd('JDrive',[
	{k:'JDrive.box.folders',t:'Estructura - Documentación',kau:'JDrive.sumaster',mdlActive:'JDrive',func:function(){
		$M.Ht.ini({g:function(){ JDrive.folders();}});
	}},
	{k:'JDrive.box',t:'Archivos',kau:'JDrive.box',mdlActive:'JDrive',func:function(){
		//$M.Ht.ini({g:function(){ JDrive.get();}});
		$M.Ht.ini({g:function(){ JDrive.i.ini({ F:{} }); }});
	}},
	{k:'JDrive.i.cardE',t:'Empleados',kau:'JDrive.i.cardE',mdlActive:'JDrive',func:function(){
		$M.Ht.ini({g:function(){
			var LBef=[];
			LBef.push({divLine:1,wxn:'wrapx1',L:'Empleado',Inode:$crd.Sea.get({cardType:'E','class':'jsFiltVars'})})
			JDrive.i.ini({ F:{LBef,Ali:{cardId:'tr'}} });
		}});
	}},
	{k:'JDrive.i.sgc',t:'Calidad',kau:'public',mdlActive:'JDrive',func:function(){
		$M.Ht.ini({g:function(){
			JDrive.i.ini({ F:{} });
		}});
	}},
]);

var JDrive={
	openFunc:'uri',
	fTree:function(P,func,pare){
		$1.delet($1.q('.JDrive_fTree',pare));
		var divT=$1.t('div',{'class':'JDrive_fTree'},pare);
		if(P.fTree){
			P.fTree=$js.sortBy('lv',P.fTree);
			for(var i in P.fTree){ var L=P.fTree[i];
				if(P.folId==L.folId){ continue; }
				$1.T.btnFa({fa:'fa-folder',textNode:L.folName,P:L,func:function(T){
					func(T);
				}},divT);
				$1.t('span',{textNode:' > '},divT);
			}
		}
		$1.t('span',{'class':'fa fa_folder',textNode:P.folName},divT);
	},
	isPerm:function(p,D){
		var D=(D)?D:{};
		if(p=='R' && (D.perms=='R' || D.perms=='W' || D.perms=='P')){ return true; }
		else if(p=='W' && (D.perms=='W' || D.perms=='P')){ return true; }
		else if(p=='P' && D.perms=='P'){ return true; }
		else{ return false; }
	},
	folders:function(){//get an put folders
		var XC=JDrive.Ht.wrapx3();
		var jsF=$Api.JS.cls;
		var D={};
		$Api.get({f:Api.JDrive.pr+'box',inputs:'', loade:XC.l, func:function(Jr){
			var Li=[
			{folId:0,fatherId:0,folName:'/',fAdd:function(T){ formu({fatherId:T.P.folId}); }}
			];
			for(var i in Jr.L){ var L=Jr.L[i];
				L.fEdit=function(T){ formu({folId:T.P.folId}); };
				L.fAdd=function(T){ formu({fatherId:T.P.folId,sysCode:T.P.sysCode}); };
				Li.push(L)
			}
			Uli.ini({Li:Li},XC.l);
		}});

		function formu(D){
			function midCont(D,pare){
				var midCont=$1.t('div',0,pare);
				var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:' ',I:{lTag:'select',opts:$Tb.ousr},btnR:{faBtn:'fa_plusCircle',textNode:'Añadir Usuario',func:function(T){
					var user=T.P.inp.value;
					addU({userId:user,perms:'R'});
				}}},midCont);
				var tb=$1.T.table(['Usuario','Permisos',''],0,midCont);
				var tBody=$1.t('tbody',0,tb);
				function addU(L){
					var tr=$1.t('tr',{'class':$Api.JS.clsL});
					if($1.uniRow('_userRow_'+L.userId,tBody,tr)){
						var td=$1.t('td',{textNode:_g(L.userId,$Tb.ousr)},tr);
						var td=$1.t('td',0,tr);
						$1.T.sel({'class':$Api.JS.clsLN,name:'perms',opts:$V.JDrivePerms,selected:L.perms},td).AJs={userId:L.userId};
						$1.lineDel(L,{},$1.t('td',0,tr));
					}
				}
				for(var z in D.U){ addU(D.U[z]); }
			}
			//ejecutar form
			$Api.form2({api:Api.JDrive.pr+'box/folders',folId:D.folId,vidn:'folId',AJs:{fatherId:D.fatherId},func:function(Jr,o){
					o.fEdit=function(T){ formu({folId:T.P.folId}); };
					o.fAdd=function(T){ formu({fatherId:T.P.folId,sysCode:T.P.sysCode}); };
					Uli.addFolder({Li:[o]},XC.l);
				},
				tbH:[
					{divLine:1,wxn:'wrapx4',L:'Nombre',req:'Y',I:{lTag:'input',name:'folName','class':jsF,value:D.folName}},
					{wxn:'wrapx8',L:'Color',I:{lTag:'input',type:'color',name:'folColor','class':jsF,value:D.folColor}},
					{wxn:'wrapx2',L:' ',I:{}},
					{wxn:'wrapx8',L:'SYS',I:{lTag:'select',name:'sysCode','class':jsF,value:D.sysCode,opts:$V.JDriveSysCode}},
					{divLine:1,wxn:'wrapx4',L:'Permisos',req:'Y',I:{lTag:'select',name:'privacity','class':'_permsDefined '+jsF,opts:$V.JDrivePermsDefined,selected:D.permsDefined,noBlank:'Y'}},
					{wxn:'wrapx4',L:'Privacidad',req:'Y',I:{lTag:'select',name:'privacity','class':'_privacity '+jsF,opts:$V.JDrivePrivacity,selected:D.privacity,noBlank:'Y'}},
					{divLine:1,wxn:'wrapx1',L:'Descripción',I:{lTag:'input',name:'folDesc','class':jsF,value:D.folDesc}},
				],
				midCont:midCont,
				reqFields:{
					D:[{k:'folName',iMsg:'Nombre Carpeta'}]
				},
				Win:{winSize:'medium',winTitle:'Carpeta'}
			});
		}
	},
	get:function(){
		var Par=$M.uriGet();
		var XC=JDrive.Ht.wrapx3();
		$Api.get({f:Api.JDrive.pr+'box', loade:XC.l, func:function(Jr){
			if(Jr.errNo){ $Api.resp(XC.l,Jr); }
			else if(Jr.L && Jr.L.errNo==1){ $Api.resp(XC.l,Jr.L); }
			else{
				var Li=[];
				Jr.L=$js.sortBy('lv',Jr.L);
				for(var i in Jr.L){ Li.push(Jr.L[i]); }
				Uli.ini({Li:Li,
					uriSet:['folId',JDrive.getFiles],
				},XC.l);
			}
		}});
		JDrive.getFiles();
	},

	postFiles:function(D,func){
		$Api.post({f:Api.JDrive.pr+'box/files', jsAdd:D, winErr3:'Y', func:func});
	},
	getFiles:function(){
		if($M.uriD.p.folId>0){}
		else{ return false; }
		var cont=$1.q('.'+JDrive.Cls.wrapFiles+'Tb');
		vGet =$M.uriD.vGet;
		if($M.uriD.l!='box'){ vGet='tt='+$M.uriD.l+'&'+vGet; }
		vGet += $1.filterGet($M.Ht.cont,{ini:'&',
		Ali:{cardId:'tr'}
		});
		$Api.get({f:Api.JDrive.pr+'box/files',inputs:vGet, loade:cont,
			More:{w:cont,func:function(Jr){
				var tBody=$1.q('tbody',cont);
				if(Jr.L && !Jr.L.errNo){ JDrive.Ht.trFile(tBody,Jr.L); }
			}},
			func:function(Jr){
				if(Jr.errNo){ $Api.resp(cont,Jr); }
				else{
					JDrive.fTree(Jr,function(T){
						$M.uriSet({folId:T.P.folId},JDrive.getFiles);
					},cont);
					var dPost={folId:Jr.folId};
					if($M.uriD.l!='box'){
						dPost.tt=$M.uriD.l; dPost.tr=$M.uriD.p.tr;
					}
					if(Jr.folDesc){ $1.t('p',{textNode:Jr.folDesc},cont); }
					var tb=$1.T.table([{textNode:'','class':'_ico'},'Nombre',{textNode:'Tamaño','class':'_w130'}],{addClass:'tableFixed'},cont);
					var tBody=$1.t('tbody',{'class':'JDrive_tbFiles'},tb);
					if(Jr.L && !Jr.L.errNo){ JDrive.Ht.trFile(tBody,Jr.L); }
					if(Jr.Lerr){ $Api.resp($1.t('div',0,cont),Jr.Lerr); }
					var upd=$1.t('div',{id:'fileUpd'},cont);
					if(JDrive.isPerm('W',Jr)){
						Attach.btnUp({func:function(Jr3){
							if(!Jr3.errNo){
								dPost.L=Jr3;
								JDrive.postFiles(dPost, function(Jrr,o){
									JDrive.Ht.trFile(tBody,o);
								});
							}
						}},upd);
					}
				}
			}
		});
	}
}
JDrive.Cls={
	wrapper:'JDrive_wrapper',
	wrapFol:'JDrive_wFolders',
	wrapFiles:'JDrive_wFiles',
	wrapViewFile:'JDrive_wViewFile',
}
JDrive.Ht={
	view:function(P){ P=(P)?P:{}; //visualizar datos archivo
		var wrap=(P.pare)?P.pare:$1.q('.'+JDrive.Cls.wrapViewFile);
		var Pa=$M.read();
		if(Pa.fileId){ P.fileId=Pa.fileId; }
		P.display='Y';
		return Attach.view(P,wrap);
	},
	wrapx2:function(P){
		var contA=$M.Ht.cont; contA.innerHTML='';
		var cont=$1.t('div',{'class':JDrive.Cls.wrapper},contA);
		//var lef=$1.t('div',{'class':JDrive.Cls.wrapFol},cont);
		var mid=$1.t('div',{'class':JDrive.Cls.wrapFiles,style:'float:none; width:auto;'},cont);
		var rig=$1.t('div',{'class':JDrive.Cls.wrapViewFile,textNode:'Vista'},cont);
		$1.t('div',{'class':'clear'},cont);
		return {c:cont,l:null,m:mid,r:rig};
	},
	wrapx3:function(P){
		P=(P)?P:{};
		var contA=$M.Ht.cont; contA.innerHTML='';
		var cont=$1.t('div',{'class':JDrive.Cls.wrapper},contA);
		if(P.F){/*filters*/
			ffOrders=[{k:'B1.dateC Desc',v:'Más nuevos'},{k:'B1.dateC Asc',v:'Más antiguos'},{k:'B1.fileName ASC',v:'Nombre A-Z'},{k:'B1.fileName DESC',v:'Nombre Z-A'}];
			$1.filter({func:JDrive.getFiles,LBef:P.F.LBef,LAft:P.F.LAft,
			L:[
				{divLine:1,L:'Ordenar por',wxn:'wrapx2',I:{lTag:'select',name:'orderBy',opts:ffOrders,noBlank:'Y'}},
				{L:'Tipo Archivo',wxn:'wrapx2',I:{lTag:'select',name:'B1.fileType',opts:$V.JDriveFileTypes}},
				{divLine:1,L:'Nombre Archivo',I:{lTag:'input',name:'B1.fileName(E_like3)'}},
				{divLine:1,L:'Tamaño >=',wxn:'wrapx2',I:{lTag:'number',name:'B1.fileSize(E_mayIgual)',min:0}},
				{L:'Tamaño <=',wxn:'wrapx2',I:{lTag:'number',name:'B1.fileSize(E_menIgual)',min:0}},
				{divLine:1,L:'Creado Desde',wxn:'wrapx2',I:{lTag:'date',name:'B1.dateC(E_menIgual)'}},
				{L:'Creado Hasta',wxn:'wrapx2',I:{lTag:'date',name:'B1.dateC(E_mayIgual)'}},
			]},cont);
		}
		var top=$1.t('div',{'class':JDrive.Cls.wrapper+'Top'},cont);
		var lef=$1.t('div',{'class':JDrive.Cls.wrapFol},cont);
		var midW=$1.t('div',{'class':JDrive.Cls.wrapFiles},cont);
		var mTop=$1.t('div',{'class':JDrive.Cls.wrapper+'TopMid'},midW);
		var mid=$1.t('div',{'class':JDrive.Cls.wrapFiles+'Tb'},midW);
		var rig=$1.t('div',{'class':JDrive.Cls.wrapViewFile,textNode:'Vista'},cont);
		$1.t('div',{'class':'clear'},cont);
		return {c:cont,t:top,
			l:lef,
			m:mid,mTop:mTop,
			r:rig};
	},
	trFile:function(tBody,JrL,P){
		var P=(P)?P:{};
		var view=P.view; delete(P.view);//card,oTy
		for(var i in JrL){ var L=JrL[i];
			var tr=$1.t('tr',0,tBody);
			if(L.isFile=='N'){
				$1.t('span',{'class':'fa fa_folder',title:'Carpeta'},$1.t('td',0,tr));
				var td=$1.t('td',{colspan:2},tr);
				var div=$1.t('div',{'class':'mPointer'},td);
				div.P=L;
				$1.t('span',{textNode:L.folName},div);
				div.onclick=function(){ $M.uriSet({folId:this.P.folId},JDrive.getFiles); }
				continue;
			}
			$1.t('td',{node:$1.t('span',{'class':'iBg iBg_ico'+L.fileType}),title:L.fileType},tr);
			var tddiv=$1.t('div',{'class':'_fileName'},$1.t('td',0,tr));
			var btn=$1.t('span',{textNode:L.fileName+' ',P:L},tddiv);
			btn.onclick=function(){
				switch(view){
					default: JDrive.Ht.view(this.P); break; //abrir directamente visualizacion info
				}
			}
			var td=$1.t('td',{textNode:L.fileSizeText},tr);
			$1.t('a',{href:L.purl,title:'Clic para descargar','class':'fa fa_attach',target:'_BLANK'},td);
			for(var ix in P.tds){
				tr.appendChild(P.tds[ix]);
			}
		}
	},
}

JDrive.i={
	ini:function(P){
		var P=(P)?P:{};
		var Pa=$M.uriGet();
		var XC=JDrive.Ht.wrapx3(P);
		if(P.iFunc){ P.iFunc(XC,function(){
			return JDrive.getFiles();
		}); } //crear input se busqueda y cambio
		if(Pa.l && Pa.l=='box'){ Pa.l=''; }//box es top
		JDrive.i.get(Pa,XC);
	},
	get:function(Pa,XC){
		$Api.get({f:Api.JDrive.pr+'box/',inputs:'sysCode='+Pa.l, loade:XC.l, func:function(Jr){
			if(Jr.errNo){ $Api.resp(XC.l,Jr); }
			else if(Jr.L && Jr.L.errNo==1){ $Api.resp(XC.l,Jr.L); }
			else{
				var Li=[];
				Jr.L=$js.sortBy('lv',Jr.L);
				for(var i in Jr.L){ Li.push(Jr.L[i]); }
				Uli.ini({Li:Li,
					uriSet:['folId',JDrive.getFiles],
				},XC.l);
			}
		}});
		JDrive.getFiles();
	},
}