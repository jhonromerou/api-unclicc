Api.Gfp={a:'/js/gfp/'};

$V.gfpMovType=[{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'},{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'R',v:'Retiros'}];
$V.gfpMovTypeWB=[{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'}];
$V.gfpWallType=[{k:'EF',v:'Efectivo'},{k:'CB',v:'Cuenta Bancaria'},{k:'TC',v:'T. Crédito'},{k:'CR',v:'Crédito'}];

$V.gfpCredType=[
	{k:'compra',v:'- Compra'},{k:'interes',v:'- Interes'},{k:'interesM',v:'- Interes Mora'},{k:'avance',v:'- Avance'},{k:'seguro',v:'- Seguro'},{k:'manejo',v:'- Manejo'},{k:'rediferido',v:'- Rediferido Compra'},{k:'otros',v:'- Otros Cargos'},
	{k:'pago',v:'+ Pago'},{k:'rediferidoP',v:'+ Rediferido Pago'},
	{k:'otrosP',v:'+ Otros Cargos'}
];
$V.gfpCredTypeCR=[
	{k:'compra',v:'- Desembolso'},{k:'interes',v:'- Interes'},{k:'interesM',v:'- Interes Mora'},{k:'seguro',v:'- Seguro'},{k:'otros',v:'- Otros Cargos'},
	{k:'pago',v:'+ Pago Cuota'},{k:'pagoC',v:'+Pago Capital'},
	{k:'otrosP',v:'+ Otros Cargos'}
];

_Fi['gfp.wal']=function(wrap){
	Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.wallName(E_like3)'}},wrap);
	if(Pa=='gfp.walCR'){
		$1.t('input',{type:'hidden','class':jsV,name:'A.wallType',value:'CR'},wrap);
	}
	else{
		$1.T.divL({wxn:'wrapx8', L:'Tipo',I:{tag:'select','class':jsV,name:'A.wallType',opts:$V.gfpWallType}},divL);
	}
	$1.T.btnSend({textNode:'Actualizar', func:Gfp.Wal.get},wrap);
};
_Fi['gfp.mov']=function(wrap){
	var Pa=$M.read();
	var ordBy=[{k:'',v:'Fecha Mayor'},{k:'docDateASC',v:'Fecha Menor'}]
	$Doc.filter({func:Gfp.Mov.get},[
		{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'M.wallId',opts:$Tb.gfpWal,selected:Pa.wallId}},
		{L:'Inicio',wxn:'wrapx8',I:{tag:'input',type:'date',name:'M.docDate(E_mayIgual)'}},
		{L:'Fecha Fin',wxn:'wrapx8',I:{tag:'input',type:'date',name:'M.docDate(E_menIgual)'}},
		{L:'Tipo',wxn:'wrapx8',I:{tag:'select',name:'M.mType',opts:$V.gfpMovType}},
		{L:'Categoria',wxn:'wrapx8',I:{tag:'select',name:'M.categId',opts:$JsV.gfpCateg}},
		{L:'Orden',wxn:'wrapx8',I:{tag:'select',name:'ordBy',opts:ordBy}},
		{divLine:1,wxn:'wrapx4', L:'Detalle',I:{tag:'input',type:'text',name:'M.lineMemo(E_like3)'}},
		{L:'Tipo TC',wxn:'wrapx10',I:{lTag:'select',name:'M.credType',opts:$V.gfpCredType}},
		{L:'Cuotas >=',wxn:'wrapx10',I:{lTag:'number',name:'M.credCuotas(E_mayIgual)'}},
		{L:'Prox.',wxn:'wrapx10',I:{lTag:'select',name:'M.cred1Pay',opts:$V.NY}}
	],wrap);
};
_Fi['gfpRep.tcp']=function(wrap){
	opts=[{k:'C',v:'Consolidado Periodo'},{k:'M',v:'Detalle Periodo'},{k:'LP',v:'Listado Periodos'}];
	$Doc.filter({func:Gfp.Rep.tcp},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'M.wallId','class':'gfpCuentaCorte',opts:$Tb.gfpWal,kIf:{wallType:'TC'}}},
	{L:'Corte',wxn:'wrapx4',I:{lTag:'select','class':'gfpCuentaCorte',name:'C1.pid',opts:[]}},
	{L:'Año',wxn:'wrapx8',I:{lTag:'number',min:0,name:'docYear'}},
	],wrap);
	Gfp.Fx.cuentaCorte(wrap,function(){
		return $1.q('[name="viewType"]',wrap).value=='LP';
	});
};
_Fi['gfpRep.copen']=function(wrap){
	opts=[{k:'G',v:'General'}];
	$Doc.filter({func:Gfp.Rep.copen},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'M.wallId',opts:$Tb.gfpWal,kIf:{wallType:'TC'}}},
	{k:'d1'},{k:'d2'},{L:'Pendiente >',wxn:'wrapx8',I:{lTag:'number',value:'0',name:'A.credSal(E_may)'}}
	],wrap);
};

$V.gfpTCjs={
cManejo:'C. Manejo', seguro:'Seguro',avances:"Avances",otros:"Otros",puntos:"Puntos"
};
var Gfp={};
Gfp.Wal={
OLg:function(L){
	var Li=[];
	var ab=new $Doc.liBtn(Li,L,{api:Api.Gfp.a+'wal',tbSerie:'N'});
	ab.add('E');
	ab.add('man',{k:'edit',ico:'fa fa-pencil',textNode:'Modificar', P:L, func:function(T){ Gfp.Wal.form(T.P); } })
	Li=ab.Opts();
	return Li;
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		$1.btnGo({br:1,textNode:'Registrar Movimientos',func:()=>{ $M.to('gfp.movForm'); }},cont);
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['',{textNode:'Tipo'},{textNode:'Nombre'},{textNode:'Disponible'},'',{textNode:'Deuda'},{textNode:'Cupo'},{textNode:'Cupo Disp.'},'','Detalles']);
			var css='backgroundColor:#EEE;';
			var cssSep='backgroundColor:#CCC;';
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',{'class':'wallId_'+L.wallId},tBody);
				var td=$1.t('td',0,tr);
				$1.Menu.winLiRel({Li:Gfp.Wal.OLg(L),PB:L},td);
				$1.t('td',{textNode:_g(L.wallType,$V.gfpWallType)},tr);
				var td=$1.t('td',{},tr);
				$1.T.btnFa({fa:'fa fa-money',P:L,func:function(T){ $M.to('gfp.mov','wallId:'+T.P.wallId)},title:'Visualizar Movimientos'},td);
				$1.t('span',{textNode:L.wallName},td);
				if(L.wallType=='TC'){
					var cssDeu='';
					var cssClose=($2d.diff({date1:L.closeDate})>=-7)?'backgroundColor:#FF0;':'';
					var percDeu=$js.toFixed((Math.abs(L.amount*1)/L.creditLine*1)*100,1);
					if(percDeu>50){ cssDeu='fontWeight:bold; color:purple;'; }
					else if(percDeu>40){ cssDeu='fontWeight:bold; color:red;'; }
					else if(percDeu>30){ cssDeu='fontWeight:bold; color:orange;'; }
					$1.t('td',{style:css},tr);
					$1.t('td',{style:cssSep},tr);
			$1.t('td',{textNode:$Str.money(L.amount),style:cssDeu,title:percDeu+' % Endeudamiento'},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine)},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine*1+L.amount*1)},tr);
					var td=$1.t('td',{style:cssClose},tr);
					var css1=(L.payDateMin<$2d.today)?'color:#0F0':'';
					$1.t('div',{textNode:'Pago Min.: '+$2d.f(L.payDateMin,'d mmm'),style:css1},td);
					$1.t('div',{textNode:'Avances: '+$Str.money(L.creditLineAvan)},td);
					$1.t('div',{textNode:'Corte : '+$2d.f(L.closeDate,'d mmm')},td);
					$1.t('div',{textNode:'Pago : '+$2d.f(L.payDate,'d mmm')},td);

				}
				else{
					$1.t('td',{textNode:$Str.money(L.amount)},tr);
					$1.t('td',{style:cssSep},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
				}
				var td=$1.t('td',0,tr);
				var jsTc=$js.parse(L.jsTc);
				if(jsTc){for(var ki in jsTc){
						var d=$1.t('div',0,td);
						$1.t('b',{textNode:$V.gfpTCjs[ki]},d);
						$1.t('span',{textNode:' '+jsTc[ki]},d);
				}}
				$1.t('div',{textNode:L.lineMemo},td);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'});
			cont.appendChild(tb);
		}
	}});
},
	form:(Pa)=>{
		Pa=(Pa)?Pa:{};
		wrap=$1.t('div'); $1.Win.open(wrap,{winTitle:'Cuenta',winSize:'medium'});
		$Api.form2({api:Api.Gfp.a+'wal',jsF:'Y',uid:{k:'wallId',v:Pa.wallId},
		tbH:[
			{L:'Tipo',wxn:'wrapx8',req:'Y',I:{lTag:'select',name:'wallType','class':'wallType',opts:$V.gfpWallType}},
			{L:'Nombre',wxn:'wrapx4',req:'Y',I:{lTag:'input',name:'wallName'}},
			{L:'Cod. Orden',wxn:'wrapx8',req:'Y',I:{lTag:'input',name:'orden'}},
			{L:'Apertura',wxn:'wrapx4',req:'Y',I:{lTag:'date',name:'dateOpen'}},
			{divLine:1,L:'Detalles',wxn:'wrapx1',I:{lTag:'input',name:'lineMemo'}},
			{divLine:1,L:'Cupo',wxn:'wrapx4',I:{lTag:'$',name:'creditLine'}},
			{L:'Avances',wxn:'wrapx4',I:{lTag:'$',name:'creditLineAvan'}},
			{L:'Vencimiento',wxn:'wrapx4',I:{lTag:'input',name:'dueDate',placeholder:'YYYY-mm'}},
			{L:'Cortes',wxn:'wrapx4',I:{lTag:'select',name:'cId',opts:$Tb.gfpCtc}},
		]},wrap);
	}
}

Gfp.Cre={
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Gfp.a+'cre',inputs:$1.G.filter(), loade:cont, func:function(Jr){
			if(Jr.errNo){ $Api.resp(cont,Jr); }
			else{
				var tb=$1.T.table(['','Saldo','Valor Prestado','Cuota','Int. Pagado','Apertura','Detalles']);
				var css='backgroundColor:#EEE;';
				var cssSep='backgroundColor:#CCC;';
				var tBody=$1.t('tbody',0,tb);
				for(var i in Jr.L){ L=Jr.L[i];
					var tr=$1.t('tr',{'class':'wallId_'+L.wallId},tBody);
					var td=$1.t('td',0,tr);
					Gfp.Fx.quotes(L,td);
					$1.t('td',{textNode:$Str.money(L.credSal)},tr);
					$1.t('td',{textNode:$Str.money(L.creBal)},tr);
					$1.t('td',{textNode:L.credCuoNum*1+' / '+L.credCuotas*1},tr);
					$1.t('td',{textNode:$Str.money(L.credInt)},tr);
					$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
					$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
					cont.appendChild(tb);
				}
			}
		}});
	},
form:function(Pa){
	Pa=(Pa)?Pa:{};
	var wrap=$1.t('div'); $1.Win.open(wrap,{winTitle:'Crédito',winSize:'full'});
	if(!Pa.docDate){ Pa.docDate=$2d.today; }
	//sysType=CR para volver matriz
	$Api.form2({api:Api.Gfp.a+'mov',jsF:'Y',uid:{k:'mid',v:Pa.mid},AJs:{sysType:'CR',mType:'G'},
		tbH:[
	{L:'Apertura',wxn:'wrapx8',req:'Y',I:{lTag:'date',name:'docDate',value:Pa.docDate}},
	{L:'Monto Prestado',wxn:'wrapx8',req:'Y',I:{lTag:'$',name:'bal',min:0}},
	{L:'Plazo (meses)',wxn:'wrapx10',req:'Y',I:{lTag:'number',name:'credCuotas',min:0}},
	{L:'Tasa E.A',wxn:'wrapx10',req:'Y',I:{lTag:'number',name:'credTasa',min:0}},
	{L:'$ Cuota',wxn:'wrapx8',req:'Y',I:{lTag:'$',name:'credBalCuota'}},
	{L:'Categoria',wxn:'wrapx8',req:'Y',I:{lTag:'select',name:'categId',opts:$JsV.gfpCategCR}},
	{divLine:1,L:'Detalles',wxn:'wrapx1',I:{lTag:'input',name:'lineMemo'}}
	],reqFields:{
		D:[{k:'docDate',iMsg:'Apertura'},{k:'bal',iMsg:'Monto Prestado'}],
	}},wrap);
},
}

Gfp.Mov={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'mov',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Tipo','Cuenta','Valor','Fecha','Categoria','TC Cuotas','Detalles']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var bal=(L.debBal>0)?L.debBal:'?';
				bal=(L.creBal>0)?L.creBal:bal;
				balCss=(L.creBal>0)?'color:red;':'color:green';
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{title:'mid: '+L.mid},tr);
				$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{
					T.P.editar='form';
					Gfp.Mov.editForm(T.P,T);
				}},td);
				if(L.wallType=='TC'){
					$1.t('td',{textNode:_g(L.credType,$V.gfpCredType)},tr);
					if(L.credCuotas>1 && L.credType.match(/^(compra|avance|rediferido)$/)){
						Gfp.Fx.quotes(L,td);
					}
					if(L.wallType=='TC'){
						$1.T.check({name:'cred1Pay',value:L.cred1Pay,'class':$Api.JS.cls,AJs:{mid:L.mid},func:(T)=>{
						$Api.put({f:Api.Gfp.a+'mov/oneField',jsBody:T.parentNode,func:function(Jr){
							if(Jr.errNo){ $1.Win.message(Jr); }
						}},td);
					}},td);
					}
				}
				else{ 
					$1.t('td',{textNode:_g(L.mType,$V.gfpMovType)},tr);
				}
				$1.t('td',{textNode:_g(L.wallId,$Tb.gfpWal)},tr);
				var td=$1.t('td',{textNode:$Str.money(bal),style:'fontWeight:bold; '+balCss},tr);
				L.editar='bal'; td.L=L;
				//td.ondblclick=function(){ Gfp.Mov.editLine(this.L,this); }
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.categId,$JsV.gfpCateg)},tr);
				$1.t('td',{textNode:L.credCuotas},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				var td=$1.t('td',0,tr);
				$Api.send({'class':'',textNode:'Borrar',DELETE:Api.Gfp.a+'mov', inputs:'mid='+L.mid,func:function(Jr2){
					$1.Win.message(Jr2);
				}},td);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'});
			cont.appendChild(tb);
		}
	}});
},
formOne:function(Pa,wMov){
		var jsF=$Api.JS.clsLN;
		var wm=$1.t('fieldset',0,wMov);
		var lg=$1.t('legend',{textNode:'Movimiento'},wm);
		$1.T.btnFa({fa:'fa_close',title:'Borrar movimiento',textNode:' Borrar',P:wm,func:function(T){ $1.delet(T.P); }},lg);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'input',type:'date','class':jsF,name:'docDate',value:Pa.docDate}},wm);
		$1.T.divL({wxn:'wrapx10',L:'cuenta',req:'Y',I:{tag:'select','class':'inpFocus '+jsF,name:'wallId',opts:$Tb.gfpWal}},divL);
		$1.T.divL({wxn:'wrapx10',req:'Y',L:'Tipo',I:{tag:'select','class':'mType '+jsF,name:'mType',opts:$V.gfpMovType,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx10',L:'T.C Tipo',req:'Y',I:{lTag:'select','class':'credType '+jsF,name:'credType',opts:$V.gfpCredType}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Valor',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil','class':jsF,name:'bal',min:0}},divL);
		$1.T.divL({wxn:'wrapx10',L:'T.C Cuotas',req:'Y',I:{lTag:'number',min:0,'class':'credCuotas '+jsF,name:'credCuotas'}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Prox.',_i:'Si marca SI permite identificar que forma parte del próximo corte, RECUERDE: debe desmarcarlas al finalizar el corte',req:'Y',I:{lTag:'select','class':'cred1Pay '+jsF,name:'cred1Pay',opts:$V.NY,noBlank:'Y'}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Categoria',req:'Y',I:{lTag:'select','class':jsF,name:'categId',opts:$JsV.gfpCateg}},divL);
		wm.classList.add($Api.JS.clsL);
		$1.T.divL({wxn:'wrapx4',L:'Detalles',I:{tag:'input',type:'text','class':jsF,name:'lineMemo'}},divL);

		var inp=$1.q('.inpFocus',wMov);
		function cambio(T){ //añ cambiar cuenta
			var wid=_gO(T.value,$Tb.gfpWal); 
			mTypeInp=$1.q('.mType',wm);
			mType=mTypeInp.parentNode;
			cType=$1.q('.credType',wm);
			cred1Pay=$1.q('.cred1Pay',wm).parentNode;
			cTypeTop=cType.parentNode;
			cType.AJs=null;
			cType.onchange=function(){
				tval=this.value+'';
				if(tval.match(/^(pago|pagoC|rediferidoP|otrosP)$/)){
					cType.AJs={mType:'I'};
				}
				else{ cType.AJs={mType:'G'}; }
			}
			cCuo=$1.q('.credCuotas',wm).parentNode;
			mTypeInp.classList.remove($Api.JS.clsLN);
			cred1Pay.style.display='none';
			mType.style.display='none';
			cTypeTop.style.display='none';
			cCuo.style.display='none';
			
			if(wid && wid.wallType=='TC'){
				$1.T.sel({reLoad:cType,opts:$V.gfpCredType});
				cred1Pay.style.display='';
				cTypeTop.style.display='';
				cCuo.style.display='';
			}
			else if(wid && wid.wallType=='CR'){
				cTypeTop.style.display='';
				cCuo.style.display='';
				$1.T.sel({reLoad:cType,opts:$V.gfpCredTypeCR});
			}
			else{
				mTypeInp.classList.add($Api.JS.clsLN);
				mType.style.display='';
			}
		}
		inp.onchange=function(){ cambio(this); }
		inp.focus();
		inp.classList.remove('inpFocus');
		cambio(inp);
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; var Pa=$M.read();
	cont.innerHTML='';
	if(!Pa.docDate){ Pa.docDate=$2d.today; }
	$Api.get({f:Api.Gfp.a+'mov',inputs:'mId='+Pa.mId,loadVerif:!Pa.mId,loade:cont, func:function(Jr){
		var wMov=$1.t('div',0,cont);
		Gfp.Mov.formOne(Pa,wMov);
		$1.t('br',0,cont);
		$1.btnGo({textNode:'Añadir Movimiento',func:()=>{ Gfp.Mov.formOne(Pa,wMov); },br:1},cont);
		var resp=$1.t('div',0,cont);
		$Api.send({POST:Api.Gfp.a+'mov',jsBody:cont,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Gfp.Mov.form({respBef:Jr2.text}); }
		}},cont);
	}});
},
quotes:function(P){
	$1.delet('#gfpMovQuotes');
	var wrap=$1.t('div');
	$1.Win.open(wrap,{winId:'gfpMovQuotes',winTitle:'Registros de Crédito',winSize:'full'});
	$Api.get({f:Api.Gfp.a+'mov/quotes',inputs:'mid='+P.mid,loade:wrap,func:(Jr)=>{
		Jq=Jr.L[0];
		if(!Jq.wallType.match(/^(TC|CR)$/)){
			return $Api.resp(wrap,{errNo:3,text:'El tipo de cuenta no maneja esta opcion'});
		}
		else if(!Jq.credType.match(/^(compra|avance|rediferido)$/)){
			return $Api.resp(wrap,{errNo:3,text:'El tipo de movimiento no maneja esta opcion'});
		}
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'input',type:'date',value:Jq.docDate,disabled:'disabled'}},wrap);
		$1.T.divL({wxn:'wrapx4',L:'Categoria',req:'Y',I:{lTag:'select',opts:$JsV.gfpCateg,value:Jq.categId,disabled:'disabled'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Valor',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil',value:Jq.bal*1,disabled:'disabled'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'T.C Cuotas',I:{lTag:'number',value:Jq.credCuotas,disabled:'disabled'}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',req:'Y',I:{lTag:'input',value:Jq.lineMemo,disabled:'disabled'}},wrap);
		var wm=$1.t('fieldset',0,wrap);
		$1.t('legend',{textNode:'Cuotas'},wm);
		var tb=$1.T.table(['#','Capital','Interes','Saldo A','Periodo','Borrar','$'],0,wm);
		tBody=$1.t('tbody',0,tb);
		$1.T.btnFa({btnCls:'btnB btn-drop fa fa_plusCircle',textNode:'Añadir',func:(T)=>{ trA({},tBody); }},wm);
		var resp=$1.t('div',0,wrap);
		$Api.JS.addF({name:'mid',value:P.mid},wrap);
		$Api.send({PUT:Api.Gfp.a+'mov/quotes',textNode:'Guardar',jsBody:wrap,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Gfp.Mov.quotes(P); }
		}},wrap);
		if(Jq.lineNum>0){
			var acum=Jq.bal*1;
			for(var i in Jr.L){
				acum = acum-Jr.L[i].capital*1;
				Jr.L[i].saldoCalc=acum;
				trA(Jr.L[i],tBody);
			}
		}
		
		function trA(L,tBody){
			jsF=$Api.JS.clsLN;
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'number',min:0,'class':jsF,name:'lineNum',value:L.lineNum},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'capital',value:L.capital*1},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'interes',value:L.interes*1},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'saldo',value:L.saldo*1},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'select',opts:Jr.Lc,'class':jsF,name:'pid',value:L.pid},td);
			td=$1.t('td',0,tr);
			$1.lineDel(L,{},td);
			td=$1.t('td',0,tr);
			if(L.id){
				$1.t('span',{textNode:$Str.money(L.saldoCalc),title:L.saldoCalc},td);
			}
		}
	}});
},

editForm:function(L){
	var wid='gfpMovLineEdit';
	$1.delet($1.q('#'+wid));
	var jsF=$Api.JS.cls;
	var wm=$1.t('div');
	var bal=(L.debBal>0)?L.debBal:'?';
	bal=(L.creBal>0)?L.creBal:bal;
	$1.Win.open(wm,{winTitle:'Modificar Movimiento',winSize:'medium'});
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha',req:'Y',I:{lTag:'input',type:'date','class':jsF,name:'docDate',value:L.docDate}},wm);
	$1.T.divL({wxn:'wrapx6',L:'Categoria',req:'Y',I:{lTag:'select','class':jsF,name:'categId',opts:$JsV.gfpCateg,value:L.categId}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Valor',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil','class':jsF,name:'bal',min:0,value:bal}},divL);
	$1.T.divL({wxn:'wrapx6',L:'T.C Cuotas',I:{lTag:'number',min:0,'class':'credCuotas '+jsF,name:'credCuotas',value:L.credCuotas}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Prox.',_i:'Si marca SI permite identificar que forma parte del próximo corte, RECUERDE: debe desmarcarlas al finalizar el corte',req:'Y',I:{lTag:'select','class':'cred1Pay '+jsF,name:'cred1Pay',opts:$V.NY,noBlank:'Y',value:L.cred1Pay}},divL);
	wm.classList.add($Api.JS.clsL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Detalles',I:{tag:'input',type:'text','class':jsF,name:'lineMemo',value:L.lineMemo}},wm);
	inp=$1.q('.credCuotas',wm);
	inp.AJs={mid:L.mid};
		var resp=$1.t('div',0,wm);
		$Api.send({PUT:Api.Gfp.a+'mov/one',textNode:'Modificar',jsBody:wm,loade:resp,func:function(Jr){
			$Api.resp(resp,Jr);
		}},wm);
},
editLine:function(L,td){
	var wid='gfpMovLineEdit';
	$1.delet($1.q('#'+wid));
	td.style.position='relative'; var jsF=$Api.JS.cls;
	var wi=$1.t('div',{id:wid,style:'position:absolute; top:90%; left:0; backgroundColor:#FFF; padding:6px; border:1px solid #000;'},td);
	if(L.editar=='bal'){
		var bal=(L.debBal>0)?L.debBal:L.creBal;
		var inp=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'bal',min:0,value:bal},wi);
	}
	inp.AJs={mid:L.mid};
		var resp=$1.t('div',0,wi);
		$Api.send({PUT:Api.Gfp.a+'mov/one',textNode:'Modificar',jsBody:wi,loade:resp,func:function(Jr){
			$Api.resp(resp,Jr);
		}},wi);
		$1.T.btnFa({textNode:'Cerrar',fa:'fa_close',func:function(){ $1.delet(wi); }},wi);
}
}

Gfp.Ctc={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal/bol', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Billetera','Bolsillo','Saldo']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:L.wallName},tr);
				$1.t('td',{textNode:L.wbName},tr);
				$1.t('td',{textNode:$Str.money(L.amount)},tr);
			}
		}
	}});
},
}
Gfp.Rep={
	tcp:(P)=>{
		$Api.Rep.base({f:Api.Gfp.a+'rep/tcp',inputs:$1.G.filter(),
		V_C:[{f:'credType',t:'Concepto',_g:$V.gfpCredType},{f:'bal',t:'Valor',fType:'$'}
		],
		V_LP:[
			{f:'wallName',t:'Cuenta'},{f:'compras',t:'Compras',fType:'$'},{f:'interes',t:'Interes',fType:'$'},{f:'otros',t:'Otros',fType:'$'},{f:'pagos',t:'Pagos',fType:'$'},
			{f:'openDate',t:'Apertura'},{f:'closeDate',t:'Cierre'},{f:'payDate',t:'Fecha Pago'}
		],
		V_M:[{f:'docDate',t:'Fecha'},{f:'categId',t:'Categoria',_g:$JsV.gfpCateg},{f:'credType',t:'Concepto',_g:$V.gfpCredType},{f:'bal',t:'Valor',fType:'$'},{f:'credCuotas',t:'Cuotas'},{f:'lineMemo',t:'Detalles'}
		],
		},$M.Ht.cont);
	},
	copen:(P)=>{
		$Api.Rep.base({f:Api.Gfp.a+'rep/copen',inputs:$1.G.filter(),
		V_G:[
		{func:(L,td)=>{
			$1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
				Gfp.Mov.quotes(T.P);
			}},td);
		}},
		{f:'wallName',t:'Cuenta'},{f:'docDate',t:'Fecha'},
		{f:'bal',t:'Valor',fType:'$'},{f:'credInt',t:'Int. Pagado',fType:'$'},{f:'credSal',t:'Pendiente',fType:'$'},
		{f:'categId',t:'Categoria',_g:$JsV.gfpCateg},{f:'credType',t:'Concepto',_g:$V.gfpCredType},{f:'credCuotas',t:'Cuotas'},{f:'credCuoNum',t:'No. Cuota'},
		{f:'lineMemo',t:'Detalles'},
		],
		},$M.Ht.cont);
	},
}
Gfp.Fx={
	cuentaCorte:(wrap,feVal)=>{
		sels=$1.q('.gfpCuentaCorte',wrap,'all');
		$1.onchange(sels[0],function(tv,TT){
			$1.T.sel({reLoad:sels[1],opts:[]});
			if(feVal && feVal()==true){}//si es true, evito reactualizar
			else{	tc=_gO(tv,$Tb.gfpWal);
				if(tc.wallType=='TC'){
					$Api.get({f:Api.Gfp.a+'wal/ctc',inputs:'wallId='+tv,func:(R)=>{
						$1.T.sel({reLoad:sels[1],opts:R,optsFTxt:(X)=>{
							sep=X.value.split(' a ');
							return $2d.f(sep[0],'mmm d')+' a '+$2d.f(sep[1],'mmm d');
						}});
					}});
				}
			}
	});
	},
	quotes:(L,td)=>{
		return $1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
			Gfp.Mov.quotes(T.P);
			}},td);
	}
}
$M.liAdd('gfp',[
{_lineText:'Finanzas Personals'},
{k:'gfp.wal',t:'Cuentas',kau:'gfp',mdlActive:'gfp',ini:{fNew:Gfp.Wal.form,f:'gfp.wal', gyp:Gfp.Wal.get}},
{k:'gfp.cre',t:'Creditos',kau:'gfp',mdlActive:'gfp',ini:{fNew:Gfp.Cre.form,f:'gfp.cre', gyp:Gfp.Cre.get}},
{k:'gfp.mov',t:'Movimientos',kau:'gfp',mdlActive:'gfp',ini:{btnGo:'gfp.movForm',f:'gfp.mov', gyp:Gfp.Mov.get}},
{k:'gfp.movForm',t:'Registrar Movimiento',kau:'gfp',mdlActive:'gfp',ini:{g:Gfp.Mov.form}},
]);

$M.liRep('gfp',[
	{_lineText:'_REP'},
	{k:'gfpRep.tcp',t:'Control Periodos Tarjetas', kauAssg:'gfp',ini:{f:'gfpRep.tcp'}},
	{k:'gfpRep.copen',t:'Compras a Cuotas', kauAssg:'gfp',ini:{f:'gfpRep.copen'}},
],{repM:['gfp']});

$JsV._i({kMdl:'a1',kObj:'gfpCateg',mdl:'a1',liTxtG:'Categorias - I/G',liTxtF:'Categoria - I/G'});
$JsV._i({kMdl:'a1',kObj:'gfpCategCR',mdl:'a1',liTxtG:'Categorias Creditos',liTxtF:'Categoria Credito'});