/* GeSN! 
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
Api.Crm = {b:'/1/crm/',sysd:'/1/crm/sysd/'};
$Mdl.CRM='Y';
$Doc.a['crmCpr']={a:'crmCpr.view',kl:'prsId',kt:'name'};
$Doc.a['crmCrd']={a:'crmCrd.view',kl:'cardId',kt:'cardName'};
$V.crmPriority=[{k:'N',v:'Normal'},{k:'M',v:'Media'},{k:'H',v:'Alta'}]

$Mdl.CRM={};
$oB.pus('cpr',$Opts,[
{ico:'fa fa-bolt',textNode:' Gestionar',hrefTo:['crmCpr.view','prsId']}
]);
$oB.pus('crd',$Opts,[
{ico:'fa fa-bolt',textNode:' Gestionar',hrefTo:['crmCrd.view','cardId']}
]);

$oB.pus('cpr',$Mdl.itemLi,[
{fa:'fa-bolt',textNode:' Gestionar',hrefTo:['crmCpr.view','prsId']}
]);
$oB.pus('crd',$Mdl.itemLi,[
{fa:'fa-bolt',textNode:' Gestionar',hrefTo:['crmCrd.view','cardId']}
]);

$M.add([{liA:'crm',folder:'Organizador',
	L:[
	{t:'Acceso Base Módulo',k:'crmBasic'}
]
}]);

_Fi['sysd.crmTypes']=function(wFilt){
	$Filt.filtFunc=function(){ $Crm.Sysd.Types.get(); }
	$Filt.form({cont:wFilt,Li:[
	{t:'Objecto',tag:'select',name:'prp1',opts:$V.crmSysdPrp1},
	{t:'Valor',tag:'text',name:'value'}
	]});
}

$M.li['crmCrd']={t:'Clientes', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:function(){ $Crm.Crd.get({cardType:'C'}); } }); }};
$M.li['crmCrd.view']={t:'Perfil de Cliente',noTitle:'Y', kau:'crmBasic', func:function(){ $M.Ht.ini({func_cont:$Crm.Crd.profile}); }};
$M.li['crmCpr']={t:'Contactos', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:$Crm.Cpr.get}); }};
$M.li['crmCpr.view']={t:'Perfil de Contacto',noTitle:'Y', kau:'crmBasic', func:function(){ $M.Ht.ini({func_cont:$Crm.Cpr.profile}); }};
$M.li['crmNov']={t:'Novedades', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:$Crm.Nov.profile}); }};
$M.li['crmTask']={t:'Tareas', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:$Crm.Task.profile}); }};
$M.li['crmEvents']={t:'Eventos', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:$Crm.Events.profile}); }};
$M.li['crmNote']={t:'Notas', kau:'crmBasic', func:function(){ $M.Ht.ini({func_pageAndCont:$Crm.Notes.profile}); }};

$M.li['crmAgenda']={t:'Agenda', kau:'crmBasic', func:function(){ $M.Ht.ini({func_cont:$Crm.Agenda.get}); }};

$Crm={
profile:'N',
tabTop:function(P,cont){
var ul=$1.t('div',0,cont);
	var uriTo=P.uriTo; /* crmCrd.view, {pars} */
	var uriToP=(P.uriToP)?P.uriToP+',':'';
	var Li=[]; var mAll=(P.m=='all');
	if(mAll || P.m.match(/task/)){
		Li.push({k:'task',textNode:'Tareas',href:$M.to(uriTo,uriToP+'tab:task','r'),P:P.Ps,func:function(T){ $Crm.Task.get(T.P); },faIco:'fa fa-list'});
	}
	if(mAll || P.m.match(/nov/)){
		Li.push({k:'nov',textNode:'Novedad',href:$M.to(uriTo,uriToP+'tab:nov','r'),P:P.Ps,func:function(T){ $Crm.Nov.get(T.P); },faIco:'fa fa-feed'});
	}
	if(mAll || P.m.match(/note/)){
		Li.push({k:'note',textNode:'Notas',href:$M.to(uriTo,uriToP+'tab:notes','r'),P:P.Ps,func:function(T){ $Crm.Notes.get(T.P); },faIco:'fa fa-sticky-note-o'});
	}
	if(mAll || P.m.match(/event/)){
		Li.push({k:'event',textNode:'Eventos',href:$M.to(uriTo,uriToP+'tab:events','r'),faIco:{fa:'fa fa-calendar-check-o',color:'blue'},P:P.Ps,func:function(T){ $Crm.Events.get(T.P); }});
	}
	for(var i in Li){
		var li=$1.t('span',0,ul);
		Li[i].style='margin-right:0.25rem';
		var a=$1.t('a',Li[i],li);
		if(Li[i].func){ a.func=Li[i].func; }
		a.onclick=function(){
			$Filt.form({clear:1}); /*clear filter */
			TF=this; $M.uriFunc=function(){ TF.func(TF); }; 
		}
	}
	switch(P.Pa.tab){
		default : $Crm.Task.get(P.Ps); break;
		case 'nov' : $Crm.Nov.get(P.Ps); break;
		case 'deals': $Crm.Deals.get(P.Ps); break;
		case 'events': $Crm.Events.get(P.Ps); break;
		case 'notes': $Crm.Notes.get(P.Ps); break;
	}
}
};
/* Almacen cache $ChT crmTask[] */
$Crm._={top:0,m:0,cont:0,}

$Crm.Cpr={
get:function(P,cont){  P=(P)?P:{};
	var cont=(cont)?cont:$M.Ht.cont;
	$Filt.filtFunc=function(){ P.filter='N'; $Crm.Cpr.get(P); }
	if(P.filter!='N'){
		$Filt.form({cont:$M.Ht.filt,active:'Y',Li:[
		{t:'Nombre',tag:'input',name:'P.name(E_like3)'},
		{t:'Cargo',tag:'select',name:'P.position(E_igual)',opts:$TbV['crdCpr.position']}
		]});
	}
	var vPost=$Filt.get($M.Ht.filt);
	$Api.get({f:Api.Crm.b+'cpr',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['Nombre','Cargo','Celular','Tel. 1','Tel. 2','Email','Empresa']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			$Doc.href('crmCpr',L,td);
			$1.t('td',{textNode:$TbV._g('crdCpr.position',L.position)},tr);
			$1.t('td',{textNode:L.cellular},tr);
			$1.t('td',{textNode:L.tel1},tr);
			$1.t('td',{textNode:L.tel2},tr);
			$1.t('td',{textNode:L.email},tr);
			var td=$1.t('td',0,tr);
			$Doc.href('crd',L,td);
		}
	}});
},
profile:function(){
	var Pa=$M.read(); $Crm.profile='cpr';
	var cont=$M.Ht.cont;
	var vPost ='prsId='+$M.Pa.prsId;
	$Api.get({f:Api.Crm.b+'cpr/profile',inputs:vPost, loade:cont, func:function(Jr){
		var divMain=$1.t('div',{'class':'__crmCprProfA',style:' border-bottom:0.0625rem solid blue; padding:0 0.25rem; margin-bottom:0.25rem;'});
		$Filt.ini({h1:Jr.name,main:divMain},cont);
		$1.t('div',{'class':'clear'},cont);
		var Ps={profile:'profile=cpr&prsId='+Jr.prsId};
		var fP={m:'all',Pa:Pa,Ps:Ps,uriTo:'crmCpr.view',uriToP:'prsId:'+Pa.prsId};
		$Crm.tabTop(fP,divMain);
		}});
}
}

$Crm.Crd={
get:function(P,cont){ P=(P)?P:{};
	var cont=(cont)?cont:$M.Ht.cont;
	var fApi=Api.Crm.b+'crd'
	$Filt.filtFunc=function(){ P.filter='N'; $Crm.Crd.get(P); }
	if(P.filter!='N'){
		$Filt.form({cont:$M.Ht.filt,active:'Y',Li:[
		{t:'Nombre Cliente',tag:'input',name:'A.cardName(E_like3)'},
		{t:'Activo',tag:'select',name:'A.active(E_igual)',opts:$V.active,selected:'active'}
		]});
	}
	var vPost='A.cardType='+P.cardType+'&'+$Filt.get($M.Ht.filt);
	$Api.get({f:fApi,inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['Nombre','Código','Responsable','No. Doc.']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			$Doc.href('crmCrd',L,td);
			$1.t('td',{textNode:L.cardCode},tr);
			$1.t('td',{textNode:_g(L.slpId,$Tb.oslp)},tr);
			$1.t('td',{textNode:L.licTradNum},tr);
		}
	}});
},
profile:function(){
	var Pa=$M.read(); $Crm.profile='crd';
	console.log(Pa);
	var cont=$M.Ht.cont;
	var vPost ='cardId='+Pa.cardId;
	$Api.get({f:Api.Crm.b+'crd/profile',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var divMain=$1.t('div',{'class':'__crmCprProfA',style:' border-bottom:0.0625rem solid blue; padding:0 0.25rem; margin-bottom:0.25rem;'});
		$Filt.ini({h1:Jr.cardName,main:divMain},cont);
		$1.t('div',{'class':'clear'},cont);
		var Ps={profile:'profile=crd&cardId='+Jr.cardId};
		var fP={m:'all',Pa:Pa,Ps:Ps,uriTo:'crmCrd.view',uriToP:'cardId:'+Pa.cardId};
		$Crm.tabTop(fP,divMain);
	}});
}
}

$Crm.E={
relData:function(Jr,P){
	Jr.prsId=(Jr.prsId)?Jr.prsId:P.prsId;
	Jr.prsName=(Jr.name)?Jr.name:P.name;
	Jr.cardId=(Jr.cardId)?Jr.cardId:P.cardId;
	Jr.cardName=(Jr.cardName)?Jr.cardName:P.cardName;
	return Jr;
},
callF:function(P,bk){
	if(P.o){ 
		var WHu=(P.Met=='PUT')?{f:'gid',v:P.Jr.gid}:false;
		switch(P.obj){
			case 'task': $ChT._a('crmNovL',P.o,WHu); $Crm.Task.drawRow(P.o); break;
			case 'nov': $ChT._a('crmNovL',P.o,WHu); $Crm.Nov.drawRow(P.o); break;
			case 'note': $ChT._a('crmNovL',P.o,WHu); $Crm.Notes.drawRow(P.o); break;
			case 'deal': $ChT._a('crmNovL',P.o,WHu); $Crm.Deals.drawRow(); break;
			case 'event': $ChT._a('crmNovL',P.o,WHu); $Crm.Events.drawRow(P.o); break;
		}
	}
	$1.delet(bk);
},
inpRels:function(Jr,P,pare){
	if(P.line=='prsAndcard'){
		var sea=$crd.Fx.inpSeaPrs({vPost:'prsId='+Jr.prsId,value:Jr.prsName});
		var divL=$1.T.divL({divLine:1,L:'Contacto',wxn:'wrapx2',Inode:sea},pare);
		var sea=$crd.Fx.inpSeaCrd({vPost:'cardId='+Jr.cardId,value:Jr.cardName});
		$1.T.divL({L:'Socio Negocios',wxn:'wrapx2',Inode:sea},divL);
	}
},
};

$Crm.Task={
profile:function(){
	var cont=$M.Ht.cont; $1.clear(cont);
	$Crm.profile='task';
	$Filt.ini({main:false},cont);
	$Crm.Task.get({profile:'profile=task'});
},
get:function(P){
	$Filt.filtFunc=function(){ P.filter='N'; $Crm.Task.get(P); }
	if(P.filter!='N'){
		var tLis=[
		{t:'Completas',tag:'YN',name:'A.completed(E_igual)',selected:'N',noBlank:'Y'},
		{t:'Asunto',tag:'input',name:'A.title(E_like3)'},
		{t:'Responsable',tag:'userAssg',name:'A.userAssg(E_igual)'},
		{t:'Tipo',tag:'select',name:'A.oType(E_igual)',opts:$V.crm_taskTypes},
		{t:'Gestión',tag:'select',name:'A.status(E_igual)',opts:$V.crm_taskStatus},
		{t:'Vencimiento',TA:[{tag:'date',name:'A.dueDate(E_mayIgual)'},{tag:'date',name:'A.dueDate(E_menIgual)'}]},
		{t:'Creada',TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]},
		];
		if($Crm.profile.match(/^(task|note|nov|event)$/)){
			tLis.push({t:'Contacto',tag:'input',name:'P.name(E_like3)'});
			tLis.push({t:'Socio',tag:'input',name:'C.cardName(E_like3)'});
		}
		else if($Crm.profile=='crd'){
			tLis.push({t:'Contacto',tag:'input',name:'P.name(E_like3)'});
		}
		else if($Crm.profile=='cpr'){
			tLis.push({t:'Socio',tag:'input',name:'C.cardName(E_like3)'});
		}
		$Filt.form({active:'Y',Li:tLis});
	}
	var wList=$Filt.wList; 
	var vPost=(P.profile)?P.profile+'&':'';
	vPost +=$Filt.get($Filt.filter);
	$Api.get({f:Api.Crm.b+'task',inputs:vPost, loade:wList, func:function(Jr){
		$Filt.btnFa({fa:'fa fa-plus',textNode:'Nueva Tarea',func:function(){ $Crm.Task.form(Jr); }});
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		else{
			$ChT._i('crmNovL');
			for(var i in Jr.L){ $ChT._a('crmNovL',Jr.L[i]); }
			var Lx=$ChT._g('crmNovL','No se encontraron resultados');
			if(Lx._err){ $Api.resp(wList,Lx); return false; }
			for(var i in Lx){ $Crm.Task.drawRow(Lx[i],wList); }
		}
	}});
},
form:function(P){ var P=(P)?P:{};
	var wrap=$1.t('div',0); var jsF='jsFields';
	var vPost='profile='+$Crm.profile;
vPost +=(P.gid)?'&gid='+P.gid:'';
	$Api.get({f:Api.Crm.b+'task/form',loadVerif:!P.gid, inputs:vPost,loade:wrap,func:function(Jr){
		Jr=$Crm.E.relData(Jr,P);
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Tipo',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'oType'},opts:$V.crm_taskTypes,selected:Jr.oType}},wrap);
		$1.T.divL({L:'Prioridad',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'priority'},opts:$V.crmPriority,noBlank:'Y',selected:Jr.priority}},divL);
		$1.T.divL({L:'Gestión',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'status'},opts:$V.crm_taskStatus,selected:Jr.status}},divL);
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Asunto',wxn:'wrapx6_1',I:{tag:'input',type:'text','class':jsF+' __asunt',name:'title',value:Jr.title}},wrap);
		$1.T.divL({L:'Agendar',wxn:'wrapx6',I:{tag:'select','class':jsF,name:'calId',opts:$Tb.crmOcal,noBlank:'Y',selected:Jr.calId,opt1:{k:0,t:'No'}}},divL);
		var divL=$1.T.divL({divLine:1,L:'Responsable',wxn:'wrapx2',I:{tag:'select',sel:{'class':jsF,name:'userAssg'},opts:$Tb.ousr,selected:Jr.userAssg}},wrap);
		var node=$2dW.btn({'class':jsF,name:'endDate',f:'mmm d H:i',divBlock:'Y',I:{style:'width:100%;'},value:Jr.endDate+' '+Jr.endDateAt},0,{time:'Y'});
		$1.T.divL({L:'Vencimiento',wxn:'wrapx2',Inode:node},divL);
		var divL=$1.T.divL({L:'Descripción',wxn:'wrapx1',I:{tag:'textarea','class':jsF,name:'shortDesc',textNode:Jr.shortDesc}},wrap);
		$Crm.E.inpRels(Jr,{line:'prsAndcard'},wrap);
		var divL=$1.t('div',0,wrap);
		var inpRep=$1.t('input',{type:'hidden','class':jsF,name:'repeatKey'},divL);
		$DateRep.form({repeatKey:Jr.repeatKey,func:function(tk){
			inpRep.value=tk;
		}},divL);
		var resp=$1.t('div',0,wrap);
		var Met=(Jr.gid)?'PUT':'POST';
		$Api.send({Met:Met,f:Api.Crm.b+'task',loade:resp,vPost:vPost,inputsFrom:wrap,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ $Crm.E.callF({Met:Met,obj:'task',Jr:Jr,o:Jr2._o},bk); }
		}},wrap);
	}});
	var bk=$1.Win.open(wrap,{winSize:'medium',onBody:1,winTitle:'Tarea'});
},
drawRow:function(L,wList){
	L=$Crm.Ht.vD(L,{tabOn:'task'});
	var wList=(wList)?wList:$Filt.wList;
	var ide='_crmEventWrapId_'+L.gid;
	var div=$1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'});
	if(old=$1.q('#'+ide)){ wList.replaceChild(div,old); }
	else{ wList.appendChild(div); }
	var ck=$1.T.check({name:'completed','class':'jsfields',value:L.completed,P:L,func:function(T){
		vPostS='gid='+T.P.gid+'&'+$1.G.inputs(T.parentNode);
		$Api.put({f:Api.Crm.b+'task/markCompleted',inputs:vPostS,func:function(Jr2){}});
	}},div);
	$Crm.Ht.open(L,{o:'task',pare:div});
	$Crm.Ht.edit(L,{o:'task',pare:div});
	$Crm.Ht.sendCopy(L,{pare:div});
	$Crm.Ht.type(L,div);
	$Crm.Ht.prio(L,div);
	$Crm.Ht.status(L,div);
	$Crm.Ht.endDate(L,div);
	$Crm.Ht.userAssg(L,div);
	$Crm.Ht.profileBot(L,div);
	$Crm.Ht.nums(L,div,{o:'task'});
	$Crm.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=(P.cont)?P.cont:$M.Ht.cont;
	var cont=$1.t('div',0); var jsF='jsFields';
var vPost='gid='+P.gid;
	$Api.get({f:Api.Crm.b+'task/view',inputs:vPost,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tipo',v:_g(Jr.oType,$V.crm_taskTypes)},
		{wxn:'tbf_x4',t:'Prioridad',v:_g(Jr.priority,$V.crmPriority)},
		{wxn:'tbf_x4',t:'Vencimiento',v:$2d.f(Jr.endDate+' '+Jr.endDateAt,'mmm d')},
		{wxn:'tbf_x4',t:'Responsable',v:_g(Jr.userAssg,$Tb.ousr)},
		{line:1,wxn:'tbf_x1',t:'Asunto',v:Jr.title},
		{line:1,wxn:'tbf_x2',t:'Contacto',v:Jr.name},
		{wxn:'tbf_x2',t:'Socio Negocios',v:Jr.cardName},
		{line:1,wxn:'tbf_x1',t:'Detalles',v:Jr.shortDesc,T:{'class':'pre'}},
		],cont);
		$Crm.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Tarea'});
},
}

$Crm.sendTaskCopy=function(P){
	var wrap=$1.t('div');
	if(!P.gid){ $Api.resp(wrap,{text:'No se ha definido Id de tarea'}); }
	else{
		$1.t('h5',{textNode:P.title},wrap);
		$Api.JS.addF({name:'gid',value:P.gid},wrap);
		var tb=$1.T.table(['Email',''],0,wrap);
		var tBody=$1.t('tbody',0,tb);
		function trA(){
			var jsF=$Api.JS.clsAr;
			var tr=$1.t('tr',0,tBody);
			$1.t('input',{type:'text','class':jsF,name:'email'},$1.t('td',0,tr));
			$1.T.btnFa({fa:'fa_close',func:function(T){ $1.delet(T.parentNode.parentNode); }},$1.t('td',0,tr));
		}
		$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir',func:function(){
			var nds=$1.q('tr',wrap,'all');
			if(nds.length>2){ $Api.resp(resp,{text:'No se puede enviar a más de 2 contactos'}); }
			else{ trA(); }
		}},wrap);
		var resp=$1.t('div',0,wrap);
		$Api.send({POST:Api.Crm.b+'task/sendCopy',jsBody:wrap,loade:resp,func:function(Jr){
			if(Jr.errNo){ $Api.resp(resp,Jr); }
			else{ $Api.resp(wrap,Jr); }
		}},wrap);
		trA();
	}
	$1.Win.open(wrap,{winTitle:'Enviar Copia de Tarea a Correo',winSize:'medium'});
}

$Crm.Notes={
profile:function(){
	var cont=$M.Ht.cont; $1.clear(cont);
	$Crm.profile='note';
	$Filt.ini({main:false},cont);
	$Crm.Notes.get({profile:'profile=note'});
},
get:function(P){
	$Filt.filtFunc=function(){ $Crm.Notes.get(P); }
	$Filt.form({Li:[
	{t:'Tipo',tag:'select',name:'A.oType(E_igual)',opts:$V.crm_noteTypes},
	{t:'Asunto',tag:'input',name:'A.title(E_like3)'},
	{t:'Creada',TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]},
	]});
	var wList=$Filt.wList; 
	var vPost=(P.profile)?P.profile+'&':'';
	vPost +=$Filt.get($Filt.filter);
	$Api.get({f:Api.Crm.b+'notes',inputs:vPost, loade:wList, func:function(Jr){
		$Filt.btnFa({fa:'fa fa-plus',textNode:'Nueva Nota',func:function(){ $Crm.Notes.form(Jr); }});
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		$ChT._i('crmNovL');
		for(var i in Jr.L){ $ChT._a('crmNovL',Jr.L[i]); }
		var Lx=$ChT._g('crmNovL','No se encontraron resultados');
		if(Lx._err){ $Api.resp(wList,Lx); return false; }
		for(var i in Lx){ $Crm.Notes.drawRow(Lx[i],wList); }
	}});
},
form:function(P){
	var wrap=$1.t('div',0); var jsF='jsFields';
	var vPost=(P.gid)?'gid='+P.gid:'';
	$Api.get({f:Api.Crm.b+'notes/form',loadVerif:!P.gid, inputs:vPost,loade:wrap,func:function(Jr){
		Jr=$Crm.E.relData(Jr,P);
		Jr.doDate=(Jr.doDate)?Jr.doDate:$2d.today;
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Tipo',wxn:'wrapx4',I:{tag:'select','class':jsF,name:'oType',selected:Jr.oType,opts:$V.crm_noteTypes}},wrap);
		$1.T.divL({L:'Fecha',wxn:'wrapx4',I:{tag:'input',type:'date','class':jsF,name:'doDate',value:Jr.doDate}},divL);
		$1.T.divL({divLine:1,req:'Y',L:'Titulo',wxn:'wrapx1',I:{tag:'input',type:'text','class':jsF+' __asunt',name:'title',value:Jr.title}},wrap);
		$1.T.divL({divLine:1,L:'Cuerpo de la Nota',wxn:'wrapx1',I:{tag:'textarea','class':jsF,name:'description',textNode:Jr.description}},wrap);
		$Crm.E.inpRels(Jr,{line:'prsAndcard'},wrap);
		var resp=$1.t('div',0,wrap);
		var Met=(Jr.gid)?'PUT':'POST';
		$Api.send({Met:Met,f:Api.Crm.b+'notes',loade:resp,vPost:vPost,inputsFrom:wrap,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){$Crm.E.callF({Met:Met,obj:'note',Jr:Jr,o:Jr2._o},bk); }
		}},wrap);
	}});
	var bk=$1.Win.open(wrap,{winSize:'medium',onBody:1,winTitle:'Crear Tarea'});
},
drawRow:function(L,wList){
	L=$Crm.Ht.vD(L,{tabOn:'note'});
	var wList=(wList)?wList:$Filt.wList;
	var ide='_crmWrap_gid_'+L.gid;
	var div=$1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'});
	if(old=$1.q('#'+ide)){ wList.replaceChild(div,old); }
	else{ wList.appendChild(div); }
	$Crm.Ht.open(L,{o:'note',pare:div});
	$Crm.Ht.edit(L,{o:'note',pare:div});
	$Crm.Ht.type(L,div);
	$Crm.Ht.doDate(L,div);
	$Crm.Ht.userAssg(L,div);
	$Crm.Ht.profileBot(L,div);
	$Crm.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=(P.cont)?P.cont:$M.Ht.cont;
	var cont=$1.t('div',0); var jsF='jsFields';
var vPost='gid='+P.gid;
	$Api.get({f:Api.Crm.b+'notes/view',inputs:vPost,loade:cont,func:function(Jr){
		$1.Tb.trCols([
		{t:'Tipo',v:_g(Jr.oType,$V.crm_noteTypes)},
		{t:'Fecha',v:$2d.f(Jr.doDate,'mmm d')},
		{t:'Contacto',v:Jr.name},
		{t:'Socio',v:Jr.cardName},
		{v:$1.t('div',{'class':'pre',textNode:Jr.description}),cs:2}
		],{cols:2},cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Nota'});
},
}

$Crm.Nov={
profile:function(){
	var cont=$M.Ht.cont; $1.clear(cont);
	$Crm.profile='nov';
	$Filt.ini({main:false},cont);
	$Crm.Nov.get({profile:'profile=nov',divBot:'nov'});
},
get:function(P){
	$Filt.filtFunc=function(){ $Crm.Nov.get(P); }
	$Filt.form({active:'Y',Li:[
		{t:'Fecha creada',TA:[{tag:'date',name:'A.dateC(E_mayIgual)',value:$2d.f('next-7','Y-m-d')},{tag:'date',name:'A.dateC(E_menIgual)'}]},
		{t:'Fecha Registro',TA:[{tag:'date',name:'A.doDate(E_mayIgual)'},{tag:'date',name:'A.doDate(E_menIgual)'}]},
	{t:'Tipo',tag:'select',name:'A.oType(E_igual)',opts:$V.crm_novTypes},
	{t:'Descripción',tag:'input',name:'A.shortdesc(E_like3)'}
	]});
	var wList=$Filt.wList; 
	var vPost=(P.profile)?P.profile+'&':'';
	vPost +=$Filt.get($Filt.filter);
	$Api.get({f:Api.Crm.b+'nov',inputs:vPost, loade:wList, func:function(Jr){
		$Filt.btnFa({fa:'fa fa-plus',textNode:'Nueva Novedad',func:function(){ $Crm.Nov.form(Jr); }});
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		$ChT._i('crmNovL');
		for(var i in Jr.L){ $ChT._a('crmNovL',Jr.L[i]); }
		var Lx=$ChT._g('crmNovL','No se encontraron resultados');
		if(Lx._err){ $Api.resp(wList,Lx); return false; }
		for(var i in Lx){ $Crm.Nov.drawRow(Lx[i],wList,P); }
	}});
},
form:function(P){
	var wrap=$1.t('div',0); var jsF='jsFields';
	var vPost=(P.gid)?'gid='+P.gid:'';
	$Api.get({f:Api.Crm.b+'nov/form',loadVerif:!P.gid, inputs:vPost,loade:wrap,func:function(Jr){
		Jr=$Crm.E.relData(Jr,P);
		Jr.doDate=(Jr.doDate)?Jr.doDate:$2d.today;
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Tipo',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'oType'},opts:$V.crm_novTypes,selected:Jr.oType}},wrap);
		$1.T.divL({L:'Fecha',wxn:'wrapx4',req:'Y',I:{tag:'input',type:'date','class':jsF,name:'doDate',value:Jr.doDate}},divL);
		$1.T.divL({L:'Prioridad',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'priority'},opts:$V.crmPriority,noBlank:'Y',selected:Jr.priority}},divL);
			$1.T.divL({L:'Responsable',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'userAssg'},opts:$Tb.ousr,selected:Jr.userAssg}},divL);
		$1.T.divL({divLine:1,L:'Descripción Novedad',wxn:'wrapx1',I:{tag:'textarea','class':jsF,name:'shortDesc',textNode:Jr.shortDesc}},wrap);
		$Crm.E.inpRels(Jr,{line:'prsAndcard'},wrap);
		var resp=$1.t('div',0,wrap);
		var Met=(Jr.gid)?'PUT':'POST';
		$Api.send({Met:Met,f:Api.Crm.b+'nov',loade:resp,vPost:vPost,inputsFrom:wrap,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){$Crm.E.callF({Met:Met,obj:'nov',Jr:Jr,o:Jr2._o},bk); }
		}},wrap);
	}});
	var bk=$1.Win.open(wrap,{winSize:'medium',onBody:1,winTitle:'Formulario Novedad'});
},
drawRow:function(L,wList){
	L=$Crm.Ht.vD(L,{tabOn:'nov'});
	var wList=(wList)?wList:$Filt.wList;
	var ide='_crmWrap_gid_'+L.gid;
	var div=$1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'});
	if(old=$1.q('#'+ide)){ wList.replaceChild(div,old); }
	else{ wList.appendChild(div); }
	var tag=$Crm.Ht.type(L,div);
	$Crm.Ht.open(L,{o:'nov',tag:tag});
	$Crm.Ht.edit(L,{o:'nov',pare:div});
	$Crm.Ht.doDate(L,div);
	$Crm.Ht.prio(L,div);
	$Crm.Ht.profileBot(L,div);
	$Crm.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=(P.cont)?P.cont:$M.Ht.cont;
	var cont=$1.t('div',0); var jsF='jsFields';
var vPost='gid='+P.gid;
	$Api.get({f:Api.Crm.b+'nov/view',inputs:vPost,loade:cont,func:function(Jr){
		$1.Tb.trCols([
		{t:'Tipo',v:_g(Jr.oType,$V.crm_novTypes)},
		{t:'Fecha',v:$2d.f(Jr.doDate,'mmm d')},
		{t:'Contacto',v:Jr.name},
		{t:'Socio',v:Jr.cardName},
		{t:'Descripción',tCs:2,v:false},
		{v:$1.t('div',{'class':'pre',textNode:Jr.shortDesc}),cs:2}
		],{cols:2},cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Novedad'});
},
}

$Crm.Events={
profile:function(){
	var cont=$M.Ht.cont; $1.clear(cont);
	$Crm.profile='event';
	$Filt.ini({main:false},cont);
	$Crm.Events.get({profile:'profile=event'});
},
get:function(P){
	$Filt.filtFunc=function(){ $Crm.Events.get(P); }
	$Filt.form({Li:[
	{t:'Asunto',tag:'input',name:'A.title(E_like3)'},
	{t:'Prioridad',tag:'select',name:'A.priority(E_in)',opts:$V.crmPriority,multiple:'Y'},
	{t:'Tipo',tag:'select',name:'A.oType(E_igual)',opts:$V.crm_eventTypes},
	{t:'Fecha Programado',TA:[{tag:'date',name:'A.doDate(E_mayIgual)'},{tag:'date',name:'A.endDate(E_menIgual)'}]},
	{t:'Creada',TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]},
	]});
	var wList=$Filt.wList; 
	var vPost=(P.profile)?P.profile+'&':'';
	vPost +=$Filt.get($Filt.filter);
	$Api.get({f:Api.Crm.b+'events',inputs:vPost, loade:wList, func:function(Jr){
		$Filt.btnFa({fa:'fa fa-plus',textNode:'Nuevo Evento',func:function(){ $Crm.Events.form(Jr); }});
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		else{
			$ChT._i('crmNovL');
			for(var i in Jr.L){ $ChT._a('crmNovL',Jr.L[i]); }
			var Lx=$ChT._g('crmNovL','No se encontraron resultados');
			if(Lx._err){ $Api.resp(wList,Lx); return false; }
			for(var i in Lx){ $Crm.Events.drawRow(Lx[i],wList); }
		}
	}});
},
form:function(P){
	var wrap=$1.t('div',0); var jsF='jsFields';
	var vPost=(P.gid)?'gid='+P.gid:'';
	$Api.get({f:Api.Crm.b+'events/form',loadVerif:!P.gid, inputs:vPost,loade:wrap,func:function(Jr){
		Jr=$Crm.E.relData(Jr,P);
		Jr.doDate=Jr.doDate+' '+Jr.doDateAt;
		Jr.endDate=Jr.endDate+' '+Jr.endDateAt;
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Asunto Evento',wxn:'wrapx6_1',I:{tag:'input',type:'text','class':jsF+' __asunt',name:'title',value:Jr.title}},wrap);
		$1.T.divL({L:'Agendar',wxn:'wrapx6',I:{tag:'select','class':jsF,name:'calId',opts:$Tb.crmOcal,noBlank:'Y',selected:Jr.calId,opt1:{k:0,t:'No'}}},divL);
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Tipo',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'oType'},opts:$V.crm_eventTypes,selected:Jr.oType}},wrap);
		$1.T.divL({L:'Prioridad',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'priority'},opts:$V.crmPriority,noBlank:'Y',selected:Jr.priority}},divL);
		$1.T.divL({L:'Responsable',wxn:'wrapx2',I:{tag:'select',sel:{'class':jsF,name:'userAssg'},opts:$Tb.ousr,selected:Jr.userAssg}},divL);
		var node=$2dW.btn({'class':jsF,name:'doDate',f:'mmm d H:i',divBlock:'Y',I:{style:'width:100%;'},value:Jr.doDate},0,{time:'Y'});
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Desde',wxn:'wrapx2',Inode:node},wrap);
			var node=$2dW.btn({'class':jsF,name:'endDate',f:'mmm d H:i',divBlock:'Y',I:{style:'width:100%;'},value:Jr.endDate},0,{time:'Y'});
		$1.T.divL({divLine:1,L:'Lugar',wxn:'wrapx1',I:{tag:'input',type:'text','class':jsF,name:'wLocation',value:Jr.wLocation}},wrap);
		$1.T.divL({req:'Y',L:'Hasta',wxn:'wrapx2',Inode:node},divL);
		$1.T.divL({divLine:1,L:'Descripción',wxn:'wrapx1',I:{tag:'textarea','class':jsF,name:'shortDesc',textNode:Jr.shortDesc}},wrap);
		$Crm.E.inpRels(Jr,{line:'prsAndcard'},wrap);
		var divCn=$Crm.addCnt(wrap);
		if(Jr.iL){
			var nt=100;
			for(var i in Jr.iL){ var L=Jr.iL[i];
				var ki=(L.email+'').replace(/\@/,'').replace(/\./,'');
				var ln='INV['+nt+']'; nt++;
				var vPost0=ln+'[id]='+L.id+'&'+ln+'[lineType]='+L.lineType+'&'+ln+'[email]='+L.email;
				$1.boxvPost({ln:ln,uid:'email_'+ki,line1:L.line1,lineBlue:L.email,vPost:vPost0},divCn);
			}
		}
		var resp=$1.t('div',0,wrap);
		var Met=(Jr.gid)?'PUT':'POST';
		$Api.send({Met:Met,f:Api.Crm.b+'events',loade:resp,vPost,vPost,inputsFrom:wrap,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ $Crm.E.callF({Met:Met,obj:'event',Jr:Jr,o:Jr2._o},bk); }
		}},wrap);
		
	}});
	var bk=$1.Win.open(wrap,{winSize:'medium',onBody:1,winTitle:'Registrar Evento'});
},
drawRow:function(L,wList){
	L=$Crm.Ht.vD(L,{tabOn:'event'});
	var wList=(wList)?wList:$Filt.wList;
	var ide='_crmWrap_gid_'+L.gid;
	var div=$1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'});
	if(old=$1.q('#'+ide)){ wList.replaceChild(div,old); }
	else{ wList.appendChild(div); }
	$Crm.Ht.open(L,{o:'event',pare:div});
	$Crm.Ht.edit(L,{o:'event',pare:div});
	$Crm.Ht.type(L,div);
	$Crm.Ht.prio(L,div);
	$Crm.Ht.doEndDate(L,div);
	$Crm.Ht.profileBot(L,div);
	$Crm.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=(P.cont)?P.cont:$M.Ht.cont;
	var cont=$1.t('div',0); var jsF='jsFields';
var vPost='gid='+P.gid;
	$Api.get({f:Api.Crm.b+'events/view',inputs:vPost,loade:cont,func:function(Jr){
		$1.Tb.trCols([
		{t:'Tipo',v:_g(Jr.oType,$V.crm_eventTypes)},
		{t:'Prioridad',v:_g(Jr.priority,$V.crmPriority)},
		{t:'Inicia',v:$2d.f(Jr.doDate+' '+Jr.doDateAt,'mmm d H:iam')},
		{t:'Finaliza',v:$2d.f(Jr.endDate+' '+Jr.endDateAt,'mmm d H:iam')},
		{t:'Contacto',v:Jr.name},
		{t:'Socio',v:Jr.cardName},
		{t:'Asunto',v:Jr.title},
		{t:'Descripción',tCs:2,v:false},
		{v:$1.t('div',{'class':'pre',textNode:Jr.shortDesc}),cs:2}
		],{cols:2},cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Evento'});
},

}

$Crm.Deals={
get:function(P){
	var wList=$Filt.wList; 
	var vPost=(P.profile)?P.profile+'&':'';
	vPost +=$1.G.filter();
	$Api.get({f:Api.Crm.b+'deals',inputs:vPost, loade:wList, func:function(Jr){
		$Filt.btnFa({fa:'fa fa-money',textNode:'Nueva Oportunidad',func:function(){
		$Crm.Deals.form(Jr);
		}});
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		$ChT._i('crmNovL');
		for(var i in Jr.L){ $ChT._a('crmNovL',Jr.L[i]); }
		$Crm.Deals.drawRow();
	}});
},
form:function(P){
	var wrap=$1.t('div',0); var jsF='jsFields';
	var vPost=(P.gid)?'gid='+P.gid:'';
	$Api.get({f:Api.Crm.b+'deals/form',loadVerif:!P.gid, inputs:vPost,loade:wrap,func:function(Jr){
		Jr=$Crm.E.relData(Jr,P);
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Nombre Oportunidad',wxn:'wrapx4_1',I:{tag:'input',type:'text','class':jsF+' __asunt',name:'title',value:Jr.title}},wrap);
		$1.T.divL({L:'Fecha de Cierre',wxn:'wrapx4',I:{tag:'input',type:'date','class':jsF,name:'dueDate',value:Jr.dueDate}},divL);
		var divL=$1.T.divL({divLine:1,req:'Y',L:'Tipo',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'oType'},opts:$V.crm_dealTypes,selected:Jr.oType}},wrap);
		$1.T.divL({req:'Y',L:'Fase',wxn:'wrapx4',I:{tag:'select',sel:{'class':jsF,name:'oClass'},opts:$V.crm_dealFases,selected:Jr.oClass}},divL);
		$1.T.divL({divLine:1,L:'Descripción',wxn:'wrapx1',I:{tag:'textarea','class':jsF,name:'description',textNode:Jr.description}},wrap);
		$Crm.E.inpRels(Jr,{line:'prsAndcard'},wrap);
		var resp=$1.t('div',0,wrap);
		var Met=(Jr.gid)?'PUT':'POST';
		$Api.send({Met:Met,f:Api.Crm.b+'deals',loade:resp,vPost,vPost,inputsFrom:wrap,func:function(Jr2){
			$Api.resp(resp,Jr2);
			$Crm.E.callF({Met:Met,obj:'deal',Jr:Jr,o:Jr2._o},bk);
		}},wrap);
	}});
	var bk=$1.Win.open(wrap,{winSize:'medium',onBody:1,winTitle:'Oportunidad Comercial'});
},
drawRow:function(wList){
	var wList=(wList)?wList:$1.q('.__crmWrapDeals');
	wList.innerHTML='';
	var Lx=$ChT._g('crmNovL','No se encontraron resultados');
	if(Lx._err){ $Api.resp(wList,Lx); return false; }
	var tb=$1.T.table([]); wList.appendChild(tb);
	var tBody=$1.t('tbody',0,tb);
	var DTh={tBody:tBody,tHead:$1.q('thead',tb),
	tHs:{_0:1,title:1,oType:1,oClass:{rt:'Fase'},dueDate:{rt:'Cierre'},created:1,modified:1}};
	$Crm.Ht.tbFie(DTh); delete(DTh.tHead);
	for(var i in Lx){ L=$js.clone(Lx[i]);
		var edit=$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:Lx[i],func:function(T){
			$Crm.Deals.form({gid:T.P.gid});
		}});
		L._0={node:edit};
		L.oType=_g(L.oType,$V.crm_dealTypes);
		L.oClass=_g(L.oClass,$V.crm_dealFases);
		L.title={node:$1.t('a',{href:$M.to('crmDeals.view','gid:'+L.gid,'r'),textNode:L.title})};
		$Crm.Ht.tbFie(DTh,L);
	}
}
}

$Crm.Agenda={
get:function(P,wList){
	var cont=$M.Ht.cont; $Api.abort('crmAgenda');
	var intr=false;
	if(!wList){
		var a=$1.T.btnFa({fa:'fa-chevron-left',title:'Anterior',func:function(T){
			upDat(det,'-6days');
		}},cont);
		var det=$1.t('span',{'data-vPost':'Y','class':'fa fa-calendar __detDate'},cont);
		det.value=$2d.today; det.vPost='';
		upDat(det);
		var b=$1.T.btnFa({fa:'fa-chevron-right',title:'Siguiente',func:function(T){
			upDat(det,'+8days');
		}},cont);
		var wList=$1.t('div',0,cont);
		function upDat(dTag,mov){
			var nDate=(mov)?$2d.add(dTag.value,mov):dTag.value;
			dTag.value=$2d.weekB(nDate);
			dTag.endDate=$2d.weekE(dTag.value);
			dTag.innerText =$2d.f(dTag.value,'mmm d')+' a '+$2d.f(dTag.endDate,'mmm d');
			dTag.vPost ='date1='+dTag.value+'&date2='+dTag.endDate;
			if(mov){
			clearInterval(intr);
			intr=setInterval(function(){ clearInterval(intr); $Crm.Agenda.get({},wList);   },500);
			}
			
		}
	}
	det=$1.q('.__detDate',cont);
	var vPost='window=calendar&'+$1.G.filter(cont);
	$Api.get({f:Api.Crm.b+'events',rid:'crmAgenda',inputs:vPost, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr);  }
		if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		{
			var Li=[];
			var D={date1:det.value,date2:det.endDate,Li:[]};
			if(!Jr.L.errNo){ for(var i in Jr.L){ 
				L=$Crm.Ht.vD(Jr.L[i],{tabOn:L.obj});
				var Lx=$2d.longDays(L); /*duplicar para dias */
				for(var i3 in Lx){
					var cnt=$1.t('div');
					$Crm.Ht.open(Lx[i3],{o:L.obj,pare:cnt});
					$Crm.Ht.type(Lx[i3],cnt);
					$Crm.Ht.prio(Lx[i3],cnt);
					$Crm.Ht.prs(Lx[i3],cnt);
					$Crm.Ht.crd(Lx[i3],cnt);
					Lx[i3].lineText=cnt;
					D.Li.push(Lx[i3]);
				}
			}}
			$Sche.V.agenda(D,wList);
		}
	}});
},
}

$Crm.addCnt=function(pare){
	var ln='INV'; jsF='jsFields';
	var wrap=$1.t('div',0,pare);
	//var opts=[{k:'prs',v:'Contacto'},{k:'user',v:'Usuario'},{k:'m',v:'Manual'}];
	var opts=[{k:'prs',v:'Contacto'},{k:'m',v:'Manual'}];
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'.',I:{tag:'select',sel:{},opts:opts,noBlank:'Y'}},wrap);
	var sel=$1.q('select',divL);
	var node=$1.t('div',{style:'position:relative'});
	$1.T.divL({L:'...',wxn:'wrapx2',Inode:node},divL);
	var n=0;
	sel.onchange=function(){ changer(this); }
	function changer(T){ node.innerHTML='';
		if(T.value=='prs'){ 
			$crd.Fx.inpSeaPrs({vPost:'_fie=A.email',clearInp:'Y',func:function(R,inp){
				var ln='INV['+n+']'; n++;
				var vPost=ln+'[oid]='+R.prsId+'&'+ln+'[lineType]=prs&'+ln+'[email]='+R.email;
				$1.boxvPost({ln:ln,uid:'prsId_'+R.prsId,line1:R.name,lineBlue:R.email,vPost:vPost},divCn);
			}},node);
		}
		else if(T.value=='m'){ 
			var inp=$1.t('input',{type:'text',placeholder:'cuenta@gmail.com'},node);
			$1.T.btnFa({fa:'btnRight fa_plusCircle',title:'Añadir',P:inp,func:function(T){
				var ki=(T.P.value+'').replace(/\@/,'').replace(/\./,'');
				var ln='INV['+n+']'; n++;
				var vPost=ln+'[lineType]=free&'+ln+'[email]='+T.P.value;
				$1.boxvPost({ide:'Y',ln:ln,uid:'email_'+ki,lineBlue:T.P.value,vPost:vPost},divCn);
				T.P.value='';
			}},node);
		}
	};
	changer({value:'prs'});
	var divCn=$1.t('div',{style:'border:0.0625rem solid #EEE;'},wrap);
	return divCn;
}

$Crm.Ht={
vD:function(L,P){
	var P=(P)?P:{};
	L.doDateFull=L.doDate+' '+L.doDateAt;
	L.endDateFull=L.endDate+' '+L.endDateAt;
	var oT=_gO(L.oType,$V['crm_'+L.obj+'Types']);
	if(L.priority){ L.prioText=_g(L.priority,$V.crmPriority); }
	L.oTypeText=oT.v;
	L.oTypeCss=(oT.color)?'backgroundColor:'+oT.color:'';
	var oT=_gO(L.status,$V['crm_'+L.obj+'Status']);
	L.statusText=oT.v;
	L.statusCss=(oT.color)?'backgroundColor:'+oT.color:'';
	if(L.name){ L.prsDefine=true; }
	if(L.cardName){ L.cardDefine=true; }
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.dueDate)){ L.dueDateDefine=true;
		if(P.dueDateF){ L.dueDateText=$2d.f(L.dueDate,P.dueDateF); }
		else { L.dueDateText=$2d.f(L.dueDate,'mmm d'); }
	}
	if(!$2d.is0(L.doDate)){ L.doDateDefine=true;
		if(P.doDateF){ L.doDateText=$2d.f(L.doDateFull,P.doDateF); }
		else { L.doDateText=$2d.f(L.doDateFull,'mmm d H:i'); }
	}
	if(!$2d.is0(L.endDate)){ L.endDateDefine=true;
		if(P.endDateF){ L.endDateText=$2d.f(L.endDateFull,P.endDateF); }
		else { L.endDateText=$2d.f(L.endDateFull,'mmm d H:i'); }
	}
	return L;
},
Fie:{
'_ck':{t:''},
'_0':{t:'...'},
title:{t:'Asunto'},
oType:{t:'Tipo'},oClass:{t:'Clase'},status:'Estado',
dueDate:{t:'Vencimiento'},status:{t:'Estado'},
doDate:{t:'Inicia'},
endDate:{t:'Finaliza'},
created:{t:'Creado'},
modified:{t:'Modificado'}
},
tbFie:function(D,L){
	/*D{tHead,tHs} */
	if(D.tHead){
		var tr=$1.t('tr',0,D.tHead);
		for(var i in D.tHs){ var X=D.tHs[i];
			var t=($Crm.Ht.Fie[i])?$Crm.Ht.Fie[i].t:i;
			if(X.rt){ t=X.rt; }/* reemplaza texto, oClass por Fase */
			$1.t('td',{textNode:t},tr);
		}
	}
	if(D.tBody && L){
		var tr=$1.t('tr',0,D.tBody);
		for(var i in D.tHs){ var X=D.tHs[i]; var tL={};
			if(X.rk){ i=X.rk; }/* reemplaza texto, oClass por Fase */
			var tex=(L[i])?L[i]:''
			if(typeof(L[i])=='object'){ tL=L[i]; tL.Y=true; };
			if(i=='created'){ tex=$Doc.by('userDate',L); }
			else if(i=='modified'){ tex=$Doc.by('userDate',{userId:L.userUpd,dateC:L.dateUpd}); }
			if(tL.Y){
				var td=$1.t('td',0,tr);
				if(tL.node){ td.appendChild(tL.node); }
			}
			else{ var td=$1.t('td',{textNode:tex},tr); }
		}
	}
},
/*fields*/
cliOpen:function(tag,P){
	tag.onclick=function(){ T=this;
		switch(P.o){
			case 'task' : $Crm.Task.view(T.P); break;
			case 'nov' : $Crm.Nov.view(T.P); break;
			case 'note' : $Crm.Notes.view(T.P); break;
			case 'event' : $Crm.Events.view(T.P); break;
		}
	};
},
open:function(L,P){ /*obj open */
	var tag=(P.tag)?P.tag:$1.t('span',{textNode:L.title,'class':'mPointer _oOpen',P:L},P.pare);
	if(P.tag){ tag.classList.add('mPointer'); tag.P=L; }
	$Crm.Ht.cliOpen(tag,P);
	return tag;
},
edit:function(L,P){
	var divR=$1.t('div',{style:'float:right'},P.pare);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		switch(P.o){
			case 'task' : $Crm.Task.form(T.P); break;
			case 'nov' : $Crm.Nov.form(T.P); break;
			case 'note' : $Crm.Notes.form(T.P); break;
			case 'event' : $Crm.Events.form(T.P); break;
		}
	}},divR);
},
sendCopy:function(L,P){
	var divR=$1.t('div',{style:'float:right'},P.pare);
	$1.T.btnFa({fa:'fa-envelope',title:'Enviar Copia a Correo',P:L,func:function(T){
		$Crm.sendTaskCopy(T.P);
	}},divR);
},
divB:function(cont){
	return $1.t('div',{style:'padding:0 0.25rem; border:0.0625rem solid #DDD;'},cont);
},
divBot:function(cont){
	return $1.t('div',{style:'padding:0.25rem 0 0 0;'},cont);
},
clOpen:function(tag,func){
	tag.onclick=function(){ func(this); }
},
prio:function(L,divT){
	if(L.priority!='N'){
		return $1.t('div',{title:'Prioridad','class':'crm_prpLine fa fa_prio',textNode:L.prioText},divT);
	}
},
type:function(L,divT,P){
	var div=$1.t('div',{textNode:L.oTypeText,title:'Tipo','class':'crm_prpLine',style:L.oTypeCss},divT);
	if(P && P.L){ div.L=L; }
	return div;
},
status:function(L,divT,P){
	if(L.status>0){
	var div=$1.t('div',{textNode:L.statusText,title:'Estado Gestión','class':'crm_prpLine',style:L.statusCss},divT);
	if(P && P.L){ div.L=L; }
	return div;
	}
},
dueDate:function(L,divT){
	if(L.dueDateDefine){
		return $1.t('div',{title:'Vencimiento','class':'fa fa-calendar crm_prpLine',textNode:L.dueDateText},divT);
	}
},
doDate:function(L,divT){
	if(L.doDateDefine){
		return $1.t('div',{title:'Fecha','class':'fa fa-calendar crm_prpLine',textNode:L.doDateText},divT);
	}
},
endDate:function(L,divT){
	if(L.endDateDefine){
		return $1.t('div',{title:'Fecha','class':'fa fa-calendar crm_prpLine',textNode:L.endDateText},divT);
	}
},
doEndDate:function(L,divT){
	if(L.doDateDefine){
		return $1.t('div',{title:'Fecha Evento','class':'fa fa-calendar',textNode:'De '+L.doDateText+' a '+L.endDateText,style:'display:inline-block; border-right:0.0625rem solid #DDD; padding:0 0.5rem;'},divT);
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'fa fa-user crm_prpLine',textNode:L.userAssgText},divT);
	}
},
nums:function(L,divT,P){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments crm_prpLine',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
shortDesc:function(L,divT){
	if(L.shortDesc && L.shortDesc!=''){
		div= $1.t('div',{textNode:L.shortDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		var div=$1.t('div',{title:'Contacto','class':'mPointer fa fa-handshake-o crm_prpLine',textNode:lineText},divB);
		div.onclick=function(){
			var pars='prsId:'+L.prsId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCpr.view',pars);
		}
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		var div= $1.t('div',{title:'Socio de Negocios','class':'mPointer fa fa-institution crm_prpLine',textNode:lineText},divB);
		div.onclick=function(){
			var pars='cardId:'+L.cardId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCrd.view',pars);
		}
		return div;
	}
},
profileBot:function(L,divP){
	if($Crm.profile && $Crm.profile.match(/^(nov|task|event|note)$/)){
		//var divP=$Crm.Ht.divBot(divP);
		$Crm.Ht.prs(L,divP);
		$Crm.Ht.crd(L,divP);
	}
	else if($Crm.profile=='crd'){
		//var divP=$Crm.Ht.divBot(divP);
		$Crm.Ht.prs(L,divP);
	}
	else if($Crm.profile=='cpr'){
		//var divP=$Crm.Ht.divBot(divP);
		$Crm.Ht.crd(L,divP);
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(T){ Attach.Tt.get(T.win.P); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},Wins.win5c);
	Attach.btn({tt:'crmNov',tr:L.gid},Wins.win5f);
	var fc=L.completedAt;
	var fcU=_g(L.completuser,$Tb.ousr);
	if($2d.is0(L.completedAt)){ fc='Sin Completar'; fcU='Sin Completar'; }
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creada',v:L.dateC},
		{wxn:'tbf_x4',t:'Creada por',v:_g(L.userId,$Tb.ousr)},
		{line:1,wxn:'tbf_x4',t:'Fecha Completada',v:fc},
		{wxn:'tbf_x4',t:'Completada por',v:fcU},
		],Wins.winInfo);
}
}

/* Sysd */
$V.crmSysdPrp1=[{k:'task',v:'Tareas'},{k:'nov',v:'Novedad'},{k:'note',v:'Notas'},{k:'event',v:'Eventos'}];
$V.crmSysdPrp2=[{k:'Types',v:'Tipos'},{k:'Status',v:'Estados'},{k:'Class',v:'Clase'},{k:'Fases',v:'Fases'}];
$M.li['sysd.crmTypes']={t:'Tipos para Organizador',kau:'sysd.suadmin', func:function(){ 
	btn=$1.T.btnFa({fa:'faBtnCt fa_plusCircle',textNode:'Nuevo',func:function(){ $Crm.Sysd.Types.form({prp2:'Types'}); }});
	$M.Ht.ini({btnNew:btn,func_filt:'sysd.crmTypes',func_pageAndCont:$Crm.Sysd.Types.get}); }
};
$M.li['sysd.crmStatus']={t:'Estados para Organizador',kau:'sysd.suadmin', func:function(){ 
	btn=$1.T.btnFa({fa:'faBtnCt fa_plusCircle',textNode:'Nuevo',func:function(){ $Crm.Sysd.Types.form({prp2:'Status'}); }});
	$M.Ht.ini({btnNew:btn,func_filt:'sysd.crmTypes',func_pageAndCont:$Crm.Sysd.Types.get}); }
};

$Crm.Sysd={};
$Crm.Sysd.Types={
get:function(){
	var cont=$M.Ht.cont;
	var Pa=$M.read('!');
	var prp2=(Pa=='sysd.crmStatus')?'Status':'Types';
	var vPost='wh[prp2]='+prp2+'&'+$Filt.get($M.Ht.filt);
	$Api.get({f:Api.Crm.sysd+'types', inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Objeto','Categ.','Nombre']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('a',{'class':'fa fa-pencil',title:L.vid,L:L},td).onclick=function(){ $Crm.Sysd.Types.form(this.L); };
				$1.t('td',{textNode:_g(L.prp1,$V.crmSysdPrp1)},tr);
				$1.t('td',{textNode:_g(L.prp2,$V.crmSysdPrp2)},tr);
				$1.t('td',{textNode:L.value},tr);
			}
		}
	}});
},
form:function(Pa){
	var cont=$1.t('div',0); console.log(Pa);
	var vPost=(Pa.vid)?'vid='+Pa.vid:'';
	$Api.get({f:Api.Crm.sysd+'types/form',loadVerif:!Pa.vid, inputs:vPost,loade:cont,func:function(Jr){
		var jsF='jsFields';
		Jr.color=(Jr.color)?Jr.color:'#ffffff';
		var hid=$1.t('input',{type:'hidden','data-vPost':'Y'},cont);
		hid.vPost ='vid='+Pa.vid;
		var Prp1=[];
		var Prp2=[];
		if(Pa.prp2=='Types'){
			Prp1=$V.crmSysdPrp1;
		}
		else if(Pa.prp2=='Status'){
			Prp1=[{k:'task',v:'Tareas'}]; Jr.prp1='task';
		}
		vPost += '&prp2='+Pa.prp2;
		var divL=$1.T.divL({divLine:1,req:'Y',wxn:'wrapx2',L:'Objeto',I:{tag:'select',sel:{name:'prp1','class':jsF},opts:Prp1,selected:Jr.prp1}},cont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4_1',L:'Nombre',I:{tag:'input',type:'text',name:'value',value:Jr.value,'class':jsF}},cont);
		var div=$1.T.divL({wxn:'wrapx4',L:'Color',I:{tag:'input',type:'color',name:'color',value:Jr.color,'class':jsF}},divL);
		resp=$1.t('div',0,cont);
		$Api.send({PUT:Api.Crm.sysd+'types/form',loade:resp,getInputs:function(){ return vPost+'&'+$1.G.inputs(cont); }, func:function(Jr2,o){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo && o.vid){
				hid.vPost ='vid='+o.vid;
				$oB.upd({k:o.vid,v:o.value,color:o.color},$V[o.kObj]);
			}
		}},cont);
	}});
	$1.Win.open(cont,{winTitle:'Defición Variables - CRM',winId:'sysdCrmTypes'});
}
}

/* Calendario */
var $Sche={
}
$Sche.V={
agenda:function(P,pare){
	var tB=$1.t('div',{'class':'sChe_agenda'},pare);
	//hora, asunto, detalles
	var diff=$2d.diff({date1:P.date1,date2:P.date2});
	for(var i=0; i<diff; i++){
		var tDat=$2d.add(P.date1,'+'+i+'days');
		var kday=$2d.g('Y-z',tDat);
		var trTop=$1.t('div',{'class':'____tbRow'},tB);
		$1.t('div',{textNode:$2d.f(tDat,'T l, d M'),'class':'sCche_trTitle'},trTop);
		$1.t('div',{'class':'_scheDay_'+kday},trTop);
	}
	for(var i in P.Li){
		addLine(P.Li[i]);
	}
	function addLine(L){
		var trTop=$1.q('._scheDay_'+L.kday,tB);
		var tr=$1.t('div',{'class':'sCche_trLine'},trTop);
		$1.t('div',{textNode:L.hText,'class':'sCche_td sCche_tdTime'},tr);
		var div=$1.t('div',{'class':'sCche_td'},tr);
		if(L.lineText && L.lineText.tagName){ div.appendChild(L.lineText); }
		else{ div.innerText=L.lineText; }
	}
	return tB;
},
}
