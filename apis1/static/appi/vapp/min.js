
Api.vApp={b:'/appi/private/vapp/',pu:'/appi/public/vapp/',o:'/appi/open/vapp/'};
var vApp={};
$V.vAppSellPriceView=[{k:'D',v:'Definido'},{k:'F',v:'Desde'},{k:'H',v:'Consultar'}];

$V.itmColors=[
	{k:1,v:'Azul'},{k:2,v:'Verde'},{k:3,v:'Rojo'},{k:4,v:'Amarillo'},
	{k:5,v:'Naranja'},{k:6,v:'Rosa'},{k:7,v:'Cafe'}
];

$V.vAppCategory=[
	{k:'tiendaropa',v:'Tienda de Ropa'},
	{k:'alimentos',v:'Alimentos y Bebidas'},
	{k:'mascotas',v:'Mascotas'},
	{k:'ventaxmayor',v:'Venta al por mayor'},
	{k:'servicios',v:'Servicios'},
	{k:'cuidadopersonal',v:'Cuidado Personal'},
	{k:'tecnologia',v:'Tecnología'},
	{k:'bebes',v:'Bebes y Niños'},
	{k:'licores',v:'Licores'},
	{k:'floristeria',v:'Floristeria'},
	{k:'restaurante',v:'Restaurante'},
	{k:'farmacia',v:'Farmacias'},
	{k:'transporte',v:'Transporte'},
	{k:'comidarapida',v:'Comida Rápida'},
	{k:'panaderia',v:'Panaderia y Reposteria'},
	{k:'verduras',v:'Verduras y Frutas'},
	{k:'tiendas',v:'Tiendas'},
	{k:'grandestiendas',v:'Supermercadores'}
];
$V.vAppCategory2=[
	{k:'mudanza',v:'Mudanza',ktop:'transporte'},
	{k:'taxi',v:'Taxi',ktop:'transporte'},
	{k:'mototaxi',v:'Moto Taxi',ktop:'transporte'},
	{k:'domicilio',v:'Domicilios',ktop:'transporte'}
];
