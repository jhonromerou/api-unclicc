
$M.liAdd('wma',[
{_lineText:'--'},
{k:'vApp.itm',t:'Mis Productos',kau:'vApp.itm',func:function(){
	$M.Ht.ini({btnNew:$1.T.btnFa({fa:'fa-plus',textNode:'Nuevo',func:function(){ vApp.Itm.winEdit({}); }}), gyp:function(){ vApp.Itm.get(); }});
}},
]);


_Fi['vApp.itm']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Req. Act.',I:{tag:'select','class':jsV,name:'M.updSubEsc',opts:$V.YN}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Costo <=',I:{tag:'input',type:'number',min:0,'class':jsV,name:'M.cost(E_menIgual)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Mpg.get},wrap);
};

vApp.Itm={
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.vApp.b+'itm',inputs:$1.G.filter(),loade:cont, errWrap:cont, func:function(Jr){
			for(var i in Jr.L){ var L=Jr.L[i];
				var dv=$1.t('hdiv',{'class':'vAppItm_row'},cont);
				var h5=$1.t('h5',0,dv);
				$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
					vApp.Itm.winEdit(T.P);
				}},h5);
				$1.t('span',{textNode:L.itemName},h5);
				var priceTxt='';
				if(L.sellPriceView=='D'){ priceTxt=$Str.money(L.sellPrice); }
				else if(L.sellPriceView=='H'){ priceTxt='Consultar Precio'; }
				else if(L.sellPriceView=='F'){ priceTxt='Desde '+$Str.money(L.sellPrice); }
				$1.t('b',{'class':'iPrice',textNode:priceTxt},dv);
				$1.t('img',{'class':'iImg',src:L.mainSrc,style:'maxWidth:16rem; maxHeight:32rem;'},dv);
			}
		}});
	},
	winEdit:function(D){
		var wrap=$1.t('div',{'class':'vAppItm_form'});
		var jsF=$Api.JS.cls;
		$Api.get({f:Api.vApp.b+'itm/'+D.itemId,loadVerif:!D.itemId,loade:wrap,func:function(Jr){
			var sX={POST:Api.vApp.b+'itm',jsBody:wrap,loade:resp,func:function(Jr2){
				$Api.resp(resp,Jr2);
			}};
			if(D.itemId){ sX.PUT=sX.POST; delete(sX.POST);
				$Api.JS.addF({name:'itemId',value:D.itemId},wrap);
			}
			if(!Jr.imgs){ Jr.imgs={}; }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4_1',L:'Nombre Artículo',I:{tag:'input',type:'text','class':jsF,name:'itemName',maxLengt:100,value:Jr.itemName}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',maxLengt:20,value:Jr.itemCode}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Tipo Precio',I:{tag:'select','class':jsF,opts:$V.vAppSellPriceView,noBlank:'Y',name:'sellPriceView',value:Jr.sellPriceView}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Precio',I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'sellPrice',value:Jr.sellPrice*1}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Colores',I:{tag:'select',multiple:'multiple','class':jsF,opts:$V.itmColors,noBlank:'Y',name:'colors',value:Jr.colors}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción del producto',I:{tag:'textarea','class':jsF,name:'description',value:Jr.description}},wrap);
			var ss=new JStor('/mnt/vol2/john');
			ss.imgInput({ttile:'Imagen Principal',name:'mainSrc',jsF:jsF,value:Jr.mainSrc},wrap);
			ss.imgInput({ttile:'Imagen 2',name:'imgs',I:{'keyname':'src2'},jsF:$Api.JS.clsArName,value:Jr.imgs.src2},wrap);
			ss.imgInput({ttile:'Imagen 3',name:'imgs',I:{'keyname':'src3'},jsF:$Api.JS.clsArName,value:Jr.imgs.src3},wrap);
			var resp=$1.t('div',0,wrap);
			$Api.send(sX,wrap);
		}});
		$1.Win.open(wrap,{winTitle:'Modificar Articulo',winSize:'medium'});
	}
}
