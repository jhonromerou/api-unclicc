Api.Gfp={a:'/appi/private/gfp/'};

$V.gfpMovType=[{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'},{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'R',v:'Retiros'}];
$V.gfpMovTypeWB=[{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'}];
$V.gfpWallType=[{k:'EF',v:'Efectivo'},{k:'CB',v:'Cuenta Bancaria'},{k:'TC',v:'T. Crédito'},{k:'CR',v:'Crédito'}];

$V.gfpCredType=[
	{k:'compra',v:'- Compra'},{k:'interes',v:'- Interes'},{k:'interesM',v:'- Interes Mora'},{k:'avance',v:'- Avance'},{k:'seguro',v:'- Seguro'},{k:'manejo',v:'- Manejo'},{k:'rediferido',v:'- Rediferido Compra'},{k:'otros',v:'- Otros Cargos'},
	{k:'pago',v:'+ Pago'},{k:'rediferidoP',v:'+ Rediferido Pago'},
	{k:'otrosP',v:'+ Otros Cargos'}
];
$V.gfpCredTypeCR=[
	{k:'compra',v:'- Desembolso'},{k:'interes',v:'- Interes'},{k:'interesM',v:'- Interes Mora'},{k:'seguro',v:'- Seguro'},{k:'otros',v:'- Otros Cargos'},
	{k:'pago',v:'+ Pago Cuota'},{k:'pagoC',v:'+Pago Capital'},
	{k:'otrosP',v:'+ Otros Cargos'}
];

_Fi['gfp.wal']=function(wrap){
	Pa=$M.read('!');
	$Doc.filter({func:Gfp.Wal.get},[
	{wxn:'wrapx4',L:'Nombre',I:{lTag:'input',name:'A.wallName(E_like3)'}},
	{wxn:'wrapx8',L:'Banco',I:{lTag:'select',name:'A.bankId',opts:$JsV.gfpBank}},
	{wxn:'wrapx8',L:'Tipo',I:{lTag:'select',name:'A.wallType',opts:$V.gfpWallType}},
	{wxn:'wrapx8',L:'Disponible >',I:{lTag:'$',name:'A.amount(E_may)'}},
	{wxn:'wrapx8',L:'Disponible <',I:{lTag:'$',name:'A.amount(E_men)'}},
	{wxn:'wrapx8',L:'Deuda >',I:{lTag:'$',name:'deuda1'}}, //< -2000 (2)
	{wxn:'wrapx8',L:'Deuda <',I:{lTag:'$',name:'deuda2'}},// > -3000 (1)
	],wrap);
};
_Fi['gfp.mov']=function(wrap){
	var Pa=$M.read();
	var ordBy=[{k:'',v:'Fecha Mayor'},{k:'docDateASC',v:'Fecha Menor'}]
	$Doc.filter({func:Gfp.Mov.get},[
		{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'M.wallId',opts:$Tb.gfpWal,selected:Pa.wallId}},
		{L:'Inicio',wxn:'wrapx8',I:{tag:'input',type:'date',name:'M.docDate(E_mayIgual)'}},
		{L:'Fecha Fin',wxn:'wrapx8',I:{tag:'input',type:'date',name:'M.docDate(E_menIgual)'}},
		{L:'Tipo',wxn:'wrapx8',I:{tag:'select',name:'M.mType',opts:$V.gfpMovType}},
		{L:'Categoria',wxn:'wrapx8',I:{tag:'select',name:'M.categId',opts:$JsV.gfpCateg}},
		{L:'Orden',wxn:'wrapx8',I:{tag:'select',name:'ordBy',opts:ordBy}},
		{divLine:1,wxn:'wrapx4', L:'Detalle',I:{tag:'input',type:'text',name:'M.lineMemo(E_like3)'}},
		{L:'Tipo TC',wxn:'wrapx10',I:{lTag:'select',name:'M.credType',opts:$V.gfpCredType}},
		{L:'Cuotas >=',wxn:'wrapx10',I:{lTag:'number',name:'M.credCuotas(E_mayIgual)'}},
		{L:'Prox.',wxn:'wrapx10',I:{lTag:'select',name:'M.cred1Pay',opts:$V.NY}}
	],wrap);
};
_Fi['gfpRep.tcp']=function(wrap){
	opts=[{k:'C',v:'Consolidado Periodo'},{k:'M',v:'Detalle Periodo'},{k:'LP',v:'Listado Periodos'}];
	$Doc.filter({func:Gfp.Rep.tcp},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'M.wallId','class':'gfpCuentaCorte',opts:$Tb.gfpWal,kIf:{wallType:'TC'}}},
	{L:'Corte',wxn:'wrapx4',I:{lTag:'select','class':'gfpCuentaCorte',name:'C1.pid',opts:[]}},
	{L:'Año',wxn:'wrapx8',I:{lTag:'number',min:0,name:'docYear'}},
	],wrap);
	Gfp.Fx.cuentaCorte(wrap,function(){
		return $1.q('[name="viewType"]',wrap).value=='LP';
	});
};
_Fi['gfpRep.copen']=function(wrap){
	opts=[{k:'G',v:'General'}];
	$Doc.filter({func:Gfp.Rep.copen},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Cuenta',wxn:'wrapx8',I:{lTag:'select',name:'A.wallId',opts:$Tb.gfpWal,kIf:{wallType:'TC'}}},
	{k:'d1'},{k:'d2'},
	{L:'Saldo >',wxn:'wrapx8',I:{lTag:'number',value:'0',name:'A.credSal(E_may)'}},
	{L:'Saldo <',wxn:'wrapx8',I:{lTag:'number',name:'A.creSal(E_men)'}}
	],wrap);
};
_Fi['gfpRep.ctotal']=function(wrap){
	opts=[{k:'G',v:'General'}];
	$Doc.filter({func:Gfp.Rep.ctotal},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Banco',wxn:'wrapx8',I:{lTag:'select',name:'bankId',opts:$JsV.gfpBank}},
	{k:'d1'},{k:'d2'},
	{L:'Saldo >',wxn:'wrapx8',I:{lTag:'number',value:'0',name:'A.credSal(E_may)'}},
	{L:'Saldo <',wxn:'wrapx8',I:{lTag:'number',name:'A.creSal(E_men)'}}
	],wrap);
};
_Fi['gfpRep.crquotes']=function(wrap){
	opts=[{k:'G',v:'General'}];
	$Doc.filter({func:Gfp.Rep.crQuotes},[
	{L:'Reporte',wxn:'wrapx8',I:{lTag:'select',name:'viewType',opts:opts,noBlank:'Y'}},
	{L:'Banco',wxn:'wrapx8',I:{lTag:'select',name:'bankId',opts:$JsV.gfpBank}},
	{k:'d1',f:'B.lineDate',value:$2d.today},{k:'d2',f:'B.lineDate',value:$2d.today},
	{L:'Saldo a >',wxn:'wrapx8',I:{lTag:'number',value:'0',name:'B.saldo(E_may)'}},
	{L:'Saldo a <',wxn:'wrapx8',I:{lTag:'number',name:'B.saldo(E_men)'}}
	],wrap);
};
_Fi['gfp.cre']=function(wrap){
	$Doc.filter({func:Gfp.Cre.get},[
	{k:'d1'},{k:'d2'},
	{wxn:'wrapx8',L:'Banco',I:{lTag:'select',name:'M.bankId',opts:$JsV.gfpBank}},
	{L:'Categoria',wxn:'wrapx8',I:{lTag:'select',name:'M.categId',opts:$JsV.gfpCategCR}},
	{L:'Pendiente >',wxn:'wrapx8',I:{lTag:'$',value:'0',name:'M.credSal(E_may)'}},
	{L:'Detalles',wxn:'wrapx4',I:{lTag:'input',name:'M.lineMemo(E_like3)'}},
	],wrap);
};

$V.gfpTCjs={
cManejo:'C. Manejo', seguro:'Seguro',avances:"Avances",otros:"Otros",puntos:"Puntos"
};
var Gfp={};
Gfp.Wal={
OLg:function(L){
	var Li=[];
	var ab=new $Doc.liBtn(Li,L,{api:Api.Gfp.a+'wal',tbSerie:'N'});
	ab.add('E');
	ab.add('man',{k:'edit',ico:'fa fa-pencil',textNode:'Modificar', P:L, func:function(T){ Gfp.Wal.form(T.P); } })
	Li=ab.Opts();
	return Li;
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		$1.btnGo({br:1,textNode:'Registrar Movimientos',func:()=>{ $M.to('gfp.movForm'); }},cont);
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['',{textNode:'Tipo'},{textNode:'Nombre'},{textNode:'Disponible'},'',{textNode:'Deuda'},{textNode:'Cupo'},{textNode:'Cupo Disp.'},'','Detalles']);
			var css='backgroundColor:#EEE;';
			var cssSep='backgroundColor:#CCC;';
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',{'class':'wallId_'+L.wallId},tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa fa-money',P:L,func:function(T){ $M.to('gfpWal.prof','wallId:'+T.P.wallId)},title:'Perfil de Cuenta'},td);
				$1.Menu.winLiRel({Li:Gfp.Wal.OLg(L),PB:L},td);
				$1.t('td',{textNode:_g(L.wallType,$V.gfpWallType)},tr);
				var td=$1.t('td',{textNode:L.wallName},tr);
				if(L.wallType=='TC'){
					var cssDeu='';
					var cssClose=($2d.diff({date1:L.closeDate})>=-7)?'backgroundColor:#FF0;':'';
					var percDeu=$js.toFixed((Math.abs(L.amount*1)/L.creditLine*1)*100,1);
					if(percDeu>50){ cssDeu='fontWeight:bold; color:purple;'; }
					else if(percDeu>40){ cssDeu='fontWeight:bold; color:red;'; }
					else if(percDeu>30){ cssDeu='fontWeight:bold; color:orange;'; }
					$1.t('td',{style:css},tr);
					$1.t('td',{style:cssSep},tr);
			$1.t('td',{textNode:$Str.money(L.amount),style:cssDeu,title:percDeu+' % Endeudamiento'},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine)},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine*1+L.amount*1)},tr);
					var td=$1.t('td',{style:cssClose},tr);
					var css1=(L.payDateMin<$2d.today)?'color:#0F0':'';
					$1.t('div',{textNode:'Pago Min.: '+$2d.f(L.payDateMin,'d mmm'),style:css1},td);
					$1.t('div',{textNode:'Avances: '+$Str.money(L.creditLineAvan)},td);
					$1.t('div',{textNode:'Corte : '+$2d.f(L.closeDate,'d mmm')},td);
					$1.t('div',{textNode:'Pago : '+$2d.f(L.payDate,'d mmm')},td);

				}
				else{
					$1.t('td',{textNode:$Str.money(L.amount)},tr);
					$1.t('td',{style:cssSep},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
				}
				var td=$1.t('td',0,tr);
				var prp=$js.parse(L.prp);
				if(prp){for(var ki in prp){
						var d=$1.t('div',0,td);
						$1.t('b',{textNode:_g(ki,$JsV.gfpWallPrp)},d);
						$1.t('span',{textNode:' '+prp[ki]},d);
				}}
				$1.t('div',{textNode:L.lineMemo},td);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'});
			cont.appendChild(tb);
		}
	}});
},
	prof:()=>{
		Pa=$M.read();
		$1.tabs([
			{textNode:'Movimientos',active:'Y',winClass:'mov',func:(Tli)=>{
				Gfp.Mov.get({vGet:'M.wallId='+Pa.wallId},Tli.win);
			}},
			{textNode:'Control Cuotas',winClass:'copen',func:(Tli)=>{
				Gfp.Rep.copen({vGet:'A.wallId='+Pa.wallId},Tli.win);
			}},
		],$M.Ht.cont);
	},
	form:(Pa)=>{
		Pa=(Pa)?Pa:{};
		wrap=$1.t('div'); $1.Win.open(wrap,{winTitle:'Cuenta',winSize:'medium'});
		$Api.form2({api:Api.Gfp.a+'wal',jsF:'Y',uid:{k:'wallId',v:Pa.wallId},
		tbH:[
			{L:'Tipo',wxn:'wrapx8',req:'Y',I:{lTag:'select',name:'wallType','class':'wallType',opts:$V.gfpWallType}},
			{L:'Codigo',wxn:'wrapx8',req:'Y',I:{lTag:'input',name:'wallCode'}},
			{L:'Nombre',wxn:'wrapx4',req:'Y',I:{lTag:'input',name:'wallName'}},
			{L:'Banco',wxn:'wrapx4',I:{lTag:'select',name:'bankId',opts:$JsV.gfpBank}},
			{divLine:1,L:'Apertura',wxn:'wrapx4',req:'Y',I:{lTag:'date',name:'dateOpen'}},
			{divLine:1,L:'Detalles',wxn:'wrapx1',I:{lTag:'input',name:'lineMemo'}},
			{divLine:1,L:'Cupo',wxn:'wrapx4',I:{lTag:'$',name:'creditLine'}},
			{L:'Avances',wxn:'wrapx4',I:{lTag:'$',name:'creditLineAvan'}},
			{L:'Vencimiento',wxn:'wrapx4',I:{lTag:'input',name:'dueDate',placeholder:'YYYY-mm'}},
			{L:'Cortes',wxn:'wrapx4',I:{lTag:'select',name:'cId',opts:$Tb.gfpCtc}},
		],
		midCont:(Jr,cont)=>{
			$1.t('h4',{textNode:'Propiedades'},cont);
			if($JsV.gfpWallPrp){
				Jr.prp=$js.parse(Jr.prp,{});
				var tBody=$1.t('tbody',0,$1.T.table(['Nombre','Valor'],0,cont));
				for(var i in $JsV.gfpWallPrp){ L=$JsV.gfpWallPrp[i];
					tr=$1.t('tr',{'class':$Api.JS.clsL,jsk:'prp'},tBody);
					$1.t('td',{textNode:L.v},tr);
					val =(Jr.prp && Jr.prp[L.k])?Jr.prp[L.k]:'';
					var td=$1.t('td',0,tr);
					$1.t('input',{type:'text',value:val,name:L.k,'class':$Api.JS.clsLk},td);
				}
			}
		}
		},wrap);
	}
}

Gfp.Cre={
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Gfp.a+'cre',inputs:$1.G.filter(), loade:cont, func:function(Jr){
			if(Jr.errNo){ $Api.resp(cont,Jr); }
			else{
				var tb=$1.T.table(['','Cuenta','Banco','Saldo','Monto Prestado','Cuotas','Cap. Pagado','Int. Pagado','$ Otros','Apertura','Categoria','Detalles','$ Cuota','Cerrado']);
				var tBody=$1.t('tbody',0,tb);
				for(var i in Jr.L){ L=Jr.L[i];
					var tr=$1.t('tr',{'class':'wallId_'+L.wallId},tBody);
					var td=$1.t('td',0,tr);
					$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{
						Gfp.Cre.form(T.P);
					}},td);
					Gfp.Fx.quotes(L,td);
					$1.t('td',{textNode:L.mName},tr);
					$1.t('td',{textNode:_g(L.bankId,$JsV.gfpBank)},tr);
					$1.t('td',{textNode:$Str.money(L.credSal)},tr);
					$1.t('td',{textNode:$Str.money(L.creBal)},tr);
					$1.t('td',{textNode:L.credCuoNum*1+' / '+L.credCuotas*1},tr);
					$1.t('td',{textNode:$Str.money(L.credCap)},tr);
					$1.t('td',{textNode:$Str.money(L.credInt)},tr);
					$1.t('td',{textNode:$Str.money(L.credOtros)},tr);
					$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
					$1.t('td',{textNode:_g(L.categId,$JsV.gfpCategCR)},tr);
					$1.t('td',{textNode:L.lineMemo},tr);
					$1.t('td',{textNode:$Str.money(L.credBalCuota)},tr);
					$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
				}
				tb=$1.T.tbExport(tb,{ext:'xlsx'}); cont.appendChild(tb);
			}
		}});
	},
form:function(Pa){
	Pa=(Pa)?Pa:{};
	var wrap=$1.t('div'); $1.Win.open(wrap,{winTitle:'Crédito',winSize:'full'});
	if(!Pa.docDate){ Pa.docDate=$2d.today; }
	//sysType=CR para volver matriz
	$Api.form2({api:Api.Gfp.a+'mov',jsF:'Y',uid:{k:'mid',v:Pa.mid},AJs:{sysType:'CR',mType:'G',credType:'desembolso'},
		tbH:[
	{L:'Banco',wxn:'wrapx4',I:{lTag:'select',name:'bankId',opts:$JsV.gfpBank}},
	{L:'Nombre Obligación',wxn:'wrapx4_1',I:{lTag:'input',name:'mName'}},
	{L:'Apertura',wxn:'wrapx8',req:'Y',I:{lTag:'date',name:'docDate',value:Pa.docDate}},
	{L:'Monto Prestado',wxn:'wrapx8',req:'Y',I:{lTag:'$',name:'bal',min:0}},
	{L:'Plazo (meses)',wxn:'wrapx10',req:'Y',I:{lTag:'number',name:'credCuotas',min:0}},
	{L:'Tasa E.A',wxn:'wrapx10',req:'Y',I:{lTag:'number',name:'credTasa',min:0}},
	{L:'$ Cuota',wxn:'wrapx8',req:'Y',I:{lTag:'$',name:'credBalCuota'}},
	{L:'Categoria',wxn:'wrapx8',req:'Y',I:{lTag:'select',name:'categId',opts:$JsV.gfpCategCR}},
	{L:'Cerrado',wxn:'wrapx8',req:'Y',I:{lTag:'date',name:'dueDate'}},
	{divLine:1,L:'Detalles',wxn:'wrapx1',I:{lTag:'input',name:'lineMemo'}}
	],reqFields:{
		D:[{k:'mName',iMsg:'Nombre Obligación'},{k:'docDate',iMsg:'Apertura'},{k:'bal',iMsg:'Monto Prestado'}],
	}},wrap);
},
}

Gfp.Mov={
get:function(P,pare){
	P=(P)?P:{};
	pare=(pare)?pare:$M.Ht.cont;//llamar
	inputs=(P.vGet)?P.vGet:$1.G.filter();
	$Api.get({f:Api.Gfp.a+'mov',inputs:inputs, loade:pare, func:function(Jr){
		if(Jr.errNo){ $Api.resp(pare,Jr); }
		else{
			var tb=$1.T.table(['','Tipo','Cuenta','Valor','Fecha','Categoria','Cuotas','Detalles']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var bal=(L.debBal>0)?L.debBal:'?';
				bal=(L.creBal>0)?L.creBal:bal;
				balCss=(L.creBal>0)?'color:red;':'color:green';
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{title:'mid: '+L.mid},tr);
				$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Mov.editForm(T.P,T); }},td);
				if(L.wallType=='TC'){
					$1.t('td',{textNode:_g(L.credType,$V.gfpCredType)},tr);
					if(L.credCuotas>1 && L.credType.match(/^(compra|avance|rediferido)$/)){
						Gfp.Fx.quotes(L,td);
					}
					if(L.wallType=='TC'){
						$1.T.check({name:'cred1Pay',value:L.cred1Pay,'class':$Api.JS.cls,AJs:{mid:L.mid},func:(T)=>{
						$Api.put({f:Api.Gfp.a+'mov/oneField',jsBody:T.parentNode,func:function(Jr){
							if(Jr.errNo){ $1.Win.message(Jr); }
						}},td);
					}},td);
					}
				}
				else{ 
					$1.t('td',{textNode:_g(L.mType,$V.gfpMovType)},tr);
				}
				$1.t('td',{textNode:_g(L.wallId,$Tb.gfpWal)},tr);
				var td=$1.t('td',{textNode:$Str.money(bal),style:'fontWeight:bold; '+balCss},tr);
				L.editar='bal'; td.L=L;
				//td.ondblclick=function(){ Gfp.Mov.editLine(this.L,this); }
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.categId,$JsV.gfpCateg)},tr);
				$1.t('td',{textNode:L.credCuotas},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				var td=$1.t('td',0,tr);
				$Api.send({'class':'',textNode:'Borrar',DELETE:Api.Gfp.a+'mov', inputs:'mid='+L.mid,func:function(Jr2){
					$1.Win.message(Jr2);
				}},td);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'}); pare.appendChild(tb);
		}
	}});
},
formOne:function(Pa,wMov){
		var jsF=$Api.JS.clsLN;
		var wm=$1.t('fieldset',0,wMov);
		var lg=$1.t('legend',{textNode:'Movimiento'},wm);
		$1.T.btnFa({fa:'fa_close',title:'Borrar movimiento',textNode:' Borrar',P:wm,func:function(T){ $1.delet(T.P); }},lg);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'input',type:'date','class':jsF,name:'docDate',value:Pa.docDate}},wm);
		$1.T.divL({wxn:'wrapx10',L:'cuenta',req:'Y',I:{tag:'select','class':'inpFocus '+jsF,name:'wallId',opts:$Tb.gfpWal}},divL);
		$1.T.divL({wxn:'wrapx10',req:'Y',L:'Tipo',I:{tag:'select','class':'mType '+jsF,name:'mType',opts:$V.gfpMovType,noBlank:1}},divL);
		$1.T.divL({wxn:'wrapx10',L:'T.C Tipo',req:'Y',I:{lTag:'select','class':'credType '+jsF,name:'credType',opts:$V.gfpCredType}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Valor',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil','class':jsF,name:'bal',min:0}},divL);
		$1.T.divL({wxn:'wrapx10',L:'T.C Cuotas',req:'Y',I:{lTag:'number',min:0,'class':'credCuotas '+jsF,name:'credCuotas'}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Prox.',_i:'Si marca SI permite identificar que forma parte del próximo corte, RECUERDE: debe desmarcarlas al finalizar el corte',req:'Y',I:{lTag:'select','class':'cred1Pay '+jsF,name:'cred1Pay',opts:$V.NY,noBlank:'Y'}},divL);
		$1.T.divL({L:'$ Cuota',wxn:'wrapx8',req:'Y',I:{lTag:'$',name:'credBalCuota'}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Categoria',req:'Y',I:{lTag:'select','class':jsF,name:'categId',opts:$JsV.gfpCateg}},divL);
		wm.classList.add($Api.JS.clsL);
		$1.T.divL({wxn:'wrapx6',L:'Detalles',I:{tag:'input',type:'text','class':jsF,name:'lineMemo'}},divL);

		var inp=$1.q('.inpFocus',wMov);
		function cambio(T){ //añ cambiar cuenta
			var wid=_gO(T.value,$Tb.gfpWal); 
			mTypeInp=$1.q('.mType',wm);
			mType=mTypeInp.parentNode;
			cType=$1.q('.credType',wm);
			cred1Pay=$1.q('.cred1Pay',wm).parentNode;
			cTypeTop=cType.parentNode;
			cType.AJs=null;
			cType.onchange=function(){
				tval=this.value+'';
				if(tval.match(/^(pago|pagoC|rediferidoP|otrosP)$/)){
					cType.AJs={mType:'I'};
				}
				else{ cType.AJs={mType:'G'}; }
			}
			cCuo=$1.q('.credCuotas',wm).parentNode;
			mTypeInp.classList.remove($Api.JS.clsLN);
			cred1Pay.style.display='none';
			mType.style.display='none';
			cTypeTop.style.display='none';
			cCuo.style.display='none';
			
			if(wid && wid.wallType=='TC'){
				$1.T.sel({reLoad:cType,opts:$V.gfpCredType});
				cred1Pay.style.display='';
				cTypeTop.style.display='';
				cCuo.style.display='';
			}
			else if(wid && wid.wallType=='CR'){
				cTypeTop.style.display='';
				cCuo.style.display='';
				$1.T.sel({reLoad:cType,opts:$V.gfpCredTypeCR});
			}
			else{
				mTypeInp.classList.add($Api.JS.clsLN);
				mType.style.display='';
			}
		}
		inp.onchange=function(){ cambio(this); }
		inp.focus();
		inp.classList.remove('inpFocus');
		cambio(inp);
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; var Pa=$M.read();
	cont.innerHTML='';
	if(!Pa.docDate){ Pa.docDate=$2d.today; }
	$Api.get({f:Api.Gfp.a+'mov',inputs:'mId='+Pa.mId,loadVerif:!Pa.mId,loade:cont, func:function(Jr){
		var wMov=$1.t('div',0,cont);
		Gfp.Mov.formOne(Pa,wMov);
		$1.t('br',0,cont);
		$1.btnGo({textNode:'Añadir Movimiento',func:()=>{ Gfp.Mov.formOne(Pa,wMov); },br:1},cont);
		var resp=$1.t('div',0,cont);
		$Api.send({POST:Api.Gfp.a+'mov',jsBody:cont,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Gfp.Mov.form({respBef:Jr2.text}); }
		}},cont);
	}});
},
quotes:function(P){
	$1.delet('#gfpMovQuotes');
	var wrap=$1.t('div');
	$1.Win.open(wrap,{winId:'gfpMovQuotes',winTitle:'Registros de Crédito',winSize:'full'});
	$Api.get({f:Api.Gfp.a+'mov/quotes',inputs:'mid='+P.mid,loade:wrap,func:(Jr)=>{
		Jq=Jr.L[0];
		if(!Jq.wallType.match(/^(TC|CR)$/)){
			return $Api.resp(wrap,{errNo:3,text:'El tipo de cuenta no maneja esta opcion'});
		}
		else if(!Jq.credType.match(/^(desembolso|compra|avance|rediferido)$/)){
			return $Api.resp(wrap,{errNo:3,text:'El tipo de movimiento no maneja esta opcion'});
		}
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'input',type:'date',value:Jq.docDate,disabled:'disabled'}},wrap);
		tOpts=(Jq.wallType=='CR')?$JsV.gfpCategCR:$JsV.gfpCateg;
		$1.T.divL({wxn:'wrapx4',L:'Categoria',req:'Y',I:{lTag:'select',opts:tOpts,value:Jq.categId,disabled:'disabled'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Monto',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil',value:Jq.bal*1,disabled:'disabled'}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Cuotas',I:{lTag:'number',value:Jq.credCuotas,disabled:'disabled'}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',req:'Y',I:{lTag:'input',value:Jq.lineMemo,disabled:'disabled'}},wrap);
		var wm=$1.t('fieldset',0,wrap);
		$1.t('legend',{textNode:'Cuotas'},wm);
		var tb=$1.T.table(['#','Capital','Interes','Otros','Borrar','$'],0,wm);
		tBody=$1.t('tbody',0,tb);
		$1.T.btnFa({btnCls:'btnB btn-drop fa fa_plusCircle',textNode:'Añadir',func:(T)=>{ trA({},tBody); }},wm);
		var resp=$1.t('div',0,wrap);
		$Api.JS.addF({name:'mid',value:P.mid},wrap);
		$Api.send({PUT:Api.Gfp.a+'mov/quotes',textNode:'Guardar',jsBody:wrap,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Gfp.Mov.quotes(P); }
		}},wrap);
		if(Jq.lineNum>0){
			var acum=Jq.bal*1;
			for(var i in Jr.L){
				acum = acum-Jr.L[i].capital*1;
				Jr.L[i].saldoCalc=acum;
				trA(Jr.L[i],tBody);
			}
		}
		
		function trA(L,tBody){
			jsF=$Api.JS.clsLN;
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'number',min:0,'class':jsF,name:'lineNum',value:L.lineNum,style:'width:50px'},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'capital',value:L.capital*1,style:'width:120px'},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'interes',value:L.interes*1,style:'width:120px'},td);
			td=$1.t('td',0,tr);
			$1.lTag({tag:'$',min:0,'class':jsF,name:'otros',value:L.otros*1,style:'width:80px'},td);
			//td=$1.t('td',{style:'backgroudColor:#CCC'},tr);
			//$1.lTag({tag:'$',min:0,'class':jsF,name:'saldo',value:L.saldo*1,style:'width:120px'},td);
			//td=$1.t('td',{style:'width:50px'},tr);
			//$1.lTag({tag:'select',opts:Jr.Lc,'class':jsF,name:'pid',value:L.pid,style:'width:50px'},td);
			td=$1.t('td',{style:'width:120px'},tr);
			$1.lineDel(L,{},td);
			td=$1.t('td',0,tr);
			if(L.id){
				$1.t('span',{textNode:$Str.money(L.saldoCalc),title:L.saldoCalc},td);
			}
		}
	}});
},

editForm:function(L){
	var wid='gfpMovLineEdit';
	$Api.form2({f:Api.Gfp.a+'mov/one',uid:{k:'mid',v:L.mid},jsF:'Y',
	Win:{winTitle:'Modificar Movimiento',winSize:'medium'},
	tbH:[
		{L:'Fecha',wxn:'wrapx4',req:'Y',I:{lTag:'input',type:'date',name:'docDate'}},
		{L:'Categoria',wxn:'wrapx6',req:'Y',I:{lTag:'select',name:'categId',opts:$JsV.gfpCateg}},
		{L:'Valor',wxn:'wrapx6',req:'Y',I:{lTag:'input',type:'text',numberformat:'mil',name:'bal',min:0}},
		{L:'T.C Cuotas',wxn:'wrapx6',I:{lTag:'number',min:0,'class':'credCuotas ',name:'credCuotas'}},
		{wxn:'wrapx8',L:'Prox.',_i:'Si marca SI permite identificar que forma parte del próximo corte, RECUERDE: debe desmarcarlas al finalizar el corte',req:'Y',I:{lTag:'select','class':'cred1Pay ',name:'cred1Pay',opts:$V.NY,noBlank:'Y'}},
		{L:'$ Cuota',wxn:'wrapx6',I:{lTag:'$',name:'credBalCuota'}},
		{divLine:1,wxn:'wrapx4',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo'}}
	]
	});
},
editLine:function(L,td){
	var wid='gfpMovLineEdit';
	$1.delet($1.q('#'+wid));
	td.style.position='relative'; var jsF=$Api.JS.cls;
	var wi=$1.t('div',{id:wid,style:'position:absolute; top:90%; left:0; backgroundColor:#FFF; padding:6px; border:1px solid #000;'},td);
	if(L.editar=='bal'){
		var bal=(L.debBal>0)?L.debBal:L.creBal;
		var inp=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'bal',min:0,value:bal},wi);
	}
	inp.AJs={mid:L.mid};
		var resp=$1.t('div',0,wi);
		$Api.send({PUT:Api.Gfp.a+'mov/one',textNode:'Modificar',jsBody:wi,loade:resp,func:function(Jr){
			$Api.resp(resp,Jr);
		}},wi);
		$1.T.btnFa({textNode:'Cerrar',fa:'fa_close',func:function(){ $1.delet(wi); }},wi);
}
}

Gfp.Ctc={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal/bol', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Billetera','Bolsillo','Saldo']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:L.wallName},tr);
				$1.t('td',{textNode:L.wbName},tr);
				$1.t('td',{textNode:$Str.money(L.amount)},tr);
			}
		}
	}});
},
}
Gfp.Rep={
	tcp:(P)=>{
		$Api.Rep.base({f:Api.Gfp.a+'rep/tcp',inputs:$1.G.filter(),
		V_C:[{f:'credType',t:'Concepto',_g:$V.gfpCredType},{f:'bal',t:'Valor',fType:'$'}
		],
		V_LP:[
			{f:'wallName',t:'Cuenta'},{f:'compras',t:'Compras',fType:'$'},{f:'interes',t:'Interes',fType:'$'},{f:'otros',t:'Otros',fType:'$'},{f:'pagos',t:'Pagos',fType:'$'},
			{f:'openDate',t:'Apertura'},{f:'closeDate',t:'Cierre'},{f:'payDate',t:'Fecha Pago'}
		],
		V_M:[{f:'docDate',t:'Fecha'},{f:'categId',t:'Categoria',_g:$JsV.gfpCateg},{f:'credType',t:'Concepto',_g:$V.gfpCredType},{f:'bal',t:'Valor',fType:'$'},{f:'credCuotas',t:'Cuotas'},{f:'lineMemo',t:'Detalles'}
		],
		},$M.Ht.cont);
	},
	copen:(P,pare)=>{
		P=(P)?P:{};
		pare=(pare)?pare:$M.Ht.cont;//llamar
		inputs=(P.vGet)?P.vGet:$1.G.filter();
		$Api.Rep.base({f:Api.Gfp.a+'rep/copen',inputs:inputs,
		V_G:[
		{func:(L,td)=>{
			$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Mov.editForm(T.P); }},td);
			$1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
				Gfp.Mov.quotes(T.P);
			}},td);
		}},
		{f:'wallName',t:'Cuenta'},
		{f:'bankId',t:'Banco',_g:$JsV.gfpBank},
		{f:'credSal',t:'Saldo',fType:'$'},
		{f:'credSal',t:'Saldo',fType:'$',totals:'Y'},
		{f:'credBalCuota',t:'$ Cuota',fType:'$'},
		{f:'credCuoNum',t:'Cuotas',fText:Gfp.Fx.cuoTxt},
		{f:'bal',t:'Monto Compra',fType:'$'},
		{f:'credCap',t:'Cap. Pagado',fType:'$'},{f:'credInt',t:'Int. Pagado',fType:'$'},{f:'credOtros',t:'$ Otros',fType:'$'},
		{f:'docDate',t:'Fecha'},
		{f:'categId',t:'Categoria',_g:$JsV.gfpCateg},
		{f:'credType',t:'Concepto',_g:$V.gfpCredType},
		],
		},pare);
	},
	ctotal:(P,pare)=>{
		P=(P)?P:{};
		pare=(pare)?pare:$M.Ht.cont;//llamar
		inputs=(P.vGet)?P.vGet:$1.G.filter();
		$Api.Rep.base({f:Api.Gfp.a+'rep/ctotal',inputs:inputs,
		V_G:[
		{func:(L,td)=>{
			if(L.wallType=='CR'){
				$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Cre.form(T.P); }},td);
			}
			else{ $1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Mov.editForm(T.P,T); }},td); }
			$1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
				Gfp.Mov.quotes(T.P);
			}},td);
		}},
		{f:'wallType',t:'Tipo',_g:$V.gfpWallType},
		{f:'wallName',t:'Cuenta',fText:Gfp.Fx.cuentaTxt},
		{f:'bankId',t:'Banco',fText:Gfp.Fx.bankTxt},
		{f:'credSal',t:'Saldo',fType:'$',totals:'Y'},
		{f:'credBalCuota',t:'$ Cuota',fType:'$'},
		{f:'credCuoNum',t:'Cuotas',fText:Gfp.Fx.cuoTxt},
		{f:'lineMemo',t:'Detalles'},
		{f:'bal',t:'Monto Compra',fType:'$',totals:'Y'},
		{f:'credCap',t:'Cap. Pagado',fType:'$',totals:'Y'},{f:'credInt',t:'Int. Pagado',fType:'$',totals:'Y'},{f:'credOtros',t:'$ Otros',fType:'$',totals:'Y'},
		{f:'docDate',t:'Fecha'},
		{f:'categId',t:'Categoria',fText:Gfp.Fx.categTxt},
		,{f:'credType',t:'Concepto',_g:$V.gfpCredType},
		],
		},pare);
	},
	crQuotes:(P,pare)=>{
		P=(P)?P:{};
		pare=(pare)?pare:$M.Ht.cont;//llamar
		inputs=(P.vGet)?P.vGet:$1.G.filter();
		$Api.Rep.base({f:Api.Gfp.a+'rep/crquotes',inputs:inputs,
		V_G:[
		{func:(L,td)=>{
			if(L.wallType=='CR'){
				$1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Cre.form(T.P); }},td);
			}
			else{ $1.T.btnFa({fa:'fa-pencil',title:'Editar',P:L,func:(T)=>{ Gfp.Mov.editForm(T.P,T); }},td); }
			$1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
				Gfp.Mov.quotes(T.P);
			}},td);
		}},
		{f:'wallType',t:'Tipo',_g:$V.gfpWallType},
		{f:'wallName',t:'Cuenta',fText:Gfp.Fx.cuentaTxt},
		{f:'bankId',t:'Banco',fText:Gfp.Fx.bankTxt},
		{f:'lineNum',t:'No. Cuota'},
		{f:'lineDate',t:'Fecha'},
		{f:'capital',t:'Capital',fType:'$',totals:'Y'},{f:'interes',t:'Interes',fType:'$',totals:'Y'},{f:'otros',t:'Otros',fType:'$',totals:'Y'},
		{f:'saldo',t:'Saldo',fType:'$',totals:'Y'},
		{f:'bal',t:'Monto Compra',fType:'$',totals:'Y'},
		{f:'credCuoNum',t:'Cuotas',fText:Gfp.Fx.cuoTxt},
		{f:'categId',t:'Categoria',fText:Gfp.Fx.categTxt},
		{f:'lineMemo',t:'Detalles'},
		],
		},pare);
	},
}
Gfp.Fx={
	bankTxt:(L)=>{ return (L.mbankId>0)?_g(L.mbankId,$JsV.gfpBank):_g(L.bankId,$JsV.gfpBank); },
	cuentaTxt:(L)=>{ return (L.wallType=='CR')?L.mName:L.wallName; },
	categTxt:(L)=>{ return (L.wallType=='CR')?_g(L.categId,$JsV.gfpCategCR):_g(L.categId,$JsV.gfpCateg); },
	cuoTxt:(L)=>{ return L.credCuoNum*1+' / '+L.credCuotas },
	cuentaCorte:(wrap,feVal)=>{
		sels=$1.q('.gfpCuentaCorte',wrap,'all');
		$1.onchange(sels[0],function(tv,TT){
			$1.T.sel({reLoad:sels[1],opts:[]});
			if(feVal && feVal()==true){}//si es true, evito reactualizar
			else{	tc=_gO(tv,$Tb.gfpWal);
				if(tc.wallType=='TC'){
					$Api.get({f:Api.Gfp.a+'wal/ctc',inputs:'wallId='+tv,func:(R)=>{
						$1.T.sel({reLoad:sels[1],opts:R,optsFTxt:(X)=>{
							sep=X.value.split(' a ');
							return $2d.f(sep[0],'mmm d')+' a '+$2d.f(sep[1],'mmm d');
						}});
					}});
				}
			}
	});
	},
	quotes:(L,td)=>{
		return $1.T.btnFa({fa:'fa-suitcase',title:'Pagos/Interes/Saldo',P:L,func:(T)=>{
			Gfp.Mov.quotes(T.P);
			}},td);
	}
}
$M.liAdd('gfp',[
{_lineText:'Finanzas Personals'},
{k:'gfp.wal',t:'Cuentas',kau:'gfp',mdlActive:'gfp',ini:{fNew:Gfp.Wal.form,f:'gfp.wal', gyp:Gfp.Wal.get}},
{k:'gfpWal.prof',t:'Perfil de Cuenta',kau:'gfp',mdlActive:'gfp',ini:{btnGo:'gfp.movForm', g:Gfp.Wal.prof}},

{k:'gfp.cre',t:'Creditos',kau:'gfp',mdlActive:'gfp',ini:{fNew:Gfp.Cre.form,f:'gfp.cre', gyp:Gfp.Cre.get}},
{k:'gfp.mov',t:'Historal Transacciones',kau:'gfp',mdlActive:'gfp',ini:{btnGo:'gfp.movForm',f:'gfp.mov', gyp:Gfp.Mov.get}},
{k:'gfp.movForm',t:'Registrar Movimiento',kau:'gfp',mdlActive:'gfp',ini:{g:Gfp.Mov.form}},
]);

$M.liRep('gfp',[
	{_lineText:'_REP'},
	{k:'gfpRep.tcp',t:'Control Periodos Tarjetas', kauAssg:'gfp',ini:{f:'gfpRep.tcp'}},
	{k:'gfpRep.copen',t:'Compras a Cuotas', kauAssg:'gfp',ini:{f:'gfpRep.copen'}},
	{k:'gfpRep.ctotal',t:'Pasivos', kauAssg:'gfp',ini:{f:'gfpRep.ctotal'}},
	{k:'gfpRep.crquotes',t:'Cuotas Pagadas', kauAssg:'gfp',ini:{f:'gfpRep.crquotes'}},
],{repM:['gfp']});

$JsV._i({kMdl:'a1',kObj:'gfpCateg',mdl:'a1',liTxtG:'Categorias - I/G',liTxtF:'Categoria - I/G'});
$JsV._i({kMdl:'a1',kObj:'gfpCategCR',mdl:'a1',liTxtG:'Categorias Creditos',liTxtF:'Categoria Credito'});
$JsV._i({kMdl:'a1',kObj:'gfpWallPrp',mdl:'a1',liTxtG:'Propiedades Cuentas',liTxtF:'Propiedade Cuenta'});
$JsV._i({kMdl:'a1',kObj:'gfpBank',mdl:'a1',liTxtG:'Bancos',liTxtF:'Banco'});

$M.kLiTable['gfp']={mdlActive:'gfp',MLis:['gfp.wal','gfp.cre','gfp.mov'],_F:[
  {rep:['gfpRep.tcp','gfpRep.copen','gfpRep.ctotal','gfpRep.crquotes']},
  {mas:['gfp.ctc','jsv.gfpCateg','jsv.gfpCategCR','jsv.gfpBank','jsv.gfpWallPrp']}
]};