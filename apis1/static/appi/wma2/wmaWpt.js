
$M.kauAssg('wma',[
	{k:'wmaWpt',t:'Partes de Trabajo'},
]);
$M.liAdd('wma',[
{k:'wmaWpt.tickets',t:'Toma Tiempos Planta', kau:'public',func:function(){
	$M.Ht.ini({g:function(){ Wma.Wpt.ini(); }});
}},
{k:'wmaWpt',t:'Partes de Trabajo', kau:'wmaWpt',func:function(){
	$M.Ht.ini({f:'wmaWpt',gyp:function(){ Wma.Wpt.get(); }});
}}
]);


_Fi['wmaWpt']=function(wrap){
	$Doc.filter({func:Wma.Wpt.get},[
		{divLine:1,L:'Fecha',wxn:'wrapx8',I:{tag:'date',name:'A.docDate(E_mayIgual)',value:$2d.today}},
		{L:'Fecha Corte',wxn:'wrapx8',I:{tag:'date',name:'A.docDate(E_menIgual)'}},
		{L:'Estado',wxn:'wrapx8',I:{tag:'select',opts:$V.docStatusOC,name:'A.docStatus',selected:'O'}},
		{L:'Cód. Operación',wxn:'wrapx8',I:{tag:'input',type:'text',name:'I.itemCode'}},
		{L:'Grupo Operación',wxn:'wrapx8',I:{tag:'select',opts:$JsV.wmaWopGr,name:'I.itemGr'}},
		{L:'Empleado',wxn:'wrapx4',I:{tag:'input',type:'text',name:'CC.cardName(E_like3)'}},
		{k:'ordBy'}
	],wrap);
};


Wma.Wpt={
	OLg:function(L){
		var Li=[];
		var ab=new $Doc.liBtn(Li,L,{api:Api.Wma.pr+'wpt',tbSerie:'N'});
		Li.push({k:'edit',ico:'fa fa-pencil',textNode:' Modificar', P:L, func:function(T){ Wma.Wpt.form(T.P); } });
		ab.add('del',{func:function(T){
			$Api.delete({f:Api.Wma.pr+'wpt/one',inputs:'docEntry='+T.P.docEntry,
			confirm:{text:'Se va a eliminar el parte de trabajo, la información no se puede recuperar.'}});
		}});
		return $Opts.add('wmaWpt',Li,L);
	},
	opts:function(P,pare){
		Li={Li:Wma.Wpt.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	ini:function(){
		var cont=$M.Ht.cont;
		$1.T.btnFa({faBtn:'fa-clock-o',textNode:'Apertura de Parte',style:'backgroundColor:var(--cSuccess)',func:function(){
			Wma.Wpt.open(tCont);
		}},cont);
		$1.T.btnFa({faBtn:'fa-crosshairs',textNode:'Cierre de Partes',style:'backgroundColor:var(--cWarning)',func:function(){
			Wma.Wpt.close(tCont);
		}},cont);
		var tCont=$1.t('div',0,cont);
	},
	get:function(){
		var cont=$M.Ht.cont; //$1.q('#wmaWptGet');
		$Doc.tbList({api:Api.Wma.pr+'wpt',inputs:$1.G.filter(),
		tbSerie:'N',fOpts:Wma.Wpt.opts,
		TD:[
			{H:'Estado',k:'docStatus',_g:$V.docStatusOC},
			{H:'Fecha',k:'docDate'},
			{H:'Empleado',k:'mocardName'},
			{H:'Operación',k:'moitemCode'},
			{H:'Cant.',k:'quantity',format:'number'},
			{H:'Udm',k:'moudm',_g:Udm.O},
			{H:'Grupo',k:'itemGr',_g:$JsV.wmaWopGr},
			{H:'Unds Ok',k:'quantity',format:'number'},
			{H:'Unds. Rec.',k:'rejQty',format:'number'},
			{H:'Abierto',k:'dateC'},
			{H:'Cerrado',k:'timeClose'},
		]
		},cont);
	},
	btnNext:function(B,pare){
		B.textNode=(B.textNode)?B.textNode:'';
		var btn=$1.t('div',{'class':'sBtn sBtn-lg sBtn-green',textNode:B.textNode,node:B.node},pare);
		btn.P=B.P;
		btn.onclick=function(){ B.func(this); }
	},
	open:function(pare){
		$1.clear(pare);
		var cont=(pare)?pare:$1.t('div');
		if(pare){ $1.t('h3',{textNode:'Apertura de Parte',style:'backgroundColor:var(--cSuccess)'},cont); }
		var D={};
		var jsF=$Api.JS.cls;
		/* Apertura: Empleado -> grupo operacion -> operacion -> apertura */
		Nom.Crd.Bio.bCode(function(Da){
			midCont.innerHTML='';
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'date','class':jsF,name:'docDate',value:$2d.today}},midCont);
			$1.T.divL({wxn:'wrapx4',L:'Empleado',I:{tag:'input',type:'text',disabled:'disabled','class':jsF,value:Da.cardName,AJs:{mocardId:Da.cardId}} },divL);
			var wL1=$1.t('div',0,midCont);
			var wL2=$1.t('div',0,midCont);
			$1.T.divLTitle('1. Selección Grupo de Operación',wL1);
			for(var i in $JsV.wmaWopGr){ var L=$JsV.wmaWopGr[i];
				Wma.Wpt.btnNext({textNode:L.v,P:L,
				func:function(T){
					trWOP({itemGr:T.P.k},wL2); 
				}},wL1);
			}
		},{pare:cont});
		var midCont=$1.t('div',0,cont);
		function trWOP(P,wL2){ // -> get MO -> open
			$Api.get({f:Api.Wma.pr+'wpt/wopGr',inputs:'itemGr='+P.itemGr,loade:wL2,func:function(Jr){
				$1.T.divLTitle('2. Selección de Operación',wL2);
				for(var i in Jr.L){ L=Jr.L[i];
					var div=$1.t('div');
					$1.t('b',{textNode:L.itemCode},div);
					$1.t('div',{textNode:L.itemName,style:'fontSize:13px;'},div);
					Wma.Wpt.btnNext({node:div,P:L,
					func:function(T){ //-> open data
						$Api.JS.addF({name:'moitemId',value:T.P.itemId},wL2);
						$Api.JS.addF({name:'moudm',value:T.P.udm},wL2);
						$Api.JS.addF({name:'price',value:T.P.invPrice},wL2);
						$Api.post({f:Api.Wma.pr+'wpt/open',jsBody:midCont,winResp:wL2,confirm:{text:'¿Abrir Parte de Trabajo?'}});
					}},wL2);
				}
			}});
		}
		if(!pare){ $1.Win.open(cont,{winTitle:'Apertura de Parte',winSize:'full',winId:'wmaWpt'}); }
	},
	
	close:function(pare){
		$1.clear(pare);
		var cont=(pare)?pare:$1.t('div');
		if(pare){ $1.t('h3',{textNode:'Cierre de Partes',style:'backgroundColor:var(--cWarning)'},cont); }
		Nom.Crd.Bio.bCode(function(Da){
			$Api.get({f:Api.Wma.pr+'wpt/open',inputs:'cardId='+Da.cardId,loade:wLClose,func:function(Jr2){
				$Api.resp(wLClose,Jr2);
				if(!Jr2.errNo){ Wma.Wpt.closeList(Jr2,wLClose); }
			}});
		},{pare:cont});
		var wLClose=$1.t('div',0,cont);
		if(!pare){ $1.Win.open(cont,{winTitle:'Cierre de Partes',winSize:'full',winId:'wmaWpt'}); }
	},
	closeList:function(Jr,pare){
		pare.innerHTML='';
		var cargo=(Jr.L[0].workPosi)?_g(Jr.L[0].workPosi,$JsV.nomWorkPosi):'';
		$1.t('h4',{textNode:Jr.L[0].cardName+', '+cargo},pare);
		var cssRej='backgroundColor:#f5a2a2';
		var tb=$1.T.table(['','Operación','Cant.','Udm','Unds. Ok',{textNode:'Unds. Rechazadas',style:cssRej},{textNode:'O.P',iHelp:'Orden de Producción'},'Eliminar'],0,pare);
		var tBody=$1.t('tbody',0,tb);
		var jsLN=$Api.JS.clsLN;
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			AJs=null;
			tr.addLnIf=['timeQty','quantity',{k:'delete',nV:'N'}];
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			var td=$1.t('td',0,tr);
			$1.lTag({tag:'number','class':jsLN,name:'quantity',style:'width:80px'},td);
			td=$1.t('td',{textNode:_g(L.moudm,Udm.O)},tr);
			var td=$1.t('td',0,tr);
			$1.lTag({tag:'number','class':jsLN,name:'okQty',style:'width:80px',AJs:AJs},td);
			var td=$1.t('td',0,tr);
			$1.lTag({tag:'number','class':jsLN,name:'rejqTy',style:'width:80px;'+cssRej},td);
			var td=$1.t('td',0,tr);
			$1.lTag({tag:'number','class':jsLN,name:'opId',style:'width:80px;'},td);
			var td=$1.t('td',0,tr);
			$1.lineDel(L,{id:'docEntry'},td);
		}
		var resp=$1.t('div',0,pare);
		$Api.send({textNode:'Cerrar Partes',POST:Api.Wma.pr+'wpt/close',jsBody:pare,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
		}},pare);
	},
	
	form:function(Pa){
	var D=$Cche.d(0,{});
	var cont=$1.t('div'); var AJs=null;
	if(!D.docDate){ D.docDate=$2d.today; }
	$Api.get({f:Api.Wma.pr+'wpt/one',inputs:'docEntry='+Pa.docEntry,loadVerif:!Pa.docEntry,loade:cont,func:function(Jr){
		if(Jr.docEntry){ D=Jr; }
		$Doc.form({tbSerie:'N',cont:cont,AJs:AJs,jsF:$Api.JS.cls,POST:Api.Wma.pr+'wpt/one',docEdit:Pa.docEntry,func:D.func,
		HLs:[
		{divLine:1,lTag:'disabled',L:'Empleado',req:'Y',wxn:'wrapx4',I:{value:D.cardName}},
		{lTag:'date',L:'Fecha',req:'Y',wxn:'wrapx4',I:{name:'docDate',value:D.docDate}},
		{lTag:'select',L:'Estado',req:'Y',wxn:'wrapx4',I:{name:'docStatus',opts:$V.docStatusOC,value:D.docStatus,noBlank:'Y'}},
		{lTag:'disabled',L:'Grupo',req:'Y',wxn:'wrapx4',I:{value:_g(D.itemGr,$JsV.wmaWopGr)}},
		{divLine:1,lTag:'disabled',L:'Operación',req:'Y',wxn:'wrapx4',I:{value:D.itemCode}},
		{lTag:'number',L:'Cantidad',wxn:'wrapx4',I:{name:'quantity',value:D.quantity*1}},
		{lTag:'number',L:'Unds. Ok',wxn:'wrapx4',I:{name:'okQty',value:D.okQty*1}},
		{lTag:'number',L:'Unds. Rec.',wxn:'wrapx4',I:{name:'rejQty',value:D.rejQty*1}},
		]
		});
	}});
	$1.Win.open(cont,{winTitle:'Modificar Parte de Trabajo',winSize:'medium'});
},
}

$M.li['itfDT.itmMO']={t:'Mano de Obra', kau:'sysd.supersu', func:function(){
	$M.Ht.ini({g:function(){
		Itf.DT.form({api:Api.Wma.pr+'dtMas/itmMO',fileName:'Plantilla Creacion Mano Obra',helpFie:'Y',k:'itfDT.itmMO',lnLimit:100,
		Li:[
				{trSep:'Datos Generales'},
			{t:'itemCode',d:'Código',xformat:'9z',len:[2,20],req:'Y',desc:'Se recomienda que empieza por <b>MO</b>'},
			{t:'itemName',d:'Nombre del Articulo',len:[1,100],req:'Y'},
			{t:'prdType',d:'Tipo Operación',req:'Y',opts:$V.wopType,optsCsv:1},
			{t:'itemGr',d:'ID del grupo de operación',optsTb:1,opts:$JsV.wmaWopGr,req:'Y'},
			{t:'udm',d:'ID Unidad de Medida',optsTb:1,opts:Udm.O,req:'Y'},
			{t:'invPrice',d:'Coste Unitario en UDM',xformat:'$'},
		]
		});
	}
	});
}};
