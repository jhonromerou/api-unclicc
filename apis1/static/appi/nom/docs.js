
$M.liAdd('nom',[
{_lineText:'Nomina - D'},
{k:'nomD.indu',noTitle:'Y',t:'Formatos Inducciones', kau:'nom', func:function(){
	$M.Ht.ini({g:function(){ Nom.D.Indu.form(); }});
}}
]);

Nom.D={};
Nom.D.Indu={
topInfo:function(Jr,pare){
	$1.t('span',{textNode:'Versión: '+Jr.version},pare);
	$1.t('span',{textNode:'\u0020\u0020Código: '+Jr.docCode},pare);
	$1.t('h4',{textNode:Jr.dbaName},pare);
},
form:function(P){
	var P=(P)?P:{};
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	var api1=Api.Nom.b+'dIndu/form';
	$Api.get({f:api1,loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={go:P.go,docEntry:Pa.docEntry, cont:cont, Series:'N', jsF:jsF,POST:api1,
		func:null,
		tbHead:[
		{lTag:'input',wxn:'wrapx8',L:'Versión',req:'Y',I:{'class':jsF,value:Jr.version,name:'version'}},
		{lTag:'input',wxn:'wrapx8',L:'Código',req:'Y',I:{'class':jsF,value:Jr.docCode,name:'docCode'}},
		{lTag:'input',wxn:'wrapx2',L:'Nombre Auditoria',req:'Y',I:{'class':jsF,value:Jr.dbaName,name:'dbaName'}},
		{divLine:1,lTag:'select',wxn:'wrapx8',L:'Area',req:'Y',I:{'class':jsF,name:'area',value:Jr.area,opts:$V.gdaAreas}},
		{lTag:'select',wxn:'wrapx8',L:'Clase',I:{'class':jsF,name:'docClass',value:Jr.docClass,opts:$V.gdaClass}},
		{lTag:'select',wxn:'wrapx8',L:'Grupo',I:{'class':jsF,name:'docGr',value:Jr.docGr,opts:$V.gdaGr}},
		{lTag:'input',wxn:'wrapx2',L:'Etapas',subText:'Planeacion,Revision,Reunion',I:{'class':jsF,name:'stages',value:Jr.stages}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Objetivo',I:{'class':jsF,name:'objetivo',textNode:Jr.objetivo,style:'resize:vertical'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Alcance',I:{'class':jsF,name:'alcance',textNode:Jr.alcance,style:'resize:vertical'}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Descripción',I:{'class':jsF,name:'descrip',textNode:Jr.descrip,style:'resize:vertical'}}
		],
		};
		if(Pa.docEntry){ tP.PUT=tP.POST; delete(tP.POST); }
		tP.midCont=$1.t('div');
		$Doc.form(tP);
		//Gda.Def.task({isTemplate:P.isTemplate,Jr:Jr},tP.midCont);
	}});
},
task:function(P,pare){
	var cont=pare;
	var Jr=P.Jr;
	if(Jr.errNo){ return $Api.resp(cont,Jr); }
	if(Jr.L && Jr.L.errNo==1){ $Api.resp(wAdd,Jr.L); }
	var wAdd=$1.t('div',0,cont);
	if(Jr.L && Jr.L.errNo){ Jr.L=[]; }
	Jr.L=$js.sortNum(Jr.L,{k:'lineNum'});
	kFie='proceso,assg,docDate,hourTxt,audit,teamAudit,lineMemo';
	if(P.isTemplate){ kFie='proceso,assg,lineMemo'; }
	if(Jr.doStages=='Y'){
		$V.gdaStageDoc=$1.optsFromTxt(Jr.stages);
		kFie='proceso,stage,assg,docDate,hourTxt,audit,teamAudit,lineMemo';
		if(P.isTemplate){ kFie='proceso,stage,assg,lineMemo'; }
	}
	var Rd=$DocTb.ini({xMov:'Y',xNum:'N',xDel:'Y',L:Jr.L,kTb:'gdaDbaL',
	btnAddL:'Y',fieldset:'Tareas de Auditoria',
	kFie:kFie
	});
	wAdd.appendChild(Rd.fieldset);
},
docs:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read();
	$Api.get({f:P.api,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		if(Jr.errNo){ return $Api.resp(cont,Jr); }
		Gda.Def.topInfo(Jr,cont);
		var jsF=$Api.xFields;
		var wAdd=$1.t('div',0,cont);
		var wList=$1.t('div',0,cont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tipo',I:{tag:'select','class':jsF,name:'docType',opts:$V.gdaDocType,noBlank:'Y'}},wAdd);
		$1.T.divL({wxn:'wrapx4',L:'Nombre Documento',req:'Y',I:{tag:'input',type:'text','class':jsF,name:'docName',AJs:{docEntry:Pa.docEntry}}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Archivo',req:'Y',I:{tag:'input',type:'file','class':jsF,name:'file'}},divL);
		var resp=$1.t('div',0,wAdd);
		$Api.send({textNode:'Añadir Archivo',POST:P.api,formData:wAdd,loade:resp,func:function(Jr2,o){
			$Api.resp(resp,Jr2);
			if(o && o.fileId){
				Gda.Dba.trADocs(o,tBody);
			}
		}},wAdd);
		//lista
		if(Jr.L && !Jr.L.errNo){
			var tb=$1.T.table(['Tipo','Nombre','Archivo'],0,wList);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ Gda.Def.trADocs(Jr.L[i],tBody); }
		}
	}});
},
trADocs:function(L,tBody){
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:_g(L.docType,$V.gdaDocType)},tr);
	$1.t('td',{textNode:L.docName},tr);
	var td=$1.t('td',0,tr);
	Attach.btnView(L,td,{btnDelete:'N'});
},
view:function(P){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:P.api+'/view',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		Jr.L=$js.sortBy('lineNum',Jr.L);
		var tP={D:Jr,
			btnsTop:P.btnsTop,
			THs:[
				{t:'Proceso',k:'dbaName',cs:5},{k:'version',t:'Versión',ln:1},
				{logo:'Y',ln:'N'},{middleInfo:'Y'},{k:'docCode',t:'Código',ln:1},
				{t:'Area',k:'area',_V:'gdaAreas'},
				{t:'Clase',k:'docClass',_V:'gdaClass'},
				{k:'objetivo',cs:8,addB:$1.t('b',{textNode:'Objetivo:\u0020'}),Tag:{'class':'pre'}},
				{k:'alcance',cs:8,addB:$1.t('b',{textNode:'Alcance:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',fieldset:'Programacion',tb:{style:'fontSize:14px'},TLs:[
				{t:'No.',k:'lineNum'},
				{t:'Etapa',k:'stage',_V:'gdaStageDoc'},
				{k:'proceso',t:'Actividad'},
				{k:'assg',t:'Responsable'},
				{k:'lineMemo',t:'Detalles',Tag:{'class':'pre'}}
			]}
			],
			TFs:null
		};
		tP.mTL=[
		{L:'L',fieldset:'Programacion',tb:{style:'fontSize:14px'},TLs:[
			{t:'No.',k:'lineNum'},
			{t:'Etapa',k:'stage',_V:'gdaStageDoc'},
			{k:'proceso',t:'Actividad'},
			{k:'assg',t:'Responsable'}
		]}
		];
		if(P.isTemplate!='Y'){ 
			tP.mTL[0].TLs.push({k:'docDate',t:'Fecha'});
			tP.mTL[0].TLs.push({k:'hourTxt',t:'Horario'});
			tP.mTL[0].TLs.push({k:'audit',t:'Auditor'});
			tP.mTL[0].TLs.push({k:'teamAudit',t:'Equipo Auditor'});
		}
		tP.mTL[0].TLs.push({k:'lineMemo',t:'Detalles',Tag:{'class':'pre'}});
		if(Jr.doStages=='N'){ delete(tP.mTL[0].TLs[1]); }
		$V.gdaStageDoc=$1.optsFromTxt(Jr.stages);
		$Doc.view(cont,tP);
	}});
}
}