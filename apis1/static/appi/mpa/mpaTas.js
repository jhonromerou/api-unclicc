/* GeSN!
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
$MdlStatus.put('mpaTas','Y');

$M.liAdd('mpa',[
{k:'mpaTas',t:'Tareas', kau:'mpaTas', func:function(){
	$M.Ht.ini({f:'mpaTas',gyp:function(){ Mpa.Tas.xHtml[1]=$M.Ht.cont; Mpa.Tas.get(); }
	});
}}
]);

_Fi['mpaTas']=function(w){
	Mpa.Tas.Fi.F.push(['Contacto',{tag:'input',name:'P.name(E_like3)'}]);
	Mpa.Tas.Fi.F.push(['Socio',{tag:'input',name:'C.cardName(E_like3)'}]);
	Mpa.Tas.profile='A';
	Mpa.Tas.xHtml=[];
	Mpa.Tas.xHtml[0]=w;
	Mpa.Tas.ini({openAlw:'Y'});
}

Mpa.Tas={
view:'list',
profile:false,//A,crd,cpr
gData:['',{}],//&cardId=1
openAlw:'Y',//Abrir siempre
xHtml:[],//0-filt,1-list
ini:function(P){
	var P=(P)?P:{};
	if(Mpa.Tas.openAlw=='Y' || P.openAlw=='Y'){
		Mpa.Tas.Fi.form({cont:Mpa.Tas.xHtml[0]});
		$1.addBtnFas([
		{fa:'fa_doc',textNode:'Nueva',func:function(){ Mpa.Tas.form(); }}
		],Mpa.Tas.xHtml[0]);
		Mpa.Tas.get();
	}
	Mpa.Tas.openAlw=P.openAlw;
},
Fi:function(){
	return new $Api.Filt({active:'Y',cols:2,
	func:function(T){ Mpa.Tas.get(T); },
	F:[
		['Completas',{tag:'YN',name:'A.completed',selected:'N',noBlank:'Y'}],
		['Completada',{TA:[{tag:'date',name:'A.completedAt(E_mayIgual)'},{tag:'date',name:'A.completedAt(E_menIgual)'}]}],
		['Vencimiento',{TA:[{tag:'date',name:'A.dueDate(E_mayIgual)'},{tag:'date',name:'A.dueDate(E_menIgual)'}]}],
		['Responsable',{tag:'userAssg',name:'A.userAssg'}],
		['Asunto',{tag:'input',name:'A.title(E_like3)'}],
		['Tipo',{tag:'select',name:'A.gType',opts:$JsV.mpaTasType}],
		['Prioridad',{tag:'select',name:'A.gPrio',opts:$JsV.mpaTasPrio}],
		['Gestión',{tag:'select',name:'A.gStatus',opts:$JsV.mpaTasStatus}],
		['Creada',{TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]}]
	]
});
},
Lg:function(L,men){
	var Li=[]; var n=0;
 Li.push({k:'edit',ico:'fa fa-pencil',textNode:'Modificar',P:L,func:function(T){ Mpa.Tas.form(T.P); }});
	Li.push({k:'edit',ico:'fa fa-envelope',textNode:'Enviar Copia',P:L,func:function(T){ Mpa.Tas.Ht.sendCopy(T.P); }});
	Li= $Opts.add('mpaTask',Li,L);
	if(men){
		Li={Li:Li,PB:L};
		return $1.Menu.winLiRel(Li,men);
	}
	return Li;
},
get:function(T){
	var vPost =Mpa.Tas.Fi.get()+Mpa.Tas.gData[0];
	var wList=Mpa.Tas.xHtml[1];
	$Api.get({f:Api.Mpa.b+'tas',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ Mpa.Tas.viewTb(Jr,wList); }
	}
	});
},
viewTb:function(Jr,wList){
	var tb=  $1.T.table(['','Tarea','Tipo','Gestión','Prioridad','Vencimiento','Responsable','Contacto','Tercero'],{'class':'table_lucid1'},wList);
	var tB=$1.t('tbody',0,tb);
	for(var i in Jr.L){ Mpa.Tas.drawRow(Jr.L[i],tB); }
},
drawRow:function(L,wPare){
	var L=Mpa.Tas.Ht.vD(L);
	var ide='_mpaTas_'+L.gid;
	var tr=$1.uniNode($1.t('tr',{id:ide}),wPare);
	var td=$1.t('td',0,tr);
	Mpa.Tas.Ht.ck(L,td);
	Mpa.Tas.Lg(L,td);
	$1.t('td',{textNode:L.title,'class':'mPointer',P:L},tr).onclick=function(){
		Mpa.Tas.view(this.P);
	};
	Mpa.Tas.Ht.type(L,$1.t('td',0,tr));
	Mpa.Tas.Ht.status(L,$1.t('td',0,tr));
	Mpa.Tas.Ht.prio(L,$1.t('td',0,tr));
	$1.t('td',{textNode:L.endDateText,xls:{t:L.endDateFull}},tr);
	$1.t('td',{textNode:L.userAssgText},tr);
	$1.t('td',{textNode:L.prsName},tr);
	$1.t('td',{textNode:L.cardName},tr);
},
form:function(P){ var P=(P)?P:{};
	var fP={Win:{title:'Tarea'},
	vidn:'gid',gid:P.gid,jsAdd:Mpa.Tas.gData[1],api:Api.Mpa.b+'tas',
	Cols:[
	['Asunto',{k:'title',T:{divLine:1,req:'Y',wxn:'wrapx6_1',tag:'input'}}],
	['Agendar',{k:'calId',T:{wxn:'wrapx6',tag:'select',opts:$Tb.crmOcal,noBlank:'Y',opt1:{k:0,t:'No'}}}],
	['Tipo',{k:'gType',T:{divLine:1,req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaTasType}}],
	['Prioridad',{k:'gPrio',T:{wxn:'wrapx4',tag:'select',opts:$JsV.mpaTasPrio}}],
	['Gestión',{k:'gStatus',T:{wxn:'wrapx4',tag:'select',opts:$JsV.mpaTasStatus}}],
	['Responsable',{k:'userAssg',T:{divLine:1,wxn:'wrapx2',tag:'select',opts:$Tb.ousr}}],
	['Vencimiento',{k:'endDate',T:{kTxt:'value',wxn:'wrapx2',tag:'btnDate',time:'Y',fText:function(L){
		return L.endDate+' '+L.endDateAt;
	}}}]
	],
	fCont:function(Jr,pare,Px){
		var divL=$1.t('div',0,pare);
		var inpRep=$1.t('input',{type:'hidden','class':Px.jsF,name:'repeatKey'},divL);
		$DateRep.form({repeatKey:Jr.repeatKey,inpRep:inpRep,func:function(tk){
			inpRep.value=tk;
		}},divL);
	},
	delCont:'Y',
	oFunc:function(o){
		Mpa.Tas.drawRow(o);
	}
	}
	if(Mpa.Tas.profile=='A' || Mpa.Tas.profile=='crd'){
		fP.Cols.push(['Contacto',{k:'name',T:{divLine:1,wxn:'wrapx2',tag:'cpr'}}]);
	}
	if(Mpa.Tas.profile=='A' || Mpa.Tas.profile=='cpr'){
		fP.Cols.push(['Socio Neg.',{k:'cardName',T:{wxn:'wrapx2',tag:'crd'}}]);
	}
	fP.Cols.push(['Descripción',{k:'shortDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'textarea'}}]);
	var x=$Api.form(fP);
},
viewGrid:function(T){
	vPost =Mpa.Tas.Fi.get()+Mpa.Tas.gData[0];
	var wList=Mpa.Tas.xHtml[1];
	$Api.get({f:Api.Mpa.b+'tas',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ leer(Jr); }
	},moreWrap:wList,moreFunc:leer
	});
	function leer(Jr){
		if(!Jr.errNo){
			for(var i in Jr.L){
				Mpa.Tas.drawRow(Jr.L[i]);
			}
		}
	}
},
drawRowGrid:function(L,wList){
	L=Mpa.Tas.Ht.vD(L);
	var wList=Mpa.Tas.xHtml[1];
	var ide='_mpaTas_'+L.gid;
	var div=$1.uniNode($1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'}),wList);
	Mpa.Tas.Ht.ck(L,div);
	$1.t('span',{textNode:L.title,'class':'mPointer _oOpen',P:L},div).onclick=function(){ Mpa.Tas.view(this.P); }
	Mpa.Tas.Ht.edit(L,div);
	Mpa.Tas.Ht.sendCopy(L,{pare:div});
	Mpa.Tas.Ht.type(L,div);
	Mpa.Tas.Ht.prio(L,div);
	Mpa.Tas.Ht.status(L,div);
	Mpa.Tas.Ht.endDate(L,div);
	Mpa.Tas.Ht.userAssg(L,div);
	Mpa.Tas.Ht.profileBot(L,div);
	Mpa.Tas.Ht.nums(L,div);
	Mpa.Tas.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=$1.t('div',0); var jsF='jsFields';
	$Api.get({f:Api.Mpa.b+'tas/'+P.gid,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tipo',iT:{k:'gType',_JsV:'mpaTasType'}},
		{wxn:'tbf_x4',t:'Prioridad',iT:{k:'gPrio',_JsV:'mpaTasPrio'}},
		{wxn:'tbf_x4',t:'Vencimiento',v:$2d.f(Jr.endDate+' '+Jr.endDateAt,'mmm d H:i')},
		{wxn:'tbf_x4',t:'Responsable',iT:{k:'userAssg',_gTb:'ousr'}},
		{line:1,wxn:'tbf_x1',t:'Asunto',v:Jr.title},
		{line:1,wxn:'tbf_x2',t:'Contacto',v:Jr.name},
		{wxn:'tbf_x2',t:'Socio Negocios',v:Jr.cardName},
		{line:1,wxn:'tbf_x1',t:'Detalles',v:Jr.shortDesc,T:{'class':'pre'}},
		{line:1,wxn:'tbf_x1',t:'Repetición',iT:{k:'repeatKey',fText:function(L){ if(L.repeatKey!='N'){ return $DateRep.readTxt(L.repeatKey); } }}},
		],cont,Jr);
		Mpa.Tas.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Tarea'});
},
}
Mpa.Tas.sendTaskCopy=function(P){
	var wrap=$1.t('div');
	if(!P.gid){ $Api.resp(wrap,{text:'No se ha definido Id de tarea'}); }
	else{
		$1.t('h5',{textNode:P.title},wrap);
		$Api.JS.addF({name:'gid',value:P.gid},wrap);
		var tb=$1.T.table(['Email',''],0,wrap);
		var tBody=$1.t('tbody',0,tb);
		function trA(){
			var jsF=$Api.JS.clsAr;
			var tr=$1.t('tr',0,tBody);
			$1.t('input',{type:'text','class':jsF,name:'email'},$1.t('td',0,tr));
			$1.T.btnFa({fa:'fa_close',func:function(T){ $1.delet(T.parentNode.parentNode); }},$1.t('td',0,tr));
		}
		$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir',func:function(){
			var nds=$1.q('tr',wrap,'all');
			if(nds.length>2){ $Api.resp(resp,{text:'No se puede enviar a más de 2 contactos'}); }
			else{ trA(); }
		}},wrap);
		var resp=$1.t('div',0,wrap);
		$Api.send({POST:Api.Mpa.b+'tas/sendCopy',jsBody:wrap,loade:resp,func:function(Jr){
			if(Jr.errNo){ $Api.resp(resp,Jr); }
			else{ $Api.resp(wrap,Jr); }
		}},wrap);
		trA();
	}
	$1.Win.open(wrap,{winTitle:'Enviar Copia de Tarea a Correo',winSize:'medium'});
}

Mpa.Tas.Ht={
vD:function(L){
	L.doDateFull=L.doDate+' '+L.doDateAt;
	L.endDateFull=L.endDate+' '+L.endDateAt;
	if(L.gPrio){ L.prioText=_g(L.gPrio,$JsV.mpaTasPrio); }
	var oT=_gO(L.gType,$JsV.mpaTasType);;
	L.typeText=oT.v;
	L.typeCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	var oT=_gO(L.gStatus,$JsV.mpaTasStatus);
	L.statusText=oT.v;
	L.statusCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.endDate)){ L.endDateDefine=true;
		L.endDateText=$2d.f(L.endDate+' '+L.endDateAt,'mmm d H:i');
	}
	if(L.prsId>0){ L.prsDefine=true; }
	if(L.cardId>0){ L.cardDefine=true; }
	L.prsName=(L.prsName)?L.prsName:'';
	L.cardName=(L.cardName)?L.cardName:'';
	return L;
},
ck:function(L,div){
	return $1.T.check({name:'completed','iClass':'fancyCheck','class':$Api.JS.cls,value:L.completed,P:L,AJs:{gid:L.gid},func:function(T){
		//vPostS='gid='+T.P.gid+'&'+$1.G.inputs(T.parentNode);
		$Api.put({f:Api.Mpa.b+'tas/markCompleted',winErr:3,jsBody:T.pare,func:function(Jr2){}});
	}},div);
},
edit:function(L,div){
	var divR=$1.t('div',{style:'float:right'},div);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		Mpa.Tas.form(T.P);
	}},divR);
},
prio:function(L,divT){
	if(L.gPrio>0){
		return $1.t('div',{title:'Prioridad','class':'badge fa fa_prio',textNode:L.prioText},divT);
	}
},
type:function(L,divT){
	var div=$1.t('div',{textNode:L.typeText,title:'Tipo Tarea','class':'badge bf-n2',style:L.typeCss},divT);
	return div;
},
status:function(L,divT){
	if(L.gStatus>0){
	var div=$1.t('div',{textNode:L.statusText,title:'Estado Gestión','class':'badge bf-n1',style:L.statusCss},divT);
	}
	return div;
},
endDate:function(L,divT){
	if(L.endDateDefine){
		$1.t('div',{title:'Vencimiento','class':'badge fa fa-calendar',textNode:L.endDateText},divT);
		if(L.repeatKey && L.repeatKey!='N'){
		$1.t('span',{'class':'badge fa fa-repeat',style:'color:blue',title:'Esta tarea se repite asi:' +$DateRep.readTxt(L.repeatKey)},divT);
		}
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'badge bf-n3 fa fa-user',textNode:L.userAssgText},divT);
	}
},
profileBot:function(L,divP){
	if(Mpa.Tas.profile=='A'){
		Mpa.Tas.Ht.prs(L,divP);
		Mpa.Tas.Ht.crd(L,divP);
	}
	else if(Mpa.Tas.profile=='crd'){
		Mpa.Tas.Ht.prs(L,divP);
	}
	else if(Mpa.Tas.profile=='cpr'){
		Mpa.Tas.Ht.crd(L,divP);
	}
},
shortDesc:function(L,divT){
	if(L.shortDesc && L.shortDesc!=''){
		div= $1.t('div',{textNode:L.shortDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
nums:function(L,divT){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
sendCopy:function(L,P){
	var divR=$1.t('div',{style:'float:right'});
	$1.T.btnFa({fa:'fa-envelope',title:'Enviar Copia a Correo',P:L,func:function(T){
		Mpa.Tas.sendTaskCopy(T.P);
	}},divR);
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		lineText=(L.prsName)?L.prsName:lineText;
		var div=$1.t('div',{title:'Contacto','class':'mPointer badge bf-n4 fa fa-handshake-o',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='prsId:'+L.prsId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCpr.view',pars);
		}
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		lineText=(L.cardName)?L.cardName:lineText;
		var div= $1.t('div',{title:'Socio de Negocios','class':'mPointer badge bf-n4 fa fa-institution',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='cardId:'+L.cardId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCrd.view',pars);
		}
		return div;
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(cFol,_5fw); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	var _5c=Wins['win5c'];
	var _5f=Wins['win5f']
	var inf=Wins['winInfo'];
	//Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},_5c);
	//Attach.Tt.form({tt:'crmNov',tr:L.gid, addLine:'Y',func:function(Jrr,o){}},_5f);
	var _5fw=$1.t('div',0,_5f);
	var fc=L.completedAt;
	var fcU=_g(L.completuser,$Tb.ousr);
	if($2d.is0(L.completedAt)){ fc='Sin Completar'; fcU='Sin Completar'; }
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creada',v:L.dateC},
		{wxn:'tbf_x4',t:'Creada por',v:_g(L.userId,$Tb.ousr)},
		{line:1,wxn:'tbf_x4',t:'Fecha Completada',v:fc},
		{wxn:'tbf_x4',t:'Completada por',v:fcU},
		],inf);
}

};

$JsV._i({kObj:'mpaTasType',mdl:'mpa',liK:'mpaJsv',liTxtG:'Tipo de Actividades',liTxtF:'Tipo Actividad (Form)'});
$JsV._i({kObj:'mpaTasPrio',mdl:'mpa',liK:'mpaJsv',liTxtG:'Prioridad de Actividades',liTxtF:'Prioridad Actividad (Form)'});
$JsV._i({kObj:'mpaTasStatus',mdl:'mpa',liK:'mpaJsv',liTxtG:'Gestión de Actividades',liTxtF:'Gestión Actividad (Form)'});
