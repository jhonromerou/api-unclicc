/* GeSN!
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
$MdlStatus.put('mpaNov','Y');

$M.liAdd('mpa',[
{k:'mpaNov',t:'Novedades', kau:'mpaNov', func:function(){
	$M.Ht.ini({f:'mpaNov',gyp:function(){ Mpa.Nov.xHtml[1]=$M.Ht.cont; Mpa.Nov.get(); }
	});
}}
]);

_Fi['mpaNov']=function(w){
	Mpa.Nov.Fi.F.push(['Contacto',{tag:'input',name:'P.name(E_like3)'}]);
	Mpa.Nov.Fi.F.push(['Socio',{tag:'input',name:'C.cardName(E_like3)'}]);
	Mpa.Nov.profile='A';
	Mpa.Nov.xHtml=[];
	Mpa.Nov.xHtml[0]=w;
	Mpa.Nov.ini({openAlw:'Y'});
}

Mpa.Nov={
profile:false,//A,crd,cpr
gData:['',{}],//&cardId=1
openAlw:'Y',//Abrir siempre
xHtml:[],//0-filt,1-list
ini:function(P){
	var P=(P)?P:{};
	if(Mpa.Nov.openAlw=='Y' || P.openAlw=='Y'){
		Mpa.Nov.Fi.form({cont:Mpa.Nov.xHtml[0]});
		$1.addBtnFas([
		{fa:'fa_doc',textNode:'Nueva',func:function(){ Mpa.Nov.form(); }}
		],Mpa.Nov.xHtml[0]);
		Mpa.Nov.get();
	}
	Mpa.Nov.openAlw=P.openAlw;
},
Fi:new $Api.Filt({active:'Y',
	func:function(T){ Mpa.Nov.get(T); },
	F:[
		['Titulo',{tag:'input',name:'A.title(E_like3)'}],
		['Tipo',{tag:'select',name:'A.gType',opts:$JsV.mpaNovType}],
		['Prioridad',{tag:'select',name:'A.gPrio',opts:$JsV.mpaNovPrio}],
		['Responsable',{tag:'userAssg',name:'A.userAssg'}],
		['Fecha Registro',{TA:[{tag:'date',name:'A.docDate(E_mayIgual)'},{tag:'date',name:'A.docDate(E_menIgual)'}]}],
		['Creada',{TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]}]
	]
}),
get:function(T){
	vPost =Mpa.Nov.Fi.get()+Mpa.Nov.gData[0];
	var wList=Mpa.Nov.xHtml[1];
	$Api.get({f:Api.Mpa.b+'nov',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		else{
			for(var i in Jr.L){
				Mpa.Nov.drawRow(Jr.L[i]);
			}
		}
	}});
},
form:function(P){ var P=(P)?P:{};
	var fP={Win:{winTitle:'Novedad'},
	vidn:'gid',gid:P.gid,jsAdd:Mpa.Nov.gData[1],api:Api.Mpa.b+'nov',
	Cols:[
	['Asunto',{k:'title',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'input'}}],
	['Origen',{k:'gOri',T:{divLine:1,req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaNovOri}}],
	['Tipo',{k:'gType',T:{req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaNovType}}],
	['Prioridad',{k:'gPrio',T:{wxn:'wrapx4',tag:'select',opts:$JsV.mpaNovPrio}}],
	['Responsable',{k:'userAssg',T:{divLine:1,wxn:'wrapx2',tag:'select',opts:$Tb.ousr}}],
	['Fecha',{k:'docDate',T:{wxn:'wrapx2',tag:'date'}}]
	],
	delCont:'Y',
	oFunc:function(o){
		Mpa.Nov.drawRow(o);
	}
	}
	if(Mpa.Nov.profile=='A' || Mpa.Nov.profile=='crd'){
		fP.Cols.push(['Contacto',{k:'name',T:{divLine:1,wxn:'wrapx2',tag:'cpr'}}]);
	}
	if(Mpa.Nov.profile=='A' || Mpa.Nov.profile=='cpr'){
		fP.Cols.push(['Socio Neg.',{k:'cardName',T:{wxn:'wrapx2',tag:'crd'}}]);
	}
	fP.Cols.push(['Descripción',{k:'longDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'textarea'}}]);
	var x=$Api.form(fP);
},
drawRow:function(L,wList){
	L=Mpa.Nov.Ht.vD(L);
	var wList=Mpa.Nov.xHtml[1];
	var ide='_mpaNov_'+L.gid;
	var div=$1.uniNode($1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'}),wList);
	$1.t('span',{textNode:L.title,'class':'mPointer _oOpen',P:L},div).onclick=function(){ Mpa.Nov.view(this.P); }
	Mpa.Nov.Ht.edit(L,div);
	Mpa.Nov.Ht.type(L,div);
	Mpa.Nov.Ht.prio(L,div);
	Mpa.Nov.Ht.doDate(L,div);
	Mpa.Nov.Ht.userAssg(L,div);
	Mpa.Nov.Ht.profileBot(L,div);
	Mpa.Nov.Ht.nums(L,div);
	Mpa.Nov.Ht.longDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=$1.t('div',0); var jsF='jsFields';
	$Api.get({f:Api.Mpa.b+'nov/'+P.gid,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tipo',iT:{k:'gType',_JsV:'mpaNovType'}},
		{wxn:'tbf_x4',t:'Prioridad',iT:{k:'gPrio',_JsV:'mpaNovPrio'}},
		{wxn:'tbf_x4',t:'Fecha',iT:{k:'docDate'}},
		{wxn:'tbf_x4',t:'Responsable',iT:{k:'userAssg',_gTb:'ousr'}},
		{line:1,wxn:'tbf_x1',t:'Asunto',iT:{k:'title'}},
		{line:1,wxn:'tbf_x2',t:'Contacto',iT:{k:'name'}},
		{wxn:'tbf_x2',t:'Socio Negocios',iT:{k:'cardName'}},
		{line:1,wxn:'tbf_x1',t:'Descripción',iT:{k:'longDesc','class':'pre'}},
		],cont,Jr);
		Mpa.Nov.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Novedad'});
},
}

Mpa.Nov.Ht={
vD:function(L){
	if(L.gPrio){ L.prioText=_g(L.gPrio,$JsV.mpaNovPrio); }
	var oT=_gO(L.gType,$JsV.mpaNovType);;
	L.typeText=oT.v;
	L.typeCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.docDate)){ L.docDefine=true;
		L.docDateText=$2d.f(L.docDate,'mmm d');
	}
	if(L.prsId>0){ L.prsDefine=true; }
	if(L.cardId>0){ L.cardDefine=true; }
	return L;
},
edit:function(L,div){
	var divR=$1.t('div',{style:'float:right'},div);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		Mpa.Nov.form(T.P);
	}},divR);
},
prio:function(L,divT){
	if(L.gPrio>0){
		return $1.t('div',{title:'Prioridad','class':'crm_prpLine fa fa_prio',textNode:L.prioText},divT);
	}
},
type:function(L,divT){
	var div=$1.t('div',{textNode:L.typeText,title:'Tipo Tarea','class':'crm_prpLine',style:L.typeCss},divT);
	return div;
},
doDate:function(L,divT){
	if(L.doDateDefine){
		$1.t('div',{title:'Vencimiento','class':'fa fa-calendar crm_prpLine',textNode:L.doDateText},divT);
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'fa fa-user crm_prpLine',textNode:L.userAssgText},divT);
	}
},
profileBot:function(L,divP){
	if(Mpa.Nov.profile=='A'){
		Mpa.Nov.Ht.prs(L,divP);
		Mpa.Nov.Ht.crd(L,divP);
	}
	else if(Mpa.Nov.profile=='crd'){
		Mpa.Nov.Ht.prs(L,divP);
	}
	else if(Mpa.Nov.profile=='cpr'){
		Mpa.Nov.Ht.crd(L,divP);
	}
},
longDesc:function(L,divT){
	if(L.longDesc && L.longDesc!=''){
		div= $1.t('div',{textNode:L.longDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
nums:function(L,divT){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments crm_prpLine',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		lineText=(L.prsName)?L.prsName:lineText;
		var div=$1.t('div',{title:'Contacto','class':'mPointer fa fa-handshake-o crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='prsId:'+L.prsId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCpr.view',pars);
		}
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		lineText=(L.cardName)?L.cardName:lineText;
		var div= $1.t('div',{title:'Socio de Negocios','class':'mPointer fa fa-institution crm_prpLine',textNode:lineText},divB);
		return div; //corregir
		div.onclick=function(){
			var pars='cardId:'+L.cardId;
			if($M.Pa.tab){ pars += ',tab:'+$M.Pa.tab; }
			$M.to('crmCrd.view',pars);
		}
		return div;
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(cFol,_5fw); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	var _5c=Wins['win5c'];
	var _5f=Wins['win5f']
	var inf=Wins['winInfo'];
	//Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},_5c);
	//Attach.Tt.form({tt:'crmNov',tr:L.gid, addLine:'Y',func:function(Jrr,o){}},_5f);
	var _5fw=$1.t('div',0,_5f);
	var fc=L.completedAt;
	var fcU=_g(L.completuser,$Tb.ousr);
	if($2d.is0(L.completedAt)){ fc='Sin Completar'; fcU='Sin Completar'; }
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creada',v:L.dateC},
		{wxn:'tbf_x4',t:'Creada por',v:_g(L.userId,$Tb.ousr)},
		{line:1,wxn:'tbf_x4',t:'Actualizada',v:fc},
		{wxn:'tbf_x4',t:'Actualizada por',v:fcU},
		],inf);
}

};

$JsV._i({kObj:'mpaNovOri',mdl:'mpa',liK:'mpaJsv',liTxtG:'Origen de Novedades',liTxtF:'Origen Novedad (Form)'});
$JsV._i({kObj:'mpaNovType',mdl:'mpa',liK:'mpaJsv',liTxtG:'Tipo de Novedades',liTxtF:'Tipo Novedad (Form)'});
$JsV._i({kObj:'mpaNovPrio',mdl:'mpa',liK:'mpaJsv',liTxtG:'Prioridad de Novedades',liTxtF:'Prioridad Novedad(Form)'});
