/* GeSN!
gestion, oportunidades, notas, requerimientos, emails, agenda, actividades
*/
$MdlStatus.put('mpa','Y');
Api.Mpa = {b:'/js/mpa/'};

_Fi['mpaCrd']=function(wrap,func){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsV,name:'A.cardCode(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.cardName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Responsable Venta'},I:{tag:'select',sel:{'class':jsV,name:'A.slpId(E_igual)'},opts:$Tb.oslp}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Grupo',I:{tag:'select','class':jsV,name:'A.grId(E_igual)',opts:$JsV.parGrC}},divL);
$1.T.btnSend({textNode:'Actualizar', func:Mpa.Crd.list},wrap);
};

var Mpa={};

$M.liAdd('mpa',[
{_lineText:'Manager de Socios Negocios'},
{k:'mpaCrd.list',noTitle:'Y',t:'Gestión de Terceros', kau:'mpaCrd', func:function(){
	$M.Ht.ini({f:'mpaCrd',gyp:function(){ Mpa.Crd.list(); }});
}},
{k:'mpaCrd',noTitle:'Y',t:'Perfil Administración Socio de Negocios', kau:'mpaCrd', func:function(){
	$M.Ht.ini({g:function(){ Mpa.Crd.profile(); }});
}}
]);

Mpa.Crd={
list:function(P){
	var P=(P)?P:{};
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost='';
	if(P.vPost){ vPost += '&'+P.vPost; }
	vPost+='&'+$1.G.filter()
	$Api.get({f:Api.Mpa.b+'crd',inputs:vPost, loade:cont, func:function(Jr){
		if(P.func){ P.func(Jr); return true; }
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['','Código','Nombre','Grupo','Resp. Ventas','Tel. 1','Tel. 2','Email']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			$1.t('td',{textNode:L.cardCode},tr);
			var td=$1.t('td',0,tr);
			$1.t('a',{href:$M.to('mpaCrd','cardId:'+L.cardId,'r'), textNode:L.cardName,'class':'fa fa_eye'},td);
			$1.t('td',{textNode:$V._g('crdGroup',L.grId)},tr);
			$1.t('td',{textNode:L.phone1},tr);
			$1.t('td',{textNode:L.phone2},tr);
			$1.t('td',{textNode:L.email},tr);
		}
	}});
},
gTo:function(tab,Wins){
	var Pa=$M.read();
	if(!tab){ tab=Pa.tab; }
	var urib='mpaCrd';
	var urib2='cardId:'+Pa.cardId+',tab:'+tab;
	$M.to(urib,urib2);
	switch(tab){
		case 'tas':
			Mpa.Tas.profile='crd';
			Mpa.Tas.gData=['&cardId='+Pa.cardId,{cardId:Pa.cardId}];
			Mpa.Tas.xHtml=[];
			Mpa.Tas.xHtml=Wins.winTas.childNodes;
			Mpa.Tas.ini();
		break;
		case 'eve':
			Mpa.Eve.profile='crd';
			Mpa.Eve.gData=['&cardId='+Pa.cardId,{cardId:Pa.cardId}];
			Mpa.Eve.xHtml=[];
			Mpa.Eve.xHtml=Wins.winEve.childNodes;
			Mpa.Eve.ini();
			break;
		case 'nov':
			Mpa.Nov.profile='crd';
			Mpa.Nov.gData=['&cardId='+Pa.cardId,{cardId:Pa.cardId}];
			Mpa.Nov.xHtml=[];
			Mpa.Nov.xHtml=Wins.winNov.childNodes;
			Mpa.Nov.ini();
			break;
			case 'cas':
			Mpa.Cas.profile='crd';
			Mpa.Cas.gData=['&cardId='+Pa.cardId,{cardId:Pa.cardId}];
			Mpa.Cas.xHtml=[];
			Mpa.Cas.xHtml=Wins.winCas.childNodes;
			Mpa.Cas.ini();
			break;
			case 'opo':
			Mpa.Opo.profile='crd';
			Mpa.Opo.gData=['&cardId='+Pa.cardId,{cardId:Pa.cardId}];
			Mpa.Opo.xHtml=[];
			Mpa.Opo.xHtml=Wins.winOpo.childNodes;
			Mpa.Opo.ini();
			break;
	}
},
profile:function(){
	var cont=$M.Ht.cont; var Pa=$M.read();
	$Api.get({f:Api.Mpa.b+'crd/'+Pa.cardId,loade:cont,func:function(Jr){
		if(Jr.errNo){ return $Api.resp(cont,Jr); }
		$1.t('h4',{'class':'h1title',textNode:Jr.cardName},cont);
		var ttPars={tt:'mpaDoc',tr:Jr.cardId};
	var Pm=[
	{textNode:'Info',winClass:'winInfo','class':'fa fa-info', func:function(){ Mpa.Crd.gTo('info',Wins); }},
	{textNode:'Tareas',winClass:'winTas','class':'fa fa-list',nDivs:[{style:'padding:6px 0'},{}], func:function(){ Mpa.Crd.gTo('tas',Wins); }},
	{textNode:'Eventos',winClass:'winEve','class':'fa fa-calendar-check-o',nDivs:[{style:'padding:6px 0'},{}], func:function(){ Mpa.Crd.gTo('eve',Wins); }},
	{textNode:'Novedades',winClass:'winNov','class':'fa fa-feed',nDivs:[{style:'padding:6px 0'},{}], func:function(){ Mpa.Crd.gTo('nov',Wins); }},
	{textNode:'Casos',winClass:'winCas','class':'fa fa-briefcase',nDivs:[{style:'padding:6px 0'},{}], func:function(){ Mpa.Crd.gTo('cas',Wins); }},
	{textNode:' Oportunidades',winClass:'winOpo','class':'fa fa-money',nDivs:[{style:'padding:6px 0'},{}], func:function(){ Mpa.Crd.gTo('opo',Wins); }},
	{textNode:'Documentación','class':'fa fa-folder-open-o',winClass:'winDoc',func:function(T){
		Mpa.Crd.gTo('doc',Wins); 
		GeDoc.Rfi.oneLoad(ttPars,T.win);
	}}
	];
	var Wins = $1.tabs(Pm,cont,{w:{style:'margin-top:0.5rem;'}});
	switch(Pa.tab){
		default :  Wins._winInfo.click(); break;
		case 'tas' : Wins._winTas.click(); break;
		case 'eve' : Wins._winEve.click(); break;
		case 'nov' : Wins._winNov.click(); break;
		case 'cas' : Wins._winCas.click(); break;
		case 'opo' : Wins._winOpo.click(); break;
		case 'doc' : Wins._winDoc.click(); break;
	}
	Mpa.Crd.wInf(Jr,Wins.winInfo);
	}});
},
wInf:function(P,cont){
	var licT=_g(P.licTradType,$V.licTradType);
	$1.T.tbf([
	{line:1,wxn:'tbf_x8',t:licT,v:P.licTradNum},
	{wxn:'tbf_x2',t:'Nombre Cliente',v:P.cardName},
	{wxn:'tbf_x8',t:'Responsable',v:_g(P.slpId,$Tb.oslp)},
	{wxn:'tbf_x8',t:'Grupo',v:_g(P.grId,$JsV.parGrC)},
	{line:1,wxn:'tbf_x6',t:'Regimen',v:_g(P.RF_regTrib,$V.RF_regTrib)},
	{wxn:'tbf_x6',t:'Tipo Entidad',v:_g(P.RF_tipEnt,$V.RF_tipEnt)},
	{wxn:'tbf_x4',t:'Email RUT',v:P.email2},
	{wxn:'tbf_x4',t:'Email F.E',v:P.email},
	{line:1,wxn:'tbf_x4',t:'Municipio',v:_g(P.RF_mmag,$V.AddrCity)},
	{wxn:'tbf_x4',t:'Departamento',v:_g(P.countyCode,$V.AddrCounty)},
	{wxn:'tbf_x2',t:'Dirección RUT',v:P.address},
	{line:1,wxn:'tbf_x8',t:'Teléfono',v:P.phone1},
	{wxn:'tbf_x8',t:'Teléfono 2',v:P.phone2},
	{wxn:'tbf_x8',t:'Celular',v:P.cellular}
	],cont);
}
}


$V.mpaCalPrivacity=[{k:'M',v:'Privado'},{k:'S',v:'Compartido'},{k:'P',v:'Público'}];

$M.liAdd('sca',[
{_lineText:'Calendarios'},
{k:'mpaCal',t:'Calendarios', kau:'sca', func:function(){
	$M.Ht.ini({g:function(){ Mpa.Cal.ini(); }});
}},
{k:'mpaCal.form',t:'Calendarios', kau:'sca.write', func:function(){
	$M.Ht.ini({g:function(){ Mpa.Cal.form($M.read()); }});
}}
]);

Mpa.Cal={
ini:function(){
	var cont=$M.Ht.cont;
	var wt=$1.t('div',{style:'display:table-cell; width:200px;'},cont);
	$1.dateMov.line({func:function(D){
		Mpa.Cal.eveOn(cont);
	}},wt);
	var wc=$1.t('div',0,wt);
	var wl=$1.t('div',{'class':'_mpaCalList',style:'display:table-cell; '},cont);
	//Obtener
	$Api.get({f:Api.Mpa.b+'cal/list',loade:wc,func:function(Jr){
		for(var i in Jr.L){ var L=Jr.L[i];
			var li=$1.t('li',{},wc); li.P=L;
			var s=$1.t('span',{style:'display:inline-block; width:16px; backgroundColor:'+L.calColor},li);
			s.innerHTML='&nbsp;';
			var b=$1.t('span',{style:'display:inline-block',textNode:L.calName},li);
			li.onclick=function(){
			$M.to('mpaCal','calId:'+this.P.calId);
				Mpa.Cal.eveOn(cont);
			}
		}
	}});
	var Pa=$M.read();
	if(Pa.calId>0){ Mpa.Cal.eveOn(cont);; }
},
eveOn:function(wt){
	var Pa=$M.read();
	var wList=$1.q('._mpaCalList',wt);
	var Dt=$1.q('.'+$1.dateMov.cls,wt);
	var vPost='calId='+Pa.calId+'&'+Dt.gData;
	$Api.get({f:Api.Mpa.b+'cal/events',inputs:vPost,loade:wList,func:function(Jr){
		{
			var Li=[];
			var D={date1:Dt.d1,date2:Dt.d2,Li:[]};
			if(!Jr.L.errNo){ for(var i in Jr.L){
				L=$Crm.Ht.vD(Jr.L[i],{tabOn:L.obj});
				var Lx=$2d.longDays(L); /*duplicar para dias */
				for(var i3 in Lx){
					var cnt=$1.t('div');
					$Crm.Ht.open(Lx[i3],{o:L.obj,pare:cnt});
					$Crm.Ht.type(Lx[i3],cnt);
					$Crm.Ht.prio(Lx[i3],cnt);
					$Crm.Ht.prs(Lx[i3],cnt);
					$Crm.Ht.crd(Lx[i3],cnt);
					Lx[i3].lineText=cnt;
					D.Li.push(Lx[i3]);
				}
			}}
			$Sche.V.agenda(D,wList);
		}
	}});
},
form:function(P){ var P=(P)?P:{};
	var fP={
	vidn:'calId',calId:P.calId,api:Api.Mpa.b+'cal',
	Cols:[
	['Nombre',{k:'calName',T:{divLine:1,req:'Y',wxn:'wrapx2',tag:'input'}}],
	['Privacidad',{k:'privacity',T:{req:'Y',wxn:'wrapx8',tag:'select',opts:$V.mpaCalPrivacity}}],
	['Color',{k:'calColor',T:{wxn:'wrapx8',tag:'color'}}],
	['Descripción',{k:'calDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'input'}}],
	],
	fCont:function(Jr,pare){
		var x=$1.t('p',0,pare);
		var sUs=$1.T.sel({opts:$Tb.ousr},x);
		sUs.onchange=function(){
			var tU=_gO(this.value,$Tb.ousr);
			addLine({calId:Jr.cardId,lineType:'U',lineText:tU.v,memId:tU.k,perms:'R'},us,te);
		}
		var us=$1.t('div',0,pare);
		var te=$1.t('div',0,pare);
		$1.t('h5',{textNode:'Usuarios'},us);
		$1.t('h5',{textNode:'Equipos'},te);
		if(Jr.M && !Jr.M.errNo){
			for(var i in Jr.M){
			addLine(Jr.M[i],us,te);
		}}
	}
	}
	var x=$Api.form(fP);
	function addLine(L,us,te){
		var AJs={gid:L.gid,lineType:L.lineType,memType:L.memType,memId:L.memId,perms:L.perms};
		if(L.id){ AJs.id=L.id; }
			var ki='scam_'+L.lineType+L.memId;
			if(L.lineType=='T'){
				$Api.boxi({xIf:'id',name:'M',uid:ki,line1:L.lineText,AJs:AJs},te);
			}
			else{ $Api.boxi({xIf:'id',name:'M',uid:ki,line1:L.lineText,AJs:AJs},us); }
	}
},
}
