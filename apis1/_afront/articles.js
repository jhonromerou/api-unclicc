HTTP.addRoutes(() => {
return {
    "articles_terminates_all": {
        "name": "Productos",
        "method": "GET",
        "host": { env: 'host_api_unclicc' },
        "path": "/appi/private/itm/a",
        "queryParameters": {
            "itemType": "P"
        },
        "headers": {
            "ocardtooken": { env: 'header_ocardtooken' }
        },
        "description": "Lista los articulos de tipo terminado",
    },

    "articles_terminates_form": {
        "uri": "#itm.p.form",
        "name": "Formulario de Producto",
        "authorizer": "itm.form",
        "api": {
            "method": "get",
            "host": { env: '$_ENV.host_api_unclicc'},
            "path": "/appi/private/itm/{id}",
            "headers": {
                "ocardtooken": { env: '$_ENV.header_ocardtooken'}
            }
        },
        "title": "Formulario de Producto",
        "description": "Formulario de creación / edición de un producto"
    }
}
});
