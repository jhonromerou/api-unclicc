var HTTP = {
    Routes:{},

    // addRoutes adds news routes to call in request by name.
    addRoutes:function(rFunc){
        items= rFunc();
        for(var k in items) {
            this.Routes[k] = items[k]
        }
    },

    // route makes a request using routes struct define.
    route:function(routeName, P)
    {
        let route = HTTP.Routes[routeName]
        let params = {}

        params.method = (route.method) ? route.method : 'GET';
        params.url = this.fromEnv(route.host) + route.path

        this.setHeaders(params, route.headers)
        this.setHeaders(params, P.headers)

        this.setQueryParameters(params, route.queryParameters)
        this.setQueryParameters(params, P.queryParameters)

        return this.newRequest(params)
    },

    // fromEnv gets value from env defined or default value.
    fromEnv:function(param)
    {
        if(param.env){
            return $_ENV.get(param.env)
        }

        if(param.__env){
            return eval(param.env)
        }

        return param;
    },

    // setQueryParameters set parameters on url.
    setQueryParameters:function(req, queryParameters)
    {
        if(queryParameters) {
            let params = ''
            for(var param in queryParameters) {
                params += param + '=' + queryParameters[param] + '&';
            }
            params = params.replace(/\&$/, '');
            req.url = (req.url.match(/\?/))
                ? req.url + '&' + params
                : req.url + '?' + params;
        }
    },

    setHeaders:function(req, headers)
    {
        if(headers) {
            req.headers = {};
            for(var k in headers) {
                req.headers[k] = this.fromEnv(headers[k]);
            }
        }
    }
}

HTTP.newRequest = function(req){
    let request = newFetchProvider(req);

    this.json = function(callback) {
        request.then((response) => {
            return response.json()
        })
        .then((data) => {
            callback(data)
        })
        return this
    }

    return this;
}

function newFetchProvider(req)
{
    let request = fetch(
        req.url,
        {
            method: req.method,
            body: req.data ? req.data : null,
            headers: req.headers ? req.headers : {}
        }
    );

    return request
}

$_ENV = {

    // defines envoirements name cannot use.
    UNALLOW_ENV_NAMES:'set|get',

    set:function(envs)
    {
        var errs = null
        for(var env in envs) {
            regExp = eval('/^(' + this.UNALLOW_ENV_NAMES + ')$/');
            if(env.match(regExp)) {
                if(errs == null) {
                    errs = {}
                }
                errs[env] = envs[env];
                continue;
            }

            this[env] = envs[env];
        }

        if(errs) {
            console.warn('cannot_set_envoirements', errs)
        }
    },

    get:function(env)
    {
        let = env_value = this[env]
        if(! env_value) {
            console.warn('envoirement {{' + env+'}} is not defined')
            return null;
        }

        return env_value
    }
}

HTTP.getFields = function(obj, P){
    P = (P) ? P : {}
    let attributeName ='x-http-filter';
    var DATA = {};
    obj = (typeof(obj) == 'string') ? document.querySelector(obj) : obj

    if (! obj || ! obj.tagName) {
        console.warn('object define is invalid')
        return DATA;
    }

    var parE = obj.querySelectorAll('['+attributeName+']');
    for(var i=0; i<parE.length; i++){
        var element = parE[i];
        if(element.classList.contains('__inputSearch')) {
            continue;
        }

        var tag=(element.tagName).toLowerCase();
        var fName = (P.attrName) ? element.getAttribute(P.attrName) : element.name;

        if (tag == 'textarea') {
            DATA[fName] =  element.value;
        }
        else if (tag=='select'){
            if (element.getAttribute('multiple')) {
                DATA[fName] = [];
                for(var o=0; o < element.options.length; o++){
                    if(element.options[o].selected && element.options[o].value != ''){
                        DATA[fName].push(element.options[o].value)
                    }
                }
            }
            else {
                DATA[fName] = element.options[element.selectedIndex].value;
            }
        }
        else if (element.type == 'checkbox') {
            DATA[fName] = (element.checked) ? 'Y' : 'N';
        }
        else if (element.type == 'radio' && element.checked) {
            DATA[fName] =  element.value;
        }
        else if (element.type == 'text') {
            DATA[fName] =  element.value;
        }
        /*
        if(element.getAttribute('numberformat') == 'mil' || element.numberformat == 'mil'){ 
            DATA[fName] = $Str.toNumber(DATA[fName]);
        }
        */
    }
    return DATA;
}
