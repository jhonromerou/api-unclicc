$DFE={};
//,{k:'',v:''}
$DFE.tipFac=[
{k:'1',v:'Factura Venta'},{k:'2',v:'Exportación'},{k:'3',v:'Contingencia'}
];

$DFE.tipPago=[
{k:'10',v:'Efectivo'},{k:'20',v:'Cheque'},{k:'41',v:'Transferencia'},{k:'42',v:'Consignacion'}
];
$DFE.tipEnt=[
{k:'1',v:'Persona Juridica'},{k:'2',v:'Persona Natural'}
];
$DFE.tipReg=[
{k:'0',v:'Simplificado'},{k:'2',v:'Común'}
];
$DFE.tipDoc=[
{k:'11',v:'Registro Civil'},{k:'12',v:'Tarjeta Identidad'},{k:'13',v:'Cédula Ciudadania'},{k:'21',v:'Tarjeta Extranjeria'},{k:'22',v:'Cedula Extranjeria'},{k:'31',v:'NIT'},{k:'41',v:'Pasaporte'},{k:'42',v:'Documento Identificación Extranjero'}
];
$DFE.tipImp=[
{k:'01',v:'IVA'},{k:'02',v:'Imp. al Consumo'},{k:'03',v:'ICA'},{k:'04',v:'Imp. Nacional al consumo'}
];
$DFE.tipNC=[
{k:'1',v:'Devolución de parte de los bienes; no aceptación de partes del servicio'},
{k:'2',v:'Anulación de factura electrónica'},
{k:'3',v:'Rebaja total aplicada'},
{k:'4',v:'Descuento total aplicado'},
{k:'5',v:'Otros'}
];
$DFE.tipND=[
{k:'1',v:'Intereses'},
{k:'2',v:'Gastos por cobrar'},
{k:'3',v:'Cambio del valor'}
];



