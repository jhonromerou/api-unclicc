/* create on sys_vars 
$V.EgreTypes, $V.carteraEdad
*/
if(!$V.carteraEdad){
$V.carteraEdad ={
'0_':{t:'Corriente',men:1}, '1_':{t:'Vencido',may:0}
};
}
if(!$V.accountBank){ $V.accountBank = {}; }
if(!$V.docSerieType){ $V.docSerieType={}; }
$js.push($V.docSerieType,{gvtInv:'Fac. Venta'});

$Doc.a['gvtInv']={a:'gvtInv.view'};
$V.docStatus={O:'Abierto',C:'Cerrado',N:'Anulado'};
$V.acbType ={'cash':'Efectivo',bank:'Banco','credit-card':'Tarjeta de Crédito'}; 
$V.EgreTypes = {directof:'Directo Fijo',indirectof:'Indirecto Fijo',indirectov:'Indirecto Variable'};
$V.IngTypes={P:'Pago a deuda',A:'Anticipo'};
if(!$V.ITM){ $V.ITM={}; }
$V.ITM.Ccateg ={
	ingActOrd:{t:'Ingresos de Actividades Ordinarias',
		L:{devol:'Devoluciones en Ventas', ventas:'Ventas'}
	},
	otrosIng:{t:'Otros Ingresos',
		L:{finan:'Ingresos Financieros', diversos:'Otros Ingresos Diversos'}
	}
};

var jsFi={'extends':'/com1/js/extends.php'};
if(!Api.Gvt){ Api.Gvt={a:'/api/gvt'}; };
Api.Vapp={
b:'/api/vapp/bodega',
itm:'/api/vapp/itm',
};

Api.ITM = {o:'/sa/com1/itm/o', whs:'/sa/com1/itm/whs', wtd:'/sa/com1/itm/wtd'};
Api.Whs = {a:'/sa/com1/whs/'};
Api.Sell = {a:'/sa/com1/sell/', o:'/sa/com1/sell/o'};
Api.Buy = {a:'/sa/com1/buy/'};
Api.GeF = {a:'/sa/com1/GeF/'};
Api.iPays = {a:'/sa/com1/iPays/'};

$M.ttK['FV']={a:'sell.envoiceView',p:{tr:'handNum'}};
$M.ttK['FC']={a:'buy.envoiceView',p:{tr:'handNum'}};
$M.ttK['WTD']={a:'items.WTD.view',p:{tr:'handNum'}};

var _Fi = {
gvtInv:function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'No. Factura'},I:{tag:'input',type:'text','class':jsV,name:'A.docNum(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Cliente'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del cliente...',name:'A.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Gvt.Inv.get});
	wrap.appendChild(btnSend);
},
buyList:function(wrap){
	var d = $2d.goRang({rang:'week'});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapxauto', L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'obuy.docDate(E_mayIgual)',value:d.date1}},wrap);
	$1.T.divL({wxn:'wrapxauto', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'obuy.docDate(E_menIgual)',value:d.date2}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Cliente'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del cliente...',name:'obuy.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Buy.get});
	wrap.appendChild(btnSend);
},
itemswhsHistory:function(){
	var d = $2d.goRang({rang:'week'});
	var wrap = $M.Ht.filt; var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapxauto', L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'IW1.dateC(E_mayIgual)(T_time)',value:d.date1}},wrap);
	$1.T.divL({wxn:'wrapxauto', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'IW1.dateC(E_menIgual)(T_time)',value:d.date2}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'IW1.whsId(E_igual)'},opts:$V.ITM.whsCode}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:ITM.Whs.history});
	wrap.appendChild(btnSend);
},
itemsList:function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)',placeholder:'Código Producto'}},wrap);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Nombre Producto'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre Producto..'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:function(){ ITM.get(null); }});
	wrap.appendChild(btnSend);
},
itmInventoryVal:function(){
	var wrap = $M.Ht.filt; var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)',placeholder:'Código Producto'}},wrap);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Bodega'},I:{tag:'select',sel:{'class':jsV,name:'IW1.whsId(E_igual)'},opts:$V.ITM.whsCode}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:ITM.inventoryVal});
	wrap.appendChild(btnSend);
},
GeFcxc:function(v){
	var dateLimit = '';
	var wrap = $M.Ht.filt; var jsV = 'jsFiltVars';
	var divL= $1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Agrupado Por'},I:{tag:'select',sel:{'class':'__cartViewType'},opts:{card:'Contacto',doc:'Documento'},noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapxauto', subText:'Corte para fecha de vencimiento de las facturas', L:{textNode:'Fecha Corte'},I:{tag:'input',type:'date','class':jsV,name:'car1.dueDate(E_menIgual)',value:dateLimit}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'No. Factura'},I:{tag:'input',type:'text','class':jsV,name:'oCtr.handNum'}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Nombre Cliente'},I:{tag:'input',type:'text','class':jsV,name:'crd.cardName(E_like3)'}},divL)
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:function(){
		var view = $1.q('.__cartViewType',wrap);
		switch(view.value){
			case 'card': GeF.carteraCard(v); break;
			default: GeF.carteraDoc(v); break;
		}
	}});
	wrap.appendChild(btnSend);
},
bussP:function(wrap){ var jsV = 'jsFiltVars';
	var divL= $1.T.divL({divLine:1,wxn:'wrapx4',L:'Nombre Contacto',I:{tag:'input',type:'text','class':jsV,name:'textSearch',placeholder:'Buscar Contacto' }},wrap);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:$crd.get});
	wrap.appendChild(btnSend);
},
'items.WTD':function(P){
	var wrap = $M.Ht.filt; var jsV = 'jsFiltVars';
	var se = $1.T.btnNew({fa:'',textNode:'Nuevo Movimiento', func:function(){ $M.to('items.WTD.form'); }});
	var divL= $1.T.divL({divLine:1, wxn:'wrapxauto',L:{textNode:''},Inode:se},wrap);
	$1.T.divL({wxn:'wrapxauto',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapxauto',L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	var upd = $1.T.btnNew({fa:'',textNode:'Actualizar', func:function(){ ITM.WTD.get(); }});
	$1.T.divL({wxn:'wrapxauto',L:{textNode:''},Inode:upd},divL);
},
'GeF.Egr':function(wrap){
	var d = $2d.goRang({rang:'week'});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapxauto', L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'oegr.docDate(E_mayIgual)',value:d.date1}},wrap);
	$1.T.divL({wxn:'wrapxauto', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'oegr.docDate(E_menIgual)',value:d.date2}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Cliente'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre del cliente...',name:'oegr.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:GeF.Egr.get});
	$1.T.divL({wxn:'wrapx4',Inode:btnSend},divL);
},
'GeF.AcB':function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:{textNode:'Tipo Cuenta'},I:{tag:'select',sel:{'class':jsV,name:'oacb.acbType(E_igual)'},opts:$V.acbType}},wrap);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,placeholder:'Bancolombia, BBVA..',name:'oacb.acbName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Número'},I:{tag:'input',type:'text','class':jsV,placeholder:'725-981589-02',name:'oacb.acbNum(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:GeF.AcB.get});
	$1.T.divL({wxn:'wrapx4',Inode:btnSend},divL);
},

}

$M.sAdd([
{topFolder:{folId:'vapp',folName:'Venta App'}},
{fatherId:'vapp',MLis:['gvtInv.get','buy.get']}
]);
$M.s={};
$M.s['bussPartner'] = {t:'Contactos',L:['crd.c','crd.s']}
$M.s.sell = {t:'Facturas', L:['gvtInv.get','buy.get']}
$M.s.cata = {t:'Catalogos', L:['itm.p','itm.mp']}
$M.s.invent = {t:'Productos', L:['items.inventoryVal','items.WTD']}
$M.s.cartera = {t:'Cartera', L:['GeF.cartera.cxc','GeF.cartera.cxp']}
$M.s.GeF = {t:'Tesorería', L:['GeF.AcB','GeF.Egr'] };
$M.s.Conf = {t:'Configuración', L:['whs.list']};
//$M.s.GeF = {t:'Tesorería', L:['GeF.Notas.nc','GeF.Notas.nd','GeF.RC.anti','GeF.Egr', 'GeF.AcB'] };

$M.li['gvtInv.get'] ={t:'Ventas', func:function(){
	$M.Ht.ini({fieldset:true, func_filt:'gvtInv', topCont:$1.T.btnNew({fa:'fa_doc',textNode:' Nueva Factura de Venta', func:function(){ $M.to('gvtInv.form'); }}),
	func_pageAndCont:function(){ Gvt.Inv.get({}); } });
}};
$M.li['gvtInv.form'] = {t:'Nueva Factura de Venta', func:function(){
	$M.Ht.ini({func_cont:function(cont){ Gvt.Inv.form(); }});
}};
$M.li['gvtInv.view'] = {t:'Factura de Venta', func:function(){
	$M.Ht.ini({func_cont:function(cont){ Gvt.Inv.view(); }});
}};


$M.li['buy.get'] ={t:'Compras', func:function(){
	$M.Ht.ini({fieldset:true, func_filt:_Fi.buyList, topCont:$1.T.btnNew({fa:'fa_plusCircle',textNode:' Nueva Compra', func:function(){  $M.to('buy.post'); }}),
	func_pager:function(){ Buy.get(); },
	func_cont:function(){ Buy.get({}); } });
}};
$M.li['buy.p'] = {t:'Nueva Factura de Compra', func:function(){
	$M.Ht.ini({func_cont:function(cont){ Buy.post(); }});
}};
$M.li['buy.envoiceView'] = {t:'Factura de Compra', func:function(){
	$M.Ht.ini({func_cont:function(cont){ Buy.envoiceView(); }});
}};


$M.li['items.inventoryVal'] = {t:'Valorización del Inventario', func:function(){
	
	$M.Ht.ini({topCont:$1.t('div',{textNode:'Solo se muestran los productos que esten definidos para manejar inventario.'}), func_filt:_Fi.itmInventoryVal, func_cont:function(cont){ ITM.inventoryVal(); }});
}};
$M.li['items.whsHistory'] = {t:'Movimientos del Producto', func:function(){
	$M.Ht.ini({fieldset:true, func_filt:_Fi.itemswhsHistory, func_cont:function(cont){ ITM.Whs.history(); }});
}};
$M.li['items.WTD'] = {t:'Movimientos / Transferencia', func:function(){
	$M.Ht.ini({func_filt:'items.WTD', func_cont:function(){ ITM.WTD.get(); }});
}};
$M.li['items.WTD.form'] = {t:'Generar Movimiento / Transfereancia', func:function(){
	$M.Ht.ini({loader:'N',func_cont:function(){ ITM.WTD.form(); }});
}};
$M.li['items.WTD.view'] = {t:'Documento de Transferencia', func:function(){
	$M.Ht.ini({func_cont:function(cont){ ITM.WTD.open(); }});
}};
$M.li['items.view'] = {t:'Ficha de Producto', func:function(){
	$M.Ht.ini({func_cont:function(cont){ ITM.open(); }});
}};

$M.li['GeF.AcB'] ={t:'Cuentas / Bancos', func:function(){
	$M.Ht.ini({fieldset:true, func_filt:'GeF.AcB', topCont:$1.T.btnNew({fa:'fa_plusCircle',textNode:' Nueva Cuenta', func:function(){  $M.to('GeF.AcB.form'); }}),
	func_pager:function(){ GeF.AcB.get(); },
	func_cont:function(){ GeF.AcB.get({}); } });
}};
$M.li['GeF.AcB.form'] ={t:'Cuenta / Banco', func:function(){
	$M.Ht.ini({func_cont:GeF.AcB.form });
}};
$M.li['GeF.AcB.move'] = {t:'Movimientos de la Cuenta', func:function(){
	$M.Ht.ini({
		func_pager:GeF.AcB.move, func_cont:GeF.AcB.move
	});
}};


$M.li['GeF.cartera.cxp'] ={t:'Cuentar por Pagar', func:function(){
	$M.Ht.ini({func_filt:function(){ _Fi.GeFcxc('cxp'); }, func_cont:function(cont){ GeF.cartera('cxp'); }});
}};
$M.li['GeF.cartera.cxc'] ={t:'Cuentas por Cobrar', func:function(){
	$M.Ht.ini({func_filt:function(){ _Fi.GeFcxc('cxc'); }, func_cont:function(cont){ GeF.cartera('cxc'); }});
}};

$M.li['GeF.Egr'] ={t:'Gastos (Egresos)', func:function(){
	var newB=$1.T.btnNew({fa:'fa_doc',textNode:' Registrar Gasto', func:function(){ $M.to('GeF.Egr.form'); }});
	$M.Ht.ini({func_filt:'GeF.Egr', mediumCont:newB, func_cont:function(cont){ GeF.Egr.get(); }});
}};
$M.li['GeF.Egr.form'] ={t:'Registrar Gasto', func:function(){
	$M.Ht.ini({func_filt:0, func_cont:function(cont){ GeF.Egr.form(); }});
}};
$M.li['GeF.Ing'] ={t:'Recibos de Caja (Ingresos)', func:function(){
	var newB=$1.T.btnNew({fa:'fa_doc',textNode:' Registrar Ingreso', func:function(){ $M.to('GeF.ReCa.form'); }});
	$M.Ht.ini({func_filt:'GeF.Ing', mediumCont:newB, func_cont:function(cont){ GeF.Egr.get(); }});
}};

$M.li['GeF.RC.anti'] ={t:'RC- Ingreso por Anticipo', func:function(){
	$M.Ht.ini({func_filt:0, func_cont:function(cont){ GeF.RC.anti(); }});
}};


$M.li['GeF.Notas.nc'] ={t:'Notas Crédito', func:function(){
	var newB=$1.T.btnNew({textNode:'Nueva Nota Crédito', func:function(){ $M.to('GeF.Notas.nc.form'); }});
	$M.Ht.ini({ mediumCont:newB, func_cont:function(cont){ GeF.Notas.get('NC'); }});
}};
$M.li['GeF.Notas.nd'] ={t:'Notas Débito', func:function(){
	var newB=$1.T.btnNew({textNode:'Nueva Nota Débito', func:function(){ $M.to('GeF.Notas.nd.form'); }});
	$M.Ht.ini({mediumCont:newB, func_cont:function(cont){ GeF.Notas.get('ND'); }});
}};
$M.li['GeF.Notas.nc.form'] ={t:'Notas Crédito', func:function(){
	$M.Ht.ini({ func_cont:function(cont){ GeF.Notas.form('NC'); }});
}};
$M.li['GeF.Notas.nd.form'] ={t:'Notas Débito', func:function(){
	$M.Ht.ini({ func_cont:function(cont){ GeF.Notas.form('ND'); }});
}};


$M.li['whs.list'] = {t:'Bodegas', func:function(){
	$M.Ht.ini({fieldset:1, func_filt:null,
	func_pager:Whs.get,
	btnNew:$1.T.btnNew({fa:'fa_doc ui_button',textNode:' Crear Nueva Bodega', func:function(){ $M.to('whs.form'); }}),
	func_cont:Whs.get });
}};
$M.li['whs.form'] = {t:'Nueva Bodega', func:function(){
	$M.Ht.ini({func_cont:Whs.form});
} }

function doPdf(html,name){
	var pdf = new jsPDF('p','pt','letter');
	pdf.fromHTML(html);
	pdf.save(name+'.pdf');
}

$M.go({ini:'sell.get'});

/* FUNCIONES DE Soc */
if(typeof(Gvt) !='object'){ Gvt={}; }
Gvt.Inv = {
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Gvt.a+'/oivs', inputs:'odocEntry='+Pa.odocEntry, loadVerif:!Pa.docEntry, loade:cont, func:function(Jr){
		var Fs=[
		{fType:'crd',wxn:'wrapx3',L:'Cliente',req:'Y',cardId:Jr.cardId,cardName:Jr.cardName,replaceData:'Y',inputs:'fields=A.phone1'},
		{wxn:'wrapx8',L:'Teléfono',I:{tag:'input',type:'text',name:'phone1','class':jsF+' '+$Sea.clsName,k:'phone1',value:Jr.phone1}},
		{wxn:'wrapx4',L:'Dirección',I:{tag:'input',type:'text',name:'address','class':jsF+' '+$Sea.clsName,k:'address',value:Jr.address}},
		{divLine:1,wxn:'wrapx8',fType:'date',name:'docDate',value:Jr.docDate,req:'Y'},
		{wxn:'wrapx8',fType:'date',textNode:'Vencimiento',name:'dueDate',value:Jr.dueDate,req:'Y'},
		{wxn:'wrapx8',L:'Condiciones Pago',I:{tag:'select',sel:{name:'pymntGrId','class':jsF},select:Jr.pymntGrId,opts:$Tb.pymntGr}},
		{divLine:1,wxn:'wrapx3',L:'Detalles',subText:'Visible en Doc.',I:{tag:'textarea',name:'lineMemo',textNode:Jr.lineMemo,'class':jsF}},
		{wxn:'wrapx3',L:'Terminos',subText:'Terminos y condiciones',I:{tag:'textarea',name:'termsConditions',textNode:Jr.termsConditions,'class':jsF}},
		{wxn:'wrapx3',L:'Observaciones',subText:'No visibles en Doc.',I:{tag:'textarea',name:'observations',textNode:Jr.observations,'class':jsF}}
		];
			var tb=$1.T.table([]);
			var fie=$1.T.fieldset(tb,{L:{textNode:'Líneas del Documento'}});
			cont.appendChild(fie);
			$Doc.formSerie({cont:cont, serieType:'gvtInv',docEntry:Pa.docEntry,jsF:jsF, middleCont:fie, Jr:{}, POST:Api.Gvt.a+'/oivs', func:function(Jr2){
			$M.to('gvtInv.view','docEntry:'+Jr.docEntry);
		},Li:Fs});
		var trH=$1.q('tr',tb);
		$iDoc.line({k:'oitm',jsF:'jsFields',thead:'Y'},trH);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		var ni=1;
		$Doc.L.itmWinSz(tBody,{priceDefiner:'N',cont:fie,fields:'I.grsId,I.src1,I.taxCode,I.taxRte&wh[I.sellItem]=Y', noWin:'N',func:function(T,L){
			$iDoc.line({k:'oitm',D:L,jsF:'jsFields',ln:'L['+ni+']',AliasG:{udm:'sellUdm',priceList:'sellPrice'},AliasN:{sellPrice:'price'}
			},tr);
			ni++;
	}});
		$iDoc.foot({discPf:'Y',iva:'Y',retencion:'Y',gastos:'Y'},tb);
	}});
},
get:function(P){
	cont = $M.Ht.cont; $1.clear(cont);
	var vPost=$1.G.filter();
	$Api.get({f:Api.Gvt.a+'/oivs', inputs:vPost, loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['No.','Estado','Cliente','Fecha','Vencimiento','Valor','Desc.','Realizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var cssdue=(L.dueDate<$2d.today)?'color:#F00; font-weight:bold;':'';
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$Doc.href('gvtInv',L,'r'), textNode:' '+L.docEntry,'class':'fa faBtn fa_eye',title:'Ver Factura'},td);
					$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d'),style:cssdue},tr);
				$1.t('td',{textNode:$Str.money({value:L.docTotal})},tr);
				$1.t('td',{textNode:(L.discTotal*1)+'%'},tr);
				$1.t('td',{textNode:'Por '+L.userName+', el '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var td = $1.t('td',0,tr);
				var pay=$1.T.btnFa({fa:'fa_dollarGreen faf_medium',title:'Registrar Pago', L:L, func:function(T){
					iPays.form('FV',T.L);
				}}); td.appendChild(pay);
			};
		}
	}});
},
view:function(){ 
	var Pa=$M.read(); var cont=$M.Ht.cont;
	$Api.get({f:Api.Gvt.a+'/oivsView', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		var Trs=[];
		if(Jr.L.errNo){ Trs[0]={colspan:6,textNode:Jr.L.text}; }
		else{
			var va='vertical-align:middle';
			var ni=0;
			for(var i in Jr.L){ var L=Jr.L[i]; Trs[ni]=[]
				Trs[ni].push({textNode:Itm.Txt.code(L),style:va});
				Trs[ni].push({textNode:Itm.Txt.name(L),style:va});
				Trs[ni].push({textNode:$Str.money(L),style:va});
				Trs[ni].push({textNode:L.quantity,style:va});
				Trs[ni].push({textNode:$Str.money(L.priceLine),style:va});
				ni++;
			}
		}
		$Tpt.use('gvtInv',cont,{Jr:Jr,Trs:Trs});
		$Str.useCurr=false;
	}});
},
}

iPays = {//Pagos Internos
form:function(tt,P){
	var vSend= 'ctt='+tt+'&ctr='+P.docEntry+'&tt='+tt+'&tr='+P.docEntry;
	var cont = $1.t('div'); jsF='jsFields';
	$1.t('h4',{textNode:'Información de Factura','class':'head1'},cont);
	var ffL=$1.T.ffLine({ffLine:1,t:'No. Factura',w:'ffx3',v:P.handNum}); cont.appendChild(ffL);
	ffL.appendChild($1.T.ffLine({t:'Total Factura',w:'ffx3',v:$1.q('.FV__balDueDeb_'+P.docEntry).innerText}));
	var bal=(tt=='FV')?P.balDueDeb:P.balDueCred;
	var saldo= $1.T.ffLine({t:'Saldo',w:'ffx3',v:$Str.money(bal)});
	$1.q('div',saldo).classList.add('FV__balDueDeb_'+P.docEntry);
	ffL.appendChild(saldo);
	var divL= $1.T.divL({divLine:1, wxn:'wrapx4',L:{openInf:'comv1/accountBank',textNode:'Cuenta'},I:{tag:'select',sel:{'class':jsF,name:'acbId'},opts:$V.accountBank,noBlank:1}},cont);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Forma de Pago'},I:{tag:'select',sel:{'class':jsF,name:'payMethod'},opts:$V.payMethod,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}},divL);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Valor a Pagar'},I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'value',value:$Str.toMil(bal*1)}},divL);
	var div = $1.t('p',0,cont);
	var noMove = $1.T.ckLabel({s:{openInf:'comv1/noMoveCash'},I:{'class':jsF,name:'noMoveCash'},L:{textNode:'No generar movimiento de dinero en la cuenta.'}}); div.appendChild(noMove);
	var divL= $1.T.divL({divLine:1, wxn:'wrapx1',L:{textNode:'Notas'},I:{tag:'textarea','class':jsF,name:'lineMemo'}},cont);
	var resp=$1.t('div',0,cont);
	var btnSend = $1.T.btnSend({'class':'fa faBtn fa_save',textNode:' Guardar Pago'},{f:'POST '+Api.iPays.a+'o',getInputs:function(){ return vSend+'&'+$1.G.inputs(cont,jsF);}, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){ $1.clearInps(cont); $js.textChange('.FV__balDueDeb_'+P.docEntry,$Str.money(Jr.newBal)); iPays.get(tt,P,wList); }
	}});
	cont.appendChild(btnSend);
	var wList=$1.t('div',0,cont);
	$1.Win.open(cont,{winTitle:'Registrar Pago Documento '+P.handNum,onBody:1,winSize:'medium'});
		iPays.get(tt,P,wList);
},
get:function(tt,P,wList){ $1.clear(wList);
	var vSend= 'ctt='+tt+'&ctr='+P.docEntry;
	$ps_DB.get({f:'GET '+Api.iPays.a+'o',inputs:vSend, loade:wList, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(wList,Jr); }
		else{
		var tb=$1.T.table(['ID','Cuenta','Forma Pago','Fecha','Valor','Realizado']); wList.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){L=Jr.L[i];
			var cssNo=(L.noMoveCash=='Y')?'color:purple; font-weight:bold;' :'';;
			css=(L.docStatus=='N')?'cash_cancel':'';
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.payId,style:cssNo},tr);
			$1.t('td',{textNode:$V.accountBank[L.acbId],title:'ID: '+L.acbId,style:cssNo},tr);
			$1.t('td',{textNode:$V.payMethod[L.payMethod]},tr);
			$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
			$1.t('td',{textNode:$Str.money(L.value),'class':'__payValue'+L.payId+' '+css},tr);
			$1.t('td',{textNode:$2d.f(L.dateC,'mmm d H:iam')},tr);
			var td=$1.t('td',{'class':'__payBtnCancel'+L.payId},tr);
			if(L.docStatus=='O'){
			var btn=$1.T.btnSend({'class':'fa faBtn fa_trash',L:L,textNode:' Anular Pago',confirm:{text:'Una vez anulado, no se puede reversar la anulación'}},{f:'DELETE '+Api.iPays.a+'o.cancel', getInputs:function(T){ return 'payId='+T.L.payId; }, func:function(Jr2){ $1.Win.message(Jr2);
				if(!Jr2.errNo){ $js.textChange('.FV__balDueDeb'+P.docEntry,$Str.money(Jr2.newBal));
					$1.q('.__payValue'+Jr2.payId,tb).classList.add('cash_cancel');
					$1.clear($1.q('.__payBtnCancel'+Jr2.payId,tb));
					iPays.get(tt,P,wList);
				}
			}
			}); td.appendChild(btn);
			}
		}
		}
	}});
}
}

Buy = {
post:function(){
	var wrap = $M.Ht.cont; var jsF = 'jsFields3';
	var fieWrap=$1.t('fieldset',0,wrap);
	var divL = $1.T.divL({divLine:1,wxn:'wrapxauto',req:'Y',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}},fieWrap);
	$1.T.divL({wxn:'wrapxauto',req:'Y',L:{textNode:'Vencimiento'},I:{tag:'input',type:'date','class':jsF,name:'dueDate'}},divL);
	var se = $o.Se.form({opts:'bussPartner_sup',jsFields:jsF,fields:'A.contAddr,A.contPhone',zIndex:1000,inLine:1,putDefine:{id:'cardId',t1:'cardName'},putOn:{}});
	var divL = $1.T.divL({divLine:1, wxn:'wrapx4',req:'Y',L:{textNode:'Proveedor'},Inode:se},fieWrap);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Teléfono'},I:{tag:'input',type:'text','class':jsF+' __fie2Change_contPhone',name:'contPhone'}},divL);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Dirección'},I:{tag:'input',type:'text','class':jsF+' __fie2Change_contAddr',name:'contAddr',placeholder:'Dirección para facturación'}},divL);
	var divL = $1.T.divL({divLine:1, wxn:'wrapx1',L:{textNode:'Notas'},I:{tag:'textarea','class':jsF,name:'lineMemo',placeholder:'Sobre la factura o para el cliente.'}},fieWrap);
	var tb = $1.T.table(['Código','Nombre','Bodega','Precio','Cant.','Total',''],{tbData:{'class':'table_zh table_x100'}}); wrap.appendChild(tb);
	var tBody = $1.t('tbody',0,tb);
	var trLast = $1.t('tr',0,tBody);
	var tdLast = $1.t('td',{colspan:7,textNode:'Busque para añadir un producto...',style:'backgroundColor:#EEE;'},trLast);
	var inpSea = $Sea.input(tdLast,{api:'itemPict',func:function(Jq){ 
		trLine(ni,Jq); ni++; updTotal();
	}});
	var tr = $1.t('tr',{'class':'tdAlign_r'},tBody);
	$1.t('td',{textNode:'Subtotal',colspan:4},tr);
	$1.t('td',{textNode:'','class':'_subTotal',colspan:3},tr);
	var tr = $1.t('tr',{'class':'tdAlign_r'},tBody);
	$1.t('td',{textNode:'Desc. %',colspan:4},tr);
	var td = $1.t('td',{colspan:3},tr);
	var disc = $1.t('input',{type:'number',inputmode:'numeric',min:0,max:100,'class':jsF+' _disc',name:'disc'},td);
	disc.onkeyup = function(){ updTotal(); }; disc.onchange = function(){ updTotal(); }
	var tr = $1.t('tr',{'class':'tdAlign_r'},tBody);
	$1.t('td',{textNode:'Total',colspan:4},tr);
	$1.t('td',{textNode:'','class':'_quantityTotal'},tr);
	$1.t('td',{textNode:'','class':'_docTotal',colspan:3},tr);
	var ni = 1; 
	function trLine(ni,L){
		var ln = 'L['+ni+']';
		var tr = $1.t('tr'); var lasttr = tBody.childNodes.length-3;
		tBody.insertBefore(tr,trLast);
		$1.t('td',{textNode:L.itemCode},tr);
		$1.t('td',{textNode:L.itemName},tr);
		L.quantity=(L.quantity)?L.quantity:1;
		var qua = $1.t('input',{type:'number',inputmode:'numeric','class':jsF+' _quantity',name:ln+'[quantity]',min:0,style:'width:3rem;',value:L.quantity});
	qua.O = {vPost:ln+'[whsId]='+defaultWhs.k};
		var tdWhs = $1.t('td',0,tr);
		var sel = $1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$V.ITM.whsCode,noBlank:1}); tdWhs.appendChild(sel);
		var td = $1.t('td',0,tr);
		var total = L.quantity*L.price;
		var price = $1.t('input',{type:'text',numberformat:'mil',inputmode:'numeric','class':jsF+' _price',name:ln+'[price]',step:0.1,min:0,style:'width:5rem;',value:L.buyPrice*1, onkeychange:function(){ updTotal(); },
		O:{vPost:ln+'[itemId]='+L.itemId+'&'+ln+'[itemName]='+L.itemName+'&'+ln+'[priceList]='+L.buyPrice}
		},td);
		var tdQua = $1.t('td',0,tr); tdQua.appendChild(qua);
		$1.t('td',{'class':'_total',textNode:$Str.money({value:total})},tr);
		var td = $1.t('td',0,tr);
		var btnDel = $1.T.btnFa({fa:'fa_trash',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode);updTotal();
		}}); td.appendChild(btnDel);
		//price.onkeyup = function(){ updTotal(); }; price.onchange = function(){ updTotal(); };
		qua.onkeyup = function(){ updTotal(); }; qua.onchange = function(){ updTotal(); };
	}
	function updTotal(){
		var quas = $1.q('._quantity',tb,'all');
		var qtotal = 0; var subTotal = 0;
		for(var i=0; i<quas.length; i++){
			var q = quas[i].value; var tr = quas[i].parentNode.parentNode;
			var p = $1.q('._price',tr).value; p=$Str.toNumber(p);
			var total = q*p;
			$1.q('._total',tr).innerText = $Str.money({value:total});
			qtotal += q*1; subTotal += total;
		}
		$1.q('._subTotal',tb).innerText = $Str.money({value:subTotal});
		var disc = $1.q('._disc',tb).value;
		var docTotal=(disc!='' || disc!=0)?subTotal*(100-disc)/100:subTotal;
		$1.q('._docTotal',tb).innerText = $Str.money({value:docTotal});
		$1.q('._quantityTotal',tb).innerText = qtotal;
	}
	var resp = $1.t('div',0,wrap);
	var btnSend = $1.T.btnSend({textNode:'Generar Venta'},{f:'POST '+Api.Buy.a+'o',getInputs:function(){ return $1.G.inputs(wrap,jsF);}, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){ $M.ttHash({tt:'FC',tr:Jr.handNum},'go');  }
	}});
	wrap.appendChild(btnSend);
},
get:function(P){
	cont = $M.Ht.cont; $1.clear(cont);
	var vPost=$1.G.filter();
	$ps_DB.get({f:'GET '+Api.Buy.a+'o', inputs:vPost, loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['No.','Cliente','Fecha','Vencimiento','Desc.','Total','Saldo','Realizado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.ttHash({tt:'FC',tr:L.handNum}), textNode:L.handNum},td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
				$1.t('td',{textNode:(L.disc*1)+'%'},tr);
				$1.t('td',{textNode:$Str.money({value:L.docTotal})},tr);
				$1.t('td',{textNode:$Str.money({value:L.balDueCred}),'class':'FV__balDueDeb_'+L.docEntry},tr);
				$1.t('td',{textNode:'Por '+L.userName+', el '+$2d.f(L.dateC,'mmm d H:iam')},tr);
				var td = $1.t('td',0,tr);
				var pay=$1.T.btnFa({fa:'fa_dollarGreen faf_medium',title:'Registrar Pago', L:L, func:function(T){
					iPays.form('FC',T.L);
				}}); td.appendChild(pay);
			};
		}
	}});
},
envoiceView:function(P){
	var Pr = $M.read();
	var cont = $M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Buy.a+'o.envoiceView', loade:cont, inputs:'handNum='+Pr.handNum ,func:function(Jr){
		var top = $1.t('div',{style:'border-bottom:0.0625rem solid #CCC; margin-bottom:1rem; text-align:center;'},cont);
		var head = $1.t('div',{'class':'ffLineNoBd'},cont);
		var lines = $1.t('div',0,cont);
		var bottom = $1.t('div',{style:'border-top:0.0625rem solid #CCC; margin-top:1rem; text-align:center; font-size:0.7rem;'},cont);
		$1.t('div',{textNode:'ADM Systems',style:'font-size:1.25rem; font-weight:bold;'},top);
		$1.t('div',{textNode:'N.I.T. 1125082294-4',style:'font-size:0.75rem;'},top);
		$1.t('div',{textNode:'Generado por ADM Systems'},bottom);
		$1.t('a',{href:'http://admsystems.co',textNode:'www.admsystems.co'},bottom);
		if(Jr.errNo){ $ps_DB.response(head,Jr); }
		else{
		var fl = $1.T.ffLine({ffLine:1, t:'Factura No:', v:Jr.handNum, w:'ffx3'},head);
		$1.T.ffLine({t:'Proveedor:', v:Jr.cardName, w:'ffx3'},fl);
		$1.T.ffLine({t:'Telefono:', v:Jr.phone, w:'ffx3'},fl);
		var fl = $1.T.ffLine({ffLine:1, t:'Fecha:', v:Jr.docDate, w:'ffxauto'},head);
		$1.T.ffLine({t:'Vencimiento:', v:Jr.dueDate, w:'ffx3'},fl);
		var tb = $1.T.table(['Código',{textNode:'Descripción',style:'width:20rem;'},'Precio','Cantidad',{textNode:'Total',style:'min-width:7rem;'}],{trHead:{'class':'trHead'}}); lines.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			$1.t('td',{textNode:$Str.money({value:L.price})},tr);
			$1.t('td',{textNode:L.quantity*1},tr);
			$1.t('td',{textNode:$Str.money({value:L.lineTotal})},tr);
		}
		var tr=$1.t('tr',0,tBody);
		var subTotalImp = Jr.subTotal-Jr.taxTotal;
		$1.t('td',{rowspan:3,colspan:2,textNode:''},tr);
		$1.t('td',{colspan:2,textNode:'Subtotal'},tr);
		$1.t('td',{textNode:$Str.money({value:subTotalImp})},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{colspan:2,textNode:'Descuento ('+(Jr.disc*1)+'%)'},tr);
		$1.t('td',{textNode:$Str.money({value:Jr.discTotal})},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{colspan:2,textNode:'Total'},tr);
		$1.t('td',{textNode:$Str.money({value:Jr.docTotal})},tr);
		}
	}});
}
}

ITM={};
ITM.inventoryVal = function(P){
	cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.ITM.whs, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb1=$1.T.table([{textNode:'Articulos / Existencias',style:'width:10rem;'},'Valor Total','Costo Total','Utilidad'],{tbData:{style:'margin-bottom:1rem; width:90%;'}})
			var tBody=$1.t('tbody',0,tb1); cont.appendChild(tb1);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:Jr.items+' / '+Jr.onHand},tr);
			$1.t('td',{textNode:$Str.money(Jr.price)},tr);
			$1.t('td',{textNode:$Str.money(Jr.buyPrice)},tr);
			$1.t('td',{textNode:$Str.money(Jr.utilidad)},tr);
			var tb = $1.T.table(['Código','Producto',{textNode:'Disponible',abbr:4},'Valor Total','Costo Total','Utilidad']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			cssO ='backgroundColor:#CCC;';
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.to('items.whsHistory','itemId:'+L.itemId,'r'), textNode:L.itemCode},td);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.onHand},tr);
				$1.t('td',{textNode:$Str.money(L.price*L.onHand)},tr);
				$1.t('td',{textNode:$Str.money(L.buyPrice*L.onHand)},tr);
				$1.t('td',{textNode:$Str.money(L.utilidad*L.onHand)},tr);
			};
		}
	}});
}

ITM.Whs={
history:function(P){
	cont=$M.Ht.cont; var Pr = $M.read();
	$ps_DB.get({f:'GET '+Api.ITM.whs+'.history', inputs:'itemId='+Pr.itemId+'&'+$1.G.inputs($M.Ht.filt,'jsFiltVars'), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['Bodega',{textNode:'Origen'},'Cantidad','Acumulado','Realizado'])
			var tBody = $1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){ L=Jr.L[i];
				L.quantity *=1; L.onHandAt*=1;
				var tr = $1.t('tr',0,tBody);
				$1.t('td',{textNode:$V.ITM.whsCode[L.whsId]},tr);
				var to = $M.ttHash({tt:L.tt,tr:L.handNum});
				var td = $1.t('td',0,tr); $1.t('a',{href:to, textNode:L.tt+'-'+L.handNum},td);
				$1.t('td',{textNode:L.quantity},tr);
				$1.t('td',{textNode:L.onHandAt},tr);
				$1.t('td',{textNode:$2d.f(L.dateC,'mmm d H:iam')},tr);
			};
		}
	}});
},
}

ITM.Vs={
WTDTypes:{
A:{I:'Ingreso / Entrada', E:'Egreso / Salida'},
I:{cambio:'Cambio del proveedor', devo:'Devolución del Cliente', regalo:'Regalo del Proveedor',ajuste:'Ajuste por Inventario',otro:'Otros Ingresos'},
E:{devo:'Devolución a Proveedor', cadu:'Caducidad / Expiración', cortesia:'Cortería a Cliente', perdida:'Pérdida o Robo',ajuste:'Ajuste por Inventario',otro:'Otros Egresos'}
}
}

ITM.WTD = {
form:function(){
	var wrap = $M.Ht.cont; var jsF = 'jsFields3';
	var divLine = $1.T.divL({divLine:1, wxn:'wrapxauto',req:'Y',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}});
	divLine.appendChild($1.T.divL({wxn:'wrapx4',req:'Y',L:{textNode:'Tipo Documento'},I:{tag:'select',sel:{'class':jsF+' __docType',name:'docType'},opts:ITM.Vs.WTDTypes.A}}));
	$1.q('.__docType',divLine).onchange = function(){
		var ns = $1.T.sel({sel:{'class':jsF+' __catType',name:'catType'},opts:ITM.Vs.WTDTypes[this.value]});
			var old = $1.q('.__catType',wrap);
		old.parentNode.replaceChild(ns,old);
	}
	divLine.appendChild($1.T.divL({wxn:'wrapx4',req:'Y',L:{textNode:'Categoria Movimiento'},I:{tag:'select',sel:{'class':jsF+' __catType',name:'catType'},opts:ITM.Vs.WTDTypes}}));
	wrap.appendChild(divLine);
	var divLine = $1.T.divL({divLine:1, wxn:'wrapx1',L:{textNode:'Observaciones'},I:{tag:'textarea','class':jsF,name:'lineMemo',placeholder:'Observacion, comentario...'}});
	wrap.appendChild(divLine);
	var tb = $1.T.table(['Nombre','Bodega','Cant.','Detalles','']); wrap.appendChild(tb);
	tb.classList.add('table_x100')
	var tBody = $1.t('tbody',0,tb);
	var trLast = $1.t('tr',0,tBody);
	var tdLast = $1.t('td',{colspan:5,style:'backgroundColor:#EEE;',textNode:'Buscar y añadir producto...'},trLast);
	var inpSea = $Sea.input(tdLast,{api:'itemPict',func:function(Jq){ 
		trLine(ni,Jq); ni++;
	}});
	var ni = 1; 
	function trLine(ni,L){
		var tc = 'itemAdd_'+L.itemId;
		if($1.q('.'+tc,tBody)){ $1.Win.message({text:'Este artículo ya esta relacionado.'});
			return false;
		}
		var ln = 'L['+ni+']';
		var tr = $1.t('tr',{'class':tc}); var lasttr = tBody.lastChild
		tBody.insertBefore(tr,trLast);
		$1.t('td',{textNode:L.itemName},tr);
		var tdWhs = $1.t('td',0,tr);
		var sel = $1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$V.ITM.whsCode,noBlank:1}); tdWhs.appendChild(sel);
		L.quantity=(L.quantity)?L.quantity:1;
		var td = $1.t('td',0,tr);
		var qua = $1.t('input',{type:'number',inputmode:'numeric','class':jsF+' _quantity',name:ln+'[quantity]',min:0,style:'width:3rem;',value:L.quantity, O:{vPost:ln+'[itemId]='+L.itemId}},td);
		var td = $1.t('td',0,tr);
		$1.t('input',{type:'text','class':jsF,name:ln+'[lineMemo]'},td);
		var td = $1.t('td',0,tr);
		var btnDel = $1.T.btnFa({fa:'fa_trash',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode);
		}}); td.appendChild(btnDel);
	}
	var resp = $1.t('div',0,wrap);
	var btnSend = $1.T.btnSend({textNode:'Generar Transferencia',confirm:{text:'Se va a generar una transferencia, no se puede modificar esta información.'}},{f:'POST '+Api.ITM.wtd,getInputs:function(){ return $1.G.inputs(wrap,jsF);}, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){ $M.ttHash({tt:'WTD',tr:Jr.handNum},'u'); }
	}});
	wrap.appendChild(btnSend);
},
get:function(P){
	cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.ITM.wtd, errWrap:cont, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		var tb = $1.T.table(['No','Tipo','Categoria','Fecha Doc.','Detalles','Realizado'],{tbData:{'class':'table_even'}}); cont.appendChild(tb);
		var tBody = $1.t('tbody',0,tb);
		for(var i in Jr.L){ L=Jr.L[i];
			var tr = $1.t('tr',0,tBody);
			var td = $1.t('td',0,tr);
			$1.t('a',{href:$M.ttHash({tt:'WTD',tr:L.handNum}),'class':'fa fa_eye', textNode:' '+L.handNum},td);
			$1.t('td',{textNode:ITM.Vs.WTDTypes.A[L.docType]},tr);
			$1.t('td',{textNode:ITM.Vs.WTDTypes[L.docType][L.catType]},tr);
			$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
			$1.t('td',{textNode:'Por '+L.userName+', el '+$2d.f(L.dateC,'mmm d H:iam')},tr);
		};
	}});
},
open:function(P){
	var Pr = $M.read();
	var cont = $M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.ITM.wtd+'.open', errWrap:cont, inputs:'handNum='+Pr.handNum ,func:function(Jr){
		var top = $1.t('div',{style:'border-bottom:0.0625rem solid #CCC; margin-bottom:1rem; text-align:center;'},cont);
		var head = $1.t('div',{'class':'ffLineNoBd'},cont);
		var lines = $1.t('div',0,cont);
		var bottom = $1.t('div',{style:'border-top:0.0625rem solid #CCC; margin-top:1rem; text-align:center; font-size:0.7rem;'},cont);
		$1.t('div',{textNode:'ADM Systems',style:'font-size:1.25rem; font-weight:bold;'},top);
		$1.t('div',{textNode:'N.I.T. 1125082294-4',style:'font-size:0.75rem;'},top);
		$1.t('div',{textNode:'Generado por ADM Systems'},bottom);
		$1.t('a',{href:'http://admsystems.co',textNode:'www.admsystems.co'},bottom);
		var fl = $1.T.ffLine({ffLine:1, t:'N° Documento:', v:Jr.handNum, w:'ffx3'},head);
		$1.T.ffLine({t:'Tipo:', v:'WTD', w:'ffx3'},fl);
		var fl = $1.T.ffLine({ffLine:1, t:'Fecha:', v:Jr.docDate, w:'ffxauto'},head);
		$1.T.ffLine({t:'Fecha Creación', v:$2d.f(Jr.dateC,'mmm d H:iam'), w:'ffx3'},fl);
		$1.T.ffLine({t:'Realizado', v:Jr.userName, w:'ffx3'},fl);
		if(Jr.lineMemo!=''){
			$1.t('div',{'class':'textarea',textNode:Jr.lineMemo,style:'margin:0.25rem 0;'},head);
		}
		var tb = $1.T.table(['Código',{textNode:'Descripción',style:'width:20rem;'},'Cantidad','Detalles'],{trHead:{'class':'trHead'}}); lines.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.itemCode},tr);
			$1.t('td',{textNode:L.itemName},tr);
			$1.t('td',{textNode:L.quantity*1},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
	}});
}

}

GeF = {
cRang:$V.carteraEdad,
cartera:function(v){
	var view = $1.q('.__cartViewType');
	switch(view.value){
		case 'card': GeF.carteraCard(v); break;
		default: GeF.carteraDoc(v); break;
	}
},
carteraCard:function(v){
	cont = $M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.GeF.a+v+'.card', inputs:$1.G.inputs($M.Ht.filt,'jsFiltVars'), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
		var tb = $1.T.table(['Contacto','Débito','Crédito','Corriente','Vencido']); cont.appendChild(tb);
		var tBody =$1.t('tbody',0,tb);
		var Tot = {deb:0, cred:0,corriente:0, due:0};
		for(var i in Jr.L){ L=Jr.L[i];
			var tr = $1.t('tr',0,tBody);
			var bal = 0;
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:$Str.money(L.balDueDeb)},tr);
			$1.t('td',{textNode:$Str.money(L.balDueCred)},tr);
			Tot.deb += L.balDueDeb*1; Tot.cred += L.balDueCred*1;
			Tot['corriente'] += (L.corriente)?L.corriente*1:0;
			Tot['due'] += (L.due)?L.due*1:0;
			var val = (L.corriente)?$Str.money(L.corriente):'';
			$1.t('td',{textNode:val},tr);
			var val = (L.due)?$Str.money(L.due):'';
			$1.t('td',{textNode:val},tr);
		}
		var trf=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total'},trf);
		$1.t('td',{textNode:$Str.money(Tot.deb)},trf);
		$1.t('td',{textNode:$Str.money(Tot.cred)},trf);
		$1.t('td',{textNode:$Str.money(Tot.corriente)},trf);
		$1.t('td',{textNode:$Str.money(Tot.due)},trf);
		
		}
	}});
},
carteraDoc:function(v){
	cont = $M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.GeF.a+v+'.doc', inputs:$1.G.inputs($M.Ht.filt,'jsFiltVars'), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
		var tb = $1.T.table(['Referencia','Contacto','Fecha','Vence','Débito','Crédito','Días']); cont.appendChild(tb);
		var tr0 = $1.q('thead tr',tb);
		for(var i in GeF.cRang){var R=GeF.cRang[i];
			$1.t('td',{textNode:R.t},tr0);
		}
			$1.t('td',{textNode:'Detalles'},tr0);
		var tBody =$1.t('tbody',0,tb);
		var Tot = {deb:0, cred:0};
		for(var i in Jr.L){ L=Jr.L[i];
			var tr = $1.t('tr',0,tBody);
			
			var bal = (L.balDueDeb-Math.abs(L.balDueCred))*1;
			var td = $1.t('td',0,tr);;
			$1.t('a',{href:$M.ttHash({tt:L.tt,tr:L.handNum}), textNode:L.tt+'-'+L.handNum},td);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
			$1.t('td',{textNode:$2d.f(L.dueDate,'mmm d')},tr);
			$1.t('td',{textNode:$Str.money(L.balDueDeb)},tr);
			$1.t('td',{textNode:$Str.money(L.balDueCred)},tr);
			$1.t('td',{textNode:L.days},tr);
			Tot.deb += L.balDueDeb*1; Tot.cred += L.balDueCred*1;
			var tR = $V.gCarteraEdad(L.days);
			for(var k in GeF.cRang){var R=GeF.cRang[k];
				var val = (tR.k==k)?$Str.money(bal):'';
				if(!Tot[k]){ Tot[k] = 0; } 
				if(tR.k==k){ Tot[k]+= bal; }
				$1.t('td',{textNode:val},tr);
			}
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		var trf=$1.t('tr',0,tBody);
		$1.t('td',{colspan:4,textNode:'Total'},trf);
		$1.t('td',{textNode:$Str.money(Tot.deb)},trf);
		$1.t('td',{textNode:$Str.money(Tot.cred)},trf);
		$1.t('td',null,trf);
		for(var k in GeF.cRang){ var R=GeF.cRang[k];
			var val = (Tot[k])?$Str.money(Tot[k]):'';
			$1.t('td',{textNode:val},trf);
		}
		}
	}});
}

}

GeF.f = {
accCateg:function(L){
	var r={t:L.tt+': '+L.tr};
	switch(L.tt){
		case 'EG': r.t = 'Gastos: '+L.tr; break;
		case 'RCA': r.t = 'Anticipo: '+L.tr; break;
		case 'PC': r.t = 'Pago de Cliente: '+L.tr; break;
		case 'PP': r.t = 'Pago a Proveedor: '+L.tr; break;
	}
	return r;
}
}

GeF.Egr={//Egresos
form:function(){
	var cont=$M.Ht.cont; var jsF='jsFields3';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Cuenta'},I:{tag:'select',sel:{'class':jsF,name:'acbId'},opts:$V.accountBank,noBlank:1}},cont);
	var se = $o.Se.form({opts:'bussPartner_all',jsFields:jsF,zIndex:1000,inLine:1,putDefine:{id:'cardId',t1:'cardName'}});
	$1.T.divL({wxn:'wrapx8_1',L:{textNode:'Contacto'},Inode:se},divL);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Tipo Gasto'},I:{tag:'select',sel:{'class':jsF,name:'egreType'},opts:$V.EgreTypes}},cont);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Clase'},I:{tag:'select',sel:{'class':jsF,name:'egreClass'},opts:$V.EgreClass,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Forma Pago'},I:{tag:'select',sel:{'class':jsF,name:'payMethod'},opts:$V.payMethod,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Valor'},I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'value',placeholder:'100.000,50'}},divL);
	$1.T.divL({divLine:1,wxn:'wrapx1',L:{textNode:'Observaciones'},I:{tag:'textarea','class':jsF,name:'lineMemo',placeholder:'sobre el egreso'}},cont);
	var resp=$1.t('div',0,cont);
	var btnSend=$1.T.btnSend({textNode:'Generar Egreso', confirm:{text:'Una vez generado no se puede eliminar el egreso'}},{f:'POST '+Api.GeF.a+'Egr', getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
	}}); cont.appendChild(btnSend);
},
get:function(P){ 
	cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.GeF.a+'Egr',inputs:$1.G.filter(), errWrap:cont, loade:cont, func:function(Jr){
		var tb=$1.T.table(['No','Fecha','Valor','Tipo','Clase','Forma Pago','Cuenta','Realizado']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb); var totalEgr = 0;
		for(var i in Jr.L){L=Jr.L[i];
			var css=(L.docStatus=='C')?'cash_cancel':'';
			var tr=$1.t('tr',0,tBody); totalEgr+=L.value*1;
			$1.t('td',{textNode:L.handNum},tr);
			$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
			$1.t('td',{textNode:$Str.money(L.value),'class':'__payValue'+L.docEntry+' '+css},tr);
			$1.t('td',{textNode:$V.EgreTypes[L.egreType]},tr);
			$1.t('td',{textNode:'-'},tr);
			//$1.t('td',{textNode:$V.EgreClass[L.egreClass]},tr);
			$1.t('td',{textNode:$V.payMethod[L.payMethod]},tr);
			$1.t('td',{textNode:$V.accountBank[L.acbId]},tr);
			$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
			var td=$1.t('td',{'class':'__payBtnCancel'+L.docEntry},tr);
			if(L.docStatus=='O'){
			var btn=$1.T.btnSend({'class':'fa faBtn fa_trash',L:L,textNode:' Anular Gasto',confirm:{text:'Una vez anulado el gasto, no se puede reversar la anulación'}},{f:'DELETE '+Api.GeF.a+'Egr.cancel', getInputs:function(T){ return 'docEntry='+T.L.docEntry; }, func:function(Jr2){ $1.Win.message(Jr2);
				if(!Jr2.errNo){
					$1.q('.__payValue'+Jr2.docEntry,tb).classList.add('cash_cancel');
					$1.clear($1.q('.__payBtnCancel'+Jr2.docEntry,tb));
				}
			}
			}); td.appendChild(btn);
			}
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total',colspan:2},tr);
		$1.t('td',{textNode:$Str.money(totalEgr)},tr);
		$1.t('td',{colspan:6},tr);
	}});
}
}

GeF.RC={
anti:function(){
	cont=$M.Ht.cont; var jsF='jsFields3';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:{textNode:'Fecha Elab.'},I:{tag:'input',type:'date','class':jsF,name:'docDate'}},cont);
	var se=$o.Se.form({opts:'bussPartner_cust',jsFields:jsF,zIndex:1000,inLine:1,putDefine:{id:'cardId',t1:'cardName'}});
	$1.T.divL({wxn:'wrapx8_1',req:'Y',L:{textNode:'Contacto'},Inode:se},divL);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Cuenta'},I:{tag:'select',sel:{'class':jsF,name:'acbId'},opts:$V.accountBank,noBlank:1}},cont);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Forma Pago'},I:{tag:'select',sel:{'class':jsF,name:'payMethod'},opts:$V.payMethod,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Valor'},I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'value'}},divL);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx1',L:{textNode:'Detalles'},I:{tag:'textarea','class':jsF,name:'lineMemo'}},cont);
	var resp=$1.t('div',0,cont);
	var btnS=$1.T.btnSend({textNode:'Guardar Ingreso'},{f:'POST '+Api.GeF.a+'RC.anti', loade:resp, getInputs:function(){ return $1.G.inputs(cont,jsF); }, func:function(Jr){
		$ps_DB.response(resp,Jr);
	}}); cont.appendChild(btnS);
}
}

GeF.Notas={//Notas Credito y Debito
form:function(docType){
	var cont=$M.Ht.cont; var jsF='jsFields3';
	var se = $o.Se.form({opts:'bussPartner_all',jsFields:jsF,zIndex:1000,inLine:1,putDefine:{id:'cardId',t1:'cardName'}});
	var divL= $1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Fecha'},I:{tag:'input',type:'date','class':jsF,name:'docDate',value:$2d.today}},cont);
	$1.T.divL({wxn:'wrapx8_1',L:{textNode:'Contacto'},Inode:se},divL);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Valor'},I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'value',placeholder:'100.000,50'}},cont);
	$1.T.divL({wxn:'wrapx8_1',L:{textNode:'Por concepto de'},I:{tag:'input',type:'text','class':jsF,name:'lineMemo',placeholder:'Devolución, interes mora, descuentos...'}},divL);
	var resp=$1.t('div',0,cont);
	var btnSend=$1.T.btnSend({textNode:'Generar Nota', confirm:{text:'Una vez generada no se puede eliminar'}},{f:'POST '+Api.GeF.a+'Notas', getInputs:function(){ var vPost = $1.G.inputs(cont,jsF); alert(vPost); return 'docType='+docType+'&'+vPost; }, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
	}}); cont.appendChild(btnSend);
},
get:function(docType){ 
	cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.GeF.a+'Notas',inputs:'docType='+docType+'&'+$1.G.filter(), errWrap:cont, loade:cont, func:function(Jr){
		var tb=$1.T.table(['No','Fecha','Valor','Contacto','Detalles','Realizado','']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb); var totalEgr = 0;
		for(var i in Jr.L){L=Jr.L[i];
			var css=(L.docStatus=='C')?'cash_cancel':'';
			var tr=$1.t('tr',0,tBody); totalEgr+=L.value*1;
			$1.t('td',{textNode:L.handNum},tr);
			$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
			$1.t('td',{textNode:$Str.money(L.value),'class':'__payValue'+L.payId+' '+css},tr);
			$1.t('td',{textNode:L.cardName},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
			$1.t('td',{textNode:'Por '+L.userName+', '+$2d.f(L.dateC,'mmm d H:iam')},tr);
			var td=$1.t('td',{'class':'__payBtnCancel'+L.payId},tr);
			if(L.docStatus=='O'){
			var btn=$1.T.btnSend({'class':'fa faBtn fa_trash',L:L,textNode:' Anular Gastos',confirm:{text:'Una vez anulado, no se puede reversar la anulación'}},{f:'DELETE '+Api.iPays.a+'o.cancel', getInputs:function(T){ return 'payId='+T.L.payId; }, func:function(Jr2){ $1.Win.message(Jr2);
				if(!Jr2.errNo){ $js.textChange('.FV__balDueDeb'+P.docEntry,$Str.money(Jr2.newBal));
					$1.q('.__payValue'+Jr2.payId,tb).classList.add('cash_cancel');
					$1.clear($1.q('.__payBtnCancel'+Jr2.payId,tb));
					iPays.get(tt,P,wList);
				}
			}
			}); td.appendChild(btn);
			}
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total',colspan:2},tr);
		$1.t('td',{textNode:$Str.money(totalEgr)},tr);
		$1.t('td',{colspan:6},tr);
	}});
}
}

GeF.AcB={
form:function(){
	var wrap=$M.Ht.cont; var jsF='jsFields';
	var Pr=$M.read();
	$ps_DB.get({f:'GET '+Api.GeF.a+'AcB.open', loadVerif:!Pr.acbId, loade:wrap, inputs:'acbId='+Pr.acbId, func:function(Jr){
	var P=(Jr && !Jr.errNo)?Jr:{};
	var wId = $1.t('input',{type:'hidden','class':jsF,name:'acbId',value:P.acbId},wrap);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx5',req:'Y',L:{textNode:'Tipo Cuenta'},I:{tag:'select',type:'select',sel:{'class':jsF,name:'acbType'},opts:$V.acbType,selected:P.acbType}},wrap);
	$1.T.divL({wxn:'wrapx5_1',req:'Y',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsF,name:'acbName',value:P.acbName,placeholder:'Bancolombia, City Bank...'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:{textNode:'Saldo Inicial'},I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'iniBal',value:P.iniBal,placeholder:'1,540,400'}},wrap);
	$1.T.divL({wxn:'wrapxauto',L:{textNode:'Fecha Saldo'},I:{tag:'date','class':jsF,name:'iniBalDate',value:P.iniBalDate}},divL);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'N°. Cuenta'},I:{tag:'input',type:'text','class':jsF,name:'acbNum',value:P.acbNum,placeholder:'725-981589-02'}},divL);
	$1.T.divL({divLine:1,wxn:'wrapx1',L:{textNode:'Descripción'},I:{tag:'textarea','class':jsF,name:'description',textNode:P.description,placeholder:'Cuenta de Ahorros'}},wrap);
	var resp=$1.t('div',0,wrap);
	var btn=$1.T.btnSend({textNode:'Guardar'},{f:'PUT '+Api.GeF.a+'AcB', getInputs:function(){ return $1.G.inputs(wrap,jsF); }, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){
			wId.value=Jr.acbId; $js.reload(jsFi.extends);
		}
	}});
	wrap.appendChild(btn);
	}});
},
get:function(docType){ 
	cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.GeF.a+'AcB',inputs:$1.G.filter(), errWrap:cont, loade:cont, func:function(Jr){
		var tb=$1.T.table(['Tipo','Cuenta','Número','Saldo','Saldo Inicial','Fecha Saldo','Descripción','Creación']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb); var totalEgr = 0;
		for(var i in Jr.L){L=Jr.L[i];
			var tr=$1.t('tr',0,tBody); totalEgr+=L.value*1;
			$1.t('td',{textNode:$V.acbType[L.acbType]},tr);
			$1.t('td',{textNode:L.acbName},tr);
			$1.t('td',{textNode:L.acbNum},tr);
			$1.t('td',{textNode:$Str.money(L.bal)},tr);
			$1.t('td',{textNode:$Str.money(L.iniBal)},tr);
			$1.t('td',{textNode:$2d.f(L.iniBalDate,'mmm d')},tr);
			$1.t('td',{textNode:L.description},tr);
			$1.t('td',{textNode:$2d.f(L.dateC,'mmm d H:iam')+' Por '+L.userName},tr);
			var td =$1.t('td',0,tr);
			var btn= $1.T.btnFa({fa:'fa_eye',title:'Ver movimientos', L:L, func:function(T){ $M.to('GeF.AcB.move','acbId:'+T.L.acbId); }}); td.appendChild(btn);
			var btn= $1.T.btnFa({fa:'fa_pencil',title:'Modificar Cuenta', L:L, func:function(T){ $M.to('GeF.AcB.form','acbId:'+T.L.acbId); }}); td.appendChild(btn);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total',colspan:2},tr);
		$1.t('td',{textNode:$Str.money(totalEgr)},tr);
		$1.t('td',{colspan:6},tr);
	}});
},
move:function(){
	cont =$M.Ht.cont;
	var Pr=$M.read();
	$ps_DB.get({f:'GET '+Api.GeF.a+'AcB.move', inputs:$1.G.filter()+'&acbId='+Pr.acbId, loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['Fecha','Categoría','Entradas','Salidas','']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var css=(L.docStatus=='N')?'cash_cancel':'';
				var ct= GeF.f.accCateg(L);
				var tr = $1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docDate},tr);
				var td=$1.t('td',0,tr);
				$1.t('a',{href:$M.ttHash({tt:L.tt,tr:L.tr}),textNode:ct.t},td);
				$1.t('td',{textNode:$Str.money(L.ing),'class':css},tr);
				$1.t('td',{textNode:$Str.money(L.egr),'class':css},tr);
				$1.t('td',{textNode:''},tr);
			};
		}
	}});
},

}


// apis2 ya
var Whs = { 
form:function(){
	var Pr = $M.read();;
	var cont = $M.Ht.cont;
	$Api.get({f:Api.Vapp.b+'.open', loadVerif:!Pr.whsId, loade:cont, inputs:'whsId='+Pr.whsId, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		P=(Jr)?Jr:{};
	var jsF='jsFields';
	var wId = $1.t('input',{type:'hidden','class':jsF,name:'whsId',value:P.whsId},cont);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx1',req:'Y',L:{textNode:'Nombre Bodega'},I:{tag:'input',type:'text','class':jsF,name:'whsName',value:P.whsName,'x-req':'owhs.whsName'}},cont);
	var divL = $1.T.divL({divLine:1,wxn:'wrapx2',L:{textNode:'Teléfono'},I:{tag:'input',type:'text','class':jsF,name:'phone',placeholder:'323 3737',value:P.phone}},cont);
	$1.T.divL({wxn:'wrapx2',L:{textNode:'Dirección'},I:{tag:'input',type:'text','class':jsF,name:'address',placeholder:'Carrera 29 #33-10',value:P.address}},divL);
	var resp=$1.t('div',0,cont);
	$Api.send({PUT:Api.Vapp.b, getInputs:function(){ return $1.G.inputs(cont,jsF); },resp:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
		if(!Jr.errNo){ wId.value = Jr.whsId; $js.reload(jsFi.extends); }
	}},cont);
	}});
},
get:function(){
	var cont = $M.Ht.cont;
	$Api.get({f:Api.Vapp.b, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb=$1.T.table(['Nombre','Teléfono','Dirección','']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.whsName},tr);
				$1.t('td',{textNode:L.phone},tr);
				$1.t('td',{textNode:L.address},tr);
				var td=$1.t('td',0,tr);
				var edit = $1.T.btnFa({fa:'fa_pencil',textNode:'Modificar', L:L, func:function(T){
					$M.to('whs.form','whsId:'+T.L.whsId);
				}}); td.appendChild(edit);
			}
			
		}
		
	}});
}
}

var ITM = {//api2
form:function(){
	cont = $M.Ht.cont;
	var jsF = 'jsFields';
	var Pr = $M.read();;
	$Api.get({f:Api.Vapp.itm+'.open', loadVerif:!Pr.itemId, loade:cont, inputs:'itemId='+Pr.itemId, func:function(Jr){
		P=(Jr)?Jr:{};
		P.udm=(P.udm)?P.udm:'Und';
		if(Pr.itemId){ $1.q('#__wrapTitle').innerText = 'Modificar Producto'; }
		var wrap = $1.t('div',{id:'_ITM_formCreate'},cont);
		var inp = $1.t('input',{type:'hidden','class':jsF,name:'itemId',value:Pr.itemId},wrap);
		var divL = $1.T.divL({divLine:1, wxn:'wrapx8',req:'Y',L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:P.itemCode,placeholder:'Código del Producto'}});
		divL.appendChild($1.T.divL({wxn:'wrapx4',req:'Y',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsF,name:'itemName',value:P.itemName,placeholder:'Nombre Producto Venta'}}));
		divL.appendChild($1.T.divL({wxn:'wrapx8',L:{textNode:'Und. Medida',title:'Unidad de Medida'},I:{tag:'input',type:'text','class':jsF,name:'udm',value:P.udm,placeholder:'Unidad de Medida'}}));
		divL.appendChild($1.T.divL({wxn:'wrapx8',req:'Y',L:{textNode:'Categoria'},I:{tag:'input',type:'text','class':jsF,name:'categName',value:P.categName,placeholder:'Bota, Collar, Camiseta'}}));
		wrap.appendChild(divL);
		var price = $1.t('input',{type:'text','class':jsF+' _price',name:'price',value:P.price,placeholder:'En cuánto lo vendo?',numberformat:'mil',onkeychange:function(){ updPriceTax(this) }});;
		var divL = $1.T.divL({divLine:1, wxn:'wrapx8',req:'Y',L:{textNode:'Precio Venta'},Inode:price},wrap);
		var tax = $1.t('input',{type:'number',inputmode:'numeric','class':jsF+' __tax',name:'tax',value:P.tax,placeholder:'19%',min:0,max:100});
		$1.T.divL({wxn:'wrapx8',L:{textNode:'Imp. Venta %'},Inode:tax},divL);
		var befTax = $1.t('input',{type:'text','class':jsF,name:'priceBefTax',value:P.priceBefTax,numberformat:'mil',disabled:'disabled'});
		divL.appendChild($1.T.divL({wxn:'wrapx8',L:{textNode:'Precio Sin Imp.',openInf:'itm.priceBefTax'},Inode:befTax}));
		var buyPrice = $1.t('input',{type:'text','class':jsF,name:'buyPrice',value:P.buyPrice,numberformat:'mil',onkeychange:function(){ updPriceTax(this) }});
		var divL = $1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:{textNode:'Costo Compra'},Inode:buyPrice},wrap);
			$1.T.divL({wxn:'wrapx8',L:{textNode:'Imp. Compra %'},I:{tag:'input',type:'number',inputmode:'numeric','class':jsF+' __tax',name:'buyTax',value:P.buyTax,placeholder:'19%',min:0,max:100}},divL);
			var util = $1.t('input',{type:'text','class':jsF,name:'utilidad',value:P.utilidad,numberformat:'mil',disabled:'disabled'});
		divL.appendChild($1.T.divL({wxn:'wrapx8',L:{textNode:'Utilidad',openInf:'itm.priceBefTax'},Inode:util}));
		divL.appendChild($1.T.divL({wxn:'wrapx8',L:{textNode:'Costo Promedio',openInf:'itm.avgBuyPrice'},I:{tag:'input',type:'text',numberformat:'mil',disabled:'disabled',value:P.avgBuyPrice,placeholder:' '}}));
		wrap.appendChild(divL);
		var divL = $1.T.divL({divLine:1, wxn:'wrapx8',L:{textNode:'Maneja Inventario',openInf:'itm.handInv'},I:{tag:'select',sel:{'class':jsF+' handInv',name:'handInv'},opts:{N:'No',Y:'Si'},noBlank:1, selected:P.handInv}},wrap);
		var canNeg =$1.T.divL({wxn:'wrapx5',L:{textNode:'Permitir Negativos',openInf:'itm.handInv'},I:{tag:'select',sel:{'class':jsF+' canNeg',name:'canNeg'},opts:{N:'No',Y:'Si'},noBlank:1, selected:P.canNeg}},divL);
		if(!P.handInv || P.handInv=='N'){ canNeg.style.display ='none'; }
		$1.T.divL({wxn:'wrapx4',L:{textNode:'Proveedor'},I:{tag:'input',type:'text','class':jsF,name:'cardName',value:P.cardName,placeholder:'Quién me vende?'}},divL);
		$1.T.divL({divLine:1, wxn:'wrapx1',L:{textNode:'Descripción'},I:{tag:'textarea','class':jsF,name:'description',placeholder:'Descripción breve del producto...',textNode:P.description}},wrap);
		tax.onkeyup = tax.onkeyup = function(){ updPriceTax(); }
		var tb = $1.t('table',0,wrap); var tBody= $1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		var td= $1.t('td',0,tr); $1.t('b',{textNode:'Imagen del Producto'},td);
		var tr=$1.t('tr',0,tBody);
		var td=$1.t('td',{style:'width:10rem; max-height:10rem;'},tr);
		$1.T.imgUpd({name:'src1',value:P.src1},td);
		function updPriceTax(){
			var p = $Str.toNumber(price.value);
			var bP = $Str.toNumber(buyPrice.value);
			befTax.value = p-(p*tax.value/100);
			util.value = befTax.value-bP;
		}
		var resp = $1.t('div',0,wrap);
		var btnSend = $Api.btnSend({textNode:'Guardar Producto'},{PUT:Api.Vapp.itm, getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr){
			$ps_DB.response(resp,Jr);
				if(!Jr.errNo){ inp.value = Jr.itemId; $M.to('items.form','itemId:'+Jr.itemId); }
		}});
		wrap.appendChild(btnSend);
		handInv = $1.q('.handInv',wrap);
		handInv.onchange = function(){
			if(this.value=='N'){ canNeg.style.display='none'; }
			else{ canNeg.style.display ='block'; }
		}
	}});
},
get:function(){
	cont =$M.Ht.cont;
	$Api.get({f:Api.Vapp.itm, inputs:$1.G.filter(), loade:cont, 
	func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var tb = $1.T.table(['Código','Nombre','Categoria','Precio Venta','Costo','Imp. Venta',{textNode:'Inv.',title:'Maneja Inventario'},{textNode:'Neg.',title:'Permitir Negativos'},'UdM','']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$M.to('items.view','itemId:'+L.itemId,'r'), textNode:L.itemCode},td);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.categName},tr);
				$1.t('td',{textNode:$Str.money({value:L.price})},tr);
				$1.t('td',{textNode:$Str.money({value:L.buyPrice})},tr);
				$1.t('td',{textNode:((L.tax)?L.tax:0)+'%'},tr);
				$1.t('td',{textNode:$V.comun[L.handInv]},tr);
				$1.t('td',{textNode:$V.comun[L.canNeg]},tr);
				$1.t('td',{textNode:L.udm},tr);
				var td =$1.t('td',0,tr);
				var edit = $1.T.btnFa({fa:'fa_pencil',textNode:'Modificar', L:L, func:function(T){
					$M.to('items.form','itemId:'+T.L.itemId);
				}}); td.appendChild(edit);
			};
		}
	}});
},
open:function(){
	var Pr = $M.read();; var cont = $M.Ht.cont;
	$Api.get({f:Api.Vapp.itm+'.open', errWrap:cont, inputs:'itemId='+Pr.itemId, loade:cont,
	func:function(Jr){
		var L = (Jr)?Jr:{};
		var edit = $1.T.btnFa({fa:'fa_pencil',textNode:'Modificar',func:function(){
			$M.to('items.form','itemId:'+Pr.itemId);
		}}); cont.appendChild(edit);
		var tb = $1.T.table(['','']); cont.appendChild(tb);
		var tBody = $1.t('tbody',0,tb);
		var tr = $1.t('tr',0,tBody);
		var td= $1.t('td',{style:'width:10rem;',rowspan:3},tr);
			$1.t('img',{src:L.src1},td);
		$1.t('td',{textNode:'Código'},tr); $1.t('td',{textNode:L.itemCode},tr);
		var tr = $1.t('tr',0,tBody);
		$1.t('td',{textNode:'Nombre'},tr); $1.t('td',{textNode:L.itemName},tr);
		var tr = $1.t('tr',0,tBody);
		$1.t('td',{textNode:'Precio Venta'},tr); $1.t('td',{textNode:$Str.money(L.price)},tr);
		var tr = $1.t('tr',0,tBody);
		$1.t('td',{textNode:'Precio Antes de Imp.'},tr); $1.t('td',{textNode:$Str.money(L.priceBefTax)},tr);
		$1.t('td',{textNode:L.tax+'% de Impuesto'},tr);
		var tr = $1.t('tr',0,tBody);
		$1.t('td',{textNode:'Costo Compra'},tr); $1.t('td',{textNode:$Str.money(L.buyPrice)},tr);
		$1.t('td',{textNode:$Str.money(L.utilidad)+' de Utilidad'},tr);
		var tr = $1.t('tr',0,tBody);
		$1.t('td',{textNode:'Proveedor'},tr); $1.t('td',{textNode:L.cardName,colspan:2},tr);
		var tr = $1.t('tr',0,tBody);
		var td = $1.t('td',{colspan:3},tr);
		$1.t('pre',{textNode:L.description},td);
	}});
}
}

_i = {//Information
'invoice.form.docDate':'Fecha en la que se emite la factura',
'invoice.form.dueDate':'Fecha de vencimiento, fecha plazo máximo para pagar',
}

/* templates */
$Tpt.T['gvtInv']=function(cont,P){
	var td=$1.t('div'); $1.t('b',{textNode:'Detalles'},td); $1.t('pre',{textNode:P.Jr.lineMemo},td);;
		var td2=$1.t('div'); $1.t('b',{textNode:'Condiciones'},td2); $1.t('pre',{textNode:P.Jr.termsConditions},td2);
		var Ls=[
		{t:'Estado',v:$V.docStatus[P.Jr.docStatus]},{middleInfo:'Y'},{logoRight:'Y'},
		{tag:'docDate'},
		{t:'Vencimiento',v:P.Jr.dueDate},
		{t:'Condic. Pago',v:$Tb._get('pymntGr',P.Jr.pymntGrId)},{tag:'cliente',cs:3,ln:1},{t:'Asesor',v:$Tb._get('oslp',P.Jr.slpId),ln:1}
		];
		Ls.push({v:td,cs:4}); Ls.push({v:td2,cs:4,ln:1});
		var discw = $1.t('div',0);
		$1.t('div',{textNode:'- Descuento : '+P.Jr.discPf*1+'%'},discw);
		$1.t('div',{textNode:'Desc. Total : '+P.Jr.discTotal*1+'%',style:'font-size:0.7rem;'},discw);
		
	$Tpt.draw(cont,{D:P.Jr,serieType:'gvtInv',print:'Y',
		Ls:Ls,
		fieldset:'Lineas del Documento',
		Tb:[{textNode:'Código',style:'width:3rem;'},{textNode:'Descripción'},{textNode:'Precio',style:'width:5rem;'},{textNode:'Cantidad'},{textNode:'Total'}],
		Trs:P.Trs,
		softFrom:'Y',
		bottomCont:$1.t('div',{textNode:'Precios antes de IVA',style:'font-size:0.75rem; text-align:center;'}),
		Foot:[
			[{textNode:'Total Lineas',colspan:4},{textNode:$Str.money(P.Jr.docTotalLine)}],
			[{colspan:4,node:discw},{textNode:$Str.money(P.Jr.discSum)}],
			[{textNode:'= Subtotal',colspan:4},{textNode:$Str.money(P.Jr.baseAmnt)}],
			[{textNode:'+ Impuestos',colspan:4},{textNode:$Str.money(P.Jr.taxIVA)}],
			[{textNode:'- Rte. Imp',colspan:4},{textNode:$Str.money(P.Jr.taxRTE)}],
			[{textNode:'Total a Pagar',colspan:4},{textNode:$Str.money(P.Jr.docTotal)}],
		]
	});
}