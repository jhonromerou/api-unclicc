<?php
class _5b{
static $whPerm = '';
static public function __ini(){
self::$whPerm = 'AND (
WB0.wboPriv=\'share\' 
OR  WB1.authRel=\'share\'
OR (WB0.wboPriv=\'owner\' AND WB0.userId=\''.a_ses::$userId.'\' )
OR (WB0.wboPriv=\'members\' AND WB0.userId=\''.a_ses::$userId.'\' )
OR (WB0.wboPriv=\'members\' AND WB20.userId=\''.a_ses::$userId.'\' )
)';
}

static public function authRel($D=array()){
	self::__ini();
	$js = '';
	$whUser = (a_ses::$user == 'supersu') ? '' : self::$whPerm;
	$qW = a_sql::fetch('SELECT WB1.authRel, WB0.userId FROM '._ADMS::$TBSoc['ap1_owbo'].' WB0 INNER JOIN '._ADMS::$TBSoc['ap1_wbo1'].' WB1 ON (WB1.wboId = WB0.wboId) LEFT JOIN '._ADMS::$TBSoc['ap1_wbo20'].' WB20 ON (WB20.wboId = WB0.wboId) WHERE 1 '.$whUser.' AND WB1.wboListId=\''.$D['wboListId'].'\' GROUP BY WB1.wboListId ORDER BY WB0.wboName ASC, WB1.listName ASC LIMIT 1');
	if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$qW['error_sql']); }
	else if(a_sql::$errNo == 2){ $js = _ADMS::jsonError(1,'Error obteniendo información de la lista del tablero ('.$D['wboListId'].'), quizá no tenga permisos para realizar la acción.'); }
	else if($qW['authRel'] == 'owner' && $qW['userId'] != a_ses::$userId){
		$js = _ADMS::jsonError(4,'Solo el Propietario del Tablero puede modificar la relación de actividades de esta Lista.');
	}
	return $js;
}

static public function i_list($wboListId=0,$fields=''){
	$f = 'WB0.userId,WB0.wboId,WB1.wboListId';
	$f .= ($fields!='')? ','.$fields : '';
	$qBo = a_sql::fetch('SELECT '.$f.' FROM '._ADMS::$TBSoc['ap1_owbo'].' WB0 INNER JOIN '._ADMS::$TBSoc['ap1_wbo1'].' WB1 ON (WB1.wboId = WB0.wboId) WHERE WB1.wboListId=\''.$wboListId.'\' LIMIT 1');
	return $qBo;
}

static public function add2List($Di=array()){
	if($Di['objType'] == _o::$Ty['activity']){ $Di['actId'] = $Di['objRef']; };
	unset($Di['objType'],$Di['objRef']);
	$ins = a_sql::insert($Di,array('u_dateC'=>true,'table'=>_ADMS::$TBSoc['ap1_wbo10'],'wh_change'=>'WHERE wboListId=\''.$Di['wboListId'].'\' AND actId=\''.$Di['actId'].'\' LIMIT 1','no_update'=>'userId,userName,dateC'));
	return $ins;
}

}

class _5a{
static $objType = 'activity';
static $asuntLimit = 700;
static $LG = array(
'err_owner'=>array('errNo'=>4,'text'=>'Solo el propietario o responsable de la actividad puede modificar la información.'),
'err_actAsunt'=>array('errNo'=>3,'text'=>'Se debe definir el Asunto.'),
'err_actAsunt_strlen'=>array('errNo'=>3,'text'=>'El asunto no puede exceder 700 caracteres.'),
'err_actType'=>array('errNo'=>3,'text'=>'Se debe definir el Tipo.'),
'err_deadLine_dueDate'=>array('errNo'=>3,'text'=>'La fecha de Vencimiento debe estar definida para este tipo de Actividad.'),
'err_wboListId_notFound'=>array('errNo'=>5,'text'=>'La actividad debe estar relaciona a alguna lista.'),
'err_actId_notFound'=>array('errNo'=>5,'text'=>'No se ha definido la actId.'),
'err_updField_fieldNotFound'=>array('errNo'=>5,'text'=>'No se ha definido el campo a actualizar.'),

'err_moveType_notFound'=>array('errNo'=>5,'text'=>'Error moviendo tarjeta, moveType no definido.'),
'err_moveType_notAllow'=>array('errNo'=>5,'text'=>'Movimiento definido no encontrado o permitido: '),
'err_moveType_fromTo_fieldNotFound'=>array('errNo'=>5,'text'=>'Error moviendo tarjeta, El destino o origen del objeto no están definidos.'),
);

function i_get($actId=0,$fields=''){
	$f = 'ACT0.userId,ACT0.userAssg';
	$f .= ($fields!='')? ','.$fields : '';
	$qBo = a_sql::fetch('SELECT '.$f.' FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 WHERE ACT0.actId=\''.$actId.'\' LIMIT 1');
	return $qBo;
}

function Auth($actId=0){
	$jsAuth = '';
	if($actId != 'newS'){
		$oAct = a_sql::fetch('SELECT userId,userAssg FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$actId.'\' LIMIT 1');
		if(a_sql::$errNo == 1){$jsAuth = _ADMS::jsonError(4,$oAct['error_sql']); }
		else if($oAct['userId'] != a_ses::$userId && $oAct['userAssg'] != a_ses::$userId){ $jsAuth = _ADMS::jsonError(4,'Solo el Propietario o Responsable pueden modificar está información.'); }
	}
	return $jsAuth;
}

static public function query($type='wboId',$D=array()){
	$uS = ($D['userId']) ? $D['userId']:a_ses::$userId;
	$wboId = $D['wboId'];
	$whPerm = 'AND (
WB0.wboPriv=\'share\' 
OR (WB0.wboPriv=\'owner\' AND WB0.userId=\''.$uS.'\')
OR (WB0.wboPriv=\'members\' AND WB0.userId=\''.$uS.'\' )
OR (WB0.wboPriv=\'members\' AND WB20.userId=\''.$uS.'\' )
)';
	$FILCONF = $D['__FIEC']; unset($D['__FIEC']);
	$whComp = ($FILCONF['completed']) ? ' AND ACT0.completed =\''.$FILCONF['completed'].'\' ' : ' AND ACT0.completed =\'N\' ';
	$whComp = ($FILCONF['completed'] == 'all') ? ' ' : $whComp;
	$whArc = ($FILCONF['archived']) ? ' AND ACT0.archived =\''.$FILCONF['archived'].'\' ' : ' AND ACT0.archived =\'N\' ';
	$whArc = ($FILCONF['archived'] == 'all') ? ' ' : $whArc;
	$whComp .= $whArc;
	$te = $D['_1f_text'];
	$wh_uGroupId = $D['uGroupId'];
	unset($D['_1f_text'],$D['wboId'],$D['uGroupId']);
	$whSe = (!_js::isse($te)) ? 'AND (ACT0.actAsunt LIKE \'%'.$te.'%\' OR oCrd.cardName LIKE \'%'.$te.'%\')' : '';
	$wh = $whSe . a_sql_filtByT($D);
	$lef_oMbo = 'LEFT JOIN '._ADMS::$TBSoc['ap0_ombo'].' MBO ON (MBO.objType=\''._o::$Ty['activity'].'\' AND MBO.objRef=ACT0.actId)';
	$lef_oSrc = 'LEFT JOIN '._ADMS::$TBSoc['ap0_obj2'].' obj2 ON (obj2.objType=\''._o::$Ty['activity'].'\' AND obj2.objRef=ACT0.actId) 
LEFT JOIN '._ADMS::$TBSoc['par_ocrd'].' oCrd ON (obj2.childType=\''._o::$Ty['bussPartner'].'\' AND oCrd.cardId = obj2.childRef)';
	if($type == 'withCards'){
		$query = 'SELECT ACT0.*, MBO.userMember,oSrc.objType, oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 '.$lef_oSrc.' WHERE 1 '.$whComp.' '.$whUser.' ORDER BY CASE WHEN ACT0.doDate >0 THEN 0 ELSE 1 end ASC';
	}
	else if($type=='admin'){
		$whIds =  a_ses::usg(array('noMe'=>true,'uGroupId'=>$wh_uGroupId));
		$whUser = 'AND (ACT0.userId IN ('.$whIds.') OR ACT0.userAssg IN ('.$whIds.') OR MBO.userMember IN ('.$whIds.')) ';
		$query = 'SELECT ACT0.*, MBO.userMember, oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 LEFT JOIN '._ADMS::$TBSoc['ap0_ombo'].' MBO ON (MBO.objType=\''._o::$Ty['activity'].'\' AND MBO.objRef=ACT0.actId) '.$lef_oSrc.' WHERE 1 '.$wh.' '.$whComp.' '.$whUser.' ORDER BY CASE WHEN ACT0.doDate >0 THEN 0 ELSE 1 end ASC';
	}
	else if($type=='unboard'){
		$whUser = 'AND (ACT0.userId=\''.$uS.'\' OR ACT0.userAssg=\''.$uS.'\' OR MBO.userMember=\''.$uS.'\') ';
		$query = 'SELECT ACT0.*, MBO.userMember, oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 LEFT JOIN '._ADMS::$TBSoc['ap0_ombo'].' MBO ON (MBO.objType=\''._o::$Ty['activity'].'\' AND MBO.objRef=ACT0.actId) '.$lef_oSrc.' WHERE 1 '.$wh.' '.$whComp.' '.$whUser.' ORDER BY CASE WHEN ACT0.doDate >0 THEN 0 ELSE 1 end ASC';
	}
	else if($type=='unboard__v1'){
		$whUser = 'AND (ACT0.userId=\''.$uS.'\' OR ACT0.userAssg=\''.$uS.'\' OR MBO.userMember=\''.$uS.'\') ';
		$query = 'SELECT ACT0.*, MBO.userMember,oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 LEFT JOIN '._ADMS::$TBSoc['ap0_ombo'].' MBO ON (MBO.objType=\''._o::$Ty['activity'].'\' AND MBO.objRef=ACT0.actId) '.$lef_oSrc.' WHERE 1 '.$wh.' '.$whComp.' '.$whUser.' ORDER BY CASE WHEN ACT0.doDate >0 THEN 0 ELSE 1 end ASC';
	}
	else if(1){
		$query = 'SELECT WB10.id wboRelId, WB1.wboListId, WB1.actTotal, WB1.actOpen, ACT0.*,oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_wbo1'].' WB1 INNER JOIN '._ADMS::$TBSoc['ap1_wbo10'].' WB10 ON (WB10.wboListId= WB1.wboListId) INNER JOIN '._ADMS::$TBSoc['ap1_oact'].' ACT0 ON (ACT0.actId = WB10.actId '.$whComp.') '.$lef_oSrc.' WHERE WB1.wboId =\''.$wboId.'\' '.$wh.' ORDER BY WB1.wboPosic ASC, WB1.wboListId ASC';
	}
	else{
		$query = 'SELECT ACT0.*, MBO.userMember,oSrc.objType, oCrd.cardId, oCrd.cardName FROM '._ADMS::$TBSoc['ap1_oact'].' ACT0 LEFT JOIN '._ADMS::$TBSoc['ap0_osrc'].' oSrc ON (oSrc.objpType=\''._o::$Ty['activity'].'\' AND oSrc.objpRef=ACT0.actId)  LEFT JOIN '._ADMS::$TBSoc['par_ocrd'].' oCrd ON (oSrc.objType=\''._o::$Ty['bussPartner'].'\' AND oCrd.cardId = oSrc.objRef) LEFT JOIN '._ADMS::$TBSoc['ap0_ombo'].' MBO ON (MBO.objType=\''._o::$Ty['activity'].'\' AND MBO.objRef=ACT0.actId) WHERE 1 '.$whComp.' '.$whUser.' ORDER BY CASE WHEN ACT0.doDate >0 THEN 0 ELSE 1 end ASC';
	}
	return $query;
}

function sBy_wboList($D=array()){
	$unboard = ($D['wboId'] == 'unboard') ? true : false; unset($D['wboId']);
	if(!$unboard && $D['wboListId'] == ''){ return _ADMS::jsonError(self::$LG['err_wboListId_notFound']); }
	_ADMS::_lb('com/_2d');
	$authRel = _5b::authRel($D); unset($D['listName']);
	if(!$unboard && $authRel != ''){ $js =  $authRel; }
	else if($D['actAsunt'] == ''){ $js = _ADMS::jsonError(self::$LG['err_actAsunt']); }
	else if(strlen($D['actAsunt'])>self::$asuntLimit){ $js = _ADMS::jsonError(self::$LG['err_actAsunt_strlen']); }
	else if($D['actType'] == ''){ $js = _ADMS::jsonError(self::$LG['err_actType']); }
	else if($D['actType'] == 'deadLine' && _2d::is0($D['dueDate'])){ $js = _ADMS::jsonError(self::$LG['err_deadLine_dueDate']); }
	else{
		$D['targetType'] = 'wboList';
		$D['targetRef'] = $wboListId = $D['wboListId']; unset($D['wboListId']);
		if($D['timeDuration'] && !_2d::is0($D['doDate'])){
			$extTime = _2d::str2Hour($D['timeDuration']);
			$D['endDate'] = date('Y-m-d H:i',(strtotime($D['doDate'])+($extTime)));
		}
		else{
			$D['timeDuration'] = (_2d::toHours(strtotime($D['endDate'])-strtotime($D['doDate'])));
		}
		$O = $D['_O'];//_O para relaciones
		unset($D['_O'],$D['objType'],$D['textSearch']);
		$ins = a_sql::insert($D,array('POST'=>true,'ou_dateC'=>true,'table'=>_ADMS::$TBSoc['ap1_oact']));
		if($ins['err']){ $js = _ADMS::jsonError(1,$ins['err']['error_sql']); }
		else{
			//o1::plan_put(_o::$Ty['activity']);
			$js = _ADMS::jsonResp('Guardada Correctamente.');
			$D['actId'] = $D['actId'] = ($ins['insertId']) ? $ins['insertId'] : $actId;
			$P['actId'] = $D['actId'];
			$P['objType'] = _o::$Ty['activity'];
			$P['objRef'] = $P['actId'];
			$P['targetType'] = _o::$Ty['wboList'];
			$P['targetRef'] = $wboListId;
			$P['lineMemo'] = $D['actAsunt'];
			$P['__notiVerif'] = true;
			if(!$unboard){
				$D3 = array('wboListId'=>$wboListId,'actId'=>$P['actId']);
				$ins = a_sql::insert($D3,array('POST'=>true,'table'=>_ADMS::$TBSoc['ap1_wbo10']));
			}
			if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$ins['error_sql']); }
			else{ _ADMS::_lb('com/_6');
				if(is_array($O) && $O['objType'] == _o::$Ty['bussPartner']){
					_ADMS::_lb('com/_5o');
					_5o::src_put(array('objpType'=>_o::$Ty['activity'],'objpRef'=>$D['actId'], 'objType'=>$O['objType'],'objRef'=>$O['objRef']));
				}
				$D2 = a_sql::fetch('SELECT * FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$D['actId'].'\' LIMIT 1');
				$D2['keyO'] = ($D['actId']*1);
				$add = '"addKard":'.a_sql::JSON($D2);
				$jsResp = array('text'=>'Creada y puesta en tablero','add'=>$add);
				$js = _6::ouFn($P,$jsResp);
			}
		}
	}
	return $js;
}

function S_full($P=array()){
	$actId = ($P['actId']) ? $P['actId'] : 'newS';
	$jsAuth = self::Auth($actId);
	_ADMS::_lb('com/_2d');
	if($jsAuth !=''){ $js = $jsAuth; }
	else if($P['actAsunt'] == ''){ $js = _ADMS::jsonError(self::$LG['err_actAsunt']); }
	else if(strlen($P['actAsunt'])>self::$asuntLimit){ $js = _ADMS::jsonError(self::$LG['err_actAsunt_strlen']); }
	else if($P['actType'] == ''){ $js = _ADMS::jsonError(self::$LG['err_actType']); }
	else if($P['actType'] == 'deadLine' && _2d::is0($P['dueDate'])){ $js = _ADMS::jsonError(self::$LG['err_deadLine_dueDate']); }
	else{
		if($P['doDate'] == ''){ $P['endDate'] = ''; }
		$P['endDate'] = ($P['endDate']) ? $P['endDate'] : $P['doDate'];
		$P['endTime'] = ($P['endTime']) ? $P['endTime'] : $P['doTime'];
		$P['doDate'] .= ' '.$P['doTime']; 
		$P['endDate'] .= ' '.$P['endTime'];
		_ADMS::_lb('com/_6');
		if($P['timeDuration'] && !_2d::is0($P['doDate'])){
			$extTime = _2d::str2Hour($P['timeDuration']);
			$P['endDate'] = date('Y-m-d H:i',(strtotime($P['doDate'])+($extTime)));
		}
		else{
			$P['timeDuration'] = (_2d::toHours($timeDif));
		}
		$timeDif = strtotime($P['endDate'])-strtotime($P['doDate']);
		$P['timeDuraText'] = _2d::toText($timeDif);
		unset($P['actId'],$P['doTime'],$P['endTime']);
		$P['dateC'] = date('Y-m-d H:i:s');
		$ins = a_sql::insert($P,array('u_dateC'=>true,'table'=>_ADMS::$TBSoc['ap1_oact'], 'wh_change'=>' WHERE actId=\''.$actId.'\' LIMIT 1','no_update'=>'dateC,userId,userName'));
		if($ins['err']){ $js = _ADMS::jsonError(1,$ins['err']['error_sql']); }
		else{
			$P['actId'] = ($ins['insertId']) ? $ins['insertId'] : $actId;
			$P['objType'] = _o::$Ty['activity'];
			$P['objRef'] = $P['actId'];
			$P['lineMemo'] = $P['actAsunt'];
			$P['__notiVerif'] = true;
			$jsResp = array('text'=>'Creada y puesta en tablero');
			$js = _6::ouFn($P,$jsResp);
		}
	}
	return $js;
}

function S_updField($D=array()){
	//movType,to, to2
	$userId = a_ses::$userId;
	$movType = $D['movType'];
	$to = $D['to']; $to2 = $D['to2']; $ref1Memo =$to;
	if(_js::isse($D['actId'])){ $js = _ADMS::jsonError(self::$LG['err_actId_notFound']); }
	else if($movType == ''){ $js = _ADMS::jsonError(self::$LG['err_updField_fieldNotFound']); }
	else{
		$udp_ = false;
		$qOld = a_sql::fetch('SELECT userId,userAssg,actId,actStatus,actPriority,actType,archived FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$D['actId'].'\' LIMIT 1');
		$isOwner = ($qOld['userId'] == a_ses::$userId || $qOld['userAssg'] == a_ses::$userId);
		if(!$isOwner){ return _ADMS::jsonError(self::$LG['err_owner']); }
		$uset = '';
		switch($movType){
			case 'actStatus':{ $dateComp = date('Y-m-d H:i:s');
				$uset = ($to=='completed')
				? 'completed=\'Y\',completedAt=\''.$dateComp.'\',actStatus=\''.$to.'\' '
				: 'completed=\'N\',completedAt=\'0000-00-00\',actStatus=\''.$to.'\' ';
				
			}break;
			case 'actPriority':  $uset = 'actPriority=\''.$to.'\''; break;
			case 'actType':  $uset = 'actType=\''.$to.'\''; break;
			case 'archived': $uset = 'archived=\''.$to.'\''; break;
			case 'userAssg': $uset = 'userAssg=\''.$to.'\', userAssgName=\''.$to2.'\''; $__notiVerif = true;
			$ref1Memo = $to2;
			break;
		}
		if($qOld[$movType] != $to){//its different
			$qUpd = a_sql::query('UPDATE '._ADMS::$TBSoc['ap1_oact'].' SET '.$uset.' WHERE actId=\''.$D['actId'].'\' LIMIT 1');
			if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$qUpd['error_sql']); }
			else{
				_ADMS::_lb('com/_6');
				//guardar cambio realizada y notificar si es necesario
				$P = array('o'=>'no', 'objType'=>_o::$Ty['activity'],'objRef'=>$D['actId'], 'ref1Type'=>'fieldName','ref1'=>$movType,'ref1Memo'=>$ref1Memo);
				$jsResp = array('text'=>'Campo actualizado correctamente.');
				$P['__notiVerif'] = $__notiVerif;
				$js = _6::ouFn($P,$jsResp);
			}
		}
		else{ $js = _ADMS::jsonResp('Ningun dato actualizado.'); }
	}
	return $js;
}

function S_moveCard($D=array()){
	$userId = a_ses::$userId;
	$movType = $D['movType']; $from = $D['from']; $to = $D['to'];
	if(_js::isse($D['actId'])){ $js = _ADMS::jsonError(self::$LG['err_actId_notFound']); }
	else if($movType == ''){ $js = _ADMS::jsonError(self::$LG['err_moveType_notFound']); }
	else if($from == '' || $to == ''){ $js = _ADMS::jsonError(self::$LG['err_moveType_fromTo_fieldNotFound']); }
	else{
		$qOld = a_sql::fetch('SELECT userId,userAssg,actId,actStatus,actPriority,actType,archived FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$D['actId'].'\' LIMIT 1');
		$isOwner = ($qOld['userId'] == a_ses::$userId || $qOld['userAssg'] == a_ses::$userId);
		if(!$isOwner){ return _ADMS::jsonError(self::$LG['err_owner']); }
		$uset = '';
		$dateComp = date('Y-m-d H:i:s');
		$qUpd = '';
		$ref1 = $movType; $ref1Memo = $to;
		switch($movType){
			case 'wboList':{
				$qUpd = 'UPDATE '._ADMS::$TBSoc['ap1_wbo10'].' SET wboListId=\''.$to.'\' WHERE actId=\''.$D['actId'].'\' AND wboListId=\''.$from.'\' LIMIT 1';
			}break;
			case 'actPriority':{
				$qUpd = 'UPDATE '._ADMS::$TBSoc['ap1_oact'].' SET actPriority=\''.$to.'\' WHERE actId=\''.$D['actId'].'\' LIMIT 1';
			}break;
			case 'actType':{
				$qUpd = 'UPDATE '._ADMS::$TBSoc['ap1_oact'].' SET actType=\''.$to.'\' WHERE actId=\''.$D['actId'].'\' LIMIT 1';
			}break;
			case 'actStatus':{
				$dateComp = date('Y-m-d H:i:s');
				$setCom = ($to=='completed') ? ',completed=\'Y\',completedAt=\''.$dateComp.'\' ' : ',completed=\'N\',completedAt=\'0000-00-00 00:00:00\'';
				$qUpd = 'UPDATE '._ADMS::$TBSoc['ap1_oact'].' SET actStatus=\''.$to.'\' '.$setCom.' WHERE actId=\''.$D['actId'].'\' LIMIT 1';
			}break;
			case 'doDate':{ _ADMS::_lb('com/_2d');
				$doDate = $to.' '.substr($qOld['doDate'],-8);
				$endDateq = '';
				if(!_2d::is0($qOld['endDate'])){
					$endTime = strtotime($qOld['endDate']);
					$add1Day = strtotime($doDate)+($endTime-strtotime($qOld['doDate']));
					$endDate = date('Y-m-d H:i',$add1Day);
					$endDateq = ',endDate=\''.$endDate.'\' ';
					$keyO_mx = '"keyO_mx":{"endDate":"'.substr($endDate,0,10).'"}';
					//Mas datos a actualizar en la matri
				} 
				$qUpd = 'UPDATE '._ADMS::$TBSoc['ap1_oact'].' SET doDate=\''.$doDate.'\' '.$endDateq.' WHERE actId=\''.$D['actId'].'\' LIMIT 1';
			}break;
		}
		if($qOld[$movType] != $to){
			if($qUpd == ''){ return _ADMS::jsonError(self::$LG['err_moveType_notAllow']['errNo'],self::$LG['err_moveType_notAllow']['text'].$movType); }
			$qUpd = a_sql::query($qUpd);
			if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$qUpd['error_sql']); }
			else{ _ADMS::_lb('com/_6');
				$P = array('o'=>'no','objType'=>'activity','objRef'=>$D['actId'],
				'ref1Type'=>'fieldName','ref1'=>$ref1,'ref1Memo'=>$ref1Memo);
				$jsResp = array('text'=>'Campo actualizado correctamente.','add'=>$keyO_mx);
					$js = _6::ouFn($P,$jsResp);
			}
		}
		else{ $js = _ADMS::jsonResp('Ningun dato actualizado.'); }
	}
	return $js;
}

function S2_Copy($D=array()){
	$actId = $D['actId'];
	$copyCk = ($D['checkList'] == 'Y') ? true : false;
	if($actId == ''){ $js = _ADMS::jsonError(5,'actId no encontrado.'); }
	else if($D['actAsunt'] == ''){ $js = _ADMS::jsonError(3,'El asunto debe estar definido.'); }
	else{
		$fCopy = 'actType,actPriority, actPlace,doDate,endDate, timeDuration,timeDuraText,userAssg,userAssgName,actNote';
		if($copyCk){ $fCopy .= ',ckTotal'; }
		$Di = a_sql::fetch('SELECT '.$fCopy.' FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$actId.'\' LIMIT 1');
		if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$Di['error_sql']); }
		else if(a_sql::$errNo == 2){ $js = _ADMS::jsonError(5,'La actividad a duplicar no existe.'); }
		else{
			unset($Di['actId']);
			$Di['actAsunt'] = $D['actAsunt'];
			$Di['dueDate'] = $D['dueDate'];
			if($D['userAssg'] == 'N'){ unset($Di['userAssg'],$Di['userAssgName']); }
			$ins = a_sql::insert($Di,array('POST'=>true,'u_dateC'=>1,'table'=>_ADMS::$TBSoc['ap1_oact']));
			if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$ins['error_sql']); }
			else{
				$newActId = $ins['insertId'];
				if(!_js::isse($D['wboListId'])){
					$D['targetType'] = 'wboList';
					$D['targetRef'] = $D['wboListId'];
					$addText = ' y puesta en tablero.';
					_5b::add2List(array('wboListId'=>$D['wboListId'],'objType'=>_o::$Ty['activity'],'objRef'=>$newActId));
				}
				if($copyCk){ _ADMS::_lb('com/_5a2');
					$js0 = _5a2::S2_copy(array('old_targetRef'=>$actId,'targetType'=>_o::$Ty['activity'],'targetRef'=>$newActId));
				}
				$P['objType'] = _o::$Ty['activity'];
				$P['objRef'] = $newActId;
				$P['targetType'] = $D['targetType'];
				$P['targetRef'] = $D['targetRef'];
				$P['lineMemo'] = $D['actAsunt'];
				$P['__notiVerif'] = true;
				_ADMS::_lb('com/_6');
				$jsResp = array('text'=>'Copiada'.$addText);
				$js = _6::ouFn($P,$jsResp);
			}
		}
	}
	return $js;
}

function O_float($actId=0){
	$T = a_sql::fetch('SELECT * FROM '._ADMS::$TBSoc['ap1_oact'].' WHERE actId=\''.$actId.'\' LIMIT 1');
	$js = '';
	if(a_sql::$errNo == 1){ $js = _ADMS::jsonError(1,$T['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _ADMS::jsonError(2,'No se encontró la actividad ('.$actId.').');}
	else{
		$T['timeDuration'] = substr($T['timeDuration'],0,5);
		$T['doDate'] = substr($T['doDate'],0,16);
		$T['endDate'] = substr($T['endDate'],0,16);
		$js = '{"DATA":'.a_sql::JSON($T,'  ').'
}'; ;
	}
	return $js;
}

}
?>