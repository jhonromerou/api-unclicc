<?php
function setPeriodos($D=array()){
	//$fIni=new DateTime($D['dateEjecu']);
	//$fEnd=new DateTime($D['datePagoAprox']);
	//$fcc=new DateTime($D['dateCcobro']);
	//$diffe=$fIni->diff($fEnd);
	//$meses=($diffe->y*12) + $diffe->m; //meses
	$meses=(strtotime($D['datePagoAprox'])-strtotime($D['dateEjecu']))/86400;
	$meses=ceil($meses/30.5);
	//fecha cuenta cobro, si es mayor a 6, omitir intereses de periodo 7 al periodo de cuenta
	//$diffeCc=$fIni->diff($fcc);
	//$difCc=($diffeCc->y*12) + $diffeCc->m;//1 mas por si acaso
	$difCc=(strtotime($D['dateCcobro'])-strtotime($D['dateEjecu']))/86400;
	$difCc=ceil($difCc/30.5);
	$nn=1; 
	$D['meses']=$meses;
	$D['L2']=array();
	#/* //Obtener tasas del rango
	$per1=str_replace('-','',substr($D['dateEjecu'],0,7));
	$perE=str_replace('-','',substr(date('Y-m-d',strtotime($D['dateEjecu'].' +'.$meses.' months')),0,7));
	$Tax=array();
	$q=a_sql::query('SELECT iType, periodo,nmv FROM ifi_tax1 WHERE periodo>=\''.$per1.'\' AND periodo <=\''.$perE.'\' ',array(1=>'Error obteniendo tasas de interes para los periodos entre '.$per1.' y '.$perE.': ',2=>'No se encontraron tasas de interes para los periodos entre '.$per1.' y '.$perE));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return _err::$errText; }
	while($L=$q->fetch_assoc()){ $Tax[$L['iType'].$L['periodo']]=$L['nmv']; }
	#*/
	//calcular meses
	$interesAt=0; $hoy=date('Ym'); $yaHoy=0;
	$CPACA=($D['tipoInt']=='CPACA');
	$xMes=($CPACA)?3:6;
	$iUsura=0; $iDtf=0;  
	while($nn<=$meses){
		$tdate=date('Y-m-d',strtotime($D['dateEjecu'].' +'.$nn.' months'));;
		$period=str_replace('-','',substr($tdate,0,7));
		$isProy=($yaHoy==1)?'Y':'N';
		$interesLine=0;//asignar 0
		$iType=($CPACA && $nn<=10)?'DTF':'U';
		if($difCc>$xMes && $nn>$xMes && $nn<=$difCc){ }
		else{
			//CPACA solo despues <10 mes
			if($CPACA && $nn<=10){
				if($yaHoy==0 && !array_key_exists('DTF'.$period,$Tax)){
					_err::err('El periodo '.$period.': No tiene definida tasa de usura. ('.$period.' h: '.$hoy.')',3);
					break;
				}
				$iDtf=($yaHoy==1)?$iDtf:$Tax['DTF'.$period];
				$iNmv=$iDtf;
				$interesLine= $interesLine+$D['valorSentencia']*$iDtf;
			}
			else if(!$CPACA || ($CPACA && $nn>10)){
				if($yaHoy==0 && !array_key_exists('U'.$period,$Tax)){
					_err::err('El periodo '.$period.': No tiene definida tasa de usura. ('.$period.' h: '.$hoy.')',3);
					break;
				}
				$iUsura=($yaHoy==1)?$iUsura:$Tax['U'.$period];
				$iNmv=$iUsura;
				$interesLine=$D['valorSentencia']*$iUsura;
			}
			$interesAt+=$interesLine;
		}
		//Asginar valor a la fecha
		if($period>=$hoy && $yaHoy==0){ $yaHoy=1;
			$D['intAtDate']=$interesAt;
			$D['balAtDate']=$D['valorSentencia']+$D['intAtDate'];
			$D['balDate']=$tdate; 
		}
		$D['L2'][]=array('i','dsp_dse2','docEntry'=>$D['docEntry'],'numPer'=>$nn,'periodo'=>$period,'iType'=>$iType,'lineDate'=>$tdate,'iNmv'=>$iNmv,
		'interesLine'=>$interesLine,'interesAt'=>$interesAt,
		'isProy'=>$isProy
		);
		$nn++;
	}
	$D['intTotal']=$interesAt;
	$D['balTotal']=$D['valorSentencia']+$D['intTotal'];;
	return $D;
}

function setPeriodosDia($D=array()){
	$timeEje=strtotime($D['dateEjecu']);
	$meses=(strtotime($D['datePagoAprox'])-$timeEje)/86400;
	$meses=ceil($meses/30.5);
	//fecha cuenta cobro, si es mayor a 6, omitir intereses de periodo 7 al periodo de cuenta
	//$diffeCc=$fIni->diff($fcc);
	//$difCc=($diffeCc->y*12) + $diffeCc->m;//1 mas por si acaso
	$difCc=(strtotime($D['dateCcobro'])-strtotime($D['dateEjecu']))/86400;
	$difCc=ceil($difCc/30.5);
	$nn=0; 
	$D['meses']=$meses;
	$D['L2']=array();
	#/* //Obtener tasas del rango
	$per1=str_replace('-','',substr($D['dateEjecu'],0,7));
	$perE=str_replace('-','',substr(date('Y-m-d',strtotime($D['dateEjecu'].' +'.$meses.' months')),0,7));
	$Tax=array();
	$q=a_sql::query('SELECT iType, periodo,ea,nmv FROM ifi_tax1 WHERE periodo>=\''.$per1.'\' AND periodo <=\''.$perE.'\' ',array(1=>'Error obteniendo tasas de interes para los periodos entre '.$per1.' y '.$perE.': ',2=>'No se encontraron tasas de interes para los periodos entre '.$per1.' y '.$perE));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return _err::$errText; }
	while($L=$q->fetch_assoc()){
		$ndv=pow(1+$L['ea']/100,0.00273972602739726)-1;// sale de 1/12/30.5 mas o menos
		$Tax[$L['iType'].$L['periodo']]=array('ea'=>$L['ea'],'nmv'=>$L['nmv'],'ndv'=>$ndv);
	}
	function getTax($k,$M){
		if($M[$k]){ return $M[$k]; } else{ return array(); }
	}
	$interesAt=0; $hoy=date('Ym'); $yaHoy=0;
	$CPACA=($D['tipoInt']=='CPACA');
	$xMes=($CPACA)?3:6;
	//plazo maximo cuenta cobro
	$maxCc=date('Y-m-d',strtotime($D['dateEjecu'].' +'.$xMes.' months'));
	$iUsura=0; $iNmv=0; $iNdv=0;
	$tdate=substr($D['dateEjecu'],0,7);
	$iNoPaga=false;//no pagar interes
	while($nn<=$meses){
		$diasCobro=0;
		if($nn==0){
			$dias=date("d",mktime(0,0,0,(substr($D['dateEjecu'],5,2)*1)+1,0,substr($D['dateEjecu'],0,4)));
			$tp2=substr($D['dateEjecu'],0,7).'-'.$dias;
			$dias=$dias-substr($D['dateEjecu'],8,2)*1;
			$tp1=$D['dateEjecu'];
		}
		else{
			$tdate=date('Y-m',strtotime($D['dateEjecu'].' +'.$nn.' months'));
			$dias=date("d",mktime(0,0,0,(substr($tdate,5,2)*1)+1,0,substr($tdate,0,4)));
			$tp1=$tdate.'-01';
			$tp2=$tdate.'-'.$dias;
		}
		$diasCobro=$dias; $lineText='';
		if($difCc>$xMes){//si cuenta cobro no se presento en los dias que es
			$txt= substr($tdate,5,2).' > '.$tp1.' a '.$tp2.' ('.$dias.') --> Max: '.$maxCc.', cc: '.$D['dateCcobro'];
			if($maxCc>$tp1 && $maxCc<$tp2){
				$diasCobro=substr($maxCc,-2)*1;
				$txt= "	cobrar solo ($diasCobro) dias: ".$txt."\n";
			}
			else if($tp1>$maxCc && $tp2<=$D['dateCcobro']){
				$diasCobro=0; $lineText='Cuenta Cobro No Presentada';
				$txt= "		no cobrar ($dias) dias: ".$txt."\n";
			}
			else if($D['dateCcobro']>=$tp1 && $D['dateCcobro']<$tp2){
				$diasCobro=($dias-substr($D['dateCcobro'],-2)*1);
				$txt=  "	cobrar solo ($diasCobro) dias: ".$txt."\n";
			}
			else{ $txt= "cobrar todos ($diasCobro) dias: ".$txt."\n";; }
			//echo $txt;
		}
		if($tp2>=$D['datePagoAprox']){
			$diasCobro=(substr($D['datePagoAprox'],-2)*1)-1;
			$tp2=$D['datePagoAprox'];
			$lineText=$D['datePagoAprox'].' Posible Pago';
		}
		if($maxCc>=$tp1 && $maxCc<=$tp2){
			if($difCc>$xMes){ $tp2=$maxCc; }
			$lineText=$maxCc.' Plazo Cuenta Cobro';
		}
		if($D['dateCcobro']>=$tp1 && $D['dateCcobro']<=$tp2){
			if($difCc>$xMes){ $tp1=date('Y-m-d',strtotime($D['dateCcobro'].' +1 days')); }
			$lineText=$D['dateCcobro'].' Cuenta Cobro';
		}
		$dias = $diasCobro.', de '.$tp1.' a '.$tp2;
		$period=str_replace('-','',$tdate);//obtener periodo de la tasa
		$isProy=($yaHoy==1)?'Y':'N';
		$interesLine=0;//asignar 0
		$iType=($CPACA && $nn<=10)?'DTF':'U';
		{
			//CPACA solo despues <10 mes
			if($CPACA && $nn<=10){
				if($yaHoy==0 && !array_key_exists('DTF'.$period,$Tax)){
					_err::err('El periodo '.$period.': No tiene definida tasa de usura. ('.$period.' h: '.$hoy.')',3);
					break;
				}
				$itx=getTax('DTF'.$period,$Tax);
				$iNmv=($yaHoy==1)?$iNmv:$itx['nmv'];;
				$iNdv=($yaHoy==1)?$iNdv:$itx['ndv'];;
				$interesLine= $D['valorSentencia']*$diasCobro*$iNdv;//pijur mal
				//$interesLine= $D['valorSentencia']*$iNmv; /
			}
			else if(!$CPACA || ($CPACA && $nn>10)){
				if($yaHoy==0 && !array_key_exists('U'.$period,$Tax)){
					_err::err('El periodo '.$period.': No tiene definida tasa de usura. ('.$period.' h: '.$hoy.')',3);
					break;
				}
				$itx=getTax('U'.$period,$Tax);
				$iNmv=($yaHoy==1)?$iNmv:$itx['nmv'];;
				$iNdv=($yaHoy==1)?$iNdv:$itx['ndv'];;
				$interesLine=($D['valorSentencia']*$diasCobro*$iNdv);
			}
			$interesAt+=$interesLine;
		}
		//Asginar valor a la fecha
		if($period>=$hoy && $yaHoy==0){ $yaHoy=1;
			$D['intAtDate']=$interesAt;
			$D['balAtDate']=$D['valorSentencia']+$D['intAtDate'];
			$D['balDate']=$tp2; 
		}
		$D['L2'][]=array('i','dsp_dse2','docEntry'=>$D['docEntry'],'numPer'=>$nn,'periodo'=>$period,'iType'=>$iType,'lineDate'=>$tdate,'iNmv'=>$iNmv,'iNdv'=>$iNdv,
		'interesLine'=>$interesLine,'interesAt'=>$interesAt,
		'isProy'=>$isProy,
		'per1'=>$tp1,'per2'=>$tp2,'dias'=>$diasCobro,'lineText'=>$lineText
		//,'dias'=>$diasCobro,'iNmd'=>$iNmd
		);
		$nn++;
	}
	$D['intTotal']=$interesAt;
	$D['balTotal']=$D['valorSentencia']+$D['intTotal'];
	return $D;
}

?>