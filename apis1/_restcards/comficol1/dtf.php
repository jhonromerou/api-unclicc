<?php
if(_0s::$router=='GET dtf'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::_lb('sql/filter');
	echo a_sql::queryL('SELECT A.* FROM ifi_tax1 A WHERE 1 '.a_sql_filtByT($_GET['wh']).' ORDER BY A.periodo DESC');
}
else if(_0s::$router=='PUT dtf'){
	if(!isset($_J['L']) || count($_J['L'])==0){ die(_js::e(3,'No hay lineas a modificar.')); }
	else{
		$nl=1;
		a_sql::transaction(); $cmt=false;
		foreach($_J['L'] as $n=>$X){
			$ln='Linea '.$nl.': '; $nl++;
			if(_js::iseErr($X['iType'],$ln.'Se debe definir el tipo de tasa.')){ break; }
			else if(_js::iseErr($X['periodo'],$ln.'Se debe definir un periodo.')){ break; }
			else if(_js::iseErr($X['ea'],$ln.'Se debe definir la tasa efectiva anual.','numeric>0')){ break; }
			else if(_js::iseErr($X['nmv'],$ln.'Se debe definir la tasa nominal mensual, utilice [(1+ea)^(1/12)] -1','numeric>0')){ break; }
			else{
				$_J['L'][$n][0]='i';
				$_J['L'][$n][1]='ifi_tax1';
				$_J['L'][$n]['_unik']='id';
			}
		}
		if(_err::$err){ $js=_err::$errText; }
		else{
			a_sql::multiQuery($_J['L']);
			if(_err::$err){ $js=_err::$errText; }
			else{ $cmt=true; $js=_js::r('Datos actualizados correctamente.'); }
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}

else if(_0s::$router=='PUT dse3'){
	if(_js::iseErr($_J['docEntry'],'Se debe definir Id de documento','numeric>0')){ die(_err::$errText); }
	revDoc($_J);
	if(_err::$err){ $js=_err::$errText; }
	$Mx=setPeriodos($_J,$_J['L']);
	print_r($Mx);
	echo $js;
}

?>