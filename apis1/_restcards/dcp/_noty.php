<?php
/* Se definen en DB
	- definex (3):
		days=1: 0 4 * * 2-5: a las 4am de l-v
		days=3: 2 4 * * 2-5: a las 4:02am de l-v
		days=8: 4 4 * * 1: a las 4:04am los lunes
	- atrasado (1): -2week a yesterday
		: 10 4 * * 1: a las 4:10am los lunes
	- noPagado (1): 
	: 10 4 * * 1: a las 4:10 los lunes
	
	Se ejecuta el envio
	definex: x/5 5 * * 1-5: cada 5min a las 5am de l-v
	atrasado: x/5 8 * * 1-5: cada 5min a las 8am de l-v
	noPagado: x/5 8 * * 1-5: cada 5min a las 8am de l-v
*/
$D = $_POST; $oTy='card';
$today = date('Y-m-d');
_ADMS::lib('sql/filter');
require_once('dian_dv.php');


function mailTem_Pdf($D2=array()){
	$thCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$tdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
	<h4>Estimado '.$D2['cardName'].'</h4>
<p>Un error en nuestro sistema generó inconsistencias en el calendario tributario 2019 que le enviamos con anterioridad. Pedimos disculpas por este error.</p>
<p>Encontrará adjunto un PDF con la información de sus vencimientos.</p>';
	$html .= '
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>
	<br/><br/><br/><br/>
	<div style="font-size:0.75rem;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}

function mailTem_week($D2=array()){
	$thCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$tdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
	<h4>Estimado '.$D2['cardName'].'</h4>
<p>Estos son sus vencimientos para esta semana:</p>
	<table>
	<tr><td '.$thCss.'>Fecha</td> <td '.$thCss.'>Grupo</td> <td '.$thCss.'>Descripción Vencimiento</td>
	</tr>';
	$Re=json_decode($D2['respJs'],1);
	foreach($Re as $nr=>$Lr){
		$html .='<tr>
		<td '.$tdCss.'>'.$Lr['dueDate'].'</td>
		<td '.$tdCss.'>'.$Lr['grName'].'</td>
		<td '.$tdCss.'>'.$Lr['respName'].'</td>
		</tr>';
	}
	$html .= '</table>
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>
	<br/><br/><br/><br/>
	<div style="font-size:0.75rem;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}

function mailTem_daily($D2=array()){
	$thCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$tdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
	<h4>Estimado '.$D2['cardName'].'</h4>
<p>Hoy se vencen:</p>
	<div>Si este ya fue realizado, por favor omita este mensaje.</div>
	<table>
	<tr><td '.$thCss.'>Fecha</td> <td '.$thCss.'>Grupo</td> <td '.$thCss.'>Descripción Vencimiento</td>
	</tr>';
	$Re=json_decode($D2['respJs'],1);
	foreach($Re as $nr=>$Lr){
		$html .='<tr>
		<td '.$tdCss.'>'.$Lr['dueDate'].'</td>
		<td '.$tdCss.'>'.$Lr['grName'].'</td>
		<td '.$tdCss.'>'.$Lr['respName'].'</td>
		</tr>';
	}
	$html .= '</table>
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>
	<br/><br/><br/><br/>
	<div style="font-size:11px;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}

function mailTem_atrasado($D2=array()){
	$thCss = 'style="background-color:#EEE; padding:4px;"';
	$tdCss = 'style="padding:4px;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
<h4>Hola '.$D2['slpName'].'</h4>
<p>Tienes vencimientos pendientes de revisar</p>
<table>
<tr><td '.$thCss.'>Fecha</td> <td '.$thCss.'>Grupo</td> <td '.$thCss.'>Descripción Vencimiento</td>
</tr>';
	foreach($D2['C'] as $k=>$L){
		$html .='<tr><td colspan="3" '.$thCss.'>'.$L['cardName'].'</td></tr>'."\n";
		$Re=json_decode($L['respJs'],1);
		foreach($Re as $nr=>$Lr){
			$html .='<tr>
<td '.$tdCss.'>'.$Lr['dueDate'].'</td>
<td '.$tdCss.'>'.$Lr['grName'].'</td>
<td '.$tdCss.'>'.$Lr['respName'].'</td>
</tr>';
		}
	}
	$html .= '</table>
	<br/><br/>
	<div>Recuerde marcar sus vencimientos en el sistema para evitar estos correos.</div>
	<br/><br/><br/><br/>
	<div style="font-size:11px;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}

function mailTem_noPagado($D2=array()){
	$thCss = 'style="background-color:#EEE; padding:4px;"';
	$tdCss = 'style="padding:4px;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
<h4>Señor/es '.$D2['cardName'].'</h4>
<p>Tiene responsabilidades pendientes por definir con su responsable '.$D2['slpName'].'.</p>
<table>
<tr><td '.$thCss.'>Fecha</td> <td '.$thCss.'>Grupo</td> <td '.$thCss.'>Descripción Vencimiento</td>
</tr>';
	$Re=json_decode($D2['respJs'],1);
	foreach($Re as $nr=>$Lr){
		$html .='<tr>
<td '.$tdCss.'>'.$Lr['dueDate'].'</td>
<td '.$tdCss.'>'.$Lr['grName'].'</td>
<td '.$tdCss.'>'.$Lr['respName'].'</td>
</tr>';
		}
	$html .= '</table>
	<br/><br/>
	<div>Si tiene dudas al respecto de está información comuniquese con su responsable</div>
	<br/><br/><br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>';
	return $html;
}

if(_0s::$router=='GET _noty/notiSend'){
	$date1=date('Y-m-d');
	$limitSend=1;
	header('Content-Type: text/html');
	$q=a_sql::query('SELECT N.*, S.slpName,S.slpEmail FROM '._0s::$Tb['__pTriNty'].' N LEFT JOIN '._0s::$Tb['par_oslp'].' S ON (S.slpId=N.slpId) WHERE N.nStatus=\'O\' AND N.dateC>=\''.$date1.' 00:00:00\' LIMIT '.$limitSend);
	$Se=array();
	$ids='';
	_ADMS::lib('_Mailer');
	while($L=$q->fetch_assoc()){
		$L['mailTo']='';
		$q2=a_sql::query('SELECT email FROM '._0s::$Tb['par_ocpr'].' WHERE cardId=\''.$L['cardId'].'\' AND position IN (\'gerente\',\'contador\') LIMIT 2',array(1=>'Error obteniendo contactos para '.$L['cardName']));
		if(a_sql::$err){ /* log error*/ }
		else if(a_sql::$errNo=='-1'){
			while($L2=$q2->fetch_assoc()){
				$L['mailTo'] .= $L2['email'].',';
			}
			$L['mailTo']=substr($L['mailTo'],0,-1);
		}
		$D['asunt'] = 'Proximos Vencimientos';
		$D['mailTo']='jromdev@gmail.com';
		$D['body'] = mailTem_user(json_decode($L['respJs'],1),$L);
		$D['fromName'] = 'DYD Consultores y Asesores S.A.S.';
		$D['fromEmail'] = 'dydplatform@admsystems.co';
		$D['replyTo'] = $D['userAssgName'].' <'.$D['userEmail'].'>';
		$s = _Mail::send($D);
		if($s['errNo']){ echo _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
		else{ $ids .= $L['id'].','; }
	}
	$up=a_sql::query('UPDATE '._0s::$Tb['__pTriNty'].' SET nStatus=\'C\' WHERE id IN ('.substr($ids,0,-1).') LIMIT 100');
}
else if(_0s::$router=='GET _noty/sendPdf'){
	$date1=date('Y-m-d');
	$limitSend=20; ##
	$q=a_sql::query('SELECT N.*, S.slpName,S.slpEmail FROM '._0s::$Tb['__pTriNty'].' N LEFT JOIN '._0s::$Tb['par_oslp'].' S ON (S.slpId=N.slpId) WHERE N.nStatus=\'O\' AND N.dateC>=\''.$date1.' 00:00:00\' LIMIT '.$limitSend);
	$Se=array();
	$ids='';
	//die(_js::e(3,'Pendiente de envio '.$q->num_rows.' de '.$limitSend));
	_ADMS::lib('_2d,_Mailer');
	$pdf_D=array(
	'pagTotal'=>'Y',
	'head_h1'=>'Calendario Tributario Año 2019',
	'head_desc'=>'DyD Consultores y Asesores SAS y DCP Revisores, le informa.',
	'foot_desc'=>'*El (número) hace referencia al día de vencimiento.',
	'imgWater'=>'http://static1.admsistems.com/_logos/logodyd-opa.png'
	);
	$n=0;
	while($L=$q->fetch_assoc()){
		$R=array(); #reset cada linea
		$Re=json_decode($L['respJs'],1);
		foreach($Re as $nr=>$Lr){
			$mx=_2d::_s($Lr['dueDate'],array('m'=>'Y'));
			$kn=$mx[2].'_'.$mx[1];
			if(!array_key_exists($kn,$R)){
				$R[$kn]=array('t'=>$mx['m'],'L'=>array());
			}
			$R[$kn]['L'][0] .= '('.substr($Lr['dueDate'],-2).') '.$Lr['grName']."\n";
		}
		ksort($R);
		$pdf=new myPdf();
		$pdf->D=$pdf_D;
		$pdf->AddPage('L','Letter',0);
		$pdf->SetFont('Arial','',10);
		$pdf->Write(0,'Estimado '.$L['cardName'].',',1);
		$pdf->Write(0,'Estos son sus próximos vencimientos.',1);
		$pdf->SetFont('Arial','',7);
		$pdf->meTable(array('cols'=>4,'L'=>$R,'chartCol'=>'d'));
		$fileName='/var/www/html/apis1/dinamicFile/calendariotributario2019-'.$L['cardId'].'.pdf';
		$pdf->Output('F',$fileName);
		//$pdf->Output();
		$D['asunt'] = 'Proximos Vencimientos';
		$D['mailTo']=$L['mailTo'];
		//$D['mailTo']='jromdev@gmail.com';
		$D['fromName'] = 'DYD Consultores y Asesores S.A.S.';
		$D['fromEmail'] = 'info@dcprevisores.com';
		$D['replyTo'] = $D['userAssgName'].' <'.$D['userEmail'].'>';
		$D['body'] = mailTem_Pdf($L);
		$s = _Mail::send($D,array('F'=>array($fileName)));
		if($s['errNo']){ echo _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
		else{ $ids .= $L['id'].','; }
	}
	$up=a_sql::query('UPDATE '._0s::$Tb['__pTriNty'].' SET nStatus=\'C\',notiDate=\''.date('Y-m-d H:i:s').'\' WHERE id IN ('.substr($ids,0,-1).') LIMIT 100');
}

else if(_0s::$router=='GET _noty/definex'){
	$days=($_GET['days'] && $_GET['days']>0 && $_GET['days']<=15)?$_GET['days']-1:8;
	$respStatus=($_GET['respStatus'])?$_GET['respStatus']:'pendiente';
	$_7=86400*$days;
	$date1=date('Y-m-d'); //domingo
	$date2=date('Y-m-d',strtotime($date1)+$_7);
	a_sql::query('DELETE FROM __pTriNty WHERE dateC <\''.$date1.'\' AND emailTemp=\'definex\'');
	echo ($date1.' a '.$date2);
	$q=pTri::due_qu(array('respStatus'=>$respStatus,'date1'=>$date1,'date2'=>$date2,'fie'=>'C.slpId','ordBy'=>'gD.dueDate ASC' ,'whs'=>''));
	if(pTri::$err){ die(pTri::$errText); }
	$S=array(); $n=-1; $Sn=array();
	while($L=$q->fetch_assoc()){
		$k=$L['cardId'];
		echo '
'.implode("	",$L);
		if(!array_key_exists($k,$Sn)){ $n++;
			$S[$n]=array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'slpId'=>$L['slpId'],'respJs'=>array());
			$Sn[$k]=$n;
			/* get contacts */
			$S[$n]['mailTo']='';
			$q2=a_sql::query('SELECT email FROM par_ocpr WHERE cardId=\''.$L['cardId'].'\' AND position IN (\'gerente\',\'contador\') LIMIT 2',array(1=>'Error obteniendo contactos para '.$L['cardName']));
			if(a_sql::$err){ /* log error*/ }
			else if(a_sql::$errNo=='-1'){
				while($L2=$q2->fetch_assoc()){
					if(preg_match('/.*\@.*\..*/',$L2['email'])){
						$S[$n]['mailTo'] .= $L2['email'].',';
					}
				}
				$S[$n]['mailTo']=substr($S[$n]['mailTo'],0,-1);
			}
		}
		$nk=$Sn[$k];
		unset($L['cardId'],$L['cardName']);
		$S[$nk]['respJs'][]=$L;
	}
	unset($Sn);
	foreach($S as $k =>$L){
		if($L['mailTo']==''){ $L['nStatus']='noEmail'; }
		$L['respJs']=json_encode($L['respJs']);
		$L['notyType']='definex';
		$L['emailTemp']='definex';
		a_sql::insert($L,array('table'=>'__pTriNty','qDo'=>'insert','kui'=>'dateC'));
	}

}
else if(_0s::$router=='GET _noty/send/venci'){
	$date1=date('Y-m-d');
	$limitSend=20;
	$q=a_sql::query('SELECT N.*, S.slpName,S.slpEmail FROM __pTriNty N LEFT JOIN par_oslp S ON (S.slpId=N.slpId) WHERE N.nStatus=\'O\' AND N.emailTemp=\'definex\' LIMIT '.$limitSend);
	$Se=array();
	$ids='';
	_ADMS::lib('_2d,_Mailer,xCurl');
	$n=0; $qtySends=0;
	while($L=$q->fetch_assoc()){
		$R=array();
		$D['subject'] = 'Vencimientos Próximos';
		$D['mailTo']=$L['mailTo'];//'johnromero492@gmail.com';// 
		$D['fromName'] = 'D&D Consultores y Asesores S.A.S.';
		$D['fromEmail'] = 'informacion@dcprevisores.com';
		$D['html'] = mailTem_week($L);
		$qtySends += count(explode('@',$D['mailTo']))-1;
		$s = _Mail::mailGun($D);
		if(is_array($s) && $s['errNo']){ echo _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
		else{ $ids .= $L['id'].','; }
	}
	$up=a_sql::query('UPDATE __pTriNty SET nStatus=\'C\',notiDate=\''.date('Y-m-d H:i:s').'\' WHERE id IN ('.substr($ids,0,-1).') LIMIT 100');
	if($qtySends>0){
		_ADMS::lib('pApp');
		$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'gTribMailDaily','qtySend'=>$qtySends);
		$s=pApp::emailQtySends($Dis,array('r'=>'txt'));
		print_r($s);
	}
}

else if(_0s::$router=='GET _noty/defineAtrasado'){
	$respStatus='pendiente';
	$hoy=date('Y-m-d');
	$date1=date('Y-m-d',strtotime('-2weeks'));
	$date2=date('Y-m-d',strtotime('yesterday'));
	a_sql::query('DELETE FROM __pTriNty WHERE dateC <\''.$hoy.'\' AND emailTemp=\'atrasado\'');
	echo ($date1.' a '.$date2);
	$q=pTri::due_qu(array('respStatus'=>$respStatus,'date1'=>$date1,'date2'=>$date2,'fie'=>'C.slpId','ordBy'=>'gD.dueDate ASC' ,'wh'=>a_sql_filtByT($_GET['wh'])));
	if(pTri::$err){ die(pTri::$errText); }
	$S=array(); $n=-1; $Sn=array();
	while($L=$q->fetch_assoc()){
		$k=$L['cardId'];
		echo '
'.implode("	",$L);
		if(!array_key_exists($k,$Sn)){ $n++;
			$S[$n]=array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'slpId'=>$L['slpId'],'respJs'=>array());
			$Sn[$k]=$n;
			if(a_sql::$err){ /* log error*/ }
		}
		$nk=$Sn[$k];
		unset($L['cardId'],$L['cardName']);
		$S[$nk]['respJs'][]=$L;
	}
	unset($Sn);
	foreach($S as $k =>$L){
		$L['respJs']=json_encode($L['respJs']);
		$L['notyType']='noMark';
		$L['emailTemp']='atrasado';
		a_sql::insert($L,array('table'=>'__pTriNty','qDo'=>'insert','kui'=>'dateC'));
	}

}
else if(_0s::$router=='GET _noty/send/atrasado'){
	$date1=date('Y-m-d');
	$limitSend=20;
	$q=a_sql::query('SELECT N.*, S.slpName,S.slpEmail FROM '._0s::$Tb['__pTriNty'].' N LEFT JOIN par_oslp S ON (S.slpId=N.slpId) WHERE N.nStatus=\'O\' AND N.emailTemp=\'atrasado\' LIMIT '.$limitSend,array(1=>'Error obteniendo notificaciones del dia',2=>'No hay notificaciones para enviar.'));
		$ids='';
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$RL=array();
	while($L=$q->fetch_assoc()){
		$k=$L['slpId'];
		if(!array_key_exists($k,$RL)){
			$RL[$k]=$L;
			unset($RL[$k]['respJs']);
		}
		$RL[$k]['C'][$L['cardId']]=array('cardName'=>$L['cardName'],'respJs'=>$L['respJs']);
		$ids .= $L['id'].',';
	}
	$Se=array();
	_ADMS::lib('_2d,_Mailer,xCurl');
	$n=0; $qtySends=0;
	foreach($RL as $nx=>$L){
		$R=array(); #reset cada linea
		$D['subject'] = 'Vencimientos por Revisar';
		$D['mailTo']= $L['slpEmail'];
		$D['fromName'] = 'D&D Consultores y Asesores S.A.S.';
		$D['fromEmail'] = 'informacion@dcprevisores.com';
		$D['html'] = mailTem_atrasado($L);
		$qtySends += 1;
		$s = _Mail::mailGun($D);
		if(is_array($s) && $s['errNo']){ echo _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
	}
	$up=a_sql::query('UPDATE __pTriNty SET nStatus=\'C\',notiDate=\''.date('Y-m-d H:i:s').'\' WHERE id IN ('.substr($ids,0,-1).') LIMIT 100');
	if($qtySends>0){
		_ADMS::lib('pApp');
		$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'gTribMailDaily','qtySend'=>$qtySends);
		$s=pApp::emailQtySends($Dis,array('r'=>'txt'));
		print_r($s);
	}
}

else if(_0s::$router=='GET _noty/noPagado/define'){
	$respStatus='problemaCliente';
	$hoy=date('Y-m-d');
	$date1=date('Y-m-d',strtotime('-3 months'));
	$date2=date('Y-m-d');
	a_sql::query('DELETE FROM __pTriNty WHERE dateC <\''.$hoy.'\' AND emailTemp=\'noPagado\' ');
	echo ($date1.' a '.$date2);
	$q=pTri::due_qu(array('respStatus'=>$respStatus,'date1'=>$date1,'date2'=>$date2,'fie'=>'C.slpId','ordBy'=>'gD.dueDate ASC' ,'wh'=>a_sql_filtByT($_GET['wh'])));
	if(pTri::$err){ die(pTri::$errText); }
	$S=array(); $n=-1; $Sn=array();
	while($L=$q->fetch_assoc()){
		$k=$L['cardId'];
		echo '
'.implode("	",$L);
		if(!array_key_exists($k,$Sn)){ $n++;
			$S[$n]=array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'slpId'=>$L['slpId'],'respJs'=>array());
			$Sn[$k]=$n;
			/* get contacts */
			$S[$n]['mailTo']='';
			$q2=a_sql::query('SELECT email FROM par_ocpr WHERE cardId=\''.$L['cardId'].'\' AND position IN (\'gerente\',\'contador\') LIMIT 2',array(1=>'Error obteniendo contactos para '.$L['cardName']));
			if(a_sql::$err){ /* log error*/ }
			else if(a_sql::$errNo=='-1'){
				while($L2=$q2->fetch_assoc()){
					if(preg_match('/.*\@.*\..*/',$L2['email'])){
						$S[$n]['mailTo'] .= $L2['email'].',';
					}
				}
				$S[$n]['mailTo']=substr($S[$n]['mailTo'],0,-1);
			}
		}
		$nk=$Sn[$k];
		unset($L['cardId'],$L['cardName']);
		$S[$nk]['respJs'][]=$L;
	}
	unset($Sn);
	foreach($S as $k =>$L){
		$L['respJs']=json_encode($L['respJs']);
		$L['notyType']='noMark';
		$L['emailTemp']='noPagado';
		a_sql::insert($L,array('table'=>'__pTriNty','qDo'=>'insert','kui'=>'dateC'));
	}

}
else if(_0s::$router=='GET _noty/noPagado/send'){
	$date1=date('Y-m-d');
	$limitSend=20;
	$q=a_sql::query('SELECT N.*, S.slpName,S.slpEmail FROM __pTriNty N LEFT JOIN par_oslp S ON (S.slpId=N.slpId) WHERE N.nStatus=\'O\' AND N.emailTemp=\'noPagado\' LIMIT '.$limitSend,array(1=>'Error obteniendo notificaciones con problema cliente',2=>'No hay notificaciones para enviar.'));
		$ids='';
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$RL=array();
	while($L=$q->fetch_assoc()){
		$RL[]=$L;
		$ids .= $L['id'].',';
	}
	$Se=array();
	_ADMS::lib('_2d,_Mailer,xCurl');
	$n=0; $qtySends=0;
	foreach($RL as $nx=>$L){
		$R=array(); #reset cada linea
		$D['subject'] = 'Vencimientos por Definir';
		$D['mailTo']= $L['mailTo'].','.$L['slpEmail'];
		$D['fromName'] = 'D&D Consultores y Asesores S.A.S.';
		$D['fromEmail'] = 'informacion@dcprevisores.com';
		$D['html'] = mailTem_noPagado($L);
		$qtySends += 1;
		$s = _Mail::mailGun($D);
		if(is_array($s) && $s['errNo']){ echo _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
	}
	$up=a_sql::query('UPDATE __pTriNty SET nStatus=\'C\',notiDate=\''.date('Y-m-d H:i:s').'\' WHERE id IN ('.substr($ids,0,-1).') LIMIT 100');
	if($qtySends>0){
		_ADMS::lib('pApp');
		$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'gTribMailDaily','qtySend'=>$qtySends);
		$s=pApp::emailQtySends($Dis,array('r'=>'txt'));
	}
}

?>
