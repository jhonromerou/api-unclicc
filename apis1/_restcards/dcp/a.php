<?php
_ADMS::_lb('com/_2d');
$oTy='dcpVis';
unset($___D['textSearch']);
$GLOBALS['logImg'] = ($ADMS_socKey == 's1') ? 'https://firebasestorage.googleapis.com/v0/b/teams-1.appspot.com/o/_cards%2Flogos%2Flogodyd.png?alt=media&token=0bb35814-d507-4f15-aec9-74c3a7f9fc7c' : 'http://static1.admsistems.com/_logos/logodyd.png';
if(_0s::$router=='GET forms/asesoria'){
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM a03_ofvc A';
	echo iDoc::getOne($_GET);
}
else if(_0s::$router=='PUT forms/asesoria'){
		unset($_J['objType']);
		$P = $_J;
		if($P['doDate']){ $P['docDate']= $P['doDate']; unset($P['doDate']); }
		$doDateText = $P['docDate'];
		$doTimeText = $P['doTime'];
		$endDateText = $P['endDate'];
		$endTimeText = $P['endTime'];
		$P['endDate'] = $P['docDate'].' '.$P['endTime'];
		$P['docDate'] .= ' '.$P['doTime'];
		$doDate = strtotime($P['docDate']);
		$endDate = strtotime($P['endDate']);
		$auTy = array();
		$docEntry = $_J['docEntry'];
		if(array_key_exists('docEntry',$_J) && _js::ise($_J['docEntry'])){ $js = _js::e(3,'No se ha seleccionado el Id del formulario.'); }
		else if(_js::ise($_J['cardId']) || _js::ise($_J['cardName'])){ $js = _js::e(3,'Debe seleccionar un cliente para poder guardar la información. '.$_J['cardId']); }
		else if($doTimeText == ''){ $js = _js::e(3,'No se ha definido la hora de entrada.'); }
		else if($endTimeText == ''){ $js = _js::e(3,'No se ha definido la hora de salida.'); }
		else if($doDate > $endDate){ $js = _js::e(3,'La hora de salida no puede ser menor a la de entrada.'); }
		else{
			unset($P['doTime'],$P['endTime']);
			//corregir con vendedor
			$custq = a_sql::fetch('SELECT slpId FROM '._0s::$Tb['par_ocrd'].' WHERE cardId=\''.$P['cardId'].'\' LIMIT 1 ');
			$P['slpId'] = $custq['slpId'];
			$P['timeDuration'] = _2d::relTime($doDate,$endDate,array('format'=>'hours'));
			$ins = a_sql::insert($P,array('kui'=>'uid_dateC','tbk'=>'a03_ofvc','wh_change'=>'WHERE docEntry=\''.$docEntry.'\' LIMIT 1 '));
			if($ins['err']){ $js = _js::e(2,'Error guardando formulario: '.$ins['text']); }
			else{
				$docEntry = ($ins['insertId']) ? $ins['insertId'] : $docEntry;
				$js = _js::r('Asesoria Actualizada correctamente','"docEntry":"'.$docEntry.'"'); 
			}
		}
		echo $js;
	}
else if(_0s::$router=='GET forms/asesorias'){
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM a03_ofvc A INNER JOIN par_ocrd C ON (C.cardId = A.cardId) ';
	echo iDoc::get($_GET,array('permsBy'=>'slps','errs'=>1));
}

else if(_0s::$router=='GET forms/asesorias.sendEmail'){
	$docEntry = $_GET['docEntry'];
	$F = a_sql::fetch('SELECT docEntry,cardId,cardName,userId,slpId FROM '._0s::$Tb['a03_ofvc'].' WHERE docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo información de formulario.',2=>'El formulario No '.$docEntry.' no existe.'));
	if(a_sql::$err){ $js = a_sql::$errNoText; }
	else{
		$quC = a_sql::query('SELECT emailType,email FROM '._0s::$Tb['par_crd13'].' WHERE cardId=\''.$F['cardId'].'\' ORDER BY lineNum ASC',array(1=>'Error obteniendo emails del cliente',2=>'El socio no tiene emails registrados'));
		if(a_sql::$err){ $F['L'] = json_encode(a_sql::$errNoText); }
		else{
			$F['L']=array();
			while($ema = $quC->fetch_assoc()){
				$F['L'][]=array('k'=>$ema['email'],'v'=>$ema['email']);
			}
		}
		$js = _js::enc($F,'just');
	}
	echo $js;
}
else if(_0s::$router=='POST forms/asesorias.sendEmail'){
	$docEntry = $___D['docEntry'];
	if(_js::ise($docEntry,'No se ha encontrado la Id del formulario, Intento de nuevo o informe al administrador de la plataforma.')){ }
	else if(_js::ise($___D['sendTo']) && _js::ise($___D['ccto'])){ $js = _js::e(3,'Se debe definir un correo de envio.'); }
	else{
		$F = a_sql::fetch('SELECT * FROM '._0s::$Tb['a03_ofvc'].' WHERE docEntry=\''.$docEntry.'\' '.$wh_userAssg.' LIMIT 1 ');
		if(a_sql::$errNo == 1){ $js = _js::e(1,$F['error_sql']); }
		else if(a_sql::$errNo == 2){ $js = _js::e(2,'El formulario No '.$docEntry.' no existe.'); }
		else{
			$U = a_ses::U_info($F['userId'],'userName,userEmail');
			$tdCssB = 'style="border:1px solid #CCC; padding:0.75em 0.4em; vertical-align:top;"';
			$HTML = '
			<div style="font-size:13px; font-style:normal; width:100%; min-width:800px; margin-top:20px;">
			<img src="'.$GLOBALS['logImg'].'" style="width:320px;" />
			<br/><b>Señores '.$D['cardName'].'</b>,
			<p><pre>'.$D['body'].'</pre></p>
			<table style=" width:100%; min-width:800px;">
			<tr><td colspan="2" '.$tdCssB.'>Fecha: '.$F['doDate'].'</td></tr>
			<tr><td '.$tdCssB.'>Entrada: '.substr($F['doDate'],11,5).'</td>
			<td '.$tdCssB.'>Salida: '.substr($F['endDate'],11,5).'</td>
			</tr>
			<tr><td colspan="2" '.$tdCssB.'>Realizado Por: '.$F['userName'].'</td></tr>
			<tr><td colspan="2" '.$tdCssB.'>Responsable: '.$F['userAssgName'].'</td></tr>
			<tr>
			<td colspan="2" '.$tdCssB.'>
			<b>1. Objetivos</b>
			<p><pre>'.$F['fiObjetivos'].'</pre></p>
			</td>
			</tr>
			<tr>
			<td colspan="2" '.$tdCssB.'>
			<b>2. Actividades Realizadas</b>
			<p><pre>'.$F['fiActividades'].'</pre></p>
			</td>
			</tr>
			<tr>
			<td '.$tdCssB.'>
			<b>3.1 Compromiso Empresa</b>
			<p><pre>'.$F['fiCompEmpresa'].'</pre></p>
			</td>
			<td '.$tdCssB.'>
			<b>3.1 Compromiso Asesor</b>
			<p><pre>'.$F['fiCompAsesor'].'</pre></p>
			</td>
			</tr>
			<tr>
			<td colspan="2" '.$tdCssB.'>
			<b>4. Conclusiones</b>
			<p><pre>'.$F['fiConclusion'].'</pre></p>
			</td>
			</tr>
			</table><br/>
<br/>
<a style="font-size:0.85rem;" href="http://www.admsistems.com">Report by ADM Sistems</a> '.date('Y').'
			</div>';
			
			_ADMS::_lb('lb/_Mailer');
			$___D['asunt'] = 'Hoja de Visita';
			$___D['body'] = $HTML;
			$___D['fromName'] = $U['userName'];
			$___D['fromEmail'] = $U['userEmail'];
			$___D['replyTo'] = $U['userEmail'];
			$___D['replyToName'] = $U['userName'];
			$___D['mailTo'] = $___D['sendTo'].','.$___D['ccto'];
			$s = _Mail::send($___D);
			if($s['errNo']){ $js = _js::e(5,'Error enviando correo: '.$s['text']); }
			else{ $js = _js::r('Correo enviado correctamente.','"cardId":"'.$F['cardId'].'"'); }
		}
	}
	echo $js;
}
?>