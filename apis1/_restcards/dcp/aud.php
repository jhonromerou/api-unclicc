<?php
if(_0s::$router== 'GET aud/base'){
	$___D['fromA']='FR.*, C.cardId,C.cardName FROM '._0s::$Tb['fqu_ofre'].' FR LEFT JOIN '._0s::$Tb['par_ocrd'].' C ON (C.cardId=FR.tr)';
	$___D['whA']='AND FR.formId=\'1\'';
	$___D['orderByDef']='FR.dateC DESC';
	echo Doc::get($___D,array('slps'=>'Y','slpAlias'=>'C'));
}
else if(_0s::$router== 'GET aud/base/form'){
	$q=a_sql::fetch('SELECT F.*, FR.docEntry,FR.docYear,C.cardId, C.cardName FROM '._0s::$Tb['fqu_oitf'].' F 
	LEFT JOIN '._0s::$Tb['fqu_ofre'].' FR ON (FR.formId=F.formId AND FR.docEntry=\''.$___D['docEntry'].'\' )
	LEFT JOIN '._0s::$Tb['par_ocrd'].' C ON (C.cardId=FR.tr AND FR.tt=\'card\')
	WHERE F.formCode=\'audBase\' LIMIT 1',array(1=>'Error obteniendo información del documento.',2=>'No se ha encontrado información del formulario.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$M=$q; $M['data']=array(); $M['L']=array();
		$q=a_sql::query('SELECT F1.*, FR1.value FROM '._0s::$Tb['fqu_itf1'].' F1 
		LEFT JOIN '._0s::$Tb['fqu_fre1'].' FR1 ON (FR1.askId=F1.askId AND FR1.docEntry=\''.$q['docEntry'].'\')
		WHERE F1.formId=\''.$q['formId'].'\'  ',array(1=>'Error obteniendo lineas del formulario.',2=>'El formulario #'.$q['formId'].', no existe .'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			while($L=$q->fetch_assoc()){
				$L['formData']=json_decode($L['formData'],1);
				$M['data'][$L['askId']]=$L['value'];
				$M['L'][]=$L;
			}
			$js=_js::enc2($M);
		}
	}
	echo $js;
}
else if(_0s::$router== 'POST aud/base/form' || _0s::$router== 'PUT aud/base/form'){
	$docEntry=(_js::ise($___D['docEntry']))?false:$___D['docEntry'];
	unset($___D['docEntry']);
	$qDo=(_0s::$router== 'POST aud/base/form' && $docEntry==false)?'insert':'update';
	if($qDo=='update' && $js=_js::ise($docEntry,'Se debe definir el ID del documento','numeric>0')){ }
	if($js=_js::ise($___D['docYear'],'Se debe definir el año para el documento.')){ }
	else if($js=_js::ise($___D['cardId'],'Se debe definir el ID del socio de negocios.')){ }
	else if(!is_array($___D['L']) || count($___D['L'])==0){ $js=_js::e(3,'No se enviaron respuestas para el formulario.'); }
	else{
	$___D['tt']='card'; $___D['tr']=$___D['cardId'];
		$L=$___D['L']; unset($___D['L'],$___D['cardId'],$___D['cardName'],$___D['textSearch']);
		$___D['dateUpd']=date('Y-m-d H:i:s');
		$___D['userUpd']=a_ses::$userId;
		$ins=a_sql::insert($___D,array('tbk'=>'fqu_ofre','qDo'=>$qDo,'kui'=>'uid_dateC','wh_change'=>'WHERE docEntry=\''.$docEntry.'\' LIMIT 1'));
		if($ins['err']){ die(_js::e(3,'Error guardando documento :'.$ins['text'])); }
		$docEntry=($ins['insertId'])?$ins['insertId']:$docEntry;
		$jsA='"docEntry":"'.$docEntry.'"';
		$js=_js::r('Documento guardado correctamente.',$jsA);
		$err=false;
		foreach($L as $n => $L2){
			$D2=array('docEntry'=>$docEntry,'askId'=>$n,'value'=>$L2['value']);
			$ins2=a_sql::insert($D2,array('tbk'=>'fqu_fre1','qDo'=>$qDo,'wh_change'=>'WHERE docEntry=\''.$docEntry.'\' AND askId=\''.$n.'\' LIMIT 1'));
			if($ins['err']){ $err='Error guardando lineas del documento: '.$ins2['text']; break; }
		}
		if($err){
			$js=_js::e(3,$err,$jsA);
		}
	}
	echo $js;
}
?>