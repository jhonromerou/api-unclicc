<?php
if($_GET['pdf']){
	_ADMS::_lb('lb/myPdf');
$pdf=new myPdf();
$pdf->D=array(
'pagTotal'=>'Y',
'head_h1'=>'Calendario Tributario Año 2019',
'head_desc'=>'DyD Consultores y Asesores SAS y DCP Revisores, le informa.',
'foot_desc'=>'*El (número) hace referencia al día de vencimiento.',
'imgWater'=>'http://static1.admsistems.com/_logos/logodyd-opa.png'
);
$pdf->AddPage('L','Letter',0);
$pdf->SetFont('Arial','',10);
$pdf->Write(0,'Estimado Alpaca SAS,',1);
$pdf->Write(0,'Estos son sus próximos vencimientos.',1);
$pdf->SetFont('Arial','',7);
$R=array(
array('t'=>'Enero',
'L'=>array('(11) IVA - Cuatrimestal')
),
array('t'=>'Febrero',
'L'=>array("(113) IVA - Cuatrimestal\n(13) Rte. Fuente\n(13) Rte. Fuente\n(13) Rte. Fuente\n(13) Rte. Fuente")
),
array('t'=>'Marzo',
'L'=>array("(113) IVA - Cuatrimestal\n(13) Rte. Fuente")
),
array('t'=>'Abril',
'L'=>array("(113) IVA - Cuatrimestal\n(13) Rte. Fuente")
)
);
$pdf->meTable(array('cols'=>6,'L'=>$R,'chartCol'=>'d'));
$pdf->Output();
}
else if($_GET['admsinfo']){
	_ADMS::_lb('adms');
	$s=lTask::ooct(array('gid'=>7,'act'=>'','gtdTask'=>'N'));
	echo a_sql::$errNoText;
	print_r($s);
}
?>