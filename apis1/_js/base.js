
$V.crdGroup={'0':'Ninguno','1':'Industria','2':'Comercio'};
$V.RF_tipEnt={PN:'Persona Natural',PJ:'Persona Jurídica'};
$V.RF_regTrib={RC:'Regimen Común',RS:'Regimen Simplificado',RE:'Regimen Extranjero',GC:'Gran Contribuyente'};
$V.crdLicTradType=[{k:'nit',v:'NIT'},{k:'dni',v:'Cédula Ciudadania'},{k:'dne',v:'Cédula Extranjeria'},{k:'pp',v:'Pasaporte'},{k:'ted',v:'Tarjeta Identidad'}];
$V.licTradType={nit:'NIT',dni:'Cédula Ciudadania',dne:'Cédula Extranjeria',pp:'Pasaporte'};
$V.gender={N:'N/A',M:'Masculino',F:'Femenino'};
$TbV['crdCpr.position']={gerente:'Gerente',contador:'Contador',repLegal:'Representante Legal',dirfiro:'Director Financiero',jCompras:'Jefe Compras',tesorero:'Tesorero',cartera:'Cartera',auxadmon:'Aux. Administrativo',compPagos:'Compras/Pagos',pagos:'Pagos'};
$V.crdPrsPosition=$TbV['crdCpr.position'];
$V.crdType=[{k:'C',v:'Cliente'},{k:'S',v:'Proveedor'},{k:'L',v:'Lead'}];
$V.crdTypeSell={C:'Cliente',L:'Lead'};

Api.Crd={a:'/api/crdv2/',b:'/1/crdv3/',
dry:'/1/crdv3/dry/',
cpr:'/1/crdv3/prsCnt/',
card:'/1/crdv3/card/',
};
$Doc.a['crd']={a:'crd.card',kl:'cardId',kt:'cardName'};

$Mdl.Cnf.crdReqCode='Y';
$Mdl.Cnf.crdFichaPut=[/* Opciones en ventana creación socio */
{textNode:'General','class':'active',winClass:'_general'},
{textNode:'Direcciones',winClass:'_address'},
{textNode:'Tributario',winClass:'_location'},
{textNode:'Financiero',winClass:'_financiero'}
];

$M.sAdd([
{L:[{folId:'crd',folName:'Socios de Negocios',ico:'fa fa-institution'}]},
{fatherId:'crd',MLis:['cpr','crd.c','crd.s','crd.l']}
]);
$M.liA['crd']={t:'Socios de Negocios', ico:'fa-institution',
L:[
	{t:'Clientes',L:[{k:'crd.c.basic',t:'Visualizar Clientes'},{k:'crd.c.write',t:'Modificar Clientes'}]},
	{t:'Proveedores',L:[{k:'crd.s.basic',t:'Ver / Modificar Proveedores'}]},
	{t:'Contactos',k:'cpr.basic'},{t:'Leads - Ver / Modificar',k:'crd.l.basic'}
]
};

_Fi['ocrd']=function(wrap,func){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'A.cardCode(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'A.cardName(E_like3)'}},divL);
	if(Pa=='crd.c' || Pa=='crd.l' ){
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Responsable Venta'},I:{tag:'select',sel:{'class':jsV,name:'A.slpId(E_igual)'},opts:$Tb.oslp}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsV,name:'A.grId(E_igual)'},opts:$V.crdGroup}},divL);
	}
	if(!func){ func=$crd.get; }
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:func});
	wrap.appendChild(btnSend);
};
_Fi['cpr']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text',name:'P.name(E_like3)','class':jsV}},wrap);
	$1.T.divL({wxn:'wrapx8',subText:'Fecha',L:{textNode:'Cargo'},I:{tag:'divSelect',sel:{'class':jsV,name:'P.position(E_igual)'},opts:$TbV['crdCpr.position']}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Empresa'},I:{tag:'input',type:'text','class':jsV,placeholder:'Nombre...',name:'A.cardName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:$crd.prsCnt.get});
	wrap.appendChild(btnSend);
};

$M.li['crd.c']={t:'Clientes', kau:'crd.c.basic', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Cliente', func:function(){ $M.to('crd.c.form'); }});
	$M.Ht.ini({fieldset:'Y',btnNew:btn, func_filt:'ocrd', func_pageAndCont:$crd.get });
}};

$M.li['crd.c.form']={t:'Formulario de Socio', kau:'crd.c.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.form({cardType:'C'}); }}); }};

$M.li['crd.s']={t:'Proveedores', kau:'crd.s.basic', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Proveedor', func:function(){ $M.to('crd.s.form'); }});
	$M.Ht.ini({fieldset:'Y',btnNew:btn, func_filt:'ocrd', func_pageAndCont:$crd.get });
}};
$M.li['crd.s.form']={t:'Formulario de Proveeedor', kau:'crd.s.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.form({cardType:'S'}); }}); }};

$M.li['crd.l']={t:'Leads', kau:'crd.l.basic', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Lead', func:function(){ $M.to('crd.l.form'); }});
	$M.Ht.ini({fieldset:'Y',btnNew:btn, func_filt:'ocrd', func_pageAndCont:$crd.get });
}};

$M.li['crd.l.form']={t:'Formulario de Lead', kau:'crd.l.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.form({cardType:'L'}); }}); }};

$M.li['cpr']={t:'Personas de Contacto', kau:'cpr.basic', ico:'fa fa-institution', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Contacto', func:function(){ $M.to('cpr.form'); }});
	$M.Ht.ini({fieldset:'Y',btnNew:btn, func_filt:'cpr', func_pageAndCont:$crd.prsCnt.get });
}};
$M.li['cpr.form']={t:'Formulario de Contacto', kau:'cpr.basic', func:function(){ $M.Ht.ini({func_cont:function(){ $crd.prsCnt.form(); }}); }};


$M.li['crd.card']={noTitle:'Y', kau:'crd.c.basic', func:function(){
	$M.Ht.ini({func_cont:$crd.Card.open }); }};


var $crd={
opts:function(P,pare,Pa){
	var L=P.L;
	var Li=[];
	Li.push({ico:'fa fa_pencil',textNode:' Modificar',P:L,href:$M.to(Pa+'.form','cardId:'+L.cardId,'r') });
	Li=$Opts.add('crd',Li,L);
	$1.Menu.winLiRel({Li:Li,textNode:P.textNode},pare);
	$Mdl.itemLiD('crd',pare,L);
},
form:function(P,cont){
	var jsF='jsFields'; var cont=$M.Ht.cont; 
	var Pa=$M.read(); var Pac=$M.read('!');
	var cardType=(Pac=='crd.s.form')?'S':'C';
	cardType=(Pac=='crd.l.form')?'L':cardType;
	isC=(Pac=='crd.c.form');
	$Api.get({f:Api.Crd.a+'a.formData',loadVerif:!Pa.cardId, inputs:'cardId='+Pa.cardId, loade:cont, func:function(Jr){
	Jr.cardType=(Jr.cardType)?Jr.cardType:cardType;
	//$1.t('input',{type:'hidden','class':jsF,name:'cardType', value:((Jr.cardType)?Jr.cardType:P.cardType)},cont);
	var cardId=(Jr.cardId)?Jr.cardId:Pa.cardId;
	var inpId=$1.t('input',{type:'hidden','class':jsF,name:'cardId',value:cardId},cont);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:$Mdl.Cnf.crdReqCode,L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsF,name:'cardCode',value:Jr.cardCode}},cont);
	$1.T.divL({wxn:'wrapx4',req:'Y',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsF+' __cardName',name:'cardName',value:Jr.cardName}},divL);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Nombre Comercial'},I:{tag:'input',type:'text','class':jsF,name:'cardNameC',value:Jr.cardNameC}},divL);
	var inpN=$1.q('.__cardName',divL);
	inpN.onkeyup=inpN.onchange=function(){ 
		$1.q('#__wrapTitle').innerText = 'Formulario de: '+this.value; 
		}
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Responsable'},I:{tag:'select',sel:{'class':jsF,name:'slpId'},opts:$Tb.oslp,selected:Jr.slpId}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsF,name:'grId'},opts:$V.crdGroup,noBlank:1,selected:Jr.grId}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Activo'},I:{tag:'select',sel:{'class':jsF,name:'actived'},opts:$V.YN,noBlank:1,selected:Jr.actived}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:'Tipo Documento',I:{tag:'select',sel:{'class':jsF,name:'licTradType'},opts:$V.crdLicTradType,selected:Jr.licTradType}},cont);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'No. Documento'},I:{tag:'input',type:'text','class':jsF,name:'licTradNum',value:Jr.licTradNum}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Tipo Entidad'},I:{tag:'select',sel:{'class':jsF,name:'RF_tipEnt'},opts:$V.RF_tipEnt,selected:Jr.RF_tipEnt}},divL);
	$1.T.divL({wxn:'wrapx8',L:{textNode:'Regimen'},I:{tag:'select',sel:{'class':jsF,name:'RF_regTrib'},opts:$V.RF_regTrib,selected:Jr.RF_regTrib}},divL);
	var winM=$1.Menu.inLine($Mdl.Cnf.crdFichaPut,{winCont:1}); cont.appendChild(winM);
	$Cnt.reset();
	$V_Mmag=$js.sortNum($V_Mmag,{k:'v'});
	var gen=$1.t('div',{'class':'winMenuInLine _general'},winM);
	$1.Tb.trsI([
		{t:'Tipo Socio',node:$1.T.sel({sel:{'class':jsF,name:'cardType'},opts:$V.crdType,selected:Jr.cardType,noBlank:'Y'})},
		{t:'Referido por',node:$1.t('input',{type:'text','class':jsF,name:'referFrom',value:Jr.referFrom})},
		{t:'Ciudad / Municipio',node:$1.T.sel({sel:{'class':jsF,name:'RF_mmag'},opts:$V_Mmag,selected:Jr.RF_mmag})},
		{t:'Dirección',node:$1.t('input',{type:'text','class':jsF,name:'address',value:Jr.address})},
		{t:'Teléfono 1',node:$1.t('input',{type:'text','class':jsF,name:'phone1',value:Jr.phone1})},
		{t:'Teléfono 2',node:$1.t('input',{type:'text','class':jsF,name:'phone2',value:Jr.phone2})},
		{t:'Tel. Movil',node:$1.t('input',{type:'text','class':jsF,name:'cellular',value:Jr.cellular})},
		{t:'Email',node:$1.t('input',{type:'text','class':jsF,name:'email',value:Jr.email})},
		{t:'ID Adicional',node:$1.t('input',{type:'text','class':jsF,name:'addId',value:Jr.addId})}
		],gen);
		var lct=$1.t('div',{'class':'winMenuInLine _location',style:'display:none'},winM);
	$1.Tb.trsI([
		{t:'Act. Económica (ciiu)',node:$1.t('input',{type:'text','class':jsF,name:'RF_actEco',value:Jr.RF_actEco})},
		{t:'Primer Nombre (PN)',node:$1.t('input',{type:'text','class':jsF,name:'RF_firstName',value:Jr.RF_firstName})},
		{t:'Segundo Nombre (PN)',node:$1.t('input',{type:'text','class':jsF,name:'RF_firstName2',value:Jr.RF_firstName2})},
		{t:'Primer Apellido (PN)',node:$1.t('input',{type:'text','class':jsF,name:'RF_lastName',value:Jr.RF_lastName})},
		{t:'Segundo Apellido (PN)',node:$1.t('input',{type:'text','class':jsF,name:'RF_lastName2',value:Jr.RF_lastName2})},
		{t:'Sujeto a Rerención',node:$1.T.sel({sel:{'class':jsF,name:'RF_reqRet'},opts:$V.NY,selected:Jr.RF_reqRet,noBlank:1})},
		{t:'¿Autoretenedor',node:$1.T.sel({sel:{'class':jsF,name:'RF_autoRet'},opts:$V.NY,selected:Jr.RF_autoRet,noBlank:1})},
		],lct);
	var cnto=$1.t('div',{'class':'winMenuInLine _address',style:'display:none'},winM);
	for(var c in Jr.cntAddr){ $Cnt.addr({cont:cnto},Jr.cntAddr[c]); }
	if(!Jr.cntAddr || (Jr.cntAddr).length==0){$Cnt.addr({cont:cnto}); }
	if($V.crdInfoOn!='N'){
		for(var c in Jr.cntPhone){ $Cnt.phone({cont:cnto},Jr.cntPhone[c]); }
		if(!Jr.cntPhone || (Jr.cntPhone).length==0){$Cnt.phone({cont:cnto}); }
		for(var c in Jr.cntEmail){ $Cnt.email({cont:cnto},Jr.cntEmail[c]); }
		if(!Jr.cntEmail || (Jr.cntEmail).length==0){ $Cnt.email({cont:cnto}); }
	}
	var fro=$1.t('div',{'class':'winMenuInLine _financiero',style:'display:none'},winM);
	$1.Tb.trsI([
		{t:'Descuento',node:$1.t('input',{type:'number',inputmode:'numeric','class':jsF,name:'discPf',value:Jr.discPf,min:0,max:100})},
		{t:'Dia Cierre Facturación',node:$1.t('input',{type:'number',inputmode:'numeric','class':jsF,name:'invDayClose',value:Jr.invDayClose,min:1,max:31})},
		{t:'Cupo de Crédito',node:$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'creditLine',value:Jr.creditLine})},
		{t:'Forma de Pago',node:$1.T.sel({sel:{'class':jsF,name:'fdpId'},opts:$Tb.gfiOfdp,selected:Jr.fdpId}),aGo:'acc.fdp'},
		{t:'Condiciones de Pago',node:$1.T.sel({sel:{'class':jsF,name:'pymId'},opts:$Tb.gfiOpym,selected:Jr.pymId}),aGo:'acc.pym'}
		],fro);
	var resp=$1.t('div',0,cont);
	var btnSend=$Api.send({PUT:Api.Crd.a+'a.formData', getInputs:function(){ return $1.G.inputs(cont,jsF); }, loade:resp, func:function(Jr2){
		if(Jr2.cardId){ inpId.value = Jr2.cardId; }
		$ps_DB.response(resp,Jr2);
	}}); cont.appendChild(btnSend);
	}});
},
get:function(P){
	var P=(P)?P:{};
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost='';
	var Pa=$M.read('!');
	if(Pa=='crd.c'){ vPost='cardType=C'; }
	else if(Pa=='crd.s'){ vPost='cardType=S'; }
	else if(Pa=='crd.l'){ vPost='cardType=L'; }
	else if(Pa=='crd.e'){ vPost='cardType=E'; }
	if(P.vPost){ vPost += '&'+P.vPost; }
	vPost+='&'+$1.G.filter()
	$Api.get({f:Api.Crd.a+'a',inputs:vPost, loade:cont, func:function(Jr){
		if(P.func){ P.func(Jr); return true; }
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['','Código','Nombre','Grupo','Resp. Ventas','Tel. 1','Tel. 2','Email']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			$crd.opts({L:L},td,Pa);
			$1.t('td',{textNode:L.cardCode},tr);
			var td=$1.t('td',0,tr);
			$1.t('a',{href:$Doc.href('crd',L,'r'), textNode:L.cardName,'class':'fa fa_eye'},td);
			$1.t('td',{textNode:$V._g('crdGroup',L.grId)},tr);
			$1.t('td',{textNode:$Tb._g('oslp',L.slpId)},tr);
			$1.t('td',{textNode:L.phone1},tr);
			$1.t('td',{textNode:L.phone2},tr);
			$1.t('td',{textNode:L.email},tr);
		}
	}});
}
}

$crd.prsCnt={
opts:function(P,pare){
	var L=P.L;
	var Li=[];
	Li.push({ico:'fa fa_pencil',textNode:' Modificar',P:L,href:$M.to('cpr.form','prsId:'+L.prsId,'r') });
	Li=$Opts.add('cpr',Li,L);
	$1.Menu.winLiRel({Li:Li,textNode:P.textNode},pare);
	$Mdl.itemLiD('cpr',pare,L);
},
at:function(cont){
	var Pa=$M.read();
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost=(Pa.cardId)?'tt=card&tr='+Pa.cardId:'';
	var jsF='jsFields'; var tBody=$1.t('tbody');
	$Api.get({f:Api.Crd.cpr+'at', inputs:vPost,loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{ $M.Ht.title.innerHTML =Jr.cardName+' - '+Jr.licTradNum; }
		var cs=8; var ni=1; 
		var tb=$1.T.table(['#','Nombre Contacto','Celular','Teléfono 1','Teléfono 2','Email','Cargo','Tipo Doc','N°. Doc.','']); cont.appendChild(tb);
		tb.appendChild(tBody);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.name},tr);
			$1.t('td',{textNode:L.cellular},tr);
			$1.t('td',{textNode:L.tel1},tr);
			$1.t('td',{textNode:L.tel2},tr);
			$1.t('td',{textNode:L.email},tr);
			$1.t('td',{textNode:$TbV._g('crdCpr.position',L.position)},tr);
			$1.t('td',{textNode:_g(L.licTradType,$V.crdLicTradType)},tr);
			$1.t('td',{textNode:L.licTradNum},tr);
		}
	}});
},
formLine:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	var vPost='cardId='+Pa.cardId;
	var jsF='jsFields'; var tBody=$1.t('tbody');
	$Api.get({f:Api.Crd.a+'prsCnt', inputs:vPost,loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{ $M.Ht.title.innerHTML =Jr.cardName+' - '+Jr.licTradNum; }
		var cs=8; var ni=1; 
		var tb=$1.T.table(['#','Nombre Contacto','Teléfono 1','Teléfono 2','Email','Cargo','Tipo Doc','N°. Doc.','']); cont.appendChild(tb);
		tb.appendChild(tBody);
		var tdAdd=$1.t('td',{colspan:cs},($1.t('tr',0,$1.t('tfoot',0,tb))));
		var btnAdd=$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea', func:function(){ trA(ni); ni++; }});
		tdAdd.appendChild(btnAdd);
		if(Jr.L && Jr.L.errNo){ $1.t('td',{textNode:Jr.L.text,colspan:cs},$1.t('tr',0,tBody)); }
		else{
			for(var n in Jr.L){ var ni=Jr.L[n].lineNum; trA(ni,Jr.L[n]); ni++; }
		}
		trA(ni); ni++;
		var resp=$1.t('div',0,cont);
		var btn=$Api.send({PUT:Api.Crd.a+'prsCnt', loade:resp, getInputs:function(){ return vPost+'&'+$1.G.inputs(cont); },func:function(Jr2){
			$ps_DB.response(resp,Jr2);
		}},cont);
	}});
	function trA(ni,L){ L=(L)?L:{};
	if(L && L.lineNum){ ni=L.lineNum; }
		var ln='L['+ni+']';
		var tr=$1.t('tr',0,tBody);
		var td=$1.t('td',{textNode:ni},tr);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'text',name:ln+'[name]','class':jsF,value:L.name},td);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'text',name:ln+'[tel1]','class':jsF,value:L.tel1,style:'width:8rem;'},td);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'text',name:ln+'[tel2]','class':jsF,value:L.tel2,style:'width:8rem;'},td);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'text',name:ln+'[email]','class':jsF,value:L.email,style:'width:10rem;'},td);
		var td=$1.t('td',0,tr);
		if($TbV['crdCpr.position']){
			$1.T.divSelect({sel:{'class':jsF,name:ln+'[position]'},opts:$TbV['crdCpr.position'],selected:L.position,width:7},td);
		}
		else{
			$1.t('input',{type:'text',name:ln+'[position]','class':jsF,value:L.position,style:'width:8rem;'},td);
		}
		var td=$1.t('td',0,tr);
		$1.T.sel({sel:{name:ln+'[licTradType]','class':jsF},opts:$V.crdLicTradType,selected:L.licTradType},td);
		var td=$1.t('td',0,tr);
		$1.t('input',{type:'text',name:ln+'[licTradNum]','class':jsF,value:L.licTradNum,style:'width:10rem;'},td);
		var td=$1.t('td',0,tr);
		var ck=$1.T.ckLabel({t:'Eliminar',I:{'class':jsF+' checkSel_trash',name:ln+'[delete]'}},td);
	}
},
get:function(){
	cont=$M.Ht.cont;
	$Api.get({f:Api.Crd.cpr,inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['','Nombre','Cargo','Celular','Tel. 1','Tel. 2','Email','Empresa']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',{'class':'__mdlCrd_itemLi',L:L},tr);
			$crd.prsCnt.opts({L:L},td);
			var td=$1.t('td',{textNode:L.name},tr);
			//$1.t('a',{href:$Doc.href('cpr',L,'r'), textNode:L.name,'class':'fa fa_eye'},td);
			$1.t('td',{textNode:$TbV._g('crdCpr.position',L.position)},tr);
			$1.t('td',{textNode:L.cellular},tr);
			$1.t('td',{textNode:L.tel1},tr);
			$1.t('td',{textNode:L.tel2},tr);
			$1.t('td',{textNode:L.email},tr);
			$1.t('td',{node:$Doc.href('crd',L)},tr);
		}
	}});
},
form:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	var vPost='prsId='+Pa.prsId;
	var jsF='jsFields'; var tBody=$1.t('tbody');
	$Api.get({f:Api.Crd.cpr+'form', loadVerif:!Pa.prsId, inputs:vPost,loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{ $M.Ht.title.innerHTML =Jr.name;
		L=Jr;
			var wid=$1.t('input',{type:'hidden','class':jsF,name:'prsId',value:Pa.prsId},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:'Nombre Completo',placeholder:'Nombre Apellido',I:{tag:'input',type:'text','class':jsF,name:'name',value:L.name}},cont);
			if($TbV['crdCpr.position']){
				$1.T.divL({wxn:'wrapx4',req:'Y',L:'Cargo',I:{tag:'divSelect',sel:{'class':jsF,name:'position'},opts:$TbV['crdCpr.position'],selected:L.position}},divL);
			}
			else{
				$1.T.divL({divLine:1,wxn:'wrapx4',req:'Y',L:'Cargo',I:{tag:'input',type:'text','class':jsF,name:'position',value:L.position}},divL);
			}
			var sea=$crd.sea({cardId:Jr.cardId,cardName:Jr.cardName});
			$1.T.divL({wxn:'wrapx4',L:'Empresa',I:{node:sea}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:'Genero',I:{tag:'divSelect',sel:{'class':jsF,name:'gender'},opts:$V.gender,selected:L.gender}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Profesión',I:{tag:'input',type:'text','class':jsF,name:'title',value:L.title}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo Documento',I:{tag:'select',sel:{'class':jsF,name:'licTradNum'},opts:$V.crdLicTradType,selected:Jr.licTradType}},divL);
			$1.T.divL({wxn:'wrapx8',L:'N°. Documento',I:{tag:'input',type:'text','class':jsF,name:'licTradNum',value:L.licTradNum}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Teléfono 1',I:{tag:'input',type:'text','class':jsF,name:'tel1',value:Jr.tel1}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Teléfono 2',I:{tag:'input',type:'text','class':jsF,name:'tel2',value:Jr.tel2}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Celular',I:{tag:'input',type:'text','class':jsF,name:'cellular',value:Jr.cellular}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Email',I:{tag:'input',type:'text','class':jsF,name:'email',value:Jr.email}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Cumpleaños',I:{tag:'input',type:'date','class':jsF,name:'birthDay',value:Jr.birthDay}},divL);
			$1.t('h4',{textNode:'Direcciónes','class':'divLineTitleSection'},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Dirección Principal',placeholder:'Cr 29-10 #33B',I:{tag:'input',type:'text','class':jsF,name:'address',value:Jr.address}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Ciudad Principal',I:{tag:'input',type:'text','class':jsF,name:'city',value:Jr.city}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Dirección Secundaria',placeholder:'Cr 29-10 #33B',I:{tag:'input',type:'text','class':jsF,name:'address2',value:Jr.address2}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Ciudad Secundaria',I:{tag:'input',type:'text','class':jsF,name:'city2',value:Jr.city2}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Notas',I:{tag:'textarea','class':jsF,name:'notes',textNode:Jr.notes}},cont);
		}
		var resp=$1.t('div',0,cont);
		var btn=$Api.send({PUT:Api.Crd.cpr, loade:resp, getInputs:function(){ return vPost+'&'+$1.G.inputs(cont); },func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(Jr2.prsId){ wid.value=Jr2.prsId; }
		}},cont);
	}});
}
}

$crd.sea=function(P,cont){
	//inputs,k, jsF, cardId,cardName
	var api=(P.k=='S')?'crd.s':'crd.c';
	api=(P.k=='c-l')?'crd.cl':api;
	var jsF=(P.jsF)?P.jsF:'jsFields';
	var inputs=P.inputs; //fields=&_otD[]&wh=
	var vS=(P.cardId)?{cardId:P.cardId}:null;
	var vP=(P.vP)?P.vP:{}; //campos adicionales obteniedos ..discPef
	var vPg=(P.vPg)?P.vPg:{}; //campos a obtener al seleccionar
	var vP0=(P.vP0)?P.vP0:{}; //Campos a obtener al crear
	for(var e in vPg){ vPg[e]=vP0[vPg[e]]; }
	vP0.cardId=P.cardId; vP0.cardName=P.cardName;
	var vPost0=$js.encUri(vP0);
	var sea=$Sea.input(null,{api:api,classKey:'___cardId', 'class':jsF,req:P.req, inputs:inputs, vS:vS, vPost:vPost0, defLineText:P.cardName, func:function(L,inp){
		vPg.cardId=L.cardId; vPg.cardName=L.cardName;
		for(var kc in vPg){
			if(L[kc]){ vP0[kc]=L[kc]; }
		}//discDef:discPf
		vPost=$js.encUri(vP0);
		for(var k in vP){ vPost += '&'+k+'='+vP[k]; }
		inp.O={vPost:vPost};
		if(P.replaceData=='Y'){ $Sea.replaceData(L,cont); }
		if(P.tbSum=='Y'){ tbSum.get(cont);}
		if(P.func){ P.func(L,inp); }
	}});
	return sea;
}

$crd.Fx={
inpSeaPrs:function(P,cont,divRel){
	P.api=Api.Crd.b+'sea/prs';
	if(P.func){ }
	else{
		P.func=function(R,inp,divRel){
			inp.vPost='prsId='+R.prsId;
			inp._jsV={prsId:R.prsId,prsName:R.name};
			divRel=(divRel)?divRel:inp.parentNode.parentNode;
			$Sea.replaceData(R,divRel);
		};
		P.vPostClear='prsId=0';
	}
	return $Api.Sea.input(P,cont);
},
inpSeaCrd:function(P,cont,divRel){
	P.api=Api.Crd.b+'sea/crd';
	P.func=function(R,inp){
		inp.vPost='cardId='+R.cardId;
		inp._jsV={}; 
		if(P._jsV){
			for(var x1 in P._jsV){ inp._jsV[x1]=R[x1]; }
		}
		else{ inp._jsV={cardId:R.cardId,cardName:R.cardName}; }
		divRel=(divRel)?divRel:inp.parentNode.parentNode;
		$Sea.replaceData(R,divRel);
	};
	P.vPostClear='cardId=0';
	return $Api.Sea.input(P,cont);
},
boxCrd:function(P,cont){
	P.api=Api.Crd.b+'sea/crd';
	P.jsVB=['cardId','cardName'];
	P.func=function(R,inp){
		divRel=(P.topPare)?P.topPare:inp.parentNode.parentNode;
		$Api.Sea.boxRep(R,divRel);
	};
	return $Api.Sea.box(P,cont);
},
boxCpr:function(P,cont,divRel){
	P.api=Api.Crd.b+'sea/prs';
	P.jsVB=['prsId','name'];
	P.func=function(R,inp){
		divRel=(P.topPare)?P.topPare:inp.parentNode.parentNode;
		$Sea.replaceData(R,divRel);
	};
	return $Api.Sea.box(P,cont);
}
}

$crd.Dry={};

$M.sAdd([
{fatherId:'masters',L:[{folId:'mastCrd',folName:'Socios'}]},
{fatherId:'mastCrd',MLis:['sysd.massData.ocrd','sysd.massData.parCpr']},
]);
$M.li['sysd.massData.parCrd']={t:'Socios de Negocios',t2:'Socios de Negocios (Maestro)',kau:'sysd.suadmin', func:function(){ $M.Ht.ini({func_pageAndCont:function(){ $Sysd.MassData.form({k:'parCrd'}) }}); }};
$M.li['sysd.massData.parCpr']={t:'Contactos',t2:'Contactos (Maestro)',kau:'sysd.suadmin', func:function(){ $M.Ht.ini({func_pageAndCont:function(){ $Sysd.MassData.form({k:'parCpr'}) }}); }};

$V.cntWorkerType={1:'Otros',2:'Gerente',3:'Contador',4:'Auxiliar',5:' Gestión Humana',6:'Compras',7:'Ventas'};
$V.cntAddrType={merch:'Destino',inv:'Facturación',other:'Otros'};
$V.cntNumType={work:'Laboral',personal:'Personal',whatsapp:'Whatsapp'};
$V.cntEmailType={work:'Laboral',personal:'Personal',inv:'Facturación'};
$Cnt ={
n:{a:1,p:1,e:1,prs:1},
reset:function(){ $Cnt.n.a=$Cnt.n.p=$Cnt.n.e=$Cnt.n.prs=1; },
addr:function(P,L){
	var L=(L)?L:{}; P=(P)?P:{};
	jsF=(P.jsF)?P.jsF:'jsFields'; var n=$Cnt.n.a; $Cnt.n.a++;
	var cont=$1.q('.__wrapCntAddr',P.cont);
	bg='#36de89';
	if(!cont){ cont=$1.t('fieldset',{'class':'__wrapCntAddr',style:'border-top:0.25rem solid '+bg+'; margin-bottom:0.5rem;'},P.cont); }
	if(n==1){
		$1.t('legend',{textNode:'Direcciones'},cont);
	}
	var ln='_Addr['+n+']';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx10',I:{tag:'select',sel:{'class':jsF,name:ln+'[addrType]'},opts:$V.cntAddrType,selected:L.addrType}});
	$1.T.divL({wxn:'wrapx10',I:{tag:'input',type:'text','class':jsF,name:ln+'[addrName]',placeholder:'Nombre Desc...',value:L.addrName}},divL);
	$1.T.divL({wxn:'wrapx5',I:{tag:'input',type:'text','class':jsF,name:ln+'[address]',placeholder:'Dirección: Calle 33 #10-21',value:L.address}},divL);
	$1.T.divL({wxn:'wrapx10',I:{tag:'input',type:'text','class':jsF,name:ln+'[city]',placeholder:'Ciudad...',value:L.city}},divL);
	$1.T.divL({wxn:'wrapx10',I:{tag:'input',type:'text','class':jsF,name:ln+'[county]',placeholder:'Departamento / Region...',value:L.county}},divL);
	$1.T.divL({wxn:'wrapx10',I:{tag:'input',type:'text','class':jsF,name:ln+'[country]',placeholder:'Pais...',value:L.country}},divL);
	$1.T.divL({wxn:'wrapx10',I:{tag:'input',type:'text','class':jsF,name:ln+'[zipCode]',placeholder:'Código Postal...',value:L.zipCode}},divL);
	if(1){
		var ck=$1.T.ckLabel({t:'Eliminar',I:{'class':jsF,name:ln+'[delete]'}},divL);
	}
	//else{ divL.appendChild($1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(divL); }})); }
	if(n==1){
		var btnA=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir otra dirección', func:function(){$Cnt.addr(P); }}); cont.appendChild(btnA);
		btnA.style.color=bg;
	}
	$1.I.before(divL,cont.lastChild);
},
phone:function(P,L){
	var L=(L)?L:{}; P=(P)?P:{};
	jsF=(P.jsF)?P.jsF:'jsFields'; var n=$Cnt.n.p; $Cnt.n.p++;
	var cont=$1.q('.__wrapCntPhone',P.cont);
	bg='#DE8B36';
	if(!cont){ cont=$1.t('fieldset',{'class':'__wrapCntPhone',style:'border-top:0.25rem solid '+bg+'; margin-bottom:0.5rem;'},P.cont); }
	subText=''; stext2='.';
	if(n==1){
		$1.t('legend',{textNode:'Teléfono/s'},cont);
		subText='Persona';
	}
	var ln='_Phone['+n+']';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',subText:subText,I:{tag:'input',type:'text','class':jsF,name:ln+'[peoName]',value:L.peoName}});
	$1.T.divL({wxn:'wrapx8',subText:stext2,I:{tag:'select',sel:{'class':jsF,name:ln+'[numType]'},opts:$V.cntNumType,selected:L.numType}},divL);
	$1.T.divL({wxn:'wrapx4',I:{tag:'input',type:'text','class':jsF,name:ln+'[number]',placeholder:'329 367 8510',value:L.number}},divL);
	$1.T.divL({wxn:'wrapx8',I:{tag:'input',type:'text','class':jsF,name:ln+'[numExt]',placeholder:'Extensión...',value:L.numExt}},divL);
	$1.T.divL({wxn:'wrapx8',I:{tag:'input',type:'text','class':jsF,name:ln+'[numHorary]',placeholder:'Atención: 7 a 12, 1 a 5',value:L.numHorary}},divL);
	if(1){
		var ck=$1.T.ckLabel({t:'Eliminar',I:{'class':jsF,name:ln+'[delete]'}},divL);
	}else{ divL.appendChild($1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(divL); }})); }
	if(n==1){
		var btnA=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir otro número', func:function(){$Cnt.phone(P); }}); cont.appendChild(btnA);
		btnA.style.color=bg;
	}
	$1.I.before(divL,cont.lastChild);
},
email:function(P,L){
	var L=(L)?L:{}; P=(P)?P:{};
	jsF=(P.jsF)?P.jsF:'jsFields'; var n=$Cnt.n.e; $Cnt.n.e++;
	var cont=$1.q('.__wrapCntMail',P.cont);
	bg='#36A8DE';
	if(!cont){ cont=$1.t('fieldset',{'class':'__wrapCntMail',style:'border-top:0.25rem solid '+bg+'; margin-bottom:0.5rem;'},P.cont); }
	var subText=''; var stext2=' ';
	if(n==1){
		$1.t('legend',{textNode:'Email'},cont);
		subText='Persona'; stext2='.';
	}
	var ln='_Email['+n+']';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',subText:subText,I:{tag:'input',type:'text','class':jsF,name:ln+'[peoName]',value:L.peoName}});
	$1.T.divL({wxn:'wrapx8',subText:stext2,I:{tag:'select',sel:{'class':jsF,name:ln+'[emailType]'},opts:$V.cntEmailType,selected:L.emailType}},divL);
	$1.T.divL({wxn:'wrapx4',subText:stext2,I:{tag:'input',type:'text','class':jsF,name:ln+'[email]',placeholder:'info@empresa.com',value:L.email}},divL);
	if(1){
		var ck=$1.T.ckLabel({t:'Eliminar',I:{'class':jsF,name:ln+'[delete]'}},divL);
	}else{ divL.appendChild($1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(divL); }})); }
	if(n==1){
		var btnA=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir otro Email', func:function(){ $Cnt.email(P); }}); cont.appendChild(btnA);
		btnA.style.color=bg;
	}
	$1.I.before(divL,cont.lastChild);
},
prs:function(P,L){
	var L=(L)?L:{}; P=(P)?P:{};
	jsF=(P.jsF)?P.jsF:'jsFields'; var n=$Cnt.n.prs; $Cnt.n.prs++;
	var cont=$1.q('.__wrapCntPrs',P.cont);
	bg='#36A8DE';
	if(!cont){ cont=$1.t('fieldset',{'class':'__wrapCntPrs',style:'border-top:0.25rem solid '+bg+'; margin-bottom:0.5rem;'},P.cont); }
	var f1=f2=f3=f4=f5='';
	if(n==1){
		$1.t('legend',{textNode:'Personas de Contacto'},cont);
		f1='Nombre'; f2='Email'; f3='Teléfono'; f4='Teléfono 2'; f5='Cargo';
	}
	var ln='_Prs['+n+']';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',supText:f1,I:{tag:'input',type:'text','class':jsF,name:ln+'[name]',value:L.name}});
	$1.T.divL({wxn:'wrapx8',supText:f2,I:{tag:'input',type:'text','class':jsF,name:ln+'[email]',placeholder:'info@empresa.com',value:L.email}},divL);
	$1.T.divL({wxn:'wrapx10',supText:f3,I:{tag:'input',type:'text','class':jsF,name:ln+'[tel2]',value:L.tel1}},divL);
	$1.T.divL({wxn:'wrapx10',supText:f4,I:{tag:'input',type:'text','class':jsF,name:ln+'[tel2]',value:L.tel2}},divL);
	$1.T.divL({wxn:'wrapx10',supText:f5,I:{tag:'input',type:'text','class':jsF,name:ln+'[position]',value:L.position}},divL);
	if(1){
		var ck=$1.T.ckLabel({t:'Eliminar',I:{'class':jsF,name:ln+'[delete]'}},divL);
	}else{ divL.appendChild($1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(divL); }})); }
	if(n==1){
		var btnA=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Añadir Otra', func:function(){ $Cnt.prs(P); }}); cont.appendChild(btnA);
		btnA.style.color=bg;
	}
	$1.I.before(divL,cont.lastChild);
},

};
$crd.W={
addr:function(){

}
}

$crd.Card={
open:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	$Api.get({f:Api.Crd.card, inputs:'cardId='+Pa.cardId, func:function(Jq){
		if(Jq.errNo){ $Api.resp(cont,Jq); }
		else{ d(Jq); }
	}});
	function d(P){
	var wrap = $1.t('div',{style:'style:padding:0.5rem 0;'},cont);
	var ri = $1.t('div',{style:'margin-left:1.25rem; font-size:1rem;'},wrap);
	$1.t('img',{src:'/img/cust_x120.png',style:'width:3rem;'},ri);
	$1.t('h3',{textNode:P.cardName},ri);
	var inf = $1.t('div',0,ri);
	var ffLine = $1.T.ffLine({ffLine:1, w:'ffx4', t:'Tipo:',v:$V._g('crdType',P.cardType)},inf);
	$1.T.ffLine({w:'ffx8', t:'Grupo:',v:$V._g('crdGroup',P.grId)},ffLine);
	$1.T.ffLine({w:'ffxauto', t:'ID:',v:P.cardId},ffLine);
	$1.T.ffLine({w:'ffxauto', t:'Activo:',v:$V._g('YN',P.active)},ffLine);
	var ffLine = $1.T.ffLine({ffLine:1, w:'ffx4', t:$V._g('licTradType',P.licTradType,'Tip. Doc.')+':', v:P.licTradNum});
	ffLine.appendChild($1.T.ffLine({w:'ffx4', t:'Regimen:',v:$V.RF_regTrib[P.RF_regTrib]}));
	ffLine.appendChild($1.T.ffLine({w:'ffx4', t:'Tipo Ent.:',v:$V.RF_tipEnt[P.RF_tipEnt]}));
	inf.appendChild(ffLine);
	var ffLine = $1.T.ffLine({ffLine:1, w:'ffx4', t:'Celular:',v:P.cellular},inf);
	$1.T.ffLine({w:'ffx4', t:'Tel. 1:',v:P.phone1},ffLine);
	$1.T.ffLine({w:'ffx4', t:'Tel. 2:',v:P.phone2},ffLine);
	var ffLine = $1.T.ffLine({ffLine:1, w:'ffx4', t:'Email:',v:P.email},inf);
	$1.T.ffLine({w:'ffx4', t:'Responsable:',v:$V._g('oslp',P.slpId,'Ninguno')},ffLine);
	wrap.appendChild($1.t('clear'));
	$crd.Card.opts(P,wrap);
	return wrap;
	}
},
info:function(P,pare){
	var wrap = $1.t('div',{'class':'winMenuInLine contactInfo'},pare);
	wrap.appendChild($1.t('br'));
	$1.t('h4',{'class':'head1',textNode:'Personas de Contacto'},wrap);
	$crd.prsCnt.at(pare);
	return wrap;
},
opts:function(P,pare){
	var wrap = $1.t('div',0,pare);
	var tT={tt:'bussParnerProfile',tr:P.cardId};
	var wrapMenu = $1.t('div',0,wrap);
	var Li = [
		{li:{textNode:'','class':'fa fa_info active',winClass:'winInfo' }},
		{li:{textNode:'','class':'fa fa_comment',winClass:'win5c', func:function(){ $5c.openOne(tT)} }},
		{li:{textNode:'','class':'fa fa_fileUpd',winClass:'win5f',func:function(){ $5fi.openOne(tT)}}}
	];
	if($Mdl.lisTask=='Y'){
		Li.push({li:{textNode:'','class':'fa fa_ulList',title:'Actividades',winClass:'winTask', func:function(){ Task.relAt(lef,{vP:{trMemo:P.cardName}}); }}});
	}
	var menu = $1.Menu.inLine(Li,wrap);
	wrapMenu.appendChild(menu);
	var info=$1.t('div',{'class':'winMenuInLine winInfo'},wrapMenu);
	var _5c=$1.t('div',{'class':'winMenuInLine win5c',style:'display:none;'},wrapMenu);
	var _5f=$1.t('div',{'class':'winMenuInLine win5f',style:'display:none;'},wrapMenu);
	$crd.Card.info(P,info);
	tT.wT={'class':'win5c'};
	$5c.formLine(tT,_5c);
	tT.wT={'class':'win5f'};
	$5fi.formLine(tT,_5f);
	if($Mdl.lisTask=='Y'){
		var task = $1.t('div',{'class':'winMenuInLine winTask',style:'display:none;'},wrapMenu);
		var lef=$1.t('div',{'class':Task.Cls.wrapper},task);
		var rig=$1.t('div',{'class':Task.Cls.wrapView},task);
		Task.Ht.iniWrap({listName:'Tareas Relacionadas'});
	}
	return wrap;
}
}

/* extensions */

