

$0s.FU ={//File Upload
x:function(Tfile,P,wrapPar){
	var wResp = $1.q('._wrapResp',wrapPar); $1.clear(wResp);
	P._FI=Tfile; P.method='POST';
	$xhr.open(P);
	return true;
	var formData = new FormData();
	for(var f=0; f<Tfile.files.length; f++){
		var file = Tfile.files[f];
		formData.append('file['+f+']', file, file.name);
	}
	if(window.XMLHttpRequest){ var xhr = new XMLHttpRequest(); }
	else if(window.ActiveXObject){ var xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
	xhr.open('POST', P.url, true);
	var abt = $1.t('button',{'class':'iBg iBg_closeSmall',textNode:'Cancelar'},wResp);
	abt.onclick = function(){ xhr.abort(); $1.clear(wResp); }
	var progress = $1.t('progress',{value:0,max:100,style:'width:50%; background-color:#0F0;'},wResp);
	xhr.upload.addEventListener("progress", function(e){
		progress.value = ((e.loaded/e.total)*100);
	}, false);
	xhr.onload = function(Event){
		if(xhr.status == 200){
			var Jq = JSON.parse(xhr.responseText);
			$Api.resp(wResp,Jq);
			if(Jq.errNo){}
			else{
				if(P.func){ P.func(wrapPar,P); }
			}
		}else{
			$Api.resp(wResp,{errNo:5,text:'Err Status: '+xhr.status});
		}
	};
	xhr.send(formData);
}
}

$VH={};
var $ccH={
	_g:function(k,k2){
		if($ccH[k] && $ccH[k][k2]){ return 'background-color:'+$ccH[k][k2]+';'; }
		else{ return ''; }
	}
}

var $o = {
apir:'/s/o/A:',
api:'/s/v1/5o/A:',
api2:'/api/sea/',
T:{activity:'activity',workBoard:'workBoard',wboList:'wboList',comment:'comment',fileUpd:'fileUpd',checkList:'checkList',bussPartner:'bussPartner',fimId:'fimId'}
,
perms:{
def:{read:'Ver', write:'Modificar'},
fimId:{read:'Leer',readEx:'Ejecutar',writeQuit:'Escribir y Quitar elementos.',write:'Ver,Escribir y eliminar elementos.',coowner:'Co-Propietario'}
}
,
open:function(O){
	switch(O.objType){
		case 'activity' : $5a.O.winFloat({reLoad:true, actId:O.objRef}); break;
		case 'workBoard' : $5b.List.get({wboId:O.objRef}); break;
		case 'wboList' : $5b.List.get({wboId:O.objRef}); break;
		case $o.T.bussPartner : location.href = '/a/bussPartner/'+O.objRef; break;
	}
}
,
tagO:function(P,C){ var C =(C)?C:{};
	var iBg = (C.justA) ? '' : 'iBg iBg_';
	if(P.objType == $o.T['bussPartner']){
		if(P.objText == ''){ ah = $1.t('textNode',''); }
		else{
			var name = P.objText;
			var ah = $1.t('a',{href:'/a/bussPartner/profile/'+P.objRef,textNode:name,'class':iBg+'customer',abbr:16});
		}
	}
	if(C.pareTag){ var t = $1.t(C.pareTag); t.appendChild(ah); ah = t; }
	return ah;
}
,
Se:{//$o.Se.form
Opts:{
	bussPartner_cust:'Cliente',bussPartner_sup:'Proveedor',bussPartner_emp:'Empleado', bussPartnerGroups:'Grupo de Socios',bussPartner_all:'Todos los contactos',
	userAssg:'Usuario Responsable'
},
form:function(P){ P=(P)?P:{};
	var jsFields = (P.jsFields)?P.jsFields:'';//clase definida para G.inputs
	var vPostBase = 'objpType='+P.objpType+'&objpRef='+P.objpRef;
	var opts = {};
	if(!P.opts){ P.opts = 'bussPartner_cust,bussPartner_sup,bussPartner_emp'; }
	var tOp = (P.opts).split(',');
	for(var e=0; e<tOp.length;e++){
		if($o.Se.Opts[tOp[e]]){ opts[tOp[e]] = $o.Se.Opts[tOp[e]]; }
	}
	var clw = (P.clw) ? P.clw : 'oSeForm_wrap';
	var zIndex = (P.zIndex) ? P.zIndex : '';
	var wrap = $1.t('div',{'class':clw,style:'position:relative;'});
	var resp = $1.t('div',{style:'position:absolute; width:90%; '});
	var disp = (P.inLine)?'':'display:inline-block';//N
	var wrap2 = $1.t('div',{style:disp});
	var le = $js.length(opts);
	if(le>0){
		var selObj = $1.T.divSelect({sel:{'class':'jsFields objType',name:'objType',s}, classBtn:'boxi1', opts:opts, noBlank:'Y', clsFa:'',classWrap:'input', func:function(){ chan(inp.value); }
	});
	if(le==1){ selObj.style.display = 'none'; }
		wrap2.appendChild(selObj);
	}
	else if(P.oSelbj){
		wrap2.style.display = '';
		var selObj = $1.t('input',{type:'hidden','class':'jsFields objType',name:'objType',value:P.objSel},wrap2);
	}
	var sty = (P.inpStyle)?P.inpStyle:'';
	var inp =  $1.t('input',{type:'text','class':jsFields+' jsFields iBg iBg_search2',name:'textSearch',style:'height:2rem; '+sty,placeholder:'Escriba 3 caracteres para buscar...',value:P.textValue});
	inp.O = {};
	if(P.vPost){ inp.O.vPost = P.vPost;
		$0s.console.put(P.vPost);
	}
	var interv;
	inp.onkeypress = function(){ clearInterval(interv); }
	inp.onchange = inp.onkeyup = function(evt){ var Th = this;
		clearInterval(interv);
		if(Th.value==''){ inp.O.vPost = ''; }
		interv = setInterval(function(){ chan(Th.value,evt); },900);
	}
	function chan(val,evt){
		clearInterval(interv);
		wrap.style.zIndex = '';
		if(evt && (evt == '[object Event]')){  }
		else if(val ==''){ $1.clear(resp); }
		else if(val !='' && val.length>=1){
			wrap.style.zIndex = zIndex;
			var vPost = $1.G.inputs(wrap,'jsFields');
			if(P.fields){ vPost +='&fields='+P.fields; }
			$Api.get({f:'GET '+$o.api2, inputs:vPost, loade:resp,
			func:function(Jq){
				var respWrap = $1.t('div',{style:'background-color:#FFF; padding:0.25rem; border:0.0625rem solid #CCC; border-radius:0.25rem;'},resp);
				if(Jq.errNo){ $Api.resp(respWrap,Jq); }
				else{
					var ul = $1.t('ul',{'class':'ulList'});
					for(var i in Jq.DATA){
						var L = Jq.DATA[i];
						var liTag = $1.t('li');
						var li = $1.t('button',{'class':'fa faBtn',textNode:L.lineText}); liTag.appendChild(li);
						if(L.cardCode){ li.appendChild($1.t('span',{textNode:' ('+L.cardCode+')',style:'font-size:0.75rem;'})); }
						li.L = L;
						li.onclick = function(){ $1.clear(resp);
							wrap.style.zIndex = '';
							var L2 = this.L;
							var objType = $1.q('.objType',wrap).value;
							var objRef = L2.objRef;
							inp.value = '';
							if(P.putResp){
								if(P.putResp===true){ P.putResp = ''; }
								//es eliminado unsey(_0)
								inp.O.vPost = P.putResp+'&_O[objType]='+L2.objType+'&_O[objRef]='+L2.objRef+'&_O[objText]='+L2.lineText;
								inp.value = L2.lineText;
							}
							else if(P.putDefine){
								inp.O.vPost = P.putDefine.id+'='+encodeURIComponent(L2.objRef);//cardId=&cardName=
								inp.O.vPost += (P.putDefine.t1) ? '&'+P.putDefine.t1+'='+encodeURIComponent(L2.objText) : '';
								inp.O.vPost += (P.putDefine.t2) ? '&'+P.putDefine.t2+'='+encodeURIComponent(L2[P.putDefine.t2]) : '';
								if(P.liClick){ P.liClick(L2); }
								if(P.putDefine.func){ P.putDefine.func(inp.O);}
								else{ inp.value = L2.objText; }
							}
							else if(P.putFunc){
								var vPost = vPostBase+'&objType='+objType+'&objRef='+objRef;
								$o.Src.put({vPost:vPost, putFunc:P.putFunc})
							}
							if(P.putOn){//pare:a leer, cls
								var cls=(P.putOn.cls)?P.putOn.cls:'.__fie2Change';
								for(var f in L2){
									$js.fieChange(cls+'_'+f,L2[f],P.putOn.pare);
								}
							}
							$0s.console.put(inp.O);
						}
						ul.appendChild(liTag);
					}
					respWrap.appendChild(ul);
				}
			}
			});
		}
	}
	wrap2.appendChild(inp);
	wrap.appendChild(wrap2);
	wrap.appendChild(resp);
	return wrap;
}
}
,
Src:{
get:function(P,P2){
	var wrap = $1.t('div');
	var noLoad = (P2 == 'noLoad') ? true : false;
	var wrap = $1.t('div',{'class':'winMenuInLine winoSrc',style:'display:none;'});
	if(noLoad){ return wrap; }
	var wrap = $1.q('.winoSrc');
	var wrapExist = $1.q('.winoSrc_open');
	if(!P.reload && wrapExist && wrapExist.tagName){ return true; }
	function reLoad(){ $o.Src.get(P); }
	wrap.classList.add('winoSrc_open');
	var vPost = 'objpType='+P.objpType+'&objpRef='+P.objpRef;
	vPost += (P.relations)?'&relations='+P.relations : '';
	P.putFunc = function(){ $o.Src.get(P); }
	var wrapList = $1.t('div',{'class':'oSrc_wrapList'});
	if(!$1.q('.oSeForm_wrap')){
		var se = $o.Se.form(P);
		wrap.appendChild($1.t('div',{'class':'head1',textNode:'Relacionar Tarjeta con...'}));
		wrap.appendChild(se); wrap.appendChild(wrapList);
	}
	wrapList = $1.q('.oSrc_wrapList')
	$Api.get({f:'GET '+$o.api+'Src', inputs:vPost, loade:wrapList, func:function(Jq2){
		if(Jq2.errNo && Jq2.errNo != 2){ $Api.resp(wrapList,Jq2); }
		else{
			var tb = $1.T.table(['#','-','','Realizado','']);
			var tBody = $1.t('tbody');
			var n = 1;
			for(var i in Jq2.DATA){ var L2 = Jq2.DATA[i];
				var tr = $1.t('tr');
				tr.appendChild($1.t('td',n)); n++;
				var td = $1.t('td')
				td.appendChild($1.t('span',{'class':'iBg obTy_'+L2.objType}));
				tr.appendChild(td);
				tr.appendChild($1.t('td',L2.objText));
				tr.appendChild($1.t('td','Por '+L2.userName+', '+$2d.f(L2.dateC,'mmm d H:iam')));
				var td = $1.t('td');
				var btn = $1.t('button',{'class':'iBg iBg_trash'}); btn.osrcId = L2.osrcId;
				btn.onclick = function(){ This = this;
					$Api.get({f:'DELETE '+$o.api+'Src',inputs:'osrcId='+this.osrcId, func:function(){ $1.delet(This.parentNode.parentNode); }});
				}
				td.appendChild(btn); tr.appendChild(td);
				tBody.appendChild(tr);
			}
			tb.appendChild(tBody);
			wrapList.appendChild(tb);
		}
	}});
	return wrap;
}
,
titleGet:function(P,pare){
	var wrap = $1.t('div');
	var vPost = 'objpType='+P.objpType+'&objpRef='+P.objpRef;
	$Api.get({file:'GET '+$o.api+'Src', inputs:vPost, loade:wrap, func:function(Jq2){
		if(Jq2.errNo && Jq2.errNo != 2){ $Api.resp(wrap,Jq2); }
		else{
			for(var i in Jq2.DATA){ var L2 = Jq2.DATA[i];
				var li = $1.t('div',{style:'width:10rem;'});
				li.appendChild($1.t('span',{'class':'iBg obTy_'+L2.objType}));
				li.appendChild($1.t('span',L2.objText));
				wrap.appendChild(li);
			}
		}
	}});
	$1.Win.title(wrap,{pare:pare});
}
,
put:function(P){
	//var vPost = vPostBase+'&objType='+objType+'&objRef='+objRef;
	$Api.get({file:'PUT '+$o.api+'Src', inputs:P.vPost, func:P.putFunc});
}
}
};

$1.blurClose = function(topC,a){
	var i = '';
	i = setTimeout(function(){ clearTimeout(i);
	document.body.onclick = function(ev){
		topP(ev.target,topC,a);
	}
	},200);
	function topP(t,p,a){
		var node = t.parentNode;
		if(node==p){ }
		else if(node == document.body){
			if(a=='display'){ p.style.display='none';} else{ $1.delet(p); }
			document.body.onclick = false;
		}
		else{ topP(node,p,a); }
	}
}

$1.dTag_g={
_g:function(k){
	if($1.dTag_g[k]){ return $1.dTag_g[k]; }
	return [];
}
};
$1.dTag_g['fdpId']=$Tb.gfiOfdp;
$1.dTag_g['slpId']=$Tb.oslp;
$1.dTag=function(L2,L,nD){
	var nD=(nD)?nD:L2;
	var kF=L2.k;
	var kTxt=(L2.kTxt)?L2.kTxt:'textNode'; /* v */
	var kTy=(L2.kTy)?L2.kTy:kF;
	var txt=L[kF];
	if(L2.funcText){ nD[kTxt]= L2.funcText(L); }
	else if(L2.fText){ nD[kTxt]=L2.fText(L); }
	else if(L2._gTb){nD[kTxt]=_g(txt,$Tb[L2._gTb]); }
	else if(L2._g){
		if(typeof(L2._g)=='string'){ L2._g=eval(L2._g); }
		nD[kTxt]=_g(txt,L2._g);
	}
	else if(L2._gV){
		if(typeof(L2._gV)=='string'){ L2._gV=eval(L2._gV); }
		nD[kTxt]=_gO(txt,L2._gV,'v');
	}
	else if(L2.gK){
		nD[kTxt]=_g(txt,$1.dTag_g._g(kF));
	}
	if(kTy=='price'){ nD[kTxt]=$Str.money(txt); }
	else if(kTy=='quantity'){ nD[kTxt]=txt*1;; }
	else if(kTy=='itemCode'){ nD[kTxt]=Itm.Txt.code(L); }
	else if(kTy=='itemName'){ nD[kTxt]=Itm.Txt.name(L); }
	else if(kTy=='udm'){ nD[kTxt]=_g(txt,Udm.O); }
	else{ nD[kTxt]=txt; }
	return nD;
}

$0s.Report = {
api:'/sys/report/',
m:1,
get:function(){
	$Api.get({f:$0s.Report.api+'byForm', inputs:'',
	func:function(Jq){ $0s.Report.list(Jq,null); }
	});
},
list:function(O,k,wrapList){
	var cont=(wrapList)?wrapList:$M.Ht.cont;
	if(O.errNo){ $Api.resp(cont,O); return true; }
	if(O.L){ O=O.L; }
	if(!cont.classList.contains('authsMain_v02')){ cont.classList.add('authsMain_v02'); }
	var ul = $1.t('ul');
	var m = $0s.Report.m; $0s.Report.m++;
	if(k && O[k]){ O = O[k].opts; }
	for(var i in O){ var T = O[i];
		var li = $1.t('li');
		if(T.opts){
			T.text = (T.text)?T.text: i;//dpto
			li.classList.add('opts');
			var span = $1.t('div',{textNode:T.text,style:'padding:0.25rem; margin-left:-0.375rem;','class':'nextUl iBgBlock iBg_folderplus'});
			span.onclick = function(){
				var ne = $1.q('.nextUl_m'+m,this.parentNode,'all');
				for(var c=0; c<ne.length; c++){
					var tdisp = ne[c].style.display;
					if(tdisp == 'block'){ this.classList.replace('iBg_folderminus','iBg_folderplus'); ne[c].style.display = 'none'; }
					else{ this.classList.replace('iBg_folderplus','iBg_folderminus'); ne[c].style.display = 'block'; }
				}
			}
			li.appendChild(span);
			var wList = $1.t('div',{'class':'nextUl_m'+m,style:'display:none;'});
			$0s.Report.list(T.opts,false,wList);
			li.appendChild(wList);
		}
		else{
			li.appendChild($1.t('span',{textNode:T.text+' ('+T.reportId+')','class':'iBgBlock iBg_arrowGo',title:'Id :'+T.reportId})); li.T = T;
			li.onclick = function(){
				if(this.T.type=='data'){ $0s.DT.form(this.T) }
				else{ $0s.Report.form(this.T); }
			}
		}
		ul.appendChild(li);
	}
	cont.appendChild(ul);
},
form:function(D){
	if(D.repVers==2){
		$Report.form(D);
		return true;
	}
	var wrap = $1.t('div');
	var wrapList = $1.t('div',0,wrap);
	var n = 0;
	var GETq = $0s.Report.api+'byForm.query';
	$Api.get({f:$0s.Report.api+'byForm.form', inputs:'reportId='+D.reportId, loade:wrapList,
	func:function(Jqr){
		if(Jqr.type == 'req'){ GETq = '/sr/'+Jqr.qu; }
		if(Jqr.descrip !=''){ $1.t('div',{'class':'pre',textNode:Jqr.descrip,style:'padding:0.25rem; border:0.0625rem solid #CCC;'},wrapList); }
		var Jq = Jqr.qwh;
		var nL = 0;
		var wFilt=$1.t('div',{id:'_sysReportFilt'});
		var wFilt=$1.T.fieldset(wFilt); wrapList.appendChild(wFilt);
		for(var i in Jq){//draq form
			var iL=Jq[i];
			var divLineT = (lastType == iL.type && lastType == 'date' && nL%3!=0)?true:false;
			divLineT = (iL.wxn && !iL.br)?true:divLineT;
			var wxn = (Jq[i].type == 'date' || Jq[i].wxn)?'wrapxauto':'wrapx1';
			wxn = (Jq[i].wxn && Jq[i].wxn != '')?Jq[i].wxn:wxn;
			wxn2 = (Jq[i].wxn && Jq[i].wxn != '')?Jq[i].wxn:wxn;
			var placeholder=(Jq[i].placeholder)?Jq[i].placeholder:'';
			nL++;
			var tConf = {};
			var req=(iL.req=='Y')?'Y':false;
			if(iL.type == 'select'){
				var opts = iL.opts;
				opts = (typeof(opts) == 'string')?eval(opts):opts;
				tConf = {wxn:wxn2,req:req,L:{textNode:iL.text}, I:{tag:'select',sel:{'class':'jsFields',name:'FIE['+n+']['+iL.name+']'},opts:opts,noBlank:iL.noBlank} };
			}
			else{
				tConf = {wxn:wxn2,req:req,L:{textNode:Jq[i].text},I:{tag:'input',type:Jq[i].type,'class':'jsFields',name:'FIE['+n+']['+Jq[i].name+']',placeholder:placeholder}};
			}
			if(divLine && divLineT){ divLine.appendChild($1.T.divLinewx(tConf)); }
			else{
				tConf.divLine = true;
				var divLine = $1.T.divLinewx(tConf);
			}
			if(divLine || !divLineT){ wFilt.appendChild(divLine);}
			n++;
			var lastType = Jq[i].type;
		}
		var resp = $1.t('div');
		var iSend = $1.T.btnSend({textNode:'Generar Reporte'},{f:GETq, getInputs:function(){ return 'reportId='+D.reportId+'&'+$1.G.inputs(wrap); }, loade:resp, func:function(Jq2){
			if(Jq2.errNo){ $Api.resp(resp,Jq2); }
			else{
				var fileName = (Jq2.fileName)?Jq2.fileName:'Archivo de Reporte';
				var Fmt = {};
				var TDS=(Jq2.TDS)?Jq2.TDS:{};
				var TRS=(Jq2.TRS)?Jq2.TRS:{};
				var jsConf=Jq2.jsConf;
				if(typeof(jsConf)!='object'){ jsConf={}; }
				var tOpts=(jsConf && jsConf.Opts)?jsConf.Opts:{};
				/* sustituir textos si */
				var textIs=(jsConf && jsConf.textIs)?jsConf.textIs:{};
				var tbFie=[]; n=0;
				for(var f in Jq2.FIE){ /* docTotal_number=Total Doc. */
					var tVal=Jq2.FIE[f];
					var format = 'plain'; var fK = ''; var f2 = f;
					var serieType='nulle';
					if(f.match(/\_\$$/)){ format = '$'; var f2 = f.replace(/\_\$$/,''); }
					else if(f.match(/\_number$/)){ format = 'number'; var f2 = f.replace(/\_number$/,''); }
					Jq2.FIE[f] = f2;
					Fmt[f] = {k:f2, v:format, t:fK, fr:f};
					var tbT={textNode:f2};
					if(jsConf && jsConf.FIEkv ==='Y'){ tbT={textNode:tVal}; }
					tbFie.push(tbT);
				}
				var tb = $1.T.table(tbFie);
				var tr0 = $1.q('thead tr',tb);
				tr0.insertBefore($1.t('td','#'),tr0.childNodes[0]);
				var tBody = $1.t('tbody');
				var n=1;
				if(Jq2.kOrder && Jq2.FIE[Jq2.kOrder]){
					Jq2.L=$js.sortNum(Jq2.L,{k:Jq2.kOrder});
				}
				for(var e in Jq2.L){ var Ld=Jq2.L[e];
					var tr = $1.t('tr');
					tr.appendChild($1.t('td',n)); n++;
					var tdn=2; var tdr=1; //col real report
					for(var f in Jq2.FIE){
						var tF = Fmt[f];
						var te = (Ld[tF.fr])?Ld[tF.fr]:'';
						te = (Ld[tF.k])?Ld[tF.k]:te;
						if((opts=tOpts['col'+tdr])){
							opts = (typeof(opts) == 'string')?eval(opts):{};
							if(opts && opts[0] && opts[0].k){ te=_g(te,opts); }
							else{ te=opts[te]; }
						}
						if(textIs){
							if(textIs['undefined'] && te==undefined){ te=textIs['undefined']; }
							else if(textIs['col'+tdr]){ te=textIs['col'+tdr]; }
						}
						if(jsConf && jsConf.serieType && jsConf.serieType['col'+tdr]){
							var td=$1.t('td',0,tr);
							$Doc.href(jsConf.serieType['col'+tdr],{docEntry:te},{pare:td,format:'id'});
						}
						else{
							if(te!='' && tF.v!='plain'){
								switch(tF.v){
									case '$': te=$Str.money(te); break;
									case 'number': te=te*1; break;
								}
							}
							var td=$1.t('td',{textNode:te,'class':tbSum.tbColNums,'tbColNum':tdn},tr);;
						}
						tdn++; tdr++;
					}
					tBody.appendChild(tr);
				}
				tb.appendChild(tBody);
				var tbC=$1.t('div'); tbC.appendChild(tb);
				tb = $1.T.tbExport(tb,{print:true,printWrap:tbC,fileName:fileName});
				resp.appendChild(tb);
				if($Soc && $Soc.softFrom){
					$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; padding:1rem 0;'},tbC);
				}
			}
		}});
		wrap.appendChild(iSend);
		wrap.appendChild(resp);
	}
	});
	$1.Win.open(wrap,{winTitle:D.text,onBody:true,winId:'reportWinSys'});
}
}

$0s.DT = {
form:function(D){
	var wrap = $1.t('div');
	$1.t('h5',{'class':'head1',textNode:D.text},wrap);
	var wrapList = $1.t('div');
	if(D.descrip){
		wrap.appendChild($1.t('div',{'class':'textarea',style:'min-height:0;',textNode:D.descrip}));
	}
	var formu = $1.t('form',{name:'_admsDataTransferForm','style':'padding:0.025rem 0;'});
	var dFile = $1.t('div');
	var fid = 'fileIdw_'+$js.srand();
	var iFile = $1.t('input',{type:'file', id:fid, 'class':'psUFile psUFile_x32 _5f_fileWrap', name:'file'});
	var iLabel = $1.t('label',{'for':fid,textNode:'Selección de Archivo'});
	formu.appendChild(iFile); formu.appendChild(iLabel);
	wrap.appendChild(formu);
	$1.t('div',{'class':'_wrapResp'},wrap);
	var btnSend = $1.T.btnFa({textNode:' Definir Plantilla',faBtn:'fa_fileUpd',Conf:{text:'Toda la información será ingresada a la base de datos y no se podrá revertir el proceso'}, func:function(T){
		$0s.FU.x(iFile,{btnDisabled:T,url:$y.apiURI+'/api/dtranster/'+D.qu+'?___ocardtooken='+$0s.stor('ocardtooken')},wrap); iFile.value = null;
	}});
	wrap.appendChild(btnSend);
	wrap.appendChild($1.t('clear')); wrap.appendChild($1.t('clear'));
	wrap.appendChild(wrapList);
	$0s.DT.readStr(D,wrapList);
	$1.Win.open(wrap,{winTitle:'Data Transfer v.2',onBody:true,winId:'_0s__datatransferv1',winSize:'medium'});
},
readStr:function(P,wList){
	$1.clear(wList);
	var GETq = $0s.Report.api+'DT.load';
	$Api.get({f:$0s.Report.api+'DT.readStr', inputs:'reportId='+P.reportId, loade:wList,
	func:function(Jqr){
	var tb = $1.t('table',{'class':'table_zh'});
	var tHead = $1.t('thead',0,tb);
	var tr0 = $1.t('tr',0,tHead);
	var tBody = $1.t('tbody',0,tb);
	var tr1 = $1.t('tr',0,tBody);
	for(var k in Jqr.qwh.Fie){ L = Jqr.qwh.Fie[k];//k=fie, title, descrip
		L.d = (L.d)?L.d:'';
		$1.t('td',{textNode:L.t},tr0);
		$1.t('td',{textNode:L.d},tr1);
	}
	tb = $1.T.tbExport(tb,{fileName:'plantilla.xls',ext:'txt'});
	wList.appendChild(tb);
	}});
}
}


$0s.Vs = {
payMethod:{cash:'Efectivo',card:'Tarjeta',transfer:'Transferencia',check:'Cheque'}
}
