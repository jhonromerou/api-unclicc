$i['itmBuyFactor']={t:'Se usa para convertir las cantidades al ingresar mercancía, aplica para <b>remisión de compra</b> y <b>facturas de compra</b>.'};
$i['itmSellFactor']={t:'<b>NO ACTIVO TODAVIA</b> Se usa para convertir las cantidades al ingresar mercancía, aplica para <b>remisión de compra</b> y <b>facturas de compra</b>.'};
/* wma */

$i['pdpPorPlanificar']={t:'Cantidad que se debe planificar a comprar o producir para satisfacer la necesidad.'};
$i['pdpNecesidad']={t:'La necesidad representa la cantidad que debe ingresar a la bodega para satisfacer los pedidos.'};
$i['pdpAcumulado']={t:'El acumulado es la cantidad total que quedaría en una bodega teniendo en cuenta la siguiente operación: <br/> Acumulado = Pendientes - Stock - Planificado - En Proceso.'};