$LA = {};
$LA.O = {};
//.v = verb (create,update,add,delete,archived)
//.f = fieldName
//.a = action

$LA.g = function(D,D2){
	//D2 == verificar objType para en esta actividad || aquí
	var t = $1.t('div',{title:'id: '+D.id+', o{'+D.objType+','+D.objRef+'}, t{'+D.targetType+','+D.targetRef+'}'});
	if(D2.nodeBeg){
		t.appendChild(D2.nodeBeg);
	}
	t.appendChild($1.t('textNode',D.userName+', '));
	var o = D.objType;//o = workBoardList
	var k1 = ''; //=f or v (verb)
	var f_k = '';//= f.actStatus
	var f_v = '';// f.actStatus[onHold]
	if(!$LA.O[o]){ return $1.t('div','LA.'+o+' Not Found...'); }
	if(D.ref1Type == 'fieldName'){
		var k1 = 'f';
		var f_k = D.ref1;
		var f_v = D.ref1Memo;
		if(D.ref1 == 'userAssg' && D.ref1Memo == ''){
			f_k = 'userAssgDelete';
		}
		var o2 = ($LA.O[o][k1] && $LA.O[o][k1][f_k]) ? $LA.O[o][k1][f_k] : {__plain:'NoDef'};
		if(typeof(o2) == 'string'){ o2 = eval(o2); }
		var vText = (o2 && o2.__plain) ? f_v : o2[f_v];
		t.appendChild($1.t('span',o2.__t));
			t.appendChild($1.t('span',' '));
		t.appendChild($1.t('b',vText));
	}
	else{
		var k1 = 'v';
		var f_k = D.verb;
		t.appendChild($1.t('span',$LA.O[o][k1][f_k]));
	}
	var Otar = {objType:D.targetType, objRef:D.targetRef};
	var noSameObj = (D.objType != D2.objType && D.targetType != D2.objType);
	var a_de = (D.verb == 'delete') ? $LA.on.de : $LA.on.a;
	if(D.objType == 'wboList'){
		if(D.targetType == 'workBoard'){
			t.appendChild($1.t('span',$LA.O.on.workBoard));
			t.appendChild($1.t('b',D.wboName));
		}
	}
	else if(D.objType == 'comment'){
		if(noSameObj){
			t.appendChild($1.t('span',$LA.on.en));
			if(D.targetType == 'activity'){
				var act = $1.t('b',{textNode:D.actAsunt,'class':'cPointer'});
				act.onclick = function(){ $o.open(Otar); }
				t.appendChild(act);
			}
		}
		t.appendChild($1.t('div',D.comment));
	}
	else if(D.objType == 'fileUpd'){
		if(D.fi_fileName == ''){
			t.appendChild($1.t('a',{textNode:D.lineMemo}));
		}
		else{ t.appendChild($1.t('a',{href:D.fi_url,textNode:D.fi_fileName})); }
		if(noSameObj){
			t.appendChild($1.t('span',a_de));
			if(D.targetType == 'activity'){
				var act = $1.t('b',{textNode:D.actAsunt,'class':'cPointer'});
				act.onclick = function(){ $o.open(Otar); }
				t.appendChild(act);
			}
		}
	}
	else if(D.objType == 'activity'){
		if(noSameObj){
			t.appendChild($1.t('span',', '));
			var act = $1.t('b',{textNode:D.actAsunt,'class':'cPointer'});
			act.onclick = function(){ $o.open(D); }
			t.appendChild(act);
		}
		if(D.targetType == 'wboList'){ //en Lista..
			t.appendChild($1.t('span',a_de));
			var wbo = $1.t('b',{textNode:D.listName});
			t.appendChild(wbo);
		}
	}
	//t.appendChild($1.t('span',D2.objType+'='+D2.objRef+', '+D.targetType+'='+D.targetRef));
	
	return t;
}

/*Dynamic LAn */
$LA.on = {
a:' a ',
en:' en ',
de:' de ',
workBoard:' en ',
wboList:' a '
}

$LA.O.activity = {
v:{
create:'Creó Actividad',
add:'Añadio ',
delete:'Eliminó '
}
,
f:{
archived:{
__t:'Marco como ',
Y:'Archivada', N:'Desarchivada'
},
actType:"$js.push({__t:\'actualizó Tipo a \'},$5a.Vs.actType);",
actStatus:"$js.push({__t:\'actualizó Estado a \'},$5a.Vs.actStatus);",
actPriority:"$js.push({__t:\'actualizó Prioridad a \'},$5a.Vs.actPriority);",
userAssg:{__t:'Asignó Responsable a',__plain:true},
userAssgDelete:{__t:'Eliminó el Responsable',__plain:true},
wboList:{__t:'Movió a otra lista'},
doDate:{__t:'Actualizó fecha de realización a ',__plain:true},
}
,
a:{
archived:'Archivó la actividad',// objText....
}
}

$LA.O.workBoard = {
v:{//verb
create:'Tablero creado',
update:'Tablero Actualizado'
}
,
f:{//ref1Type = fieldName
wboPriv:{// = ref1
__t:'Hizo el tablero ',
owner:'privado.',// = ref1Text
members:'visible para los miembros.',
shared:'visible para los usuarios.'
},
}
}

$LA.O.wboList = {
v:{
create:'Lista Creada',
update:'Lista Actualizada'
}
,
f:{
}
}

$LA.O.comment = {
v:{
create:'Comentó',
add:'-'
}
,
f:{
}
}

$LA.O.fileUpd = {
v:{
create:'subió ',
add:'-',
delete:'Eliminó el archivo '
}
,
f:{
}
}

$LA.$5n = {};
$LA.$5n.O = {//akey
activityOnwboList:{
a1:'Añadio una', a2:'actividad', a3:'a tu', a4:'tablero'
},
activityAssg:{a1:'te asigno a una', a2:'actividad'},
activityCompleted:{a1:'marcó como completada la', a2:'actividad'},
activityArchived:{a1:'archivó la', a2:'actividad'},
commentOnActivity:{
a1:'comentó', a2:'en una actividad'
},
fileUpdOnActivity:{
a1:'subió un archivo', a2:'en una actividad'
}
}