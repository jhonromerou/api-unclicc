
$V.bar2={1:'SGA',2:'SAP',3:'Siigo'};

$Tb.pymntGr={prepa:'Prepago',c30:'Crédito 30 días',c45:'Crédito 45 días',c60:'Crédito 60 días'};

$V.ordTypePE['BR']='Baja Rotación';
$V.ordTypePE['PO']='Producto Obsoleto';
$V.ordTypePE['LB']='Libre';

$Tb.oitp_opts={
'1':{tag:'select',opts:{I:'Inyección',C:'Cementado',V:'Vulcanizado',PVC:'PVC'}}
}
$V.cvtCondicGen='Tiempo de entrega: 25-30 días hábiles.\nCondiciones Comerciales: 15% de descuento autorizado\nGarantia: 6 meses en condiciones normales de uso.';
$V.cvtDefPayGrText='Crédito a 30 días';
$Mdl.GeDoc_oTy=[{folId:'opvt',folName:'Pedidos de Venta'},{folId:'ocvt',folName:'Cotizaciónes de Venta'},{folId:'opdp',folName:'Plan de Producción'}];


$V.ingDocClass['dota']='Dotación';
$V.ingDocClass['mnfab']='Mercancía No Fabricada';
$V.egrDocClass['dota']='Dotación';
$V.egrDocClass['mnfab']='Mercancía No Fabricada';
$V.egrDocClass['obseq']='Obsequios';
$V.egrDocClass['imperf']='Imperfectos';

$M.li['sellRep.scheduleWeek']={t:'Programación de Entregas', func:function(){ $M.Ht.ini({fieldset:'Y',func_filt:'sellRep.scheduleWeek', func_cont:null}); }};

$oB.pus('wmaOdp',$Opts,[
{ico:'fa fa_pencil',textNode:' Definir en SGA 1.0',func:function(T){ console.log(T); Wma3.Odp.defineOnSga2(T.P); }}
]);

$M.li['exp.emSap']={t:'Cargue a SAP', func:function(){ 
	$M.Ht.ini({topCont:$1.t('p',{textNode:'Genera la plantilla para cargar cantidades EM a SAP.'}), fieldset:true, func_filt:function(filt){
		var jsV='jsFiltVars';
		var divL=$1.T.divL({divLine:1, wxn:'wrapx8',req:'Y',L:{textNode:'Grupo de Códigos'},I:{tag:'select',sel:{'class':jsV,name:'grTypeId'},opts:$V.bar2}},filt);
		$1.T.divL({wxn:'wrapx8',req:'Y',L:{textNode:'No. Documento'},I:{tag:'input',type:'text',inputmode:'numeric','class':jsV,name:'docEntry'}},divL)
		var btn=$1.T.btnSend({textNode:'Obtener Plantilla', func:getPlantilla});
		filt.appendChild(btn);
	}, func_cont:null }); function getPlantilla(){
		var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Exp.a+'emSap', loade:cont, errWrap:cont, inputs:$1.G.filter(), func:function(Jr){
			var tb=$1.T.table(['RecordKey','lineNum','itemcode','zDescription','quantity','warehousecode','Currency','unitprice','linetotal','accountcode']);
			var trH=$1.q('tr',tb);
			tb.appendChild(trH.cloneNode(1));
			var tBody=$1.t('tbody',0,tb);
			var n=1;
			for(var i in Jr.L){ L=Jr.L[i]; 
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:1},tr);
				$1.t('td',{textNode:n},tr); n++;
				$1.t('td',{textNode:L.barCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
				$1.t('td',{textNode:L.whsCode},tr);
				$1.t('td',{textNode:'$'},tr);
				$1.t('td',{textNode:L.costo},tr);
				$1.t('td',{textNode:''},tr);
				$1.t('td',{textNode:L.accountCode},tr);
			}
			tb=$1.T.tbExport(tb,{fileName:'Documento Inventario (EM)',ext:'txt'});
			cont.appendChild(tb);
		}});
	}
}};

$Tpt.T['gvtCvt_template']=function(cont,P){
	var Jr=P.Jr;
		var Ls=[
		{t:'Estado',v:_g(Jr.docStatus,$V.docStatus)},{middleInfo:'Y'},{logoRight:'Y'},{tag:'docDate'},{t:'Vencimiento',v:Jr.dueDate},
			{tag:'cliente',cs:5},{t:'Resp.',v:_g(Jr.slpId,$Tb.oslp),ln:1}
		];
		var div=$1.t('pre');
			$1.t('b',{textNode:'Persona: '},div);
			$1.t('span',{textNode:P.Jr.prsCnt+'\t\t'},div);
			$1.t('b',{textNode:'Teléfono: '},div);
			$1.t('span',{textNode:P.Jr.phone+"\n"},div);
			$1.t('b',{textNode:'Correo: '},div);
			$1.t('span',{textNode:P.Jr.email+"\n"},div);
			$1.t('b',{textNode:'Dirección: '},div);
			$1.t('span',{textNode:P.Jr.address},div);
		Ls.push({v:div,cs:6});
		Ls.push({t:'Condic. Pago',v:P.Jr.payGrText+'-',ln:1});
		var td=$1.t('div'); $1.t('b',{textNode:'Detalles'},td); $1.t('pre',{textNode:Jr.lineMemo,'class':'pre100'},td);;
		var td2=$1.t('div'); $1.t('b',{textNode:'Condiciones'},td2); $1.t('pre',{textNode:Jr.condicGen,'class':'pre100'},td2);
		Ls.push({v:td,cs:4}); Ls.push({v:td2,cs:4,ln:1});
		var va='vertical-align:middle';
		var Trs=[]; var ni=0;
		for(var i in Jr.L){ var L=Jr.L[i]; Trs[ni]=[];
			Trs[ni].push({style:'width:8rem;',node:$1.t('img',{src:Itm.Txt.imgSrc(L),ttitle:L.src1,style:'max-width:8rem;'})});
			Trs[ni].push({textNode:Itm.Txt.code(L),style:va});
			Trs[ni].push({textNode:Itm.Txt.name(L),style:va});
			Trs[ni].push({textNode:$Str.money(L.price),style:va});
			Trs[ni].push({textNode:L.quantity*1,style:va,'class':tbCal.trQty});
			Trs[ni].push({textNode:L.disc*1+'%',style:va});
			Trs[ni].push({textNode:$Str.money(L.priceLine),style:va});
			Trs[ni].push({style:va,'class':'pre',node:$1.t('div',{textNode:L.lineMemo})});
			ni++;
		}
	$Tpt.draw(cont,{D:P.Jr,serieType:'ocvt',print:'Y',
		Ls:Ls,
		middleCont:$1.t('div',{textNode:'Precios antes de IVA',style:'font-size:0.75rem; text-align:center;'}),softFrom:'Y',
		fieldset:'Artículos Cotizados',
		Tb:['Imagen Art.',{textNode:'Código',style:'width:3rem;'},{textNode:'Descripción'},{textNode:'Precio',style:'width:5rem;'},{textNode:'Cant',style:'width:5rem;'},'Desc.',{textNode:'Total',style:'width:5rem;'},{textNode:'Detalles',style:'width:14rem;'}],
		Trs:Trs
	});
}

Wma3.Odp.defineOnSga2=function(P){
	var wrap=$1.t('div');
	var jsF='jsFields';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:'Distribuir en',subText:'Unidades',I:{tag:'input',type:'number',inputmode:'numeric',min:1,max:30,'class':jsF,name:'var1'}},wrap);
	$1.T.divL({wxn:'wrapx2',L:'Si resto menor a',subText:'Se añade a fila 1',I:{tag:'input',type:'number',inputmode:'numeric',min:1,max:30,'class':jsF,name:'var2'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:'Código Troq. Cuero',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'var3'}},wrap);
	$1.T.divL({wxn:'wrapx2',L:'Código Troq. Forradura',req:'Y',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'var4'}},divL);
	var resp=$1.t('div',0,wrap);
	$Api.send({PUT:Api.Odp.a+'a.defineOnSga2',getInputs:function(){ return 'docEntry='+P.docEntry+'&'+$1.G.inputs(wrap); }, loade:resp, func:function(Jr){
		$ps_DB.response(resp,Jr);
	}},wrap);
	$1.Win.open(wrap,{winTitle:'Definir Orden #'+P.docEntry+' en SGA 1.0',winSize:'medium',onBody:1});
}

