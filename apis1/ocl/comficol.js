Api.Dsp='/1c/comficol1/';
$Xls_extDef='xlsx';
/* Reset Mlds */
$M.liReset();
/* end reset */
$M.iniSet={
nty:'N',help:'N',menu:'L',
mliDel:['sysreports']};

$M.sAdd([
{MLis:['dspDse','dspRfiDse','dspTax.usura','dspTax.dtf']}
]);

$M.liAdd('dspComf',[
{k:'dspDse',t:'Sentencias',kau:'dspDse',func:function(){ $M.Ht.ini({f:'dspDse',btnGo:'dspDse.form',gyp:function(){ Dsp.Dse.get({}); } }); } },
{k:'dspDse.form',t:'Sentencia (Form)',kau:'dspDse.write',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Dse.form({}); } }); } },
{k:'dspDseSor.form',t:'Propuesta de Venta (Form)',kau:'dspDseSor.write',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Dse.sorForm({}); } }); } },
{k:'dspDseSor.view',noTitle:'Y',t:'Propuesta de Venta (Doc)',kau:'dspDseSor',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Dse.sorView({}); } }); } },
{k:'dspDse.view',t:'Sentencia',kau:'dspDse',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Dse.view({}); } }); } },
{k:'dspTax.usura',t:'Definir Usura',kau:'dspDse.usura',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Tax.dtf({iType:'U'}); } }); } },
{k:'dspTax.dtf',t:'Definir DTF',kau:'dspDse.dtf',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.Tax.dtf({iType:'DTF'}); } }); } },
{k:'dspRfiDse',t:'Documentos Requeridos Sentencia',kau:'dspRfiDse',func:function(){ $M.Ht.ini({func_cont:function(){ Dsp.RfiDse(); } }); } },
]);

_Fi['dspDse']=function(wrap){
	var jsF='jsFiltVars';
	var Bal=[
	{wxn:'wrapx8',L:'Radicado',I:{tag:'input',type:'text',name:'A.noRadicado','class':jsF}},
	{wxn:'wrapx8',L:'Intereses Hasta',I:{tag:'input',type:'date',name:'A.balDate(E_menIgual)','class':jsF}},
	{wxn:'wrapx8',L:'Demandante',I:{tag:'input',type:'text',name:'A.demandante(E_like3)','class':jsF}},
	{wxn:'wrapx8',L:'Demandado',I:{tag:'input',type:'text',name:'A.demandado(E_like3)','class':jsF}}
	];
	$Doc.filtForm({func:Dsp.Dse.get,adds:Bal,docFie:'A.dateEjecu',docStatus:'Y',docNum:'N',rows:'N',card:'N',orderBy:'dateCDesc'},wrap);
}

$V.dsptipoInt=[{k:'CCA',v:'CCA'},{k:'CPACA',v:'CPACA'}];
$V.dsptipoPerj=[{k:'MOR',v:'Morales'},{k:'MAT',v:'Materiales'}];
$V.dspDseType=[{k:'P',v:'Propia'},{k:'C',v:'Compañia'},{k:'T',v:'Terceros'}];
var Dsp={};

Dsp.Dse={
OLg:function(L){
	var Li=[]; var n=0; var tbSerie='dspDse';
	Li.push({ico:'fa fa-eye',textNode:' Perfil Sentencia', P:L, func:function(T){ $Doc.go(tbSerie,'v',T.P,1); } });
	Li.push({k:'edit',ico:'fa fa-pencil',textNode:'Modificar', P:L, func:function(T){ $Doc.go(tbSerie,'f',T.P,1); }});
	Li.push({k:'edit',ico:'fa fa-spinner',textNode:'Actualizar', P:L, func:function(T){ $Api.put({f:Api.Dsp+'dse',jsAdd:{updSimple:'Y',docEntry:T.P.docEntry},func:function(Jr2){
		if(!Jr2.errNo){ Dsp.Dse.get(); }
	}}); } });
	Li.push({k:'sorForm',ico:'fa fa-percil',textNode:'Modificar Propuesta', P:L, func:function(T){ $Doc.go('dspDseSor','form',T.P,1); }});
	Li.push({k:'sorForm',ico:'fa fa-file-o',textNode:'Ver Propuesta Venta', P:L, func:function(T){ $Doc.go('dspDseSor','view',T.P,1); }});
	//Li.push({k:'logs',ico:'fa fa-history',textNode:' Logs de Documento', P:L, func:function(T){ $Doc.tb99({api:Api.Gvt.b+'sop/tb99',serieType:tbSerie,docEntry:T.P.docEntry}); } });
	return $Opts.add(tbSerie,Li,L);;
},
opts:function(P,pare){
	Li={Li:Dsp.Dse.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.Dsp+'dse',inputs:$1.G.filter(),
	fOpts:Dsp.Dse.opts,view:'Y',docBy:'userDate',
	tbSerie:'dspDse',
	TD:[
		{H:'Radicado',k:'noRadicado'},
		{H:'Demandante',k:'demandante'},
		{H:'Demandado',k:'demandado'},
		{H:'Tipo Interes',k:'tipoInt',_V:'dsptipoInt'},
		{H:'Fecha Ejecutoria',k:'dateEjecu'},
		{H:'Cuenta Cobro',k:'dateCcobro'},
		{H:'Fecha Aprox.',k:'datePagoAprox'},
		{H:'Valor Sentencia',k:'valorSentencia',format:'$'},
		{H:'Intereses Hasta',k:'balDate'},
		{H:'Intereses',k:'intAtDate',format:'$'},
		{H:'Valor Hasta',k:'balAtDate',format:'$'}
	],
	tbExport:{ext:'xlsx',fileName:'Sentencias'}
	},cont);
},
form:function(P){
	var api=Api.Dsp+'dse';
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:api+'/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={go:'dspDse',docEntry:Pa.docEntry,docEdit:Pa.docEntry, cont:cont, tbSerie:'N', jsF:jsF,POST:api,
		func:null,Jr:Jr,
		tbH:{kTb:'N',
			L:[{divLine:1,lTag:'input',wxn:'wrapx2',req:'Y',L:'Demandante',I:{name:'demandante',value:Jr.demandante}},
		{lTag:'input',wxn:'wrapx2',req:'Y',L:'Demandado',I:{name:'demandado',value:Jr.demandado}},
		{divLine:1,lTag:'input',wxn:'wrapx8',L:'No. Radicado',I:{name:'noRadicado',value:Jr.noRadicado}},
		
		{lTag:'input',wxn:'wrapx4',req:'Y',L:'Entidad que condena',I:{name:'entidadConde',value:Jr.entidadConde}},
		{lTag:'number',wxn:'wrapx8',req:'Y',L:'Salario Minino',I:{name:'salarioYear',value:Jr.salarioYear}},
		{lTag:'select',wxn:'wrapx8',req:'Y',L:'Tipo',I:{name:'docType',opts:$V.dspDseType}},
		{divLine:1,lTag:'$',wxn:'wrapx4',req:'Y',L:'Valor Sentencia',I:{name:'valorSentencia',value:Jr.valorSentencia}},
		{lTag:'select',wxn:'wrapx8',req:'Y',L:'Tipo Interes',I:{name:'tipoInt',opts:$V.dsptipoInt,value:Jr.tipoInt}},
		{lTag:'date',wxn:'wrapx8',req:'Y',L:'Ejecutoria',I:{name:'dateEjecu',value:Jr.dateEjecu}},
		{lTag:'date',req:'Y',wxn:'wrapx6',L:'Cuenta Cobro',I:{name:'datePcobro',value:Jr.datePcobro}},
		{lTag:'date',req:'Y',wxn:'wrapx6',L:'Fecha Cumplimiento',I:{name:'dateCcobro',value:Jr.dateCcobro}},
		{lTag:'date',wxn:'wrapx6',L:'Fecha Aprox. Pago',I:{name:'datePagoAprox',value:Jr.datePagoAprox}},
		
		]
		}
		};
		tP.addHtml=[];
		tP.addHtml.push($1.t('div'));
		tP.addHtml.push($1.t('div'));
		if(!Jr.docEntry){
			$Api.resp(cont,{text:'Defina la información básica de la sentencía, luego podrá registrar las personas.'});
			tP.goV='f';
			tP.func=function(){ Dsp.Dse.form(); }
		}
		$Doc.form(tP);
		if(Jr.docEntry){
			Dsp.Dse.prs({L:Jr.L,title:'Registro Personas'},tP.addHtml[0]);
		}
	}});
},
view:function(P){
	var api=Api.Dsp+'dse';
	var topc=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:api+'/view',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:topc,func:function(Jr){
		var _5fPars=_5cPars={tt:'dspDse',tr:Pa.docEntry};
		var btnsTop={contPrint:topc}; 
		btnsTop.Li=Dsp.Dse.OLg(Jr);
		$Doc.btnsTop('edit,sorForm,',btnsTop,topc);
		var cont=$1.t('div',0,topc);
		var Pm=[
	{textNode:'Info',winClass:'winInfo','class':'fa fa-info'},
	{textNode:'Proyección',winClass:'winProy','class':'fa fa-line-chart',func:function(T){
		Dsp.Dse.proy({docEntry:Pa.docEntry},T.win);
	}},
	{textNode:'Documentos',winClass:'winRfi','class':'fa fa-paperclip', func:function(T){
		var Pa=$M.read();
		GeDoc.Rfi.oneLoad(_5fPars,T.win);
	}},
	{textNode:'Comentarios',winClass:'winComm','class':'fa fa-comments', func:function(T){
		_5cPars.load='Y';
		Commt.formLine(_5cPars,T.win);
	}},
	];
		var Wins = $1M.tabs(Pm,cont,{w:{style:'margin-top:0.5rem;'}});
		Wins._winInfo.click();
		//Info
		var tb=$1.T.table(['',''],0,Wins.winInfo);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'No. Radicado'},tr);
		$1.t('td',{textNode:Jr.noRadicado},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Demandante'},tr);
		$1.t('td',{textNode:Jr.demandante},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Demandado'},tr);
		$1.t('td',{textNode:Jr.demandado},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Entidad que condena'},tr);
		$1.t('td',{textNode:Jr.entidadConde},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Fecha de Ejecutoria'},tr);
		$1.t('td',{textNode:Jr.dateEjecu},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Salario del Año'},tr);
		$1.t('td',{textNode:$Str.money(Jr.salarioYear)},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Tipo Interes'},tr);
		$1.t('td',{textNode:_g(Jr.tipoInt,$V.dsptipoInt)},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Fecha cuenta de cobro'},tr);
		$1.t('td',{textNode:Jr.dateCcobro},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Fecha probable de pago'},tr);
		$1.t('td',{textNode:Jr.datePagoAprox},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Valor Sentencia'},tr);
		$1.t('td',{textNode:$Str.money(Jr.valorSentencia)},tr);
		//Perjuicios
		var tb=$1.T.table(['Nombre del Beneficiario','Perjucios en SMMLV','Valor en Pesos','Interes','Total','Detalles'],0);
		var tBody=$1.t('tbody',0,tb);
		Wins.winInfo.appendChild($1.T.tbExport(tb));
		Jr.valorSentencia *=1; Jr.intAtDate *=1;
		var bal1=Jr.valorSentencia/Jr.salarioNums;
		var int1=Jr.intTotal/Jr.salarioNums;
		var balTotal=Jr.balTotal;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.prsName},tr);
			$1.t('td',{textNode:L.quantity*1},tr);
			$1.t('td',{textNode:$Str.money(L.balParcial,0)},tr);
			$1.t('td',{textNode:$Str.money(L.priceLine,0)},tr);
			$1.t('td',{textNode:$Str.money(L.balParcial*1+L.priceLine*1,0)},tr);
			$1.t('td',{textNode:L.lineMemo},tr);
		}
		var css='backgroundColor:#CCC';
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Totales',style:css},tr);
		$1.t('td',{textNode:'',style:css},tr);
		$1.t('td',{textNode:$Str.money(Jr.valorSentencia),style:css},tr);
		$1.t('td',{textNode:$Str.money(Jr.intTotal),style:css},tr);
		$1.t('td',{textNode:$Str.money(Jr.balTotal),style:css},tr);
		$1.t('td',{rowspan:4},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Valor a la fecha: '+$2d.f(Jr.balDate,'d mmm'),colspan:2},tr);
		$1.t('td',{textNode:$Str.money(Jr.valorSentencia)},tr);
		$1.t('td',{textNode:$Str.money(Jr.intAtDate,0)},tr);
		$1.t('td',{textNode:$Str.money(Jr.valorSentencia+Jr.intAtDate,0)},tr);
		//Datos tir
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Datos para TIR ',style:css},tr);
		$1.t('td',{textNode:'Fechas'},tr);
		$1.t('td',{textNode:$2d.today},tr);
		$1.t('td',{textNode:Jr.datePagoAprox},tr);
		$1.t('td',{rowspan:2},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{style:css},tr);
		$1.t('td',{textNode:'Valores'},tr);
		$1.t('td',{textNode:$Str.money(1)},tr);
		$1.t('td',{textNode:$Str.money(Jr.balTotal)},tr);
	}});
},
proy:function(P,wList){
	if($1.q('.__open',wList)){ return false; }
	$Api.get({f:Api.Dsp+'dse/proy',inputs:'docEntry='+P.docEntry,loade:wList,func:function(Jr){
		var tb=$1.T.table(['#','Periodo','Tipo','Dias Int.','Interes Periodo','Interes Acumulados','% N.M.V','% N.D.V','']);
		var tBody=$1.t('tbody',{'class':'__open'},tb);
		var iType=[{k:'U',v:'Usura'},{k:'DTF',v:'DTF'}];
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var css=(L.isProy=='Y')?'backgroundColor:#EE0':'';
			var iText=$Str.round(L.iNmv*100,3)+' %';
			var iText2=$Str.round(L.iNdv*100,3)+' %';
			$1.t('td',{textNode:L.numPer,style:css},tr);
			$1.t('td',{textNode:$2d.f(L.per1,'d mmm')+' a '+$2d.f(L.per2,'d mmm'),style:css},tr);
			$1.t('td',{textNode:_g(L.iType,iType),style:css},tr);
			$1.t('td',{textNode:L.dias,style:css},tr);
			$1.t('td',{textNode:$Str.money($Str.round(L.interesLine*1)),style:css},tr);
			$1.t('td',{textNode:$Str.money($Str.round(L.interesAt*1)),style:css},tr);
			$1.t('td',{textNode:iText,style:css},tr);
			$1.t('td',{textNode:iText2,style:css},tr);
			$1.t('td',{textNode:L.lineText,style:css},tr);
		}
		var tb=$1.T.tbExport(tb,{ext:'XLSX',fileName:'Proyección Periodos Sentencia No. '+P.docEntry});
		wList.appendChild(tb);
	}});
},

sorForm:function(){
	var P=$M.T.d(0,{});
	var api=Api.Dsp+'dse';
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:api+'/sorForm',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var AJs={};
		if(Jr.dseId){ AJs.dseId=Jr.dseId; }
		var tP={go:'dspDseSor',docEntry:Pa.docEntry,docEdit:Pa.docEntry, cont:cont, tbSerie:'N', jsF:jsF,POST:api+'/sorForm',
		func:null,Jr:Jr,
		tbH:{kTb:'N',
			L:[{divLine:1,lTag:'input',wxn:'wrapx2',L:'Demandante',I:{disabled:'disabled',value:Jr.demandante}},
		{lTag:'input',wxn:'wrapx2',L:'Demandado',I:{disabled:'disabled',value:Jr.demandado}},
		{divLine:1,lTag:'input',wxn:'wrapx8',L:'Fecha Ejecutoria',I:{disabled:'disabled',value:Jr.dateEjecu}},
		{lTag:'input',wxn:'wrapx4',L:'Entidad que condena',I:{disabled:'disabled',value:Jr.entidadConde}},
		{lTag:'number',wxn:'wrapx8',L:'Salario Minimo año',I:{disabled:'disabled',value:Jr.salarioYear}},
		{lTag:'input',wxn:'wrapx8',L:'Tipo Interes',I:{disabled:'disabled',value:Jr.tipoInt}},
		{divLine:1,lTag:'input',wxn:'wrapx6',L:'Fecha Cuenta Cobro',I:{disabled:'disabled',value:Jr.dateCcobro}},
		{lTag:'$',wxn:'wrapx8',L:'Valor Sentencia',I:{disabled:'disabled',value:Jr.valorSentencia}},
		{lTag:'input',wxn:'wrapx8',L:'Fecha Intereses',I:{disabled:'disabled',value:Jr.balDate}},
		{lTag:'$',wxn:'wrapx8',L:'Interes a la Fecha',I:{disabled:'disabled',value:Jr.intAtDate}},
		{divLine:1,lTag:'date',wxn:'wrapx6',L:'Fecha Compra',I:{name:'docDate',value:Jr.docDate}},
		{lTag:'input',wxn:'wrapx6',L:'Fecha Aprox. Pago',I:{disabled:'disabled',value:Jr.datePagoAprox}},
		{lTag:'input',wxn:'wrapx2',L:'Nombre Cliente',I:{name:'cardName',value:Jr.cardName}},
		{lTag:'date',wxn:'wrapx6',L:'Valido hasta',I:{name:'dueDate',value:Jr.dueDate}},
		{divLine:1,lTag:'$',wxn:'wrapx6',L:'Valor del Giro',I:{name:'balOferta',value:Jr.balOferta}},
		{lTag:'$',wxn:'wrapx6',L:'Valor Total Proyectado',I:{disabled:'disabled',value:Jr.balTotal}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Condiciones de Pago',I:{name:'condicPym',textNode:Jr.condicPym}},
		{divLine:1,lTag:'textarea',wxn:'wrapx1',L:'Tiempo de Pago',I:{name:'timePym',textNode:Jr.timePym}}
		]
		}
		};
		$Doc.form(tP);
	}});
},
sorView:function(){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Dsp+'dse/sorView',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		Jr.docTitle='Propuesta Compra de Sentencia';
		Jr.BLAN=' ';
		var tP={D:Jr,
			btnsTop:{ks:'print,',icons:'Y'},
			THs:[
				{docEntry:'Y'},{k:'docTitle',cs:5,ln:1},{t:'Fecha Doc',k:'docDate',ln:1},
				{t:'Fecha Ejecutoria',k:'dateEjecu'},{middleInfo:'Y'},{logo:'Y'},
				{t:'Tipo Interés',k:'tipoInt',_V:'dsptipoInt'},
				{t:'Fecha Probable Pago',k:'datePagoAprox'},
				{t:'Valor Giro',k:'balOferta',format:'$'},
				{k:'balOferta',cs:4,format:'num2Text',ln:1},{t:'Vencimiento',k:'dueDate',ln:1},
				{t:'Demandante',k:'demandante',cs:3},{t:'Demandado',k:'demandado',cs:4,ln:1},
				{t:'Fecha Cuenta Cobro',k:'dateCcobro'},{t:'Valor Sentencia',k:'valorSentencia',format:'$',ln:1},{t:'Entidad que condega',k:'entidadConde',ln:1,cs:5},
				{t:'Cliente',k:'cardName',cs:7},
				{k:'condicPym',cs:8,addB:$1.t('b',{textNode:'Condiciones de Pago:\u0020'}),HTML:1,Tag:{'class':'pre'}},
				{k:'timePym',cs:8,addB:$1.t('b',{textNode:'Tiempo de Pago:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',
			TFTo:{quantity:{},etapa:{format:'$'}},
			TLs:[
				{t:'Nombre Beneficiario',k:'prsName'},
				{t:'Perjuicios en Salarios',k:'quantity',format:'number'},
				{t:'Valor en Pesos',k:'etapa',format:'$',fText:function(L){ 
					return $Str.round((Jr.valorSentencia/Jr.salarioNums)*L.quantity);
				}},
				{t:'Intereses',k:'etapa',format:'$',fText:function(L){ 
					return $Str.round((Jr.intTotal/Jr.salarioNums)*L.quantity);
				}},
				{t:'Detalles',k:'lineMemo'}
			],
			trBot:[
			[{textNode:'Impuestos a la fecha: '+$Str.money(Jr.intAtDate),colspan:5}]
			]
			}
			]
		};
		$Doc.view(cont,tP);
	}});
}

}

Dsp.prsL={
prsName:['Tipo Perjuicio',{tag:'select',kf:'lineType',k:'lineType',opts:$V.dsptipoPerj}],
prsName:['Nombre Persona',{tag:'input',kf:'prsName',k:'prsName',style:'width:9rem'}],
quantity:['Salarios',{tag:'number',kf:'quantity',k:'quantity',style:'width:6rem'}],
lineMemo:['Detalles',{tag:'input',kf:'lineMemo',k:'lineMemo'}]
};
Dsp.Dse.prs=function(P,pare){
	var cont=pare;P
	var tL=P.L;
	if(tL && tL.errNo==1){ return $Api.resp(wAdd,tL); }
	var wAdd=$1.t('div',0,cont);
	if(tL && tL.errNo){ tL=[]; }
	if(tL){ tL=$js.sortNum(tL,{k:'lineNum'}); }
	var Rd=$DocTb.ini({xMov:'Y',xNum:'N',xDel:'Y',itmAdd:'N',L:tL,
	btnAddL:'Y',fieldset:P.title,
	RowsL:Dsp.prsL
	});
	wAdd.appendChild(Rd.fieldset);
}

Dsp.RfiDse=function(){
	GeDoc.Rfi.define({api:'/',tt:'dspDse'});
}

Dsp.Tax={
dtf:function(Px){
	var iType=Px.iType;
	var ymd=$2d.add($2d.today,'-4months').substr(0,7).replace('-','');
	$Filt.filtFunc=function(){ Dsp.Tax.dtf(Px); }
	$Filt.form({cont:$M.Ht.filt,whs:'Y',active:'Y',Li:[
	{t:'Periodo Mayor a',tag:'input',value:ymd,name:'periodo(E_mayIgual)'},
	{t:'Periodo Menor a',tag:'input',name:'periodo(E_menIgual)'}
	]});
	$Tb._Massi.form({api:Api.Dsp+'dtf',vPost:'wh[iType]='+iType+'&'+$Filt.get($M.Ht.filt),filter:'Y',
	L:[
	[{textNode:'Periodo'},{tag:'input',k:'periodo',name:'periodo',AJsBase:{iType:iType},AJs:['id']}],
	[{textNode:'E. Anual'},{tag:'input',k:'ea',name:'ea',fx:function(TP){
		if(TP.inps){
			var base=1+(TP.inps[1].value*1/100);
			TP.inps[2].value=$js.toFixed(Math.pow(base,1/12)-1,4);
		}
	}}],
	[{textNode:'Mensual'},{tag:'input',k:'nmv',name:'nmv',disabled:'disabled'}]
	]
	});
}
}