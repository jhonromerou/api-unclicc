<?php
if(_0s::$router== 'GET cvt'){ a_ses::hashKey('gvtOcvt.basic');
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	echo Doc::get(array('fromA'=>'A.docEntry,A.dateC,A.userId,A.slpId,A.docDate,A.docStatus,A.dueDate,A.cardName FROM gvt_ocvt A ','whA'=>$wh),array('slps'=>'Y'));
}
else if(_0s::$router==('GET cvt/view')){ a_ses::hashKey('gvtOcvt.basic');
	$_ext=array();
	if(1){
		_0s::jSocGet('$Tpt.gvtCvt_template');
		if(_0s::$jSoc['$Tpt.gvtCvt_template']){ $_ext['docTemplate']=_0s::$jSoc['$Tpt.gvtCvt_template']; }
	}
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'_EXT'=>$_ext,
	'fromA'=>'A.* FROM gvt_ocvt A',
	'fromB'=>'B.*,I.itemCode,I.itemName,I.grsId,I.src1 FROM gvt_cvt1 B JOIN itm_oitm I ON (I.itemId=B.itemId) ','whB'=>'ORDER BY B.lineNum ASC'));
}
else if(_0s::$router==('GET cvt/form')){ a_ses::hashKey('gvtOcvt.basic');
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.* FROM gvt_ocvt A','fromB'=>'B.*,I.itemCode,I.itemName,B.price,B.priceList sellPrice,I.src1 FROM '._0s::$Tb['gvt_cvt1'].' B JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId) ','whB'=>'ORDER BY B.lineNum ASC'));
}
else if(_0s::$router==('PUT cvt/form')){ a_ses::hashKey('gvtOcvt.edit');
	_ADMS::_lb('com/_2d');
	$docEntry=$___D['docEntry'];
	if($docEntry!='' && $js=Doc::status(array('serieType'=>'ocvt','docEntry'=>$___D['docEntry']))){}
	else if($js=_js::ise($___D['cardName'],'Se debe definir el socio de negocios.')){}
	else if($js=_js::ise($___D['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($___D['dueDate'],'La fecha de validez debe estar definida.','Y-m-d')){}
	else if(!_js::textLimit($___D['lineMemo'])){ $js= _js::e(3,'Los detalles no pueden exceder los 255 caracteres.'); }
	else if(!is_array($___D['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{
		unset($___D['textSearch']); $Di=array();
		$Ld=$___D['L']; unset($___D['L']); $errs=0; $n=1; $nl=1;
		$ks='gvtCvt_lineMemoLen';
		_0s::jSocGet($ks);
		$memoLen=100;
		$memoLen=(_0s::$jSoc[$ks]!='_null_' && _0s::$jSoc[$ks]!='')?_0s::$jSoc[$ks]:$memoLen;
		$memoLen=($memoLen=='' || $memoLen>255)?100:$memoLen;
		foreach($Ld as $nk=>$L){
			$totaln=0; $ln ='Linea '.$n.': ';
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir un precio mayor a 0.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0','numeric>0')){ $errs++; break; }
			else if(!_js::textLimit($L['lineMemo'],$memoLen)){ $js= _js::e(3,$ln.'Los detalles no pueden exceder los '.$memoLen.' caracteres.'); $errs++; break; }
			else{
				$Di[$nl]=$L; $Di[$nl]['lineNum']=$n;
				if($L1['delete']){ $Di[$nl]=$L1['delete']; }
				$nl++;
			}
			$n++;
		}
		if($errs==0 && count($Di)==0){ die(_js::e(3,'No se han definido lineas a guardar luego de la verificación.')); }
		if($errs==0){
		a_sql::transaction(); $comit=false;
		$ins=a_sql::insert($___D,array('kui'=>'uid_dateC','table'=>'gvt_ocvt','wh_change'=>'WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1'));
		if($ins['err']){ $js=_js::e(1,$ins['text']); }
		else{
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			$jsEntry='"docEntry":"'.$docEntry.'"';
			$Wh=array();
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum'];
				if($L2['curr'] && $L2['curr']!=_0s::$currDefault){
					$L2['priceME']= $L2['price'];
					$L2['rate']=3000;
					$L2['price'] *=3000; //_0s::$currDefRate;
				}
				$whc='WHERE docEntry=\''.$docEntry.'\' AND lineNum=\''.$L2['lineNum'].'\' LIMIT 1';
				$L2['docEntry']=$docEntry;
				if($L['priceList']==0){ $L['priceList']=$L['price']; }
				if(!array_key_exists('disc',$L2)){ 
					$L2['disc']= (1-($L['price']/$L['priceList']))*100;
				}
				$ins2=a_sql::insert($L2,array('table'=>'gvt_cvt1','wh_change'=>$whc));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text'],$jsEntry);
					$errs++; break;
				}
			}
			if($errs==0){ $comit=true;
				$js=_js::r('Información guardada correctamente.',$jsEntry);
				if($ins['insertId']){
				Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
			}
			}
			a_sql::transaction($comit);
		}
		}
	}
	echo $js;
}
else if(_0s::$router==('PUT cvt/statusCancel')){ a_ses::hashKey('gvtOcvt.edit');
	$___D['serieType']='ocvt';
	echo Doc::statusCancel($___D);
}
else if(_0s::$router==('PUT cvt/statusClose')){ a_ses::hashKey('gvtOcvt.edit');
	$___D['serieType']='ocvt';
	echo Doc::statusClose($___D);
}
else if(_0s::$router==('GET cvt/updateDocTotal')){
	#lineTotal=,lineTotalList,discPf,discSum,docTotal -- <=897
	$wh='WHERE docEntry>=897';
	#/* 
	$q1=a_sql::query('update gvt_cvt1 
	SET price=priceList,priceLine=priceList*(1-(disc/100)),lineTotal=priceLine,lineTotalList=priceList*quantity,discSum=(lineTotalList-lineTotal) '.$wh.' ');
	print_r($q1);
	#*/
	$q=a_sql::query('SELECT docEntry,sum(lineTotal) lineTotal,sum(lineTotalList) lineTotalList,sum(discSum) discSum, sum(lineTotal) docTotal FROM gvt_cvt1  '.$wh.' GROUP BY docEntry');
	print_r($q);
	while($L=$q->fetch_assoc()){
		$L['._.discTotal']='+-+round(discSum/lineTotalList*100,2)';
		a_sql::insert($L,array('tbk'=>'gvt_ocvt','qDo'=>'update','wh_change'=>'WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1'));
	}
}
?>