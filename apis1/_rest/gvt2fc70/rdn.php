<?php
$serieType='gvtRdn';
if(_0s::$router=='GET rdn'){ a_ses::hashKey('gvtRdn');
	$___D['fromA']='A.docEntry,A.docStatus,A.cardId,BC.cardName,A.slpId,A.docDate,A.userId,A.dateC,A.whsId,A.dateGet FROM gvt_ordn A JOIN par_ocrd BC ON (BC.cardId=A.cardId)';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET rdn/view'){ a_ses::hashKey('gvtRdn');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT A.docEntry,A.cardId,ocrd.cardName,A.userId,A.docStatus,A.docDate,A.dateGet,A.ref1,A.whsId,A.doAction,A.lineMemo FROM gvt_ordn A LEFT JOIN par_ocrd ocrd ON (ocrd.cardId=A.cardId) WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo documento: ',2=>'No se encontró el documento '.$___D['docEntry'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{ $Mx=$q; $Mx['L']=array();
		$q2=a_sql::query('SELECT B.lineNum,I.itemId,I.itemCode,I.itemName,I.udm,B.itemSzId,B.quantity,B.reason,B.ref1,B.ref2,B.ref3,B.delivRef,B.delivDate FROM '._0s::$Tb['gvt_rdn1'].' B JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId) WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento: ',2=>'No se encontraron lineas del documento.'));
		if(a_sql::$err){ $Mx['L']=json_decode(a_sql::$errNoText,1); }
		else{
			while($L=$q2->fetch_assoc()){
				$Mx['L'][]=$L;
			}
		}
	}
	echo _js::enc2($Mx);
}
else if(_0s::$router=='GET rdn/tb99'){ a_ses::hashKey('gvtRdn');
	echo Doc::getOne(array('fromA'=>'A.docDate FROM gvt_ordn A','fromB'=>'* FROM gvt_doc99 B','whB'=>'AND B.serieType=\''.$serieType.'\'','docEntry'=>$___D['docEntry']));
}
/*reports*/
else if(_0s::$router=='GET rdn/rep/status'){ a_ses::hashKey('gvtRdn');
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	$q=a_sql::query('SELECT A.docEntry,A.docStatus,A.cardId,ocrd.cardName,A.slpId,A.docDate,A.userId,A.dateC,A.whsId,A.dateGet,I.itemCode,I.itemName,B.itemSzId,B.quantity,B.reason,B.ref1,B.ref2,B.ref3,B.delivRef,B.delivDate,B.daysToDeliv FROM '._0s::$Tb['gvt_ordn'].' A 
JOIN '._0s::$Tb['par_ocrd'].' ocrd ON (ocrd.cardId=A.cardId) 
JOIN '._0s::$Tb['gvt_rdn1'].' B ON (B.docEntry=A.docEntry) 
JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId) 
WHERE 1 '.$wh.' ',array(1=>'Error obteniendo listado de documentos.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{ $Mx=array('L'=>array());
		while($L=$q->fetch_assoc()){
			$Mx['L'][]=$L;
		}
		$js=_js::enc2($Mx);
	}
	echo $js;
}


else if(_0s::$router=='PUT rdn/statusCancel'){ a_ses::hashKey('gvtRdn');
	if($js=_js::ise($___D['lineMemo'],'Debe definir el motivo de anulación.')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else if($js=Doc::status(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry']),array('fie'=>'whsId,canceled','D'=>'Y'))){ die($js); }
	else{
		a_sql::transaction(); $errs=0;
		$upd=a_sql::query('UPDATE gvt_ordn SET docStatus=\'N\',canceled=\'Y\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error anuladno estado del documento: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
		else{
			/* revertir transferncia si.. */
			if(Doc::$D['docStatus']=='O'){
				_ADMS::_lb('src/Invt'); $Liv=array(); 
				Invt::onHand_rever(array('tt'=>$serieType,'tr'=>$___D['docEntry'],'docDate'=>date('Y-m-d')));
				if(Invt::$err){ $js=Invt::$errText; $errs++; }
			}
			if($errs==0){ $js=_js::r('Documento recibido correctamente.');
				Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'N','lineMemo'=>$___D['lineMemo']));
				a_sql::transaction(true);
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT rdn/statusClose'){a_ses::hashKey('gvtRdn');
	$___D['serieType']=$serieType;
	echo Doc::statusClose($___D);
}
else if(_0s::$router=='PUT rdn/statusReceive'){ a_ses::hashKey('gvtRdn.statusReceive');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	else if($js=_js::ise($___D['dateGet'],'Se debe definir la fecha de recibido.')){ die($js); }
	else if($js=_js::ise($___D['whsId'],'Se debe definir la bodega de ingreso.')){ die($js); }
	else if($js=Doc::status(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry']),array('fie'=>'whsId,canceled','D'=>'Y'))){ die($js); }
	else if(Doc::$D['docStatus']=='O'){ die(_js::e(3,'El documento ya fue recibido.')); }
	else{
		$errs=0;
		a_sql::transaction(); $commit=false;
		$upd=a_sql::query('UPDATE gvt_ordn SET docStatus=\'O\',whsId=\''.$___D['whsId'].'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
		else{
			$gb='B.itemId,B.itemSzId';
			$ql=Doc::lineGet(array('docEntry'=>$___D['docEntry'],'r'=>'query','fromB'=>$gb.',SUM(B.quantity) quantity  FROM gvt_rdn1 B','whB'=>'GROUP BY '.$gb));
			if(a_sql::$err){ $js=a_sql::$errNoText; }
			else{
				_ADMS::_lb('src/Invt'); $Liv=array(); 
				while($L=$ql->fetch_assoc()){
					$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$___D['whsId'],'inQty'=>$L['quantity']);
				}
				Invt::onHand_put($Liv,array('docDate'=>$___D['dateGet'],'tt'=>$serieType,'tr'=>$___D['docEntry']));
				if(Invt::$err){ $js=Invt::$errText; $errs++; }
			}
		}
		if($errs==0){ $js=_js::r('Documento recibido correctamente.');
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'O'));
			a_sql::transaction(true);
		}
	}
	echo $js;
}

else if(_0s::$router=='GET rdn/form'){ a_ses::hashKey('gvtRdn');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT A.docEntry,A.cardId,ocrd.cardName,A.userId,A.docStatus,A.docDate,A.ref1,A.whsId,A.doAction,A.lineMemo,A.slpId FROM gvt_ordn A JOIN par_ocrd ocrd ON (ocrd.cardId=A.cardId) WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo documento: ',2=>'No se encontró el documento '.$___D['docEntry'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if($js=a_ses::nis_slp($q['slpId'])){ die($js); }
	else{ $Mx=$q; $Mx['L']=array();
		$q2=a_sql::query('SELECT B.id,B.lineNum,I.itemId,I.itemCode,I.itemName,I.sellUdm,B.itemSzId,B.quantity,B.reason FROM gvt_rdn1 B JOIN itm_oitm I ON (I.itemId=B.itemId) WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento: ',2=>'No se encontraron lineas del documento.'));
		if(a_sql::$err){ $Mx['L']=json_decode(a_sql::$errNoText,1); }
		else{
			while($L=$q2->fetch_assoc()){
				$Mx['L'][]=$L;
			}
		}
	}
	echo _js::enc($Mx,'NO_PAGER');
}
else if(_0s::$router=='PUT rdn/form'){ a_ses::hashKey('gvtRdn');
	$L=$___D['L'];
	_ADMS::_lb('com/_2d'); unset($___D['L'],$___D['serieType'],$___D['textSearch'],$___D['cardName']); 
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha del documento.','Y-m-d')){}
	else if($js=_js::ise($___D['cardId'],'El socio de negocios debe definirse.','numeric>0')){}
	else if($js=_js::ise($___D['slpId'],'Se debe definir el responsable de ventas.','numeric>0')){}
	else if($js=_js::ise($___D['doAction'],'Defina una acción propuesta.')){}
	else if(!($js=_js::textLimit($___D['doAction'],1,'La acción propuesta no puede exceder los 255 caracteres.'))){ }
	else if(!is_array($L) || count($L)==0){ $js=_js::e(3,'No se recibieron lineas'); }
	else if(!_js::ise($___D['docEntry']) && $js=Doc::status(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry']),array('D'=>'Y'))){}
	else if(Doc::$D['docStatus']=='O'){ $js=_js::e(3,'El documento ya fue recibido, no se puede modificar.');}
	else{
		_ADMS::_lb('itm');
		foreach($L as $n => $D2){ $ln='Linea '.$n.': ';
			if(_js::ise($D2['quantity'],'','numeric>0')){ $js=_js::e(3,$ln.'La cantidad debe ser un número mayor a 0.',$addJs); $errs++; break; }
			else{
				if($itemId_!=$D2['itemId']){
					$R=_itm::get_oipc(array('itemId'=>$D2['itemId'],'fie'=>'PC.sellPrice'));
					if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
				}
				$L[$n]['price']=$R[$D2['itemSzId']]['sellPrice'];
				$L[$n]['cost']=$R[$D2['itemSzId']]['defineCost'];
			}
			$itemId_=$D2['itemId'];
		}
		if($errs==0){
			$ins=a_sql::insert($___D,array('table'=>'gvt_ordn','kui'=>'uid_dateC','wh_change'=>'WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1'));
			if($ins['err']){ $errs++; $js=_js::e(3,'Error guardando documento: '.$ins['text']); }
			else{
				$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
				foreach($L as $n => $D2){ $ln='Linea '.$n.': ';
					$D2['docEntry']=$docEntry; $D2['lineNum']=$n;
					$ins2=a_sql::insert($D2,array('table'=>'gvt_rdn1','wh_change'=>'WHERE docEntry=\''.$D2['docEntry'].'\' AND id=\''.$D2['id'].'\' LIMIT 1'));
					if($ins2['err']){ $js=_js::e(3,$ln.'Error guardando linea del documento: '.$ins2['text']); $errs++; break; }
				}
			}
		}
		if($errs==0){
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"');
			if($ins['insertId']){
				Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
			}
		}
	
	}
	echo $js;
}

elseif(_0s::$router=='GET rdn/form2'){  a_ses::hashKey('gvtRdn.form2');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT A.docEntry,A.cardId,ocrd.cardName,A.userId,A.docStatus,A.docDate,A.ref1,A.whsId,A.doAction,A.lineMemo,A.slpId FROM gvt_ordn A JOIN par_ocrd ocrd ON (ocrd.cardId=A.cardId) WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo documento: ',2=>'No se encontró el documento '.$___D['docEntry'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if($js=a_ses::nis_slp($q['slpId'])){ die($js); }
	else{ $Mx=$q; $Mx['L']=array();
		$q2=a_sql::query('SELECT B.lineNum,I.itemId,I.itemCode,I.itemName,I.udm,B.itemSzId,B.quantity,B.reason,B.ref1,B.ref2,B.ref3,B.delivRef,B.delivDate FROM '._0s::$Tb['gvt_rdn1'].' B JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId) WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento: ',2=>'No se encontraron lineas del documento.'));
		if(a_sql::$err){ $Mx['L']=json_decode(a_sql::$errNoText,1); }
		else{
			while($L=$q2->fetch_assoc()){
				$Mx['L'][]=$L;
			}
		}
	}
	echo _js::enc($Mx,'NO_PAGER');
}
else if(_0s::$router=='PUT rdn/form2'){ a_ses::hashKey('gvtRdn.form2');
	$L=$___D['L'];
	_ADMS::_lb('com/_2d'); unset($___D['L']);
	if(!_js::ise($___D['docEntry']) && $js=Doc::status(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'verif'=>'(ordnClasif)'))){ die($js); }
	else if(count($L)==0){ $js=_js::e(3,'No se recibieron lineas'); }
	else{
		$docEntry=$___D['docEntry'];
		$q=a_sql::fetch('SELECT docStatus,canceled,slpId,docDate FROM gvt_ordn WHERE docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo información del documento: ',2=>'No se encontró el documento #'.$docEntry.'.'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else if($js=a_ses::nis_slp($q['slpIds'])){ die($js); }
		$D1=array('lineMemo'=>$___D['lineMemo']);
		$ins2=a_sql::insert($D1,array('table'=>'gvt_ordn','wh_change'=>'WHERE docEntry=\''.$docEntry.'\' LIMIT 1'));
				if($ins2['err']){ die(_js::e(3,$ln.'Error actualizando documento: '.$ins2['text'],$addJs)); }
		$addJs='"docEntry":"'.$docEntry.'"'; $errs=0;
		foreach($L as $n => $D2){ $ln='Linea '.$n.': ';
			if($D2['delivDate']!='' && $D2['delivDate']<$q['docDate']){ $js=_js::e(3,$ln.'La fecha de despacho no puede ser menor a la fecha del documento.'); $errs++; break;}
		}
		if($errs==0){ _ADMS::_lb('com/_2d');
			foreach($L as $n => $D2){ $ln='Linea '.$n.': ';
				$Di=array('ref1'=>$D2['ref1'],'ref2'=>$D2['ref2'],'ref3'=>$D2['ref3'],'delivRef'=>$D2['delivRef'],'delivDate'=>$D2['delivDate']);
				$Di['daysToDeliv']=_2d::relTime($q['docDate'],$D2['delivDate']);
				$ins2=a_sql::insert($Di,array('table'=>'gvt_rdn1','wh_change'=>'WHERE docEntry=\''.
				$docEntry.'\' AND lineNum=\''.$n.'\' LIMIT 1'));
				if($ins2['err']){ $js=_js::e(3,$ln.'Error guardando linea del documento: '.$ins2['text'],$addJs); $errs++; break; }
			}
		}
		if($errs==0){ $js=_js::r('Documento guardado correctamente.',$addJs); }
	
	}
	echo $js;
}
?>