<?php
$serieType='gvtPdn';
Doc::$owh=false;
if(_0s::$router=='GET pdn'){
	$___D['fromA']='A.docEntry,A.docStatus,A.cardId,A.cardName,A.docType,A.docDate,A.docTotal,A.curr,A.docTotalME,A.userId,A.dateC 
	FROM gvt_opdn A';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET pdn/tb99'){
	echo Doc::tb99($serieType,$___D);
}
else if(_0s::$router=='POST pdn'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha.')){}
	else if($js=_js::ise($___D['cardId'],'Se debe definir ID del socio de negocios.','numeric>0')){}
	else if($js=_js::ise($___D['cardName'],'Se debe definir socio de negocios.')){}
	else if($js=_js::ise($___D['docClass'],'Se debe definir la clasificación.')){}
	else if($js=_js::ise($___D['baseType'],'Se debe definir el tipo de documento base.')){}
	else if($js=_js::ise($___D['baseRef'],'Se debe definir el número de documento base.')){ die($js); }
	else if($js=_js::ise($___D['baseDocDate'],'Se debe definir la fecha del documento base.')){}
	else if($js=_js::ise($___D['baseDueDate'],'Se debe definir la fecha de vencimiento del documento base.')){}
	else if(!_js::textLimit($___D['lineMemo'],200)){ $js=_js::e(3,'Los detalles no pueden exceder 200 caracteres.'); }
	else if(!is_array($___D['L']) || count($___D['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$___D['L']; unset($___D['L']);
		$errs=0; $nl=0;
		$_docTotalLine=$_docTotalList=0;
		foreach($Ld as $n => $L){ $nl++;
			$ln='Linea '.$nl.': '; 
			if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
			else{
				$Ld[$n]['lineNum']=$nl;
				$Ld[$n]=Doc::lineTotal($L,array('discPf'=>$___D['discPf'],'curr'=>$___D['curr']));
				$_docTotalList+=$Ld[$n]['lineTotalList'];
				$_docTotalLine+=$Ld[$n]['priceLine'];
			}
		}
		$___D['docTotalList']=$_docTotalList;
		$___D['docTotalLine']=$_docTotalLine;
		$___D=Doc::docTotal($___D);
		if($errs==0){ unset($___D['undefined']);
			a_sql::transaction(); $cmt=false;
			$ins=a_sql::insert($___D,array('table'=>'gvt_opdn','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{ $Liv=array();
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				$ins2=a_sql::insert($L,array('table'=>'gvt_pdn1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
				else{
					$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'inQty'=>$L['quantity']);
				}
			}
			}
		}
		if($errs==0){
			_ADMS::_lb('src/Invt');
			Invt::onHand_put($Liv,array('tt'=>$serieType,'tr'=>$docEntry,'docDate'=>$___D['docDate'],'isPurc'=>'Y'));
			if(Invt::$err){ $js=Invt::$errText; }
			else{ $js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); 
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
			a_sql::transaction(true);
			}
			
		}
		
	}
	echo $js;
}
else if(_0s::$router=='GET pdn/view'){ //a_ses::hashKey('.basic');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'C.cardCode, C.licTradType,C.licTradNum,A.* 
	FROM gvt_opdn A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)',
	'fromB'=>'I.itemCode, I.itemName,I.buyUdm, B.* FROM gvt_pdn1 B 
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));
	//else if($js=a_ses::nis_slp($L['slpId'])){ die($js); }
}
else if(_0s::$router=='PUT pdn/statusCancel'){ a_ses::hashKey('gvtPdn.statusCancel');
	if($js=_js::ise($___D['docEntry'],'ID de documento no definido.','numeric>0')){}
	else if($js=_js::ise($___D['lineMemo'],'Debe definir el motivo de anulación')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else{
		$___D['closeOmit']='Y'; $___D['serieType']=$serieType;
		a_sql::transaction();
		$js=Doc::statusCancel($___D);
		if(!Doc::$ok){}
		else{
			_ADMS::_lb('src/Invt');
			$r=Invt::onHand_rever(array('tt'=>$serieType,'tr'=>$___D['docEntry'],'docDate'=>date('Y-m-d')));
			if(_err::$err){ $js=_err::$errText; }
			else{ $js=_js::r('Documento Anulado correctamente.'); a_sql::transaction(true); }
		}
	}
	echo $js;
}
?>