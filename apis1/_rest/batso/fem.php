<?php
$serieType='batsoFem';
if(_0s::$router=='GET fem'){
	$formCode=$___D['formCode'];
	if($js=_js::ise($___D['femId'],'Se debe definir el ID del documento de respuesta.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT A.femId,A.empId,A.tipoEmp,B.data
FROM dsp_ofem A LEFT JOIN dsp_fem1 B ON (B.femId=A.femId AND B.formCode LIKE \''.$formCode.'%\') 
WHERE A.femId=\''.$___D['femId'].'\' LIMIT 1',array(1=>'Error obteniendo documento '.$___D['femId'].': ',2=>'No se encontró el documento #'.$___D['femId'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$M=$q; $M['L']=array(); $M['data']=json_decode($M['data'],1);
	if($formCode=='intra'){ $formCode .= $M['tipoEmp']; }
	$q=a_sql::query('SELECT F1.askId,F1.req,F1.lineNum,F1.formType,F1.lineText,F1.formData,F1.group1,F1.group2
	FROM dsp_itf1 F1 
	JOIN dsp_oitf Fo ON (Fo.formId=F1.formId)  
	WHERE Fo.formCode=\''.$formCode.'\' ORDER BY F1.lineNum ASC',array(1=>'Error obteniendo formularios de baterias: ',2=>'No se encontraron datos guardados para el formulario ('.$formCode.'.'));
	if(a_sql::$err){ $M['L']=_js::dec(a_sql::$errNoText); }
	else{
		$M['R']=a_sql::fetch('SELECT data FROM dsp_fem1 WHERE');
		while($L=$q->fetch_assoc()){
			$L['formData']=json_decode($L['formData'],1);
			$M['L'][]=$L;
			}
	}
	echo _js::enc($M);
}
else if(_0s::$router=='PUT fem'){
	$femId=$___D['femId'];
	$formCodeA=$___D['formCode'];
	if($js2=_js::ise($___D['femId'],'ID de documento respuesta no definido o valido ('.$___D['femId'].').','numeric>0')){ die($js2); }
	else if($js2=_js::ise($formCodeA,'El tipo de documento no es soportado: '.$formCodeA,'/^(intra|extra|estres)$/')){ die($js2); }
	else if(!is_array($___D['L']) || count($___D['L'])==0){ die(_js::e(3,'No se recibieron lineas a guardar.')); }
	else if($js=a_sql::noExists(array('lastFetch'=>'Y','fie'=>'femId,tipoEmp','tbk'=>'dsp_ofem','wh'=>'femId='.$___D['femId'],2=>'El documento '.$___D['femId'].' no existe.'))){ die($js); }
	else{
		$fkD=batSo::getKeys($formCodeA,a_sql::$lastFetch['tipoEmp']);
		$formCode=$fkD['formCode'];
		$Ld=$___D['L']; unset($___D['L']); $sinResp=0;
		$n=1;
		$sDo=$sDi=$sFo=array();
		$prom1=$prom2=$prom3=$prom4=0;
		$F1=array(); $R=array();
		foreach($Ld as $askId => $L){
			$ln='Linea '.$n.': '; $n++; $lineNum=$L['lineNum'];
			if($js=_js::ise($L['lineNum'],$ln.'Número de linea no definido.')){ $errs++; break; }
			else if($js=_js::ise($L['value'],$ln.'No se definió ninguna respuesta.')){ $errs++; break; }
			else if($js=_js::ise($L['group1'],$ln.'Dominio no Definido.')){ $errs++; break; }
			else if($js=_js::ise($L['group2'],$ln.'Dimensión no Definida.')){ $errs++; break; }
			else{
					$k1=$L['group1']; $k2=$L['group2'];
					$L['askId']=$askId;
					$F1[$askId]=$L['value'];
					$k3=-1;
					if($formCode!='estres'){ $k3=$k1;
						if(!array_key_exists($k1,$sDo)){
							$sDo[$k1]=array('suma'=>0,'lineType'=>'dom','group1'=>$k1);
						}
						if(!array_key_exists($k2,$sDi)){
							$sDi[$k2]=array('suma'=>0,'lineType'=>'dim','group1'=>$k1,'group2'=>$k2);
						}
						$sDo[$k1]['suma'] +=$L['value'];
						$sDi[$k2]['suma'] +=$L['value'];
						$sFo['suma']+=$L['value'];
					}
					else{
						if(_js::numRang($lineNum,1,8)){ $k3=1; $prom1 += $L['value']; }
						else if(_js::numRang($lineNum,9,12)){ $k3=2; $prom2 += $L['value']; }
						else if(_js::numRang($lineNum,13,22)){ $k3=3; $prom3 += $L['value']; }
						else if(_js::numRang($lineNum,23,31)){ $k3=4; $prom4 += $L['value']; }
					}
					$R[$k3] .= $lineNum.' // ';
				}
		}
		unset($Ld); ksort($R);
		$femDo1='insert'; $femDo2='insert';
		a_sql::transaction(); $cmt=false;
		if($errs==0){
			$L=array('femId'=>$femId,'formCode'=>$formCodeA,'data'=>json_encode($F1));
			$ins=a_sql::insert($L,array('table'=>'dsp_fem1','wh_change'=>'WHERE femId=\''.$___D['femId'].'\' AND formCode=\''.$formCodeA.'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,'Error guardando lineas fem1: '.$ins['text']); $errs++; }
			else{
				$L=array('suma'=>$sFo['suma'],'lineType'=>'form');
				$whBa='WHERE femId=\''.$femId.'\' AND formCode=\''.$formCodeA.'\' ';
				$F2=array();
				$L=batSo::get_sumTra($L,array('fcvK'=>$fkD['fcvK'],'brmK'=>$fkD['brmK'],'formCode'=>$formCode,'from'=>'form',
				'prom1'=>$prom1,'prom2'=>$prom2,'prom3'=>$prom3,'prom4'=>$prom4,'R'=>$R));
				$F2[]=$L;
				foreach($sDo as $k1 => $L){ $L['femId']=$femId;
					$L=batSo::get_sumTra($L,array('fcvK'=>$fkD['dom'].$k1,'brmK'=>$fkD['dim'].$k1,'formCode'=>$formCode,'from'=>'dom'));
					$F2[]=$L;
				}
				foreach($sDi as $k2 => $L){ $L['femId']=$femId;
					$L=batSo::get_sumTra($L,array('fcvK'=>$fkD['dom'].$k2,'brmK'=>$fkD['dim'].$k2,'formCode'=>$formCode,'from'=>'dim'));
					$F2[]=$L;
				}
				$L=array('femId'=>$femId,'formCode'=>$formCodeA,'data'=>json_encode($F2));
				$ins2=a_sql::insert($L,array('table'=>'dsp_fem2','wh_change'=>$whBa.' LIMIT 1'));
				if($ins2['err']){ $errs++; $js=_js::e(3,'Error definiendo puntaje de formlario: '.$ins2['err']['error_sql']); }
			}
		}
		if($errs==0){ $js=_js::r('Información guardada correctamente.'); $cmt=true; }
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='PUT fem.statusCancel'){
	$___D['femId']=$___D['docEntry'];
	if($js=Doc::status(array('serieType'=>$serieType,'docEntry'=>$___D['femId'],'docEntryAlias'=>'femId'))){ die($js); }
	else{
		$up=a_sql::query('UPDATE dsp_ofem SET docStatus=\'N\',canceled=\'Y\' WHERE femId=\''.$___D['femId'].'\' LIMIT 1',array(1=>'Error anulando documento'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Documento anulado correctamente'); }
	}
	echo $js;
}
else if(_0s::$router=='GET fem.view'){
	if($js=_js::ise($___D['femId'],'Se debe definir el ID del documento de respuesta.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT *
FROM dsp_ofem 
WHERE femId=\''.$___D['femId'].'\' LIMIT 1',array(1=>'Error obteniendo documento '.$___D['femId'].': ',2=>'No se encontró el documento #'.$___D['femId'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$M=$q; $M['L']=array();
	$gb='data';
	$q=a_sql::query('SELECT formCode,data FROM dsp_fem2 Ef2
	WHERE Ef2.femId=\''.$___D['femId'].'\' ',array(1=>'Error obteniendo formularios de baterias: ',2=>'No se encontraron formularios de baterias.'));
	$prom1=$prom2=$prom3=0;
	$M['L']=array();
	if(a_sql::$err){ $M['L']=_js::dec(a_sql::$errNoText); }
	else while($L1=$q->fetch_assoc()){
		$formCode=$L1['formCode'];
		$formCode=($formCode=='intra')?$formCode.$M['tipoEmp']:$formCode;
		$L1=json_decode($L1['data'],1);
			foreach($L1 as $nx => $L){
			$k='form_';
			if(!array_key_exists($formCode,$M['L'])){
				$M['L'][$formCode]=array();
			}
			switch($L['lineType']){
				case 'form': $k='form'; break;
				case 'dom': $k=$L['group1']; break;
				case 'dim': $k=$L['group2']; break;
			}
			unset($L['lineType'],$L['group1'],$L['group2']);
			if(array_key_exists('sumTransform',$L)){ $L['pt']=$L['sumTransform']; unset($L['sumTransform']); }
			$M['L'][$formCode][$k]=$L;
		}
	}
	echo _js::enc($M,'just');
}
else if(_0s::$router=='GET fem.xls'){
	if($js=_js::ise($___D['odocEntry'],'No se ha definido correctamente el odocEntry del cliente.','numeric>0')){ die($js); }
	echo Batso::fem_xls($___D);
}
?>