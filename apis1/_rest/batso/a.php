<?php 
if(_0s::$router=='GET '){
	$q=a_sql::query('SELECT * FROM dsp_ofrc WHERE 1 '.a_sql::nextLimit(),array(1=>'Error obteniendo documentos: ',2=>'No se encontraron documentos.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$M=array('L'=>array());
	while($L=$q->fetch_assoc()){
		$M['L'][]=$L;
	}
	echo _js::enc($M);
}
else if(_0s::$router=='POST '){
	$n=1; $errs=0; _ADMS::_lb('com/_2d');
	if($js=_js::ise($___D['docYear'],'Se debe definir el año de aplicación.','numeric>0')){ die($js); }
	else if($js=_js::ise($___D['cardName'],'Se debe definir el nombre del cliente.')){ die($js); }
	else if($js=_js::ise($___D['empQty'],'La cantidad de empleados debe definirse y ser mayor a 0.','numeric>0')){ die($js); }
	else if($js=_js::ise($___D['docDate'],'Se debe definir la fecha de inicio.','Y-m-d')){ die($js); }
	else if($js=_js::ise($___D['dueDate'],'Se debe definir la fecha de fianlización.','Y-m-d')){ die($js); }
	else if($js=_js::ise($___D['makeBy'],'Se debe definir por quién fue aplicado.')){ die($js); }
	else if(!_js::textLimit($___D['lineMemo'],255)){ die(_js::e(3,'Los detalles no pueden exceder los 255 caracteres.')); }
	else{
		unset($___D['docEntry']);
		$ins=a_sql::insert($___D,array('table'=>'dsp_ofrc','kui'=>'uid_dateC','wh_change'=>'WHERE odocEntry=\''.$___D['odocEntry'].'\' LIMIT 1'));
		if($ins['err']){ $js=_js::e(3,'Error generando documento de aplicación :'.$ins['text']); }
		else{
			$odocEntry=($ins['insertId'])?$ins['insertId']:$___D['odocEntry'];
			$js=_js::r('Información guardada correctamente.','"odocEntry":"'.$odocEntry.'"');
			
		}
	}
	echo $js;
}
else if(_0s::$router=='GET a.open'){
	if($js=_js::ise($___D['odocEntry'],'Se debe definir el ID del documento de la empresa.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT * FROM dsp_ofrc WHERE odocEntry=\''.$___D['odocEntry'].'\'  LIMIT 1',array(1=>'Error obteniendo documento de aplicación para empresa '.$___D['odocEntry'].': ',2=>'No se encontró el documento de aplicación #'.$___D['odocEntry'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$M=$q; $M['L']=array();
	$q=a_sql::query('SELECT Ef.femId,Ef.dateC,Ef.empId,Ef.fullName,Ef.docStatus,Ef.tipoEmp FROM dsp_ofem Ef WHERE Ef.odocEntry=\''.$___D['odocEntry'].'\' '.a_sql::nextLimit(),array(1=>'Error obteniendo formularios aplicados a empleados: ',2=>'No se encontraron registros aplicados a empleados.'));
	if(a_sql::$err){ $M['L']=_js::dec(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		}
	}
	echo _js::enc($M);
}
?>