<?php
if(_0s::$router=='POST femDatos'){
	$n=1; $errs=0;
	if($js=_js::ise($___D['odocEntry'],'Se debe definir el documento de aplicación empresa.','numeric>0')){ die($js); }
	else if($js=_js::ise($___D['empId'],'Se debe definir un identificador de empleado.','numeric>0')){ die($js); }
	else if($js=_js::ise($___D['fullName'],'Se debe definir el nombre del empleado.')){ die($js); }
	else if($js=_js::ise($___D['tipoCargo'],'Se debe definir el tipo de cargo del empleado.')){ die($js); }
	if($errs==0){
		unset($___D['lineNum']);
		if(preg_match('/^(J|P)$/',$___D['tipoCargo'])){ $___D['tipoEmp']='A'; }
		else{ $___D['tipoEmp']='B'; }
		$ins=a_sql::insert($___D,array('table'=>'dsp_ofem','qDo'=>'insert','kui'=>'dateC'));
		if($ins['err']){ $js=_js::e(3,'Error generando documento de empleado :'.$ins['text']); }
		else{
			$femId=($ins['insertId'])?$ins['insertId']:$___D['femId'];
			$js=_js::r('Información guardada correctamente.','"femId":"'.$femId.'"');
			
		}
	}
	echo $js;
}
else if(_0s::$router=='GET femDatos'){
	$q=a_sql::fetch('SELECT * FROM dsp_ofem WHERE femId=\''.$___D['femId'].'\' LIMIT 1',array(1=>'Error obteniendo formularios de empleado: ',2=>'No se encontró el documento.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{ $js=_js::enc($q); }
	echo $js;
}
else if(_0s::$router=='PUT femDatos'){
	$n=1; $errs=0;
	if($js2=_js::ise($___D['femId'],'ID de documento respuesta no definido o valido ('.$___D['femId'].').','numeric>0')){ die($js2); }
	foreach($___D['L'] as $fk => $L){
		if($js=_js::ise($L['value'],'Linea '.$n.': se debe definir un valor')){ break; $errs++; }
		else{  $Di[$fk]=$L['value']; }
	}
	if($errs==0){
		if(preg_match('/^(J|P)$/',$Di['tipoCargo'])){ $Di['tipoEmp']='A'; }
		else{ $Di['tipoEmp']='B'; }
		$Di['edad']=date('Y')-$Di['birYear'];
		$ins=a_sql::insert($Di,array('table'=>'dsp_ofem','wh_change'=>'WHERE femId=\''.$___D['femId'].'\' LIMIT 1'));
		if($ins['err']){ $js=_js::e(3,'Error generando documento de empleado :'.$ins['text']); }
		else{
			$femId=($ins['insertId'])?$ins['insertId']:$___D['femId'];
			$js=_js::r('Información guardada correctamente.','"femId":"'.$femId.'"');
			
		}
	}
	echo $js;
}
?>