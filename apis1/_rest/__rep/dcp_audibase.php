<?php
$rootPATH = realpath($_SERVER['DOCUMENT_ROOT']);
require($rootPATH.'/__adms/__restNoApp.php');
$docYear=$___D['FIE'][0]['R.docYear(E_igual)'];
unset($___D['FIE'][0]['R.docYear(E_igual)']);
if($js=_js::ise($docYear,'Se debe definir el año.')){ die($js); }
$Mx = array(
'kOrder'=>'itemSzId',
'FIE'=>array('docYear'=>'Año','cardName'=>'Cliente'),
'L'=>array(),
);
$formId=1;
$formCode='audBase';
_ADMS::_lb('sql/filter');
$wh=a_sql_filtByT($___D);
$Cols=array(); $coln=3;
$q=a_sql::query('SELECT F1.askId,F1.lineNum,F1.formType, F1.lineText, F1.formData
FROM '._0s::$Tb['fqu_oitf'].' F 
JOIN '._0s::$Tb['fqu_itf1'].' F1 ON (F1.formId=F.formId)
WHERE F.formId=\''.$formId.'\' ORDER BY F1.lineNum ASC
',array(1=>'Error obteniendo información del formulario: ',2=>'No se encontraron resultados para el formulario.'));
if(a_sql::$err){ die(a_sql::$errNoText); }
while($L = $q->fetch_assoc()){
	$k='P'.$L['askId'];
	$Mx['FIE'][$k]=$L['lineText'];
	$os=json_decode($L['formData'],1);
	if($L['formType']=='select'){
		$Cols['col'.$coln]=$os['opts'];
	}
	$coln++;
}
$q=a_sql::query('SELECT C.cardId,R.docEntry,R.docYear,C.cardName, R1.askId, R1.value
FROM '._0s::$Tb['par_ocrd'].' C 
LEFT JOIN '._0s::$Tb['fqu_ofre'].' R ON (R.tt=\'card\' AND R.tr=C.cardId AND R.formId=\''.$formId.'\' AND R.docYear=\''.$docYear.'\')
LEFT JOIN '._0s::$Tb['fqu_fre1'].' R1 ON (R1.docEntry=R.docEntry)
WHERE C.actived=\'Y\' '.$wh.'
',array(1=>'Error obteniendo información del reporte: ',2=>'No se encontraron resultados registrados'));
if(a_sql::$err){ die(a_sql::$errNoText); }
$nl=0; $Ex=array();
while($L = $q->fetch_assoc()){
	$k='P'.$L['askId']; $docE=$L['cardId'];
	if(!array_key_exists($docE,$Ex)){ $Ex[$docE]=$nl; $nl++; }
	$nk=$Ex[$docE];
	$Mx['L'][$nk]['docYear']=$L['docYear'];
	$Mx['L'][$nk]['cardName']=$L['cardName'];
	$valu=($L['value'])?$L['value']:'SIN RESPUESTA-';
	$Mx['L'][$nk][$k]=$valu;
}
$Mx['jsConf']=array(
'FIEkv'=>'Y',
'textIs'=>array('undefined'=>'Sin Respuesta'),
'Opts'=>$Cols
);
echo _js::enc2($Mx);
?>