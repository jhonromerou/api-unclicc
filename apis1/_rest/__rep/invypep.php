<?php
$rootPATH = realpath($_SERVER['DOCUMENT_ROOT']);
require($rootPATH.'/__adms/__restNoApp.php');
$whsId=$___D['FIE'][0]['whsId'];
$wfaClass=$___D['FIE'][1]['wfaClass'];
unset($___D['FIE'][0],$___D['FIE'][1]);
if($js=_js::ise($whsId,'Se debe definir la Bodega. '.$whsId)){ die($js); }
if($js=_js::ise($wfaClass,'Se debe definir la clase de fase.')){ die($js); }
$Mx = array(
'kOrder'=>'itemSzId',
'FIE'=>array('itemCode'=>'Código','itemSzId'=>'Talla','onHand'=>'Stock','isCommited'=>'- Comprometido','onOrder'=>'+ Planif.','onWork'=>'+ En Proceso','acum'=>'= Acumulado'),'L'=>array(),
);
#/*
#actualizar producto en proceso

function pepQty($O=array(),$D=array()){
	$wfaC=a_sql::toSe($D['wfaClass'],'in');
	$q1 = a_sql::fetch('SELECT SUM(W.onHand) onWork 
	FROM pep_oitw W
	JOIN wma_owfa wfa ON (wfa.wfaId=W.wfaId)
	WHERE wfa.wfaClass '.$wfaC.' AND 
	W.itemId=\''.$O['itemId'].'\' AND W.itemSzId=\''.$O['itemSzId'].'\' ',array(1=>'Error obteniendo producto en proceso.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==2){ return array('onWork'=>0); }
	else return $q1;
}

function planQty($O=array()){
	$q1 = a_sql::fetch('SELECT SUM(B.progQty+B.openQty) onOrder 
FROM wma3_pdp1 B JOIN wma3_opdp A ON (B.docEntry=A.docEntry) 
WHERE A.docStatus=\'O\' AND (B.progQty+B.openQty)>0 AND B.itemId=\''.$O['itemId'].'\' AND B.itemSzId=\''.$O['itemSzId'].'\'',array(1=>'Error obteniendo planificación'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==2){ return array('onOrder'=>0); }
	else return $q1;
}
#*/
#invty
_ADMS::_lb('sql/filter');
$wh=a_sql_filtByT($___D);
$q=a_sql::query('SELECT I.itemCode,I.itemName, W.itemId,W.itemSzId, W.onHand, W.isCommited, W.onOrder, W.onWork 
FROM '._0s::$Tb['itm_oitw'].' W JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=W.itemId)
WHERE W.whsId=\'1\' '.$wh.'
');
while($L = $q->fetch_assoc()){
	$q3=planQty($L); $onOrder=$q3['onOrder']*1;
	$q4=pepQty($L,array('wfaClass'=>$wfaClass)); $onWork=$q4['onWork']*1;
	$onHand=$L['onHand']*1; $isCommited=$L['isCommited']*1;
	$acum=$onHand-$isCommited+$onOrder+$onWork;
	$Mx['L'][]=array('itemCode'=>$L['itemCode'],'itemSzId'=>$L['itemSzId'],'onHand'=>$onHand,'isCommited'=>$isCommited,'onOrder'=>$onOrder,'onWork'=>$onWork,'acum'=>$acum);
}

echo json_encode($Mx);
?>