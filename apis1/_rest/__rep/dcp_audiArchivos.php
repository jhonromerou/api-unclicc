<?php
$rootPATH = realpath($_SERVER['DOCUMENT_ROOT']);
require($rootPATH.'/__adms/__restNoApp.php');
$docYear=$___D['FIE'][0]['R.docYear(E_igual)'];

$Mx = array(
'kOrder'=>'cardName',
'FIE'=>array('RF_tipEnt'=>'Tipo Ent.','cardName'=>'Cliente'),
'L'=>array(),
);
$fatherId=59;
$formId=1;
$formCode='audBase';
_ADMS::_lb('sql/filter'); adms::mdLoad('geDoc');
$wh=a_sql_filtByT($___D);
$Cols=array(); $coln=3;
$fids='';
$q=a_sql::query('SELECT Fo.folId,Fo.folName
FROM '._0s::$Tb['gtd_offc'].' Fo
WHERE Fo.fatherId='.$fatherId.'
',array(1=>'Error obteniendo carpetas de datos de socio: ',2=>'No se encontraron carpetas.'));
if(a_sql::$err){ die(a_sql::$errNoText); }
while($L = $q->fetch_assoc()){
	$k='P'.$L['folId'];
	$Mx['FIE'][$k]=$L['folName'];
	$fids .=$L['folId'].',';
	$coln++;
}
$fids = substr($fids,0,-1);
$q=a_sql::query('SELECT C.cardId,C.RF_tipEnt,C.cardName, F1.folId,F1.fileId
FROM '._0s::$Tb['par_ocrd'].' C 
LEFT JOIN '._0s::$Tb['gtd_ffc1'].' F1 ON (F1.tt=\'card\' AND F1.tr=C.cardId AND F1.folId IN('.$fids.')) 
WHERE actived=\'Y\' '.$wh.'
',array(1=>'Error obteniendo información del reporte: ',2=>'No se encontraron resultados registrados'));
if(a_sql::$err){ die(a_sql::$errNoText); }
$nl=0; $Ex=array();
while($L = $q->fetch_assoc()){
	$k='P'.$L['folId']; $docE=$L['cardId'];
	if(!array_key_exists($docE,$Ex)){ $Ex[$docE]=$nl; $nl++; }
	$nk=$Ex[$docE];
	$Mx['L'][$nk]['RF_tipEnt']=$L['RF_tipEnt'];
	$Mx['L'][$nk]['cardName']=$L['cardName'];
	$valu=($L['fileId']>0)?'Si':'No';
	$Mx['L'][$nk][$k]=$valu;
}
$Mx['jsConf']=array(
'FIEkv'=>'Y',
'textIs'=>array('undefined'=>'Sin Respuesta'),
'Opts'=>$Cols
);
echo _js::enc2($Mx);
?>