<?php
_0s::$Tb['gtd_ffc1']='gtd_ffc1';
if(_0s::$router=='POST upload'){
	echo Attach::curSend($_FILES['file'],array('revStor'=>'Y'));
}
else if(_0s::$router=='DELETE file'){
	if($js=_js::ise($___D['fileId'],'Se debe definir ID del archivo.','numeric>0')){}
	else if($js=_js::ise($___D['svrFileId'],'Se debe definir svrFileId del archivo.','numeric>0')){}
	else{
		a_sql::$DBn=1;
		$R=Attach::getInfo(array('ocard'=>'A.tbFile,A.filePath','ofil'=>'F.svr,F.svrLocal'));
		$q=a_sql::fetch('SELECT url FROM '.$R['tbFile'].' WHERE svrFileId=\''.$___D['svrFileId'].'\' LIMIT 1',array(1=>'Error obteniendo información de archivo at adms.'));
		if(a_sql::$err){ $js=a_sql::$errNotext; }
		else if(a_sql::$errNo==2){ $js=_js::e(3,'El archivo no existe o fue eliminado on adms.db.'); }
		else{
			$Rc=_curl::post($R['svr'].'/deletef_ile/',array('D'=>array('file'=>$R['filePath'].$q['url'])));
			if(_curl::$err){ echo _curl::$errText; }
			if(!$Rc['errNo']){
				a_sql::query('DELETE FROM '.$R['tbFile'].' WHERE svrFileId=\''.$___D['svrFileId'].'\' LIMIT 1',array(1=>'Error eliminando archivo on adms.db.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; }
				else{
					a_sql::$DBn=false;
					$q=a_sql::query('DELETE FROM '._0s::$Tb['gtd_ffc1'].' WHERE fileId=\''.$___D['fileId'].'\' LIMIT 1',array(1=>'Error eliminando archivo on local.db.'));
					if(a_sql::$err){ $js=a_sql::$errNoText; }
					else{ $js= _js::r('Archivo eliminado correctamente.'); }
				}
			}
		}
	}
	echo $js;
}

else if(_0s::$router=='GET tt/up'){
	if($js=_js::ise($___D['tt'],'Se debe definir tt para relacionar archivos.')){}
	else if($js=_js::ise($___D['tr'],'Se debe definir tr para relacionar archivos.')){}
	else{
		$q=a_sql::query('SELECT * FROM '._0s::$Tb['gtd_ffc1'].' WHERE tt=\''.$___D['tt'].'\' AND tr=\''.$___D['tr'].'\' LIMIT 30',array(1=>'Error obteniendo listado de archivos tt/up.',2=>'No se encontraron archivos en tt/up.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $M=array('L'=>array());
			while($L=$q->fetch_assoc()){
				if($L['userId']==a_ses::$userId || $L['userId']==1){
					$L['canDelete']='Y';
				}
				$M['L'][]=$L;
			}
			$js=_js::enc($M);
		}
	}
	echo $js;
}
else if(_0s::$router=='POST tt/up'){
	$Lx=$___D['L']; unset($___D['L']);
	if($js=_js::ise($___D['tt'],'Se debe definir tt para relacionar archivos.')){}
	else if($js=_js::ise($___D['tr'],'Se debe definir tr para relacionar archivos.')){}
	else if(!is_array($Lx)){ $js=_js::e(3,'No se enviaron archivos a guardar.'); }
	else{
		$n=0; $errs=0; $files=0;
		a_sql::transaction(); $cmt=false;
		$Ls=array();
		foreach($Lx as $nx => $L){ $n++;
			$ln='Linea '.$n.': ';
			$L['tt']=$___D['tt']; $L['tr']=$___D['tr'];
			$L['dateC']=date('Y-m-d H:i:s');
			$ins=a_sql::insert($L,array('tbk'=>'gtd_ffc1','qDo'=>'insert','kui'=>'uid'));
			if($ins['err']){ $js=_js::e(3,$ln.'Error guardando archivo . '.$ins['text']); $errs++; break; }
			else{ $files++;
				$L['versNum']=1;
				$L['fileId']=$ins['insertId'];
				$L['userId']=a_ses::$userId;
				$Ls[]=$L;
			}
		}
		if($errs==0){ $js=_js::r('Se subieron '.$files.' archivos.',$Ls); $cmt=true; }
		a_sql::transaction($cmt);
	}
	echo $js;
}


else if(_0s::$router=='POST uploadCurl'){ #call from curl - relace with curSend 
	$Fi=$_FILES['file'];
	if(!is_array($Fi)){ $js=_js::e(3,'No se recibió ningun archivo.'); }
	else{
		$path=$___D['_filePath']; #/var/mnt/dcp
		$ext=Attach::getExt($Fi['name']);
		$fileName=time().mt_rand().'.'.$ext;
		$filePath=$fileName;
		$fileName= $Fi['name'];
		$fileType=Attach::getTypeMe($ext);
		$copya= Comm::copy($Fi['tmp_name'],$filePath,$path); //@copy($Fi['tmp_name'],$filePath);
		if(!$copya){ echo Comm::$errText; }
		else{
			$M=array('text'=>'Archivos guardados correctamente','L'=>array());
			$fileSize=Attach::getFileSize($Fi['size']);
			$fileSizeText= Attach::getFileSizeText($Fi['size']);
			a_sql::dbase(Attach::$Svr);
			a_sql::transaction(); $cmt=false;
			$Di=array('fileName'=>$fileName,
			'fileType'=>$fileType,'mimeType'=>$Fi['type'],
			'svr'=>$___D['_svr'],'url'=>$filePath,'fileSize'=>$fileSize,'fileSizeText'=>$fileSizeText);
			$ins=a_sql::insert($Di,array('table'=>$___D['_tbFile'],'qDo'=>'insert','kui'=>'dateC'));
			if($ins['err']){
				@unlink(Comm::$cD['filePath']);
				$js=_js::e(1,'Error guardando archivo en Attach::uploadCurl. '.$ins['text']);
			}
			else{ $cmt=true;
				unset($Di['url'],$Di['mimeType']);
				//$Di['svrTooken']=_jwt::encText(_0s::$ocardcode.'.'.$ins['insertId']);
				$Di['svrFileId']=$ins['insertId'];
				$q3=a_sql::query('UPDATE ofil SET storOpen=storOpen-'.$fileSize.', storUsaged=storUsaged+'.$fileSize.' WHERE ocardId=\''.$___D['_ocardId'].'\' LIMIT 1',array(1=>'Error actualizando control de almacenamiento Attach::uploadCurl. '));
				if(a_sql::$err){
					@unlink(Comm::$cD['filePath']);
					$js=a_sql::$errNoText;
				}
				else{
					$M['L'][]=$Di;
					$js=_js::enc2($M);
				}
			}
			a_sql::transaction($cmt);
		}
	}
	echo $js;
}

?>