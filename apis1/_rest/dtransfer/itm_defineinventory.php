<?php
$ALM=$Fas=array();
if(a_ses::$userId!=1){
	die(_js::e(3,'Acción deshabilitada'));
}
/* Definir bodegas y fases */
$qA=a_sql::query('SELECT whsId,whsCode FROM itm_owhs WHERE whsType=\'P\' ',array(1=>'Error obteniendo bodegas de producto en proceso',2=>'No hay definidas bodegas de producto en proceso'));
if(a_sql::$err){ die(a_sql::$errNoText); }
else while($L=$qA->fetch_assoc()){
	$k=strtolower($L['whsCode']);
	$ALM[$k]=$L['whsId'];
}
/* Leer archivo */
$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>900,
'K'=>array('almacen','fase','itemCode','talla','onHand'))
);
$Di2=array();
$IT=array();
$LnE=array(); /*Evitar duplicados */
if($R['errNo']){ $js = _js::e($R); }
else{
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		$whsId=$Da['almacen']=strtolower($Da['almacen']);
		$kDup=$whsId.'_'.$Da['itemCode'].'_'.$Da['talla'];
		$lnAnt=$LnE[$kDup];
		if(array_key_exists($kDup,$LnE)){
			die(_js::e(3,$lnt.'No puede definir lineas iguales. Ya existe esta combinación en : '.$lnAnt));
		}
		$LnE[$kDup]=$ln;
		$Da['onHand']=str_replace(',','.',$Da['onHand']);
		if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['onHand'],$lnt.'Se debe definir la cantidad disponible')){ die($js); }
		else if(!array_key_exists($whsId,$ALM)){ die(_js::e(3,'El almacen '.$whsId.' no tiene Id definida.')); }
		$itk=$Da['itemCode'];
		if(!array_key_exists($itk,$IT)){
			$q1=a_sql::fetch('SELECT I.itemId FROM itm_oitm I WHERE I.itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información del artículo.',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			$It[$itk]=$q1['itemId'];
		}
		$itemId=$It[$itk];
		$whsId=$ALM[$whsId];
		$Di2[]=array('whsId'=>$whsId,'itemId'=>$itemId,'itemSzId'=>$Da['talla'],'onHand'=>$Da['onHand']);
		
	}
	if($errs==0){
		a_sql::transaction(); $comit=false;
		foreach($Di2 as $n =>$Di){
			$ins=a_sql::insert($Di,array('table'=>'pep_oitw','wh_change'=>'WHERE itemId=\''.$Di['itemId'].'\' AND itemSzId=\''.$Di['itemSzId'].'\' AND whsId=\''.$Di['whsId'].'\' AND wfaId=\''.$Di['wfaId'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error actualizando definiendo cantidad disponible: '.$ins['text']); $errs++; break; }
		}
		if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
		a_sql::transaction($comit);
	}
}
echo $js;
?>