<?php
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('docEntry','itemCode','itemSize','quantity'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	_ADMS::_lb('itm');
	$date1=date('Y-m-d');
	$date2=date('Y-m-d',strtotime($date1)+(86400*3));
	$docEntry=$R['L'][3]['docEntry'];
	/* $mx=a_sql::fetch('SELECT MAX(docEntry)-1 docEntryMax FROM wma3_opdp ',array(1=>'Error obteniendo consecutivo siguiente de sistema: ',2=>'No se encontraron resultados para documento máximo'));
	a_sql::transaction(); $comit=false;
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if($docEntry<=$mx['docEntryMax']){ die(_js::e(3,'Documento #'.$docEntry.', es menor al consecutivo máximo en el sistema ('.$mx['docEntryMax'].').')); } */
	$qd=a_sql::fetch('SELECT isImport FROM wma3_opdp WHERE docEntry=\''.$docEntry.'\' LIMIT 1',array(1=>'Error obteniendo documento de planiación: '));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1 && $qd['isImport']!='Y'){ die(_js::e(3,'El documento '.$docEntry.', no es de tipo importación, no se puede realizar el cargue de información.')); }
	$ins=a_sql::insert(array('docEntry'=>$docEntry,'isImport'=>'Y','docDate'=>$date1,'dueDate'=>$date2),array('table'=>'wma3_opdp','kui'=>'uid_dateC','wh_change'=>'WHERE docEntry=\''.$docEntry.'\' LIMIT 1'));
	$num=1; $Di=array();
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$qi=_itm::get_itemIdSize($Da);
		if(!$qi){ die(a_sql::$errNoText); }
		else if($js=_js::ise($Da['quantity'],$lnt.'La cantidad debe ser un número y mayor a 0.','numeric>0')){ die($js); }
		else{
			$Di[$num]=array('docEntry'=>$docEntry,'lineNum'=>$num,'itemId'=>$qi['itemId'],'itemSzId'=>$qi['itemSzId'],'quantity'=>$Da['quantity'],'openQty'=>$Da['quantity']); $num++;
		}
	}
	a_sql::query('DELETE FROM wma3_pdp1 WHERE docEntry=\''.$docEntry.'\' LIMIT 1000',array(1=>'Error redefiniendo documento de planiación: '));
	;
	if(a_sql::$err){ die(a_sql::$errNoText); }
	foreach($Di as $n => $L){
		$ins=a_sql::insert($L,array('qDo'=>'insert','table'=>'wma3_pdp1'));
		if($ins['err']){ $js=_js::e(3,'Error guardando lineas: '.$ins['text']); $errs++; break; }
	}
	if($errs==0){ $js=_js::r('Lineas guardadas correctamente.');$comit=true; }
	a_sql::transaction($comit);
}
echo $js;
?>