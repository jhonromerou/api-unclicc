<?php
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('docEntry','docDate','docClass','lineMemo','_L_',
'itemCode','itemSize','whsId','quantity'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	$Di=array();
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		if($js=_js::ise($Da['docEntry'],$lnt.'Se debe definir el número de documento','numeric>0')){ die($js); }
		else if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['itemSize'],$lnt.'Se debe definir la talla del producto.')){ die($js); }
		else if($js=_js::ise($Da['whsId'],$lnt.'Se debe definir el código de la bodega.')){ die($js); }
		else if($js=_js::ise($Da['quantity'],$lnt.'Se debe definir la cantidad y ser un número mayor a 0.','numeric>0')){ die($js); }
		else if($js=verify::itemCode($Da['itemCode'])){ die($js); }
		else if($js=verify::itemSize($Da['itemSzId'])){ die($js); }
		else if($js=verify::whsCode($Da['whsId'])){ die($js); }
		else{
			$Dq=verify::$D; verify::_resetAll();
			$Di=array('docEntry'=>$Da['docEntry'],'whsId'=>$Dq['whsId'],'itemId'=>$Dq['itemId'],'itemSzId'=>$Dq['itemSzId'],'quantity'=>$Da['quantity']);
			$ins=a_sql::insert($Di,array('tbk'=>'ivt_awh1','qDo'=>'insert'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error registrando linea: '.$ins['text']); $errs++; break; }
		}
	}
	if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
	a_sql::transaction($comit);
}
echo $js;
?>