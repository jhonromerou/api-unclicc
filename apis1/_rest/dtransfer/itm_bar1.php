<?php
$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>5000,
'K'=>array('itemCode','grTypeId','itemSzId','barCode'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['grTypeId'],$lnt.'Se debe definir el grupo de código de barras.','numeric>0')){ die($js); }
		else if($js=_js::ise($Da['itemSzId'],$lnt.'Código de talla debe estar definido.')){ die($js); }
		else if($js=_js::ise($Da['barCode'],$lnt.'Se debe definir el código de barras a asignar.')){ die($js); }
			else if(!_js::textLimit($Da['barCode'],20)){ die(_js::e(3,$lnt.'El código no puede exceder 20 caracteres.')); }
		$q1=a_sql::fetch('SELECT itemId FROM itm_oitm WHERE itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo artículo',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		
		$q2=a_sql::fetch('SELECT itemSzId FROM itm_grs1 WHERE ocardId=\''.a_ses::$ocardId.'\' AND itemSize=\''.$Da['itemSzId'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo talla',2=>$lnt.'La talla '.$Da['itemSzId'].' no existe'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{
			$Di=array('itemId'=>$q1['itemId'],'itemSzId'=>$q2['itemSzId'],'grTypeId'=>$Da['grTypeId'],'barCode'=>$Da['barCode']);
			$ins=a_sql::insert($Di,array('table'=>'itm_bar1','wh_change'=>'WHERE grTypeId=\''.$Da['grTypeId'].'\' AND itemId=\''.$q1['itemId'].'\' AND itemSzId=\''.$q2['itemSzId'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error asignado código: '.$ins['text']); $errs++; break; }
		}
	}
	if($errs==0){ $comit=true; $js=_js::r('Se actualizar '.$lineTotal.' lineas.'); }
	a_sql::transaction($comit);
}
echo $js;
?>