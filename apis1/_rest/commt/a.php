<?php
_0s::$Tb['gtd_ffc1']='gtd_ffc1';

class appCommt{
static public function ttc($P=array(),$num=1){
	if($js=_js::ise($P['tt'],'Se debe definir tt para registrar el comentario.')){}
	else if($js=_js::ise($P['tr'],'Se debe definir tr para registrar el comentario.')){}
	else{
		$Di=array('tt'=>$P['tt'],'tr'=>$P['tr'],'commets='=>'commets+'.$num,'commets'=>1);
		$a=a_sql::oneMulti($Di,array('tbk'=>'app_ottc','qDo'=>'uniRow','wh_change'=>'tt=\''.$P['tt'].'\' AND tr=\''.$P['tr'].'\' LIMIT 1'));
		if(a_sql::$err){ _err::err('Error registrando conteo de comentarios: '.a_sql::$errText,3); } 
	}
}
}

if(_0s::$router=='GET '){
	$wh=($___D['tt'])?' AND A.tt=\''.$___D['tt'].'\' AND A.tr=\''.$___D['tr'].'\' ':'';
	$q=a_sql::query('SELECT A.* FROM app_com1 A 
WHERE 1 '.$wh.' ORDER BY dateC DESC LIMIT 50',array(1=>'Error obteniendo listado de comentarios.',2=>'No se encontraron comentarios registrados.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$M=array('L'=>array());
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		}
		$js=_js::enc($M);
	}
	echo $js;
}
else if(_0s::$router=='POST '){
	$ottc=($_J['ottc']); unset($_J['ottc']);
	if($js=_js::ise($_J['tt'],'Se debe definir tt para registrar el comentario.')){}
	else if($js=_js::ise($_J['tr'],'Se debe definir tr para registrar el comentario.')){}
	else if($js=_js::ise($_J['comment'],'Debe realizar el comentario.')){}
	else{
		$n=0; $errs=0; $files=0;
		a_sql::transaction(); $cmt=false;
		$ins=a_sql::oneMulti($_J,array('tbk'=>'app_com1','qDo'=>'insert','qk'=>'ud','retD'=>true,'insertId'=>'id'));
		if(a_sql::$err){ $js=_js::e(3,$ln.'Error guardando comentario: '.a_sql::$errText); }
		else{
			if($ottc=='Y'){  appCommt::ttc($_J); }
			if(_err::$err){ $js=_err::$errText; }
			else{ $cmt=true;
				$js=_js::r('Comentario realizado correctemente',$ins['_D']);
			}
			a_sql::transaction($cmt);
		}
	}
	echo $js;
}
else if(_0s::$router=='DELETE '){
	$id = $___D['commentId'];
	if($js=_js::ise($id,'ID de comentario no definida.')){ die($js); }
	$gFile = a_sql::fetch('SELECT userId,tt,tr FROM app_com1 
	WHERE id=\''.$id.'\' LIMIT 1',array(1=>'No se pudo eliminar el comentario: '));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	if(a_sql::$errNo=='-1' && $gFile['userId'] != a_ses::$userId){
		die(_js::e(4,'No tiene permisos para eliminar comentarios de otro usuario.'));
	}
	else{
		$ottc=($___D['ottc']);
		$cList = a_sql::query('DELETE FROM app_com1 WHERE id=\''.$id.'\' LIMIT 1',array(1=>'Error eliminando comentario: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			if(a_sql::$DB->affected_rows>0){
				if($ottc=='Y'){  appCommt::ttc($gFile,'-1'); }
				if(_err::$err){ $js=_err::$errText; }
			}
			$js = _js::r('Eliminado correctamente.','"id":"'.$id.'"');
		}
	}
	echo $js;
}
?>