<?php
_0s::$Tb['gtd_ffc1']='gtd_ffc1';
if(_0s::$router=='GET '){
	$wh=($___D['tt'])?' AND A.tt=\''.$___D['tt'].'\' AND A.tr=\''.$___D['tr'].'\' ':'';
	$q=a_sql::query('SELECT A.* FROM '._0s::$Tb['app_com1'].' A 
WHERE 1 '.$wh.' ORDER BY dateC DESC LIMIT 200',array(1=>'Error obteniendo listado de comentarios.',2=>'No se encontraron comentarios registrados.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$M=array('L'=>array());
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		}
		$js=_js::enc($M);
	}
	echo $js;
}
else if(_0s::$router=='POST '){
	if($js=_js::ise($___D['tt'],'Se debe definir tt para registrar el comentario.')){}
	else if($js=_js::ise($___D['tr'],'Se debe definir tr para registrar el comentario.')){}
	else if($js=_js::ise($___D['comment'],'Debe realizar el comentario.')){}
	else{
		$n=0; $errs=0; $files=0;
		a_sql::transaction(); $cmt=false;
		$ins=a_sql::insert($___D,array('table'=>'app_com1','qDo'=>'insert','kui'=>'ud','retD'=>true,'insertId'=>'id'));
		if($ins['err']){ $js=_js::e(3,$ln.'Error guardando comentario: '.$ins['text']); }
		else{
			//Obj::logg('comment',array('oTy'=>$___D['tt'],'gid'=>$___D['tr'],'vb'=>'comment'));
			if($___D['tt']=='gtdTask'){
				lTask::news_write(array('gid'=>$___D['tr'],'k'=>'comments'));
			}
			a_sql::transaction(true);
			$js=_js::r('Comentario realizado correctemente',$ins['_D']);
		}
	}
	echo $js;
}
else if(_0s::$router=='DELETE '){
	$id = $___D['commentId'];
	if($js=_js::ise($id,'ID de comentario no definida.')){ die($js); }
	$gFile = a_sql::fetch('SELECT userId,tt,tr FROM '._0s::$Tb['app_com1'].' 
	WHERE id=\''.$id.'\' LIMIT 1',array(1=>'No se pudo eliminar el comentario: '));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	if(a_sql::$errNo=='-1' && $gFile['userId'] != a_ses::$userId){
		die(_js::e(4,'No tiene permisos para eliminar comentarios de otro usuario.'));
	}
	else{
		$cList = a_sql::query('DELETE FROM '._0s::$Tb['app_com1'].' WHERE id=\''.$id.'\' LIMIT 1',array(1=>'Error eliminando comentario: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			if(a_sql::$DB->affected_rows>0){
				if($gFile['tt']=='gtdTask'){
					lTask::news_write(array('gid'=>$gFile['tr'],'k'=>'comments','minus'=>'Y'));
				}
			}
			$js = _js::r('Eliminado correctamente.','"id":"'.$id.'"');
		}
	}
	echo $js;
}
?>