<?php
$fies='A.calId,A.endDate,A.endDateAt,A.completed,A.priority,A.shortDesc,A.userAssg,A.status';
if(_0s::$router=='GET task'){ a_ses::hashKey('crmBasic');
	_ADMS::_lb('sql/filter');
	a_sql::$limitDefBef=50;
	$___D['wh']['A.obj']='task';
	$___D['_fie']=$fies;
	echo crm::nov_get($___D,array('prsLeft'=>'Y',
	'Join'=>array('AP.commets','LEFT JOIN app_ottc AP ON (AP.tt=\'crmNov\' AND AP.tr=A.gid) ')
	));
}
else if(_0s::$router=='GET task/view'){ a_ses::hashKey('crmBasic');
	$___D['_fie']='A.*';
	echo crm::nov_one($___D);
}
else if(_0s::$router=='GET task/form'){ a_ses::hashKey('crmBasic');
	$___D['_fie']=$fies.',A.repeatKey';
	echo crm::nov_one($___D);
}
else if(_0s::$router=='POST task'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textLen($___D['title'],200,'EL asunto no puede exceder los 200 caracteres.')){}
	else{
		$___D['obj']='task';
		$___D['doDate']=$___D['endDate'];
		$___D=crm::nov_post($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{ $js=_js::r('Tarea creada correctamente.',$___D); }
	}
	echo $js;
}
else if(_0s::$router=='PUT task'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
	else if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textLen($___D['title'],200,'EL asunto no puede exceder los 200 caracteres.')){}
	else{
		$___D['doDate']=$___D['endDate'];
		$___D=crm::nov_put($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{ $js=_js::r('Tarea actualizada correctamente.',$___D); }
	}
	echo $js;
}
else if(_0s::$router=='PUT task/markCompleted'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
	else{
		$errs=0;
		$Di=array('completed'=>$___D['completed']);
		a_sql::transaction();
		$doRep=null;
		if($___D['completed']=='Y'){
			$Di['completedAt']=date('Y-m-d H:i:s');
			$Di['completUser']=a_ses::$userId;
			$L=a_sql::duplicatGet(array('tbk'=>'crm_onov','wh'=>'gid=\''.$___D['gid'].'\'','noFie'=>'gid','fN'=>array('gidParent'=>$___D['gid'])));
			if(_err::$err){ die(_err::$errText); }
			/* Duplicar tarea si ya no tiene repeticion */
			if($L['repeatKey']!='' && $L['repeatKey']!='N' && $L['repeatDo']=='N'){
				_ADMS::lib('dateCicle');
				$L['completDate']=$Di['completedAt'];
				$R=dateCicle::calc($L['repeatKey'],$L);
				if(!is_array($R)){ $R=array(); }
				unset($L['completDate'],$L['status']);
				$L['doDate']=$R['nextDate'];
				$L['endDate']=$R['nextDate'];
				$doRep=crm::nov_post($L,array('err1'=>'Error duplicando tarea'));
				if(_err::$err){ $js=_err::$errText; }
				$Di['repeatDo']='Y';
			}
		}
		else{ $Di['completedAt']=''; $Di['completUser']=0; }
		if($errs==0){
			$ins=a_sql::insert($Di,array('table'=>'crm_onov','qDo'=>'update','updater'=>'uid_dateC','wh_change'=>'WHERE gid=\''.$___D['gid'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,'Error actualizando tarea: '.$ins['err']['error_sql']); }
			else{ $js=_js::r('Tarea cambio de estado correctamente.',$doRep);
				a_sql::transaction(true);
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='POST task/sendCopy'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($_J['gid'],'Se debe definir Id de tarea','numeric>0')){}
	else if($js=_err::iff(count($_J['email'])==0,'No se definieron correos para enviar la copia.')){}
	else if($js=_err::iff(count($_J['email'])>2,'No se puede enviar más de 3 copias.')){}
	else{
		$qT=a_sql::fetch('SELECT U.userName, U2.userName userNameFrom, A.title,A.shortDesc,P.name, C.cardName,A.endDate,A.endDateAt 
		FROM crm_onov A
		LEFT JOIN a0_vs0_ousr U ON (U.userId=A.userId)
		LEFT JOIN a0_vs0_ousr U2 ON (U2.userId=A.userAssg)
		LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
		LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.gid=\''.$_J['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información de la tarea a copiar.',2=>'La tarea no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			$qT['mailTo']=implode(',',$_J['email']);
			sendCopyTask($qT);
			if(xCurl::$err){ $js=xCurl::$errText; }
			else{ $js=_js::r('Copias Enviadas.'); }
		}
	}
	echo $js;
}
?>