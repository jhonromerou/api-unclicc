<?php
$fies='A.doDate,A.priority,A.userAssg,A.shortDesc';
if(_0s::$router=='GET nov'){ a_ses::hashKey('crmBasic');
	_ADMS::_lb('sql/filter');
	$___D['wh']['A.obj']='nov';
	$___D['_fie']=$fies;
	echo crm::nov_get($___D,array('prsLeft'=>'Y'));
}
else if(_0s::$router=='GET nov/form'){ a_ses::hashKey('crmBasic');
	$___D['_fie']=$fies;
	echo crm::nov_one($___D);
}
else if(_0s::$router=='GET nov/view'){ a_ses::hashKey('crmBasic');
	$___D['_fie']=$fies;
	echo crm::nov_one($___D);
}
else if(_0s::$router=='POST nov'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['shortDesc'],'Se debe digitar la novedad.')){}
	else if($js=_js::textLen($___D['shortDesc'],1200,'La novedad no puede exceder 1200 caracteres.')){}
	else{
		$___D['obj']='nov';
		$ins=a_sql::insert($___D,array('table'=>'crm_onov','kui'=>'uid_dateC','qDo'=>'insert','updater'=>'uid_dateC'));
		if($ins['err']){ $js=_js::e(3,'Error generando novedad: '.$ins['err']['error_sql']); }
		else{
			$q=crm::nov_gOne($___D);
			if(!a_sql::$err){ $___D=$q;}
			$js=_js::r('Novedad creada correctamente.',$___D);
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT nov'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
		if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['shortDesc'],'Se debe digitar la novedad.')){}
	else if($js=_js::textLen($___D['shortDesc'],1200,'La novedad no puede exceder 1200 caracteres.')){}
	else{
		$___D=crm::nov_put($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{ $js=_js::r('Novedad actualizada correctamente.',$___D); }
	}
	echo $js;
}
?>