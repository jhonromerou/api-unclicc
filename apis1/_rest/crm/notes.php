<?php
if(_0s::$router=='GET notes'){ a_ses::hashKey('crmBasic');
	_ADMS::_lb('sql/filter');
	$___D['wh']['A.obj']='note';
	$___D['_fie']='A.doDate,A.shortDesc';
	echo crm::nov_get($___D,array('prsLeft'=>'Y'));
}
else if(_0s::$router=='GET notes/view'){ a_ses::hashKey('crmBasic');
	$___D['_fie']='A.doDate,A.userAssg,A.description';
	echo crm::nov_one($___D);
}
else if(_0s::$router=='GET notes/form'){ a_ses::hashKey('crmBasic');
	$___D['_fie']='description';
	echo crm::nov_one($___D);
}
else if(_0s::$router=='POST notes'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textLen($___D['title'],200,'El asunto no puede exceder los 200 caracteres.')){}
	else{
		$___D['obj']='note';
		$___D['shortDesc']=substr($___D['description'],0,120);
		$___D=crm::nov_post($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{$js=_js::r('Tarea generada correctamente.',$___D); }
	}
	echo $js;
}
else if(_0s::$router=='PUT notes'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
	else if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textLen($___D['title'],200,'El asunto no puede exceder los 200 caracteres.')){}
	else{
		$___D['shortDesc']=substr($___D['description'],0,120);
		$___D=crm::nov_put($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{ unset($___D['description']);
		$js=_js::r('Nota actualizada correctamente.',$___D); }
	}
	echo $js;
}
?>