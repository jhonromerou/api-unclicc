<?php
if(_0s::$router=='GET ffc'){
	echo geDoc::get(array('wh'=>'A.oTy=\'card\' '));
}

else if(_0s::$router=='GET ffc.open'){
	if($js=_js::ise($___D['folId'],'Se debe definir el ID de la carpeta')){ die($js); }
	if($js=_js::ise($___D['cardId'],'Se debe definir el ID del contacto.')){ die($js); }
	$lW=geDoc::usershare();
	$M=a_sql::fetch('SELECT A.* FROM gtd_offc A '.$lW['left'].'
	WHERE A.folId=\''.$___D['folId'].'\' '.$lW['wh'].' LIMIT 1',array(1=>'Error obteniendo información de carpeta',2=>'La carpeta no existe o no tiene permisos suficientes.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$q=a_sql::query('SELECT B1.* FROM gtd_ffc1 B1
	JOIN gtd_offc A ON (A.folId=B1.folId)
	WHERE A.folId=\''.$___D['folId'].'\' AND B1.cardId=\''.$___D['cardId'].'\'
	',array(1=>'Error obteniendo archivos de carpeta: ',2=>'La carpeta no tiene ningun archivo.'));
	$M['L']=array();
	if(a_sql::$err){$M['L']=json_decode(a_sql::$errNoText,1); }
	else{
		while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
	}
	$js=_js::enc($M,'just'); unset($M);
	echo $js;
}

/* Papeles de Trabajo */
else if(_0s::$router=='GET ffc/pwork'){ a_ses::hashKey('geDoc.card');
	//if(a_ses::$userId==1){ echo 'ALERTJSON '; }
	$M['L']=geDoc::get(array('wh'=>'A.isPWork=\'Y\' '));
	echo _js::enc2($M);
}
else if(_0s::$router=='GET ffc/pwork.open'){ a_ses::hashKey('geDoc.card');
	_ADMS::lib('Attach');
	$M= geDoc::getFolder(array('folId'=>$___D['folId'],'perms'=>'R'));
	if(_err::$err){ die(_err::$errText); }
	$q=a_sql::query('SELECT B1.*
	FROM gtd_ffc1 B1
	JOIN gtd_offc A ON (A.folId=B1.folId)
	WHERE A.folId=\''.$___D['folId'].'\' AND tt=\'pwork\' AND tr=\'1\'
	',array(1=>'Error obteniendo archivos de carpeta: ',2=>'La carpeta no tiene ningun archivo (ffc/card).'));
	$M['L']=array();
	if(a_sql::$err){$M['L']=json_decode(a_sql::$errNoText,1); }
	else{
		while($L=$q->fetch_assoc()){
			$L['url']=Attach::getLink($L);
			$M['L'][]=$L;
		}
	}
	$js=_js::enc($M,'just'); unset($M);
	echo $js;
}

/* obtener carpeta especifica */
else if(_0s::$router=='GET ffc/oty'){ a_ses::hashKey('geDoc.card');
	_ADMS::lib('Attach');
	$M= geDoc::getFolder(array('WH'=>'A.oTy=\''.$___D['oTy'].'\'','perms'=>'R'));
	if(_err::$err){ die(_err::$errText); }
	$q=a_sql::query('SELECT B1.*
	FROM gtd_ffc1 B1
	JOIN gtd_offc A ON (A.folId=B1.folId)
	WHERE A.folId=\''.$M['folId'].'\'
	',array(1=>'Error obteniendo archivos de carpeta: ',2=>'La carpeta no tiene ningun archivo (ffc/card).'));
	$M['L']=array();
	if(a_sql::$err){$M['L']=json_decode(a_sql::$errNoText,1); }
	else{
		while($L=$q->fetch_assoc()){
			$L['url']=Attach::getLink($L);
			$M['L'][]=$L;
		}
	}
	$js=_js::enc($M,'just'); unset($M);
	echo $js;
}

/* socios */
else if(_0s::$router=='GET ffc/cards'){ a_ses::hashKey('geDoc.card');
	$wh=''; _ADMS::_lb('sql/filter');
	$slps=a_ses::U_slpIds(array('f'=>'A.slpId','r'=>'in'));
	$wh .= a_sql_filtByT($___D);
	$q=a_sql::query('SELECT A.cardId,A.cardCode,A.cardName FROM par_ocrd A WHERE 1 '.$slps.' '.$wh.' '.a_sql::nextLimit(),array(1=>'Error obteniendo listado de socios de negocios: ',2=>'No se encontraron socios de negocios.'));
	if(a_sql::$errNoText!=''){ $js=a_sql::$errNoText; }
	else{$M=array('L'=>array());
		while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
		$js=_js::enc($M);
	}
	echo $js;
}
else if(_0s::$router=='GET ffc/card'){ a_ses::hashKey('geDoc.card');
	$slps=a_ses::U_slpIds(array('f'=>'slpId','r'=>'in'));
	$M=a_sql::fetch('SELECT cardId,cardName,RF_tipEnt FROM par_ocrd A WHERE cardId=\''.$___D['cardId'].'\' '.$slps.' LIMIT 1 ',array(1=>'Error obteniendo información del cliente para listar carpetas.',2=>'El socio buscando no existe o no tiene permisos para visualizar las carpetas.'));
	if(a_sql::$err){ echo a_sql::$errNoText; }
	else{
		$M['L']=geDoc::getOTy(array('oTy'=>'card','wh'=>'A.folGrCode IN(\'GN\',\''.$M['RF_tipEnt'].'\') ','err1'=>'Error obteniendo carpetas del socio','err2'=>'No se encontraron carpetas definidas para el tipo de socio.'));
		echo _js::enc2($M);
	}
}
else if(_0s::$router=='GET ffc/card.open'){ a_ses::hashKey('geDoc.card');
	_ADMS::lib('Attach');
	if($js=_js::ise($___D['folId'],'Se debe definir el ID de la carpeta')){ die($js); }
	if($js=_js::ise($___D['cardId'],'Se debe definir el ID del contacto.')){ die($js); }
	$M=a_sql::fetch('SELECT A.* FROM gtd_offc A
	WHERE A.folId=\''.$___D['folId'].'\' LIMIT 1',array(1=>'Error obteniendo información de carpeta',2=>'La carpeta no existe o no tiene permisos suficientes.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$q=a_sql::query('SELECT B1.* FROM gtd_ffc1 B1
	JOIN gtd_offc A ON (A.folId=B1.folId)
	WHERE A.folId=\''.$___D['folId'].'\' AND tt=\'card\' AND tr=\''.$___D['cardId'].'\'
	',array(1=>'Error obteniendo archivos de carpeta: ',2=>'La carpeta no tiene ningun archivo (ffc/card).'));
	$M['L']=array();
	if(a_sql::$err){$M['L']=json_decode(a_sql::$errNoText,1); }
	else{
		while($L=$q->fetch_assoc()){
			$L['url']=Attach::getLink($L);
			$M['L'][]=$L;
		}
	}
	$js=_js::enc($M,'just'); unset($M);
	echo $js;
}

else if(_0s::$router=='POST ffc/fileUp'){
	$Lx=$_J['L']; unset($_J['L']);
	if($js=_js::ise($_J['folId'],'Se debe definir el ID de la carpeta','numeric>0')){}
	else if(!is_array($Lx)){ $js=_js::e(3,'No se enviaron archivos a guardar en la carpeta.'); }
	else{
		//$M= geDoc::getFolder(array('folId'=>$_J['folId'],'perms'=>'W'));
		//if(_err::$err){ die(_err::$errText); }
		$n=0; $errs=0; $files=0;
		$Ls=array();
		_ADMS::lib('Attach');
		foreach($Lx as $nx => $L){ $n++;
			$ln='Linea '.$n.': ';
			$L['folId']=$_J['folId'];
			$L['tt']=$_J['tt']; $L['tr']=$_J['tr'];
			$L['fileId']=a_sql::qInsert($L,array('tbk'=>'gtd_ffc1','qk'=>'ud'));
			if(a_sql::$err){ $js=_js::e(3,$ln.'Error guardando archivo . '.a_sql::$errText); $errs++; break; }
			else{ $files++;
				$L['versNum']=1;
				$Ls[]=$L;
			}
		}
		if($errs==0){ $js=_js::r('Se guardaron '.$files.' archivos.',$Ls);  }
	}
	echo $js;
}

else if(_0s::$router=='POST ffc/tt/fileUp'){
	$Lx=$___D['L']; unset($___D['L']);
	if($js=_js::ise($___D['tt'],'Se debe definir tt para relacionar archivos.')){}
	else if($js=_js::ise($___D['tr'],'Se debe definir tr para relacionar archivos.')){}
	else if(!is_array($Lx)){ $js=_js::e(3,'No se enviaron archivos a guardar en la carpeta.'); }
	else{
		_ADMS::lib('Attach');
		$n=0; $errs=0; $files=0;
		$Ls=array();
		foreach($Lx as $nx => $L){ $n++;
			$ln='Linea '.$n.': ';
			$L['tt']=$___D['tt']; $L['tr']=$___D['tr'];
			$L['dateC']=date('Y-m-d H:i:s');
			$ins=a_sql::insert($L,array('tbk'=>'gtd_ffc1','qDo'=>'insert','kui'=>'uid'));
			if($ins['err']){ $js=_js::e(3,$ln.'Error guardando archivo . '.$ins['text']); $errs++; break; }
			else{ $files++;
				$L['versNum']=1;
				$L['fileId']=$ins['insertId'];
				$L['url']=Attach::getLink($L);
				$Ls[]=$L;
			}
		}
		if($errs==0){ $js=_js::r('Se subieron '.$files.' archivos.',$Ls); $cmt=true; }
	}
	echo $js;
}

else if(_0s::$router=='GET ffc/fileDelete'){
	$M=a_sql::fetch('SELECT * FROM gtd_ffc1 WHERE fileId=\''.$___D['fileId'].'\' LIMIT 1 ',array(1=>'Error obteniendo información del archivo',2=>'El archivo no existe'));
	if(a_sql::$err){ echo a_sql::$errNoText; }
	else{
	 _ADMS::lib('Attach');
		$M['url']= Attach::getLink($M,'delete');
		print_r(_curl::post($M['url']));
	}
}

else if(_0s::$router=='GET ffc/oTy.open'){
	_ADMS::lib('Attach');
	if($js=_js::ise($___D['folId'],'Se debe definir el ID de la carpeta')){ die($js); }
	$M=array('folId'=>$___D['folId'],'folName'=>'');
	$lW=geDoc::usershare();
	$q=a_sql::query('SELECT B1.* FROM app_fil1 B1
	WHERE B1.tt=\''.$___D['folId'].'\' ',array(1=>'Error obteniendo archivos de carpeta: ',2=>'La carpeta no tiene ningun archivo (oTy/card).'));
	$M['L']=array();
	if(a_sql::$err){$M['L']=json_decode(a_sql::$errNoText,1); }
	else{
		while($L=$q->fetch_assoc()){
			$L['url']=Attach::getLink($L);
			$M['L'][]=$L;
		}
	}
	$js=_js::enc($M,'just'); unset($M);
	echo $js;
}
?>
