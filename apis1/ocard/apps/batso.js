
Api.Batso ={a:'/v/batso/'};
Api.Fqu ={a:'/v/fqu/'};
$Xls_extDef='xlsx';

if(!$V.docSerieType){ $V.docSerieType={}; }
$js.push($V.docSerieType,{ofrc:'Doc. Empresa'});
$js.push($V.docSerieType,{ofem:'Bat. Pers.'});
$Doc.a['ofrc']={a:'batso.open',kl:'odocEntry'};

$Doc.a['ofem']={a:'batsoFem',kl:'femId'};
$V.estres1=[{k:9,v:'1- Siempre'},{k:6,v:'2- Casi Siempre'},{k:3,v:'3- Aveces'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.estres2=[{k:6,v:'1- Siempre'},{k:4,v:'2- Casi Siempre'},{k:2,v:'3- Aveces'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.estres3=[{k:3,v:'1- Siempre'},{k:2,v:'2- Casi Siempre'},{k:1,v:'3- Aveces'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.estresFcv=61.16;
$V.extra1=[{k:0,v:'1- Siempre'},{k:1,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:3,v:'4- Casi Nunca'},{k:4,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.extra2=[{k:4,v:'1- Siempre'},{k:3,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:1,v:'4- Casi Nunca'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];

$V.intraA1=[{k:0,v:'1- Siempre'},{k:1,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:3,v:'4- Casi Nunca'},{k:4,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.intraA2=[{k:4,v:'1- Siempre'},{k:3,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:1,v:'4- Casi Nunca'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.intraB1=[{k:0,v:'1- Siempre'},{k:1,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:3,v:'4- Casi Nunca'},{k:4,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.intraB2=[{k:4,v:'1- Siempre'},{k:3,v:'2- Casi Siempre'},{k:2,v:'3- Algunas Veces'},{k:1,v:'4- Casi Nunca'},{k:0,v:'5- Nunca'},{k:'00',v:'No Aplica'}];
$V.brmRiesgo=['Sin Riesgo','Riesgo Bajo','Riesgo Medio','Riesgo Alto','Riesgo muy Alto'];
$V.brmRiesgoEst=['Muy Bajo','Bajo',' ','Alto','Muy Alto'];

$V.genre={M:'Masculino',F:'Femenino'};
$V.estadoCivil={S:'Soltero (a)',C:'Casado (a)',UL:'Unión Libre',SP:'Separado (a)',Di:'Divorciado (a)',Vi:'Viudo (a)',SM:'Sacerdote / Monta'};
$V.nivelEstudiosBat={N:'Ninguno',PR1:'Primaria Incompleta',PR2:'Primaria Completa',B1:'Bachillerato Incompleto',B2:'Bachillerato Completo',T1:'Técnico Incompleto',T2:'Técnico Completo',P1:'Profesional Incompleto',P2:'Profesional Completo',CM:'Carrera Militar / Policia',POS1:'Pos-grado Incomplet',POS2:'Postgrado Completo'};
$V.estractoVivienda={e1:1,e2:2,e3:3,e4:4,e5:5,e6:6,ef:'Finca',N:'No sé'};
$V.tipoVivienda={P:'Propia',A:'En arriendo',F:'Famiiliar'};
$V.tipoCargoBat={J:'Jefatura',P:'Profesional, analista, técnico',A:'Auxiliar, asistente',O:'Operario, ayudante, servicios generales'};
$V.tipoContratoBat={T0:'Temporal de menos de 1 año',T1:'Temporal >= 1 año',TI:'Termino indefinido',CP:'Cooperativa',PS:'Prestación de Servicios',N:'No sé'};
$V.tipoSalarioRecibido={F:'Fijo (diario, semanal, quincenal o mensual',FV:'Parte fija / Parte variable',V:'Todo variable (destajo, producción)'};

$M.sAdd([
{topFolder:{MLis:['batso']}}
]);
$M.liA['batso']={k:'batso.basic',t:'Baterias Psicosociales'};

$V.BatsoDo={
do1:"Liderazgo y relaciones sociales en el trabajo",
do2:"Control sobre el trabajo",
do3:"Demandas del trabajo",
do4:"Recompensas",
do5:"Factores Extralaborales",
do6:"Evaluación Estres",
do7:"Datos Generales"
};
$V.BatsoDi={
"di1": "Caracteristica del Liderazgo",
"di2": "Relaciones sociales en el trabajo",
"di3": "Retroalimentación del desempeño",
"di4": "Relación con los colaboradores",

"di5": "Claridad del rol",
"di6": "Capacitación",
"di7": "Participación y manejo del cambio",
"di8": "Oportunidades para el uso y desarrollo de habilidades y conocimientos",
"di9": "Control y autonomia sobre el trabajo",

"di10": "Demandas ambientales y de esfuerzo físico",
"di11": "Demandas emocionales",
"di12": "Demandas cuantitativas",
"di13": "Influencia del trabajo sobre el entorno extralaboral",
"di14": "Exigencias de responsabilidad del cargo",
"di15": "Demandas de carga mental",
"di16": "Cosistencia del rol",
"di17": "Demandas de la jornada de trabajo",

"di18": "Recompensas derivadas de la pertenencia a la organización y del trabajo que se realiza",
"di19": "Reconocimiento compensación",

"di20": "Tiempo Fuera del trabajo",
"di21": "Relaciones familiares",
"di22": "Comunicación y relaciones interpersonales",
"di23": "Situación económica del grupo familiar",
"di24": "Característica de la vivienda y de su entorno",
"di25": "Influencia del entorno extralaboral sobre el trabajo",
"di26": "Desplazamiento vivienda - trabajo - vivienda",
"di27": "Estres",
"di28": "Datos Generales"
}
$V.BFname={intraA:'Intralaboral Forma A',intraB:'Intralaboral Forma B',extra:'Extralaboral',extraA:'Extralaboral',extraB:'Extralaboral', estres:'Estres'};

$V.BForm={
intraA:{do1:['di1','di2','di3','di4'],
do2:['di5','di6','di7','di8','di9'],
do3:['di10','di11','di12','di13','di14','di15','di16','di17'],
do4:['di18','di19']
},
intraB:{
do1:['di1','di2','di3'],
do2:['di5','di6','di7','di8','di9'],
do3:['di10','di11','di12','di13','di15','di17'],
do4:['di18','di19']
},
extra:{
do5:['di20','di21','di22','di23','di24','di25','di26'],
},
estres:{do6:['di27']}
};


$M.li['batso']={t:'Aplicación de Baterias a Clientes',kau:'batso.basic',func:function(){
	var btn=$1.T.btnFa({faBtn:'fa_doc',textNode:'Nueva Aplicación Empresa',func:function(){ $M.to('batso.form'); }});
	$M.Ht.ini({btnNew:btn,func_pageAndCont:Batso.get});
}};
$M.li['batso.form']={t:'Formulario Aplicación Baterias',kau:'batso.basic',func:function(){
	$M.Ht.ini({func_cont:Batso.form});
}};
$M.li['batso.open']={t:'Baterias Aplicadas Empresa',kau:'batso.basic',func:function(){
	var Pa=$M.read();
	var btn=$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Registro Empleado',func:function(){ $M.to('batsoFem.form','odocEntry:'+Pa.odocEntry); }});
	$M.Ht.ini({btnNew:btn, func_pageAndCont:Batso.open});
}};
$M.li['batsoFem.form']={noTitle:'Y',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.post}); }};
$M.li['batsoFem.datos']={t:'Formulario Datos Generales',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.datos}); }};
$M.li['batsoFem.intra']={t:'Formulario Intralaboral',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.form}); }};
$M.li['batsoFem.extra']={t:'Formulario Extralaboral',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.form}); }};
$M.li['batsoFem.estres']={t:'Formulario Estres',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.form}); }};
$M.li['batsoFem.view']={noTitle:'Y',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.view}); }};
$M.li['batsoFem.xls']={t:'Reporte de Formularios Empleados',kau:'batso.basic',func:function(){ $M.Ht.ini({func_cont:Batso.Fem.xls}); }};

var Fqu={ /* Forms Querys Obsoleto, revisar el st1/apps/frs */
namer:function(k,ln){
	var name='';
	var ln=(ln)?ln:'L';
	if(ln=='key'){ return k; }
	else{ return ln+'['+k+']'; } 
},
reply_put:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	$ps_DB.get({f:'GET '+Api.Fqu.a+'batso.fre', inputs:'freId='+Pa.freId, loade:cont, func:function(Jr){
			if(Jr.errNo){ $ps_DB.response(cont,Jr); }
			else{
				Fqu.Tpt.base(Jr,cont);
			}
		}
	});
}
}

Fqu.Tpt={
base:function(P,cont,P2){
	var P2=(P2)?P2:{};
	var fromData=(P2.fromData=='Y' && P.data);
	if(!P.tb){ P.tb=['No.','Pregunta','Respuesta']; }
	if(P2.lineNum=='N'){ delete(P.tb[0]); }
	var tb=$1.T.table(P.tb);
	var tBody=$1.t('tbody',0,tb); var jsF='jsFields';
	for(var i in P.L){ var L=P.L[i];
		var tr=$1.t('tr',0,tBody);
		if(!L.formData){ L.formData={}; }
		var tk=(P2.byCode)?L.askCode:L.askId;
		var namev=(P2.nameisCode)?L.askCode:'value';
		var ln='L['+tk+']';
		optk=L.formData.opts;
		if(P2.reset){ L.value=null; }
		if(fromData && P.data[tk]){ L.value=P.data[tk]; }
		var val=(P2.D && P2.D[tk])?P2.D[tk]:L.value;
		//if(P2.lineNum!='N'){ $1.t('td',{textNode:L.lineNum+' ('+val+') - '+optk},tr); }
		if(P2.lineNum!='N'){ $1.t('td',{textNode:L.lineNum},tr); }
		$1.t('td',{textNode:L.lineText},tr);
		var td=$1.t('td',0,tr);
		L.value=L.formData.value=L.formData.selected=val; //reset form
		if(L.disabled){ L.formData.disabled=L.disabled; }
		if(P2.namer){ ln= P2.namer; }
		if(L.value){ L.formData.value=L.value; L.formData.selected=L.value; }
		L.formData.name= Fqu.namer(namev,ln);
		L.formData['class']='jsFields';
		var tag='input';
		var opts='NA';
		switch(L.formType){
			case 'text': L.formData.type='text'; break;
			case 'number': L.formData.type='number'; L.formData.inputmode='numeric'; break;
			case 'date': L.formData.type='date'; break;
			case 'select':{
			tag='select'; L.formData.sel ={name:L.formData.name,'class':L.formData['class'],disabled:L.disabled};
			delete(L.formData.name); delete(L.formData['class']);
			opts=L.formData.opts;
			}break;
			case 'textarea': tag='textarea'; break;
		}
		if((typeof(L.formData.opts)) == 'string'){ L.formData.opts=eval(L.formData.opts);  }
		if(tag=='textarea'){ var tag=$1.t('textarea',L.formData,td); }
		else if(tag=='select'){ var tag=$1.T.sel(L.formData); td.appendChild(tag); }
		else{ var tag=$1.t('input',L.formData,td); }
		if(P2.lineNum){ $1.t('input',{type:'hidden',name:Fqu.namer('lineNum',ln),value:L.lineNum,'class':jsF},td); }
		if(P2.group1){ $1.t('input',{type:'hidden',name:Fqu.namer('group1',ln),value:L.group1,'class':jsF},td); }
		if(P2.group2){ $1.t('input',{type:'hidden',name:Fqu.namer('group2',ln),value:L.group2,'class':jsF},td); }
		tag.title = opts;
		//td.innerHTML+= JSON.stringify(L.formData.opts);
	}
	if(cont){ cont.appendChild(tb); }
	return tb;
}
}

var Batso={
opts:function(P,e){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	L.docEntry=L.femId;
	Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.cancel({serieType:'batFem', reqMemo:'N',docEntry:T.P.docEntry,api:Api.Batso.a+'fem.statusCancel',text:'Se va anular el documento, no se puede reversar está acción.'}); } }; n++;
	return Li={Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Batso.a,inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','ID','Cliente','Año','Estado','Detalles']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td','',tr);
				var td2=$1.t('td',0,tr);
				$Doc.href('ofrc',L,td2);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docYear},tr);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFields';
	$Api.get({f:Api.Batso.a+'a.one', inputs:'odocEntry='+Pa.odocEntry, loadVerif:!Pa.odocEntry, loade:cont, func:function(Jr){
		var Fs=[
		/* {fType:'crd',wxn:'wrapx3',L:'Cliente',req:'Y',cardId:Jr.cardId,cardName:Jr.cardName}, */
		{wxn:'wrapx3',req:'Y',L:'Nombre Cliente',I:{tag:'input',type:'text',name:'cardName','class':jsF,value:Jr.cardName}},
		{wxn:'wrapx8',L:'Año',req:'Y',I:{tag:'input',type:'number',inputmode:'numeric',min:2008,name:'docYear','class':jsF,value:Jr.docYear}},
		{wxn:'wrapx8',L:'Cant. Empleados',req:'Y',I:{tag:'input',type:'number',inputmode:'numeric',min:1,name:'empQty','class':jsF,value:Jr.empQty}},
		{divLine:1,wxn:'wrapx8',fType:'date',name:'docDate',value:Jr.docDate,req:'Y'},
		{wxn:'wrapx8',fType:'date',textNode:'Finaliza',name:'dueDate',value:Jr.dueDate,req:'Y'},
		{wxn:'wrapx4',req:'Y',L:'Aplicado por',I:{tag:'input',type:'text',name:'makeBy','class':jsF,value:Jr.makeBy}},
		{divLine:1,wxn:'wrapx2',L:'Detalles',I:{tag:'textarea',name:'lineMemo',textNode:Jr.lineMemo,'class':jsF}}
		];
		$Doc.formSerie({cont:cont, serieType:'ofrc',docEntry:Pa.odocEntry,jsF:jsF,Jr:{}, POST:Api.Batso.a, func:function(Jr2){
			$M.to('batso.open','odocEntry:'+Jr2.odocEntry);
		},Li:Fs});
	}});
	
},
open:function(){/* empleados */
	var cont=$M.Ht.cont; Pa=$M.read();
	$Api.get({f:Api.Batso.a+'a.open',inputs:'odocEntry='+Pa.odocEntry, loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else if(Jr.L && Jr.L.errNo){ $ps_DB.response(cont,Jr.L); }
		else{
			$M.Ht.title.innerHTML = Jr.cardName+', Año: '+Jr.docYear;
			var divT=$1.t('div',{style:'margin:0.5rem 0;'},cont);
			$1.T.btnFa({fa:'iBg iBg_icoxls',textNode:'Reporte Consolidado',func:function(T){ $M.to('batsoFem.xls','odocEntry:'+Pa.odocEntry); }},divT);
			var tb=$1.T.table(['','ID','ID Empleado','Estado','Nombre','Tipo','Formularios','Reporte','Creado']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
					var menu=$1.Menu.winLiRel(Batso.opts({L:L})); td.appendChild(menu);
				var td2=$1.t('td',{textNode:L.femId},tr);
				$1.t('td',{textNode:L.empId},tr);
				$1.t('td',{textNode:$V.dStatus[L.docStatus]},tr);
				$1.t('td',{textNode:L.fullName},tr);
				$1.t('td',{textNode:L.tipoEmp},tr);
				var td=$1.t('td','',tr);
				$1.T.btnFa({fa:'fa_doc',textNode:'General',P:L,func:function(T){
					$M.to('batsoFem.datos','femId:'+T.P.femId);
				}},td);
				$1.T.btnFa({fa:'fa_doc',textNode:'Intra',P:L,func:function(T){
					$M.to('batsoFem.intra','femId:'+T.P.femId);
				}},td);
				$1.T.btnFa({fa:'fa_doc',textNode:'Extra', P:L,func:function(T){
					$M.to('batsoFem.extra','femId:'+T.P.femId);
				}},td);
				$1.T.btnFa({fa:'fa_doc',textNode:'Estres',P:L,func:function(T){
					$M.to('batsoFem.estres','femId:'+T.P.femId);
				}},td);
				var td=$1.t('td','',tr);
				$1.T.btnFa({fa:'fa fa_eye',textNode:'Resumen',P:L,func:function(T){ $M.to('batsoFem.view','femId:'+T.P.femId); }},td);
				$1.t('td',{textNode:L.dateC},tr);
			}
			cont.appendChild(tb);
		}
	}});
},

}

Batso.Frc={
opts:function(P){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	Li[n]={ico:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('batsoFem.form','femId:'+T.P.femId); } }; n++;
	Li[n]={ico:'fa fa_eye',textNode:' Visualizar', P:L, func:function(T){ $M.to('batsoFem.view','femId:'+T.P.femId); } }; n++;
	//Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.cancel({serieType:'ocvt',docEntry:T.P.docEntry,api:Api.Gvt.cvt+'.status.cancel',text:'Se va anular el documento, no se puede reversar está acción.'}); } }; n++;
	//Li[n]={ico:'iBg iBg_candado',textNode:'Cerrar Documento', P:L, func:function(T){ $Doc.close({docEntry:T.P.docEntry,api:Api.Gvt.cvt+'.status.handClose',text:'Se va a cerrar el Documento, no se puede reversar está acción.'}); } }; n++; 
	return Li={Li:Li,textNode:P.textNode};
},
}

Batso.Fem={
post:function(){
	var cont=$M.Ht.cont; Pa=$M.read();
	var resp2=$1.t('div',0,cont);
	$1.t('h3',{textNode:'Generar Registro de Empleado'},cont);
	var formBa=[
	{askCode:'empId',lineText:'ID de empleado'},
	{askCode:'fullName',lineText:'Nombre de Empleado'},
	{askCode:'tipoCargo',formType: "select",lineText:'Tipo de Cargo',formData:{opts:$V.tipoCargoBat}}
	];
	var fie=$1.T.fieldset(Fqu.Tpt.base({tb:['Campo','Valor'],L:formBa},null,{lineNum:'N',nameisCode:'Y',namer:'key'}));
	cont.appendChild(fie);
	var resp=$1.t('div',0,cont);
	var btnSend=$Api.btnSend({textNode:'Guardar Datos'},{POST:Api.Batso.a+'femDatos', getInputs:function(){  return 'odocEntry='+Pa.odocEntry+'&'+$1.G.inputs(cont); }, loade:resp, func:function(Jr2){
		if(Jr2.femId){ $M.to('batsoFem.datos','femId:'+Jr2.femId);
			$1.delet(btnSend);
		}
		$ps_DB.response(resp,Jr2);
	} });
	cont.appendChild(btnSend);
},
datos:function(){
	var cont=$M.Ht.cont; Pa=$M.read();
	$Api.get({f:Api.Batso.a+'femDatos',inputs:'femId='+Pa.femId+'&'+$1.G.filter(), loadeFull:1, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var resp2=$1.t('div',0,cont);
			var ide=$1.t('input',{type:'hidden',name:'femId',value:Pa.femId},cont);
			var D2={lineNum:'N',byCode:'Y',reset:'Y'};
			if(!Jr.errNo){ D2.D=Jr; }
			var fie=$1.T.fieldset(Fqu.Tpt.base({L:Batso.Fdata},null,D2));
			cont.appendChild(fie);
			var resp=$1.t('div',0,cont);
			var btnSend=$Api.btnSend({textNode:'Guardar Datos'},{PUT:Api.Batso.a+'femDatos', getInputs:function(){  return 'femId='+Pa.femId+'&'+$1.G.inputs(cont); }, loade:resp, func:function(Jr2){
				if(Jr2.femId){ $M.to('batsoFem.datos','femId:'+Jr2.femId); }
				$ps_DB.response(resp,Jr2);
			} });
			cont.appendChild(btnSend);
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont;
	Pa=$M.read();
	var formCode=$M.read('!');
	switch(formCode){
		case 'batsoFem.intra': formCode='intra'; break;
		case 'batsoFem.extra': formCode='extra'; break;
		case 'batsoFem.estres': formCode='estres'; break;
	}
	var vPost='femId='+Pa.femId+'&formCode='+formCode;
	$Api.get({f:Api.Batso.a+'fem',inputs:vPost, loadeFull:1, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var resp2=$1.t('div',0,cont);
			var fie=$1.T.fieldset(Fqu.Tpt.base(Jr,null,{fromData:'Y',lineNum:'Y',group1:'Y',group2:'Y'}));
			cont.appendChild(fie);
			var resp=$1.t('div',0,cont);
			var btnSend=$Api.btnSend({textNode:'Guardar Datos'},{PUT:Api.Batso.a+'fem', getInputs:function(){  return vPost+'&'+$1.G.inputs(cont); }, loade:resp, func:function(Jr2){
				$ps_DB.response(resp,Jr2);
			} });
			cont.appendChild(btnSend);
		}
	}});
},
view:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	var formCode=$M.read('!');
	switch(formCode){
		case 'batsoFem.intra': formCode='intra'; break;
		case 'batsoFem.extra': formCode='extra'; break;
		case 'batsoFem.estres': formCode='estres'; break;
	}
	var vPost='femId='+Pa.femId+'&formCode='+formCode;
	$Api.get({f:Api.Batso.a+'fem.view', inputs:vPost, loadeFull:1, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		Batso.F.intra(cont,Jr);
	}});
},
xls:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	var vPost='odocEntry='+Pa.odocEntry;
	$Api.get({f:Api.Batso.a+'fem.xls', inputs:vPost, loadeFull:1, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
		var winM=$1.Menu.inLine([
		{textNode:'Reporte',winClass:'_reporte','class':'active'},
		{textNode:'Gráficos',winClass:'_graficos'},
		{textNode:'Tablas',winClass:'_tablas'}
		],{winCont:1}); cont.appendChild(winM);
		var rpt=$1.t('div',{'class':'winMenuInLine _reporte'},winM);
		var tble=$1.t('div',{'class':'winMenuInLine _tablas',style:'display:none;'},winM);
		var gra=$1.t('div',{'class':'winMenuInLine _graficos',style:'display:none;'},winM);
		Batso.Fem.xlsTb(Jr.Gra,tble);
		Batso.Fem.xlsRpt(Jr,rpt);
		Batso.Fem.xlsGra(Jr.Gra,gra);
		}
	}});
},
xlsRpt:function(Jr,cont){
		var tb=$1.T.table();
		var trh=$1.q('tr',tb);
		$1.delet($1.q('td',trh));
		var toGra=/^(genre|edad|estadoCivil|nivelEstudios|estracto|tipoViv)$/;
		for(var k in $V.batSoXls){ var tK=$V.batSoXls[k];
			var te=tK.t; var tit='';
			var teIni=(tK.dim)?'Dimensión: ':((tK.dom)?'Dominio: ':((tK.t)?tK.t:''));
			if(tK.dom && tK.pt){ te= teIni+$V.BatsoDo[tK.dom]+ '\n(Punt. Transf.)'; tit=tK.dom; }
			else if(tK.dim && tK.pt){ te= teIni+$V.BatsoDi[tK.dim]+ '\n(Punt. Transf.)'; tit=tK.dim; }
			else if(tK.pt){ te= teIni+ '\n(Punt. Transf.)'; }
			if(tK.dom && tK.rie){ te= teIni+$V.BatsoDo[tK.dom]+ '\n(Nivel Riesgo)'; tit=tK.dom; }
			else if(tK.dim && tK.rie){ te= teIni+$V.BatsoDi[tK.dim]+ '\n(Nivel Riesgo)'; tit=tK.dim; }
			else if(tK.rie){ te= teIni+ '\n(Nivel Riesgo)'; }
			var style=(tK.style)?tK.style:styleo;
			var xls=(tK.xls)?tK.xls:xlso;
			var td=$1.t('td',{textNode:te,style:style,'class':'pre',title:tit,xls:xls},trh);
			if(tK.graph=='Y'){
				td.k=k;
				td.onclick=function(){ $myChart.fromTb({title:this.innerText,k:this.k},tb); }
				td.innerHTML += ' +'
			}
			var styleo=style;
			var xlso=xls;
		}
		/* datos */
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			for(var k in $V.batSoXls){
				var valReal=val=L[k];
				var tK=$V.batSoXls[k];
				if(tK.rie){ val=$V.brmRiesgo[val]; };
				if(val==undefined && valReal!=''){ val=valReal; };
				if(tK.dim){
					if(L.tipoEmp=='B' && tK.forma=='A'){ val='N/A'; }
				}
				else if(tK.opts){
					var vk=eval(tK.opts); val=vk[val]; 
				}
				val=(val==undefined)?'SIN RESPUESTA':val;
				var td=$1.t('td',{textNode:val,title:L[k]},tr);
				if(tK.graph=='Y'){
					td.classList.add($myChart.cls+k);
					td.myChart={v:val,l:val};
				}
			}
		}
		tb =$1.T.tbExport(tb,{ext:'xlsx',fileName:'Reporte Baterias'});
		cont.appendChild(tb);
		
},
xlsTb:function(Gra,cont){
	var Intra={
	do1:{rs:4,
		di1:{},di2:{},di3:{},di4:{}
	},
	do2:{rs:5,
		di5:{},di6:{},di7:{},di8:{},di9:{},
	},
	do3:{rs:8,
		di10:{},di11:{},di12:{},di13:{},di14:{},di15:{},di16:{},di17:{}
	},
	do4:{rs:2,
		di18:{},di19:{}
	}
	};
	var Extra={
		di20:{},di21:{},di22:{},di23:{},di24:{},di25:{},di26:{},
	};
	var Estres={
		di27:{},
	};
	var bT=['Dominio','Dimensión'];
	for(var r in $V.brmRiesgo){ bT.push($V.brmRiesgo[r]); }
	bT.push(''); bT.push('Dimensión');
	for(var r in $V.brmRiesgo){ bT.push($V.brmRiesgo[r]); }
	var tb=$1.T.table(bT); 
	var trH=$1.q('thead tr',tb);
	var tBody=$1.t('tbody',0,tb);
	for(var dom in Intra){ var Do=Intra[dom];
		var tr=$1.t('tr',0,tBody);
		//$1.t('td',{textNode:$V.BatsoDo[dom],rowspan:Do.rs+2},tr);
		var rs=Do.rs; var nr=1;
		delete(Do.rs);
		for(var d in Do){
			$1.t('td',{textNode:$V.BatsoDo[dom]},tr);
			$1.t('td',{textNode:$V.BatsoDi[d]},tr);
			for(var r in $V.brmRiesgo){
				var val=(Gra.intraA[d] && Gra.intraA[d][r])?Gra.intraA[d][r]:0;
				var perc=val/Gra.intraA.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
			//$1.t('td','',tr);
			$1.t('td',{textNode:$V.BatsoDo[dom],style:'backgroundColor:#DDD;'},tr);
			if(d.match(/^(di4|di14|di16)$/)){ $1.t('td',{textNode:'No Aplica'},tr); }
			else{ $1.t('td',{textNode:$V.BatsoDi[d]},tr); }
			for(var r in $V.brmRiesgo){
				var val=(Gra.intraB[d] && Gra.intraB[d][r])?Gra.intraB[d][r]:0;
				var perc=val/Gra.intraB.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
		if(rs>nr){ var tr=$1.t('tr',0,tBody); } nr++;
		}
		var tr=$1.t('tr',0,tBody); /*blank */
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:''},tr);
		$1.t('td',{textNode:'Resultado Dominio'},tr);
		for(var r in $V.brmRiesgo){
			var val=(Gra.intraA[dom] && Gra.intraA[dom][r])?Gra.intraA[dom][r]:0;
			var perc=val/Gra.intraA.total*100;
			te=(val==0)?' ':$js.toFixed(perc)+'%';
			$1.t('td',{textNode:te},tr);
		}
		$1.t('td',' ',tr);
		$1.t('td',{textNode:''},tr);
		$1.t('td',{textNode:'Resultado Dominio'},tr);
		for(var r in $V.brmRiesgo){
			var val=(Gra.intraB[dom] && Gra.intraB[dom][r])?Gra.intraB[dom][r]:0;
			var perc=val/Gra.intraB.total*100;
			te=(val==0)?'':$js.toFixed(perc)+'%';
			$1.t('td',{textNode:te},tr);
		}
	}
	var tb2=tb.cloneNode();
	cont.appendChild($1.T.tbExport(tb,{t:'Formulario Intralaboral',fileName:'Formulario Intralaboral'}));
	/* extra */
	tb=tb2;
	cont.appendChild($1.T.fieldset(tb,{L:'Formulario Extralaboral'}));
	var trH2=trH.cloneNode(1);
	tb.appendChild(trH2);
	trH2.removeChild(trH2.childNodes[0]);
	var tBody=$1.t('tbody',0,tb);
	var tr=$1.t('tr',0,tBody);
	{var dom='do5';
		for(var d in Extra){
			$1.t('td',{textNode:$V.BatsoDi[d]},tr);
			for(var r in $V.brmRiesgo){
				var val=(Gra.extraA[d] && Gra.extraA[d][r])?Gra.extraA[d][r]:0;
				var perc=val/Gra.extraA.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
			$1.t('td',' ',tr);
			$1.t('td',{textNode:$V.BatsoDi[d]},tr);
			for(var r in $V.brmRiesgo){
				var val=(Gra.extraB[d] && Gra.extraB[d][r])?Gra.extraB[d][r]:0;
				var perc=val/Gra.extraB.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
			var tr=$1.t('tr',0,tBody);
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Resultado Total'},tr);
		for(var r in $V.brmRiesgo){
			var val=(Gra.extraA[dom] && Gra.extraA[dom][r])?Gra.extraA[dom][r]:0;
			var perc=val/Gra.extraA.total*100;
			te=(val==0)?' ':$js.toFixed(perc)+'%';
			$1.t('td',{textNode:te},tr);
		}
		$1.t('td',' ',tr);
		$1.t('td',{textNode:'Resultado Total'},tr);
		for(var r in $V.brmRiesgo){
			var val=(Gra.extraB[dom] && Gra.extraB[dom][r])?Gra.extraB[dom][r]:0;
			var perc=val/Gra.intraB.total*100;
			te=(val==0)?' ':$js.toFixed(perc)+'%';
			$1.t('td',{textNode:te},tr);
		}
	}
	cont.appendChild($1.T.tbExport(tb,{t:'Formulario Extralaboral',fileName:'Formulario Extralaboral'}));
	/* estres */
	var bT=[];
	for(var r in $V.brmRiesgoEst){ bT.push($V.brmRiesgoEst[r]); }
	bT.push('');
	for(var r in $V.brmRiesgoEst){ bT.push($V.brmRiesgoEst[r]); }
	var tb=$1.T.table(bT); 
	cont.appendChild($1.T.fieldset(tb,{L:'Formulario Estres'}));
	var tBody=$1.t('tbody',0,tb);
	var tr=$1.t('tr',0,tBody);
	{var dom='do6';
		for(var d in Estres){
			for(var r in $V.brmRiesgoEst){
				var val=(Gra.estresA[d] && Gra.estresA[d][r])?Gra.estresA[d][r]:0;
				var perc=val/Gra.estresA.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
			$1.t('td',{textNode:''},tr);
			for(var r in $V.brmRiesgoEst){
				var val=(Gra.estresB[d] && Gra.estresB[d][r])?Gra.estresB[d][r]:0;
				var perc=val/Gra.estresB.total*100;
				te=(val==0)?' ':$js.toFixed(perc)+'%';
				$1.t('td',{textNode:te},tr);
			}
			var tr=$1.t('tr',0,tBody);
		}
	}
	cont.appendChild($1.T.tbExport(tb,{t:'Formulario Estres',fileName:'Formulario Estres'}));
},
xlsGra:function(Gra,cont){
	//var Forms=['intraA','intraB','extraA','extraB','estresA','estresB'];
	var Forms=['intraA','intraB'];
	var FormsN=['Intralaboral A','Intralaboral B','Extralaboral A','Extralaboral B','Estres A','Estres B'];
	var Intra=['do1','do2','do3','do4'];
	var IntraDi=['di1','di2','di3','di4','di5','di6','di7','di8','di9','di10','di11','di12','di13','di14','di15','di16','di17','di18','di19'];
	var Re=$V.brmRiesgo;
	var rieBg=['#059BFF','#22CECE','#FFCD56','#FF9124','#FF3D67'];
	$1.t('h4',{'class':'clear',textNode:'Formularios'},cont);
	for(var i in Forms){ var dom=Forms[i];
		var canvw=$1.t('div',{style:'width:32.5rem; height:300px; float:left; border:0.0625rem solid #000; borderRadius:0.25rem; backgroundColor:#FFF; margin:0 0.25rem 0.25rem 0'},cont);
		var canv=$1.t('canvas',0,canvw);
		var Dat=[];
		var totalEmp=0;
		for(var r in Re){
			var val=(Gra[dom] && Gra[dom].form && Gra[dom].form[r])?Gra[dom].form[r]:0;
			Dat.push(val);
			totalEmp +=val;
		}
		$myChart.get(canv,{type:'doughnut',forma:'semiCircle',
		title:'Formulario '+FormsN[i],
		data:Dat,legend:false,labels:Re,
		bgColor:rieBg,
		},{'labels':'float%'});
		
	}
	
	$1.t('h4',{'class':'clear',textNode:'Dominios Forma A'},cont);
	for(var i in Intra){ var dom=Intra[i];
		var canvw=$1.t('div',{style:'width:32.5rem;  height:300px; float:left; border:0.0625rem solid #000; borderRadius:0.25rem; backgroundColor:#FFF; margin:0 0.25rem 0.25rem 0'},cont);
		var canv=$1.t('canvas',0,canvw);
		var Dat=[];
		for(var r in Re){
			var val=(Gra.intraA[dom] && Gra.intraA[dom][r])?Gra.intraA[dom][r]:0;
			Dat.push(val);
		}
		$myChart.get(canv,{type:'doughnut',forma:'semiCircle',
		title:'Dominio: '+$V.BatsoDo[dom]+' - Forma A',
		data:Dat,legend:false,labels:Re,
		bgColor:rieBg
		});
	}
	
	$1.t('h4',{'class':'clear',textNode:'Dominios Forma B'},cont);
	for(var i in Intra){ var dom=Intra[i];
		var canvw=$1.t('div',{style:'width:32.5rem;  height:300px; float:left; border:0.0625rem solid #000; borderRadius:0.25rem; backgroundColor:#FFF; margin:0 0.25rem 0.25rem 0'},cont);
		var canv=$1.t('canvas',0,canvw);
		var Dat=[];
		for(var r in Re){
			var val=(Gra.intraB[dom] && Gra.intraB[dom][r])?Gra.intraB[dom][r]:0;
			Dat.push(val);
		}
		$myChart.get(canv,{type:'doughnut',forma:'semiCircle',
		title:'Dominio: '+$V.BatsoDo[dom]+' - Forma B',
		data:Dat,legend:false,labels:Re,
		bgColor:rieBg
		});
	}
	
	$1.t('h4',{'class':'clear',textNode:'Dimensiones Forma A'},cont);
	for(var i in IntraDi){ var d=IntraDi[i];
		var canvw=$1.t('div',{style:'width:32.5rem; float:left; border:0.0625rem solid #000; borderRadius:0.25rem; backgroundColor:#FFF; margin:0 0.25rem 0.25rem 0'},cont);
		var canv=$1.t('canvas',0,canvw);
		var Dat=[];
		for(var r in Re){
			var val=(Gra.intraA[d] && Gra.intraA[d][r])?Gra.intraA[d][r]:0;
			Dat.push(val);
		}
		$myChart.get(canv,{type:'doughnut',forma:'semiCircle',
		title:'Dimensión: '+$V.BatsoDi[d]+' - Forma A',
		data:Dat,legend:false,labels:Re,
		bgColor:rieBg
		});
	}
	
	$1.t('h4',{'class':'clear',textNode:'Dimensiones Forma B'},cont);
	for(var i in IntraDi){ var d=IntraDi[i];
		if(d.match(/^(di4|di14|di16)$/)){ continue; }
		var canvw=$1.t('div',{style:'width:32.5rem; float:left; border:0.0625rem solid #000; borderRadius:0.25rem; backgroundColor:#FFF; margin:0 0.25rem 0.25rem 0'},cont);
		var canv=$1.t('canvas',0,canvw);
		var Dat=[];
		for(var r in Re){
			var val=(Gra.intraA[d] && Gra.intraA[d][r])?Gra.intraA[d][r]:0;
			Dat.push(val);
		}
		$myChart.get(canv,{type:'doughnut',forma:'semiCircle',
		title:'Dimensión: '+$V.BatsoDi[d]+' - Forma B',
		data:Dat,legend:false,labels:Re,
		bgColor:rieBg
		});
	}
	
	$1.t('div',{'class':'clear'},cont);
}
}

Batso.F={
intra:function(cont,Jr){
	var brmCol=['','backgroundColor:#0F0','backgroundColor:#FF0','backgroundColor:orange','backgroundColor:#F00'];
	$1.t('h4',{textNode:'Resumen de Baterias Aplicadas'},cont);
	var pre=$1.t('p',{textNode:'ID Empleado: '+Jr.empId,'class':'pre'},cont);
	pre.appendChild($1.t('textNode','\nNombre: '+Jr.fullName));
	var tbf=['Concepto','Puntaje','%','Riesgo'];
	for(var fk in Jr.L){ var L=Jr.L[fk];
		var tb=$1.T.table(tbf);
		var tBody=$1.t('tbody',0,tb);
		if(fk!='estres'){
		for(var dom in $V.BForm[fk]){
			for(var nk in $V.BForm[fk][dom]){
				var dim=$V.BForm[fk][dom][nk];
				var Ld=(L[dim])?L[dim]:{};
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:$V.BatsoDi[dim]},tr);
				$1.t('td',{textNode:Ld.suma},tr);
				$1.t('td',{textNode:Ld.pt},tr);
				$1.t('td',{textNode:$V.brmRiesgo[Ld.brm]},tr);
			}
			var Ld=(L[dom])?L[dom]:{};
			var css='backgroundColor:#EEE; font-weight:bold;';
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:$V.BatsoDo[dom],style:css},tr);
			$1.t('td',{textNode:Ld.suma,style:css},tr);
			$1.t('td',{textNode:Ld.pt,style:css},tr);
			$1.t('td',{textNode:$V.brmRiesgo[Ld.brm],style:css},tr);
		}
		}
		var Ld=(L.form)?L.form:{};
		var css='backgroundColor:#DDD; font-weight:bold;';
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'Total Formulario',style:css},tr);
		$1.t('td',{textNode:Ld.suma,style:css},tr);
		$1.t('td',{textNode:Ld.pt,style:css},tr);
		$1.t('td',{textNode:$V.brmRiesgo[Ld.brm],style:css},tr);
		tbr=$1.T.tbExport(tb,{ext:'xlsx',print:'Y'});
		$1.I.before($1.t('h4',{textNode:'Formulario '+$V.BFname[fk]}),tb);
		cont.appendChild(tbr);
	}
	
}
}

//(genre|edad|estadoCivil|nivelEstudios|estracto|tipoViv)
$V.batSoXls={
tipoEmp:{t:'Tipo Empleado',graph:'Y'},
fullName:{t:'Nombre de empleado',style:'backgroundColor:#23d5db; color:#000;',xls:{style:{fill:'#23d5db', font:'#000'}}},
empId:{t:'ID de empleado'},
genre:{t:'Genero',opts:"$V.genre",graph:'Y'},
birYear:{t:'Año de Nacimiento'},
edad:{t:'Edad',graph:'Y'},
estadoCivil:{t:'Estado Civil',opts:"$V.estadoCivil",graph:'Y'},
nivelEstudios:{t:'Ult. nivel de estudios',opts:$V.nivelEstudiosBat,graph:'Y'},
ocupacion:{t:'Ocupación o Profesión'},
city:{t:'Ciudad Residencia'},
county:{t:'Depart. Residencia'},
estracto:{t:'Estracto Servicios',opts:$V.estractoVivienda,graph:'Y'},
tipoViv:{t:'Tipo de Vivienda',opts:$V.tipoVivienda,graph:'Y'},
persCargo:{t:'Persona a Cargo',graph:'Y'},
cityWork:{t:'Ciudad Trabajo',graph:'Y'},
countyWork:{t:'Depart. pais de Trabajo'},
cargo:{t:'Cargo'},
tipoCargo:{t:'Tipo de Cargo',opts:$V.tipoCargoBat},
workYears:{t:'Años en el cargo',graph:'Y'},
empDpto:{t:'Area de Trabajo',graph:'Y'},
hourWork:{t:'Horas Trabajo Cargo',graph:'Y'},
tipoCont:{t:'Tipo Contrato',opts:$V.tipoContratoBat,graph:'Y'},
tipoSalario:{t:'Tipo de Salario',opts:$V.tipoSalarioRecibido},
/*intra*/
intra_di1_pt:{dim:'di1',pt:1,forma:'AB',style:'backgroundColor:#00F; color:#FFF;',xls:{
style:{fill:'#00F', font:'#FFF'}}},
intra_di1_rie:{dim:'di1',rie:1,forma:'AB'},
intra_di2_pt:{dim:'di2',pt:1,forma:'AB'},
intra_di2_rie:{dim:'di2',rie:1,forma:'AB'},
intra_di3_pt:{dim:'di3',pt:1,forma:'AB'},
intra_di3_rie:{dim:'di3',rie:1,forma:'AB'},
intra_di4_pt:{dim:'di4',pt:1,forma:'A'},
intra_di4_rie:{dim:'di4',rie:1,forma:'A'},
intra_do1_pt:{dom:'do1',pt:1},
intra_do1_rie:{dom:'do1',rie:1},

intra_di5_pt:{dim:'di5',pt:1,forma:'AB'},
intra_di5_rie:{dim:'di5',rie:1,forma:'AB'},
intra_di6_pt:{dim:'di6',pt:1,forma:'AB'},
intra_di6_rie:{dim:'di6',rie:1,forma:'AB'},
intra_di7_pt:{dim:'di7',pt:1,forma:'AB'},
intra_di7_rie:{dim:'di7',rie:1,forma:'AB'},
intra_di8_pt:{dim:'di8',pt:1,forma:'AB'},
intra_di8_rie:{dim:'di8',rie:1,forma:'AB'},
intra_di9_pt:{dim:'di9',pt:1,forma:'AB'},
intra_di9_rie:{dim:'di9',rie:1,forma:'AB'},
intra_do2_pt:{dom:'do2',pt:1},
intra_do2_rie:{dom:'do2',rie:1},

intra_di10_pt:{dim:'di10',pt:1,forma:'AB'},
intra_di10_rie:{dim:'di10',rie:1,forma:'AB'},
intra_di11_pt:{dim:'di11',pt:1,forma:'AB'},
intra_di11_rie:{dim:'di11',rie:1,forma:'AB'},
intra_di12_pt:{dim:'di12',pt:1,forma:'AB'},
intra_di12_rie:{dim:'di12',rie:1,forma:'AB'},
intra_di13_pt:{dim:'di13',pt:1,forma:'AB'},
intra_di13_rie:{dim:'di13',rie:1,forma:'AB'},
intra_di14_pt:{dim:'di14',pt:1,forma:'A'},
intra_di14_rie:{dim:'di14',rie:1,forma:'A'},
intra_di15_pt:{dim:'di15',pt:1,forma:'AB'},
intra_di15_rie:{dim:'di15',rie:1,forma:'AB'},
intra_di16_pt:{dim:'di16',pt:1,forma:'A'},
intra_di16_rie:{dim:'di16',rie:1,forma:'A'},
intra_di17_pt:{dim:'di17',pt:1,forma:'AB'},
intra_di17_rie:{dim:'di17',rie:1,forma:'AB'},
intra_do3_pt:{dom:'do3',pt:1},
intra_do3_rie:{dom:'do3',rie:1},

intra_di18_pt:{dim:'di18',pt:1,forma:'AB'},
intra_di18_rie:{dim:'di18',rie:1,forma:'AB'},
intra_di19_pt:{dim:'di19',pt:1,forma:'AB'},
intra_di19_rie:{dim:'di19',rie:1,forma:'AB'},
intra_do4_pt:{dom:'do4',pt:1},
intra_do4_rie:{dom:'do4',rie:1},

intra_pt:{t:'Formulario: Intralaboral', pt:1},
intra_rie:{t:'Formulario: Intralaboral', rie:1},
/* extra */
extra_di20_pt:{dim:'di20',pt:1,style:'backgroundColor:#ef1a76; color:#FFF;',xls:{style:{fill:'#ef1a76', font:'#FFF'}}},
extra_di20_rie:{dim:'di20',rie:1},
extra_di21_pt:{dim:'di21',pt:1},
extra_di21_rie:{dim:'di21',rie:1},
extra_di22_pt:{dim:'di22',pt:1},
extra_di22_rie:{dim:'di22',rie:1},
extra_di23_pt:{dim:'di23',pt:1},
extra_di23_rie:{dim:'di23',rie:1},
extra_di24_pt:{dim:'di24',pt:1},
extra_di24_rie:{dim:'di24',rie:1},
extra_di25_pt:{dim:'di25',pt:1},
extra_di25_rie:{dim:'di25',rie:1},
extra_di26_pt:{dim:'di26',pt:1},
extra_di26_rie:{dim:'di26',rie:1},
extra_do5_pt:{dom:'do5',pt:1},
extra_do5_rie:{dom:'do5',rie:1},
extra_pt:{t:'Formulario: Extralaboral', pt:1},
extra_rie:{t:'Formulario: Extralaboral', rie:1},
/* intrayExtra */
intraExtra_pt:{t:'Formulario: Intra y Extralaboral',pt:1,style:'backgroundColor:#EBDB1F;',xls:{style:{fill:'#EBDB1F', font:'#000'}}},
intraExtra_rie:{t:'Formulario: Intra y Extralaboral',rie:1},
/* estres */
estres_pt:{t:'Formulario: Estres',pt:1,style:'backgroundColor:#4bed1e;',xls:{style:{fill:'#4bed1e', font:'#000'}}},
estres_rie:{t:'Formulario: Estres',rie:1},
};

Batso.Fdata=[
{
	askCode: "tipoCargo",
	disabled:'disabled',
	"lineNum": "14",
	"formType": "select",
	"lineText": "Seleccione el tipo de cargo que más se parece al que usted desempeña",
	"formData": {"opts": "$V.tipoCargoBat"},
	"group1": "do7",
	"group2": "di28"
},
{askCode: "empId",
	"req": "Y",
	"lineNum": "0",
	"formType": "text",
	"lineText": "ID Empleado",
},
{askCode: "fullName",
	"req": "Y",
	"lineNum": "1",
	"formType": "text",
	"lineText": "Nombre Completo",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "genre",
	"req": "Y",
	"lineNum": "2",
	"formType": "select",
	"lineText": "Genero",
	formData: {opts: "$V.genre"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "birYear",
	"req": "Y",
	"lineNum": "3",
	"formType": "text",
	"lineText": "Año de nacimiento",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "estadoCivil",
	"req": "Y",
	"lineNum": "4",
	"formType": "select",
	"lineText": "Estado Civil",
	"formData": {"opts": "$V.estadoCivil"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "nivelEstudios",
	"req": "Y",
	"lineNum": "5",
	"formType": "select",
	"lineText": "Ultimo nivel de estudios",
	"formData": {"opts": "$V.nivelEstudiosBat"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "ocupacion",
	"req": "Y",
	"lineNum": "6",
	"formType": "text",
	"lineText": "Ocupación o Profesión",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "city",
	"req": "Y",
	"lineNum": "7",
	"formType": "text",
	"lineText": "Ciudad \/ Municipio",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "county",
	"req": "Y",
	"lineNum": "7",
	"formType": "text",
	"lineText": "Departamento",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "estracto",
	"req": "Y",
	"lineNum": "8",
	"formType": "select",
	"lineText": "Estracto de los servicios públicos",
	"formData": {"opts": "$V.estractoVivienda"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "tipoViv",
	"req": "Y",
	"lineNum": "9",
	"formType": "select",
	"lineText": "Tipo de Vivienda",
	"formData": {"opts": "$V.tipoVivienda"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "persCargo",
	"req": "Y",
	"lineNum": "10",
	"formType": "number",
	"lineText": "Número de personas que dependen económicante de usted",
	"formData": {"min": 0},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "cityWork",
	"req": "Y",
	"lineNum": "11",
	"formType": "text",
	"lineText": "Ciudad donde trabaja actualmente",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "countyWork",
	"req": "Y",
	"lineNum": "11",
	"formType": "text",
	"lineText": "Departamento donde trabaja actualmente",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "workYears",
	"req": "Y",
	"lineNum": "12",
	"formType": "number",
	"lineText": "¿Hace cuántos años trabaja en esta empresa?",
	"formData": {"min": 0},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "cargo",
	"req": "Y",
	"lineNum": "13",
	"formType": "text",
	"lineText": "¿Cuál es el nombre del cargo que ocupa en la empresa?",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},

{
	askCode: "cargoYears",
	"req": "Y",
	"lineNum": "15",
	"formType": "number",
	"lineText": "Hace cuantos años desempeña el cargo",
	"formData": {"min": 0},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "empDpto",
	"req": "Y",
	"lineNum": "16",
	"formType": "text",
	"lineText": "Nombre del departamento, área  o sección en la que trabaja",
	"formData": null,
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "tipoCont",
	"req": "Y",
	"lineNum": "17",
	"formType": "select",
	"lineText": "Tipo de contracto que tiene actualmente",
	"formData": {"opts": "$V.tipoContratoBat"},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "hourWork",
	"req": "Y",
	"lineNum": "18",
	"formType": "number",
	"lineText": "Indique cuántas horas diaras de trabajo están establecidas habitualmente por la empresa para su cargo",
	"formData": {"min": 0},
	"group1": "do7",
	"group2": "di28"
},
{
	askCode: "tipoSalario",
	"req": "Y",
	"lineNum": "19",
	"formType": "select",
	"lineText": "Tipo de salario que recibe",
	"formData": {"opts": "$V.tipoSalarioRecibido"},
	"group1": "do7",
	"group2": "di28"
}
];
