<?php
/* no usar */
class PeP{
static $err=false;
static $errText='';
static $D=array();
static public function _reset(){
	self::$err=false; self::$errText='';
}
static public function onHand($D=array(),$P=array()){
	$lnt=$P['lnt']; self::_reset();
	$revI=($P['revD']);
	if(!array_key_exists('whsId',$P) && $D['whsId']){ $P['whsId']=$D['whsId']; }
	if(!array_key_exists('wfaId',$P) && $D['wfaId']){ $P['wfaId']=$D['wfaId']; }
	if($revI){ $ert=$lnt.'Error en consulta inventario en proceso. ';
		if($js=_js::ise($D['itemId'],$ert.'Se debe definir el artículo.')){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($D['itemSzId'],$ert.'Se debe definir la talla.')){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($P['whsId'],$ert.'Se debe definir la bodega.')){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($P['wfaId'],$ert.'Se debe definir la fase. ('.$P['wfaId'].')')){
			self::$err=true; self::$errText=$js; return self::$err;
		}
	}
	$qul='SELECT W1.onHand, W.whsName whsCode,WF.wfaName 
	FROM '._0s::$Tb['itm_owhs'].' W
	LEFT JOIN '._0s::$Tb['pep_oitw'].' W1 ON (W1.whsId=W.whsId AND W1.wfaId=\''.$P['wfaId'].'\' AND W1.itemId=\''.$D['itemId'].'\' AND W1.itemSzId=\''.$D['itemSzId'].'\' )
	LEFT JOIN '._0s::$Tb['wma_owfa'].' WF ON (WF.wfaId=\''.$P['wfaId'].'\')
	WHERE W.whsId=\''.$P['whsId'].'\' LIMIT 1';
	$qS = a_sql::fetch($qul,array(1=>'Error obteniendo inventario de producto en proceso: '));
	$enS='on PeP::onHand(). whs:'.$P['whsId'].', wfa:'.$P['wfaId'].'';
	$errType=0;
	if(a_sql::$err){ self::$err=true; self::$errText=a_sql::$errNoText; }
	else if(a_sql::$errNo==2){ self::$err=true; $errType='onHand'; 
		$errText= $lnt.'La cantidad a transferir del almacen '.$qS['whsCode'].' en fase '.$qS['wfaName'].' ('.$D['quantity'].') es mayor a la disponible.'.$enS; 
	}
	else if($qS['onHand']<$D['quantity']){ $errType='onHand'; 
		$errText=$lnt.'La cantidad a transferir del almacen '.$qS['whsCode'].' en fase '.$qS['wfaName'].' ('.$D['quantity'].') es mayor a la disponible ('.($qS['onHand']*1).').'.$enS; self::$err=true; }
	if($errType=='onHand'){
		if($P['errOnHand']){ self::$errText=_js::e(3,$P['errOnHand'].': '.$errText); }
		else{ self::$errText=_js::e(3,$errText); }
	}
	self::$D=$qS;
	return self::$err;
}
static public function onHand_put($D=array(),$P=array()){
	self::_reset();
	if(self::$errText=_js::ise($P['docDate'],'Se debe definir fecha para movimiento en pep.updOnHand.')){ self::$err=true; }
	else if(self::$errText=_js::ise($P['tt'],'TT debe estar definido para movimiento en pep.updOnHand.')){ self::$err=true; }
		else if(self::$errText=_js::ise($P['tr'],'TR debe estar definido para movimiento en pep.updOnHand.')){ self::$err=true; }
	else if(!is_array($D)){ self::$err=true; self::$errText=_js::e(3,'No se recibieron cantidades para actualizar inventario en proceso.'); }
	else{ $nl=1;
	foreach($D as $n => $L){ $ln='Linea '.$nl.': '; $nl++;
		if(self::$errText=_js::ise($L['whsId'],$ln.'La bodega debe estar definida en pep.updOnHand.')){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['itemId'],$ln.'ID artículo debe estar definido en pep.updOnHand.')){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['itemSzId'],$ln.'ID talla de artículo debe estar definido en pep.updOnHand.')){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['wfaId'],$ln.'ID fase debe estar definida en pep.updOnHand.')){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['quantity'],$ln.'La cantidad debe estar definida en pep.updOnHand.')){ self::$err=true; break; }
		$inQty=$outQty=0;
		if($L['inQty']=='Y'){ $inQty=$L['quantity']; }
		if($L['outQty']=='Y'){ $outQty=$L['quantity']; }
		unset($L['inQty'],$L['outQty']);
		$Dm = array('docDate'=>$P['docDate'],'tt'=>$P['tt'],'tr'=>$P['tr'],'whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'], 'itemSzId'=>$L['itemSzId'],'inQty'=>$inQty,'outQty'=>$outQty);
		$ins = a_sql::insert($Dm,array('qDo'=>'insert','table'=>_0s::$Tb['pep_wtr1']));
		if($ins['err']){ self::$errText= _js::e(3,$ln.'Error registrando movimientos wtr1: '.$ins['text']); self::$err=true; break; }
		$wh = 'WHERE whsId=\''.$L['whsId'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1';
		$L['._.onHand']='+-+onHand+'.$inQty.'-'.$outQty;##pasar + o -
		$L['onHand']=$inQty-$outQty;
		unset($L['quantity']);
		$ins2=a_sql::insert($L,array('tbk'=>'pep_oitw','wh_change'=>$wh));
		if($ins2['err']){ self::$errText= _js::e(3,$ln.'Error actualizando cantidades de inventario pep::onHand_put(): '.$ins2['text']); self::$err=true;  break; }
	}
	}
	return self::$err;
}
}
?>