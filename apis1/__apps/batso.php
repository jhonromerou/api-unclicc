<?php
a_sql::$limitDefBef=20;
$SOC_TbKeys = array(
'cnt_addr','cnt_phone','cnt_email',
'par_ocrd','par_ocpr',
'app_com1','app_fil1',
//Vistas
//dsp
'dsp_oitf','dsp_itf1','dsp_ofrc',
'dsp_ofem','dsp_fem1','dsp_fem2',
'dsp_fre1',
);
foreach($SOC_TbKeys as $n =>$k){ _0s::$Tb[$k]=$k; }

Doc::$sTy=array(
'batsoFem'=>array('otb'=>'dsp_ofem')
);

class batSo{
static $_Do=array();
static $_Di=array();
static $_FCV=array();
static $_RIE=array();
static $_RIEest=array();
static $_BRM=array();
static $Di=array();
static $Sum=array();
static public function get_formCode($formId=-1){
	$FK=array(1=>'intraA',2=>'intraB',3=>'extra',4=>'estres',5=>'datos');
	return $FK[$formId];
}
static public function get_sumTra($D=array(),$P=array()){
	//$FK=array(1=>'intraA_',2=>'intraB_',3=>'extra',4=>'estres',5=>'datos');
	/* puntaje  para estres
PB = puntaje bruto total = Sumatoria de A+B+C+D
Transforma PB = PB / 61.16 * 100
A = (promedio del 1 al 8)* 4    items = 8
B = (promedio del 9 al 12)*3    items= 4
C = (promedio del 13 al 22)*2   items=10
D = (promedio del 23 al 31)     items=9
*/
	$formCode=$P['formCode'];
	if($formCode=='estres'){
		$sm1= ($P['prom1']/8)*4; #1 al 8
		$sm2= ($P['prom2']/4)*3; #9 al 12
		$sm3= ($P['prom3']/10)*2; #13 al 22
		$sm4= ($P['prom4']/9); #23 al 31
		$D['suma']=round($sm1+$sm2+$sm3+$sm4,1);
	}
	if($D['suma']==61.2){ $D['suma']=61.16; } /* puntaje maximo */
	$fcvk=$P['fcvK'];
	$brmk=$P['brmK'];
	$fcv=batSo::$_FCV[$fcvk];
	if(!$fcv || $fcv===0){ die(_js::e(3,'Error obteniendo factor de transformación para '.$fcvk.', on sumTra() from '.$P['from'].'.')); }
	$porc=round($D['suma']/$fcv*100,1);
	$brm=batSo::getBrm($porc,$brmk,array('from'=>$P['from']));
	$D['sumTransform']=$porc;
	$D['fcv']=$fcv;
	$D['brm']=$brm;
	return $D;
}
static public function getBrm($puntos=0,$k='_NULL_',$P=array()){
	/* Obtener baremos de riesgo, pasando $k*/
	$BR=self::$_BRM[$k];
	if(!is_array($BR)){ die(_js::e(3,'Error obteniendo baremos para :'.$k.' ('.$P['from'].')')); }
	$noExist=true;
	foreach($BR as $n => $maxi){
		if($puntos<=$maxi){
			$noExist=false;
			if($P['r']=='text'){ return self::$_RIE[$n]; }
			else{ return $n; }
			break;
		}
	}
	if($noExist){ die(_js::e(3,'Error obteniendo valor para baremo, puntaje ('.$puntos.') no definido en baremo ('.$k.') from ('.$P['from'].')'));  }
}
static public function getKeys($formCode='',$tipoEmp=''){
	$R=array();
	if($formCode=='intra' && $tipoEmp=='A'){
		$R=array('formCode'=>'intraA','fcvK'=>'intraA_total','brmK'=>'intraA_total','dom'=>'intraA_','dim'=>'intraA_');
	}
	else if($formCode=='intra' && $tipoEmp=='B'){
		$R=array('formCode'=>'intraB','fcvK'=>'intraB_total','brmK'=>'intraB_total','dim'=>'intraB_','dom'=>'intraB_');
	}
	else if($formCode=='estres' && $tipoEmp=='A'){
		$R=array('formCode'=>$formCode,'fcvK'=>'estres_total','brmK'=>'estres_Je','dom'=>'estres','dim'=>'estres');
	}
	else if($formCode=='estres' && $tipoEmp=='B'){
		$R=array('formCode'=>$formCode,'fcvK'=>'estres_total','brmK'=>'estres_Au','dom'=>'estres','dim'=>'estres');
	}
	else if($formCode=='extra'  && $tipoEmp=='A'){
		$R=array('formCode'=>$formCode,'fcvK'=>'extra_total','brmK'=>'extraJe_do5','dom'=>'extra_','dim'=>'extraJe_');
	}
	else if($formCode=='extra'  && $tipoEmp=='B'){
		$R=array('formCode'=>$formCode,'fcvK'=>'extra_total','brmK'=>'extraAu_do5','dom'=>'extra_','dim'=>'extraAu_');
	}
	return $R;
}

static public function get_femBrm($formCode='',$Do=array()){
	/*Form:{sum, fconversion, sumTransform, brm,
		Do:{sum, fconversion, sumTransform, brm,
			Di:{ sum, fconversion, sumTransform, brm }
		}
	}*/
	if($formCode=='estres'){ return batSo::get_femBrmEstres($formCode,$Do); }
	switch($formCode){
		case 'intraA' : $x='_a'; $brmDo='intraA_'; $brmDi='intraA_'; $brmTotal='intraForma_a'; break;
		case 'intraB' : $x='_b'; $brmDo='intraB_'; $brmDi='intraB_'; $brmTotal='intraForma_b'; break;
		case 'extra' : $x=''; $brmDo='extraJe_'; $brmDi='extraJe_'; $brmTotal='extraJe_do5'; break;
		case 'estres' : $x=''; $brmDo='estresJe_'; $brmDi='estresJe_'; $brmTotal='estresJe_di27'; break;
	}
	$totalForm=0;
	foreach($Do as $k1 => $L){
		$fcv=batSo::$_FCV[$k1.$x];
		$totalForm+=$L['sum'];
		
		if($fcv<=0){
			$Do[$k1]['sumTransform']='_ERROR = 0 k1='.$k1;
			$Do[$k1]['brm']='_ERROR = 0';
		}
		else{
			$porc=round($L['sum']/$fcv*100,1);
			$Do[$k1]['fconversion']=$fcv;
			$Do[$k1]['sumTransform']=$porc;
			$Do[$k1]['brm']=batSo::getBrm($porc,$brmDo.$k1,'fromDo');
			$Do[$k1]['dom']=batSo::$_Do[$k1];
			//unset($Do[$k1]['fconversion'],$Do[$k1]['sumTransform'],$Do[$k1]['brm']);
		}
		$Do[$k1]['rowspan']=count($L['Di']);
		foreach($L['Di'] as $k2 => $L2){
			$fcv=batSo::$_FCV[$k2.$x];
			$porc=round($L2['sum']/$fcv*100,1);
			$Do[$k1]['Di'][$k2]['fconversion']=$fcv;
			$Do[$k1]['Di'][$k2]['sumTransform']=$porc;
			$Do[$k1]['Di'][$k2]['brm']=batSo::getBrm($porc,$brmDi.$k2,'fromDi');
		$Do[$k1]['Di'][$k2]['dim']=batSo::$_Di[$k2];
		//unset($Do[$k1]['Di'][$k2]['fconversion'],$Do[$k1]['Di'][$k2]['sumTransform'],$Do[$k1]['Di'][$k2]['brm']);
		}
	}
	$fcv=batSo::$_FCV[$brmTotal];
	$porc=round($totalForm/$fcv*100,1);
	$brm=batSo::getBrm($porc,$brmTotal);
	$domi='Y';
	if($formCode=='extra'){ $domi='N'; }
	$Do=array(
		'domi'=>$domi,
	'form'=>array('sum'=>$totalForm,'fconversion'=>$fcv,'sumTransform'=>$porc,'brm'=>$brm),
	'Do'=>$Do);
	return $Do;
}

static $XLS=array();
static public function fem_xls($P=array()){
	$B=array();
	$Gr=array('total'=>0,'intraA'=>$B,'intraB'=>$B,'extraA'=>$B,'extraB'=>array(),'estresA'=>$B,'estresB'=>$B);
	$wh=($P['femId'])?'AND A.femId=\''.$P['femId'].'\' ' :'';
	$qa=a_sql::query('SELECT A.* FROM '._0s::$Tb['dsp_ofem'].' A WHERE A.odocEntry=\''.$P['odocEntry'].'\' AND A.canceled=\'N\' '.$wh.' LIMIT 500',array(1=>'Error obteniendo documento: ',2=>'No se encontraron documentos.'));
	if(a_sql::$err){ $js= a_sql::$errNoText; }
	else{ $M=array('L'=>array());
	$n=0;
	while($L0=$qa->fetch_assoc()){
		$M['L'][$n]=$L0; 
		$Gr['total'] +=1;
		/* intra */
		$q=a_sql::fetch('SELECT data FROM '._0s::$Tb['dsp_fem2'].' WHERE femId=\''.$L0['femId'].'\' AND formCode=\'intra\' ',array(1=>'Error obteniendo formulario intra laboral'));
		$sumIntra='N';
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			$grK=($L0['tipoEmp']=='A')?'intraA':false;
			$grK=($L0['tipoEmp']=='B')?'intraB':$grK;
			$Gr[$grK]['total']+=1;
			$L1=json_decode($q['data'],1);
			foreach($L1 as $nx => $L){
				$k1=$L['group1']; $k2=$L['group2'];
				$brm=$L['brm'];
				if($L['lineType']=='form'){
					$sumIntra=$L['suma'];
					$Gr[$grK]['form'][$brm]+=1;
					$M['L'][$n]['intra_pt']=$L['sumTransform'];
					$M['L'][$n]['intra_rie']=$L['brm'];
				}
				else if($L['lineType']=='dom'){
					if(!array_key_exists($grK1,$Gr)){ $Gr[$grK1]=array(); }
					$Gr[$grK][$k1][$brm]+=1;
					$M['L'][$n]['intra_'.$k1.'_pt']=$L['sumTransform'];
					$M['L'][$n]['intra_'.$k1.'_rie']=$L['brm'];
				}
				else if($L['lineType']=='dim'){
					$Gr[$grK][$k2][$brm]+=1;
					$M['L'][$n]['intra_'.$k2.'_pt']=$L['sumTransform'];
					$M['L'][$n]['intra_'.$k2.'_rie']=$L['brm'];
				}
			}
		}
		/* extra */
		$q=a_sql::fetch('SELECT data FROM '._0s::$Tb['dsp_fem2'].' WHERE femId=\''.$L0['femId'].'\' AND formCode =\'extra\' ',array(1=>'Error obteniendo formulario extra laboral'));
		$sumExtra='N';
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			$grK=($L0['tipoEmp']=='A')?'extraA':false;
			$grK=($L0['tipoEmp']=='B')?'extraB':$grK;
			$Gr[$grK]['total']+=1;
			$L1=json_decode($q['data'],1);
			foreach($L1 as $nx => $L){
				$k1=$L['group1']; $k2=$L['group2']; $brm=$L['brm'];
				if($L['lineType']=='form'){
					$sumExtra=$L['suma'];
					$Gr[$grK]['form'][$brm]+=1;
					$M['L'][$n]['extra_pt']=$L['sumTransform'];
					$M['L'][$n]['extra_rie']=$L['brm'];
				}
				else if($L['lineType']=='dom'){
					$Gr[$grK][$k1][$brm]+=1;
					$M['L'][$n]['extra_'.$k1.'_pt']=$L['sumTransform'];
					$M['L'][$n]['extra_'.$k1.'_rie']=$L['brm'];
				}
				else if($L['lineType']=='dim'){
					$Gr[$grK][$k2][$brm]+=1;
					$M['L'][$n]['extra_'.$k2.'_pt']=$L['sumTransform'];
					$M['L'][$n]['extra_'.$k2.'_rie']=$L['brm'];
				}
			}
		}
		/*intraExtra */
		$intraExtra_pt=''; $brmK='intraExtra';
		if($sumIntra=='N' || $sumExtra=='N'){
			$M['L'][$n]['intraExtra_pt']='N/A';
			$M['L'][$n]['intraExtra_rie']='N/A';
		}
		else{
			if($L0['tipoEmp']=='A'){
				$intraExtra_pt=round(($sumIntra+$sumExtra)/616*100,1);
				$brmK='intraExtraA';
			}
			else if($L0['tipoEmp']=='B'){
				$intraExtra_pt=round(($sumIntra+$sumExtra)/512*100,1);
				$brmK='intraExtraB';
			}
			$M['L'][$n]['intraExtra_pt']=$intraExtra_pt;
			if($intraExtra_pt>100){ $M['L'][$n]['intraExtra_rie'] ='ERROR puntaje mayor a 100'; }
			else if($intraExtra_pt!=''){
				$M['L'][$n]['intraExtra_rie']=self::getBrm($intraExtra_pt,$brmK,array('from'=>'xlsReport'));
			}
		}
		/*estres */
		$q=a_sql::fetch('SELECT data FROM '._0s::$Tb['dsp_fem2'].' WHERE femId=\''.$L0['femId'].'\' AND formCode =\'estres\' ',array(1=>'Error obteniendo formulario estres'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			$grK=($L0['tipoEmp']=='A')?'estresA':false;
			$grK=($L0['tipoEmp']=='B')?'estresB':$grK;
			$Gr[$grK]['total']+=1;
			$L1=json_decode($q['data'],1);
			foreach($L1 as $nx => $L){
				$brm=$L['brm'];
				$Gr[$grK]['di27'][$brm]+=1;
				if($L['lineType']=='form'){
					$Gr[$grK]['form'][$brm]+=1;
					$M['L'][$n]['estres_pt']=$L['sumTransform'];
					$M['L'][$n]['estres_rie']=$L['brm'];
				}
			}
		}
		$n++;
	}
	$M['Gra']=$Gr; unset($Gr);
	$js=_js::enc($M,'just');
	}
	return $js;
}
}

{batSo::$_Do=array( /* Dominios */
'do1'=>'Liderazgo y relaciones sociales en el trabajo',
'do2'=>'Control sobre el trabajo',
'do3'=>'Demandas del trabajo',
'do4'=>'Recompensas',
'do5'=>'Factores Extralaborales',
'do6'=>'Evaluación Estres',
'do7'=>'Datos Generales'
); }

{batSo::$_Di=array( /* Dimensiones */
'di1'=>'Caracteristica del Liderazgo',
'di2'=>'Relaciones sociales en el trabajo',
'di3'=>'Retroalimentación del desempeño',
'di4'=>'Relación con los colaboradores',
'di5'=>'Claridad del rol',
'di6'=>'Capacitación',
'di7'=>'Participación y manejo del cambio',
'di8'=>'Oportunidades para el uso y desarrollo de habilidades y conocimientos',
'di9'=>'Control y autonomia sobre el trabajo',
'di10'=>'Demandas ambientales y de esfuerzo físico',
'di11'=>'Demandas emocionales',
'di12'=>'Demandas cuantitativas',
'di13'=>'Influencia del trabajo sobre el entorno extralaboral',
'di14'=>'Exigencias de responsabilidad del cargo',
'di15'=>'Demandas de carga mental',
'di16'=>'Cosistencia del rol',
'di17'=>'Demandas de la jornada de trabajo',
'di18'=>'Recompensas derivadas de la pertenencia a la organización y del trabajo que se realiza',
'di19'=>'Reconocimiento compensación',
/* factores extra */
'di20'=>'Tiempo Fuera del trabajo',
'di21'=>'Relaciones familiares',
'di22'=>'Comunicación y relaciones interpersonales',
'di23'=>'Situación económica del grupo familiar',
'di24'=>'Característica de la vivienda y de su entorno',
'di25'=>'Influencia del entorno extralaboral sobre el trabajo',
'di26'=>'Desplazamiento vivienda - trabajo - vivienda',
'di27'=>'Estres',
'di28'=>'Datos Generales'
); }

{batSo::$_RIE=array('Sin Riesgo','Riesgo Bajo','Riesgo Miedo','Riesgo Alto','Riesgo muy Alto'
);
batSo::$_RIEest=array(0=>'Muy Bajo', 1=>'Bajo',
2=>'Medio',3=>'Alto',4=>'Muy Alto'
);
}

{batSo::$_BRM=array(); /* Baremos */
//baremos para definir riesgo del total del intralaboral
//batSo::$_BRM['intraForma_a']=array(19.7,25.8,31.5,38,100);
//batSo::$_BRM['intraForma_b']=array(20.6,26,31.2,38.7,100);
batSo::$_BRM['intraA_total']=array(19.7,25.8,31.5,38,100);
batSo::$_BRM['intraB_total']=array(20.6,26,31.2,38.7,100);
//baremos para definir riesgo del total del intralaboral y extralaboral
batSo::$_BRM['intraExtraA']=array(18.8,24.4,29.5,35.4,100);
batSo::$_BRM['intraExtraB']=array(19.9,24.8,29.5,35.4,100);

batSo::$_BRM['intraA_do1']=array(9.1,17.7,25.6,34.8,100);
batSo::$_BRM['intraA_do2']=array(10.7,19,29.8,40.5,100);
batSo::$_BRM['intraA_do3']=array(28.5,35,41.5,47.5,100);
batSo::$_BRM['intraA_do4']=array(4.5,11.4,20.5,29.5,100);

batSo::$_BRM['intraB_do1']=array(8.3,17.5,26.7,38.3,100);
batSo::$_BRM['intraB_do2']=array(19.4,26.4,34.7,43.1,100);
batSo::$_BRM['intraB_do3']=array(26.9,33.3,37.8,44.2,100);
batSo::$_BRM['intraB_do4']=array(2.5,10,17.5,27.5,100);

batSo::$_BRM['intraA_di1']=array(3.8,15.4,30.8,46.2,100);
batSo::$_BRM['intraA_di2']=array(5.4,16.1,25,37.5,100);
batSo::$_BRM['intraA_di3']=array(10,25,40,55,100);
batSo::$_BRM['intraA_di4']=array(13.9,25,33.3,47.2,100);
batSo::$_BRM['intraA_di5']=array(0.9,10.7,21.4,39.3,100);
batSo::$_BRM['intraA_di6']=array(0.9,16.7,33.3,50,100);
batSo::$_BRM['intraA_di7']=array(12.5,25,37.5,50,100);
batSo::$_BRM['intraA_di8']=array(0.9,6.3,18.8,31.3,100);
batSo::$_BRM['intraA_di9']=array(8.3,25,41.7,58.3,100);
batSo::$_BRM['intraA_di10']=array(14.6,22.9,31.3,39.6,100);
batSo::$_BRM['intraA_di11']=array(16.7,25,33.3,47.2,100);
batSo::$_BRM['intraA_di12']=array(25,33.3,45.8,54.2,100);
batSo::$_BRM['intraA_di13']=array(18.8,31.3,43.8,50,100);
batSo::$_BRM['intraA_di14']=array(37.5,54.2,66.7,79.2,100);
batSo::$_BRM['intraA_di15']=array(60,70,80,90,100);
batSo::$_BRM['intraA_di16']=array(15,25,35,45,100);
batSo::$_BRM['intraA_di17']=array(8.3,25,33.3,50,100);
batSo::$_BRM['intraA_di18']=array(0.9,5,10,20,100);
batSo::$_BRM['intraA_di19']=array(4.2,16.7,25,37.5,100);

batSo::$_BRM['intraB_di1']=array(3.8,13.5,25,38.5,100);
batSo::$_BRM['intraB_di2']=array(6.3,14.6,27.1,37.5,100);
batSo::$_BRM['intraB_di3']=array(5,20,30,50,100);
//no aplica 'di4']=array(,,,,100);
batSo::$_BRM['intraB_di5']=array(0.9,5,15,30,100);
batSo::$_BRM['intraB_di6']=array(0.9,16.7,25,50,100);
batSo::$_BRM['intraB_di7']=array(16.7,33.3,41.7,58.3,100);
batSo::$_BRM['intraB_di8']=array(12.5,25,37.5,56.3,100);
batSo::$_BRM['intraB_di9']=array(33.3,50,66.7,75,100);
batSo::$_BRM['intraB_di10']=array(22.9,31.3,39.6,47.9,100);
batSo::$_BRM['intraB_di11']=array(19.4,27.8,38.9,47.2,100);
batSo::$_BRM['intraB_di12']=array(16.7,33.3,41.7,50,100);
batSo::$_BRM['intraB_di13']=array(12.5,25,31.3,50,100);
//no evalua 'di14']=array(,,,,100);
batSo::$_BRM['intraB_di15']=array(50,65,75,85,100);
//no evalua 'di16']=array(,,,,100);
batSo::$_BRM['intraB_di17']=array(25,37.5,45.8,58.3,100);
batSo::$_BRM['intraB_di18']=array(0.9,6.3,12.5,18.8,100);
batSo::$_BRM['intraB_di19']=array(0.9,12.5,25,37.5,100);

/* baremos para extralaboral */
batSo::$_BRM['extraJe_di20']=array(6.3,25,37.5,50,100);
batSo::$_BRM['extraJe_di21']=array(8.3,25,33.3,50,100);
batSo::$_BRM['extraJe_di22']=array(0.9,10,20,30,100);
batSo::$_BRM['extraJe_di23']=array(8.3,25,33.3,50,100);
batSo::$_BRM['extraJe_di24']=array(5.6,11.1,13.9,22.2,100);
batSo::$_BRM['extraJe_di25']=array(8.3,16.7,25,41.7,100);
batSo::$_BRM['extraJe_di26']=array(0.9,12.5,25,43.8,100);
batSo::$_BRM['extraJe_do5']=array(11.3,16.9,22.6,29,100);

batSo::$_BRM['extraAu_di20']=array(6.3,25,37.5,50,100);
batSo::$_BRM['extraAu_di21']=array(8.3,25,33.3,50,100);
batSo::$_BRM['extraAu_di22']=array(5,15,25,35,100);
batSo::$_BRM['extraAu_di23']=array(16.7,25,41.7,50,100);
batSo::$_BRM['extraAu_di24']=array(5.6,11.1,16.7,27.8,100);
batSo::$_BRM['extraAu_di25']=array(0.9,16.7,25,41.7,100);
batSo::$_BRM['extraAu_di26']=array(0.9,12.5,25,43.8,100);
batSo::$_BRM['extraAu_do5']=array(12.9,17.7,24.2,32.3,100);
/* baremos estres */
batSo::$_BRM['estres_Je']=array(7.8,12.6,17.7,25,100);
batSo::$_BRM['estres_Au']=array(6.5,11.8,17,23.4,100);
}

{batSo::$_FCV=array(/* factores de conversion */
/* para totales */
'intraA_total'=>492,'intraForma_a'=>492,
'intraB_total'=>388,'intraForma_b'=>388,
'extra_total'=>124,'extra_do5'=>124,
'estres_total'=>61.16,
'intraExtra_A'=>616,//sumar intra a+extra y 616
'intraExtra_B'=>512,

/* para dominios pag 83-libro 1*/
'intraA_do1'=>164,'intraA_do2'=>84,'intraA_do3'=>200,'intraA_do4'=>44,
'intraB_do1'=>120,'intraB_do2'=>72,'intraB_do3'=>156,'intraB_do4'=>40,
/* extras */
'do5'=>1,
/* para dimensiones A pag 83 libro 1*/
'intraA_di1'=>52,'intraA_di2'=>56,'intraA_di3'=>20,
'intraA_di4'=>36,'intraA_di5'=>28,'intraA_di6'=>12,
'intraA_di7'=>16,'intraA_di8'=>16,'intraA_di9'=>12,
'intraA_di10'=>48,'intraA_di11'=>36,'intraA_di12'=>24,
'intraA_di13'=>16,'intraA_di14'=>24,'intraA_di15'=>20,
'intraA_di16'=>20,'intraA_di17'=>12,'intraA_di18'=>20,
'intraA_di19'=>24,
/* Dimensiones forma b */
'intraB_di1'=>52,'intraB_di2'=>48,'intraB_di3'=>20,
'intraB_di4'=>'NA','intraB_di5'=>20,'intraB_di6'=>12,
'intraB_di7'=>12,'intraB_di8'=>16,'intraB_di9'=>12,
'intraB_di10'=>48,'intraB_di11'=>36,'intraB_di12'=>12,
'intraB_di13'=>16,'intraB_di14'=>'NE','intraB_di15'=>20,
'intraB_di16'=>'NE','intraB_di17'=>24,'intraB_di18'=>16,
'intraB_di19'=>24,
/* extra */
'extra_di20'=>16,'extra_di21'=>12,'extra_di22'=>20,
'extra_di23'=>12,'extra_di24'=>36,'extra_di25'=>12,
'extra_di26'=>16,
);
}

$_IT=array(
4=>array(0,1,2,3,4)
);

?>