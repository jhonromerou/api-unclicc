<?php

class accT{
static $err=false;
static $errText='';
static public function err($t='',$er=0){
	self::$err=true;
	self::$errText=($er)?_js::e($er,$t):$t;
	return self::$errText;
}
static public function acc_getOne($D=array()){
	$q=a_sql::fetch('SELECT '.$D['f'].' FROM gfi_opdc A WHERE '.$D['wh'].' LIMIT 1',array(1=>'Error obteniendo información de la cuenta: ',2=>'No se encontraron resultados para la cuenta buscada .'));
	if(a_sql::$err){ $js=self::err(a_sql::$errNoText); }
	else{
		if($D['r']=='D'){ return $q; }
		if($D['r']){ return $q[$D['r']]; }
		return _js::enc2($q);
	}
	return $js;
}
}

?>