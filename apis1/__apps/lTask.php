<?php
$SOC_TbKeys = array(
'gtd_otas','gtd_tas2','gtd_ooct',
'gtd_tas99','gtd_tas5',
'gtd_otli','gtd_tli1','gtd_tli2'
);
foreach($SOC_TbKeys as $n =>$k){ _0s::$Tb[$k]=$k; } unset($SOC_TbKeys);

_ADMS::_lb('Obj,Nty');
Obj::$sTy=array(
'gtdTask'=>array('gid'=>'taskId','otb'=>'gtd_otas','tb2'=>'gtd_tas2'),
'gtdList'=>array('gid'=>'listId','otb'=>'gtd_otli','tb1'=>'gtd_tli1','tb2'=>'gtd_tli2',
	'St'=>array('A'=>'Lista archivada, no se puede modificar.'),
	'userTypes'=>array('member'=>'Miembro')
)
);

class lTask{
static $oTy='gtdTask';
static $err=false;
static $errText='';
static public function qu($P=array(),$___D=array()){
	$oTy='gtdTask';
	$_fieTaskLine='A.taskId,A.completed,A.name,A.tType,A.priority,A.userAssg,A.dueDate,A.endDate,oct.news';
	$meBox=' AND (A.userId=\''.a_ses::$userId.'\' OR A.userAssg=\''.a_ses::$userId.'\')';
	$whNoStask=' AND A.parentTask=0';
	$whCom=($___D['completedView']=='completedTask')?' AND A.completed=\'Y\'':' AND A.completed=\'N\''; unset($___D['completedView']);
	
	$lef_='LEFT JOIN '._0s::$Tb['gtd_ooct'].' oct ON (oct.oTy=\''.$oTy.'\' AND oct.gid=A.taskId AND oct.userId=\''.a_ses::$userId.'\')';
	$err1=$err2=false; $Lines=false;
	if(is_numeric($P['listId'])){
		if($js=_js::ise($P['listId'],'Se debe definir el ID de la lista.')){ die($js); }
		else if($js=Obj::member(array('oTy'=>'gtdList','gid'=>$P['listId'],'memberText'=>'No tiene permisos para visualizar las actividades de esta lista.'))){ die($js); }
		$M=a_sql::fetch('SELECT L.* FROM '._0s::$Tb['gtd_otli'].' L WHERE L.listId=\''.$P['listId'].'\' ',array(1=>'Error obteniendo lista.',2=>'No se encontró la lista.'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		$Lines=true;
	}
	switch($P['listId']){
		case 'inbox':{
		$qu='SELECT A.* FROM '._0s::$Tb['gtd_otas'].' A 
		WHERE A.listId=0 '.$meBox.''.$whCom.$whNoStask;
		$err1='Error obteniendo listado de tareas del inbox';
		}break;
		case 'assg':{
		$qu='SELECT A.* FROM '._0s::$Tb['gtd_otas'].' A 
WHERE A.userAssg=\''.a_ses::$userId.'\''.$whCom.$whNoStask;
		$err1='Error obteniendo listado de tareas asignadas.';
		$err2='No hay tareas asignadas...';
		}break;
		case 'delegated':{
		$qu='SELECT A.* FROM '._0s::$Tb['gtd_otas'].' A 
WHERE A.userId=\''.a_ses::$userId.'\' AND A.userAssg!=0 AND A.userAssg!=\''.a_ses::$userId.'\' '.$whCom.$whNoStask;
		$err1='Error obteniendo listado de tareas delegadas.';
		$err2='No hay tareas delegadas...';
		}break;
		default :{
			$qu='SELECT '.$_fieTaskLine.' FROM '._0s::$Tb['gtd_otas'].' A '.$lef_.'
	WHERE A.listId=\''.$P['listId'].'\''.$whCom;
	$err1='Error obteniendo actividades de la lista.';
	$err2='No se encontraron actividades en la lista.';
		}break;
	}
	$q=a_sql::query($qu,array(1=>$err1,2=>$err2));
	if(a_sql::$err){
		self::$err=true; self::$errText=a_sql::$errNoText;
	}
	if($Lines){
		$M['qL']=$q;
	}
	else{ $M=$q; }
	return $M;
}
static public function rel_post($___D=array()){
	$taskId=$___D['taskId']; unset($___D['taskId']);
	if($js=_js::ise($taskId,'Se debe definir el ID de la tarea.')){ self::$err=true; self::$errText=$js; }
	else if($js=_js::ise($___D['tt'],'Se debe definir el tipo de relación')){ self::$err=true; self::$errText=$js; }
	else if($js=_js::ise($___D['tr'],'Se debe definir el ID de la relación')){ self::$err=true; self::$errText=$js; }
	else if($js=Obj::owner(array('oTy'=>self::$oTy,'gid'=>$taskId,'coText'=>'Solo un usuario con permisos de propietario puede modificar las relaciones de la tarea.'))){ self::$err=true; self::$errText=$js; }
	else{
		$Di=array('taskId'=>$taskId,'tt'=>$___D['tt'],'tr'=>$___D['tr'],'trMemo'=>$___D['trMemo']);
		$ins=a_sql::insert($Di,array('tbk'=>'gtd_tas5','qDo'=>'insert','kui'=>'uid_dateC',
		'deleteInsert'=>'taskId=\''.$___D['taskId'].'\' AND tt=\''.$___D['tt'].'\' AND tr=\''.$___D['tr'].'\' '));
		if($ins['err']){ self::$err=true; self::$errText=_js::e(3,'Error realizando relación a tarea.: '.$ins['text']); }
		else{
			$Di['id']=$ins['insertId'];
			Obj::logg(false,array('tbk'=>'gtd_tas99','gid'=>$taskId,'vb'=>'cardRel','vto'=>$___D['tr']));
			$js= _js::r('Relación realizada correctamente.',$Di);
		}
	}
	if(self::$err){ return self::$errText; }
	return $js;
}
static public function _99($P=array(),$ins=array()){
	$log=Obj::_99('gtd_tas99',array('taskId'=>$P['taskId'],'vb'=>$P['vb'],'vto'=>$P['vto'],'qRes'=>$ins));
}
static public function usersAt($gid=0){
	$q=a_sql::query('SELECT A.listId,A.userId,A.userAssg, A2.userId userId2, L.userId  userListId, L2.userId userListId2
	FROM '._0s::$Tb['gtd_otas'].' A
	LEFT JOIN '._0s::$Tb['gtd_tas2'].' A2 ON (A2.taskId=A.taskId)
	LEFT JOIN '._0s::$Tb['gtd_otli'].' L ON (L.listId=A.listId)
	LEFT JOIN '._0s::$Tb['gtd_tli2'].' L2 ON (L2.listId=L.listId)
	WHERE A.taskId=\''.$gid.'\' LIMIT 200
	',array(1=>'Error obteniendo usuarios de la actividad.',2=>'La actividad no existe.'));
	if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
	else{
		$M=array(); $n=0;
		while($L=$q->fetch_assoc()){
			$userId=$L['userId']; $userList=$L['userListId'];
			if($n==0){ $M=array('listId'=>$L['listId'],'L'=>array()); $n=1; }
			if($userId!=$L['userListId2'] && !_js::ise($L['userListId2'],'','numeric>0')){
				$M['L'][$L['userListId2'].'_']=array('userId'=>$L['userListId2'],'type'=>'memberList');
			}
			if($userId!=$L['userListId'] && !_js::ise($L['userListId'],'','numeric>0')){
				$M['L'][$L['userListId'].'_']=array('userId'=>$L['userListId'],'type'=>'userList');
			}
			if($userId!=$L['userId2'] && !_js::ise($L['userId2'],'','numeric>0')){
				$M['L'][$L['userId2']]=array('userId'=>$L['userId2'],'type'=>'memberTask');
			}
			if(!_js::ise($L['userId'],'','numeric>0')){
				$M['L'][$L['userId']]=array('userId'=>$L['userId'],'type'=>'owner');
			}
			if($userId!=$L['userAssg'] && !_js::ise($L['userAssg'],'','numeric>0')){
				$M['L'][$L['userAssg']]=array('userId'=>$L['userAssg'],'type'=>'userAssg');
			}
		}
		return $M;
	}
}
static public function news_write($P=array()){//evern
	//oTy,gid,userId,news,comments,attachs,dateUpd
	$dateUpd=date('Y-m-d H:i:s');
	$readd=($P['read']=='Y');
	$uid=a_ses::$userId;
	$Ur=self::usersAt($P['gid']);
	##if(_0s::$ocardcode=='alpacapruebas'){}
	if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
	else{
		$num=($P['minus']=='Y')?-1:1;
		$num2=($P['minus']=='Y')?0:1;
		$Di=array('._.news'=>'+-+news+'.$num,'news'=>$num2,'dateUpd'=>$dateUpd,'userId'=>$uid);
		if($P['k']){ $ka=$P['k'];
			$Di['._.'.$ka]='+-+'.$ka.'+'.$num; $Di[$ka]=$num2;//comments,attachs
			if($readd){ $Di['._.'.$ka]=0; $Di[$ka]=0; }
		}
		if($P['news0']){ $Di['._.news']= 0;;}
		if($P[$ka.'0']){
			$Di['._.'.$ka]= 0;
			$Di['._news']='news-'.$ka; //news=news-comments
		}
		if($readd){ $Di['._.news']=0; $Di['news']=0; }
		foreach($Ur['L'] as $_uid =>$L){
			if($L['userId']!=$uid){
				$Di['userId']=$L['userId'];
				if($P['gtdTask']!='N'){
					$Di['oTy']='gtdTask'; $Di['gid']=$P['gid'];
					$ins=a_sql::insert($Di,array('table'=>'gtd_ooct','wh_change'=>'WHERE oTy=\''.$Di['oTy'].'\' AND gid=\''.$Di['gid'].'\' AND userId=\''.$L['userId'].'\' LIMIT 1 '));
				}
				//at list
				if(1){
					$Di['oTy']='gtdList'; $Di['gid']=$Ur['listId'];
					$ins=a_sql::insert($Di,array('table'=>'gtd_ooct','wh_change'=>'WHERE oTy=\''.$Di['oTy'].'\' AND gid=\''.$Di['gid'].'\' AND userId=\''.$L['userId'].'\' LIMIT 1 '));
				}
			}
		}
	}
	//$ins=a_sql::insert($P,array('table'=>'gtd_ooct','w));
}
static public function news_read($P=array()){//evern
	$Di=array(); $uid=a_ses::$userId;
	$k=$P['k'];
	$tk=($P['k']=='news')?0:'news-'.$k; //news-comments
	$Di['._.news']='+-+'.$tk;
	if($P['k']){ $Di[$k]=0; }
	if($P['oTy']=='gtdList'){
		$u=a_sql::insert($Di,array('tbk'=>'gtd_ooct','qDo'=>'update','wh_change'=>'WHERE oTy=\'gtdList\' AND gid=\''.$P['gid'].'\' AND userId=\''.$uid.'\' LIMIT 1'));
	}
	else if($P['oTy']=='gtdTask'){
		$q=a_sql::fetch('SELECT A.listId,L.'.$k.' FROM '._0s::$Tb['gtd_otas'].' A 
		LEFT JOIN '._0s::$Tb['gtd_ooct'].' L ON (L.oTy=\'gtdTask\' AND L.gid=A.taskId AND L.userId=\''.$uid.'\') WHERE A.taskId=\''.$P['gid'].'\'  LIMIT 1',array(1=>'Error obteniendo información de tarea on lTask::news_read().'));
		if(a_sql::$errNo=='-1' && $q['listId']>0){
			$Di['._.news']='+-+news-'.$q[$k]*1;
			$u=a_sql::insert($Di,array('tbk'=>'gtd_ooct','qDo'=>'update','wh_change'=>'WHERE oTy=\'gtdList\' AND gid=\''.$q['listId'].'\' AND userId=\''.$uid.'\' LIMIT 1'));
			//print_r($u);
		}
		$u=a_sql::insert($Di,array('tbk'=>'gtd_ooct','qDo'=>'update','wh_change'=>'WHERE oTy=\'gtdTask\' AND gid=\''.$P['gid'].'\' AND userId=\''.$uid.'\' LIMIT 1'));
	}
}

static public function emailNty($taskId=0,$userId=0){
	if(!adms::active('lTask_emailNtyAssg')){ return false; }
	_ADMS::_app('Mailing');
	$Ds=Mailing::ncf('gtdTaskAssg',array('userId'=>$userId));
	if($Ds){
		_ADMS::_lb('lb/_Mailer');
		$qt=a_sql::fetch('SELECT U1.userName,T.name taskName,T.dueDate,U2.userName userNameFrom, T.notes taskNotes FROM '._0s::$Tb['gtd_otas'].' T 
		LEFT JOIN '._0s::$Tb['A0_vs0_ousr'].' U1 ON (U1.userId=T.userAssg) 
		LEFT JOIN '._0s::$Tb['A0_vs0_ousr'].' U2 ON (U2.userId=T.userId) 
		WHERE T.taskId=\''.$taskId.'\' LIMIT 1');
		$qt['userEmail']=$Ds['userEmail'];
		$qt['dueDate']=(preg_match('/^0000\-/',$qt['dueDate']))?'No definido':$qt['dueDate'];
		$D=array();
		$D['asunt'] = 'Tarea Asignada';
		$D['mailTo']=$Ds['userEmail'];
		$D['fromName'] = 'Gestor de Actividades';
		//$D['fromEmail'] = 'noresponse@calzadoalpaca.com.co';
		$D['body']=Mailing::fromTemp(array('tCode'=>'gtdTaskAssg'),$qt);
		$r=_Mail::send($D);
		if(_Mail::$err){ _err::err(_Mail::$errText,3); }
	}
}
}

class objTask extends Obj{

static public function perms($P=array()){
	$userId=a_ses::$userId; $oTy='gtdTask';
	$R=array();
	if($js=_js::ise($P['gid'],'Se debe definir ID de la tarea on objTask::perms')){ self::$err; return $js; }
	else{
		$Ds=array(); $nqu=0;
		if(array_key_exists('userId',$P) && array_key_exists('userAssg',$P)){
			$Ds['userId']=$P['userId']; $Ds['userAssg']=$P['userAssg'];
			$nqu++;
		}
		if(array_key_exists('userList',$P) && array_key_exists('userMList',$P)){
			$Ds['userList']=$P['userList']; $Ds['userAssg']=$P['userMList'];
			$nqu++;
		}
		if($nqu<2){
			$Ds=a_sql::fetch('SELECT A.listId,A.userId,A.userAssg, L.userId userList, L2.userId userMList 
			FROM '._0s::$Tb['gtd_otas'].' A 
			LEFT JOIN '._0s::$Tb['gtd_otli'].' L ON (L.listId=A.listId) 
			LEFT JOIN '._0s::$Tb['gtd_tli2'].' L2 ON (L2.listId=A.listId) 
			WHERE A.taskId=\''.$P['gid'].'\' LIMIT 1 ',array(1=>'Error obteniendo información de la actividad: ',2=>'La actividad no existe on objTask::perms.'));
		}
		if(a_sql::$err){ self::$err=true; return a_sql::$errNoText; }
		else{
			$obR='"__o":{"oTy":"'.$oTy.'","gid":"'.$P['gid'].'"}';
			$errs=array();
			$errs['member']=($P['memberText'])?$P['memberText'].' Obj::Owner':'No tiene permisos para visualizar la actividad (member on objTask::perms)';
			$errs['coText']=($P['coText'])?$P['coText'].' Obj::Owner':'No tiene permisos de propietario para esta actividad';
			$errs['assgText']=($P['assgText'])?$P['assgText'].' Obj::Owner':'No tiene permisos de propietario o usuario asignado para esta actividad.';
			$isOwner=($userId==$Ds['userId']);
			$isAssg=($userId==$Ds['userAssg']); 
			$isMember=($userId==$Ds['userList'] || $userId==$Ds['userMList']);
			switch($P['is']){
				default: # 'member': 
				if(!$isOwner && !$isAssg && !$isMember){ return _js::e(3,$errs['member'],$obR); }
				break;
				case 'assg': 
				if(!$isOwner && !$isAssg){ return _js::e(3,$errs['assgText'],$obR); }
				break;
				case 'owner':
				if(!$isOwner){ return _js::e(3,'No tieme permisos de propietario para esta actividad',$obR); }
				break;
			}
		}
	}
}
}

?>