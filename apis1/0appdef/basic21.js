
$M.liReset(true);
$M.iniSet={nty:'N',help:'N',menu:'L',mliDel:[]};

$M.liTable([
  {mdlActive:'itm',folId:'ivt',folName:'Inventarios',ico:'fa fa-cubes',folColor:'#00E',_F:[
    {folId:'Itm',folName:'Articulos',MLis:['itm.p','itm.mp','itm.se','itmSub','itmSub.gr']},
    {doc:['ivtIng','ivtEgr','ivtWht','ivtAwh','ivtRiv']},
    {mas:['tb.itmOwhs','jsv.itmGr','tb.itmOitp','gfiItmGr','ivtStock.mmr']},
    {rep:['ivtStock.p','ivtStock.pHistory','ivtStock.mp','ivtStock.mpHistory','ivtRep.ivtBal','ivtRep.rota','ivtRep.kardex']},
    {mdlActive:'wma',folId:'Cost',folName:'Costes',MLis:['ipc','ipc.mpDiff']},
    {mdlActive:'ivtGes',folId:'Bit',folName:'Gestión Lotes'},
  ]},

  {folId:'gvtSell',folName:'Ventas',ico:'fa fa-tags',folColor:'#00E',_F:[
    {doc:['gvtSop','gvtSor','gvtSdn','gvtSrd','gvtSin','gvtSnc','gvtSnd']},
    {mas:['tb.oslp','jsv.parGrC','jsv.parCprPos']},
    {rep:['finRep.cxc','finRep.estadcuenta','gvtRep.sor','gvtRep.sin']}
  ]},

  {mdlActive:'gvtPur',folId:'gvtBuy',folName:'Compras',ico:'fa fa-shopping-cart',folColor:'#00E',_F:[
    {doc:['gvtPor','gvtPdn','gvtPrd','gvtPin','gvtPnc','gvtPnd']},
    {mas:['jsv.gvtPrdRea']},
    {rep:['finRep.cxp']}
  ]},
  {folId:'crd',folName:'Terceros',ico:'fa fa-handshake-o',folColor:'purple',MLis:['crd.c','crd.s','cpr'],_F:[
    {mas:['jsv.parGrC','tb.oslp','jsv.parCprPos','jsv.parDpto']}
  ]},
  {mdlActive:'crdNov',folId:'crdNov',folName:'Seguimiento',ico:'fa fa-heartbeat',folColor:'var(--blue)',MLis:['crdNov'],_F:[
    {mas:['jsv.crdNovType','jsv.crdNovOri','jsv.crdNovPrio']},
    {rep:['crdRep.nov']}
  ]},

  {folId:'gfi',folName:'Finanzas',ico:'fa fa-bank',MLis:['gfiBan'],_F:[
    {doc:['gvtRcv','gvtRce']},
    {mas:['gfiFdp','gfiPym','gfiTax','gfiTie','tb.gfiOcdc']},
    {rep:['finRep.ing','finRep.egr']},
  ]},
  {folId:'acc',folName:'Contabilidad',ico:'fa fa-balance-scale',MLis:[],_F:[
    {mdlActive:'gfiAcc',doc:['gfiDcc']},
    {mas:['gfiPdc']},
    {mdlActive:'gfiAcc',rep:['gfiAccRep.daily','gfiAccRep.major','gfiAccRep.auxAcc','gfiAccRep.taxes','gfiAccRep.sf','gfiAccRep.er']},
  ]},

  {folId:'sys',folName:'Sistema',ico:'fa fa-user-secret',folColor:'green'},
  {fatherId:'sys',folId:'cnf',folName:'Configuración',ico:'fa fa-cog',MLis:['cnf.mecrd','cnf.docserie','cnf.meusr'],_F:[
    {mdlActive:'sysUsers',folId:'User',folName:'Usuarios',MLis:['cnf.ousr','cnf.ousp','cnf.ousa','cnf.repAssg']}
  ]},

  /* interfaces */
  {fatherId:'sys',mdlActive:'itf',folId:'itf',folName:'Interfaces',ico:'fa fa-rocket',folColor:'red'},
  {mdlActive:'itf',fatherId:'itf',_F:[
    {mdlActive:'itf',folId:'DT',folName:'Importaciones',ico:'fa fa-upload'}
  ]},
  {mdlActive:'itf',fatherId:'itfDT',_F:[
    {mdlActive:'itf',folId:'Ivt',folName:'Inventarios',MLis:['itfDT.ivtItmN','itfDT.ivtAwh','itfDT.ivtRiv']}
  ]}
]);