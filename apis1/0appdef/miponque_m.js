
Gvt.Sor.get=function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.Gvt.pr+'sor',inputs:$1.G.filter(),
	fOpts:Gvt.Sor.opts,view:'Y',docBy:'userDate',
	tbSerie:'gvtSor',
	TD:[
		{H:'Estado',k:'docStatus',_g:$V.docStatusSor},
		{H:'Fecha',k:'docDate'},
		{H:'Cliente',k:'cardName'},
		{H:'O.C',k:'ref1'},
		{H:'Fecha Entrega',k:'dueDate'},
		{H:'Total',k:'docTotal',format:'$'}
	],
	tbExport:{ext:'xlsx',fileName:'Ordenes de Venta'}
	},cont);
}

$M.liAdd2([
	{k:'xMP.label1',kMdl:'gvp',
		K:[
			{k:'get',t:'Plantilla Etiqueta',i:{g:()=>{ xMP.label1();} }},
	]},
]);
var xMP={
	label1:function(){
		var cont=$M.Ht.cont;
		Itm.sea2Add({fie:'I.sellWei',fie:'I.sellWei',noBtn:'Y',func:function(Ds){
			for(var i in Ds){ trA(Ds[i]); }
		}},cont);
		var tb=$1.T.table(['Codigo','Descripcion','Peso','']);
		var tBody=$1.t('tbody',0,tb);
		function trA(L){
			var unk=L.itemId+L.itemSzId;
			if(!$Htm.uniqLine(unk,tBody)){
			var tr=$1.t('tr',0,tBody); $Htm.uniqCls(unk,tr);
				$1.t('td',{textNode:L.barCode},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:L.sellWei*1+' g'},tr);
				var td=$1.t('td',0,tr);
				$1.lineDel({},{},td);
			}
		}
		$1.T.tbExport(tb,{ext:'txt',fileName:'DatosEtiqueta'},cont);
	}
}

$M.sAdd([
{fatherId:'gvp',mdlActive:'gvp',MLis:['xMP.label1']}
]);
