<?php
require('__0_requi.php');
require('__0_aorigin.php');
require 'phpbase2.php';
_ADMS::lib('_err,_js,a_sql,JRoute,_jwt');
a_sql::dbase(['sql_h'=>'sql1.admsistems.com','sql_u'=>'lector','sql_p'=>'Lectura1234--','sql_db'=>'admsh']);
JRoute::render('defined');

JRoute::get('routes',function(){
	return '$V.helpRoutes='.a_sql::fetchL('SELECT code,path from hlp_routes ',['kf'=>'code',1=>'Error obteniendo información',2=>'Ninguna'],true).';';
});
JRoute::get('categories',function(){
	return a_sql::fetchL('SELECT * from hlp_ocat WHERE actived=\'Y\' ORDER BY orden ASC',['k'=>'L',1=>'Error obteniendo información de categorias',2=>'Ninguna definida'],true);
});
JRoute::get('category',function($D){
	return a_sql::fetchL('SELECT vid,title from hlp_otut WHERE cId=\''.$D['cId'].'\'',['k'=>'L',1=>'Error obteniendo documentos de ayuda de la categoria',2=>'Aun no hemos creado contenido en esta categoria'],true);
});


JRoute::get('tutos',function($D){
	$wh='';
	if($_GET['tag']){
		$wh='find_in_set(\''.$D['buscar'].'\',tags)';
	}
	else{ $tx=a_sql::toSe($D['buscar']);
		$wh='(title '.$tx.' OR html '.$tx.')';
	}
	return a_sql::fetchL('SELECT vid,title from hlp_otut 
	WHERE '.$wh,
	['k'=>'L',1=>'Error obteniendo documentos de ayuda de la categoria',2=>'Aun no hemos creado contenido en esta categoria'],true);
});
JRoute::get('tuto',function($D){
	return a_sql::fetch('SELECT title,murl,tags,html from hlp_otut WHERE vid=\''.$D['vid'].'\' LIMIT 1',['k'=>'L',1=>'Error obteniendo documentos de ayuda de la categoria',2=>'Aun no hemos creado contenido en esta categoria'],true);
});
?>
