<?php
JRoute::get('mpa/cas',function($D,$P,$M){
	a_sql::$limitDefBef=30;
	return $M->get($_GET,array('prsLeft'=>'Y'));
},['hashPerms'=>'mpaCas','lib'=>'sql/filter']);
JRoute::get('mpa/cas/{gid}','getOne',['hashPerms'=>'mpaCas','_wh'=>['gid'=>'\d+']]);
JRoute::get('mpa/cas/form','getOne',['hashPerms'=>'mpaCas','mpaCas.write']);

JRoute::post('mpa/cas','post',['hashPerms'=>'mpaCas.write','lib'=>'_2d']);
JRoute::put('mpa/cas','put',['hashPerms'=>'mpaCas.write']);
?>
