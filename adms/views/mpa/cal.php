<?php
JRoute::get('mpa/cal',function($D,$P,$M){
	return $M->get($_GET);
},['hashPerms'=>'mpaCal','lib'=>'sql/filter']);
JRoute::get('mpa/cal/form','getOne',['hashPerms'=>'mpaCal.write']);
JRoute::get('mpa/cal/{gid}/events','getEvents',['hashPerms'=>'mpaCal']);

JRoute::post('mpa/cal','post',['hashPerms'=>'mpaCal.write']);
JRoute::put('mpa/cal/list','put',['hashPerms'=>'mpaCal.write']);
?>
