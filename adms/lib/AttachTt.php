<?php
class AttachTt{
static $rootPATH; 
static public function post($D=array()){
	if($js=_js::ise($D['tt'],'Se debe definir tt para guardar relación de archivo.')){ die($js); }
	else if($js=_js::ise($D['tr'],'Se debe definir tr para guardar relación de archivo.')){ die($js); }
	$L=json_decode($D['L'],1); unset($D['L']);
	_ADMS::lib('Attach');
	foreach($L as $nu => $resp){
		$D['svr'] = $resp['svr'];
		$D['fileType'] = $resp['fileType'];
		$D['fileSize'] = $resp['fileSize'];
		$D['fileSizeText'] = $resp['fileSizeText'];
		$D['file'] = $resp['file'];
		$D['purl'] = $resp['purl'];
		$D['fileName'] = $resp['fileName'];
		$fileId = a_sql::qInsert($D,array('tbk'=>'gtd_ffc1','qk'=>'ud','FIELDS'=>'insert'));
		if(a_sql::$err){
			$js = _js::e(3,'Error subiendo archivo: '.a_sql::$errText);  break;
		}
		else{ $filesUpd++;
			$L[$nu]=a_sql::$FIELDS; a_sql::$FIELDS=array();
			$L[$nu]['fileId']=$fileId;
			//$L[$nu]['purl']=Attach::getLink($resp,'g');
		}
	}
	if($filesUpd>0){ $js=_js::r('Se subieron '.$filesUpd.' archivos.',$L); }
	return $js;
}

static public function get($P=array()){
	if(_js::ise($P['tt']) || _js::ise($P['tr'])){ return _js::e(3,'No se ha definido el target para obtener los archivos.'); }
	$P['EXACTO'] = true;
	_ADMS::_lb('sql/filter');
	_ADMS::lib('Attach');
	$whe = 'WHERE 1 '.a_sql_filtByT($P);
	//gtd_ffc1
	$cList = a_sql::query('SELECT fileId,userId,svr,dateC,tt,tr,fileType,file,fileName,fileSize,fileSizeText,purl FROM gtd_ffc1 '.$whe.' '.a_sql::nextLimit(),array(1=>'Error obteniendo archivos: ',2=>'No se encontraron archivos relacionados.'));
	if(a_sql::$err){ return a_sql::$errNoText; }
	else{ $Mx=array('L'=>array());
		while($L = $cList->fetch_assoc()){
			//$L['purl']=Attach::getLink($L,'g');
			$L['canDelete'] = ($L['userId'] == a_ses::$userId) ? 'Y' :'N';
			$Mx['L'][]=$L;
		}
		$js=_js::enc($Mx);
	}
	return $js;
}
static public function deleteSave($P=array()){
self::$rootPATH=preg_replace('/\/$/','',self::$rootPATH);
	$fileId = $P['fileId'];
	if($js=_js::ise($fileId,'ID del archivo no definida.')){ die($js); }
	a_sql::transaction(); $cmt=false;
	$gFile = a_sql::fetch('SELECT userId,svr FROM gtd_ffc1 WHERE fileId=\''.$fileId.'\' LIMIT 1',array(1=>'Error eliminando archivo: '));
	if(a_sql::$err){ return a_sql::$errNoText; }
	/* else if(a_sql::$errNo==-1 && $gFile['userId'] != a_ses::$userId){
		return _js::e(4,'No tiene permisos para eliminar archivos de otro usuario.');
	} */
	else if(a_sql::$errNo==2){ $cmt=true;
		$js = _js::r('Archivo eliminado correctamente (2).',$gFile);
	}
	else{
		$q = a_sql::query('DELETE FROM gtd_ffc1 WHERE fileId=\''.$fileId.'\' LIMIT 1',array(1=>'Error eliminando archivo:'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{ $cmt=true;
			$js = _js::r('Archivo eliminado correctamente.');
		}
	}
	a_sql::transaction($cmt);
	return $js;
}
}
?>