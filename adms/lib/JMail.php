<?php
$rootPATH = realpath($_SERVER['DOCUMENT_ROOT']);
/* 2021, no usar _Mailer ni otros */
class JMail{
  static $mail;
  static $driver='api';
	static $err=false; static $errText='';
	static public function readAddress($email=''){
		preg_match('/\<(.*)\>/',$email,$ma);
		if($ma[1]) return $ma[1];
		else return $email;
  }
  static public function send($D){
    $D['from'] = (c::$EmailSvr['fromEmail'])?c::$EmailSvr['fromEmail']:'';
    if($D['fromName']){ $D['from'] = $D['fromName'].'<'.$D['from'].'>'; }
    if(_js::iseErr($D['to'],'to must defined on JMail::api()')){}
    else if(_js::iseErr($D['from'],'from must defined on JMail::api()')){}
    else if(_js::iseErr($D['subject'],'subject must defined on JMail::api()')){}
    else{
      if(self::$driver=='api'){ return self::api($D,$P); }
      else{ return self::simple($D,$P); }
    }
  }
	static public function simple($P=array(),$aD=array()){
		self::$err=false;
		/* asunt, to, body,
		fromName: Plataforma  ADMS
		fromEmail: noresponse@admsistems.com
		*/
		{
      require(c::$V['PATH_libexterna'].'phpmailer/class.phpmailer.php');
			self::$mail = new PHPMailer();
			//self::$confirmReadingTo="johnromero492@gmail.com";// recibir confirmación lectura
			self::$mail->CharSet = 'UTF-8';
			$multiTo = explode(',',$P['to']);
			$body = '<html>
			<head>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
			</head>
			<body>'.
			$P['body'].
			'</body>
			</html>';
			$fromName = $P['fromName'];
			$fromEmail = c::$EmailSvr['email'];
			if($P['replyTo']){ $replyTo = $P['replyTo']; }
			else{
				$replyTo = (c::$EmailSvr['replyTo']) ? c::$EmailSvr['replyTo']:$fromEmail;
			}

			$replyToName = ($P['replyToName']) ? $P['replyToName'] : 'Plataforma ADM';
			self::$mail->IsSMTP();
			self::$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
			if($aD['heads']){
				foreach($aD['heads'] as $k1=>$v1){
					self::$mail->addCustomHeader($k1,$v1);
				}
			}
			self::$mail->SMTPDebug  = 1;
			self::$mail->Host = c::$EmailSvr['host'];
			self::$mail->SetFrom($fromEmail,$fromName);
			self::$mail->ClearReplyTos();
			self::$mail->AddReplyTo($replyTo,$replyToName);
			self::$mail->Subject =  '=?ISO-8859-1?B?'.base64_encode(utf8_decode($P['subject'])).'=?=';
			if(count($multiTo) >1){ foreach($multiTo as $to2){
				if($to2 != ''){
					$realAddress = self::readAddress($to2);
					$addA = @self::$mail->AddAddress($realAddress);
				if(preg_match('/^Invalid address/is',$addA)){ return array(1,'Dirección Incorrecta: '.$realAddress); }
				};
			}}
			else{
				$realAddress = self::readAddress($to);
				$addA = @self::$mail->AddAddress($realAddress);
				if(preg_match('/^Invalid address/is',$addA)){ return array(1,'Dirección Incorrecta: '.$realAddress); }
			}
			if($aD['_Fi']){/* Añadir archivos [n]{}  */
				foreach($aD['_Fi'] as $n => $L){
					self::$mail->AddAttachment($L['tmp_name'],$L['name']);
				}
			}
			self::$mail->IsHTML(true);
			self::$mail->CharSet = 'UTF-8';
			self::$mail->Port = c::$EmailSvr['port']; //587 ipage, 465 2host;
			self::$mail->SMTPAuth = true;
			if(c::$EmailSvr['secure']){
				self::$mail->SMTPSecure = (c::$EmailSvr['secure'])?c::$EmailSvr['secure']:'ssl';
			}
			self::$mail->Username = c::$EmailSvr['email'];
			self::$mail->Password = c::$EmailSvr['password'];
			self::$mail->MsgHTML($body);
			try{
				@$send = self::$mail->Send();
				if(!$send){
          _err::err('Error en envio: '.self::$mail->ErrorInfo,3);
					unset(c::$EmailSvr['password']);
				}
				else{ $js = array('ok'=>1,'text'=>'Correo Enviado correctamente.'); }
			}
			catch(Exception $ex) {
				_err::err('Error en envio: '.$ex,3);
			}
		}
		return $js;
	}
	static public function api($D=array(),$P=array()){
    if(_js::iseErr(c::$EmailSvr['uid'],'uid is undefined on JMail::api()')){}
    else if(_js::iseErr(c::$EmailSvr['uri'],'uri is undefined on JMail::api()')){}
    else{
      xCurl::$attachV='v3';//version de creación archivos _Fi en curl
      //uid, uri,fromEmail
      $P['uid']=c::$EmailSvr['uid'];//api:key
      $r=xCurl::post(c::$EmailSvr['uri'],$D,$P);
      if($r['id']){ return $r; }
      else{  _err::err('Error on JMail::api() => '.$r['message'],3); return ['errNo'=>3,'text'=>$r['message']]; }
    }
		return $r;
	}
}
?>