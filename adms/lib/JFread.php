<?php
class JFread {
static public function tsv($FI='',$P=array()){
	return self::get($FI,$P);
}
static public function get($FI='',$P=array()){
	/* FI = archivo
	P{K}: Matriz numerica con el nombre de los campos 0=cardId
	*/
	if($FI['tmp_name'][0]==''){ return array('errNo'=>3,'text'=>'No se definido ningun archivo de plantilla.'); }
	$type = $FI['type'][0];
	$size = self::getSize($FI['size'][0]);
	if(!preg_match('/(text\/plain|application\/octet\-stream|application\/vnd\.ms\-excel)/',$type)){
		return array('errNo'=>3,'text'=>'El tipo de archivo no es válido: '.$type);
	}
	else if($size>2){ return array('errNo'=>3,'text'=>'El tamaño del archivo no puede ser mayor a 2MB.'); }
	$lineIni = ($P['lineIni'])?$P['lineIni']:1;
	$lineEnd = ($P['lineEnd'])?$P['lineEnd']:20;
	$Lx=file($FI['tmp_name'][0],FILE_SKIP_EMPTY_LINES);
	$lineTotal = count($Lx)-$lineIni+1;
	if($lineTotal>$lineEnd){ return array('errNo'=>3,'text'=>'El archivo no puede contener más de '.$lineEnd.' lineas de ingresos. Actual: '.$lineTotal.'.'); }
	$fp = fopen($FI['tmp_name'][0], 'rb');
	$l = 1;
	$Ln = array('H'=>array(),'L'=>array());
	//while(($line = fgets($fp)) !== false){
	foreach($Lx as $nli=>$line){
		$sep = explode('	',trim($line));
		if($l==1){ foreach($sep as $n =>$k){
			$Ln['H'][$n] = str_replace('﻿','',$k); }
		}
		else if($l>=$lineIni){
			foreach($Ln['H'] as $n=>$kn){
				if($P['keyL0']=='Y'){ $ky=$kn; }
				else{ $ky = ($P['K'] && $P['K'][$n])? $P['K'][$n]:false; }
				if($ky){
				$val = $sep[$n];
				$val = preg_replace('/^\"/','',$val);
				$val = preg_replace('/\"(\r\n)?/','',$val);
				$val = preg_replace('/(\r\n)?/','',$val);
				$Ln['L'][$l][$ky] = $val;
				}
			}
		}
		$l++;
	}
	fclose($fp);
	return $Ln;
}
static public function getSize($bytes=0,$udm='MB'){
	if($udm == 'GB'){ $bytes = number_format($bytes / 1073741824, 4); }
	else if($udm == 'MB'){ $bytes = number_format($bytes / 1048576, 4); }
	else if($udm == 'Kb'){ $bytes = number_format($bytes / 1024, 4); }
	else if($udm == 'Bytes'){ $bytes; }
	return $bytes;
}
}
?>