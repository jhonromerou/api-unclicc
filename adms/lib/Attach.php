<?php
class Attach{
static $Svr=array('sql_h'=>'svr2.admsistems.com','sql_u'=>'root','sql_p'=>'Pepa1992--','sql_db'=>'adms');
static $err=false;
static $errText='';
static $ERRORS = array(
0=>'There is no error, the file uploaded with success',
1=>'The uploaded file exceeds the upload_max_filesize directive in php.ini',
2=>'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
3=>'The uploaded file was only partially uploaded',
4=>'No file was uploaded',
6=>'Missing a temporary folder',
7=>'Failed to write file to disk.',
8=>'A PHP extension stopped the file upload.',
);
static public function _r(){ self::$err=false; self::$errText= ''; }
//Comun
static public function getFileSize($bytes=0,$mb='Mb'){
	$bytes=($bytes==0)?16:$bytes;
	switch($mb){
		case 'Gb': $b = number_format($bytes/1073741824,6); break;
		case 'Mb': $b = number_format($bytes/1048576,6); break;
		case 'Kb': $b = number_format($bytes/1024,6); break;
		case 'Mb': $b = number_format($bytes/1048576,6); break;
		default: $b = number_format($bytes/1048576,6); break;
	}
	return $b;
}
static public function getFileSizeText($bytes=0){
	$bytes=($bytes==0)?16:$bytes;
	if ($bytes >= 1073741824){ $bytes = number_format($bytes / 1073741824, 2) . ' Gb'; }
	elseif ($bytes >= 1048576){ $bytes = number_format($bytes / 1048576, 2) . ' Mb'; }
	elseif ($bytes >= 1024){ $bytes = number_format($bytes / 1024, 2) . ' Kb'; }
	elseif ($bytes > 1){ $bytes = $bytes . ' Bytes'; }
	elseif ($bytes == 1){ $bytes = $bytes . ' Byte'; }
	else{ $bytes = '0 Bytes'; }
	return $bytes;
}
static public function getExt($name=''){
	$ext = explode('.',$name); $c = count($ext)-1;
	$ext = $ext[$c];
	return $ext;
}
static public function getTypeMe($ext=''){
	if(preg_match('/(png|jpeg|jpg|gif)/is',$ext)){ $fileType = 'img';}
	else if(preg_match('/(xls|xlsx)/is',$ext)){ $fileType = 'xls';}
	else if(preg_match('/(csv|tsv)/is',$ext)){ $fileType = 'xls2';}
	else if(preg_match('/(txt|rtf)/is',$ext)){ $fileType = 'txt';}
	else if(preg_match('/(html)/is',$ext)){ $fileType = 'html';}
	else if(preg_match('/(avi|mpeg|mp4)/is',$ext)){ $fileType = 'video';}
	else if(preg_match('/(mp3|wav|ra|au|aiff)/is',$ext)){ $fileType = 'audio';}
	else if(preg_match('/(doc|docx)/is',$ext)){ $fileType = 'doc';}
	else if(preg_match('/(pdf)/is',$ext)){ $fileType = 'pdf';}
	else { $fileType = $ext; }
	return $fileType;
}
static public function errUp($FI=array(),$n=0){
	self::_r();
	$errNo=($FI['error'] && $FI['error'][$n])?$FI['error'][$n]:0;
	if($errNo!=0){
		$adde=($errNo==1)? ' Max. Mb: '.ini_get('upload_max_filesize'):'';
		self::$err=true; self::$errText=_js::e(3,'Error Attach Num ('.$errNo.'): '.self::$ERRORS[$errNo].$adde);
	}
}
static public function getSvr($svr='L'){
	$url='undefined_'.$svr;
	switch($svr){
		case 'L': $url=c::$V['STOR_URI']; break;
		case 'stor1': $url='http://stor1.admsistems.com'; break;
		case 'api0': $url='http://stor1.admsistems.com'; break;
		case 'svr2' :$url='http://static2.admsistems.com';
		break;
	}
	return $url;
}
static public function getLink($D=array(),$type='g'){
	$url ='undefinedSvr_'.$D['svr'].'/';
	if($type=='u'){ $endf='/__mnt/uploadFile.php'; }
	else if($type=='d'){ $endf='/__mnt/delete.php'; }
	else if($type=='delMulti'){ $endf='/__mnt/deleteMultiple.php'; }
	else if($type=='g'){ $endf='/__mnt/downFile.php?file='.$D['file'].'&fileName='.urlencode($D['fileName']); }
	return self::getSvr($D['svr']).$endf;
}
static public function getFile($P=array()){
	self::_r();
	a_sql::$DBn=1;
	a_sql::dbase(c::$Sql2);
	$q=a_sql::fetch('SELECT A.ocardId,A.tbFile,A.filePath, F.svr,F.svrLocal FROM ocard A LEFT JOIN ofil F ON (F.ocardId=A.ocardId) WHERE A.ocardCode=\''.$P['ocardcode'].'\' LIMIT 1',array(1=>'Error obteniendo información on Attach::revStor().',2=>'El ocardcode ('.$P['ocardcode'].') no existe en nuestros servidor at Attach::revStor().'));
	if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
	else{
		$q2=a_sql::fetch('SELECT fileName,url,mimeType FROM '.$q['tbFile'].' WHERE svrFileId=\''.$P['fileId'].'\' LIMIT 1',array(1=>'Error obteniendo información del archivo on Attach::getFile.',2=>'El archivo no se encuentra en Attach::getFile.'));
		a_sql::$DBn=false;
		if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
		else{
			$q2['svr']=$q['svr'];
			$q2['svrLocal']=$q['svrLocal'];
			$q2['filePath']=$q['filePath'];
			return $q2;
		}
	}
}

//adms
static public function getInfo($P=array()){
	a_sql::dbase(c::$Sql2);
	$ocard=($P['ocard'])?','.$P['ocard']:'';
	$qu1='SELECT A.ocardId'.$ocard.' ';
	$qu2='FROM ocard A ';
	if($P['ofil']){
		$qu1 .=','.$P['ofil'];
		$qu2 .= 'JOIN ofil F ON (F.ocardId=A.ocardId) ';
	}
	$q=a_sql::fetch($qu1.' '.$qu2.' WHERE A.ocardCode=\''._0s::$ocardcode.'\' LIMIT 1',array(1=>'Error obteniendo información on Attach::revStor().',2=>'El ocardcode ('._0s::$ocardcode.') no existe en nuestros servidor at Attach::revStor().'));
	if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
	else{
		if($P['ofil']){
			switch($q['svr']){
				case 'L' : $s=$q['svrLocal']; break;
				case 'stor1' : $s='http://stor1.admsistems.com'; break;
				case 'api0' : $s='http://api0.admsistems.com'; break;
				case 'svr2' : $s='http://svr2.admsistems.com'; break;
			}
			$q['svr']=$s; unset($q['svrLocal']);
		}
		return $q;
	}
}
static public function revStor($P=array()){
	self::_r(); $ori= ' on[attach::revStor()]';
	a_sql::dbase(c::$Sql2);
	$q=a_sql::fetch('SELECT ocardId,tbFile,filePath FROM ocard WHERE ocardCode=\''._0s::$ocardcode.'\' LIMIT 1',array(1=>'Error obteniendo información on Attach::revStor().'.$ori,2=>'El ocardcode ('._0s::$ocardcode.') no existe en nuestros servidor.'.$ori));
	if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
	else{
		$q2=a_sql::fetch('SELECT * FROM ofil WHERE ocardId=\''.$q['ocardId'].'\' LIMIT 1',array(1=>'Error obteniendo información de almacenamiento on Attach::revStor().',2=>'No se encontrarón parametros de almacenamiento para '._0s::$ocardcode.'. on Attach::revStor().'));
		if(a_sql::$err){ self::$err=true; self::$errText= a_sql::$errNoText; }
		else if($P['sizeUpd']>$q2['fileMaxSize']){
			self::$err=true; self::$errText=_js::e(3,'El tamaño máximo por archivo es de '.($q2['fileMaxSize']*1).' Mb.');
		}
		else if($q2['storOpen']==0){
			self::$err=true; self::$errText=_js::e(3,'Alcanzó el máximo de almacenamiento permitido para usted: '.($q2['storMnt']*1).' Mb.');
		}
		else if($P['sizeUpd'] && $P['sizeUpd']>$q2['storOpen']){
			self::$err=true; self::$errText=_js::e(3,'El tamaño del archivo ('.$P['sizeUpd'].' Mb) excede el espacio actualmente disponible: '.($q2['storOpen']*1).' Mb.');
		}
		else{
			$q['fileMaxSize']=$q2['fileMaxSize']*1;
			$q['svr']=$q2['svr']; #api0, L
			$q['svrLocal']=$q2['svrLocal']; #api0, L
			return $q;
		}
	}
}
static public function curUpd($FIx=array(),$P=array()){
	//$FI=$_FILES['fileName'];
	$rev=array();
	if($P['revStor']=='Y'){
		$sizeUpd=self::getFileSize($FIx['size'][0]);
		$rev=self::revStor(array('sizeUpd'=>$sizeUpd));
		if(self::$err){ echo self::$errText; return false; }
	}
	$errIni=self::errUp($FIx,0);
	if(self::$err){ echo self::$errText; return false; }
	$FI=array('name'=>$FIx['name'][0],'type'=>$FIx['type'][0],'tmp_name'=>$FIx['tmp_name'][0]);
	if($FIx['error']){ }
	self::_r();
	$args=array();
	foreach($rev as $nk=>$nv){ $args['_'.$nk]=$nv; }
	$args['file'] = curl_file_create($FI['tmp_name'],$FI['type'],$FI['name']);
	$headers = array("Content-Type" => "multipart/form-data");
	$headers['ocardtooken']=_0s::$ocardtooken;
	$ch = curl_init();
	$svrPost=Comm::getSvr($args['_svr'],'/1/attach/uploadCurl');
	#$args['_svr'].'http://192.168.0.104:8051/1/attach/uploadCurl';
	curl_setopt($ch, CURLOPT_URL,$svrPost.'?___ocardtooken='._0s::$ocardtooken);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resp = curl_exec($ch);
	$err=curl_errno($ch);
	if($err){ self::$errText=_js::e(3,'Error on Attac::curUpd. '.$err); self::$err=true; }
	else{
		$info=curl_getinfo($ch);
		if($info['http_code']=='200'){ $js=$resp; }
	}
	return $js;
}
static public function updStor($P=array()){
	$ori=' on[attach::updStor()]';
	if($js=_js::ise($P['ocardId'],'Se debe Id de ocard.'.$ori,'numeric>0')){ return $js; }
	$q3=a_sql::query('UPDATE ofil SET storOpen=storOpen-'.$P['fileSize'].', storUsaged=storUsaged+'.$P['fileSize'].' WHERE ocardId=\''.$P['ocardId'].'\' LIMIT 1',array(1=>'Error actualizando control de almacenamiento on[Attach::updStor()].'.$ori));
	if(a_sql::$err){ return a_sql::$errNoText; }
	else{ return false; }
}
static public function curSend($FIx=array(),$P=array()){
	//$FI=$_FILES['fileName'];
	$rev=array();
	if($P['revStor']=='Y'){
		$sizeUpd=self::getFileSize($FIx['size'][0]);
		$rev=self::revStor(array('sizeUpd'=>$sizeUpd));
		if(self::$err){ echo self::$errText; return false; }
		$svrLocal=$rev['svrLocal']; unset($rev['svrLocal']);
	}
	$errIni=self::errUp($FIx,0);
	if(self::$err){ echo self::$errText; return false; }
	$FI=array('name'=>$FIx['name'][0],'type'=>$FIx['type'][0],'tmp_name'=>$FIx['tmp_name'][0]);
	if($FIx['error']){ }
	self::_r();
	$args=array();
	foreach($rev as $nk=>$nv){ $args['_'.$nk]=$nv; }
	$args['file'] = curl_file_create($FI['tmp_name'],$FI['type'],$FI['name']);
	$headers = array("Content-Type" => "multipart/form-data");
	$headers['ocardtooken']=_0s::$ocardtooken;
	$svrPost=false;
	switch($args['_svr']){
		case 'stor1' : $svrPost='http://stor1.admsistems.com/__mnt/uploadFile.php'; break;
		case 'api0' : $svrPost='http://static1.admsistems.com/__mnt/uploadFile.php'; break;
		case 'svr2' : $svrPost='http://static2.admsistems.com/__mnt/uploadFile.php'; break;
		case 'L' : $svrPost= $svrLocal.'/__mnt/uploadFile.php'; break;
	}
	$R=_curl::post($svrPost.'?___ocardtooken='._0s::$ocardtooken,array('D'=>$args,'H'=>$headers));
	if(_curl::$err){ $js= _curl::$errText; }
	else{
		$js=self::saveAdm($R,$args);
	}
	return $js;
}

static public function saveAdm($Fi=array(),$P=array()){//tabla ofil cliente
	$M=array('text'=>'Archivos guardados correctamente','L'=>array());
	$fileSize=self::getFileSize($Fi['size']);
	$fileSizeText=self::getFileSizeText($Fi['size']);
	a_sql::transaction(); $cmt=false;
	$filePath=$Fi['file'];
	$Di=array('fileName'=>$Fi['name'],
	'fileType'=>$Fi['fileType'],'mimeType'=>$Fi['type'],
	'svr'=>$P['_svr'],'url'=>$Fi['url'],'fileSize'=>$fileSize,'fileSizeText'=>$fileSizeText);
	$ins=a_sql::insert($Di,array('table'=>$P['_tbFile'],'qDo'=>'insert','kui'=>'dateC'));
	if($ins['err']){
		$js=_js::e(1,'Error guardando archivo en Attach::saveAdm. '.$ins['text']);
	}
	else{ $cmt=true;
		unset($Di['url'],$Di['mimeType']);
		//$Di['svrTooken']=_jwt::encText(_0s::$ocardcode.'.'.$ins['insertId']);
		$Di['svrFileId']=$ins['insertId'];
		$sup=self::updStor(array('fileSize'=>$fileSize,'ocardId'=>$P['_ocardId']));
		if($sup){ $js=$sup; }
		else{
			$Di['file']=$filePath;
			$M['L'][]=$Di;
			$js=_js::enc2($M);
		}
	}
	a_sql::transaction($cmt);
	echo $js;
}

//attach anter
static $upL=array();
static public function storageRev($P=array(),$revi=true){
	if(c::$V['STOR_PATH']!='_DB_'){
		return array('svr'=>'L','filePath'=>c::$V['STOR_PATH']);
	}
	_ADMS::lib('a_mdl');
	a_sql::$DBn=2;
	a_sql::dbase(c::$Sql2);
	$P['ocardCode']=a_ses::$ocardCode;
	$R=a_mdl::storGet($P,$revi);
	a_sql::$DBn=false;
	if(_err::$err){}
	else{ return $R; }
}

static public function delLast(){
	if(is_array(self::$upL) && count(self::$upL)>0){ self::del(self::$upL); }
}
static public function del($FL=array()){
	$q=self::storageRev(array(),false);
	a_sql::$DBn=2;
	if(!_err::$err){
		$sizeUpd=0; $FDE=array(); $nx=0;
		foreach($FL as $n=> $L){ $sizeUpd -=$L['fileSize']; $FDE[]=$L['file']; $nx++; }
		$svrPost=self::getLink($q,'delMulti');
		$headers = array('ocardtooken'=>_0s::$ocardtooken);
		$Fi=xCurl::post($svrPost.'?___ocardtooken='._0s::$ocardtooken,array('L'=>$FDE,'h'=>$headers));
		if(xCurl::$err){ self::$err=true; self::$errText=xCurl::$errText; }
		else{
			if(c::$V['STOR_PATH']=='_DB_'){
				a_mdl::storPut(array('ocardId'=>$q['ocardId'],'qty'=>$sizeUpd));
				if(_err::$err){ self::$err=true; self::$errText=_err::$errText; }
			}
		}
	}
	a_sql::$DBn=false;
}

static public function post($FIx=array(),$P=array()){
	$sizeUp=0; self::$upL=array();
	$args=array(); $FD=array('upTotal'=>0,'maxFile'=>0);
	foreach($FIx['name'] as $n =>$X){
		$FD['uptotal']+=$FIx['size'][$n];
		if($FIx['size'][$n]>$FD['maxFile']){ $FD['maxFile']=$FIx['size'][$n]; }
		$errIni=self::errUp($FIx,$n);
		if(self::$err){ _err::err(self::$errText); return self::$errText; }
	}
	$args['filePath']=c::$V['STOR_PATH'];
	$args['postMultiple']='Y';
	$args['_Fi']=$FIx; unset($FIx);
	if($P['revStor']=='Y'){
		$FD['uptotal']=self::getFileSize($FD['uptotal']);
		$FD['maxFile']=self::getFileSize($FD['maxFile']);
		$rev=self::storageRev($FD);
		$args['filePath']=$rev['filePath'];
		$args['svr']=$rev['svr'];
		if(_err::$err){ return _err::$errText; }
	}
	$args['uriStor']=c::$V['STOR_URI'];
	$svrPost=self::getLink($args,'u');
	$Fi=xCurl::post($svrPost,$args,array('h'=>$headers));
	/*
	echo "\n".'uri: '.$svrPost;
	echo "\n".'txt: '; print_r(xCurl::$txt);
	echo "\n".'args: '; print_r($args);
	echo "\n".'Fi: ';print_r($Fi); # */
	if(xCurl::$err){ _err::err(xCurl::$errText); $js= xCurl::$errText; }
	else{ $js=json_encode($Fi); }
	$lnx=(array_key_exists('Ln',$P));
	if($P['L'] || $lnx){
		if(is_array($Fi)){ self::$upL=$Fi;
			if($lnx){ return $Fi[$P['Ln']]; }
			return $Fi;
		}
	}
	return $js;
}
/* crear en tabla directamente */
static public function postAndTb1($FIx=array(),$P=array()){
	//$iF=Attach::post($_FILES['file'],array('Ln'=>0,'revStor'=>'Y'));
	$iF=Attach::post($FIx,array('Ln'=>0,'revStor'=>'Y'));
	if(_err::$err){ return false; }
	$iF['tt']=$P['tt']; $iF['tr']=$P['tr'];
	$iF['fileId']=a_sql::qInsert($iF,array('tbk'=>'gtd_ffc1','qk'=>'ud'));
	if(a_sql::$err){
		Attach::delLast();
		a_sql::query('DELETE FROM gtd_ffc1 WHERE fileId=\''.$iF['fileId'].'\' LIMIT 1');
		_err::err('Error guardando archivo: '.a_sql::$errText,3);
	}
	return $iF;
}
static public function delAndTb1($P=array()){
	$ori=' on[Attach::delAndTb1]';
	$tbk='gtd_ffc1'; $js=false;
	if($js=_js::ise($P['fileId'],'Se debe definir Id de archivo.','numeric>0')){ _err::err($js); }
	else{
		$qA=a_sql::fetch('SELECT F.fileId,F.file,fileSize FROM '.$tbk.' F
		WHERE F.fileId=\''.$P['fileId'].'\' LIMIT 1',array(1=>'Error obteniendo información del archivo a eliminar.'.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==2){ $js=_js::r('Proceso ya realizado anteriormente.'); }
		else{
			Attach::del(array($qA));
			if(Attach::$err){ _err::err(Attach::$errText); }
			a_sql::query('DELETE FROM '.$tbk.' WHERE fileId=\''.$P['fileId'].'\' LIMIT 1',array(1=>'Error eliminando relación de archivo.'.$ori));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			$js=_js::r('Archivo eliminado correctamente.');
		}
	}
	return $js;
}


//viejo
static public function getLinkv10($D=array(),$type='down'){
	$url ='undefinedSvr_'.$D['svr'].'/';
	$endf='';
	/* ojo con cambio de host */
	$Rf=Attach::getFile(array('ocardcode'=>_0s::$ocardcode,'fileId'=>$D['svrFileId']));
	if($type=='down'){
		$endf='/downfile'.$Rf['filePath'].$Rf['url'].'?fileName='.urlencode($D['fileName']);
	}
	else if($type=='delete'){
		$endf='/deletef_ile'.$Rf['filePath'].$Rf['url'];
	}
	$url=self::getSvr($D['svr'],$Rf) .$endf;
	return $url;
}
}
?>
