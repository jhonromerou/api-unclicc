<?php
class mpaCal{
static $tbk='mpa_ocal';
static $tbk7='mpa_cal7';
static public function rev($P=array()){
	if(_js::iseErr($P['calName'],'Se debe definir nombre del calendario')){}
	else if($js=_js::textMax($P['calName'],30,'Nombre')){ _err::err($js); }
	else if(_js::iseErr($P['privacity'],'Se debe definir privacidad')){}
	else if($P['privacity']=='S' && (!isset($P['M']) || count($P['M'])==0)){ _err::err('El tipo de privacidad requiere que defina miembros.',3); }
}
static public function post($P=array()){
	self::rev($P); if(_err::$err){ return _err::$errText; }
	else{
		$M=$P['M']; unset($P['M']);
		a_sql::transaction(); $cmt=false;
		$calId=a_sql::qInsert($P,array('tbk'=>self::$tbk,'qk'=>'ud'));
		if(a_sql::$err){ return _err::err('Error creando calendario. '.a_sql::$errText,3); }
		else{
			$P['calId']=$calId;
			_ADMS::lib('iPerms');
			uPerms::$tbk7=self::$tbk7;
			uPerms::put($P['calId'],$M);
			if(_err::$err){ return _err::$errText; }
		}
		if(_err::$err){ return $js=_err::$errText; }
		else{ $cmt=true;
			$js=_js::r('Calendario creado correctamente.',$P);
		}
		a_sql::transaction($cmt);
	}
	return $js;
}
static public function put($P=array()){
	if(_js::iseErr($P['calId'],'Se debe definir Id de calendario a actualizar','numeric>0')){ return false; }
	_ADMS::lib('iPerms');
	uPerms::verif(array('perms'=>'O','tbk7'=>self::$tbk7,'gid'=>$P['calId']));
	if(_err::$err){ return _err::$errText; }
	self::rev($P);
	if(_err::$err){ return _err::$errText; }
	else{
		$M=$P['M']; unset($P['M']);
		a_sql::transaction(); $cmt=false;
		a_sql::qUpdate($P,array('tbk'=>self::$tbk,'wh_change'=>'calId=\''.$P['calId'].'\' LIMIT 1'));
		if(a_sql::$err){ return _err::err('Error actualizando calendario.'.a_sql::$errText,3); }
		else{
			_ADMS::lib('iPerms');
			uPerms::$tbk7=self::$tbk7;
			uPerms::put($P['calId'],$M);
			if(_err::$err){ return _err::$errText; }
		}
		if(_err::$err){ $js=_err::$errText; }
		else{ $cmt=true;
			$js=_js::r('Calendario actualizado correctamente.',$P);
		}
		a_sql::transaction($cmt);
	}
}
static public function getOne($P=array()){
	if(_js::iseErr($P['calId'],'Se debe definir Id de calendario')){ return _err::$errText; }
	_ADMS::lib('iPerms');
	uPerms::verif(array('perms'=>'O','tbk7'=>self::$tbk7,'gid'=>$P['calId']));
	if(_err::$err){ return _err::$errText; }
	$M=a_sql::fetch('SELECT A.*FROM '.self::$tbk.' A
	WHERE calId=\''.$P['calId'].'\' LIMIT 1',array(1=>'Error obteniendo información.',2=>'La información no existe.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		$M['M']=array();
		$q=a_sql::query('SELECT A.id,A.lineType,A.memType,A.memId,A.perms,U.userName lineText,UT.teamName
		FROM '.self::$tbk7.' A
		LEFT JOIN a0_vs0_ousr U ON (U.userId=A.memId)
		LEFT JOIN a0_oust UT ON (UT.teamId=A.memId)
		WHERE A.gid=\''.$P['calId'].'\' LIMIT 20',array(1=>'Error obteniendo miembros del calendario.'));
		if(a_sql::$err){ $M['M']=json_decode(a_sql::$errNoText,1); }
		else if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){
				if($L['lineType']=='T'){ $L['lineText']=$L['teamName']; unset($L['teamName']); }
				$M['M'][]=$L;
			}
		}
		$js=_js::enc2($M);
	}
	return $js;
}
static public function get($P=array(),$P2=array()){
	$wh='';
	$cardId=($P['cardId']);
	$prsId=($P['prsId']);
	_ADMS::lib('iPerms');
	$rJ=uPerms::joinn(array('tbk7'=>self::$tbk7,'gid'=>$P['calId']));
	$ordBy='ORDER BY ';
	if($P['ordBy']){$ordBy .= $P['ordBy']; }
	else { $ordBy .= 'doDate'; }
	$ordBy .=($P['ordByT'])?' '.$P['ordByT']:' DESC';
	if($P['whText']){ $wh .=' AND ('.$P['whText'].') '; };
	$profile=$P['profile']; unset($P['profile']);
	$wh .= a_sql_filtByT($P['wh']);
	$M['L']=array();
	$qu='SELECT A.*
	FROM '.self::$tbk.' A	'.$rJ['t7On'].' (T7.gid=A.calId)	LEFT '.$rJ['t'].'
	WHERE '.$rJ['wh'].' ';
	$q=a_sql::query($qu,array(1=>'Error obteniendo resultados.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		} unset($q);
		return _js::enc($M);
	}
}
static public function getEvents($P=array()){
	_ADMS::lib('iPerms');
	uPerms::verif(array('perms'=>'O','tbk7'=>self::$tbk7,'gid'=>$P['calId']));
	if(_err::$err){ return _err::$errText; }
	$wh='';
	$M['L']=array();
	$qu='SELECT A.* FROM mpa_otas A	WHERE A.calId=\''.$P['calId'].'\' LIMIT 500';
	$q=a_sql::query($qu,array(1=>'Error obteniendo eventos.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		} unset($q);
		return _js::enc($M);
	}
}
}
?>
