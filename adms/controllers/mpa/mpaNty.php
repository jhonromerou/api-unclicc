<?php
class mpaNty{
public $userAssg=0;
public function userAssg($P){
	$this->userAssg=0;
	if($P['userAssg']>0){//Solo si hay uno definido nuevo
		$qd=a_sql::fetch('SELECT userAssg FROM mpa_otas  WHERE gid=\''.$P['gid'].'\' LIMIT 1');
		if(a_sql::$errNo==-1){ $this->userAssg=$qd['userAssg']; }
	}
}
public function mail2Assg($P=array()){
	if($P['userAssg']>0 && $this->userAssg!=a_ses::$userId && $this->userAssg!=$P['userAssg'] ){
		_ADMS::lib('_2d,xCurl,_File,_MailCurl');
		$qA2=a_sql::fetch('SELECT U1.userName userNameFrom,U2.userName userName, U2.userEmail FROM mpa_otas C
			LEFT JOIN a0_vs0_ousr U1 ON (U1.userId=C.userId)
			LEFT JOIN a0_vs0_ousr U2 ON (U2.userId=C.userAssg)
			WHERE gid=\''.$P['gid'].'\' LIMIT 1');
			$Dte=array('userName'=>$qA2['userName'],'userNameFrom'=>$qA2['userNameFrom'],'title'=>$P['title'],'taskNotes'=>$P['shortDesc'],'prsName'=>$P['prsName'],'cardName'=>$P['cardName'],'endDate'=>_2d::f($P['endDate'].' '.$P['endDateAt'],'d M H:i'));
			$m = new _MailCurl(array('accK'=>'mpaTasAssg','templateCode'=>'crmTaskAssg'));
			$m->addFile(['file_jsData'=>$Dte]);
			$m->send(['mailTo'=>$qA2['userEmail']]);
	}
}

static public function eveInvSave($Ls=array(),$P=array()){
	$errs=0;
	if(_js::iseErr($P['gid'],'Id de evento no definido','numeric>0')){ $errs++;}
	else if(is_array($Ls)){
		$Li=array();
		foreach($Ls as $n => $L){ $fro=' ('.$L['id'].'-'.$L['email'].')';
			$revisa=(!array_key_exists('id',$L));
			if($revisa && $L['delete']){ continue; }
			else if($revisa && ($L['lineType']!='free' && !$L['oid'])){ _err::err('Se debe definir oid para relacionar invitado.'.$fro,3); $errs=1; break; }
			else if($revisa && _js::iseErr($L['email'],'Email no definido correctamente.'.$fro)){ $errs=1; break; }
			else{
				$L[0]='i';
				$L[1]=mpaEve::$tbkEve;
				$L['_unik']='id';
				$L['gid']=$P['gid'];
				$Li[]=$L;
			}
		}
		if($errs==0){
			a_sql::multiQuery($Li);
			if(_err::$err){ $errs=1; }
		}
	}
}
static public function eveInvSend($D=array()){
	$q=a_sql::query('SELECT id,email FROM '.mpaEve::$tbkEve.' WHERE gid=\''.$D['gid'].'\' AND inviteSend=\'N\' ',array('Error obteniendo correos para notificación.'));
	if(a_sql::$err){ _err::err($js); }
	else if(a_sql::$errNo==2){  }
	else{
		_ADMS::lib('_File,_MailCurl,Mailing,_2d,xCurl');
		$Dte=array('eventName'=>$D['title']);
		$Dte['eventDate']=_2d::dateLong($D['doDate'],$D['endDate']);
		$Dte['eventLocation']=$D['wLocation'];
		$Dte['ocardName']=a_ses::$ocardCode;
		$fileCal='guardarencalendario'.a_ses::$userId.time().'.ics';
		$m = new _MailCurl(array('accK'=>'crmMailEventInv','templateCode'=>'crmEventInv'));
		$m->addFile(['file_jsData'=>$Dte]);
		$m->addFile( Mailing::ics_create(array('date1'=>$D['doDate'].' '.$D['doDateAt'],'date2'=>$D['endDate'].' '.$D['endDateAt'],'title'=>$D['title'],'cn'=>'Invitación Evento','mailto'=>'noresponse@admsistems.com','location'=>$D['wLocation'])),$fileCal);
		while($L=$q->fetch_assoc()){
			$m->send(['mailTo'=>$L['email']]);
			if(_err::$err){ return _err::$errText; break; }
		}
	}
}
}
?>
