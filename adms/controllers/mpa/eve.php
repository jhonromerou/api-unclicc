<?php
require('mpaNty.php');
class mpaEve{
static $tbk='mpa_otas';
static $tbkEve='mpa_oaev';
static $ob='E';
static public function rev($P=array()){
	//if($js=_js::ise($P['gType'],'El tipo debe estar definidido','numeric>0')){}
	if(_js::iseErr($P['title'],'Se debe definir el asunto del evento.')){}
	else if($js=_js::textMax($P['title'],200,'Asunto')){ _err::err($js); }
	else if(_js::iseErr($P['doDate'],'Se debe definir la fecha de inicio.')){ }
	else if(_js::iseErr($P['endDate'],'Se debe definir la fecha de fin.')){}
	else if(strtotime($P['endDate']) <strtotime($P['doDate'])){ _err::err('Fecha de fin es menor a fecha inicial.',3); }
	else if($js=_js::textMax($P['shortDesc'],500,'Descripción ')){ _err::err($js); }
}
static public function post($P=array()){
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	self::rev($P); if(_err::$err){ return _err::$errText;; }
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['timeDuration']=_2d::relTime($P['doDate'],$P['endDate']);
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	$P['doDate']  = substr($P['doDate'],0,10);
	$P['endDate'] = substr($P['endDate'],0,10);
	$P['obj']='E';
	$INV=$P['INV']; unset($P['INV']);
	a_sql::transaction(); $cmt=false;
	$gid=a_sql::qInsert($P,array('tbk'=>self::$tbk,'qk'=>'ud'));
	if(a_sql::$err){ _err::err('Error guardando evento.'.a_sql::$errText,3); }
	else{
		$P['gid']=$gid;
		$P['prsName']=$prsName; $P['cardName']=$cardName;
		mpaNty::eveInvSave($INV,array('gid'=>$P['gid']));
		if(_err::$err){ return _err::$errText; }
		mpaNty::eveInvSend($P);
		//if(_err::$err){ $js=_err::$errText; }
		if(0){}
		else{ _err::$err=false; $cmt=true;
			$js=_js::r('Evento creado correctamente.',$P);
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText;; }
	return $js;
}
static public function put($P=array()){
	if(_js::iseErr($P['gid'],'Se debe definir Id de evento a actualizar','numeric>0')){ return false; }
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	self::rev($P); if(_err::$err){ return _err::$errText; }
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['timeDuration']=_2d::relTime($P['doDate'],$P['endDate']);
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	$P['doDate']  = substr($P['doDate'],0,10);
	$P['endDate'] = substr($P['endDate'],0,10);
	$P['obj']='E';
	$INV=$P['INV']; unset($P['INV']);
	a_sql::transaction();
	$Rx=a_sql::qUpdate($P,array('tbk'=>self::$tbk,'wh_change'=>'gid=\''.$P['gid'].'\' LIMIT 1'));
	if(a_sql::$err){ return _err::err('Error actualizando evento.'.a_sql::$errText,3); }
	else{
		$P['prsName']=$prsName; $P['cardName']=$cardName;
		mpaNty::eveInvSave($INV,array('gid'=>$P['gid']));
		if(_err::$err){ return _err::$errText; }
		else{
			mpaNty::eveInvSend($P);
			if(0){}//if(_err::$err){ return _err::$errText; }
			else{ $js=_js::r('Evento actualizado correctamente.',$P);
				a_sql::query('UPDATE '.self::$tbkEve.' SET inviteSend=\'Y\' WHERE gid=\''.$D['gid'].'\' AND inviteSend=\'N\' LIMIT 100');
			}
		}
	}
	a_sql::transaction(true);
	return $js;
}
static public function getOne($P=array()){
	$fie='A.*,P.name,C.cardName';
	$fie .=($P['_fie'])?','.$P['_fie']:'';
	if($P['__fields']){ $fie .=','.$P['__fields']; }
	$M=a_sql::fetch('SELECT '.$fie.'
	FROM '.self::$tbk.' A
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE gid=\''.$P['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información.',2=>'La información de gid:'.$P['gid'].', no existe.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		$M['doDate'] .=' '.$M['doDateAt'];
		$M['endDate'] .=' '.$M['endDateAt'];
		$M['iL']=array();
		$q=a_sql::query('SELECT A.id,A.lineType,A.oid,A.email,P.name prsName
		FROM '.self::$tbkEve.' A
		LEFT JOIN par_ocpr P ON (P.prsId=oid)
		WHERE A.gid=\''.$P['gid'].'\' LIMIT 20',array(1=>'Error obteniendo invitados al evento'));
		if(a_sql::$err){ $M['iL']=json_decode(a_sql::$errNoText,1); }
		else if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){
				if($L['lineType']=='prs'){ $L['line1']=$L['prsName']; unset($L['prsName']); }
				$M['iL'][]=$L;
			}
		}
		$js=_js::enc2($M);
	}
	return $js;
}
static public function get($P=array(),$P2=array()){
	$wh='';
	$cardId=($P['cardId']);
	$prsId=($P['prsId']);
	a_sql::$limitDefBef=1;
	unset($P['cardId'],$P['prsId']);
	if($cardId>0){
		_ADMS::lib('iPerms');
		iPerms::slp2Crd($cardId);
		if(_err::$err){ return (_err::$errText); }
	}
	$ordBy='ORDER BY ';
	if($P['ordBy']){$ordBy .= $P['ordBy']; }
	else { $ordBy .= 'doDate'; }
	$ordBy .=($P['ordByT'])?' '.$P['ordByT']:' DESC';
	if($P['whText']){ $wh .=' AND ('.$P['whText'].') '; };
	$profile=$P['profile']; unset($P['profile']);
	/* Permisos con base tercero o usuario */
	$wh .= a_sql_filtByT($P['wh']);
	$M['L']=array();
	$fie='A.gid,A.title,A.gType,A.gPrio,A.userAssg,A.doDate,A.doDateAt,A.endDate,A.endDateAt,A.wLocation';
	$fie.=($P['_fie'])?','.$P['_fie']:'';
	$le='';
	if(!$prsId){
		$fie.=',A.prsId,P.name';
		$le .='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)';
	}else{ $wh .=' AND A.prsId=\''.$prsId.'\' '; }
	if(!$cardId){
		$fie.=',C.cardId,C.cardName';
		$le .='LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}else{ $wh .=' AND A.cardId=\''.$cardId.'\' '; }
	$JoinTb='';
	if($P2['Join']){
		$fie .=','.$P2['Join'][0];
		$le .=$P2['Join'][1].' ';
	}
	$qu='SELECT '.$fie.' FROM '.self::$tbk.' A '.$le.'
	WHERE obj=\'E\' '.$wh.' '.$ordBy.' '.a_sql::nextLimit();
	$q=a_sql::query($qu,array(1=>'Error obteniendo resultados.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		} unset($q);
		return _js::enc($M);
	}
}
}
?>
