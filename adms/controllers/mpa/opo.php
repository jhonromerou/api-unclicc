<?php
class mpaOpo{
static $tbk='mpa_oopo';
static $ob='E';
static public function rev($P=array()){
	//if($js=_js::ise($P['gType'],'El tipo debe estar definidido','numeric>0')){}
	if(_js::iseErr($P['title'],'Se debe definir el nombre de la oportunidad')){}
	else if($js=_js::textMax($P['title'],150,'Titulo')){ _err::err($js); }
	else if(_js::iseErr($P['bal'],'Se debe definir el valor de la oportunidad.','numeric>0')){ }
	else if(_js::iseErr($P['percWin'],'Se debe definir la probabilidad de la oportunidad.','numeric')){ }
	else if(_js::iseErr($P['funId'],'Se debe definir el Embudo.','numeric>0')){ }
	else if(_js::iseErr($P['stageId'],'Se debe definir la Etapa.','numeric>0')){ }
	else if(_js::iseErr($P['dueDate'],'Se debe definir la fecha estimada.')){ }
	else if($P['docStatus']!='O' && _js::iseErr($P['closeDate'],'Se debe definir la fecha de cierre.')){ }
	else if($js=_js::textMax($P['shortDesc'],300,'Descripción ')){ _err::err($js); }
}
static public function defVal($D=array()){
	if($D['docStatus']!='O'){
		$D['timeDuration']=0;
	}
	return $D;
}
static public function post($P=array()){
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	self::rev($P); if(_err::$err){ return _err::$errText; }
	$gid=a_sql::qInsert($P,array('tbk'=>self::$tbk,'qk'=>'ud'));
	if(a_sql::$err){ _err::err('Error guardando oportunidad.'.a_sql::$errText,3); }
	else{
		$P['gid']=$gid;
		$js=_js::r('Oportunidad creada correctamente.',$P);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
static public function put($P=array()){
	if(_js::iseErr($P['gid'],'Se debe definir Id de oportunidad a actualizar','numeric>0')){ return false; }
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	self::rev($P); if(_err::$err){ return _err::$errText; }
	a_sql::qUpdate($P,array('tbk'=>self::$tbk,'wh_change'=>'gid=\''.$P['gid'].'\' LIMIT 1'));
	if(a_sql::$err){ _err::err('Error actualizando oportunidad.'.a_sql::$errText,3); }
	else{
		$js=_js::r('Oportunidad actualizada correctamente.',$P);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
static public function getOne($P=array()){
	$fie='A.*,P.name,C.cardName';
	$fie .=($P['_fie'])?','.$P['_fie']:'';
	if($P['__fields']){ $fie .=','.$P['__fields']; }
	$M=a_sql::fetch('SELECT '.$fie.'
	FROM '.self::$tbk.' A
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE gid=\''.$P['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información.',2=>'La información de gid:'.$P['gid'].', no existe.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{ $js=_js::enc2($M); }
	if(_err::$err){ return _err::$errText; }
	return $js;
}

static public function get($P=array(),$P2=array()){
	$wh='';
	$cardId=($P['cardId']);
	$prsId=($P['prsId']);
	unset($P['cardId'],$P['prsId']);
	if($cardId>0){
		_ADMS::lib('iPerms');
		iPerms::slp2Crd($cardId);
		if(_err::$err){ return (_err::$errText); }
	}
	if($P['whText']){ $wh .=' AND ('.$P['whText'].') '; };
	$wh .= a_sql_filtByT($P['wh']);
	$M['L']=array();
	$fie='A.gid,A.title,A.docStatus,A.funId,A.stageId,A.gOri,A.bal,A.percWin,A.gType,A.gPrio,A.userAssg,A.dueDate';
	$fie.=($P['_fie'])?','.$P['_fie']:'';
	$le='';
	if(!$prsId){
		$fie.=',A.prsId,P.name';
		$le .='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)';
	}else{ $wh .=' AND A.prsId=\''.$prsId.'\' '; }
	if(!$cardId){
		$fie.=',C.cardId,C.cardName';
		$le .='LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}else{ $wh .=' AND A.cardId=\''.$cardId.'\' '; }
	$JoinTb='';
	if($P2['Join']){
		$fie .=','.$P2['Join'][0];
		$le .=$P2['Join'][1].' ';
	}
	$qu='SELECT '.$fie.'	FROM '.self::$tbk.' A	'.$le.'
	WHERE 1 '.$wh.' '.$ordBy.a_sql::nextLimit();
	$q=a_sql::query($qu,array(1=>'Error obteniendo novedades.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		}
		return _js::enc($M);
	}
}
}
?>
