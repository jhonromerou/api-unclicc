<?php
$serieType='wmaDcf';
$serieDoc=array('serieType'=>$serieType,'docEntry'=>$___D['docEntry']);
Doc::$owh=false;
if(_0s::$router=='GET dcf'){
	$___D['fromA']='A.*,C.cardName 
	FROM wma3_odcf A LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET dcf/tb99'){
	echo Doc::tb99($serieType,$___D);
}
else if(_0s::$router=='GET dcf/view'){
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],
	'fromA'=>'C.cardName,A.* FROM wma3_odcf A LEFT JOIN par_ocrd C ON (C.cardId=A.cardId) ',
	'fromB'=>'B.nfId,B.tt,B.tr,B.lineType,B.itemId,B.itemSzId,B.cardId,C.cardName,I.itemCode,I.itemName,I.udm,B.wfaId,B.whsId,B.whsIdFrom,B.quantity,B.rejReason FROM wma3_dcf1 B 
	JOIN itm_oitm I ON (I.itemId=B.itemId)
	LEFT JOIN par_ocrd C ON (C.cardId=B.cardId)'));
}
else if(_0s::$router=='POST dcf'){
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha del documento.')){}
	else if($___D['docType']=='E' && $js=_js::ise($___D['cardId'],'Se debe definir el tercero.')){}
	else if(!is_array($___D['L']) || count($___D['L'])==0){ $js=_js::e(3,'No se enviaron lineas.'); }
	else{
		a_sql::transaction(); $cmt=false;
		$docEntryN=$___D['docEntry']=a_sql::nextID('wma3_odcf');
		if(_err::$err){ $errs=1; }
		$L2=$___D['L']; unset($___D['L'],$___D['cardName']);
		$n=1; $errs=0; $Liv=array();
		_ADMS::_app('wma3'); $quantity=0;
		_ADMS::libC('wma','ivtPep');
		if(!_err::$err){
			$ivtPep=new ivtPep(['hands'=>true,'tt'=>'wmaDcf','tr'=>$docEntryN,'docDate'=>$___D['docDate']]);
			wma3::i_wfaFS();
			$odpNum=0; /* contador para ver si hay ordenes de producción a revisar */
			/* revisar campos y definir variables para transferencia Liv */
			foreach($L2 as $nk=>$L){ $ln='Linea '.$n.': '; $n++;
				$rej=($L['lineType']=='R');
				$L['quantity']*=1;
				if($js=_js::ise($L['itemId'],$ln.'Se debe definir ID del artículo.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir ID del subproducto.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['whsIdFrom'],$ln.'Se debe definir el almacen de componentes.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['whsId'],$ln.'Se debe definir el almacen de ingreso.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['wfaId'],$ln.'Se debe definir la fase realizada.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser un número mayor a 0.','numeric>0')){ $errs++; break; }
				else if($rej && $js=_js::ise($L['rejReason'],$ln.'Se debe definir el motivo del rechazado.')){ $errs++; break; }
				else{
					$wfaIdBef=0;
					if($L['tt']=='wmaOdp'){ /*revisar orden: estado fase y obt fase anterior */
						$odpNum++;
						/* Revisar fas anterior en odp */
						$qf=wma3::odp_faseAnt(array('docEntry'=>$L['tr'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
						if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
						$qFa=wma3::odp_fase(array('docEntry'=>$L['tr'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
						if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
						else{ $wfaIdBef=$qFa['wfaIdBef'];
							$L['pdp_docEntry']=$qFa['tr'];/*definir numero para actualizar si es necesario plan de producción */
						}
					}
					else{/* consultar fase anterior del producto */
						$qFa=wma3::mpg_fasePosi($L);
						if(_err::$err){ $js=_err::$errText; $errs++; break; }
						else{ $wfaIdBef=$qFa['wfaIdBef']; }
					}
					$L['wfaIdBef']=$wfaIdBef;
					$L=wma3::dcf_lineTotalDef($L,$qFa,'lineTotal');
					$L2[$nk]=$L;
					$quantity+=$L['quantity'];
					/* consumir componentes si no es fase-0 e ingresar nuevos comp. */
					if($wfaIdBef!=wma3::$V['wfaFS']){
						$Lr=array('outQty'=>'Y','whsId'=>$L['whsIdFrom'],'wfaId'=>$wfaIdBef,'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'price'=>$L['price'],'lineTotal'=>$L['lineTotal']);
						$ivtPep->getInfo($Lr); if(_err::$err){ break; }
						$ivtPep->handRevi(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
						$ivtPep->handSett(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					}
					if($errs==0){
						if($L['lineType']=='R'){ $L['wfaId']=wma3::$V['wfaFS-RC']; }
						else if($L['lineType']=='D'){ $L['wfaId']=wma3::$V['wfaFS-NC']; }
						$Lr=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'price'=>$L['price'],'lineTotal'=>$L['lineTotal']);
						$ivtPep->getInfo($Lr); if(_err::$err){ break; }
						$ivtPep->handRevi(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
						$ivtPep->handSett(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
						//$Liv[]=array('inQty'=>'Y','whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'price'=>$L['price'],'lineTotal'=>$L['lineTotal']);
					}
				}
			}
		}
		if($errs==0 && $quantity==0){ $errs++; $js=_js::e(3,'No se enviaron cantidades a recibir'); }
		if($errs==0 && $odpNum>0){/* odp20 - actualizar fase de ordenes */
			foreach($L2 as $nk=>$L){ $ln='Linea '.$n.': '; $n++;
				$quantity=$L['quantity'];
				$wfaIdBef=$L['wfaIdBef'];
				$pdp_docEntry=$L['pdp_docEntry'];
				$isRej=($L['lineType']=='R');
				if($L['tt']=='wmaOdp'){
					$setq=($isRej) 
					?'openQty=openQty-'.$quantity.',rejQty=rejQty+'.$quantity
					:'openQty=openQty-'.$quantity.',compQty=compQty+'.$quantity;
					wma3::odp_fasePut(array('qSet'=>$setq,'docEntry'=>$L['tr'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId']),array('ln'=>$ln));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
					/* Si es fase inicial, quitar de programado en wmaPdp */
					if($wfaIdBef==wma3::$V['wfaFS']){
						wma3::pdp_putLine(array('docEntry'=>$pdp_docEntry,'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty-'.$L['quantity']));
						if(_err::$err){ $js=_err::$errText; $errs++; break; }
					}
				}
			}
		}
		if($errs==0){/* Generar doc y lineas */
			//$docDate=date('Y-m-d'); $___D['docDate']=$docDate;
			$rt=wma3::dcf_post($___D,$L2);
			if(_err::$err){ $js=_err::$errText; $errs++; }
		}
		if($errs==0 && count($ivtPep->Livt)>0){// actualizar inventario pep 
			a_sql::multiQuery($ivtPep->Livt);
			if(_err::$err){ $errs=1; $js=_err::$errText; }
		}
		/* if($errs==0 && count($Liv)>0){// actualizar inventario pep 
			PeP::onHand_put($Liv,array('docDate'=>$___D['docDate'],'tt'=>'wmaDcf','tr'=>$docEntryN));
			if(PeP::$err){ $js=PeP::$errText; $errs++; }
		} */
		if($errs==0){ $cmt=true;
			$js=_js::r('Documento de cierre generado correctamente.','"docEntry":"'.$docEntryN.'"');
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntryN,'dateC'=>1));
		}
		_err::errDie();
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='POST dcf/fromDdf'){
	_ADMS::lib('iDoc');
	$docEntry=$___D['docEntry']; unset($___D['docEntry']);
	iDoc::vStatus(array('tbk'=>'wma3_oddf','docEntry'=>$docEntry));
	if(_err::$err){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $cmt=false;
		$docEntryN=$___D['docEntry']=a_sql::nextID('wma3_odcf');
		if(_err::$err){ $errs=1; }
		$L2=$___D['L']; unset($___D['L']); $n=1; $errs=0; $Liv=array();
		_ADMS::mApps('wma/wma3,wma/PeP'); $quantity=0;
		_ADMS::libC('wma','ivtPep');
		$docDate=date('Y-m-d');
		if(!_err::$err){
			$ivtPep=new ivtPep(['hands'=>true,'tt'=>'wmaDcf','tr'=>$docEntryN,'docDate'=>$docDate]);
			wma3::i_wfaFS();
			/* revisar campos y definir variables para transferencia Liv */
			$cardId=0;
			foreach($L2 as $nk=>$L){ $ln='Linea '.$n.': '; $n++;
				$whsIdTo=$L['whsId']; /* almacen donde recibo */
				if(!array_key_exists('quantity',$L) || $L['quantity']==''){
					unset($L2[$nk]); continue;
				}
				$rej=($L['lineType']=='R');
				$L['quantity']*=1;
				if($js=_js::ise($L['nfId'],$ln.'Se debe definir la nota del documento de fase.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.','numeric>0')){ $errs++; break; }
				else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser un número mayor a 0.','numeric>0')){ $errs++; break; }
				else if($rej && $js=_js::ise($L['rejReason'],$ln.'Se debe definir el motivo del rechazado.')){ $errs++; break; }
				else{ $quantity+=$L['quantity'];
					/*Consultar documento de fase. La linea del documento tiene la fase anterior */
					$qf=wma3::ddf_getLine($L,array('ln'=>$ln));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
					else{
						$cardId=$qf['cardId'];
						$L2[$nk]['pdp_docEntry']=$qf['tr'];
						$L2[$nk]['whsIdFrom']=$qf['whsId'];
						$L2[$nk]['wfaIdBef']=$qf['wfaIdBef'];
						$L2[$nk]['cardId']=$qf['cardId'];
						$L2[$nk]['wfaId']=$qf['wfaId'];
						$L2[$nk]['itemId']=$L['itemId']=$qf['itemId'];
						$L2[$nk]['itemSzId']=$L['itemSzId']=$qf['itemSzId'];
						$isFase0=($qf['wfaIdBef']==wma3::$V['wfaFS']);
						if(!$isFase0){
							/* Revisar fas anterior en odp */
							if($qf['tt']=='wmaOdp'){
								wma3::odp_faseAnt(array('docEntry'=>$qf['tr'],'wfaId'=>$qf['wfaIdBef'],'itemId'=>$qf['itemId'],'itemSzId'=>$qf['itemSzId'],'quantity'=>$L['quantity']),$ln);
								if(wma3::$err){ $errs++; $js=wma3::$errText;break; }
							}
							/* Revisar Inventario en la fase anterior a lo que se va a cerrar */
							//PeP::onHand($L,array('revD'=>true,'lnt'=>$ln));
							//if(PeP::$err){ $js=PeP::$errText; $errs++;  break; }
						}
						$L2[$nk]['tt']=$qf['tt']; /* relacionado a orden de producción o no */
						$L2[$nk]['tr']=$qf['tr'];
						if($qf['whsId']>0){ /*mover inventario */
							$Liv=array('whsId'=>$qf['whsId'],'wfaId'=>$qf['wfaIdBef'],'itemId'=>$qf['itemId'],'itemSzId'=>$qf['itemSzId'],'quantity'=>$L['quantity']);
							$ivtPep->getInfo($Liv); if(_err::$err){ break; }
							$ivtPep->handRevi(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
							$ivtPep->handSett(['outQty'=>$L['quantity']]);
						}
						if($L['lineType']=='R'){ $qf['wfaId']=wma3::$V['wfaFS-RC']; }
						else if($L['lineType']=='D'){ $qf['wfaId']=wma3::$V['wfaFS-NC']; }
						$L=array('whsId'=>$whsIdTo,'wfaId'=>$qf['wfaId'],'itemId'=>$qf['itemId'],'itemSzId'=>$qf['itemSzId'],'quantity'=>$L['quantity']);
						$ivtPep->getInfo($L); if(_err::$err){ break; }
						$ivtPep->handRevi(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
						$ivtPep->handSett(['inQty'=>$L['quantity']]);
					}
				}
			}
			//if($errs==1){ $errs++; }
			if($errs==0&& $quantity==0){ $errs++; 	$js=_js::e(3,'No se enviaron cantidades a recibir'); }
		}
		/* odp20 - actualizar fase de ordenes y doc de fase */
		if($errs==0){
			foreach($L2 as $nk=>$L){ $ln='Linea '.$n.': '; $n++;
				$quantity=$L['quantity'];
				$wfaIdBef=$L['wfaIdBef'];
				$pdp_docEntry=$L['pdp_docEntry'];
				$isRej=($L['lineType']=='R');
				if($L['tt']=='wmaOdp'){
					$setq=($isRej) 
					?'onProQty=onProQty-'.$quantity.',rejQty=rejQty+'.$quantity
					:'onProQty=onProQty-'.$quantity.',compQty=compQty+'.$quantity;
					wma3::odp_fasePut(array('qSet'=>$setq,'docEntry'=>$L['tr'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId']),array('ln'=>$ln));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
				}
				if($errs==0){/* Actualizar documento de fase */
					$setq='openQty=openQty-'.$quantity;
					if($isRej){ $setq .= ',rejQty=rejQty+'.$quantity; }
					else{ $setq .= ',compQty=compQty+'.$quantity; }
					wma3::ddf_put(array('nfId'=>$L['nfId'],'qSet'=>$setq),$ln);
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
				}
			}
		}
		if($errs==0){/* Generar doc y lineas */
			$docDate=date('Y-m-d'); $___D['docDate']=$docDate;
			$___D['tt']='wmaDdf'; $___D['tr']=$docEntry;
			$___D['cardId']=$cardId;
			$rt=wma3::dcf_post($___D,$L2);
			if(_err::$err){ $js=_err::$errText; $errs++; }
		}
		if($errs==0 && count($ivtPep->Livt)>0){// actualizar inventario pep 
			a_sql::multiQuery($ivtPep->Livt);
			if(_err::$err){ $errs=1; }
		}
		/* if($errs==0 && count($Liv)>0){// actualizar inventario pep
			PeP::onHand_put($Liv,array('docDate'=>$docDate,'tt'=>$serieType,'tr'=>$docEntryN));
			if(PeP::$err){ $js=PeP::$errText; $errs++; }
		} */
		if($errs==0){ $cmt=true;
			$js=_js::r('Documento de cierre generado correctamente.','"docEntry":"'.$docEntryN.'"');
			_ADMS::lib('JLog');
			JLog::post(array('tbk'=>'wma3_doc99','serieType'=>$serieType,'docEntry'=>$docEntryN,'lineMemo'=>'create from wmaDdf #'.$docEntry,'dateC'=>1));
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}

else if(_0s::$router=='PUT dcf/statusCancel'){ a_ses::hashKey('wma3.dcf.statusC');
	if($js=_js::ise($___D['docEntry'],'Se debe definir Id del documento a anular.')){ }
	else{
		/* Obtener lineas del documento a cerrar */
		$q=a_sql::query('SELECT A.docStatus,A.emiStatus,A.tt ttDoc,A.tr trDoc,B.id,B.nfId,B.tt,B.tr,B.whsId,B.wfaId,B.wfaIdBef,B.lineType,B.itemId,B.itemSzId,B.quantity FROM wma3_odcf A 
		JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry) 
		WHERE A.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo información de documento',2=>'El documento '.$___D['docEntry'].' no existe o no tiene lineas.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $errs=0; $Liv=array();
			_ADMS::_app('wma3'); wma3::i_wfaFS();
			a_sql::transaction(); $n=1;
			while($L=$q->fetch_assoc()){
				/* Revisar que el documento no este cerrado o anulado total o por emisiones */
				if($n==1){
					if($L['docStatus']=='N'){ $js=_js::e(3,'El documento ya ha sido anulado.'); $errs++; break; }
					else if($L['docStatus']=='C'){ $js=_js::e(3,'El documento está cerrado y no puede anularse.'); $errs++; break; }
					else if($L['emiStatus']=='C'){ $js=_js::e(3,'Las emisiones del documento ya han sido cerradas, no se puede anular el documento.'); $errs++; break;
					}
				}
				if($L['wfaIdBef']==0){ $L['wfaIdBef']= wma3::$V['wfaFS']; }
				$qty=$L['quantity']; $ln='Linea '.$n.': '; $n++;
				$fromDdf=($L['nfId']>0);
				/* Si hay relación a n. fabr de ddf, actualizar */
				if($fromDdf){
					$qSet=($L['lineType']=='R')
					?'openQty=openQty+'.$qty.',rejQty=rejQty-'.$qty
					:'openQty=openQty+'.$qty.',compQty=compQty-'.$qty;
					wma3::ddf_put(array('nfId'=>$L['nfId'],'qSet'=>$qSet));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
				}
				/* Si hay relacion a una odp
				1. actualizar las cantidades con base a lo anulado.
				2. Si es tiene relaciona a pdp y es fase0, aumentar lo programado
				*/
				if($errs==0 && $L['tt']=='wmaOdp'){
					$L['docEntry']=$L['tr'];
					$L['qSet']=($L['lineType']=='R')
					?'openQty=openQty+'.$qty.',rejQty=rejQty-'.$qty
					:'openQty=openQty+'.$qty.',compQty=compQty-'.$qty;
					/*si esta relacionda a ddf, no aumento openQty, xk ya lo hizo el ddf */
					if($fromDdf){
						$L['qSet']=($L['lineType']=='R')
						?'rejQty=rejQty-'.$qty.',onProQty=onProQty+'.$qty
						:'compQty=compQty-'.$qty.',onProQty=onProQty+'.$qty;
					}
					wma3::odp_fasePut($L,array('revOdp'=>'Y','ln'=>$ln));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
					/* Si viene desde ddf, no aumento pdp porque ya lo hice en el ddf y esta en inventario */
					else if(!$fromDdf && wma3::$D['pdp_docEntry'] && $L['wfaIdBef']==wma3::$V['wfaFS']){
						wma3::pdp_putLine(array('docEntry'=>wma3::$D['pdp_docEntry'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty+'.$L['quantity']));
						if(_err::$err){ $js=_err::$errText; $errs++; break; }
					}
				}
			}
			$docDate=date('Y-m-d');/* reversar movimientos de inventario */
			if($errs==0){ _ADMS::_app('wma3.PeP');
				$docDate=date('Y-m-d');
				PeP::onHand_rever(array('tt'=>'wmaDcf','tr'=>$___D['docEntry'],'docDate'=>$docDate));
				if(_err::$err){ $js=_err::$errText; $errs++; }
			}
			/* Anular el documento si todo es correcto */
			if($errs==0){
				$upd=a_sql::query('UPDATE wma3_odcf SET docStatus=\'N\',canceled=\'Y\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1 ',array(1=>'Error anulando documento de cierre de fase.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
				else{ $js=_js::r('Documento Anulado correctamente.'); 
					Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'lineMemo'=>$___D['lineMemo'],'docStatus'=>'N'));
				a_sql::transaction(true); }
			}
		}
	}
	echo $js;
}

else if(_0s::$router=='GET dcf/emision'){ 
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número de documento de cierre','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT * FROM wma3_odcf WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información del documento de cierre.',2=>'El documento de cierre no existe.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$gb='B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.wfaId';
	$q=a_sql::query('SELECT '.$gb.',B.quantity 
	FROM wma3_dcf1 B
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo información del documento de cierre.',2=>'El documento de cierre no existe.'));
	if(a_sql::$err){ $js=ac_sql::$errNoText; }
	else{ $M['L']=array(); $M['Lc']=array(); $M['Lm']=array();
		_ADMS::lib('sql/filter'); 
		_ADMS::libC('wma','mrp');
		wmaMrp::$MR=array(); $errs=0;
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L; $L['reqQty']=$L['quantity'];
			$whI=array('T1.wfaId'=>$L['wfaId']);
			$MR=wmaMrp::get($L,array('wh'=>$whI,'fKey'=>'itemId'));
			if($MR){ echo $MR; $errs++; break; }
		}
		if($errs==0){
			$gb='B.id,B.itemId,I.itemCode,I.itemName,I.udm,I.buyFactor,I.buyPrice,B.whsId,B.detail ';
			$qr=a_sql::query('SELECT '.$gb.',SUM(B.realQty) realQty
			FROM wma3_dcf2 B 
			LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
			WHERE B.docEntry=\''.$___D['docEntry'].'\' GROUP BY '.$gb,array(1=>'Error obteniendo emisiónes realizadas'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
			else if(a_sql::$errNo==-1){
				while($L=$qr->fetch_assoc()){
					$k=$L['itemId'];
					if(!array_key_exists($k,wmaMrp::$MR)){ wmaMrp::$MR[$k]=$L; }
					else{ 
						wmaMrp::$MR[$k]['realQty'] += $L['realQty'];
					}
				}
			}
		}
		if($errs==0){
			$gb='B.id,B.itemId,I.itemCode,I.itemName,I.udm,I.buyFactor,I.buyPrice,B.whsId,B.detail,B.realQty';
			$qr=a_sql::query('SELECT '.$gb.'
			FROM wma3_dcf2 B 
			LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
			WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo emisiónes realizadas'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
			else if(a_sql::$errNo==-1){
				while($L=$qr->fetch_assoc()){
					$M['Lm'][]=$L;
				}
			}
		}
		if($errs==0){
			$M['Lc']=wmaMrp::$MR;
			$js=_js::enc2($M); unset($M);
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT dcf/emision'){ a_ses::hashKey('wma3.dcf.emision');
		if($js=_js::ise($___D['docEntry'],'Se debe definir el número de documento de cierre','numeric>0')){}
	else if($js=Doc::getStatus(array('docEntry'=>$___D['docEntry'],'tbk'=>'wma3_odcf'),array('D'=>'Y','fie'=>'emiStatus','Fi'=>array(array('emiStatus','C','Los consumos ya se encuentran cerrados, no se pueden modificar.'))) )){}
	else{ $nl=0;
		foreach($___D['Lm'] as $n =>$L){ $nl++; $ln='Linea '.$nl.': ';
			$realQty_err=_js::ise($L['realQty'],$ln.'La cantidad debe ser un número mayor a 0','numeric>=0');
			if($L['realQty']==''){ $___D['Lm'][$n]['_omit']='Y'; continue; }
			else if($js=_js::ise($L['itemId'],$ln.'Se debe definir el artículo','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir el almacen para el consumo','numeric>0')){ $errs++; break; }
			else if($js=_js::textLen($L['detail'],20,$ln.'El detalle no puede exceder 20 caracteres.')){ $errs++; break; }
			else if($js=$realQty_err){ $errs++; break; }
		}
		if($errs==0){ $nl=0; 
			$qD=array();
			foreach($___D['Lm'] as $n =>$L){ $nl++; $ln='Linea '.$nl.': ';
				if($L['_omit']=='Y'){ continue; }
				$L['docEntry']=$___D['docEntry']; $L['lineNum']=$nl;
				$L['diff']=$L['realQty']-$L['reqQty'];
				$L[0]='i';
				$L[1]='wma3_dcf2'; $L['_unik']='id'; 
				//$L['_unikWh']='AND docEntry=\''.$___D['docEntry'].'\'';
				$qD[]=$L;
			}
			a_sql::transaction(); $cmt=false;
			a_sql::multiQuery($qD);
			if(_err::$err){ $js=_err::$errText; }
			else{ $cmt=true; $js=_js::r('Consumos guardados correctamente.'); }
			a_sql::transaction($cmt);
		}
	}
	echo $js;
}
else if(_0s::$router=='POST dcf/emisionClose'){ a_ses::hashKey('wma3.dcf.emision');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número de documento de cierre','numeric>0')){}
	else if($js=Doc::getStatus(array('docEntry'=>$___D['docEntry'],'tbk'=>'wma3_odcf'),array('D'=>'Y','fie'=>'emiStatus','Fi'=>array(array('emiStatus','C','Los consumos ya se encuentran cerrados.'))) )){}
	else{
		$gb='B.itemId,B.itemSzId,B.whsId';
		$qC=a_sql::query('SELECT '.$gb.',SUM(B.realQty) quantity FROM wma3_dcf2 B WHERE B.docEntry=\''.$___D['docEntry'].'\' GROUP BY '.$gb,array(1=>'Error obteniendo materiales del documento de cierre.',2=>'El documento no tiene materiales registrados'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
		else{
			_ADMS::_lb('src/Invt'); $Liv=array();
			while($L=$qC->fetch_assoc()){ $L['quantity']*=1;
				Invt::onHand($L,array('fie'=>'I.itemType','itemType'=>'MP'));
				if(Invt::$err){ $js=Invt::$errText; $errs++; break; }
				$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'outQty'=>$L['quantity']);
			}
		}
		if($errs==0){
			a_sql::transaction(); $cmt=false;
			$qu=a_sql::query('UPDATE wma3_odcf SET emiStatus=\'C\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando cierre del documento: '));
			if(a_sql::$err){ $js=a_sql::$errNoText; }
			else{
				Invt::onHand_put($Liv,array('docDate'=>date('Y-m-d'),'tt'=>'wmaDcf','tr'=>$___D['docEntry']));
				if(Invt::$err){ $js=Invt::$errText; }
				else{ $js=_js::r('Documento cerrado para consumos correctamente.');
					Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'lineMemo'=>'Consumos generados','emiStatus'=>'C'));
					$cmt=true; a_sql::transaction($cmt);
				}
			}
		}
	}
	echo $js;
}

/* me */
else if(_0s::$router=='GET dcf/actualizarde2'){
	$q=a_sql::query('SELECT A.docEntry,B.whsId,B.wfaId, B.whsIdFrom,B.wfaIdBef, B.itemId,B.itemSzId, B.quantity
	FROM wma3_odcf A
	JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry)
	WHERE A.tt=\'wmaDdf\' AND  A.dateC BETWEEN \'2020-12-20 05:00:00\' AND \'2020-12-21 23:00:00\' LIMIT 1',array(1=>'Error obteniendo datos',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$Docs=[];
		$Mx=[];
		while($L=$q->fetch_assoc()){
			$Mx[]=$L;
			$Docs[$L['docEntry']]=['inQty'=>0,'outQty'=>0];
		}
		/* ver que documentos estan diferentes */
		if(0) foreach($Docs as $doc => $Lxx){
			$q=a_sql::query('SELECT SUM(A.inQty) inQty, SUM(A.outQty) outQty
			FROM pep_wtr1 A
			WHERE A.tt=\'wmaDcf\' AND A.tr=\''.$doc.'\' '
			,array(1=>'Error obteniendo datos',2=>'No se encontraron resultados.'));
			if(a_sql::$errNo==-1) while($L=$q->fetch_assoc()){
				//if($L['inQty']==$L['outQty']){ unset($Docs[$doc]); continue; }
				$Docs[$doc]['inQty'] +=$L['inQty'];
				$Docs[$doc]['outQty'] +=$L['outQty'];
			}
		}
		/* con los documentos, actualizar wtr1 */
		$mU=[];
		$Docs=[14004,14005,14007,14008,14010,14011,14012,14013,14016,14019,14020,14021,14022,14023,14024,14025];
		if(0) foreach($Docs as $n=> $doc){
			$q=a_sql::query('SELECT id,A.whsId,A.wfaId,A.itemId,A.itemSzId,A.inQty inQty, A.outQty
			FROM pep_wtr1 A
			WHERE A.tt=\'wmaDcf\' AND A.tr IN ('.implode(',',$Docs).' ',array(1=>'Error obteniendo datos',2=>'No se encontraron resultados.'));
			if(a_sql::$errNo==-1) while($L=$q->fetch_assoc()){
				//if($L['inQty']==$L['outQty']){ unset($Docs[$n]); continue; }
				//$mU[]=['p','update pep_wtr1 SET inQty=0 WHERE id=\''.$L['id'].'\' LIMIT 1'];
				//$mU[]=['p','update pep_wtr1 SET onHandAt=onHandAt-'.$L['outQty'].' WHERE id=\''.$L['id'].'\' LIMIT 1'];
				$mU[]=['p','update pep_oitw SET onHand=onHand-'.$L['outQty'].' WHERE whsId=\''.$L['whsId'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1'];
				$L['docEntry']=$doc;
				$Docs[$n]=$L;
			}
		}
		if(1){
			// se multiplico por 
			$gb='A.docDate,A.docEntry,B.whsId,B.wfaId, B.itemId,B.itemSzId';
			$q=a_sql::query('SELECT '.$gb.', SUM(B.quantity*15) quantity
	FROM wma3_odcf A
	JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry)
	WHERE A.docEntry IN ('.implode(',',$Docs).') GROUP BY '.$gb.' LIMIT 500 ',array(1=>'Error obteniendo datos',2=>'No se encontraron resultados.'));
			if(a_sql::$errNo==-1) while($L=$q->fetch_assoc()){
				$L['tr']=$L['docEntry']; unset($L['docEntry']);
				//$mU[]=['p','s'=>$L['docStatus'],'update pep_oitw SET onHand=onHand-'.($L['quantity']*1).' WHERE whsId=\''.$L['whsId'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1'];
				//$mU[]=['p','update pep_wtr1 SET docDate=\''.$L['docDate'].'\' WHERE tt=\'wmaDcf\' AND tr=\''.$L['tr'].'\' LIMIT 1000'];
				//wtr1
				//tt,tr,docDate,whsId,wfaId,itemId,itemSzId,quantity,inQty,
				$L[0]='i'; $L[1]='pep_wtr1';
				$L['tt']='wmaDcf';
				$L['inQty']=$L['quantity'];
				//$mU[]=$L;
				//$mU[]=$L;;
			}
		}
		echo implode(',',$Docs).'
';
		a_sql::multiQuery($mU);
		echo _err::$errText;
		print_r($mU);
	}
}
else if(_0s::$router=='GET dcf/actualizarde'){
	$q=a_sql::query('SELECT A.docDate,A.docEntry,B.whsId,B.wfaId, B.whsIdFrom,B.wfaIdBef, B.itemId,B.itemSzId, B.quantity
	FROM wma3_odcf A
	JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry)
	WHERE A.tt=\'wmaDdf\' AND  A.dateC BETWEEN \'2020-12-22 01:00:00\' AND \'2020-12-22 23:00:00\' ',array(1=>'Error obteniendo datos',2=>'No se encontraron resultadoss.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$mU=[];
		if(1){
			while($L=$q->fetch_assoc()){
				$L['tr']=$L['docEntry']; unset($L['docEntry']);
				$L['quantity'] *= 1;
				$mU[]=['p','update pep_oitw SET onHand=onHand-'.$L['quantity'].' WHERE whsId=\''.$L['whsIdFrom'].'\' AND wfaId=\''.$L['wfaIdBef'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1'];
				$mU[]=['p','update pep_oitw SET onHand=onHand+'.$L['quantity'].' WHERE whsId=\''.$L['whsId'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1'];
				//$mU[]=['p','update pep_wtr1 SET whsId=\''.$L['whsId'].'\', wfaId=\''.$L['wfaId'].'\' WHERE whsId=\''.$L['whsIdFrom'].'\' AND wfaId=\''.$L['wfaIdBef'].'\' AND tt=\'wmaDcf\' AND tr=\''.$L['tr'].'\' AND inQty>0 AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 100'];
				//$mU[]=['p','update pep_wtr1 SET docDate=\''.$L['docDate'].'\' WHERE  tt=\'wmaDcf\' AND tr=\''.$L['tr'].'\' '];
				//wtr1
				//tt,tr,docDate,whsId,wfaId,itemId,itemSzId,quantity,inQty,
				$L[0]='i'; $L[1]='pep_wtr1';
				$L['tt']='wmaDcf';
				$L['inQty']=$L['quantity'];
				//$mU[]=$L;
				//$mU[]=$L;;
			}
		}
		//a_sql::multiQuery($mU);
		echo _err::$errText;
		print_r($mU);
	}
}

?>