<?php
_ADMS::_lb('com/_fread_tab');
if(_0s::$router=='POST dt/pepAwh'){
	$docEntry=a_sql::nextAI(['tb'=>'ivt_oawh']);
	_err::errDie();
	$js=false;
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('itemCode','itemSzId','quantity'))
);
	//die(print_r($R));
	$D=$_POST;
	$D['docEntry']=$docEntry;
	if($R['errNo']){ $js = _js::e($R); }
	else if(_js::iseErr($D['docEntry'],'Se debe definir el número de documento','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['serieId'],'Se debe definir el Id de la serie','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['whsId'],'Se debe definir la bodega')){ $js=_err::$errText; }
	else{
		$Bod=array();
		$ITM=array();
		//Obtener bodegas
		$w=a_sql::fetch('SELECT whsCode,whsId FROM ivt_owhs WHERE whsId=\''.$D['whsId'].'\' LIMIT 1',array(1=>'Error obteniendo bodega.',2=>'No existe la bodega definida.'));
		$whsId=0;
		if(a_sql::$err){ die(a_sql::$errNoText); }
		$whsId=$w['whsId'];
		$DC=array('docEntry'=>$docEntry,'docDate'=>date('Y-m-d'),
		'omitCard'=>'Y','cardName'=>'Sistema',
		'serieId'=>$D['serieId'],'whsId'=>$whsId,
		'lineMemo'=>'Generado desde Data Transfer.');
		$DC['L']=array();
		/* generar */
		$Di=array(); $qI=array();
		foreach($R['L'] as $ln => $Da){
			$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
			$lineTotal++;
			$Da['docEntry']=$docEntry;
			if($js=_js::ise($Da['docEntry'],$lnt.'Se debe definir el número de documento','numeric>0')){ die($js); }
			else if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
			else if($js=_js::ise($Da['itemSzId'],$lnt.'Se debe definir el ID del Subproducto.')){ die($js); }
			else if($js=_err::iff($Da['quantity']<0,$lnt.'La cantidad a definir no puede ser negativa.')){ die($js); }
			else{
				//verificar articulo
				$quk=$Da['itemCode'];
				$QK1=_uniK::fromQuery(['k'=>$Da['itemCode'],'f'=>'itemId','from'=>'itm_oitm','wh'=>'itemCode=\''.$Da['itemCode'].'\'',
				1=>$lnt.'Error obteniendo Id de articulo.',2=>$lnt.'El código del articulo ('.$Da['itemCode'].') no existe']);
				if(_err::$err){ die(_err::$errText); }
				//verificar s/p
				$QK2=_uniK::fromQuery(['k'=>'sp_'.$Da['itemSzId'],'f'=>'itemSzId','from'=>'itm_grs1','wh'=>'itemSzId=\''.$Da['itemSzId'].'\'',
				1=>$lnt.'Error obteniendo Id de articulo.',2=>$lnt.'El subproducto ('.$Da['itemSzId'].') no existe']);
				if(_err::$err){ die(_err::$errText); }
				$DC['L'][]=array(
				'handInv'=>'Y','itemId'=>$QK1['itemId'],'itemSzId'=>$QK2['itemSzId'],'quantity'=>$Da['quantity']);
			}
		}//for
	}
	if($js==false){
		_ADMS::lib('iDoc,docSeries');
		_ADMS::mApps('ivt/Awh');
		$js=ivtAwh::post($DC);
		if(_err::$err){ $js=_err::$errText; }
		echo $js;
	}
	else{ echo $js; }
}
?>