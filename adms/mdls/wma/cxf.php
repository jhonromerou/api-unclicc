<?php
$serieType='wmaCxf';
Doc::$owh=false; unset($___D['___ocardtooken']);
if(_0s::$router=='POST cxf/of'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha.')){}
	else if($js=_js::ise($___D['tr'],'Se debe definir el ID de la orden de fabricación')){}
	else if($js=_js::ise($___D['wfaId'],'Se debe definir el ID de la fase')){}
	else if(!_js::textLimit($___D['lineMemo'],200)){ $js=_js::e(3,'Los detalles no pueden exceder 200 caracteres.'); }
	else{
		$___D['tt']='wmaOdp';
		$Q=_qu::q(array('q'=>'SELECT A.canceled FROM '._0s::$Tb['wma3_ocxf'].' A WHERE tt=\''.$___D['tt'].'\' AND tr=\''.$___D['tr'].'\' AND A.wfaId=\''.$___D['wfaId'].'\' AND A.canceled=\'N\' LIMIT 1'));
		if(!$Q){ die(_qu::$errText); }
		else if($Q['canceled']=='N'){ die(_js::e(3,'Ya existe un documento de consumo para la fase en esta orden de fabricación.')); }
		//$ins=a_sql::insert($___D,array('tbk'=>'wma3_ocxf','qDo'=>'insert','kui'=>'uid_dateC'));
		$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
		$q=a_sql::query('SELECT A.canceled,B.itemId,B.itemSzId,B.quantity reqQty FROM '._0s::$Tb['wma3_oodp'].' A JOIN '._0s::$Tb['wma3_odp1'].' B ON (B.docEntry=A.docEntry) WHERE A.docEntry=\''.$___D['tr'].'\' ',array(1=>'Error obteniendo cantidades de la orden de fabricación.',2=>'La orden de fabricación no tiene cantidades definidas.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			_ADMS::_lb('sql/filter');
			_ADMS::_app('mrp'); $errs=0;
			$Pw=array('fie'=>'I.itemId,I.itemName','sum'=>'SUM(T1.lineTotal) baseCost');
			$Pw['wh']['T1.wfaId(E_igual)']=$___D['wfaId'];
			while($L=$q->fetch_assoc()){
				if($L['canceled']=='Y'){ $js=_js::e(3,'La orden de fabricación está anulada.'); break; }
				$MR=mrp::mpRequire($L,$Pw);
				if($MR){ $js= $MR; $errs++; break; }
			}
			if($errs==0){/* lineas */
				a_sql::transaction(); 
				$ins=a_sql::insert($___D,array('tbk'=>'wma3_ocxf','qDo'=>'insert','kui'=>'uid_dateC'));
				if($ins['err']){ $js=_js::e(3,'Error generando documento: '.$ins['text']); }
				else{
					$docEntry=$ins['insertId'];
					foreach(mrp::$MR as $nx => $L2){
						$Di=array('docEntry'=>$docEntry,'itemId'=>$L2['itemId'],'baseQty'=>$L2['reqQty'],'baseCost'=>$L2['baseCost']);
						$ins2=a_sql::insert($Di,array('tbk'=>'wma3_cxf1','qDo'=>'insert'));
						if($ins2['err']){ $js=_js::e(3,'Error guardando lineas de documento: '.$ins2['text']); $errs++; break; }
					}
				}
			}
			if($errs==0){ $cmt=false;
				$js=_js::r('Documento #'.$docEntry.' generado correctamente.',$___D);
				a_sql::transaction(true); 
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='GET cxf'){ //a_ses::hashKey('ivt.wht.basic');
	$___D['fromA']='A.docEntry,A.tr,A.docDate,WF.wfaCode,WF.wfaName,A.lineMemo FROM '._0s::$Tb['wma3_ocxf'].' A LEFT JOIN '._0s::$Tb['wma_owfa'].' WF ON (WF.wfaId=A.wfaId)';
	echo Doc::get($___D);
	echo $js;
}
else if(_0s::$router=='GET cxf/one'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el ID del documento','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.docEntry,A.tr,A.docDate,WF.wfaCode,WF.wfaName,A.lineMemo FROM '._0s::$Tb['wma3_ocxf'].' A LEFT JOIN '._0s::$Tb['wma_owfa'].' WF ON (WF.wfaId=A.wfaId) WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de documento de consumo.',2=>'El documento de consumo '.$___D['docEntry'].' no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			$M['L']=array();
			$M['L2']=array();
			$q=a_sql::query('SELECT I.itemCode,I.itemName,I.udm,B.baseQty,B.baseCost,B.qtyCons,B.qtyCost,B.diff FROM '._0s::$Tb['wma3_cxf1'].' B LEFT JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId) WHERE B.docEntry=\''.$M['docEntry'].'\' ',array(1=>'Error obteniendo información de lineas de documento de consumo.',2=>'El documento de consumo '.$___D['docEntry'].' no tiene lineas registradas.'));
			if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
			else{
				while($L=$q->fetch_assoc()){
					$M['L'][]=$L;
				}
				$q=a_sql::query('SELECT I.itemCode,I.itemName,I.udm,B2.quantity,B2.cost,B2.lineTotal FROM '._0s::$Tb['wma3_cxf2'].' B2 LEFT JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B2.itemId) WHERE B2.docEntry=\''.$M['docEntry'].'\' ',array(1=>'Error obteniendo registros de consumo',2=>'No hay registros de consumo.'));
				if(a_sql::$err){ $M['L2']=json_decode(a_sql::$errNoText,1); }
				else{ 
					while($L=$q->fetch_assoc()){
						$M['L2'][]=$L;
					}
				}
			}
			$js=_js::enc2($M);
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT cxf/close'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el ID del documento','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.docEntry,A.tr,A.docStatus,A.canceled FROM '._0s::$Tb['wma3_ocxf'].' A WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de documento de consumo.',2=>'El documento de consumo '.$___D['docEntry'].' no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else if($M['canceled']=='Y'){ $js=_js::e(3,'El documento está anulado, no se puede cerrar.'); }
		else if($M['docStatus']=='C'){ $js=_js::e(3,'El documento ya ha sido cerrado.'); }
		else{
			$q=a_sql::query('UPDATE '._0s::$Tb['wma3_ocxf'].' SET docStatus=\'C\',dateClose=\''.date('Y-m-d').'\', userClose=\''.a_ses::$userId.'\',dateUpd=\''.date('Y-m-d H:i:s').'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error cerrando documento de consumo.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; }
			else{
				$js=_js::r('Documento cerrado correctamente.');
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT cxf/canceled'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el ID del documento','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.docEntry,A.tr,A.docStatus,A.canceled FROM '._0s::$Tb['wma3_ocxf'].' A WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de documento de consumo.',2=>'El documento de consumo '.$___D['docEntry'].' no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else if($M['docStatus']=='C'){ $js=_js::e(3,'El documento está cerrado, no se puede anular.'); }
		else if($M['canceled']=='Y'){ $js=_js::e(3,'El documento ya ha sido anulado.'); }
		else{
			$q=a_sql::query('UPDATE '._0s::$Tb['wma3_ocxf'].' SET docStatus=\'N\',canceled=\'Y\',dateClose=\''.date('Y-m-d').'\', userClose=\''.a_ses::$userId.'\',dateUpd=\''.date('Y-m-d H:i:s').'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error cerrando anulado documento de consumo.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; }
			else{
				$js=_js::r('Documento anulado correctamente.');
			}
		}
	}
	echo $js;
}


else if(_0s::$router=='POST cxf/cons'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el Id del documento.','numeric>0')){}
	else if($js=_js::ise($___D['itemId'],'Se debe definir el ID del artículo para consumo','numeric>0')){}
	else if($js=_js::ise($___D['whsId'],'Se debe definir la bodega.','numeric>0')){}
	else if($js=_js::ise($___D['quantity'],'La cantidad debe ser un número mayor a 0.','numeric>0')){}
	else{
		$Q=_qu::q(array('q'=>'SELECT A.docStatus,I.itemId,I.buyPrice,I.handInv,I.itemType 
		FROM '._0s::$Tb['wma3_ocxf'].' A 
		LEFT JOIN '._0s::$Tb['wma3_cxf1'].' B ON (B.docEntry=A.docEntry AND B.itemId=\''.$___D['itemId'].'\') 
		LEFT JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=\''.$___D['itemId'].'\')
		WHERE A.docEntry=\''.$___D['docEntry'].'\' AND I.itemId=\''.$___D['itemId'].'\' LIMIT 1','err2'=>'El documento o el artículo no existe.'));
		if(!$Q){ die(_qu::$errText); }
		else if($Q['docStatus']=='C'){ die(_js::e(3,'El documento de consumo está cerrado, no se pueden registrar consumos.')); }
		else if($Q['docStatus']=='N'){ die(_js::e(3,'El documento de consumo está anulado, no se pueden registrar consumos.')); }
		else if($Q['itemType']!='MP'){ die(_js::e(3,'Solo se puede registrar consumos de materia prima.')); }
		$Di=array('docEntry'=>$___D['docEntry'],'whsId'=>$___D['whsId'],'itemId'=>$___D['itemId'],'quantity'=>$___D['quantity'],'cost'=>$Q['buyPrice'],'lineTotal'=>$___D['quantity']*$Q['buyPrice']);
		a_sql::transaction();
		$Liv[]=array('itemId'=>$___D['itemId'],'itemSzId'=>0,'whsId'=>$___D['whsId'],'outQty'=>$___D['quantity']);
		_ADMS::_lb('src/Invt');
		Invt::onHand_put($Liv,array('tt'=>$serieType,'tr'=>$___D['docEntry'],'docDate'=>date('Y-m-d')));
		if(Invt::$err){ die(Invt::$errText); }
		$ins=a_sql::insert($Di,array('tbk'=>'wma3_cxf2','qDo'=>'insert','kui'=>'uid_dateC'));
		if($ins['err']){ $js=_js::e(3,'Error guardando consumo: '.$ins['text']); }
		else{
			$Di2=array('docEntry'=>$___D['docEntry'],'itemId'=>$___D['itemId']);
			$qtyCost=$Di['lineTotal'];
			$Di2['._.qtyCost']='+-+qtyCost+'.$qtyCost;
			$Di2['qtyCost']=$qtyCost;
			$Di2['._.qtyCons']='+-+qtyCons+'.$Di['quantity'];
			$Di2['qtyCons']=$Di['quantity'];
			$Di2['._.diff']='+-+qtyCons-baseQty';
			$Di2['diff']=$Di['quantity'];
			$ins2=a_sql::insert($Di2,array('tbk'=>'wma3_cxf1','wh_change'=>'WHERE docEntry=\''.$___D['docEntry'].'\' AND itemId=\''.$___D['itemId'].'\' LIMIT 1'));
			if($ins2['err']){ $js=_js::e(3,'Error actualizando consumo total: '.$ins2['text']); }
			else{
				$js=_js::r('Consumo registrado correctamente.');
				a_sql::transaction(true);
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='DELETE cxf/cons'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['id'],'Se debe definir el ID del consumo.','numeric>0')){}
	else{
		$Q=_qu::q(array('q'=>'SELECT A.docEntry,B.itemId,A.docStatus,B2.whsId,B2.quantity,B2.lineTotal 
		FROM '._0s::$Tb['wma3_cxf2'].' B2 
		JOIN '._0s::$Tb['wma3_cxf1'].' B ON (B.docEntry=B2.docEntry AND B.itemId=B2.itemId)
		JOIN '._0s::$Tb['wma3_ocxf'].' A ON (A.docEntry=B.docEntry) 
		WHERE B2.id=\''.$___D['id'].'\' LIMIT 1','err2'=>'El consumo '.$___D['id'].', no existe.'));
		if(!$Q){ die(_qu::$errText); }
		else if($Q['docStatus']=='C'){ die(_js::e(3,'El documento de consumo '.$Q['docEntry'].' está cerrado, no se puede eliminar el consumo.')); }
		else if($Q['docStatus']=='N'){ die(_js::e(3,'El documento de consumo está anulado, no se puede anular el consumo.')); }
		a_sql::transaction();
		$Liv[]=array('itemId'=>$Q['itemId'],'itemSzId'=>0,'whsId'=>$Q['whsId'],'inQty'=>$Q['quantity']);
		_ADMS::_lb('src/Invt');
		Invt::onHand_put($Liv,array('tt'=>$serieType,'tr'=>$Q['docEntry'],'docDate'=>date('Y-m-d')));
		if(Invt::$err){ die(Invt::$errText); }
		$upd=a_sql::query('UPDATE '._0s::$Tb['wma3_cxf1'].' SET qtyCons=qtyCons-'.$Q['quantity'].', qtyCost=qTyCost-'.$Q['lineTotal'].', diff=qtyCons-baseQty WHERE docEntry=\''.$Q['docEntry'].'\' AND itemId=\''.$Q['itemId'].'\' LIMIT 1',array(1=>'Error actualizando consumo y costo de la linea.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			$del=a_sql::query('DELETE FROM '._0s::$Tb['wma3_cxf2'].' WHERE id=\''.$___D['id'].'\' LIMIT 1',array(1=>'Error eliminando linea del consumo.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Linea eliminada correctamente.'); a_sql::transaction(true); }
		}
	}
	echo $js;
}
?>