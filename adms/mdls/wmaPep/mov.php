<?php
$serieType='pepMov';
unset($_J['serieId']);
if(_0s::$router=='GET mov'){ a_ses::hashKey('pepMov');
	$___D['fromA']='A.docEntry,A.docDate,A.docStatus,A.lineMemo,A.userId,A.dateC FROM pep_omov A';
	echo Doc::get($___D);
}
else if(_0s::$router=='POST mov'){ a_ses::hashKey('pepMov');
	if($js=_js::ise($_J['docDate'],'Se debe definir la fecha.')){ die($js); }
	else if($js=_js::ise($_J['cardId'],'Se debe definir el socio de negocios.')){ die($js); }
	else if($js=_js::ise($_J['cardName'],'Se debe definir el socio de negocios.')){ die($js); }
	else if(!_js::textLimit($_J['lineMemo'],100)){ $js=_js::e(3,'Los detalles no pueden exceder 100 caracteres.'); }
	else if(!is_array($_J['L']) || count($_J['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$_J['L']; unset($_J['L'],$_J['cardName']);
		$errs=0; $Liv=array();
		_ADMS::_app('wma3.PeP'); $nn=1;
		foreach($Ld as $n => $L){/* verificar linea e inventario */
			$ln='Linea '.$nn.': ';
			if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.','')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.','',$jsA)){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega.','')){ $errs++; break; }
			else if($js=_js::ise($L['wfaId'],$ln.'Se debe definir la fase.','')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad a ingresar','numeric>0')){ $errs++; break; }
			else{
				if($L['lineType']=='O'){
					PeP::onHand($L,array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'lnt'=>$ln));
					if(PeP::$err){ $js=PeP::$errText; $errs++; break; }
					$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'outQty'=>'Y');
				}
				else{
					$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'inQty'=>'Y');
				}
				$Ld[$n]['lineNum']=$nn;
			}
			$nn++;
		}
		if($errs==0){ /* generar lineas */
			a_sql::transaction(); $cmt=false;
			$ins=a_sql::insert($_J,array('table'=>'pep_omov','qDo'=>'insert','kui'=>'ud'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{
			$docEntry=$ins['insertId'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				unset($L['handInv'],$L['priceList']);
				$ins2=a_sql::insert($L,array('table'=>'pep_mov1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
			}
			}
		}
		if($errs==0){/* actualizar inventario */
			PeP::onHand_put($Liv,array('docDate'=>$_J['docDate'],'tt'=>$serieType,'tr'=>$docEntry));
			if(PeP::$err){ $errs++; $js=PeP::$errText; }
		}
		if($errs==0){ $cmt=true; $js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); }
		a_sql::transaction($cmt);
		
	}
	echo $js;
}
else if(_0s::$router=='GET mov/view'){ a_ses::hashKey('pepMov');
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.docEntry,A.docDate,A.docStatus,A.lineMemo,A.userId,A.dateC FROM pep_omov A','fromB'=>'B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,B.lineType,B.whsId,B.wfaId,B.quantity FROM pep_mov1 B LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
?>