<?php
/* Crear tablas base del sistema */
c::$Sql2=array(
 'sql_h'=>'sql1.admsistems.com',
 'sql_u'=>'root',
 'sql_p'=>'Pepa1992--',
 'sql_db'=>'adms'
);
header('content-type:text/html');
a_sql::dbase(c::$Sql2);
$DTy=[
'Doc'=>['c'=>'green','t'=>'Docs o Transaciones'],
'Log'=>['c'=>'aquamarine','t'=>'Logs'],
'Def'=>['c'=>'purple','t'=>'Catalogos'],
'Cnf'=>['c'=>'purple','t'=>'Catalogos'],
'Sys'=>['c'=>'red','t'=>'Parametros de Sistema'],
'Data'=>['c'=>'blue','t'=>'Datos como comentarios'],
];
$Dtxt='';
foreach($DTy as $k=>$L){
	$Dtxt .= '<b style="background-color:'.$L['c'].'">'.$k.'</b>: '.$L['t'].', ';
}
echo '<p><form method="POST">
<div><b>Redefinir</b> <select name="a"><option>No</option><option value="redefinir">Si</option></select></div>
<div><b>Get Truncate?</b> <select name="gTruncate" selected="'.$_POST['gTruncate'].'"><option>No</option><option value="Y">Si</option></select></div>
<div><b>DB</b> <input type="text" name="db" value="'.$_POST['db'].'" /></div>
<div>
	<b>mdl</b> <input type="text" name="mdl(E_in)" value="'.$_POST['mdl(E_in)'].'" /> / 
	<b>subMdl</b> <input type="text" name="subMdl(E_in)" value="'.$_POST['subMdl(E_in)'].'" />
</div>
<div><b>dataType</b> <input type="text" name="dataType(E_in)" value="'.$_POST['dataType(E_in)'].'" />'.$Dtxt.'</div>
<div><b>resetType</b> <input type="text" name="resetType(E_in)" value="'.$_POST['resetType(E_in)'].'" /> (all,normal)</div>
<input type="submit" value="actualizar" />
</form></p>';
if($_POST['a']=='redefinir'){
	$q = a_sql::query('SELECT S.table_name tbName 
	FROM information_schema.tables S
 LEFT JOIN adms.tb_mdls TM ON (TM.tbName=S.table_name) 
	WHERE S.table_schema=\'pr_adms\' AND TM.tbName is NULL',array(1=>'Error obteniendo listado de tablas: ',2=>'No se encontraron tablas'));
	print_r($q);
 while($L=$q->fetch_assoc()){
  $r=a_sql::uniRow(['tbName'=>$L['tbName']],['tbk'=>'tb_mdls','wh_change'=>'tbName=\''.$L['tbName'].'\' LIMIT 1']);
		if(a_sql::$err){ echo a_sql::$errNoText; break; }
 }
}
else{ /* buscar para crear truncate */
echo '<style>
table{ border-collapse:collapse; }
td{  border:1px solid #000; }
</style>
<table>
<tr>
<td>Mdl</td> <td>subMdl</td> <td>Tabla</td> <td>tbType</td><td>dataType</td> <td>resetType</td> <td>tbSname</td>
</tr>';
_ADMS::lib('sql/filter');
$db=$_POST['db'];
$gTruncate=($_POST['gTruncate']=='Y');
unset($_POST['db'],$_POST['gTruncate'],$_POST['a']);
$q=a_sql::query('SELECT tbStatus,mdl,subMdl,tbName,tbType,dataType,resetType,tbSname FROM adms.tb_mdls WHERE 1 '.a_sql_filtByT($_POST).' ORDER BY mdl ASC,subMdl ASC, tbType ASC,tbName ASC');
$truncate='';
$lastMdl='';
while($L=$q->fetch_assoc()){
	
	$css=($lastMdl!=$L['mdl'])?' style="background-color:#ffff99"':'';
	$css1=$css;
	if($L['tbStatus']=='I'){ $css1=' style="background-color:red"'; }
	else{
		if($gTruncate) $truncate .='TRUNCATE TABLE `'.$db.'`.`'.$L['tbName'].'`; # '.$L['tbSname'].''."\n";
	}
	$cssType=$DTy[$L['dataType']]['c'];
	echo '<tr>
	<td'.$css1.'>'.$L['mdl'].'</td>
	<td'.$css1.'>'.$L['subMdl'].'</td>
	<td'.$css1.'>'.$L['tbName'].'</td>
	<td'.$css.'>'.$L['tbType'].'</td>
	<td style="background-color:'.$cssType.'">'.$L['dataType'].'</td>
	<td'.$css.'>'.$L['resetType'].'</td>
	<td'.$css.'>'.$L['tbSname'].'</td>
	</tr>'; $lastMdl=$L['mdl'];
}
echo '</table>
<textarea style="width:100%; height:500px;">'.$truncate.'</textarea>
';
}
?>
