<?php
$_mdl='organizapp';
$_accK='agendaDia';
_ADMS::lib('a_mdl');
a_sql::dbase(c::$Sql2);
/* Obtener datos de base de cliente y guardar
- No define si no hay tareas (cambiar a LEFT JOIN
*/
$readJoin ='JOIN';
if($_GET['step']==1){
	$dateH=date('Y-m-d');
	$doDate=date('Y-m-d');
	$whCron='mdl=\''.$_mdl.'\' AND accK=\''.$_accK.'\'';
	$q0=a_sql::query('SELECT ocardcode FROM mdl_crons 
	WHERE actived=\'Y\' AND dateUpd <\''.$dateH.'\' AND '.$whCron.' LIMIT 2',array(1=>'Error obteniendos clientes con modulo activado.',2=>'Ningun cliente con módulo '.$_mdl.'/'.$_accK.' tiene tareas por ejecutar.'));
	if(a_sql::$err){ $rText=(a_sql::$errNoText); }
	else{
		$SV=array(); a_sql::$DBn=1; 
		while($L0=$q0->fetch_assoc()){/* Leer datos desde tabla de cliente */
			$ocardCode=$L0['ocardcode'];
			$qCa=a_sql::fetch('SELECT ocardName,sqlh sql_h,sqlu sql_u,sqlp sql_p,sqldb sql_db FROM ocard WHERE ocardcode=\''.$ocardCode.'\' LIMIT 1');
			a_sql::$DBn=1;
			a_sql::dbase($qCa);
			$q=a_sql::query('SELECT U.userId,U.userName,U.userEmail,N.gid,N.title,N.doDate,N.doDateAt,N.endDate, N.endDateAt,IF(C.cardName IS NULL,\'\',C.cardName) cardName,IF(P.name IS NULL,\'\',P.name) prsName,IF(Ua.userName IS NULL,\'Sin Definir\',Ua.userName) userAssgName
			FROM a0_vs0_ousr U 
			JOIN crm_onov N ON ((N.userId=U.userId OR N.userAssg=U.userId) AND N.onCal=\'Y\' AND N.doDate=\''.$doDate.'\')
			LEFT JOIN a0_vs0_ousr Ua  ON (Ua.userId=N.userAssg)
			LEFT JOIN par_ocrd C ON (C.cardId=N.cardId)
			LEFT JOIN par_ocpr P ON (P.prsId=N.prsId)
			WHERE U.inactivo=\'N\' ORDER BY N.doDate ASC, N.doDateAt ASC
			',array(1=>'Error obteniendo tareas del dia'));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			if(a_sql::$errNo==-1) while($L=$q->fetch_assoc()){
				$k=$L['userId'].'_'.$ocardCode;
				if(!array_key_exists($k,$SV)){ 
					$SV[$k]=array('userId'=>$L['userId'],'userName'=>$L['userName'],'userEmail'=>$L['userEmail'],'ntyKey'=>'daily','doDate'=>$doDate,
					'jsBody'=>array('ocardName'=>$qCa['ocardName'],
					'L'=>array())
					);
				}
				if($L['doDateAt']){
					$hDate=($L['doDateAt']==$L['endDateAt'])
						?substr($L['doDateAt'],0,5)
						:substr($L['doDateAt'],0,5).' - '.substr($L['endDateAt'],0,5);
					$SV[$k]['jsBody']['L'][]=array('hDate'=>$hDate,'title'=>$L['title'],'cardName'=>$L['cardName'],'prsName'=>$L['prsName'],'userAssgName'=>$L['userAssgName']);
				}
			}
			a_sql::$DBn=false;
			if($_GET['update']!='N'){
				$rs=a_sql::query('UPDATE mdl_crons SET dateUpd=\''.$dateH.'\' WHERE ocardCode=\''.$ocardCode.'\' AND '.$whCron.' LIMIT 1');
			}
		}
		if(count($SV)==0){ $rText= _js::e(3,'No se programaron envio  de mensajes'); }
		else{
			a_sql::$DBn=false;
			$nr=0;
			foreach($SV as $k=>$L){
				$L['jsBody']=json_encode($L['jsBody']);
				$qi=a_sql::insert($L,array('table'=>'z_nty_crm','qDo'=>'insert'));
				$nr++;
			}
			$rText= _js::r('Se programaron '.$nr.' envios.');
		}
		
	}
	a_mdl::clogs(array('tt'=>'crmAgendaDia','lineMemo'=>$rText));
	echo $rText;
}
else if($_GET['step']==2){
	$sends=0;
	$q=a_sql::query('SELECT kid,userId,userName,userEmail,doDate,jsBody FROM z_nty_crm WHERE emStatus=\'P\' LIMIT 10',array(1=>'Error obteniendo envios de tabla z_nty_crm',2=>'No hay mensajes por enviar'));
	if(a_sql::$err){ echo a_sql::$errNoText; }
	else{
		_ADMS::_lb('sql/filter');
		_ADMS::lib('_2d,Mailing,_Mailer');
		Mailing::$tbTemp='z_ema_otmp';
		$Ds=array(); $kids='';
		while($L=$q->fetch_assoc()){
			$doDate = _2d::f($L['doDate'],'d F');
			$Dte=array('userName'=>$L['userName'],'doDateText'=>$doDate);
			$x=json_decode($L['jsBody'],1);
			$bder=' style="border:1px solid #000; padding:2px;" ';
			$Dte['ocardName']=($x['ocardName'])?$x['ocardName']:'<<Tu Empresa>>';
			if(is_array($x['L']) && count($x['L'])>0){
				$Dte['trsHTML']='<tr><td class="date"'.$bder.'>Hora</td> <td'.$bder.'>Asunto</td> <td'.$bder.'>Responsable</td> <td'.$bder.'>Socio Negocios</td> <td'.$bder.'>Persona Contacto</td></tr>';
				foreach($x['L'] as $n=>$L2){
					$hDate=$L2['hDate'];
					$Dte['trsHTML'] .='<tr><td class="date"'.$bder.'>'.$hDate.'</td> <td class="title"'.$bder.'>'.$L2['title'].'</td>
					<td'.$bder.'>'.$L2['userAssgName'].'</td>
					<td'.$bder.'>'.$L2['cardName'].'</td> <td'.$bder.'>'.$L2['prsName'].'</td></tr>';
				}
			}
			else{ $Dte['trsHTML'] .='<tr><td>No hay ningun evento programado para hoy.</td></tr>'; }
			$Ds['body']=Mailing::fromTemp(array('tCode'=>'crmCalendDay'),$Dte,array('encodeHTML'=>'N'));;
			if(_err::$err){ echo _err::$errText; }
			else{
				$Ds['asunt'] = 'Agenda del Dia '.$doDate;
				$Ds['mailTo']=$L['userEmail'];
				$Ds['fromName'] = 'Agenda Diaria';
				$s = _Mail::send($Ds,array());
				if($s['errNo']){ echo 'No se envió el correo a los destinatarios ('.$L['email'].'). '.$s['text']; }
				else{ $sends++; $kids .= $L['kid'].','; }
			}
		}
		if($_GET['update']!='N'){
		a_sql::query('UPDATE z_nty_crm SET emStatus=\'S\' WHERE kid IN ('.substr($kids,0,-1).') ');
		}
		if($sends>0){
		a_mdl::ctrl(array('ocardId'=>0,'variName'=>'crmDailyAgenda','qty'=>$sends));
	}
	}
}
?>