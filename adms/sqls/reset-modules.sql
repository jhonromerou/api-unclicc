use pr_adms;
/* **SISTEMA** */
# TRUNCATE table a0_mcnf; # Configuración de Modulos
# TRUNCATE table a0_mecrd; # Datos de Empresa
# TRUNCATE table a0_ojsv; # variablesde sistema    - NO USAR
# TRUNCATE table a0_oust; #

# TRUNCATE table a0_par_ocrd; # datos de Sociedad
# TRUNCATE table a0_par_crd1; #
# TRUNCATE table a0_par_crd2; # Configuración especial (v>3)
# TRUNCATE table a0_par_ousm; # Obsoleto

# TRUNCATE table a0_vs0_ousa; # Permisos de acceso
# TRUNCATE table a0_vs0_ousp; # permisos de usuarios sobre cards, slps
# TRUNCATE table a0_vs0_ousr; # Usuarios
# TRUNCATE table a0_oust; #Equipo de usuarios
# TRUNCATE table a0_ust1; #miembros de equipos
# TRUNCATE table a0_vs3_oqry; # Consultas
# TRUNCATE table a0_vs3_qry1 ; # Consultas permitidas usuario

/* APPS -- */
#TRUNCATE table app_com1; #Comentarios
#TRUNCATE table app_fil1; #comentarios (v>3)
#TRUNCATE table app_onfy;
#TRUNCATE table app_ottc;

/* Mdl gestion de auditoria */
# TRUNCATE table gda_oaud;
# TRUNCATE table gda_aud1;
# TRUNCATE table gda_aud2;
# TRUNCATE table gda_ocnc;
# TRUNCATE table gda_cnc1;
# TRUNCATE table gda_odba;
# TRUNCATE table gda_dba1;
# TRUNCATE table gda_dba1;
# TRUNCATE table gda_ojsv; #variables de modulos
/*  Doc Series */
#TRUNCATE table doc_oser;

/* **CONTABILIDAD** */
# TRUNCATE table gfi_dac1; # Asientos Contables
# TRUNCATE table gfi_odcc; # Comprobante Contable - otros
# TRUNCATE table gfi_dcc1; #
# TRUNCATE table gfi_imp2; # Impuestos de Documentos
# TRUNCATE table gfi_oacc; # borrar
# TRUNCATE table gfi_opdc; # plan de cuentas
# TRUNCATE table gfi_oban; # Bancos
# TRUNCATE table gfi_ocdc; # centro costos
# TRUNCATE table gfi_ofdp; # formas de pago
# TRUNCATE table gfi_ojsv; # variables
# TRUNCATE table gfi_opym; # cond de pago
# TRUNCATE table gfi_otax; # Impuestos y rte
# TRUNCATE table gfi_otba; # Transferencia Bancaria



/* SOCIOS DE NEGOCIOS */
# TRUNCATE table par_ojsv; # variables
# TRUNCATE table par_ocrd; # socios de negocios
# TRUNCATE table par_oslp; # responsables de ventas
# TRUNCATE table par_ocpr; # personas de contacto
# TRUNCATE table par_cpr1; # persona -empresa cargo
# TRUNCATE table par_oprp; # Propiedades Socios
# TRUNCATE table par_prp1; # Propiedades Socios assg
# TRUNCATE table par_otwa; # tooekn para acceso externo
# TRUNCATE table cnt_addr; #  Direcciones
   # NO USaDOS-----------
# TRUNCATE table par_slp1; # Permiso usuario sobre vendedor 1 a 1
# TRUNCATE table par_aut1; # desuso - relacion usuario-ocardid
# TRUNCATE table par_owac; # desuso - relacion usuario-ocardid
# TRUNCATE table cnt_email; #
# TRUNCATE table cnt_phone; #

/* Artículos -------------------------- */
# TRUNCATE table itm_ojsv; # variables
# TRUNCATE table itm_oitm;
# TRUNCATE table itm_bar1; #Codigos de barras
# TRUNCATE table itm_ogrs; # grupo de tallas
# TRUNCATE table itm_grs1;
# TRUNCATE table itm_grs2;
# TRUNCATE table itm_oitp; # Propiedades para articulos
# TRUNCATE table itm_itp1; # Definición de propiedades en articulo
# TRUNCATE table itm_oiac; # Cuentas contables para grupo (accGrId)
# TRUNCATE table itm_src1; # imagenes extension
# TRUNCATE table itm_oipc; # Costos por articulo-talla
# TRUNCATE table itm_itt1; # Composicion de produccion - lista materiales
  # NO USaDOS-----------
# TRUNCATE table itm_oitg; #  grupos ya en ojsv


/* INVENTARIOS -------------------------- */
# TRUNCATE table itm_owhs; # bodegas
TRUNCATE table ivt_oitw; # inventario por bodega
TRUNCATE table ivt_wtr1; # historico transferencia
TRUNCATE table ivt_oing; # ingresos
TRUNCATE table ivt_ing1;
TRUNCATE table ivt_oegr; # salidas
TRUNCATE table ivt_egr1;
TRUNCATE table ivt_egr2;
TRUNCATE table ivt_owht; # Transferencia
TRUNCATE table ivt_wht1;
TRUNCATE table ivt_oawh; # Ajustes Inventario
TRUNCATE table ivt_awh1;
TRUNCATE table ivt_ocat; # toma inventario
TRUNCATE table ivt_cat1;
 # REVISAR PARA CONTINUAR USSO
TRUNCATE table ivt_ocph; # Cambio por homologacion
TRUNCATE table ivt_cph1;
TRUNCATE table ivt_oinc; # Conteo de Inventario
TRUNCATE table ivt_inc1;
TRUNCATE table ivt_omov; # Movimientos de inventario
TRUNCATE table ivt_mov1;



/* **GVT ** */
TRUNCATE table gvt_dinv; # Documentos Electronicos
TRUNCATE table gvt_doc99; # Logs de Documentos
TRUNCATE table gvt_opor; # Orden de Compra
TRUNCATE table gvt_por1;
TRUNCATE table gvt_opdn; # remsion por Compra
TRUNCATE table gvt_pdn1;
TRUNCATE table gvt_opin; # Factura compra
TRUNCATE table gvt_pin1;
TRUNCATE table gvt_pin2; # F.V Impuestos
TRUNCATE table gvt_opnd; # Nota debito compra
TRUNCATE table gvt_pnd1;
TRUNCATE table gvt_pnd2;
TRUNCATE table gvt_opnc; # Nota credito compra
TRUNCATE table gvt_pnc1;
TRUNCATE table gvt_pnc2;
TRUNCATE table gvt_orce; # Recibo de Egreso
TRUNCATE table gvt_rce1;

TRUNCATE table gvt_osop; # Cotizacion de Venta
TRUNCATE table gvt_sop1;
TRUNCATE table gvt_osdn; # remision ventas
TRUNCATE table gvt_snd1;
TRUNCATE table gvt_osor; # Orden de venta
TRUNCATE table gvt_soar1;
TRUNCATE table gvt_osrd; # devoluciones de venta
TRUNCATE table gvt_srd1;
TRUNCATE table gvt_oinv; # Factura de Venta
TRUNCATE table gvt_inv1;
TRUNCATE table gvt_inv2; # F.V Impuestos
TRUNCATE table gvt_osnc; # Nota credito
TRUNCATE table gvt_snc1;
TRUNCATE table gvt_snc2; # Impuestos
TRUNCATE table gvt_osnd; # Nota credito
TRUNCATE table gvt_snd1;
TRUNCATE table gvt_snd2; # Impuestos
TRUNCATE table gvt_orcv; # Recibo de Caja
TRUNCATE table gvt_rcv1;


/* Produccion  --------------------------- */
    /* COSTOS DE PRODUCCIÓN */
# TRUNCATE table wma_oipc; # Costo definido por articulo.
# TRUNCATE table wma_ilc1; #no usado
 TRUNCATE table itm_log2; # Registro log de modificaciones autorizados o no.
    /* LISTA DE MATERIALES */
# TRUNCATE table itm_oitt;
# TRUNCATE table itm_itt1;
    /* FASES Y OPERACIONES */
# TRUNCATE table wma_ompg; # Consolidado de Fases y Operaciones
# TRUNCATE table wma_mpg1; # Fases ordenadas
# TRUNCATE table wma_mpg2; #en desuso
# TRUNCATE table wma_mpg3; # nunca usado - Operaciones de una fase
    /* CATALOGOS */
# TRUNCATE table wma_owfa; # FASES
# TRUNCATE table wma_owop; # OPERACIONES
# TRUNCATE table wma_ocif; # en desuso - CIF
# TRUNCATE table wma_cif1;
# TRUNCATE table wma_cif2;
    /* DOCUMENTO DE PRODUCCIÓN */
TRUNCATE table wma3_doc99; # logs
TRUNCATE table wma3_opdp; # Planinificación de Producción
TRUNCATE table wma3_pdp1;
TRUNCATE table wma3_oodp; # Orden de Producción
TRUNCATE table wma3_odp1;
TRUNCATE table wma3_odp20; # Fases y Operaciones de una odp.
TRUNCATE table wma3_oddf; # Documento de Fase
TRUNCATE table wma3_ddf1;
TRUNCATE table wma3_odcf; # Documento de cierre Fase.
TRUNCATE table wma3_dcf1;
TRUNCATE table wma3_dcf2;

/* PRODUCTO EN PROCESO  --------------------------- */
    /* INVENTARIO PEP */
 TRUNCATE pep_oitw; #inventario por fase
 TRUNCATE pep_wtr1; #Historico
    /* DOCUMENTOS */
TRUNCATE pep_owht;
TRUNCATE pep_wht1;
TRUNCATE pep_oing;
TRUNCATE pep_ing1;
TRUNCATE pep_oegr;
TRUNCATE pep_egr1
