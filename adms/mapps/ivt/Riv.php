<?php
class ivtRiv{
static $serie='ivtRiv';
static $tbk='ivt_oriv';
static $tbk1='ivt_riv1';
static $tbk99='ivt_doc99';
static public function revDoc($_J=array()){
	$omitCard=($_J['omitCard']=='Y');
	if(!$omitCard && _js::iseErr($_J['cardId'],'Se debe definir un tercero (ID).'.$ori,'numeric>0')){}
	else if(!$omitCard && _js::iseErr($_J['cardName'],'Se debe definir un tercero (N).'.$ori)){}
	else if(_js::iseErr($_J['whsId'],'Se debe definir una bodega.','numeric>0')){}
	else if(_js::iseErr($_J['docDate'],'La fecha del documento debe estar definida.')){}
	else if($js=_js::textMax($_J['ref1'],20,'Ref. 1: ')){ _err::err($js); }
	else if($js=_js::textMax($_J['ref2'],20,'Ref. 2: ')){ _err::err($js); }
	else if($js=_js::textMax($_J['lineMemo'],200,'Detalles ')){ _err::err($js); }
	else if(!is_array($_J['L'])){ _err::err('No se han enviado lineas para el documento.',3); }
}
static $L1=array();
static $L1i=array(); //inventario
static $L1d=array();//Asiento Lineas
static function revL1($L=array(),$P=array(),$P2=array()){
	$ln=$P['ln']; $js=false;
	$ori=' on[ivtRiv::revL1()]';;
	$L['priceLine']=$L['priceLine']*$L['quantity'];
	if($L['handInv']!='Y'){ _err::err($ln.'El articulo debe ser inventariable.'.$ori,3); }
	else if(_js::iseErr($L['itemId'],$ln.'Se debe definir el ID del artículo.'.$ori,'numeric>0')){}
	else if(_js::iseErr($L['itemSzId'],$ln.'Se debe definir el Id del subproducto.'.$ori,'numeric>0')){}
	else if(_js::iseErr($L['quantity'],$ln.'Se debe definir la cantidad.'.$ori,'numeric>=0')){}
	else if(_js::iseErr($L['priceLine'],$ln.'Se debe definir el nuevo valor del inventario.'.$ori,'numeric>=0')){}
	else{
		$k=$L['whsId'].$L['itemId'].$L['itemSzId'];
		if(array_key_exists($k,self::$L1)){
			_err::err($ln.'Ya existe una linea igual en el documento. Linea: '.self::$L1[$k]['lineNum'].$ori,3);
		}
		else{
			$q=Ivt::getInf($L,array('ln'=>$ln,'reqAcc'=>array('accIvt','accAwh')));
			if(_err::$err){ }
			//if($q['_upd']!='Y' || $q['onHand']<=0){ _err::err($ln.'No se puede valorizar, porque no existen unidades en el inventario.',3); }
			else{
				$L['handAt']=$q['onHand'];
				$L['priceAt']=$q['stockValue'];
				$L['diffQty']=$L['quantity']-$q['onHand'];
				$L['diffPrice']=$L['priceLine']-$q['stockValue'];
				$L[0]='i'; $L[1]=self::$tbk1;
				self::$L1[$k]=$L;
				if($L['handAt']<=0){ $L['avgPriceAt']=0; }
				else{ $L['avgPriceAt']=$q['stockValue']/$q['onHand']; }
				$balActual=$q['stockValue'];// C 1000
				$balNue=$L['priceLine'];//d 900 =1000 +(-100)
				$bal61=$balNue-$balActual;//900-1000 = -100  d o c
				$debCre=($bal61>0)?'creBal':'debBal';//si tengo mas, cred ala 61
				//14xx          c 1000   valor actual 1000 pares
				//14xx d 900             valor nuevo 900 pares, me da $-100
				//61xx d 100             es neg, deb al costo en +100
				//61xx          c 100    es pos, cre al costo en -100
				self::$L1d[$k.'_1']=array('accId'=>$q['accIvt'],'creBal'=>$balActual,'accCode'=>'14xx');
				self::$L1d[$k.'_2']=array('accId'=>$q['accIvt'],'debBal'=>$balNue,'accCode'=>'14xx');
				self::$L1d[$k.'_3']=array('accId'=>$q['accAwh'],$debCre=>abs($bal61),'accCode'=>'61xx');
				
				//toInvetory
				$avgPrice=($L['quantity']>0)?$L['priceLine']/$L['quantity']:0;
				$kAct=($q['_upd']!='Y')?'i':'u';
				self::$L1i[]=[$kAct,'ivt_oitw','itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'onHand'=>$L['quantity'],
				'avgPrice'=>$avgPrice,'stockValue'=>$L['priceLine'],'_wh'=>'itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND whsId=\''.$L['whsId'].'\' LIMIT 1'];
			}
		}
	}
	return $L;
}

static public function post($_J=array()){
	self::revDoc($_J); unset($_J['omitCard']);
	if(_err::$err){}
	else{
		_ADMS::mApps('ivt/Ivt');
		$_J['docStatus']='C';
		$errs=0; $nl=0;
		a_sql::transaction(); $cmt=false;
		$_J=docSeries::nextNum($_J,$_J);
		$Lwtr=[]; $nCambios=0;//wtr1
		if(!_err::$err){
			$nDoc=new ivtDoc(array('tt'=>self::$serie));
			foreach($_J['L'] as $n=>$L){ $nl++;
				$L['whsId']=$_J['whsId'];
				$L=self::revL1($L,array('ln'=>'Linea '.$nl.': '));
				if(_err::$err){ $errs++; break; }
				else{
					$qty=0;
					if($L['diffQty']>0){ $qty=$L['diffQty']; $kQty='inQty'; }
					else if($L['diffQty']<=0){ $qty=abs($L['diffQty']); $kQty='outQty'; }
					if($qty>0){//solo registrar si diff
						$price=($L['quantity']>0)?$L['priceLine']/$L['quantity']:0;
						$Lx=['i','ivt_wtr1','tt'=>self::$serie,'serieId'=>$_J['serieId'],'docNum'=>$_J['docNum'],'docDate'=>$_J['docDate'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'quantity'=>$qty,$kQty=>$qty,
						'price'=>$price,'priceLine'=>$L['priceLine'],
						'onHandAt'=>$L['handAt'],'avgPrice'=>$L['avgPriceAt'],'stockValue'=>$L['priceAt'],'userId'=>a_ses::$userId];
						$Lwtr[]=$Lx; $nCambios++;
					}
				}
			}
			//(print_r(self::$L1i)); die(print_r($Lwtr));
			if($errs==0){/* Generar Doc, L1 y L2 */
				unset($_J['L'],$_J['Vats']);
				{
					$docEntry=a_sql::qInsert($_J,array('tbk'=>self::$tbk,'qk'=>'ud'));
					if(a_sql::$err){ _err::err('Error guardando documento: '.a_sql::$errText,3); $errs++; }
					else{
						$_J['docEntry']=$docEntry;
						$nDoc->Doc=$_J;
						a_sql::multiQuery(self::$L1i);
						if(_err::$err){ $errs++; }
						if(!_err::$err && $nCambios>0){
							a_sql::multiQuery($Lwtr,0,array('tr'=>$docEntry));
							if(_err::$err){ $errs++; }
						}
						if(!_err::$err){
							a_sql::multiQuery(self::$L1,0,array('docEntry'=>$docEntry));
							if(_err::$err){ $errs++; }
						}
					}
				}
			}
			if($errs==0){/* Contabilizar */
				self::dacPost($nDoc);
				if(_err::$err){ $errs++; }
			}
			if($errs==0){ $cmt=true;
				$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"');
				Doc::logPost(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$docEntry,'dateC'=>1));
			}
			a_sql::transaction($cmt);
		}
	}
	return $js;
}

static public function putCancel($D=array()){
	a_sql::transaction(); $ori=' on[ivtRiv::putCancel()]';
	Doc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>'ivt_owht','docEntry'=>$D['docEntry'],'serieType'=>self::$serie,'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$D['lineMemo']));
	if(_err::$err){ $js=_err::$errText; }
	else{ $errs=0;
		_ADMS::mApps('gfi/Dac');
		gfiDac::putCancel(array('tt'=>self::$serie,'tr'=>$D['docEntry']));
		if(_err::$err){ $js=_err::$errText; }
		else{
			_ADMS::_app('Ivt'); 
			Ivt::onHand_rever(array('tt'=>self::$serie,'tr'=>$D['docEntry'],'docDate'=>date('Y-m-d')));
			if(Ivt::$err){ $js=Invt::$errText; $errs++; }
			if($errs==0){
				$js=_js::r('Documento anulado correctamente.');
				a_sql::transaction(true);
			}
		}
	}
	return $js;
}

static public function dacPost($nDoc){
	$ori =' on[ivtRiv::dacPost()]';
	if($js=_js::ise($nDoc->Doc['docEntry'],'No se ha definido el número de documento.'.$ori,'numeric>0')){ _err::err($js); return array(); }
	$n=0; $errs=0;
	foreach(self::$L1 as $kk=>$L){
	}
	_ADMS::mApps('gfi/Dac');
	$nDac=new gfiDac(array('tt'=>self::$serie,'tr'=>$nDoc->Doc['docEntry']));
	$nDac->Doc=$nDoc->Doc;
	$nDac->setLine(self::$L1d);
	$nDac->post(); if(_err::$err){ return false; }
}
static public function logGet($D=array()){
	return Doc::logGet(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$D['docEntry']));
}
}
?>