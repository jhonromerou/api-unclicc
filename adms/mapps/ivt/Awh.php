<?php
class ivtAwh{
static $serie='ivtAwh';
static $tbk='ivt_oawh';
static $tbk1='ivt_awh1';
static $tbk99='ivt_doc99';
static $doAcc='Y'; //contabilizar
static public function setTbk($P=array()){
	self::$serie=$P['serie'];
	self::$tbk=$P['tbk'];
	self::$tbk1=$P['tbk1'];
	if($P['tbk99']){ self::$tbk99=$P['tbk99']; }
	if($P['doAcc']){ self::$tbk99=$P['doAcc']; }
}

static public function revDoc($_J=array()){
	$ori=' on[ivtAwh::revDoc()]';
	$omitCard=($_J['omitCard']=='Y');
	if(!$omitCard && _js::iseErr($_J['cardId'],'Se debe definir un tercero (ID).'.$ori,'numeric>0')){}
	else if(!$omitCard && _js::iseErr($_J['cardName'],'Se debe definir un tercero (N).'.$ori)){}
	else if(_js::iseErr($_J['whsId'],'Se debe definir una bodega.'.$ori,'numeric>0')){}
	else if(_js::iseErr($_J['docDate'],'La fecha del documento debe estar definida.'.$ori)){}
	else if($js=_js::textMax($_J['ref1'],20,'Ref. 1: '.$ori)){ _err::err($js); }
	else if($js=_js::textMax($_J['ref2'],20,'Ref. 2: '.$ori)){ _err::err($js); }
	else if($js=_js::textMax($_J['lineMemo'],200,'Detalles '.$ori)){ _err::err($js); }
	else if(!is_array($_J['L'])){ _err::err('No se han enviado lineas para el documento.'.$ori,3); }
}
static $L1=array();
static $L1d=array();//Asiento Lineas
static function revL1($L=array(),$P=array(),$P2=array()){
	$ln=$P['ln']; $js=false;
	$ori=' on[ivtAwh::revL1()]';;
	if($L['handInv']!='Y'){ _err::err($ln.'El articulo debe ser inventariable.'.$ori,3); }
	else if(_js::iseErr($L['itemId'],$ln.'Se debe definir el ID del artículo.'.$ori,'numeric>0')){}
	else if(_js::iseErr($L['itemSzId'],$ln.'Se debe definir el Id del subproducto.'.$ori,'numeric>0')){}
	else if(_js::iseErr($L['whsId'],$ln.'Se debe definir la bodega'.$ori,'numeric>0')){ }
	//else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.'.$ori,'numeric>0')){}
	else if(_js::iseErr($L['quantity'],$ln.'Se debe definir la cantidad.'.$ori,'numeric')){}
	else{
		$k=$L['whsId'].$L['itemId'].$L['itemSzId'];
		if(array_key_exists($k,self::$L1)){
			_err::err($ln.'Ya existe una linea igual en el documento. Linea: '.self::$L1[$k]['lineNum'].$ori,3);
		}
		else{
			$q=Ivt::getInf($L,array('ln'=>$ln,'reqAcc'=>array('accIvt','accAwh')));
			if(_err::$err){ }
			else{
				$L['handAt']=$q['onHand'];
				$L['diffQty']=$L['quantity']-$q['onHand'];
				$L['price']=$q['cost'];
				$L['priceLine']=$q['cost']*abs($L['diffQty']);
				$L[0]='i'; $L[1]=self::$tbk1;
				self::$L1[$k]=$L;
				$balActual=$q['cost']*$L['handAt'];// C 1000
				$balNue=$q['cost']*($L['handAt']+$L['diffQty']);//d 900 =1000 +(-100)
				$bal61=$balNue-$balActual;//900-1000 = -100  d o c
				$debCre=($bal61>0)?'creBal':'debBal';//si tengo mas, cred ala 61
				//14xx          c 1000   valor actual 1000 pares
				//14xx d 900             valor nuevo 900 pares, me da $-100
				//61xx d 100             es neg, deb al costo en +100
				//61xx          c 100    es pos, cre al costo en -100
				self::$L1d[$k.'_1']=array('accId'=>$q['accIvt'],'creBal'=>$balActual,'accCode'=>'14xx');
				self::$L1d[$k.'_3']=array('accId'=>$q['accIvt'],'debBal'=>$balNue,'accCode'=>'14xx');
				self::$L1d[$k.'_2']=array('accId'=>$q['accAwh'],$debCre=>abs($bal61),'accCode'=>'61xx');
				//die(print_r(self::$L1d));
				//toInvetory
				if($L['diffQty']>0){ $L['inQty']=$L['diffQty']; }
				else if($L['diffQty']<0){ $L['outQty']=abs($L['diffQty']); }
				$L['onHand']=$q['onHand'];
				$L['cost']=$q['cost'];
				$L['stockValue']=$q['stockValue'];
				$L['_upd']=$q['_upd'];
			}
		}
	}
	return $L;
}

static public function post($_J=array()){
	$ori=' on[ivtAwh::post()]';
	self::revDoc($_J); unset($_J['omitCard']);
	if(_err::$err){}
	else{
		_ADMS::mapps('ivt/Ivt');
		$_J['docStatus']='C';
		$errs=0; $nl=0;
		$nDoc=new ivtDoc(array('tt'=>self::$serie));
		foreach($_J['L'] as $n=>$L){ $nl++;
			$L['whsId']=$_J['whsId'];
			$L=self::revL1($L,array('ln'=>'Linea '.$nl.': '));
			if(_err::$err){ $errs++; break; }
			else{
				if($L['diffQty']!=0){
					$nDoc->handSet($L,0,$L);
				}
			}
		}
		if($errs==0){/* Generar Doc, L1 y L2 */
			unset($_J['L'],$_J['Vats']);
			a_sql::transaction(); $cmt=false;
			$_J=docSeries::nextNum($_J,$_J);
			if(_err::$err){ $errs++; }
			else{
				$docEntry=a_sql::qInsert($_J,array('tbk'=>self::$tbk,'qk'=>'ud'));
				if(a_sql::$err){ _err::err('Error guardando documento: '.a_sql::$errText,3); $errs++; }
				else{
					$_J['docEntry']=$docEntry;
					$nDoc->Doc=$_J;
					a_sql::multiQuery(self::$L1,0,array('docEntry'=>$docEntry));
					if(_err::$err){ $errs++; }
				}
			}
		}
		if($errs==0){/* Contabilizar */
			self::dacPost($nDoc);
			if(_err::$err){ $errs++; }
		}
		if($errs==0){/* Mover Inventario */
			$nDoc->handPost();
			if(_err::$err){ $errs++; }
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"');
			//$js=_js::e(3,'Documento guardado correctamente.');
			_ADMS::lib('JLog');
			JLog::post(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$docEntry,'dateC'=>1));
		}
		a_sql::transaction($cmt);
	}
	return $js;
}
static public function putCancel($D=array()){
	a_sql::transaction(); $cmt=false; $ori=' on[ivtAwh::putCancel()]';
	iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$D['docEntry'],'serieType'=>self::$serie,'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$D['lineMemo']));
	if(_err::$err){ $js=_err::$errText; }
	else{ $errs=0;
		_ADMS::mApps('gfi/Dac');
		gfiDac::putCancel(array('tt'=>self::$serie,'tr'=>$D['docEntry']));
		if(_err::$err){ $js=_err::$errText; }
		else{
			_ADMS::mApps('ivt/Ivt'); 
			IvtDoc::rever(array('tt'=>self::$serie,'tr'=>$D['docEntry'],'docDate'=>date('Y-m-d')));
			if(_err::$err){ $js=_err::$errText;}
			else{ $cmt=true;
				$js=_js::r('Documento anulado correctamente.');
			}
		}
	}
	a_sql::transaction($cmt);
	return $js;
}

static public function dacPost($nDoc){
	if(self::$doAcc=='Y'){
		$ori =' on[ivtAwh::dacPost()]';
		if($js=_js::ise($nDoc->Doc['docEntry'],'No se ha definido el número de documento.'.$ori,'numeric>0')){ _err::err($js); return array(); }
		$n=0; $errs=0;
		foreach(self::$L1 as $kk=>$L){
		}
		_ADMS::mApps('gfi/Dac');
		$nDac=new gfiDac(array('tt'=>self::$serie,'tr'=>$nDoc->Doc['docEntry']));
		$nDac->Doc=$nDoc->Doc;
		$nDac->setLine(self::$L1d);
		$nDac->post(); if(_err::$err){ return false; }
	}
}
static public function logGet($D=array()){
	_ADMS::lib('JLog');
	return JLog::get(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$D['docEntry']));
}
}
?>
