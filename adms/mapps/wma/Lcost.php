<?php
class wmaLcost{//_itm
static $Mt=array();
static public function put($P=array()){ /* log costos */
	$Di=array('updateType'=>$P['updateType'],'itemId'=>$P['itemId'],'itemSzId'=>$P['itemSzId']);
	if($Di['updateType']=='defineCost'){
		$Di['userExec']=a_ses::$userId;
		$Di['isExec']='Y'; $Di['dateExec']=date('Y-m-d H:i:s');
	}
	$Di['vari1']=$P['vari1']; $P['itemSzId']*=1;
	$Di['lineMemo']=$P['lineMemo']; $Di['dateC']=date('Y-m-d H:i:s');
	return a_sql::uniRow($Di,array('tbk'=>'itm_log2','userId'=>a_ses::$userId,'wh_change'=>' updateType=\''.$P['updateType'].'\' AND itemId=\''.$P['itemId'].'\' AND itemSzId=\''.$P['itemSzId'].'\' AND isExec=\'N\' LIMIT 1'));
}
static public function setUpdSubEsc($a=1,$itemId){
	$ori='('.$a.') on[wmaLcost::setUpdSubEsc()]';
	$wh='isModel=\'Y\' AND itemId=\''.$itemId.'\'';
	if($a=='Y'){
		$q=a_sql::query('UPDATE itm_oipc SET updSubEsc=\'Y\' WHERE '.$wh.' LIMIT 1',array(1=>'Error actualizando requerimiento actualización escandallos de subproductos.'.$ori));
	}
	else if($a=='N'){
		a_sql::query('UPDATE itm_oipc SET updSubEsc=\'N\',dateUpd=\''.date('Y-m-d H:i:s').'\' WHERE '.$wh.' LIMIT 1',array(1=>'Error actualizando requerimiento actualización escandallos de subproductos.'.$ori));
	}
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
}

static public function mpgSet1($P=array()){//Actualizar itm_oipc y wma_mpg1
	$ori=' on[wmaLcost::mpgSet1()]';
	$qU=array();
$gb='B.wfaId';
	$qu='SELECT '.$gb.',
	SUM(IF(B.lineType IN(\'MP\',\'SE\'),B.lineTotal,0)) costMP,
	SUM(IF(B.lineType=\'MO\',B.lineTotal,0)) costMO,
	SUM(IF(B.lineType=\'MA\',B.lineTotal,0)) costMA,
	SUM(IF(B.lineType=\'CIF\',B.lineTotal,0)) cif
	FROM itm_itt1 B
	WHERE B.itemId=\''.$P['itemId'].'\' AND B.itemSzId=0
	GROUP BY '.$gb;
	$q=a_sql::query($qu,array(1=>'Error obteniendo información para recalcular ficha de costos de los subproductos.'.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{ //costo general
		$qU[0]=array('u','itm_oipc','isModel'=>'Y','itemid'=>$P['itemId'],'cost'=>0,'costMO'=>0,'costMP'=>0,'costMA'=>0,'cif'=>0,'_wh'=>'isModel=\'Y\' AND itemId=\''.$P['itemId'].'\' LIMIT 1');
		if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){//costos de fases
				$k2='F_'.$L['wfaId'];
				if(!array_key_exists($k2,$qU)){
					$whU='itemId=\''.$P['itemId'].'\' AND  wfaId=\''.$L['wfaId'].'\' LIMIT 1';
					$qU[$k2]=array('u','wma_mpg1','itemid'=>$P['itemId'],'wfaId'=>$L['wfaId'],'cost'=>0,'costMO'=>0,'costMP'=>0,'costMA'=>0,'cif'=>0,
					'_wh'=>$whU);
				}
				$qU[$k2]['costMO'] +=$L['costMO'];
				$qU[$k2]['costMP'] +=$L['costMP'];
				$qU[$k2]['costMA'] +=$L['costMA'];
				$qU[$k2]['cif'] +=$L['cif'];
				$qU[$k2]['cost'] +=$qU[$k2]['costMO']+$qU[$k2]['costMP']+$qU[$k2]['costMA']+$qU[$k2]['cif'];
				//base
				$qU[0]['costMO'] +=$L['costMO'];
				$qU[0]['costMP'] +=$L['costMP'];
				$qU[0]['costMA'] +=$L['costMA'];
				$qU[0]['cif'] +=$L['cif'];
			}
		}
		$qU[0]['cost'] =$qU[0]['costMO']+$qU[0]['costMP']+$qU[0]['costMA']+$qU[0]['cif'];
		$r=a_sql::multiQuery($qU);
		if(_err::$err){}
		else if(a_sql::$affected_rows>0){
			self::put(array('updateType'=>'model','itemId'=>$P['itemId']));
		}
	}
}

static public function mpgSet2($P=array()){//actualiza itm_oipc isModel=N
	$ori=' on[wmaLcost::ipcSet()]';
	$qU=array();
$gb='B.isVari,G1.itemSzId,B.wfaId';
	$qu='SELECT '.$gb.',
	SUM(IF(B.lineType IN(\'MP\',\'SE\'),B.lineTotal,0)) costMP,
	SUM(IF(B.lineType=\'MO\',B.lineTotal,0)) costMO,
	SUM(IF(B.lineType=\'MA\',B.lineTotal,0)) costMA,
	SUM(IF(B.lineType=\'CIF\',B.lineTotal,0)) cif
	FROM itm_oitm I
	JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
	JOIN itm_grs1 G1 ON (G1.itemSzId=G2.itemSzId)
	LEFT JOIN itm_itt1 B ON (B.itemId=I.itemId AND ((B.itemSzId=0 AND B.variType=\'N\') OR 
	(B.isVari=\'Y\' AND B.itemSzId=G1.itemSzId)
	))
	WHERE I.itemId=\''.$P['itemId'].'\'
	GROUP BY '.$gb;
	$q=a_sql::query($qu,array(1=>'Error obteniendo información para recalcular ficha de costos de los subproductos.'.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		if(a_sql::$errNo==-1){
			$priTa=''; //primera talla;
			while($L=$q->fetch_assoc()){
				$k='0_'.$L['itemSzId'].$L['wfaId'];
				$whU='isModel=\'N\' AND itemId=\''.$P['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND wfaId=\''.$L['wfaId'].'\' LIMIT 1';
				if(!array_key_exists($k,$qU)){//cada talla
					//insertar si no existe
					$B=array('isModel'=>'N','itemId'=>$P['itemId'],'itemSzId'=>$L['itemSzId'],'wfaId'=>$L['wfaId']);
					a_sql::uniRow($B,array('tbk'=>'wma_mpg2','wh_change'=>$whU));
					if(a_sql::$err){ _err::err('Error actualizando costos: '.a_sql::$errText,3); break; }
					$qU[$k]=array('u','wma_mpg2','itemSzId'=>$L['itemSzId'],'wfaId'=>$L['wfaId'],'cost'=>0,'costMO'=>0,'costMP'=>0,'costMA'=>0,'cif'=>0,
					'_wh'=>$whU);
				}
				$qU[$k]['costMO'] +=$L['costMO'];
				$qU[$k]['costMP'] +=$L['costMP'];
				$qU[$k]['costMA'] +=$L['costMA'];
				$qU[$k]['cif'] +=$L['cif'];
				$qU[$k]['cost'] +=$qU[$k]['costMO']+$qU[$k]['costMP']+$qU[$k]['costMA']+$qU[$k]['cif'];
			}
		}
		a_sql::multiQuery($qU);
		if(_err::$err){}
	}
}
}
?>